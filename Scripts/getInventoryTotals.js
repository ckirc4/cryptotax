const fs = require('fs')
const { getOutputFolder, FINANCIAL_YEARS } = require('./constants')

const financialYearInput = process.argv[2]
const currencies = process.argv.slice(3) ?? []

const financialYears = financialYearInput === 'all' ? FINANCIAL_YEARS : [financialYearInput]

for (const financialYear of financialYears) {
  const runState = `${getOutputFolder(financialYear)}RunState.json`
  const data = JSON.parse(fs.readFileSync(runState))

  console.log('Getting inventory contents for financial year', financialYear)

  const contents = data.Inventories[0].m
  let totals = []
  for (const currency of Object.keys(contents)) {
    if (currencies.length > 0 && !currencies.includes(currency)) {
      continue
    }

    const total = contents[currency].reduce((x, y) => x + y.a, 0)
    totals.push([currency, total])
  }

  const sortedTotals = totals.sort((x, y) => y[1] - x[1])
  for (const [currency, total] of sortedTotals) {
    console.log(currency, total)
  }
}
