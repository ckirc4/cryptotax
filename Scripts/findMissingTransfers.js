// There used to be a bug where it was possible for matched Transfers to finish before they started
// due to differences in how exchanges report the transaction status. These funds were missing because
// we tried to add them back to the inventory before they got removed, i.e. nothing was added back,
// but everything got removed shortly thereafter.
// This got fixed just before the 2021-2022 report (#n6).
// We now find ourselves in a position where some of these missing funds are being disposed/transferred
// and the inventory is overdrawn.
// We need to go back over the previous Reports and find situations where this happened, list all
// parcels that were removed and never added back, and then add them back manually.

const fs = require('fs')
const { getOutputFolder, getReportCells } = require('./constants')


// the bug was present during these years:
const FINANCIAL_YEARS = ['2016-2017', '2017-2018', '2018-2019', '2019-2020', '2020-2021']

for (const financialYear of FINANCIAL_YEARS) {
  console.log('Analysing report for year', financialYear)

  let foundMissingFunds = [] // array of arrays, where the inner dimensions is the rows relating to the bug
  const folder = getOutputFolder(financialYear)
  const files = fs.readdirSync(folder)
  for (const file of files) {
    if (!file.startsWith('Report_')) {
      continue
    }

    const reportLines = fs.readFileSync(folder + file).toString().split('\n')
    let currentTimestamp = null
    let startLine = null
    for (let i = 1; i < reportLines.length; i++) {
      const line = reportLines[i]
      if (line === '') {
        continue
      }

      const cells = getReportCells(line)

      if (currentTimestamp != null) {
        if (cells[1] !== currentTimestamp) {
          // this transfer event has finished, write it to an output file
          console.log(`Found lost transfer spanning ${i - startLine} lines starting at line ${startLine + 1}`)
          foundMissingFunds.push(startLine)

          currentTimestamp = null
          startLine = null
        } else {
          // this transfer event is ongoing
        }
      } else if (cells[2] === 'Action' && cells[3] === 'Transfer' && cells[4].includes('instantly')) {
        // the word "instantly" is used for durations <= 0

        // if the next N rows are of type State, where we remove the parcels, it signifies that
        // the duration is < 0 (otherwise we wouldn't have removed the parcels).

        // at this point we have found an instance of the bug - log all of these N rows.
        // no need to check if they ever get added back - they won't.
        currentTimestamp = cells[1]
        startLine = i
      }
    }

    if (currentTimestamp != null) {
      // this should never happen unless the report happens to be split during the transfer event, which is unlikely
      throw new Error('Currently analysing missing funds, but the report has ended.')
    }
  }

  if (foundMissingFunds.length > 0) {
    console.log(`Found ${foundMissingFunds.length} missing funds for this financial year.`)
  } else {
    console.log('Did not found any missing funds for this financial year.')
  }
}

