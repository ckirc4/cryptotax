// These are from the XRP withdrawal from BTCMarkets to FTX (tx 159B871BE2C8A2500709F86B871E7EDCE1AD2E10F1DB4B82995FB429BC125DA9) on 1/2/2021
// Once parsed, I will manually add them to the output state of the previous run (#n6, 2021-2022) so they can be re-included in the next tax report.
// I will then manually add a new line to the finished report stating the extra-imported currency amounts.
const data = `CurrencyAmount,creation type and line in report,date(UTC),amount(XRP),value(AUD),exchange,originalId(optional)
#c2242991,purch on 786,01/02/2021 20:40:37,3.23583182,1.7586745900000000000000000001,BTCMarkets,
#c2242978,purch on 772,01/02/2021 20:25:43,6750,3557.25,BTCMarkets,
#c2242952,split on 756,29/01/2021 04:45:52,18.3361,6.8100275400000000000000000014,FTX,#c2242922
#c2242931,purch on 735,29/01/2021 04:45:52,12,4.4568,FTX,
#c2242940,purch on 744,29/01/2021 04:45:52,1,0.3714,FTX,
#c2242461,purch on 269,22/10/2020 20:38:47,800,295.68,BTCMarkets,
#c2242494,purch on 307,12/11/2020 22:16:21,560,198.072,BTCMarkets,
#c2242316,purch on 123,05/10/2020 21:44:18,1130,396.969,BTCMarkets,
#c2242473,purch on 282,29/10/2020 20:27:10,570,198.474,BTCMarkets,
#c2242296,purch on 101,25/09/2020 19:37:37,570,198.075,BTCMarkets,
#c2242831,purch on 640,28/01/2021 22:11:47,250,86.225,BTCMarkets,
#c2242851,purch on 659,28/01/2021 22:13:07,613.14474771,211.47362348000000000000000002,BTCMarkets,
#c2242842,purch on 650,28/01/2021 22:12:30,286.85525229,98.93637651,BTCMarkets,
#c2242354,purch on 160,06/10/2020 02:43:56,832,286.624,FTX,
#c2242367,purch on 171,06/10/2020 02:43:56,1,0.3445,FTX,
#c2242376,purch on 180,06/10/2020 02:43:56,7,2.4115,FTX,
#c2242385,purch on 189,06/10/2020 02:43:56,10,3.445,FTX,
#c2242394,purch on 198,06/10/2020 02:43:56,10,3.445,FTX,
#c2242403,purch on 207,06/10/2020 02:43:56,5,1.7225,FTX,
#c2242412,purch on 216,06/10/2020 02:43:56,5,1.7225,FTX,
#c2242421,purch on 225,06/10/2020 02:43:56,5,1.7225,FTX,
#c2242440,purch on 245,06/10/2020 03:10:19,1,0.3445,FTX,
#c2242484,purch on 296,06/11/2020 00:34:39,580,198.128,BTCMarkets,
#c2242451,purch on 258,16/10/2020 09:14:29,300,102,BTCMarkets,
#c2242286,purch on 90,10/09/2020 07:50:57,1175,396.5625,BTCMarkets,
#c2242306,purch on 112,02/10/2020 05:17:58,600,197.34,BTCMarkets,
#c2218706,imported,22/05/2020 21:09:48,1600,495.68,BTCMarkets,
#c2242781,purch on 593,01/01/2021 12:52:00,650,198.315,BTCMarkets,
#c1982176,imported,23/12/2019 00:21:39,1.1112,0.314792081352,Binance,#c1982165
#c2243001,split on 795,27/06/2020 10:00:52,16.54269918,4.373889663192,BTCMarkets,#c2242174`

let results = []
for (const line of data.split('\n').slice(1)) {
  const cells = line.split(',')
  if (cells.length !== 7) {
    throw new Error(`Expected 7 lines but got ${cells.length} (${line})`)
  }

  const [id, type, date, amount, value, exchange, originalId] = cells

  let exchangeId
  if (exchange === 'Binance') {
    exchangeId = 0
  } else if (exchange === 'BTCMarkets') {
    exchangeId = 4
  } else if (exchange === 'FTX') {
    exchangeId = 1
  } else {
    throw new Error(`Invalid exchange "${exchange}"`)
  }

  const [datePart, timePart] = date.split(' ')
  const timestamp = Date.parse(`${datePart.split('/').reverse().join('-')} ${timePart}`)
  const dateStr = new Date(timestamp).toISOString()

  results.push({
    i: {
      Value: id
    },
    o: {
      Value: originalId === '' ? null : originalId,
      HasValue: originalId === '' ? false : true
    },
    g: dateStr,
    c: {
      Value: 'XRP'
    },
    a: Number(amount),
    v: Number(value),
    t: dateStr,
    e: exchangeId
  })
}

// to be copied into the RunState.json of the previous run
console.log(JSON.stringify(results))

// to be copied into the report of the new run
console.log(results.map(x => {
  return `${x.i.Value} (${x.a} XRP valued at ${x.v} AUD)`
}).join(', '))

console.log(results.reduce((acc, x) => acc + x.a, 0))