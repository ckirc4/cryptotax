const BASE_PATH = 'C:\\Users\\chris\\Google Drive\\Other\\Trading\\Tax\\'

const FINANCIAL_YEARS = ['2016-2017', '2017-2018', '2018-2019', '2019-2020', '2020-2021', '2021-2022', '2022-2023', '2023-2024']

function getOutputFolder(financialYear) {
  switch (financialYear) {
    case '2016-2017':
      return `${BASE_PATH}Data\\Output\\#n1\\`
    case '2017-2018':
      return `${BASE_PATH}Data\\Output\\#n2\\`
    case '2018-2019':
      return `${BASE_PATH}Data\\Output\\#n3\\`
    case '2019-2020':
      return `${BASE_PATH}Data\\Output\\#n4\\`
    case '2020-2021':
      return `${BASE_PATH}Data\\Output\\#n5\\`
    case '2021-2022':
      return `${BASE_PATH}Data\\Output\\#n6\\`
    case '2022-2023':
      return `${BASE_PATH}Data\\Output\\#n7\\`
    case '2023-2024':
      return `${BASE_PATH}Data\\Output\\#n8\\`
    default:
      throw new Error(`Invalid financial year ${financialYear}`)
  }
}

function getReportCells (line) {
  let cells = []
  let currentCell = ''
  let depth = 0
  for (const c of line) {
    if (c === '"') {
      depth = depth === 0 ? 1 : 0
      continue
    } else if (c === ',' && depth === 0) {
        cells.push(currentCell)
        currentCell = ''
    } else {
      currentCell += c
    }
  }

  cells.push(currentCell ?? '')

  if (cells.length !== 9) {
    console.log(cells.length)
    console.log(line)
    throw new Error(`Invalid number of cells in this Report line: ${line} (${cells.join('|')})`)
  }

  return cells
}

module.exports = {
  FINANCIAL_YEARS,
  getOutputFolder,
  getReportCells
}
