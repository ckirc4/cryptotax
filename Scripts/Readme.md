The following are re-usable scripts to help reporting or debugging.

# `getInventoryTotals`
Gets the aggregate inventory contents after the given financial year concluded.

Arguments:
- Financial Year: If `'all'`, returns the inventory contents for each financial year.
- Currencies (optional): The filter of currencies to return. If not set, returns all currencies.

Examples:
- `node getInventoryTotals 2020-2021`
- `node getInventoryTotals all BTC XRP`
