using CryptoTax.Domain;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Domain.Stores;
using CryptoTax.Persistence.Services;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using System;

namespace CryptoTax
{
    internal class Program
    {
        /// <summary>
        /// Performs tax calculation with the given parameters (using default values where applicable) and returns a positive value if the application completed successfully, or a negative value if there was an uncaught exception. A value of zero indicates a fatal error, and no output folder will be available.<br/>
        /// The absolute value specifies the run number that was just completed, e.g. a return value of 151 implies that run with id #151 (found under /Data/Output/#n151/) has completed successfully.<br/>
        /// Results are available via the Results.json file (not guaranteed to exist if the application did not complete successfully).
        /// </summary>
        static int Main(string[] args)
        {
            bool inputDisabled = args.Length > 0 && !Constants.DRY_RUN;
            bool isError = false;
            int runId = 0;

            // Base services
            ICryptoService cryptoService = new CryptoService();
            IFileService fileService = new FileService(cryptoService); //new FileService(cryptoService);
            IFileService fileServiceSafe = fileService; //new FileServiceSafe(cryptoService);
            IWebService webService = new WebService();

            // There is a bit of a circular dependency going on - we want logging to be available as early
            // as possible, but we cannot write to the file system until we know which output folder to use.
            // Further, in order to enable logging in the persistor, we must use a generator function so the
            // loggingService can inject itself into the persistor.
            ILoggingSettings loggingSettings = new LoggingSettings(LoggingTemplate.Console_Verbose, LoggingTemplate.Persistor_Verbose);
            ILoggingPersistorService loggingPersistorService = null;
            Func<ILoggingService, ILoggingPersistorService> loggingPersistorServiceGenerator = log =>
            {
                loggingPersistorService = new LoggingPersistorService(fileService, log);
                return loggingPersistorService;
            };
            ILoggingService loggingService = new LoggingService(loggingSettings, new ConsoleFrontEndService(), loggingPersistorServiceGenerator);
            if (loggingPersistorService == null) throw new NullReferenceException("LoggingPersistorService has not been set.");
            object obj = new object();
            IRegisteredLogger programLogger = loggingService.Register(obj, "Application");
            // programLogger.Log(LogMessage.MSG_UI_COLOUR_TEST);

            try
            {
                // Id Service
                IdProviderPersistorService idProviderPersistor = new IdProviderPersistorService(
                    fileServiceSafe,
                    Constants.ID_DATA_PATH);
                IdProviderService idProviderService = new IdProviderService(idProviderPersistor);

                // Run Service
                RunPersistorService runPersistorService = new RunPersistorService(
                    fileService,
                    Constants.OUTPUT_DATA_PATH);
                RunService runService = new RunService(
                    idProviderService,
                    runPersistorService,
                    loggingService);
                runService.Initialise(args);
                loggingService.UpdateSettings(new LoggingSettings(Constants.DRY_RUN ? LoggingTemplate.Console_Verbose : runService.GetConsoleLoggingTemplate(), runService.GetPersistorLoggingTemplate()));
                loggingPersistorService.SetFolder(Constants.OUTPUT_DATA_PATH(runService.GetRunId()));
                runId = IdProviderService.TryConvertToNumeric(runService.GetRunId()).Value;

                // Reporting
                ReportingPersistorService reportingPersistorService = new ReportingPersistorService(
                    fileService,
                    Constants.OUTPUT_DATA_PATH(runService.GetRunId()));
                ReportRowGenerationService reportRowGenerationService = new ReportRowGenerationService(idProviderService);
                ReportingService reportingService = new ReportingService(
                    loggingService,
                    runService,
                    reportingPersistorService,
                    reportRowGenerationService);

                // Candlesticks
                CandlestickPersistorService candlestickPersistor = new CandlestickPersistorService(
                    fileServiceSafe,
                    Constants.CANDLESTICK_DATA_PATH);
                IExchangeResponsePersistorService responsePersistor = new ExchangeResponsePersistorService(fileService, Constants.CANDLESTICK_DATA_PATH);
                MissingCandlestickPersistorService missingCandlestickPersistor = new MissingCandlestickPersistorService(fileService, Constants.CANDLESTICK_DATA_PATH, runService.GetCandlestickInterval());
                ExchangeCache exchangeCache = new ExchangeCache(candlestickPersistor, responsePersistor, missingCandlestickPersistor);

                // Prices
                IExchangeApiService btcMarketsApiService = new BtcMarketsApiService(webService, loggingService, exchangeCache);
                IExchangeApiService binanceSpotApiService = new BinanceSpotApiService(webService, loggingService, exchangeCache);
                IExchangeApiService ftxApiService = new FtxApiService(webService, loggingService, exchangeCache);
                IExchangeApiService krakenApiService = new KrakenApiService(webService, loggingService, exchangeCache);
                IPriceCalculator priceCalculator = new PriceCalculatorFactory().Create(runService.GetPriceCalculationType());

                ICurrencyConversionService currencyConversionService = new CurrencyConversionService(
                    btcMarketsApiService,
                    binanceSpotApiService,
                    ftxApiService,
                    krakenApiService,
                    reportingService,
                    idProviderService,
                    runService,
                    priceCalculator);

                BulkCandlestickKeyPersistorService bulkCandlestickKeyPersistor = new BulkCandlestickKeyPersistorService(fileService, Constants.CANDLESTICK_DATA_PATH);
                BulkCandlestickService bulkCandlestickService = new BulkCandlestickService(
                    btcMarketsApiService,
                    binanceSpotApiService,
                    ftxApiService,
                    krakenApiService,
                    runService,
                    fileService,
                    loggingService,
                    exchangeCache,
                    bulkCandlestickKeyPersistor);

                // Inventories
                IInventoryFactory inventoryFactory = InventoryFactoryGetter.Get(runService.GetInventoryType());
                IInventoryManagerService inventoryManagerService = new InventoryManagerService(idProviderService, runService, inventoryFactory, reportingService, loggingService);

                // Stores
                AssessableIncomeStore assessableIncomeStore = new AssessableIncomeStore();

                // Processing
                TransactionWhitelistService transactionWhitelistService = new TransactionWhitelistService(loggingService);
                TransferService transferService = new TransferService(idProviderService, transactionWhitelistService, loggingService);
                TimeEventService timeEventService = new TimeEventService();
                FtxStablecoinService ftxStablecoinService = new FtxStablecoinService(idProviderService, loggingService, reportingService, currencyConversionService);
                TradeProcessingService tradeProcessingService = new TradeProcessingService(idProviderService, currencyConversionService, reportingService, inventoryManagerService, bulkCandlestickService, loggingService);
                TransactionProcessingService transactionProcessingService = new TransactionProcessingService(idProviderService, currencyConversionService, reportingService, inventoryManagerService, transferService, timeEventService, assessableIncomeStore, transactionWhitelistService, bulkCandlestickService, loggingService, ftxStablecoinService);

                // Data
                ExchangeDataLocationService exchangeDataLocationService = new ExchangeDataLocationService(Constants.TRADE_LOGS_PATH);
                DtoCachePersistorService dtoCachePersistorService = new DtoCachePersistorService(fileService, cryptoService, exchangeDataLocationService, Constants.EXCHANGE_DATA_CACHE_PATH);
                CsvDataReaderServiceFactory csvDataReaderServiceFactory = new CsvDataReaderServiceFactory();
                DataImporterService dataImporterService = new DataImporterService(fileService, exchangeDataLocationService, dtoCachePersistorService, csvDataReaderServiceFactory);

                // Calculator
                TaxCalculator calculator = new TaxCalculator(
                    dataImporterService,
                    loggingService,
                    idProviderService,
                    reportingService,
                    runService,
                    tradeProcessingService,
                    transactionProcessingService,
                    inventoryManagerService,
                    timeEventService,
                    assessableIncomeStore,
                    bulkCandlestickService);

                // Start program
                CalculationResults results = calculator.DoTaxCalculation().Result;

                reportingService.Flush();
                idProviderService.Flush();
                runService.Save();
                runService.SaveState();
                runService.SaveResults(results);

                programLogger.Log(LogMessage.MSG_RESULTS, results.NetCapitalGains, results.AssessableIncome);
            }
            catch (Exception e)
            {
                programLogger.Log(LogMessage.MSG_FATAL_UNEXPECTED_EXCEPTION, e);
                isError = true;
            }

            loggingService.Flush();
            if (fileService is FileServiceCached fileServiceCached) fileServiceCached.FlushAll();

            if (!inputDisabled)
            {
                Console.WriteLine("Done. Press Enter to exit.");
                Console.ReadLine();
            }
            return (isError ? -1 : 1) * runId;
        }
    }
}
