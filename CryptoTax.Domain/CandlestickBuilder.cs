﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTax.Domain
{
    public class CandlestickBuilder
    {
        private readonly CandlestickInterval interval;
        private readonly DateTime fromUTCInclusive;
        private readonly DateTime toUTCExclusive;
        private readonly ExchangeSymbol symbol;

        private List<Candlestick> candlesticks;
        private Candlestick current; // not nullable because it's a pain, but considered null if its open price is -1
        private bool hasFinished;

        /// <summary>
        /// toUTC is the startTime of the last candlestick to build.
        /// </summary>
        public CandlestickBuilder(CandlestickInterval interval, DateTime fromUTC, DateTime toUTC, ExchangeSymbol symbol)
        {
            if (CandlestickHelpers.GetStartTime(interval, fromUTC) != fromUTC || CandlestickHelpers.GetStartTime(interval, toUTC) != toUTC)
            {
                throw new Exception("Start/end times of the CandlestickBuilder do not align with standard candlestick times");
            }

            this.interval = interval;
            this.fromUTCInclusive = fromUTC;
            this.toUTCExclusive = CandlestickHelpers.GetNextTime(interval, toUTC);
            this.symbol = symbol;

            this.candlesticks = new List<Candlestick>();
            this.current = new Candlestick(fromUTC, interval, symbol, -1, -1);
            this.hasFinished = false;
        }

        /// <summary>
        /// Symchronously add your trades, sorted by time in ascending order.
        /// Returns true if all required candlesticks have been fully constructed (do NOT call AddTrade again after this).
        /// </summary>
        public bool AddTrade(DateTime timeUTC, decimal price)
        {
            if (this.hasFinished)
            {
                throw new Exception("Cannot add new trades because the builder has finished.");
            }

            if (!this.hasCurrent())
            {
                // this is undefined behaviour - it is better to be safe and throw here and deal with it if it ever comes up
                if (timeUTC >= this.toUTCExclusive)
                {
                    throw new Exception("The first trade must be within or before the given time range.");
                }

                // note: we allow trades before the range for easier construction of candlesticks (e.g. if no trade data is available
                // at the start time, we can construct a candlestick nonetheless from the previous price data)
                DateTime startTime = CandlestickHelpers.GetStartTime(this.interval, timeUTC);
                this.current = new Candlestick(startTime, this.interval, this.symbol, price, price);
            }
            else
            {
                bool hasAdvanced = false;
                while (timeUTC >= this.current.GetNextTime())
                {
                    this.advanceCandlestick();
                    hasAdvanced = true;

                    if (this.shouldTerminate())
                    {
                        this.hasFinished = true;
                        return true;
                    }
                }

                // update candlestick
                if (hasAdvanced)
                {
                    this.current.Open = price;
                }
                this.current.Close = price;
            }

            return false;
        }

        /// <summary>
        /// May have leading/trailing blanks, but never internal blanks.
        /// </summary>
        public List<Candlestick> GetResults()
        {
            if (this.candlesticks.Count == 0 || !this.hasFinished)
            {
                throw new Exception("The CandlestickBuilder has not finished building candlesticks.");
            }

            return this.candlesticks.Where(c => c.StartTime >= this.fromUTCInclusive).ToList();
        }

        public bool hasCurrent()
        {
            return this.current.Open != -1;
        }

        private bool shouldTerminate()
        {
            return this.current.StartTime >= this.toUTCExclusive;
        }

        private void advanceCandlestick()
        {
            Candlestick newCandlestick = new Candlestick(this.current);
            this.candlesticks.Add(this.current);
            this.current = newCandlestick;
        }
    }
}
