﻿using System;

namespace CryptoTax.Domain.Stores
{
    /// <summary>
    /// In-memory store - no need for persistence.
    /// </summary>
    public class AssessableIncomeStore
    {
        private decimal _AssessableIncome;

        public AssessableIncomeStore()
        {
            this._AssessableIncome = 0;
        }

        public decimal GetTotalAssessableIncome() => this._AssessableIncome;

        public void AddAssessableIncome(decimal income)
        {
            if (income < 0) throw new ArgumentException("Provided assessable income must be a non-negative number");
            this._AssessableIncome += income;
        }
    }
}
