﻿namespace CryptoTax.Domain.Enums
{
    public enum DisposalType
    {
        SpotSale,
        FuturesPnlLoss,
        OptionsPnlLoss,
        FuturesFundingPayment,
        /// <summary>
        /// Disposal due to converting small asset to BNB
        /// </summary>
        DustConversionPayment
    }
}
