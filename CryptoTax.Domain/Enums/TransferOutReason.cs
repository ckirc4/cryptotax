﻿namespace CryptoTax.Domain.Enums
{
    public enum TransferOutReason
    {
        /// <summary>
        /// We want to transfer out funds to burn them.
        /// </summary>
        Burn,
        /// <summary>
        /// We want to transfer out funds to send them to a different inventory.
        /// </summary>
        Transfer
    }
}
