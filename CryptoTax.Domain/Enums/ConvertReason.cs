﻿namespace CryptoTax.Domain.Enums
{
    public enum ConvertReason
    {
        FtxStablecoinWithdrawal,
        FtxStablecoinDeposit,
    }
}
