﻿namespace CryptoTax.Domain.Enums
{
    public enum TransferStage
    {
        Withdrawal,
        Deposit,
        None
    }
}
