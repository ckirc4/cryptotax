﻿namespace CryptoTax.Domain.Enums
{
    public enum TimeEventType
    {
        ItemWillProcess,
        ItemDidProcess,
        None
    }
}
