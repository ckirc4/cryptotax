﻿namespace CryptoTax.Domain.Enums
{
    public enum TransactionWhitelistStatus
    {
        /// <summary>
        /// Transfer is whitelisted with explanation
        /// </summary>
        Whitelisted_Explained,
        /// <summary>
        /// Transfer is whitelisted, but without explanation
        /// </summary>
        Whitelisted_Unexplained,
        /// <summary>
        /// Transfer is whitelisted, and the attached information should be printed when processed
        /// </summary>
        Whitelisted_Info,
        /// <summary>
        /// Transfer is whitelisted, and the attached warning should be printed when processed
        /// </summary>
        Whitelisted_Warning,
        /// <summary>
        /// Transfer is not allowed at all - will be skipped silently
        /// </summary>
        Soft_Blocked,
        /// <summary>
        /// Transfer is not allowed at all - will throw error
        /// </summary>
        Hard_Blocked,
        /// <summary>
        /// It should not be possible for this transaction to be processed by itself (because it is always part of a transfer object). As we implement more and more exchanges, Whitelisted_Warning will gradually change to Assert_Unreachable.
        /// </summary>
        Assert_Unreachable
    }
}
