﻿namespace CryptoTax.Domain.Enums
{
    public enum TransferSide
    {
        Source,
        Destination
    }
}
