﻿namespace CryptoTax.Domain.Enums
{
    public enum CreateReason
    {
        FuturesFundingIncome,
        FuturesPnlIncome,
        OptionsPnlIncome,
        StakingIncome,
        Fork,
        LoanInterestIncome,
        /// <summary>
        /// BNB due to small assets conversion
        /// </summary>
        DustConversionIncome,
        BankDeposit,
        /// <summary>
        /// No reason. We just want to.
        /// </summary>
        None,
        TransferWithExternalSource,
        DepositWithExternalSource
    }
}
