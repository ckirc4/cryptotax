﻿namespace CryptoTax.Domain.Enums
{
    public enum IdType
    {
        Inventory,
        CurrencyAmount,
        ReportRowData,
        ReportRow,
        Run
    }
}
