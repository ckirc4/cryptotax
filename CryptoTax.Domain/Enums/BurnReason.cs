﻿namespace CryptoTax.Domain.Enums
{
    public enum BurnReason
    {
        /// <summary>
        /// Burn funds as part of a disposal (fees)
        /// </summary>
        Disposal,
        /// <summary>
        /// Burn funds as part of a completed transfer (fees)
        /// </summary>
        Transfer,
        /// <summary>
        /// Burn funds as part of a bank deposit (fees)
        /// </summary>
        BankDepositFee,
        /// <summary>
        /// Burn funds as part of a futures funding payment
        /// </summary>
        FuturesFundingPayment,
        /// <summary>
        /// Burn funds as part of a futures trade
        /// </summary>
        FuturesTradeFee,
        /// <summary>
        /// Burn funds as part of an options trade
        /// </summary>
        OptionsTradeFee,
        /// <summary>
        /// Discard AUD because it has been withdrawn into a bank account
        /// </summary>
        BankWithdrawal,
        /// <summary>
        /// No reason. We just want to.
        /// </summary>
        None,
        /// <summary>
        /// Burn funds because we are transferring them away to an unknown destination [do NOT use this for fees - transfer-related fees should always use <see cref="BurnReason.Transfer"/>]
        /// </summary>
        TransferWithExternalDestination,
        /// <summary>
        /// Burn funds because we are withdrawing them to an unknown destination
        /// </summary>
        WithdrawalWithExternalDestination
    }
}
