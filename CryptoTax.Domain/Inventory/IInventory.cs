﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Inventory
{
    /// <summary>
    /// Represents the buying and selling of currencies to calculate the capital gain. The reason this is an interface is to allow different calculations for base cost, e.g. FIFO, LIFO, average, or other.
    /// Note: The terms "purchase" and "disposal" are used for the tax-related events, whereas "buy" and "sell" are used to identify the trading type of a particular event.
    /// </summary>
    public interface IInventory
    {
        /// <summary>
        /// This should be the file name of the inventory, e.g. FILOInventory.
        /// </summary>
        InventoryType Type { get; }
        DString Name { get; }
        DString Id { get; }
        Exchange Exchange { get; }

        /// <summary>
        /// The net capital gains after all purchases and sales.
        /// </summary>
        CapitalGains NetCapitalGains { get; }

        /// <summary>
        /// Adds the given CurrencyAmount to the inventory.
        /// </summary>
        void Purchase(CurrencyAmount purchasedCurrency); // Since the given currency must be newly created, do not need to provide purchase time.

        /// <summary>
        /// Returns the capital gains (or losses) due to this disposal. If the inventory contains an insufficient quantity, will handle according to the implementation (might throw error) and overdrawnAmount will be positive. Is a tax event.
        /// AdditionalCostBasis can be used to factor in fees (in AUD).
        /// </summary>
        CapitalGainsDelta Disposal(CurrencyAmount disposedCurrency, decimal additionalCostBasis, out decimal overdrawnAmount);

        /// <summary>
        /// Add the given currency to the inventory. Not a tax event.
        /// </summary>
        void TransferIn(CurrencyAmount transferredCurrency, DateTime time); // Since the given currency may have existed before, must also provide the current time.

        /// <summary>
        /// Attempts to remove the specified amount of currency from the inventory. The resulting list includes the zero-valued overdrawn CurrencyAmount, if applicable. Not a tax event.
        /// <br/>
        /// Note: The value of the currencyAmount is ignored.
        /// </summary>
        IEnumerable<CurrencyAmount> TransferOut(CurrencyAmount currencyAmount, TransferOutReason reason, out decimal overdrawnAmount);

        /// <summary>
        /// An unordered collection of the inventory contents, affected by purchases and sells.
        /// </summary>
        Dictionary<DString, IEnumerable<CurrencyAmount>> GetCurrentInventory();

        InventoryDTO ToDTO();
    }

    /// <summary>
    /// Represents the expected constructors of each inventory implementation.
    /// </summary>
    public interface IInventoryFactory
    {
        IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name);
        IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto);
    }

    public static class InventoryFactoryGetter
    {
        public static IInventoryFactory Get(InventoryType type)
        {
            switch (type)
            {
                case InventoryType.FILO:
                    return new FILOInventoryFactory();
                case InventoryType.FIFO:
                    return new FIFOInventoryFactory();
                case InventoryType.Min:
                    return new MinInventoryFactory();
                case InventoryType.Max:
                    return new MaxInventoryFactory();
                case InventoryType.Oldest:
                    return new OldestInventoryFactory();
                case InventoryType.Newest:
                    return new NewestInventoryFactory();
                case InventoryType.Average:
                    return new AverageInventoryFactory();
                case InventoryType.MockImmutable:
                case InventoryType.MockTracking:
                default:
                    throw new AssertUnreachable(type, "Mock inventories do not exist for test-project code");
            }
        }
    }
}
