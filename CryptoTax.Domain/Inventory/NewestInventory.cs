﻿using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CaList = System.Collections.Generic.List<CryptoTax.Domain.CurrencyAmount>;

namespace CryptoTax.Domain.Inventory
{
    public class NewestInventoryFactory : IInventoryFactory
    {
        private const InventoryType _TYPE = InventoryType.Newest;

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new Inventory<CaList>(idProvider, reportingService, loggingService, _TYPE, name, exchange, new NewestInventoryAdapter(idProvider, reportingService));
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new Inventory<CaList>(idProvider, reportingService, loggingService, _TYPE, dto, new NewestInventoryAdapter(idProvider, reportingService));
        }
    }

    public class NewestInventoryAdapter : EnumerableInventoryAdapter<CaList>
    {
        public NewestInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
            : base(idProvider, reportingService)
        { }

        protected override void AddNew(CaList collection, CurrencyAmount newItem, DateTime time)
        {
            collection.Add(newItem);
        }

        protected override CurrencyAmount RemoveNext(CaList collection)
        {
            CurrencyAmount toRemove = collection.OrderByDescending(ca => ca.OriginalTime).First();
            int n = collection.RemoveAll(ca => ca.Id == toRemove.Id); // don't use default equality comparator because matches may not be unique
            if (n != 1) throw new Exception($"NewestInventory was supposed to remove one CurrencyAmount, but instead removed {n} with id {toRemove.Id}");
            return toRemove;
        }

        public override CaList Instantiate()
        {
            return new CaList();
        }

        public override CaList FromDTO(IEnumerable<InventoryItemDTO> items)
        {
            return new CaList(items.Select(i => new CurrencyAmount(this._IdProviderService, i)));
        }
    }
}
