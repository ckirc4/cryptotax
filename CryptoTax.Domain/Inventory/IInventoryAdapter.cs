﻿using CryptoTax.DTO;
using System;
using System.Collections.Generic;

namespace CryptoTax.Domain.Inventory
{
    /// <summary>
    /// Represents an interface for modifying an inventory's collection for an arbitrary currency.<br/>
    /// It only reports modifications of CA, e.g. Merge or Split events.
    /// </summary>
    /// <typeparam name="TCollection">The collection type that holds the parcels of an arbitrary currency.</typeparam>
    public interface IInventoryAdapter<TCollection>
        where TCollection : class // this ensures that `Collection` is a reference type, so we don't need to return the updated object.
    {
        /// <summary>
        /// The total amount of currency stored by the collection.
        /// </summary>
        decimal TotalAmount(TCollection collection);

        /// <summary>
        /// Adds the given <see cref="CurrencyAmount"/> to the collection.
        /// </summary>
        void Add(TCollection collection, CurrencyAmount newAmount, DateTime time);

        /// <summary>
        /// Removes the specified amount from the collection, returning the <see cref="CurrencyAmount"/> parcels that were extracted.
        /// </summary>
        /// <param name="amount">The amount to remove. Never negative.</param>
        /// <param name="overdrawnAmount">The left-over amount that was not present in the collection, if any; the difference between the sum of removed amounts and amount to remove. Never negative.</param>
        IEnumerable<CurrencyAmount> RemoveMany(TCollection collection, decimal amount, DateTime time, out decimal overdrawnAmount);

        /// <summary>
        /// Instantiate a new instance of the collection.
        /// </summary>
        TCollection Instantiate();

        /// <summary>
        /// Instantiates the collection from the given DTO items.
        /// </summary>
        TCollection FromDTO(IEnumerable<InventoryItemDTO> items);

        /// <summary>
        /// Converts the collection to the DTO collection.
        /// </summary>
        IEnumerable<InventoryItemDTO> ToDTO(TCollection collection);

        /// <summary>
        /// Returns the enumerator of the collection's contents.
        /// </summary>
        IEnumerable<CurrencyAmount> GetContents(TCollection collection);
    }
}
