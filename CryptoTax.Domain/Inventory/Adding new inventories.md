1. Add a new `InventoryType` that corresponds to the name of the inventory. Ensure that the new value contains the `ArgEnumValue` so that it can be identified by the `RunService` via process `args`.
2. Create a new file called `{NewName}Inventory.cs` with two members:
  - To make life simpler, define the `TCollection` type, e.g. `using CaStack = System.Collections.Generic.Stack<CryptoTax.Domain.CurrencyAmount>;`
  - Create a `{NewName}InventoryFactory` class that implements the `IInventoryFactory` interface.
    - This will instantiate the `Inventory<TCollection>` class using the adapter (see below).
    - One constructor is used to instantiate a new inventory, the other constructor is used to load an existing inventory.
  - Create a `{NewName}InventoryAdapter` class that implements the `IInventoryAdapter<TCollection>` interface.
    - This is used to modify the state, where the state is the current collection of `CurrencyAmount` parcels held by the inventory. Implementation is completely unrestricted.
    - If the underlying state's data type is of the form `IEnumerable<CurrencAmount>`, consider inheriting from the abstract class `EnumerableInventoryAdapter<TCollection>` to enjoy a variety of default implementations that will work out-of-the-box for most collection types.
3. Add a case to `InventoryFactoryGetter` in `IInventory.cs` that instantiates the new factory.
4. Add a test class `CryptoTax.Tests.Inventory.{NewName}InventoryDataTests` that implements the abstract `InventoryDataTests` class. A template is provided below.

```C#
using CryptoTax.Domain;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UtilityLibrary.Types;
using Tag = CryptoTax.Tests.Inventory.TestDataSetTag;

namespace CryptoTax.Tests.Inventory
{
    [TestClass]
    public class FIFOInventoryDataTests : InventoryDataTests<TCollection>
    {
        private IInventory _Inventory;
        private FIFOInventoryAdapter _Adapter;

        public override IInventory Inventory { get { return this._Inventory; } }
        public override IInventoryAdapter<TCollection> Adapter { get { return this._Adapter; } }
        public override bool IsOrderedInventory { get { return true; } }

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._ReportingService = new MockTrackingReportingService();
            this._LoggingService = new MockLoggingService();
            this._Inventory = new FIFOInventoryFactory().Create(this._IdProviderService, this._ReportingService, this._LoggingService, Exchange, "Test Inventory");
            this._Adapter = new FIFOInventoryAdapter(this._IdProviderService, this._ReportingService);
        }

        public override Dictionary<DString, IEnumerable<CurrencyAmount>> GetExpectedInventory(TestDataSetTag tag)
        {
            // use the static `A`, `B`, `Empty`, `CA()` and `Dict()` fields to minimise code to write.
            switch (tag)
            {
                case Tag.Empty__0a:
                    return Empty;
                // ...
            }
            throw new NotImplementedException();
        }

        public override decimal? GetExpectedCapitalGains(TestDataSetTag tag)
        {
            switch (tag)
            {
                case Tag.Empty__0a:
                    return 0;
                // ...
            }
            throw new NotImplementedException();
        }
    }
}
```
