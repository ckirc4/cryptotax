﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Inventory
{
    /// <summary>
    /// Base inventory logic. Underlying data per currency is stored as a <typeparamref name="TCollection"/> and manipulated by the injected <see cref="IInventoryAdapter{TCollection}"/>.
    /// </summary>
    public class Inventory<TCollection> : IInventory
        where TCollection : class
    {
        private readonly IdProviderService _IdProviderService;
        private readonly IReportingService _ReportingService;
        private readonly IRegisteredLogger _Log;
        private readonly IInventoryAdapter<TCollection> _Adapter;

        private readonly Dictionary<DString, TCollection> _CurrentInventory;

        public InventoryType Type { get; }
        public DString Name { get; }
        public DString Id { get; }
        public Exchange Exchange { get; }
        public CapitalGains NetCapitalGains { get; }

        /// <summary>
        /// Used for instantiating a new inventory.
        /// </summary>
        public Inventory(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryType type, DString name, Exchange exchange, IInventoryAdapter<TCollection> adapter)
        {
            this._IdProviderService = idProvider;
            this._ReportingService = reportingService;
            this._Log = loggingService.Register(this, name);
            this._Adapter = adapter;
            this._CurrentInventory = new Dictionary<DString, TCollection>();

            this.Type = type;
            this.Name = name;
            this.Id = this._IdProviderService.GetNext(IdType.Inventory);
            this.Exchange = exchange;
            this.NetCapitalGains = new CapitalGains();
        }

        /// <summary>
        /// Used for loading an existing inventory.
        /// </summary>
        public Inventory(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryType type, InventoryDTO dto, IInventoryAdapter<TCollection> adapter)
        {
            this._IdProviderService = idProvider;
            this._ReportingService = reportingService;
            this._Log = loggingService.Register(this, dto.Name);
            this._Adapter = adapter;
            this._CurrentInventory = dto.Items.ToDictionary(kvp => new DString(kvp.Key), kvp => this._Adapter.FromDTO(kvp.Value));

            this.Type = type;
            this.Id = dto.Id;
            this.Name = dto.Name;
            this.Exchange = dto.Exchange;
            this.NetCapitalGains = new CapitalGains(dto.CapitalGains);
        }

        public void Purchase(CurrencyAmount purchasedCurrency)
        {
            this.TransferIn(purchasedCurrency, purchasedCurrency.TimeModified);
        }

        public CapitalGainsDelta Disposal(CurrencyAmount disposedCurrency, decimal additionalCostBasis, out decimal overdrawnAmount)
        {
            CurrencyAmount originalDisposedCurrency = disposedCurrency;
            DString currency = disposedCurrency.Currency;
            decimal amount = disposedCurrency.Amount;
            DateTime time = disposedCurrency.TimeModified;
            CapitalGainsDelta delta = CapitalGainsDelta.ZERO;

            IEnumerable<CurrencyAmount> chunkPurchases = this.removeMany(currency, amount, time, out overdrawnAmount);
            foreach (CurrencyAmount chunkPurchase in chunkPurchases)
            {
                decimal thisSellAmount = chunkPurchase.Amount;
                decimal costBasis = chunkPurchase.Value + chunkPurchase.Amount / amount * additionalCostBasis;
                (CurrencyAmount _disposedCurrency, CurrencyAmount chunkSale) = disposedCurrency.ReduceAmount(thisSellAmount, time);
                disposedCurrency = _disposedCurrency;
                delta += this.NetCapitalGains.AddTaxableEvent(costBasis, chunkSale.Value, chunkPurchase.OriginalTime, chunkSale.OriginalTime);

                // we removed chunkPurchase from the inventory
                ReportingRemoveData row = new ReportingRemoveData(this._IdProviderService, time, chunkPurchase, this, originalDisposedCurrency, delta, false);
                this._ReportingService.AddData(row);
            }

            // If there is still left-over amount that hasn't been disposed yet, we have "overdrawn" our inventory (no known purchases are left to dispose), and thus assume a base cost of zero.
            if (overdrawnAmount > 0)
            {
                this._Log.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, this.Id, overdrawnAmount, currency, "disposal");
                decimal costBasis = overdrawnAmount / amount * additionalCostBasis;
                delta += this.NetCapitalGains.AddTaxableEvent(costBasis, disposedCurrency.Value);
                CurrencyAmount phantomCurrencyAmount = disposedCurrency.WithValue(costBasis); // did not actually exist in the inventory - created out of thin air (there might be some fees though)
                ReportingRemoveData row = new ReportingRemoveData(this._IdProviderService, time, phantomCurrencyAmount, this, originalDisposedCurrency, delta, true);
                this._ReportingService.AddData(row);
            }

            return delta;
        }

        public void TransferIn(CurrencyAmount transferredCurrency, DateTime time)
        {
            this._ReportingService.AddData(new ReportingAddData(this._IdProviderService, time, transferredCurrency, this));

            // it is not guaranteed that the transferredCurrency will be added to the collection as-is
            // so we want to make sure reporting occurs first.
            this.add(transferredCurrency, time);
        }

        public IEnumerable<CurrencyAmount> TransferOut(CurrencyAmount currencyAmount, TransferOutReason reason, out decimal overdrawnAmount)
        {
            DString currency = currencyAmount.Currency;
            decimal amount = currencyAmount.Amount;
            DateTime time = currencyAmount.TimeModified;
            bool isTransfer = reason == TransferOutReason.Transfer;

            IEnumerable<CurrencyAmount> result = this.removeMany(currency, amount, time, out overdrawnAmount);
            foreach (CurrencyAmount ca in result)
            {
                ReportingRemoveData row = new ReportingRemoveData(this._IdProviderService, time, ca, this, currencyAmount, false, isTransfer);
                this._ReportingService.AddData(row);
            }

            if (overdrawnAmount > 0)
            {
                this._Log.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, this.Id, overdrawnAmount, currency, $"transfer out ({reason})");
                CurrencyAmount phantomCurrencyAmount = currencyAmount.AsPhantom(overdrawnAmount); // did not actually exist in the inventory - created out of thin air
                result = result.Append(phantomCurrencyAmount);
                ReportingRemoveData row = new ReportingRemoveData(this._IdProviderService, time, phantomCurrencyAmount, this, currencyAmount, true, isTransfer);
                this._ReportingService.AddData(row);
            }

            return result;
        }

        public Dictionary<DString, IEnumerable<CurrencyAmount>> GetCurrentInventory()
        {
            return this._CurrentInventory.ToDictionary(kvp => kvp.Key, kvp => this._Adapter.GetContents(kvp.Value));
        }

        public InventoryDTO ToDTO()
        {
            return new InventoryDTO(
                this.Name,
                this.Type,
                this.Id,
                this.Exchange,
                this._CurrentInventory.ToDictionary(kvp => kvp.Key.Value, kvp => this._Adapter.ToDTO(kvp.Value)),
                this.NetCapitalGains.ToDTO());
        }

        #region private methods
        /// <summary>
        /// Wrapper for adding a parcel to the inventory's collection.
        /// </summary>
        private void add(CurrencyAmount currencyAmount, DateTime time)
        {
            DString currency = currencyAmount.Currency;
            this.addKeyIfRequired(currency);
            this._Adapter.Add(this._CurrentInventory[currency], currencyAmount, time);
        }

        /// <summary>
        /// Wrapper for removing parcels from the inventory's collection.
        /// </summary>
        private IEnumerable<CurrencyAmount> removeMany(DString currency, decimal amount, DateTime time, out decimal overdrawnAmount)
        {
            this.addKeyIfRequired(currency);
            IEnumerable<CurrencyAmount> result = this._Adapter.RemoveMany(this._CurrentInventory[currency], amount, time, out overdrawnAmount);
            this.removeKeyIfRequired(currency);

            return result;
        }

        private void addKeyIfRequired(DString key)
        {
            if (!this._CurrentInventory.ContainsKey(key)) this._CurrentInventory.Add(key, this._Adapter.Instantiate());
        }

        private void removeKeyIfRequired(DString key)
        {
            if (this._CurrentInventory.ContainsKey(key))
            {
                TCollection collection = this._CurrentInventory[key];
                if (this._Adapter.TotalAmount(collection) == 0)
                {
                    this._CurrentInventory.Remove(key);
                }
            }
        }
        #endregion
    }
}
