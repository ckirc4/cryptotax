﻿using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Inventory
{
    public class AverageInventoryFactory : IInventoryFactory
    {
        private const InventoryType _TYPE = InventoryType.Average;

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new Inventory<Ref<CurrencyAmount>>(idProvider, reportingService, loggingService, _TYPE, name, exchange, new AverageInventoryAdapter(idProvider, reportingService));
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new Inventory<Ref<CurrencyAmount>>(idProvider, reportingService, loggingService, _TYPE, dto, new AverageInventoryAdapter(idProvider, reportingService));
        }
    }

    public class AverageInventoryAdapter : IInventoryAdapter<Ref<CurrencyAmount>>
    {
        private readonly IdProviderService _IdProviderService;
        private readonly IReportingService _ReportingService;

        public AverageInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
        {
            this._IdProviderService = idProvider;
            this._ReportingService = reportingService;
        }

        public decimal TotalAmount(Ref<CurrencyAmount> collection)
        {
            if (collection.Has())
            {
                return collection.Get().Amount;
            }
            else
            {
                return 0;
            }
        }

        public void Add(Ref<CurrencyAmount> collection, CurrencyAmount newAmount, DateTime time)
        {
            if (collection.Has())
            {
                CurrencyAmount initial = default, merged = default;
                collection.Update((CurrencyAmount _initial) =>
                {
                    initial = _initial;
                    merged = initial.WithAmount(initial.Amount + newAmount.Amount)
                        .WithValue(initial.Value + newAmount.Value, time);
                    return merged;
                });

                ReportingMergeData data = new ReportingMergeData(this._IdProviderService, time, initial, newAmount, merged);
                this._ReportingService.AddData(data);
            }
            else
            {
                collection.Set(newAmount);
            }
        }

        public IEnumerable<CurrencyAmount> RemoveMany(Ref<CurrencyAmount> collection, decimal amount, DateTime time, out decimal overdrawnAmount)
        {
            if (collection.Has())
            {
                overdrawnAmount = 0;
                decimal total = this.TotalAmount(collection);
                if (amount > total)
                {
                    overdrawnAmount = amount - total;
                    amount = total;
                }

                CurrencyAmount initial = default, leftOver = default, reduced = default;
                collection.Update((CurrencyAmount _initial) =>
                {
                    initial = _initial;
                    (leftOver, reduced) = _initial.ReduceAmount(amount, time);
                    return leftOver;
                });

                ReportingSplitData data = new ReportingSplitData(this._IdProviderService, time, initial, leftOver, reduced);
                this._ReportingService.AddData(data);

                return new List<CurrencyAmount>() { reduced };
            }
            else
            {
                overdrawnAmount = amount;
                return new List<CurrencyAmount>();
            }
        }

        public Ref<CurrencyAmount> Instantiate()
        {
            return new Ref<CurrencyAmount>();
        }

        public Ref<CurrencyAmount> FromDTO(IEnumerable<InventoryItemDTO> items)
        {
            if (!items.Any())
            {
                return this.Instantiate();
            }
            else if (items.Count() > 1)
            {
                throw new ArgumentException("Cannot create an AverageInventory from multiple inventory DTO items.");
            }
            else
            {
                return new Ref<CurrencyAmount>(new CurrencyAmount(this._IdProviderService, items.Single()));
            }
        }

        public IEnumerable<InventoryItemDTO> ToDTO(Ref<CurrencyAmount> collection)
        {
            if (collection.Has())
            {
                return new List<InventoryItemDTO>() { collection.Get().ToDTO() };
            }
            else
            {
                return new List<InventoryItemDTO>();
            }
        }

        public IEnumerable<CurrencyAmount> GetContents(Ref<CurrencyAmount> collection)
        {
            if (collection.Has())
            {
                return new List<CurrencyAmount>() { collection.Get() };
            }
            else
            {
                return new List<CurrencyAmount>();
            }
        }
    }
}
