﻿using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Domain.Inventory
{
    /// <summary>
    /// Common functionality for an inventory whose underlying state implements <see cref="IEnumerable{CurrencyAmount}"/>.
    /// </summary>
    public abstract class EnumerableInventoryAdapter<EnumerableType> : IInventoryAdapter<EnumerableType>
        where EnumerableType : class, IEnumerable<CurrencyAmount>
    {
        protected readonly IdProviderService _IdProviderService;
        private readonly IReportingService _ReportingService;

        public EnumerableInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
        {
            this._IdProviderService = idProvider;
            this._ReportingService = reportingService;
        }

        /// <summary>
        /// Default: Returns 0 if the collection is null or empty, otherwise sums the <see cref="CurrencyAmount.Amount"/>s.
        /// </summary>
        public virtual decimal TotalAmount(EnumerableType collection)
        {
            if (collection == null || !collection.Any())
            {
                return 0;
            }
            else
            {
                return collection.Sum(i => i.Amount);
            }
        }

        public void Add(EnumerableType collection, CurrencyAmount item, DateTime time)
        {
            this.AddNew(collection, item, time);
        }

        /// <summary>
        /// Called when a new item is added. Corresponds to the <see cref="IInventoryAdapter{TCollection}.Add(TCollection, CurrencyAmount)"/> method.
        /// </summary>
        protected abstract void AddNew(EnumerableType collection, CurrencyAmount newItem, DateTime time);

        /// <summary>
        /// Called when an item is re-added to the collection, for example after it has been modified.
        /// <br/>
        /// Default: uses <see cref="AddNew(EnumerableCa, CurrencyAmount)"/> implementation.
        /// </summary>
        protected virtual void AddExisting(EnumerableType collection, CurrencyAmount item, DateTime time)
        {
            this.AddNew(collection, item, time);
        }

        /// <summary>
        /// Return the next item from the collection and remove it. Collection is never empty.
        /// </summary>
        protected abstract CurrencyAmount RemoveNext(EnumerableType collection);

        /// <summary>
        /// Removes the required amount from the inventory, returning the full or partial CurrencyAmounts that were removed.
        /// </summary>
        /// <param name="overdrawn">The left-over amount that could not be dequeued because of insufficient inventory contents.</param>
        public IEnumerable<CurrencyAmount> RemoveMany(EnumerableType collection, decimal amount, DateTime time, out decimal overdrawnAmount)
        {
            overdrawnAmount = amount;
            decimal totalAmount = this.TotalAmount(collection);

            if (totalAmount == 0) return new List<CurrencyAmount>();

            List<CurrencyAmount> result = new List<CurrencyAmount>();
            while (totalAmount > 0 && overdrawnAmount > 0)
            {
                CurrencyAmount next = this.RemoveNext(collection);

                if (next.Amount > overdrawnAmount)
                {
                    // next is reduced
                    (CurrencyAmount leftOver, CurrencyAmount removed) = next.ReduceAmount(overdrawnAmount, time);
                    overdrawnAmount -= removed.Amount;
                    totalAmount -= removed.Amount;

                    if (leftOver.Amount > 0)
                    {
                        this.AddExisting(collection, leftOver, time);
                    }
                    result.Add(removed);

                    ReportingSplitData data = new ReportingSplitData(this._IdProviderService, time, next, leftOver, removed);
                    this._ReportingService.AddData(data);
                }
                else
                {
                    // next is completely removed from collection
                    result.Add(next);
                    overdrawnAmount -= next.Amount;
                    totalAmount -= next.Amount;
                }
            }

            return result;
        }

        public abstract EnumerableType Instantiate();

        public abstract EnumerableType FromDTO(IEnumerable<InventoryItemDTO> items);

        /// <summary>
        /// Default: Select().ToList()
        /// </summary>
        public virtual IEnumerable<InventoryItemDTO> ToDTO(EnumerableType collection)
        {
            return collection.Select(v => v.ToDTO()).ToList();
        }

        /// <summary>
        /// Default: ToList()
        /// </summary>
        public virtual IEnumerable<CurrencyAmount> GetContents(EnumerableType collection)
        {
            return collection.ToList();
        }
    }
}
