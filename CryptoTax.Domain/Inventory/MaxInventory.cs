﻿using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CaList = System.Collections.Generic.List<CryptoTax.Domain.CurrencyAmount>;

namespace CryptoTax.Domain.Inventory
{
    public class MaxInventoryFactory : IInventoryFactory
    {
        private const InventoryType _TYPE = InventoryType.Max;

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new Inventory<CaList>(idProvider, reportingService, loggingService, _TYPE, name, exchange, new MaxInventoryAdapter(idProvider, reportingService));
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new Inventory<CaList>(idProvider, reportingService, loggingService, _TYPE, dto, new MaxInventoryAdapter(idProvider, reportingService));
        }
    }

    public class MaxInventoryAdapter : EnumerableInventoryAdapter<CaList>
    {
        private bool isSorted = false;

        public MaxInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
            : base(idProvider, reportingService)
        { }

        protected override void AddNew(CaList collection, CurrencyAmount newItem, DateTime time)
        {
            collection.Add(newItem);
            this.isSorted = false;
        }

        protected override CurrencyAmount RemoveNext(CaList collection)
        {
            if (!this.isSorted)
            {
                collection.Sort((ca1, ca2) => -Math.Sign(ca1.Value / ca1.Amount - ca2.Value / ca2.Amount));
                this.isSorted = true;
            }
            CurrencyAmount toRemove = collection.First();
            collection.Remove(toRemove);
            return toRemove;
        }

        public override CaList Instantiate()
        {
            return new CaList();
        }

        public override CaList FromDTO(IEnumerable<InventoryItemDTO> items)
        {
            return new CaList(items.Select(i => new CurrencyAmount(this._IdProviderService, i)));
        }
    }
}
