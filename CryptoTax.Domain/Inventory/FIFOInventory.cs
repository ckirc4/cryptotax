﻿using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CaQueue = System.Collections.Generic.Queue<CryptoTax.Domain.CurrencyAmount>;

namespace CryptoTax.Domain.Inventory
{
    public class FIFOInventoryFactory : IInventoryFactory
    {
        private const InventoryType _TYPE = InventoryType.FIFO;

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new Inventory<CaQueue>(idProvider, reportingService, loggingService, _TYPE, name, exchange, new FIFOInventoryAdapter(idProvider, reportingService));
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new Inventory<CaQueue>(idProvider, reportingService, loggingService, _TYPE, dto, new FIFOInventoryAdapter(idProvider, reportingService));
        }
    }

    public class FIFOInventoryAdapter : EnumerableInventoryAdapter<CaQueue>
    {
        public FIFOInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
            : base(idProvider, reportingService)
        { }

        protected override void AddNew(CaQueue collection, CurrencyAmount newItem, DateTime time)
        {
            collection.Enqueue(newItem);
        }

        protected override void AddExisting(CaQueue collection, CurrencyAmount item, DateTime time)
        {
            // we must re-add it to the beginning of the queue. that is not built-in functionality unfortunately.
            // in order to keep the existing collection reference, we cycle through the whole queue. this is fine :)

            int N = collection.Count;
            collection.Enqueue(item);
            for (int i = 0; i < N; i++)
            {
                collection.Enqueue(collection.Dequeue());
            }
            // `item` is now the first item.
        }

        protected override CurrencyAmount RemoveNext(CaQueue collection)
        {
            return collection.Dequeue();
        }

        public override CaQueue Instantiate()
        {
            return new CaQueue();
        }

        public override CaQueue FromDTO(IEnumerable<InventoryItemDTO> items)
        {
            return new CaQueue(items.Select(i => new CurrencyAmount(this._IdProviderService, i)));
        }
    }
}
