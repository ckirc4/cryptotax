﻿using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CaStack = System.Collections.Generic.Stack<CryptoTax.Domain.CurrencyAmount>;

namespace CryptoTax.Domain.Inventory
{
    public class FILOInventoryFactory : IInventoryFactory
    {
        private const InventoryType _TYPE = InventoryType.FILO;

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new Inventory<CaStack>(idProvider, reportingService, loggingService, _TYPE, name, exchange, new FILOInventoryAdapter(idProvider, reportingService));
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new Inventory<CaStack>(idProvider, reportingService, loggingService, _TYPE, dto, new FILOInventoryAdapter(idProvider, reportingService));
        }
    }

    public class FILOInventoryAdapter : EnumerableInventoryAdapter<CaStack>
    {
        public FILOInventoryAdapter(IdProviderService idProvider, IReportingService reportingService)
            : base(idProvider, reportingService)
        { }

        protected override void AddNew(CaStack collection, CurrencyAmount newItem, DateTime time)
        {
            collection.Push(newItem);
        }

        protected override CurrencyAmount RemoveNext(CaStack collection)
        {
            return collection.Pop();
        }

        public override CaStack Instantiate()
        {
            return new CaStack();
        }

        public override CaStack FromDTO(IEnumerable<InventoryItemDTO> items)
        {
            // the items are ordered as if Peek() was called repeatedly.
            // however, they will now be stacked in that order, which is undesirable.
            // so we need to reverse the order.
            // see https://dotnetfiddle.net/Jhk8DJ
            return new CaStack(items.Reverse().Select(i => new CurrencyAmount(this._IdProviderService, i)));
        }

        public override IEnumerable<InventoryItemDTO> ToDTO(CaStack collection)
        {
            // reverses the order, as if Peek() was called repeatedly
            return collection.Select(v => v.ToDTO());
        }

        public override IEnumerable<CurrencyAmount> GetContents(CaStack collection)
        {
            // reverses the order, as if Peek() was called repeatedly
            return collection.ToList();
        }
    }
}
