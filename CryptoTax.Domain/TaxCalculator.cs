﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.DTO;
using CryptoTax.Domain.Services;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Persistence.Services;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Stores;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Shared.Exceptions;

namespace CryptoTax.Domain
{
    /// <summary>
    /// Main Domain entry class.
    /// </summary>
    public class TaxCalculator
    {
        private readonly IdProviderService _IdProviderService;
        private readonly IRegisteredLogger _Logger;
        private readonly ReportingService _ReportingService;
        private readonly DataImporterService _DataImporter;
        private readonly IRunService _RunService;
        private readonly TradeProcessingService _TradeProcessingService;
        private readonly TransactionProcessingService _TransactionProcessingService;
        private readonly IInventoryManagerService _InventoryManagerService;
        private readonly TimeEventService _TimeEventService;
        private readonly AssessableIncomeStore _AssessableIncomeStore;
        private readonly IBulkCandlestickService _BulkCandlestickService;

        private List<TradeDTO> _Trades;
        private List<TransactionDTO> _Transactions;

        public TaxCalculator(DataImporterService dataImporter, ILoggingService logginService, IdProviderService idProviderService, ReportingService reportingService, IRunService runService, TradeProcessingService tradeProcessingService, TransactionProcessingService transactionProcessingService, IInventoryManagerService inventoryManagerService, TimeEventService timeEventService, AssessableIncomeStore assessableIncomeStore, IBulkCandlestickService bulkCandlestickService)
        {
            this._IdProviderService = idProviderService;
            this._Logger = logginService.Register(this, "TaxCalculator");
            this._ReportingService = reportingService;
            this._DataImporter = dataImporter;
            this._RunService = runService;
            this._TradeProcessingService = tradeProcessingService;
            this._TransactionProcessingService = transactionProcessingService;
            this._InventoryManagerService = inventoryManagerService;
            this._TimeEventService = timeEventService;
            this._AssessableIncomeStore = assessableIncomeStore;
            this._BulkCandlestickService = bulkCandlestickService;
        }

        public async Task<CalculationResults> DoTaxCalculation()
        {
            this._ReportingService.AddData(new ReportingStartRunData(this._IdProviderService, DateTime.Now, this._RunService.GetRunId(), this._RunService.GetFinancialYear()));

            var (trades, transactions) = this._DataImporter.ImportAll(this._RunService.GetFinancialYear());
            this._Trades = trades.ToList();
            this._Transactions = transactions.ToList();

            IEnumerable<object> all = this._Trades.ConvertAll(t => (object)t).Concat(this._Transactions.ConvertAll(t => (object)t)).OrderBy(obj =>
                {
                    if (obj is TradeDTO trade) return trade.Time;
                    else if (obj is TransactionDTO tx) return tx.Time;
                    else throw new UnreachableCodeException($"Invalid object type {obj.GetType().Name}");
                });

            this._TransactionProcessingService.Setup(this._Transactions.OrderBy(tx => tx.Time));
            this._TradeProcessingService.Setup(this._Trades.OrderBy(tx => tx.Time));
            this._BulkCandlestickService.FetchAndSaveAll().Wait();

            int index = 0;
            int txIndex = 0;
            int N = all.Count();
            int txCount = this._Transactions.Count;
            DateTime? previousTime = null;
            foreach (object item in all)
            {
                index++;
                DateTime time;
                if (item is TradeDTO trade)
                {
                    time = trade.Time;
                    this._TimeEventService.OnItemWillProcess(previousTime, time);
                    await this._TradeProcessingService.ProcessTradeAsync(trade);
                }
                else if (item is TransactionDTO tx)
                {
                    txIndex++;
                    time = tx.Time;
                    bool isLastTx = txIndex == txCount;
                    this._TimeEventService.OnItemWillProcess(previousTime, time);
                    await this._TransactionProcessingService.ProcessTransactionAsync(tx);
                }
                else throw new UnreachableCodeException($"Invalid object type {item.GetType().Name}");

                bool isLastItem = index == N; // index starts at 1
                if (isLastItem)
                {
                    this._TimeEventService.OnLastItemDidProcess(time);
                }
                previousTime = time;
                this._InventoryManagerService.OnProgress(index, N);
            }

            // without this, all our inventory state changes would have been lost.
            this._InventoryManagerService.UpdateRunState();

            CapitalGains aggregateCapitalGains = this._InventoryManagerService.GetAggregateCapitalGains();
            decimal assessableIncome = this._AssessableIncomeStore.GetTotalAssessableIncome();
            this._ReportingService.AddData(new ReportingEndRunData(this._IdProviderService, DateTime.Now, this._RunService.GetRunId(), this._RunService.GetFinancialYear(), aggregateCapitalGains, assessableIncome));

            this._Logger.Log(LogMessage.MSG_INFO_CAPITAL_GAINS, aggregateCapitalGains.StandardCapitalGains, aggregateCapitalGains.DiscountableCapitalGains, aggregateCapitalGains.CapitalLosses, aggregateCapitalGains.GetTotalGainsOrLosses());

            CalculationResults results = new CalculationResults(aggregateCapitalGains, assessableIncome);
            return results;
        }
    }
}
