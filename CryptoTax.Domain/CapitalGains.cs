﻿using CryptoTax.Domain.Models;
using CryptoTax.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain
{
    public class CapitalGains
    {
        public const decimal DISCOUNT_MULTIPLIER = 0.5m;

        // https://www.ato.gov.au/general/capital-gains-tax/working-out-your-capital-gain-or-loss/working-out-your-capital-gain/

        /// <summary>
        /// The cumulative capital gains that were calculating using the 'other' method (never negatively valued).
        /// </summary>
        [JsonProperty]
        public decimal StandardCapitalGains { get; private set; }

        /// <summary>
        /// The cumulative capital gains that were calculating using the 'discount' method, and which may be eligible for a discount (never negatively valued).
        /// <br/>
        /// The discount has NOT been applied to these gains, since all capital losses for a financial year have to be applied before this can be done.
        /// <br/><br/>
        /// From https://en.wikipedia.org/wiki/Capital_gains_tax_in_Australia#Operation:
        /// <br/>
        /// Capital losses are applied before the capital gains discount:
        /// <br/>
        /// <code>
        /// An individual makes a capital gain of $100 eligible for 50% discount = $50 net capital gain.If the person also had capital losses of $50, the losses would apply first and there would be $50 capital gains left over eligible for 50% discount = $25 net capital gain.
        /// </code>
        /// </summary>
        [JsonProperty]
        public decimal DiscountableCapitalGains { get; private set; }

        /// <summary>
        /// The cumulative capital losses (never positively valued).
        /// </summary>
        [JsonProperty]
        public decimal CapitalLosses { get; private set; }

        public CapitalGains()
        {
            this.StandardCapitalGains = 0;
        }

        public CapitalGains(CapitalGainsDTO dto)
        {
            this.StandardCapitalGains = dto.StandardCapitalGains;
            this.DiscountableCapitalGains = dto.DiscountableCapitalGains;
            this.CapitalLosses = dto.CapitalLosses;
        }

        /// <summary>
        /// Base cost: The cost (in AUD) of buying x amount of currency (including all relevant fees of the whole CGT cycle).
        /// <br/>
        /// Sell cost: The cost (in AUD) of disposing of (usually selling) the same x amount of currency.
        /// <br/>
        /// Returns the change in capital gains or losses due to this taxable event, such that PreviousCapitalGains.Add(Delta) -> NewCapitalGains.
        /// </summary>
        public CapitalGainsDelta AddTaxableEvent(decimal baseCost, decimal sellCost, DateTime timeObtained, DateTime timeDisposed)
        {
            return this.addTaxableEvent(baseCost, sellCost, timeObtained, timeDisposed);
        }

        public CapitalGainsDelta AddTaxableEvent(decimal baseCost, decimal sellCost)
        {
            return this.addTaxableEvent(baseCost, sellCost, null, null);
        }

        private CapitalGainsDelta addTaxableEvent(decimal baseCost, decimal sellCost, DateTime? timeObtained, DateTime? timeDisposed)
        {
            decimal standardGains = 0, discountedGains = 0, losses = 0;

            decimal difference = sellCost - baseCost;
            if (difference < 0)
            {
                losses = difference;
            }
            else if (difference > 0)
            {
                bool eligible = eligbleForDiscount(timeObtained, timeDisposed);
                if (eligible) discountedGains = difference;
                else standardGains = difference;
            }

            CapitalGainsDelta delta = new CapitalGainsDelta(standardGains, discountedGains, losses);
            this.Add(delta);
            return delta;
        }

        /// <summary>
        /// Adds the delta to this CapitalGains instance.
        /// </summary>
        public void Add(CapitalGainsDelta delta)
        {
            this.StandardCapitalGains += delta.StandardCapitalGains;
            this.DiscountableCapitalGains += delta.DiscountableCapitalGains;
            this.CapitalLosses += delta.CapitalLosses;
        }

        public void Add(CapitalGains gains)
        {
            this.Add(new CapitalGainsDelta(gains));
        }

        public CapitalGainsDTO ToDTO()
        {
            return new CapitalGainsDTO(this.StandardCapitalGains, this.DiscountableCapitalGains, this.CapitalLosses);
        }

        public void Reset()
        {
            this.StandardCapitalGains = 0;
            this.DiscountableCapitalGains = 0;
            this.CapitalLosses = 0;
        }

        public decimal GetTotalGainsOrLosses()
        {
            // net result: total capital gains for the year - total capital losses https://www.ato.gov.au/general/capital-gains-tax/working-out-your-capital-gain-or-loss/working-out-your-net-capital-gain-or-loss/
            // we have to apply losses first before we can discount any gains
            decimal netStandardGainOrLoss = this.StandardCapitalGains + this.CapitalLosses;
            decimal eligibleDiscountableGains;
            if (netStandardGainOrLoss >= 0)
            {
                eligibleDiscountableGains = this.DiscountableCapitalGains;
            }
            else
            {
                // must first offest discountable gains by the losses
                netStandardGainOrLoss += this.DiscountableCapitalGains;
                if (netStandardGainOrLoss > 0)
                {
                    // any profit is now subject to the discount
                    eligibleDiscountableGains = netStandardGainOrLoss;
                    netStandardGainOrLoss = 0;
                }
                else
                {
                    eligibleDiscountableGains = 0;
                }
            }

            decimal finalValue = netStandardGainOrLoss + eligibleDiscountableGains * DISCOUNT_MULTIPLIER;
            return finalValue;
        }

        public static decimal GetTotalGainsOrLosses(IEnumerable<CapitalGains> capitalGains)
        {
            CapitalGains summedCG = Sum(capitalGains);
            return summedCG.GetTotalGainsOrLosses();
        }

        public static CapitalGains Sum(IEnumerable<CapitalGains> capitalGains)
        {
            decimal totalStandardGains = capitalGains.Sum(gc => gc.StandardCapitalGains);
            decimal totalDiscountableGains = capitalGains.Sum(gc => gc.DiscountableCapitalGains);
            decimal totalLosses = capitalGains.Sum(gc => gc.CapitalLosses);

            CapitalGains summedCG = new CapitalGains();
            summedCG.Add(new CapitalGainsDelta(totalStandardGains, totalDiscountableGains, totalLosses));
            return summedCG;
        }

        private static bool eligbleForDiscount(DateTime? dateBought, DateTime? dateSold)
        {
            return dateBought.HasValue && dateSold.HasValue
                && dateSold.Value > dateBought.Value.AddYears(1);
        }
    }
}
