﻿namespace CryptoTax.Domain.Models
{
    /// <summary>
    /// A change in CapitalGains.
    /// Note: it doesn't make sense to calculate an "effective total" for a delta because discounted capital gains can only be applied at the end of a financial year, once all data for that year is known.
    /// </summary>
    public struct CapitalGainsDelta
    {
        public static CapitalGainsDelta ZERO = new CapitalGainsDelta(0, 0, 0);

        public decimal StandardCapitalGains { get; }
        public decimal DiscountableCapitalGains { get; }
        public decimal CapitalLosses { get; }

        public CapitalGainsDelta(decimal standardCapitalGains, decimal discountableCapitalGains, decimal capitalLosses)
        {
            this.StandardCapitalGains = standardCapitalGains;
            this.DiscountableCapitalGains = discountableCapitalGains;
            this.CapitalLosses = capitalLosses;
        }

        public CapitalGainsDelta(CapitalGains capitalGains) : this(
            capitalGains.StandardCapitalGains,
            capitalGains.DiscountableCapitalGains,
            capitalGains.CapitalLosses)
        { }

        /// <summary>
        /// Adds the two deltas, returning a new delta.
        /// </summary>
        public CapitalGainsDelta Add(CapitalGainsDelta other)
        {
            return new CapitalGainsDelta(
                this.StandardCapitalGains + other.StandardCapitalGains,
                this.DiscountableCapitalGains + other.DiscountableCapitalGains,
                this.CapitalLosses + other.CapitalLosses);
        }

        public bool IsZero()
        {
            return this.StandardCapitalGains == 0 && this.DiscountableCapitalGains == 0 && this.CapitalLosses == 0;
        }

        public static CapitalGainsDelta operator +(CapitalGainsDelta first, CapitalGainsDelta second)
        {
            return first.Add(second);
        }
    }
}
