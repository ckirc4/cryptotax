﻿using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using System;

namespace CryptoTax.Domain.Models
{
    public struct Candlestick
    {
        /// <summary>
        /// UTC time
        /// </summary>
        public DateTime StartTime { get; set; }
        public CandlestickInterval Interval { get; set; }
        public ExchangeSymbol Symbol { get; set; }
        public decimal Open { get; set; }
        public decimal Close { get; set; }

        public Candlestick(DateTime startTime, CandlestickInterval interval, ExchangeSymbol symbol, decimal open, decimal close)
        {
            DateTime calculatedStartTime = CandlestickHelpers.GetStartTime(interval, startTime);
            if (calculatedStartTime != startTime)
            {
                throw new InvalidStartTimeException(calculatedStartTime, startTime);
            }

            this.StartTime = startTime;
            this.Interval = interval;
            this.Symbol = symbol;
            this.Open = open;
            this.Close = close;
        }

        /// <summary>
        /// Carry over the previous candlestick.
        /// </summary>
        public Candlestick(Candlestick previousCandlestick)
            : this(CandlestickHelpers.GetNextTime(previousCandlestick.Interval, previousCandlestick.StartTime),
                  previousCandlestick.Interval,
                  previousCandlestick.Symbol,
                  previousCandlestick.Close,
                  previousCandlestick.Close)
        { }

        public Candlestick(CandlestickDTO candlestick) : this(
            candlestick.Time,
            candlestick.Interval,
            candlestick.Symbol,
            candlestick.Open,
            candlestick.Close)
        { }

        public DateTime GetNextTime()
        {
            return this.StartTime.AddTicks(CandlestickHelpers.IntervalToTicks(this.Interval));
        }

        public DateTime GetPreviousTime()
        {
            return this.StartTime.AddTicks(-CandlestickHelpers.IntervalToTicks(this.Interval));
        }

        public CandlestickDTO ToDTO()
        {
            return new CandlestickDTO(this.StartTime, this.Interval, this.Symbol, this.Open, this.Close);
        }
    }
}
