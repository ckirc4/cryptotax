﻿using CryptoTax.DTO;

namespace CryptoTax.Domain.Models
{
    public class CalculationResults
    {
        /// <summary>
        /// The aggregate gains and losses that are subject to capital gains tax.
        /// </summary>
        public CapitalGains CapitalGains { get; }

        /// <summary>
        /// The final net capital gains/losses.
        /// </summary>
        public decimal NetCapitalGains { get; private set; }
        /// <summary>
        /// The net ordinary assessable income due to special transactions.
        /// </summary>
        public decimal AssessableIncome { get; }

        public CalculationResults(CapitalGains capitalGains, decimal assessableIncome)
        {
            this.CapitalGains = capitalGains;
            this.NetCapitalGains = capitalGains.GetTotalGainsOrLosses();
            this.AssessableIncome = assessableIncome;
        }

        public CalculationResultsDTO ToDTO()
        {
            return new CalculationResultsDTO(this.CapitalGains.ToDTO(), this.NetCapitalGains, this.AssessableIncome);
        }
    }
}
