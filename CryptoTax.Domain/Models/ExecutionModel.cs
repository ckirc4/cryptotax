﻿using System;

namespace CryptoTax.Domain.Models
{
    /// <summary>
    /// Used by TimeEventService.
    /// </summary>
    public struct ExecutionModel
    {
        public Action Action { get; private set; }
        public DateTime TriggerTime { get; private set; }
        public bool ExecuteOnLastItem { get; private set; }

        public ExecutionModel(Action action, DateTime triggerTime, bool executeOnLastItem)
        {
            this.Action = action;
            this.TriggerTime = triggerTime;
            this.ExecuteOnLastItem = executeOnLastItem;
        }
    }
}
