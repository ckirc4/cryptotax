﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CryptoTax.Domain.Models
{
    public static class Helpers
    {
        public static bool TryDeserialiseObject<T>(this string serialised, out T result)
        {
            // from https://stackoverflow.com/a/51428508
            bool success = true;
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Error = (sender, args) => { success = false; args.ErrorContext.Handled = true; },
                // cause error when there are extra properties in the JSON that are not present on T
                MissingMemberHandling = MissingMemberHandling.Error
            };
            result = JsonConvert.DeserializeObject<T>(serialised, settings);
            return success;
        }

        public static T GetMin<T>(IEnumerable<T> collection, Func<T, decimal> processor)
        {
            decimal minValue = decimal.MaxValue;
            T minItem = default;

            foreach (T item in collection)
            {
                decimal thisValue = processor(item);
                if (thisValue < minValue)
                {
                    minValue = thisValue;
                    minItem = item;
                }
            }

            return minItem;
        }

        public static T GetMax<T>(IEnumerable<T> collection, Func<T, decimal> processor)
        {
            decimal maxValue = decimal.MinValue;
            T maxItem = default;

            foreach (T item in collection)
            {
                decimal thisValue = processor(item);
                if (thisValue > maxValue)
                {
                    maxValue = thisValue;
                    maxItem = item;
                }
            }

            return maxItem;
        }
    }
}
