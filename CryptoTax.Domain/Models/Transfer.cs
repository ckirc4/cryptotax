﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models
{
    /// <summary>
    /// Represents a completed crypto transfer, may be to/from a container that we have no control over, or that should not explicitly be represented by an inventory (e.g. BankAccount).
    /// </summary>
    public class Transfer
    {
        private static readonly TimeSpan _MAX_OVERLAP = new TimeSpan(0, 5, 0);

        public TransactionDTO OriginalSourceTransaction { get; set; }
        public TransactionDTO OriginalDestinationTransaction { get; set; }

        public NString SourceInternalId { get; private set; }
        public NString DestinationInternalId { get; private set; }
        public NString PublicId { get; private set; }
        /// <summary>
        /// The actual amount that was transferred, AFTER the sending fees have been applied. Only the currency and amount are of interest.
        /// <br/><br/>
        /// Effectively, Transferred and FeeToSend are subtracted from the source inventory, then Transferred is added and FeeToReceive is subtracted from the destination inventory.
        /// </summary>
        public CurrencyAmount Transferred { get; private set; }

        public Container Source { get; private set; }
        public DateTime Start { get; private set; }
        /// <summary>
        /// Only the currency and amount are of interest.
        /// </summary>
        public CurrencyAmount? FeeToSend { get; private set; }

        public Container Destination { get; private set; }
        /// <summary>
        /// May be the same as Start.
        /// </summary>
        public DateTime End { get; private set; }
        /// <summary>
        /// Only the currency and amount are of interest.
        /// <br/>
        /// This value is generally not required unless a destination relay is in place, and where sending the relay transfer was not free.
        /// </summary>
        public CurrencyAmount? FeeToReceive { get; private set; }
        /// <summary>
        /// This withdrawal may currently be in transit - what is the next stage that would be processed?
        /// </summary>
        public TransferStage NextStage { get; private set; }

        public Transfer(NString sourceInternalId, NString destinationInternalId, NString publicId, CurrencyAmount transferred, Container source, DateTime start, CurrencyAmount? feeToSend, Container destination, DateTime end, CurrencyAmount? feeToReceive, TransactionDTO originalSourceDto, TransactionDTO originalDestinationDto)
        {
            if (!(sourceInternalId.HasValue && destinationInternalId.HasValue || publicId.HasValue)) throw new TransferIdArgumentException("Must provide at least one type of id (both internal or one public)");

            this.SourceInternalId = sourceInternalId;
            this.DestinationInternalId = destinationInternalId;
            this.PublicId = publicId;
            this.Transferred = transferred;

            this.Source = source;
            this.Start = start;
            this.FeeToSend = feeToSend;

            this.Destination = destination;
            this.End = end;
            this.FeeToReceive = feeToReceive;

            this.NextStage = TransferStage.Withdrawal;

            this.OriginalSourceTransaction = originalSourceDto;
            this.OriginalDestinationTransaction = originalDestinationDto;
        }

        /// <summary>
        /// Gets the time at which the <see cref="NextStage"/> will occur. Retruns null if there are no further events (i.e. the transfer is complete).
        /// </summary>
        public DateTime? GetContinueTime()
        {
            switch (this.NextStage)
            {
                case TransferStage.Withdrawal:
                    return this.Start;
                case TransferStage.Deposit:
                    return this.End;
                case TransferStage.None:
                    return null;
                default:
                    throw new AssertUnreachable(this.NextStage);
            }
        }

        public void AdvanceToNextStage()
        {
            switch (this.NextStage)
            {
                case TransferStage.Withdrawal:
                    this.NextStage = TransferStage.Deposit;
                    return;
                case TransferStage.Deposit:
                    this.NextStage = TransferStage.None;
                    return;
                case TransferStage.None:
                    return;
                default:
                    throw new AssertUnreachable(this.NextStage);
            }
        }

        /// <summary>
        /// Creates the complete valid transfer object from the ordered partial transfers (one on the source side, the other on the destination side).
        /// <br/>
        /// Throws if there are any data inconsistencies or incompatibilities.
        /// </summary>
        public static Transfer FromPartials(PartialTransfer first, PartialTransfer second)
        {
            // Step 1: input validation and consistency checks
            if (!first.IsSourceSide || first.IsDestinationSide) throw new TransferSideArgumentException("First partial transfer must be source side.");
            if (!second.IsDestinationSide || second.IsSourceSide) throw new TransferSideArgumentException("Second partial transfer must be destination side.");

            // we make an exception about public ids when certain exchanges are involved because we simply do not know that information (without doing a huge amount of work).
            bool isBTCMarketsInvolved = involvesExchanges(first, second, Exchange.BTCMarkets);
            bool isBitmexDeposit = involvesExchanges(first, second, Exchange.Binance, Exchange.BitMex) || involvesExchanges(first, second, Exchange.MercatoX, Exchange.BitMex);
            bool includesNullPublicId = !first.PublicId.HasValue || !second.PublicId.HasValue;
            bool isBTCMarketsSpecialCase = isBTCMarketsInvolved && includesNullPublicId;
            bool isBitMexSpecialCase = isBitmexDeposit && includesNullPublicId; // bitmex always has null hash
            bool isBinanceSpecialCase = involvesExchanges(first, second, Exchange.Binance, Exchange.FTX) && includesNullPublicId;
            bool isKrakenSpecialCase = involvesExchanges(first, second, Exchange.Kraken) && includesNullPublicId;
            if (first.PublicId != second.PublicId && !isBTCMarketsSpecialCase && !isBitMexSpecialCase && !isBinanceSpecialCase && !isKrakenSpecialCase && !isEthArbNovaBridge(first, second) && !isArbNovaEthBridge(first, second)) throw new TransferIdArgumentException("Partial transfer public ids do not agree.");
            if (second.FinishTime.Value < first.StartTime.Value.Subtract(_MAX_OVERLAP)) throw new TransferTimeArgumentException("A transfer cannot complete before it has started. (taking into account some overlap)");
            if (first.AmountTransferred.Currency != second.AmountTransferred.Currency) throw new TransferCurrencyArgumentException("The partial transfer currencies do not agree.");

            // Step 2: infer additional fee by the difference in report transferred amounts between the two sides
            // if there was no sending fee, this additional fee will be assigned to that, otherwise it will be assigned to the receiving fee
            decimal firstAmount = first.AmountTransferred.Amount;
            decimal secondAmount = second.AmountTransferred.Amount;
            decimal firstAmountDelta = 0; // kind of a hack to adjust amount transferred
            if (firstAmount < secondAmount) throw new TransferAmountException("Amount of source partial transfer cannot be less than destination partial transfer");
            else if (firstAmount > secondAmount)
            {
                // this implies that there were fees to send or receive - add these.
                decimal additionalFees = firstAmount - secondAmount;

                // fees might not be explicit in the TxDTO
                // the reason we prefer setting the sending fee is because sending fees are much more common, and if the current sending fee is null it implies that the fee was unable to be obtained from the exchange data.
                if (!first.FeeToSend.HasValue)
                {
                    first.AddFee(TransferSide.Source, first.AmountTransferred.WithAmount(additionalFees));
                    firstAmountDelta = -additionalFees; // make sure we take away the fees from the transferred amount
                }
                else
                {
                    second.AddFee(TransferSide.Destination, second.AmountTransferred.WithAmount(additionalFees));
                    // don't take away from first amount here because we essentially bumped up the second amount
                }
            }

            // Step 3: now we must collapse any relays - more fees may be discovered this way
            NString sourceInternalId = first.InternalId;
            NString destinationInternalId = second.InternalId;
            NString publicId = first.PublicId.HasValue ? first.PublicId : second.PublicId;
            Container source = first.Source.Value;
            Container destination = second.Destination.Value;

            // we allow a tiny bit of overlap because the source exchange may have delayed its reporting, but the actual transfer object must have a positive duration
            // otherwise we will come across issues where CurrencyAmounts are not actually being transferred between inventories.
            DateTime startTime = first.StartTime.Value;
            DateTime finishTime = second.FinishTime.Value;
            if (startTime > finishTime) startTime = finishTime;

            if (first.SourceRelay != null)
            {
                PartialTransfer relay = first.SourceRelay;
                source = relay.Source.Value;
                startTime = relay.StartTime.Value;
                if (relay.InternalId.HasValue) sourceInternalId = relay.InternalId;

                decimal difference = relay.AmountTransferred.Amount - first.AmountTransferred.Amount;
                if (difference < 0) throw new TransferAmountException("Amount of source relay cannot be less than the source partial transfer amount");
                else if (difference > 0)
                {
                    // implies further withdrawal fees. but don't modify amount transferred, since it is supposed to be the "public" transferred amount (AFTER the sending fees have been applied)
                    relay.AddFee(TransferSide.Source, first.AmountTransferred.WithAmount(difference));
                }

                if (relay.FeeToSend.HasValue) first.AddFee(TransferSide.Source, relay.FeeToSend.Value);
            }

            if (second.DestinationRelay != null)
            {
                PartialTransfer relay = second.DestinationRelay;
                destination = relay.Destination.Value;
                finishTime = relay.FinishTime.Value;
                if (relay.InternalId.HasValue) destinationInternalId = relay.InternalId;

                // important: use second.AmountTransferred, even though it may already be reduced from the "real" transferred amount, since we could have receiving fees at the partial transfer AND relay destinations.
                decimal difference = second.AmountTransferred.Amount - relay.AmountTransferred.Amount;
                if (difference < 0) throw new TransferAmountException("Amount transferred to original destination container cannot be less than the one transferred to the destination relay");
                if (difference < 0) throw new TransferAmountException("Amount transferred to original destination container cannot be less than the one transferred to the destination relay");
                else if (difference > 0)
                {
                    // implies further receiving fees
                    relay.AddFee(TransferSide.Destination, second.AmountTransferred.WithAmount(difference));
                }

                if (relay.FeeToReceive.HasValue) second.AddFee(TransferSide.Destination, relay.FeeToReceive.Value);
            }

            CurrencyAmount amountTransferred = first.AmountTransferred.WithAmount(first.AmountTransferred.Amount + firstAmountDelta);
            CurrencyAmount? feeToSend = first.FeeToSend;
            CurrencyAmount? feeToReceive = second.FeeToReceive;

            return new Transfer(sourceInternalId, destinationInternalId, publicId, amountTransferred, source, startTime, feeToSend, destination, finishTime, feeToReceive, first.OriginalDTO, second.OriginalDTO);
        }

        private static bool involvesExchanges(PartialTransfer first, PartialTransfer second, params Exchange[] exchanges)
        {
            bool prevResult = exchanges.Length > 1 ? involvesExchanges(first, second, exchanges.Skip(1).ToArray()) : true;
            Exchange exchange = exchanges.First();
            bool currentResult = first.Source?.Exchange == exchange || first.Destination?.Exchange == exchange
                || second.Source?.Exchange == exchange || second.Destination?.Exchange == exchange;
            return prevResult && currentResult;
        }

        private static bool isEthArbNovaBridge(PartialTransfer first, PartialTransfer second)
        {
            if (first.OriginalDTO == null || second.OriginalDTO == null || first.OriginalDTO.Type != TransactionType.OnChainTransfer || second.OriginalDTO.Type != TransactionType.OnChainTransfer)
            {
                return false;
            }

            return first.OriginalDTO.Counterparty == KnownWallets.ETH_MAIN_ARB_NOVA_BRIDGE_CONTRACT.Address
                && second.OriginalDTO.Counterparty == KnownWallets.ETH_ARB_NOVA_MAIN_BRIDGE_CONTRACT.Address;
        }

        private static bool isArbNovaEthBridge(PartialTransfer first, PartialTransfer second)
        {
            if (first.OriginalDTO == null || second.OriginalDTO == null || first.OriginalDTO.Type != TransactionType.OnChainTransfer || second.OriginalDTO.Type != TransactionType.OnChainTransfer)
            {
                return false;
            }

            return first.OriginalDTO.Counterparty == KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_ARB_NOVA.Address
                && second.OriginalDTO.Counterparty == KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_MAINNET.Address;
        }
    }
}
