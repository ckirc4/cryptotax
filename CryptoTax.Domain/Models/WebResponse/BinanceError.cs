﻿using Newtonsoft.Json;

namespace CryptoTax.Domain.Models.WebResponse
{
    public class BinanceError
    {
        // the required attributes ensure that deserialisation fails if the member is not contained in the JSON
        [JsonProperty(Required = Required.Always)]
        public int Code { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string Msg { get; set; }

        public BinanceError(int code, string msg)
        {
            this.Code = code;
            this.Msg = msg;
        }

        public override string ToString()
        {
            return $"Error code {this.Code}: {this.Msg}";
        }
    }
}
