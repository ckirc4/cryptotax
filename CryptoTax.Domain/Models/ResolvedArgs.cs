﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models
{
    public class ResolvedArgs
    {
        /// <summary>
        /// The RunInfo Id that is being referenced (does not check if valid).
        /// </summary>
        public NString Ref { get; private set; }

        /// <summary>
        /// The RunInfo Id whose output state should be used (does not check if valid).
        /// </summary>
        public NString State { get; private set; }

        /// <summary>
        /// The RunInfo Id from which to continue. Can be defined only if no other args are provided.
        /// </summary>
        public NString Cont { get; private set; }

        /// <summary>
        /// Key-value pairs of RunInfo properties to override (does not check if valid).
        /// </summary>
        public Dictionary<DString, NString> CustomProperties { get; private set; }

        public ResolvedArgs(NString refId, NString stateId, NString contId, Dictionary<DString, NString> customProperties)
        {
            if (contId.HasValue)
            {
                if (refId.HasValue || stateId.HasValue || (customProperties != null && customProperties.Any()))
                {
                    throw new ArgumentException("cont was defined in args, but other args were provided as well which is not allowed.");
                }

                this.Cont = IdProviderService.FixFormat(contId.Value, IdType.Run);
                this.Ref = null;
                this.State = null;
                this.CustomProperties = new Dictionary<DString, NString>();
                return;
            }

            if (refId.HasValue) this.Ref = IdProviderService.FixFormat(refId.Value, IdType.Run);
            else this.Ref = null;
            if (stateId.HasValue) this.State = IdProviderService.FixFormat(stateId.Value, IdType.Run);
            else this.State = null;

            // todo UTIL-15: ternary operator does not work when NString is null because FixFormat() is called for some reason. Investigate
            // this.Ref = refId.HasValue ? IdProviderService.FixFormat(refId.Value, IdType.Run) : null;
            // this.State = state.HasValue ? IdProviderService.FixFormat(state.Value, IdType.Run) : null;
            this.CustomProperties = customProperties;
        }
    }
}
