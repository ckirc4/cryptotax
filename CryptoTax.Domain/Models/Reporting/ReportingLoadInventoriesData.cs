﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingLoadInventoriesData : ReportingBaseData
    {
        public DString LoadedFromRunId { get; }
        public IEnumerable<IInventory> Inventories { get; }

        public ReportingLoadInventoriesData(IdProviderService idProvider, DateTime time, DString loadedFromRunId, IEnumerable<IInventory> inventories)
            : base(idProvider, time, ReportRowType.LoadInventories)
        {
            this.LoadedFromRunId = loadedFromRunId;
            this.Inventories = inventories;
        }
    }
}
