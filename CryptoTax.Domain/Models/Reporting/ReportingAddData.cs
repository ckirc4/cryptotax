﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingAddData : ReportingBaseData
    {
        public CurrencyAmount CurrencyAmount { get; }
        public IInventory Inventory { get; }
        /// <summary>
        /// If this addition is part of a transfer, what is the original whole CurrencyAmount that we are transferring, and of which this addition is a part of
        /// </summary>
        public CurrencyAmount? OriginalAmount { get; }

        /// <summary>
        /// Used for purchases/creations.
        /// </summary>
        public ReportingAddData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory inventory)
            : base(idProvider, time, ReportRowType.Add)
        {
            this.CurrencyAmount = currencyAmount;
            this.Inventory = inventory;
            this.OriginalAmount = null;
        }

        /// <summary>
        /// Used for transfers. transferredAmount should be the original (zero-valued) amount that was transferred, while currencyAmount is the individual parcel we are depositing which must be part of the transferred amount.
        /// </summary>
        public ReportingAddData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory inventory, CurrencyAmount transferredAmount)
            : base(idProvider, time, ReportRowType.Add)
        {
            this.CurrencyAmount = currencyAmount;
            this.Inventory = inventory;
            this.OriginalAmount = transferredAmount;
        }
    }
}
