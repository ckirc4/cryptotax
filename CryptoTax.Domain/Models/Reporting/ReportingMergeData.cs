﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingMergeData : ReportingBaseData
    {
        public CurrencyAmount From1 { get; }
        public CurrencyAmount From2 { get; }
        public CurrencyAmount To { get; }

        public ReportingMergeData(IdProviderService idProvider, DateTime time, CurrencyAmount from1, CurrencyAmount from2, CurrencyAmount to)
            : base(idProvider, time, ReportRowType.Merge)
        {
            this.From1 = from1;
            this.From2 = from2;
            this.To = to;
        }
    }
}
