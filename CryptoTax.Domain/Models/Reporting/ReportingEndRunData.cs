﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingEndRunData : ReportingBaseData
    {
        public DString RunId { get; }
        public FinancialYear FinancialYear { get; }
        public CapitalGains CapitalGains { get; }
        public decimal AssessableIncome { get; }

        public ReportingEndRunData(IdProviderService idProvider, DateTime time, DString runId, FinancialYear financialYear, CapitalGains capitalGains, decimal assessableIncome)
    : base(idProvider, time, ReportRowType.EndRun)
        {
            this.RunId = runId;
            this.FinancialYear = financialYear;
            this.CapitalGains = capitalGains;
            this.AssessableIncome = assessableIncome;
        }
    }
}
