﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingDisposeData : ReportingBaseData
    {
        /// <summary>
        /// The currency amount that was disposed
        /// </summary>
        public CurrencyAmount DisposedAmount { get; }

        public DisposalType DisposalType { get; }

        /// <summary>
        /// The total fees that were paid during the disposal, if any.
        /// </summary>
        public CurrencyAmount? FeeAmount { get; }

        public ReportingDisposeData(IdProviderService idProvider, DateTime time, CurrencyAmount disposedAmount, DisposalType disposalType, CurrencyAmount? feeAmount)
            : base(idProvider, time, ReportRowType.Dispose)
        {
            this.DisposedAmount = disposedAmount;
            this.DisposalType = disposalType;
            this.FeeAmount = feeAmount;
        }
    }
}
