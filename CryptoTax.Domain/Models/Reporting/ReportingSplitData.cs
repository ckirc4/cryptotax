﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingSplitData : ReportingBaseData
    {
        public CurrencyAmount From { get; }
        public CurrencyAmount To1 { get; }
        public CurrencyAmount To2 { get; }

        public ReportingSplitData(IdProviderService idProvider, DateTime time, CurrencyAmount from, CurrencyAmount to1, CurrencyAmount to2)
            : base(idProvider, time, ReportRowType.Split)
        {
            this.From = from;
            this.To1 = to1;
            this.To2 = to2;
        }
    }
}
