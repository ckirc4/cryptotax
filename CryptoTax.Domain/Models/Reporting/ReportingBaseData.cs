﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models.Reporting
{
    public abstract class ReportingBaseData
    {
        public DString Id { get; private set; }
        public DateTime Time { get; private set; }
        public ReportRowType Type { get; private set; }

        /// <summary>
        /// All constructors should call this base constructor. It is preferred that row data constructors should be used for as unique a situation as possible, rather than one "master" constructor with optional/default values, as it eases the process of maintaining and testing.
        /// </summary>
        public ReportingBaseData(IdProviderService idProvider, DateTime time, ReportRowType type)
        {
            this.Id = idProvider.GetNext(IdType.ReportRowData);
            this.Time = time;
            this.Type = type;
        }

        /// <summary>
        /// May be null.
        /// </summary>
        public virtual CapitalGainsDelta? GetCapitalGainsDelta()
        {
            return null;
        }

        /// <summary>
        /// May be null.
        /// </summary>
        public virtual decimal? GetAssessableIncomeDelta()
        {
            return null;
        }

        public dynamic Transform()
        {
            switch (this.Type)
            {
                case ReportRowType.Split:
                    return this as ReportingSplitData;
                case ReportRowType.Merge:
                    return this as ReportingMergeData;
                case ReportRowType.Add:
                    return this as ReportingAddData;
                case ReportRowType.Remove:
                    return this as ReportingRemoveData;
                case ReportRowType.Purchase:
                    return this as ReportingPurchaseData;
                case ReportRowType.Dispose:
                    return this as ReportingDisposeData;
                case ReportRowType.Create:
                    return this as ReportingCreateData;
                case ReportRowType.Burn:
                    return this as ReportingBurnData;
                case ReportRowType.Transfer:
                    return this as ReportingTransferData;
                case ReportRowType.Convert:
                    return this as ReportingConversionData;
                case ReportRowType.Price:
                    return this as ReportingPriceData;
                case ReportRowType.StartRun:
                    return this as ReportingStartRunData;
                case ReportRowType.EndRun:
                    return this as ReportingEndRunData;
                case ReportRowType.LoadInventories:
                    return this as ReportingLoadInventoriesData;
                default:
                    throw new AssertUnreachable(this.Type, $"ReportRowType Transform() method is not implemented.");
            }
        }
    }
}
