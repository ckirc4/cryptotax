﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingStartRunData : ReportingBaseData
    {
        public DString RunId { get; }
        public FinancialYear FinancialYear { get; }

        public ReportingStartRunData(IdProviderService idProvider, DateTime time, DString runId, FinancialYear financialYear)
    : base(idProvider, time, ReportRowType.StartRun)
        {
            this.RunId = runId;
            this.FinancialYear = financialYear;
        }
    }
}
