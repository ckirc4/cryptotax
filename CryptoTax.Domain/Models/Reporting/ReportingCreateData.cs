﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingCreateData : ReportingBaseData
    {
        public CurrencyAmount CreatedAmount { get; }
        public CreateReason Reason { get; }
        /// <summary>
        /// For futures funding, this is the position that earned funding oncome (if known).
        /// <br/>
        /// For a stake, a fork, or loan interest income, it is the currency amount that was staked, forked, or lent.
        /// </summary>
        public CurrencyAmount? RelevantCA { get; }
        public decimal? AssessableIncomeDelta { get; }

        /// <summary>
        /// Use this for bank account deposits.
        /// </summary>
        public ReportingCreateData(IdProviderService idProvider, DateTime time, CurrencyAmount createdAmount, CreateReason reason)
            : base(idProvider, time, ReportRowType.Create)
        {
            this.CreatedAmount = createdAmount;
            this.Reason = reason;
            this.RelevantCA = null;
            this.AssessableIncomeDelta = null;
        }

        /// <summary>
        /// Use this for anything that is not a bank account deposit.
        /// </summary>
        public ReportingCreateData(IdProviderService idProvider, DateTime time, CurrencyAmount createdAmount, CreateReason reason, CurrencyAmount relevantCA)
            : base(idProvider, time, ReportRowType.Create)
        {
            this.CreatedAmount = createdAmount;
            this.Reason = reason;
            this.RelevantCA = relevantCA;
            this.AssessableIncomeDelta = null;
        }

        /// <summary>
        /// Use this for transactions that count towards assessable income. Note that ideally the attached CA should be provided so that the report can reference the actual source/cause of the assessable income.
        /// </summary>
        public ReportingCreateData(IdProviderService idProvider, DateTime time, CurrencyAmount createdAmount, CreateReason reason, CurrencyAmount? relevantCA, decimal assessableIncome)
            : base(idProvider, time, ReportRowType.Create)
        {
            this.CreatedAmount = createdAmount;
            this.Reason = reason;
            this.RelevantCA = relevantCA;
            this.AssessableIncomeDelta = assessableIncome;
        }

        public override decimal? GetAssessableIncomeDelta()
        {
            return this.AssessableIncomeDelta;
        }
    }
}
