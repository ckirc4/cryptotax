﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingPurchaseData : ReportingBaseData
    {
        public CurrencyAmount PurchasedAmount { get; }

        public ReportingPurchaseData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount)
            : base(idProvider, time, ReportRowType.Purchase)
        {
            this.PurchasedAmount = currencyAmount;
        }
    }
}
