﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingBurnData : ReportingBaseData
    {
        /// <summary>
        /// The currency amount that was burned (with value)
        /// </summary>
        public CurrencyAmount BurnedAmount { get; }

        /// <summary>
        /// The disposal/transfer amount that this burn is attached to, if any.
        /// </summary>
        public BurnReason Reason { get; }

        /// <summary>
        /// May be specified for disposals/transfers/bank deposits when there are fees.
        /// </summary>
        public CurrencyAmount? RelevantCA { get; }

        /// <summary>
        /// For use when burning a standalone currency amount.
        /// </summary>
        public ReportingBurnData(IdProviderService idProvider, DateTime time, CurrencyAmount burnedAmount, BurnReason reason)
            : base(idProvider, time, ReportRowType.Burn)
        {
            this.BurnedAmount = burnedAmount;
            this.Reason = reason;
            this.RelevantCA = null;
        }

        /// <summary>
        /// For use when burning a currency amount as part of a disposal/transfer/bank deposit (e.g. fees).
        /// </summary>
        public ReportingBurnData(IdProviderService idProvider, DateTime time, CurrencyAmount burnedAmount, BurnReason reason, CurrencyAmount relevantCA)
            : base(idProvider, time, ReportRowType.Burn)
        {
            this.BurnedAmount = burnedAmount;
            this.Reason = reason;
            this.RelevantCA = relevantCA;
        }
    }
}
