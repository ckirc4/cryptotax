﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingTransferData : ReportingBaseData
    {
        public CurrencyAmount CurrencyAmount { get; }
        public IInventory From { get; }
        public IInventory To { get; }
        public NString TransactionId { get; }
        /// <summary>
        /// Fee to send, taken from the 'From' inventory
        /// </summary>
        public CurrencyAmount? WithdrawalFees { get; set; }
        /// <summary>
        /// Fee to receive, taken from the 'To' inventory
        /// </summary>
        public CurrencyAmount? DepositFees { get; set; }
        public TimeSpan? Duration { get; }

        /// <summary>
        /// Used for instant (often internal to an exchange) transfers.
        /// </summary>
        public ReportingTransferData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory from, IInventory to, NString transactionId, CurrencyAmount? withdrawalFees, CurrencyAmount? depositFees)
            : base(idProvider, time, ReportRowType.Transfer)
        {
            this.CurrencyAmount = currencyAmount;
            this.From = from;
            this.To = to;
            this.TransactionId = transactionId;
            this.WithdrawalFees = withdrawalFees;
            this.DepositFees = depositFees;
            this.Duration = null;
        }

        /// <summary>
        /// Used for non-instant (e.g. on-chain) transfers.
        /// </summary>
        public ReportingTransferData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory from, IInventory to, NString transactionId, CurrencyAmount? withdrawalFees, CurrencyAmount? depositFees, TimeSpan duration)
            : base(idProvider, time, ReportRowType.Transfer)
        {
            this.CurrencyAmount = currencyAmount;
            this.From = from;
            this.To = to;
            this.TransactionId = transactionId;
            this.WithdrawalFees = withdrawalFees;
            this.DepositFees = depositFees;
            this.Duration = duration;
        }
    }
}
