﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingPriceData : ReportingBaseData
    {
        /// <summary>
        /// The chain of prices, starting with the farthest from an -AUD pair.
        /// </summary>
        public (ExchangeSymbol symbol, decimal price)[] Chain { get; set; }

        public ReportingPriceData(IdProviderService idProvider, DateTime time, (ExchangeSymbol symbol, decimal price)[] chain)
            : base(idProvider, time, ReportRowType.Price)
        {
            this.Chain = chain;
        }
    }
}
