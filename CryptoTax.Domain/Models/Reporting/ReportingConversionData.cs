﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingConversionData : ReportingBaseData
    {
        public IInventory Inventory { get; }
        public ConvertReason Reason { get; }
        public CurrencyAmount InjectedAmount { get; }
        public CurrencyAmount DiscardedAmount { get; }

        public ReportingConversionData(IdProviderService idProvider, DateTime time, IInventory inventory, ConvertReason reason, CurrencyAmount injectedAmount, CurrencyAmount discardedAmount)
            : base(idProvider, time, ReportRowType.Convert)
        {
            this.Inventory = inventory;
            this.Reason = reason;
            this.InjectedAmount = injectedAmount;
            this.DiscardedAmount = discardedAmount;
        }
    }
}
