﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Domain.Models.Reporting
{
    public class ReportingRemoveData : ReportingBaseData
    {
        public CurrencyAmount RemovedAmount { get; }
        public IInventory Inventory { get; }

        /// <summary>
        /// If this removal is part of a disposal, transfer, or burn, what is the original whole CurrencyAmount that we are disposing, transferring, or burning, and of which this removal is a part of
        /// </summary>
        public CurrencyAmount? OriginalAmount { get; }
        /// <summary>
        /// Only defined if this Remove event is part of a disposal.
        /// </summary>
        public CapitalGainsDelta? CapitalGainsDelta { get; }
        /// <summary>
        /// If true, RemovedAmount did not actually exist in the inventory - that is, it was created out of thin air and then immediately removed.
        /// </summary>
        public bool IsPhantomRemoval { get; }
        /// <summary>
        /// Whether this removal is part of a transfer.
        /// </summary>
        public bool IsTransfer { get; set; }

        private ReportingRemoveData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory inventory)
            : base(idProvider, time, ReportRowType.Remove)
        {
            this.RemovedAmount = currencyAmount;
            this.Inventory = inventory;
            this.OriginalAmount = null;
            this.CapitalGainsDelta = null;
            this.IsPhantomRemoval = false;
            this.IsTransfer = false;
        }

        /// <summary>
        /// Used for disposing. Set isPhantomRemoval to true if the currencyAmount does not exist (e.g. when overdrawing from an inventory).
        /// </summary>
        public ReportingRemoveData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory inventory, CurrencyAmount originalAmount, CapitalGainsDelta capitalGainsDelta, bool isPhantomRemoval)
            : this(idProvider, time, currencyAmount, inventory)
        {
            this.OriginalAmount = originalAmount;
            this.CapitalGainsDelta = capitalGainsDelta;
            this.IsPhantomRemoval = isPhantomRemoval;
            this.IsTransfer = false;
        }

        /// <summary>
        /// Used for burning or transferring. Set isPhantomRemoval to true if the currencyAmount does not exist (e.g. when overdrawing from an inventory).
        /// </summary>
        public ReportingRemoveData(IdProviderService idProvider, DateTime time, CurrencyAmount currencyAmount, IInventory inventory, CurrencyAmount originalAmount, bool isPhantomRemoval, bool isTransfer)
            : this(idProvider, time, currencyAmount, inventory)
        {
            this.OriginalAmount = originalAmount;
            this.IsPhantomRemoval = isPhantomRemoval;
            this.IsTransfer = isTransfer;
        }

        public override CapitalGainsDelta? GetCapitalGainsDelta()
        {
            return this.CapitalGainsDelta;
        }
    }
}
