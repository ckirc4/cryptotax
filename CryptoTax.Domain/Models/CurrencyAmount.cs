﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain
{
    public struct CurrencyAmount
    {
        private readonly IdProviderService _IdProviderService;
        /// <summary>
        /// Internal id.
        /// </summary>
        public DString Id { get; }
        /// <summary>
        /// The id of the parent from which this CurrencyAmount originated from, if any.
        /// </summary>
        public NString OriginalId { get; }
        /// <summary>
        /// The time at which the CurrencyAmount ancestry chain begun.
        /// </summary>
        public DateTime OriginalTime { get; }
        /// <summary>
        /// The symbol of the currency.
        /// </summary>
        public DString Currency { get; }
        /// <summary>
        /// The amount of currency (position size for futures contract). Never negative
        /// </summary>
        public decimal Amount { get; }
        /// <summary>
        /// The value, in AUD, spent to purchase this amount.
        /// </summary>
        public decimal Value { get; }
        /// <summary>
        /// The time at which this CurrencyAmount child was created.
        /// </summary>
        public DateTime TimeModified { get; }

        // Optional properties
        public Exchange? ExchangeAcquired { get; }

        public CurrencyAmount(IdProviderService idProvider, InventoryItemDTO item)
        {
            this._IdProviderService = idProvider;
            this.Id = item.Id;
            this.OriginalId = item.OriginalId;
            this.OriginalTime = item.OriginalTime;
            this.Currency = item.Currency;
            this.Amount = item.Amount;
            this.Value = item.Value;
            this.TimeModified = item.TimeModified;
            this.ExchangeAcquired = item.ExchangeAcquired;
        }

        public CurrencyAmount(IdProviderService idProvider, DString currency, decimal amount, decimal value, DateTime time)
            : this(idProvider, currency, amount, value, time, null, null)
        { }

        public CurrencyAmount(IdProviderService idProvider, DString currency, decimal amount, decimal value, DateTime time, Exchange? exchange)
            : this(idProvider, currency, amount, value, time, exchange, null)
        { }

        private CurrencyAmount(IdProviderService idProvider, DString currency, decimal amount, decimal value, DateTime time, Exchange? exchange, CurrencyAmount? parent)
        {
            if (amount < 0 || value < 0) throw new ArgumentOutOfRangeException("Amount or value in a CurrencyAmount object must not be negative.");

            this._IdProviderService = idProvider;
            this.Id = this._IdProviderService.GetNext(IdType.CurrencyAmount);
            this.OriginalId = new NString(parent?.Id);
            this.OriginalTime = parent?.OriginalTime ?? time;

            this.Currency = currency;
            this.Amount = amount;
            this.Value = value;
            this.TimeModified = time;

            this.ExchangeAcquired = exchange;
        }

        /// <summary>
        /// Reduce the amount while keeping the value proportionally the same.
        /// Returns the CurrencyAmount that was effectively taken away from this, and modifies this.
        /// Throws an exception if overdrawn.<br/>
        /// First output: The existing CA after the specified amount has been subtracted.<br/>
        /// Second output: The subtracted amount.
        /// </summary>
        /// <exception cref="Exception">Thrown if amount is reduced beyond zero.</exception>
        public (CurrencyAmount leftOver, CurrencyAmount reduced) ReduceAmount(decimal byAmount, DateTime reducedAtTime, Exchange? reducedAtExchange = null)
        {
            decimal proportionReduced = byAmount / this.Amount;
            decimal originalValue = this.Value;
            decimal leftOverAmount = this.Amount - byAmount;
            decimal leftOverValue = originalValue * (1 - proportionReduced);
            decimal reducedValue = originalValue * proportionReduced;

            if (leftOverAmount < 0) throw new CurrencyAmountOverdrawException(this, byAmount);

            CurrencyAmount leftOver = new CurrencyAmount(this._IdProviderService, this.Currency, leftOverAmount, leftOverValue, reducedAtTime, this.ExchangeAcquired, this);
            CurrencyAmount reduced = new CurrencyAmount(this._IdProviderService, this.Currency, byAmount, reducedValue, reducedAtTime, reducedAtExchange ?? this.ExchangeAcquired, this);

            return (leftOver, reduced);
        }

        /// <summary>
        /// Changes the value of this CurrencyAmount, optionally updating its last-modified time.
        /// </summary>
        public CurrencyAmount WithValue(decimal value, DateTime? updateTime = null)
        {
            return new CurrencyAmount(this._IdProviderService, this.Currency, this.Amount, value, updateTime ?? this.TimeModified, this.ExchangeAcquired, this);
        }

        /// <summary>
        /// Changes the amount of this CurrencyAmount, optionally updating its last-modified time and optionally scaling its value.
        /// </summary>
        public CurrencyAmount WithAmount(decimal amount, DateTime? updateTime = null, bool scaleValue = false)
        {
            decimal newValue;
            if (scaleValue && this.Amount != 0)
            {
                newValue = this.Value * amount / this.Amount;
            }
            else if (scaleValue)
            {
                newValue = 0;
            }
            else
            {
                newValue = this.Value;
            }

            return new CurrencyAmount(this._IdProviderService, this.Currency, amount, newValue, updateTime ?? this.TimeModified, this.ExchangeAcquired, this);
        }

        public CurrencyAmount WithCurrency(DString newCurrency, Exchange? updateExchange = null, DateTime? updateTime = null)
        {
            return new CurrencyAmount(this._IdProviderService, newCurrency, this.Amount, this.Value, updateTime ?? this.TimeModified, updateExchange ?? this.ExchangeAcquired, this);
        }

        public CurrencyAmount AsPhantom(decimal? withAmount = null, DateTime? updateTime = null)
        {
            return new CurrencyAmount(this._IdProviderService, this.Currency, withAmount ?? this.Amount, 0, updateTime ?? this.TimeModified, null, null);
        }

        public InventoryItemDTO ToDTO()
        {
            return new InventoryItemDTO(
                this.Id,
                this.OriginalId,
                this.OriginalTime,
                this.Currency,
                this.Amount,
                this.Value,
                this.TimeModified,
                this.ExchangeAcquired);
        }

        /// <summary>
        /// Purposefully ignores ids or original items because it wouldn't work well for testing. If required, must explicitly compare Id, OriginalId, and OriginalTime.
        /// </summary>
        public static bool operator ==(CurrencyAmount first, CurrencyAmount second)
        {
            return EqualWithinTol(first, second, 0);
        }

        public static bool EqualWithinTol(CurrencyAmount first, CurrencyAmount second, decimal tol)
        {
            return first.Currency.Value.ToLower().Trim() == second.Currency.Value.ToLower().Trim()
            && Math.Abs(first.Amount - second.Amount) <= tol
            && Math.Abs(first.Value - second.Value) <= tol
            && first.TimeModified == second.TimeModified
            && first.ExchangeAcquired == second.ExchangeAcquired;
        }

        public static bool operator !=(CurrencyAmount first, CurrencyAmount second)
        {
            // https://stackoverflow.com/questions/6916884/why-must-we-define-both-and-in-c#comment8242377_6919149
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj is CurrencyAmount other && this == other;
        }

        public override int GetHashCode()
        {
            string currency = this.Currency.Value.ToLower().Trim();
            return new
            {
                currency,
                this.Amount,
                this.Value,
                this.TimeModified,
                this.ExchangeAcquired
            }.GetHashCode();
        }
    }
}
