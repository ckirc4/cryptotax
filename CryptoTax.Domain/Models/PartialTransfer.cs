﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models
{
    /// <summary>
    /// Represents a partial transfer of currency from one container to another. We must have full information for either the source or destination container, and we may or may not have some/all information of the other.
    /// </summary>
    public class PartialTransfer
    {
        public TransactionDTO OriginalDTO { get; }
        public bool IsRelay { get; }
        /// <summary>
        /// A leading addon that should be applied to this <see cref="PartialTransfer"/>, taking into account that the ids, transferred amount, container, time, or fees may be different (but, if not different, are always explicitly duplicated). Can only be defined when <see cref="IsSourceSide"/> is true. The source relay object is guaranteed to also have <see cref="IsSourceSide"/> set to true.
        /// </summary>
        public PartialTransfer SourceRelay { get; private set; }
        /// <summary>
        /// A trailing addon that should be applied to this <see cref="PartialTransfer"/>, taking into account that the ids, transferred amount, container, time, or fees may be different (but, if not different, are always explicitly duplicated). Can only be defined when <see cref="IsDestinationSide"/> is true. The source relay object is guaranteed to also have <see cref="IsDestinationSide"/> set to true.
        /// </summary>
        public PartialTransfer DestinationRelay { get; private set; }

        /// <summary>
        /// This should be direction agnostic (i.e. be the same for both transfer parts)
        /// </summary>
        public NString InternalId { get; }
        /// <summary>
        /// This should be direction agnostic (i.e. be the same for both transfer parts)
        /// </summary>
        public NString PublicId { get; }
        /// <summary>
        /// The value of the CurrencyAmount is ignored. The amount is AFTER the sending fees have been applied, think of it as the amount that was transferred on-chain.
        /// </summary>
        public CurrencyAmount AmountTransferred { get; }
        /// <summary>
        /// If true, both Source and StartTime are defined, and FeeToSend may possibly be defined. Not mutually exclusive to <see cref="IsDestinationSide"/>.
        /// </summary>
        public bool IsSourceSide { get; }
        /// <summary>
        /// If true, both Destination and FinishTime are defined, and FeeToReceive may possibly be defined. Not mutually exclusive to <see cref="IsSourceSide"/>.
        /// </summary>
        public bool IsDestinationSide { get; }

        public Container? Source { get; private set; }
        public DateTime? StartTime { get; private set; }
        /// <summary>
        /// The value of the CurrencyAmount is ignored.
        /// </summary>
        public CurrencyAmount? FeeToSend { get; private set; }

        public Container? Destination { get; private set; }
        public DateTime? FinishTime { get; private set; }
        /// <summary>
        /// The value of the CurrencyAmount is ignored.
        /// </summary>
        public CurrencyAmount? FeeToReceive { get; private set; }

        public PartialTransfer(TransactionDTO originalDTO, NString internalId, NString publicId, CurrencyAmount amountTransferred, Container? source, DateTime? startTime, CurrencyAmount? feeToSend, Container? destination, DateTime? finishTime, CurrencyAmount? feeToReceive)
        {
            if (!internalId.HasValue && !publicId.HasValue) throw new TransferIdArgumentException("Must provide at least one type of id (internal or public)");

            if (source.HasValue != startTime.HasValue) throw new TransferSideArgumentException("Source and startTime must either both be null, or both be defined");
            bool isSourceSide = source.HasValue;
            if (!isSourceSide && feeToSend.HasValue) throw new TransferSideArgumentException("Cannot define feeToSend if partial transfer is not on the source side");

            if (destination.HasValue != finishTime.HasValue) throw new TransferSideArgumentException("Destination and finishTime must either both be null, or both be defined");
            bool isDestinationSide = destination.HasValue;
            if (!isDestinationSide && feeToReceive.HasValue) throw new TransferSideArgumentException("Cannot define feeToReceive if partial transfer is not on the destination side");

            this.OriginalDTO = originalDTO;
            this.IsRelay = false;
            this.SourceRelay = null;
            this.DestinationRelay = null;
            this.InternalId = internalId;
            this.PublicId = publicId;
            this.AmountTransferred = amountTransferred;
            this.IsSourceSide = isSourceSide;
            this.IsDestinationSide = isDestinationSide;

            this.Source = source;
            this.StartTime = startTime;
            this.FeeToSend = feeToSend.GetValueOrDefault().Amount == 0 ? null : feeToSend;

            this.Destination = destination;
            this.FinishTime = finishTime;
            this.FeeToReceive = feeToReceive.GetValueOrDefault().Amount == 0 ? null : feeToReceive;
        }

        public void WithRelay(TransferSide side, NString internalId, NString publicId, CurrencyAmount amountRelayed, Container container, DateTime time, CurrencyAmount? fee)
        {
            if (this.getRelay(side) != null) throw new TransferDuplicateRelayException($"Cannot add {side} relay because one already exists.");

            if (!internalId.HasValue && !publicId.HasValue) throw new TransferIdArgumentException("Must provide at least one type of id (internal or public)");

            if (amountRelayed.Currency != this.AmountTransferred.Currency) throw new TransferCurrencyArgumentException($"{side} relay currency ({amountRelayed.Currency}) must be the same as main partial transfer currency ({this.AmountTransferred.Currency})");

            if (!this.isSide(side)) throw new TransferSideArgumentException($"Cannot add a {side} relay when the partial transfer is not on the {side} side");

            PartialTransfer relay = side == TransferSide.Source ?
                 new PartialTransfer(this.OriginalDTO, internalId, publicId, amountRelayed, container, time, fee, null, null, null) :
                 new PartialTransfer(this.OriginalDTO, internalId, publicId, amountRelayed, null, null, null, container, time, fee);
            this.setRelay(side, relay);
        }

        /// <summary>
        /// Keeps all other properties the same.
        /// </summary>
        public void WithRelay(TransferSide side, Container container)
        {
            this.WithRelay(side, this.InternalId, this.PublicId, this.AmountTransferred, container, this.getTime(side).Value, this.getFee(side));
        }

        /// <summary>
        /// The currency must be compatible with the respective side's current fee and relay fee, where applicable.
        /// </summary>
        public void AddFee(TransferSide side, CurrencyAmount addedFee)
        {
            if (!this.isSide(side)) throw new TransferSideArgumentException($"Cannot add a fee to receive if the partial transfer is not on the {side} side");

            if (this.getFee(side).HasValue)
            {
                // add to existing if currencies agree
                CurrencyAmount existing = this.getFee(side).Value;
                if (existing.Currency != addedFee.Currency) throw new TransferCurrencyArgumentException($"Cannot add fee in currency {addedFee.Currency} because the existing fee is in currency {existing.Currency}");
                else this.setFee(side, existing.WithAmount(existing.Amount + addedFee.Amount));
            }
            else
            {
                // don't need to do any checking
                this.setFee(side, addedFee);
            }
        }

        #region getter and setter methods
        private PartialTransfer getRelay(TransferSide side) => side == TransferSide.Source ? this.SourceRelay : this.DestinationRelay;
        private bool isSide(TransferSide side) => side == TransferSide.Source ? this.IsSourceSide : this.IsDestinationSide;
        private DateTime? getTime(TransferSide side) => side == TransferSide.Source ? this.StartTime : this.FinishTime;
        private CurrencyAmount? getFee(TransferSide side) => side == TransferSide.Source ? this.FeeToSend : this.FeeToReceive;

        private void setRelay(TransferSide side, PartialTransfer relay)
        {
            if (side == TransferSide.Source) this.SourceRelay = relay;
            else this.DestinationRelay = relay;
        }
        private void setFee(TransferSide side, CurrencyAmount amount)
        {
            if (side == TransferSide.Source) this.FeeToSend = amount;
            else this.FeeToReceive = amount;
        }

        #endregion
    }
}
