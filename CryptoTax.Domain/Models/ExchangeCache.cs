﻿using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using UtilityLibrary.Collections;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Models
{
    public class ExchangeCache
    {
        private readonly ICandlestickPersistorService _CandlestickPersistorService;
        private readonly IExchangeResponsePersistorService _ExchangeResponsePersistorService;
        private readonly IMissingCandlestickPersistorService _MissingCandlestickPersistorService;

        private readonly DateTime _DefaultStartTime = default(Candlestick).StartTime;

        private readonly Dictionary<Exchange, IEnumerable<ExchangeSymbol>> _SymbolCache;
        /// <summary>
        /// Sorted list of candlesticks, from earliest to latest.<br/>
        /// IMPORTANT: DO NOT DIRECTLY ACCESS THIS. Instead, use <c>this.getCandlestickCache()</c> or <c>this.setCandlestickCache()</c>.
        /// </summary>
        private readonly Dictionary<DString, Dictionary<DateTime, Candlestick>> _CandlestickCache;
        private readonly Dictionary<DString, (DateTime time, int index, Candlestick cs)> _LruTimeCache;
        private readonly Dictionary<DString, Exception> _ExchangeExceptionCache;
        /// <summary>
        /// Used whenever reading from or writing to the cache - only want this to be done one at a time.
        /// </summary>
        private readonly object _CacheLock;

        public readonly DString CacheType;

        /// <summary>
        /// In-memory cache only.
        /// </summary>
        public ExchangeCache()
        {
            this._SymbolCache = new Dictionary<Exchange, IEnumerable<ExchangeSymbol>>();
            this._CandlestickCache = new Dictionary<DString, Dictionary<DateTime, Candlestick>>();
            this._LruTimeCache = new Dictionary<DString, (DateTime time, int index, Candlestick cs)>();
            this._ExchangeExceptionCache = new Dictionary<DString, Exception>();
            this._MissingCandlestickPersistorService = null;
            this.CacheType = "In-memory";
            this._CacheLock = new object();
        }

        /// <summary>
        /// Retrieves and stores data from the disk.
        /// </summary>
        /// <param name="candlestickPe"></param>
        public ExchangeCache(ICandlestickPersistorService candlestickPersistorService, IExchangeResponsePersistorService exchangeResponsePersistorService, IMissingCandlestickPersistorService missingCandlestickPersistorService) : this()
        {
            this._CandlestickPersistorService = candlestickPersistorService;
            this._ExchangeResponsePersistorService = exchangeResponsePersistorService;
            this._MissingCandlestickPersistorService = missingCandlestickPersistorService;
            this.CacheType = "Persisting";
        }

        public bool HasSymbols(Exchange exchange)
        {
            return this._SymbolCache.ContainsKey(exchange);
        }

        /// <summary>
        /// Returns true if the candlestick exists.
        /// </summary>
        public bool HasCandlestick(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime time)
        {
            Dictionary<DateTime, Candlestick> candlesticks = this.getCandlestickCache(exchange, symbol, interval);

            if (candlesticks == null || !candlesticks.Any()) return false;

            DateTime requiredStartTime = CandlestickHelpers.GetStartTime(interval, time);
            return candlesticks.ContainsKey(requiredStartTime);
        }

        /// <summary>
        /// Returns the ordered times between the two dates (inclusive) for which no candlesticks with that start time exist.
        /// </summary>
        public IEnumerable<DateTime> GetMissingCandlestickTimes(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime fromTime, DateTime toTime)
        {
            return (this.GetMissingCandlestickTimesGrouped(exchange, symbol, interval, fromTime, toTime)).SelectMany(group => group);
        }

        /// <summary>
        /// Returns the ordered grouped times between the two dates (inclusive) for which no candlesticks with that start time exist. Grouped times are contiguous sections of times.
        /// </summary> 
        public IEnumerable<IEnumerable<DateTime>> GetMissingCandlestickTimesGrouped(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime fromTime, DateTime toTime)
        {
            if (fromTime > toTime) throw new ArgumentException($"{nameof(fromTime)} ({fromTime}) must come after {nameof(toTime)} ({toTime})");
            List<List<DateTime>> groups = new List<List<DateTime>>();
            DateTime firstTime = CandlestickHelpers.GetStartTime(interval, fromTime);
            DateTime lastTime = CandlestickHelpers.GetStartTime(interval, toTime);

            DateTime nextTime = firstTime;
            List<DateTime> currentGroup = new List<DateTime>();
            while (nextTime <= lastTime)
            {
                DateTime thisTime = nextTime;
                if (!this.HasCandlestick(exchange, symbol, interval, thisTime))
                {
                    // current group continuation
                    currentGroup.Add(thisTime);
                    nextTime = CandlestickHelpers.GetNextTime(interval, thisTime);
                }
                else
                {
                    // current group ended
                    if (currentGroup.Any())
                    {
                        groups.Add(currentGroup);
                        currentGroup = new List<DateTime>();
                    }

                    nextTime = CandlestickHelpers.GetNextTime(interval, thisTime);
                }
            }

            if (currentGroup.Any()) groups.Add(currentGroup);
            return groups;
        }

        /// <summary>
        /// Gets all cached candlesticks in the specified temporal range. Does not handle missing candlesticks.
        /// </summary>
        public List<Candlestick> GetCandlesticks(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime fromTime, DateTime toTime)
        {
            if (fromTime > toTime) throw new ArgumentException($"{nameof(fromTime)} ({fromTime}) must come after {nameof(toTime)} ({toTime})");

            DateTime firstTime = CandlestickHelpers.GetStartTime(interval, fromTime);
            DateTime lastTime = CandlestickHelpers.GetStartTime(interval, toTime);

            if (firstTime == lastTime)
            {
                Candlestick? cached = this.GetCandlestick(exchange, symbol, interval, firstTime);
                if (cached.HasValue) return new List<Candlestick>() { cached.Value };
                else return new List<Candlestick>();
            }

            // this is rarely (ever?) encountered during the processing loops, since normally
            // we want only a single candlestick. so don't worry about using LRU here.
            return (this.getCandlestickCache(exchange, symbol, interval)?.Values.ToList() ?? new List<Candlestick>())
                .FindAll(c => c.StartTime >= firstTime && c.StartTime <= lastTime)
                .OrderBy(c => c.StartTime).ToList();
        }

        /// <summary>
        /// Gets the candlestick at the specified time, if it exists. If not, returns null.
        /// </summary>
        public Candlestick? GetCandlestick(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime time)
        {
            DateTime timeToFind = CandlestickHelpers.GetStartTime(interval, time);
            Dictionary<DateTime, Candlestick> candlesticks = this.getCandlestickCache(exchange, symbol, interval);

            if (candlesticks == null || !candlesticks.ContainsKey(timeToFind)) return null;
            else return candlesticks[timeToFind];
        }

        /// <summary>
        /// Retrieves the candlestick that comes directly after the given candlestick, if it exists. If not, returns null.
        /// </summary>
        public Candlestick? GetNextCandlestick(Candlestick candlestick)
        {
            return this.GetCandlestick(candlestick.Symbol.Exchange, candlestick.Symbol, candlestick.Interval, candlestick.GetNextTime());
        }

        /// <summary>
        /// Returns the symbols cached for the current exchange, or returns null if none exist.
        /// </summary>
        public IEnumerable<ExchangeSymbol> GetSymbols(Exchange exchange)
        {
            if (this.HasSymbols(exchange)) return this._SymbolCache[exchange];
            else return null;
        }

        public void SetSymbols(Exchange exchange, IEnumerable<ExchangeSymbol> symbols)
        {
            if (!this.HasSymbols(exchange)) this._SymbolCache.Add(exchange, symbols);
            else this._SymbolCache[exchange] = symbols;
        }

        /// <summary>
        /// Adds the given candlesticks to the cache, overwriting existing entries if applicable. The provided candlesticks do not need to be ordered or grouped.
        /// </summary>
        public void SetCandlesticks(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, IEnumerable<Candlestick> candlesticks)
        {
            if (!candlesticks.Any()) return;

            // group subsequent candlesticks so we can handle batch-inserting easily.
            candlesticks = candlesticks.OrderBy(c => c.StartTime);
            Candlestick? previousCandlestick = null;
            List<List<Candlestick>> groups = new List<List<Candlestick>>();
            List<Candlestick> currentGroup = new List<Candlestick>();
            foreach (Candlestick candlestick in candlesticks)
            {
                if (previousCandlestick == null)
                {
                    // first candlestick
                    currentGroup = new List<Candlestick>() { candlestick };
                    previousCandlestick = candlestick;
                }
                else if (previousCandlestick?.GetNextTime() == candlestick.StartTime)
                {
                    // still part of group
                    currentGroup.Add(candlestick);
                    previousCandlestick = candlestick;
                }
                else
                {
                    // new group
                    groups.Add(currentGroup);
                    currentGroup = new List<Candlestick>() { candlestick };
                    previousCandlestick = candlestick;
                }
            }

            groups.Add(currentGroup);

            // add each group to the cache
            // this is highly inefficient for multiple groups, and we could optimise this significantly by waiting until all changes are made, and THEN saving
            // e.g. by using a transaction mechanism, saveAfterTransaction(() => doTransactions)
            // but nowadays we only set candlesticks from the BulkCandlestickService, which
            // already does grouping and performance is not an issue as we save everything.
            groups.ForEach(group => this.setSubsequentCandlesticksUnsafe(exchange, symbol, interval, group));
        }

        public void CacheException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, T exception) where T : Exception
        {
            if (this._ExchangeResponsePersistorService == null)
            {
                DString key = ExchangeResponsePersistorService.GetExceptionCacheKey(exchange, url, parameters);
                if (this._ExchangeExceptionCache.ContainsKey(key)) this._ExchangeExceptionCache[key] = exception;
                else this._ExchangeExceptionCache.Add(key, exception);
            }
            else
            {
                this._ExchangeResponsePersistorService.CacheException(exchange, url, parameters, exception);
            }
        }

        /// <exception cref="InvalidTypeException"/>
        public bool TryGetCachedException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, out T exception) where T : Exception
        {
            if (this._ExchangeResponsePersistorService == null)
            {
                DString key = ExchangeResponsePersistorService.GetExceptionCacheKey(exchange, url, parameters);
                if (this._ExchangeExceptionCache.TryGetValue(key, out Exception exceptionBase))
                {
                    // note: exceptionBase.GetType() returns the expected high-order exception type
                    exception = exceptionBase as T;
                    if (exception == null) throw new InvalidTypeException("Cannot get cached exception because the given type did not match the cached type.", exceptionBase.GetType(), typeof(T));
                    return true;
                }
                else
                {
                    exception = null;
                    return false;
                }
            }
            else
            {
                return this._ExchangeResponsePersistorService.TryGetCachedException(exchange, url, parameters, out exception);
            }
        }

        /// <summary>
        /// Returns true if it is known that at least one candlestick's data is missing. If true, also outputs the upper bound of the data gap (inclusive), which is less than or equal to toUTC.
        /// </summary>
        public bool IncludesKnownMissingData(ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, out DateTime? unknownDataUntilTime)
        {
            unknownDataUntilTime = null;
            if (this._MissingCandlestickPersistorService == null) return false; // in-memory cache does not support caching missing data
            else return this._MissingCandlestickPersistorService.IncludesKnownMissingData(symbol.Exchange, symbol.Name, fromUTC, toUTC, out unknownDataUntilTime);
        }

        public void AddKnownMissingData(ExchangeSymbol symbol, DateTime noDataFrom, DateTime noDataTo)
        {
            if (this._MissingCandlestickPersistorService == null) return; // in-memory cache does not support caching missing data
            else this._MissingCandlestickPersistorService.AddKnownMissingData(symbol.Exchange, symbol.Name, noDataFrom, noDataTo);
        }

        #region Private Methods
        private DString getCandlestickCacheKey(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval)
        {
            return exchange.ToString() + symbol.Symbol.ToString() + interval.ToString();
        }

        /// <summary>
        /// Inserts the given candlesticks into the cache, potentially overwriting existing ones. The candlesticks must be temporally connected, and sorted in ascending order.
        /// Unsafe because it accesses the raw cache several times for efficiency. CALLER MUST NOT USE LOCK.
        /// </summary>
        private void setSubsequentCandlesticksUnsafe(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, IEnumerable<Candlestick> candlesticks)
        {
            List<Candlestick> existing = this.GetCandlesticks(exchange, symbol, interval, candlesticks.First().StartTime, candlesticks.Last().StartTime);
            DString key = this.getCandlestickCacheKey(exchange, symbol, interval);

            // check if we actually have changes... if not, no need to waste time accessing the file system.
            if (existing.Any() && candlesticks.Any()
                && existing.First().StartTime == candlesticks.First().StartTime
                && existing.Last().StartTime == candlesticks.Last().StartTime
                && existing.Count == candlesticks.Count()) return;

            lock (this._CacheLock)
            {
                if (existing.Any())
                {
                    // remove existing from the cache
                    foreach (Candlestick c in existing)
                    {
                        this._CandlestickCache[key].Remove(c.StartTime);
                    }
                }

                if (this._CandlestickCache.ContainsKey(key))
                {
                    foreach (Candlestick c in candlesticks)
                    {
                        this._CandlestickCache[key].Add(c.StartTime, c);
                    }
                }
                else
                {
                    // initialise cache
                    this._CandlestickCache.Add(key, candlesticks.ToDictionary(c => c.StartTime, c => c));
                }

                this.trySaveCandlesticks(exchange, symbol, interval);
            }
        }

        /// <summary>
        /// Use this method when retrieving candlesticks. Returns null if no candlesticks exist. CALLER MUST NOT USE LOCK.
        /// </summary>
        private Dictionary<DateTime, Candlestick> getCandlestickCache(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval)
        {
            lock (_CacheLock)
            {
                this.tryLoadInCandlestickDataIfRequired(exchange, symbol, interval);

                DString key = this.getCandlestickCacheKey(exchange, symbol, interval);
                if (this._CandlestickCache.ContainsKey(key))
                {
                    return this._CandlestickCache[key];
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Use this method when updating candlesticks. Overwrites any existing candlesticks. CALLER MUST NOT USE LOCK.
        /// </summary>
        private void setCandlestickCache(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, Dictionary<DateTime, Candlestick> candlesticks)
        {
            DString key = this.getCandlestickCacheKey(exchange, symbol, interval);

            lock (this._CacheLock)
            {
                if (this._CandlestickCache.ContainsKey(key))
                {
                    this._CandlestickCache[key] = candlesticks;
                }
                else
                {
                    this._CandlestickCache.Add(key, candlesticks);
                }

                this.trySaveCandlesticks(exchange, symbol, interval);
            }
        }

        /// <summary>
        /// If the data doesn't exist yet, checks to see if there is persistent data and, if so, loads the data into the cache. CALLER MUST USE LOCK.
        /// </summary>
        private void tryLoadInCandlestickDataIfRequired(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval)
        {
            if (this._CandlestickPersistorService == null) return;

            DString symbolName = symbol.Name;
            DString key = this.getCandlestickCacheKey(exchange, symbol, interval);

            if ((!this._CandlestickCache.ContainsKey(key) || !this._CandlestickCache[key].Any()) && this._CandlestickPersistorService.HasData(symbol, interval))
            {
                // load data into the cache. unless the cache gets invalidated later, we will not have to load this again in the future.
                // since we don't yet have any data, we can simply set the dictionary value as the result of this.
                List<CandlestickDTO> data = this._CandlestickPersistorService.ReadCandlesticks(symbol, interval);
                this._CandlestickCache.Remove(key);
                this._CandlestickCache.Add(key, data.ConvertAll(c => new Candlestick(c)).ToDictionary(c => c.StartTime, c => c));
            }
            else
            {
                // we don't have any data for this exchange-symbol-interval pair
            }
        }

        /// <summary>
        /// Saves the data from the cache to the disk. CALLER MUST USE LOCK.
        /// </summary>
        private void trySaveCandlesticks(Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval)
        {
            if (this._CandlestickPersistorService == null) return;

            DString symbolName = symbol.Name;
            DString key = this.getCandlestickCacheKey(exchange, symbol, interval);

            if (this._CandlestickCache.ContainsKey(key) && this._CandlestickCache[key].Any())
            {
                List<CandlestickDTO> orderedData = this._CandlestickCache[key].Values.OrderBy(c => c.StartTime).ToList().ConvertAll(c => c.ToDTO());
                this._CandlestickPersistorService.WriteCandlesticks(symbol, interval, orderedData);
            }
        }

        #endregion
    }
}
