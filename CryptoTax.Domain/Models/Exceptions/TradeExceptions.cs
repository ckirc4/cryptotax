﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class TradeException : Exception
    {
        public TradeException(string message) : base(message) { }
    }

    public class TradeSetupException : TradeException
    {
        public TradeSetupException(string message) : base(message) { }
    }
}
