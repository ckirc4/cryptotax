﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    // note: these are mostly for testing purposes, but may also be useful for other things in the future.
    public abstract class TransferException : Exception
    {
        public TransferException(string message) : base(message) { }
    }

    public class TransferDiscoveryAlreadyDoneException : TransferException
    {
        public TransferDiscoveryAlreadyDoneException(string message) : base(message) { }
    }

    public class TransferDiscoveryNotDoneException : TransferException
    {
        public TransferDiscoveryNotDoneException(string message) : base(message) { }
    }

    public class TransferIdArgumentException : TransferException
    {
        public TransferIdArgumentException(string message) : base(message) { }
    }

    public class TransferSideArgumentException : TransferException
    {
        public TransferSideArgumentException(string message) : base(message) { }
    }

    public class TransferTimeArgumentException : TransferException
    {
        public TransferTimeArgumentException(string message) : base(message) { }
    }

    public class TransferCurrencyArgumentException : TransferException
    {
        public TransferCurrencyArgumentException(string message) : base(message) { }
    }

    public class TransferAmountException : TransferException
    {
        public TransferAmountException(string message) : base(message) { }
    }

    public class TransferDuplicateRelayException : TransferException
    {
        public TransferDuplicateRelayException(string message) : base(message) { }
    }
}
