﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class BlockedTransactionException : Exception
    {
        public BlockedTransactionException(string msg)
            : base(msg)
        { }
    }
}
