﻿using CryptoTax.DTO;
using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class InconsistentTradeDataException : Exception
    {
        public TradeDTO Trade { get; }

        public InconsistentTradeDataException(TradeDTO trade) : base("Trade data is inconsistent - check your treatment of fees in the purchased and disposed amounts.")
        {
            this.Trade = trade;
        }

    }
}
