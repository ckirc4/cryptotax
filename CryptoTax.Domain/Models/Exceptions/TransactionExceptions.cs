﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class TransactionException : Exception
    {
        public TransactionException(string message) : base(message) { }
    }

    public class TransactionSetupException : TransactionException
    {
        public TransactionSetupException(string message) : base(message) { }
    }

    public class IncompleteTransactionException : TransactionException
    {
        public IncompleteTransactionException(string message) : base(message) { }
    }

    public class TransactionSideException : TransactionException
    {
        public TransactionSideException(string message) : base(message) { }
    }

    public class TransactionOutOfRangeException : TransactionException
    {
        public TransactionOutOfRangeException(string message) : base(message) { }
    }

    public class InvalidStakingSideTransactionException : TransactionException
    {
        public InvalidStakingSideTransactionException(string message) : base(message) { }
    }

    public class InvalidForkSideTransactionException : TransactionException
    {
        public InvalidForkSideTransactionException(string message) : base(message) { }
    }

    public class InvalidLoanSideTransactionException : TransactionException
    {
        public InvalidLoanSideTransactionException(string message) : base(message) { }
    }

    public class InvalidFiatSideTransactionException : TransactionException
    {
        public InvalidFiatSideTransactionException(string message) : base(message) { }
    }

    public class InvalidFiatCurrencyTransactionException : TransactionException
    {
        public InvalidFiatCurrencyTransactionException(string message) : base(message) { }
    }

    public class InvalidDepositSideTransactionException : TransactionException
    {
        public InvalidDepositSideTransactionException(string message) : base(message) { }
    }

    public class InvalidWithdrawalSideTransactionException : TransactionException
    {
        public InvalidWithdrawalSideTransactionException(string message) : base(message) { }
    }
}
