﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class InvalidStartTimeException : Exception
    {
        public DateTime ExpectedTime { get; set; }
        public DateTime ActualTime { get; set; }

        public InvalidStartTimeException(DateTime expectedTime, DateTime actualTime)
            : base($"Expected {expectedTime} but received {actualTime}")
        {
            this.ExpectedTime = expectedTime;
            this.ActualTime = actualTime;
        }
    }
}
