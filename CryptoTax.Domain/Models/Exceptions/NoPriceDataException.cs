﻿using System;
using System.Collections.Generic;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class NoPriceDataException : Exception
    {
        /// <summary>
        /// Intended for debug purposes only.
        /// </summary>
        public int? RecursionDepth { get; private set; }
        /// <summary>
        /// The earliest time to which we have gone back to try to fill-in the missing data, but still ended up with no data.<br/>
        /// Equal to the original `fromUTC` if the missing data was loaded.<br/>
        /// Null if there is no data at all.
        /// </summary>
        public DateTime? EarliestTimeChecked { get; private set; }
        /// <summary>
        /// The earliest time for which candlestick data had been found, if any. If defined, this must lie somewhere in the original fromUTC-toUTC range.<br/>
        /// Must be set once the exception has been caught, since this information is not available during initial throw.
        /// </summary>
        public DateTime? EarliestTimeWithData { get; set; }
        /// <summary>
        /// The candlesticks that _did_ have data when making the request. The first candlestick's time must be <see cref="EarliestTimeWithData"/>, as long as it occurs before the original `toUTC`. May be empty. Null only if <see cref="EarliestTimeChecked"/> is null.
        /// </summary>
        public IEnumerable<Candlestick> AvailableDataForRequest { get; set; }

        /// <summary>
        /// Set <paramref name="earliestTimeChecked"/> to null if no data exists at all.
        /// </summary>
        public NoPriceDataException(string message, DateTime? earliestTimeChecked)
        {
            this.RecursionDepth = null;
            this.EarliestTimeChecked = earliestTimeChecked;
            this.EarliestTimeWithData = null;
            this.AvailableDataForRequest = null;
        }

        public NoPriceDataException(string message, int recursionDepth, DateTime earliestTimeChecked) : base(message)
        {
            this.RecursionDepth = recursionDepth;
            this.EarliestTimeChecked = earliestTimeChecked;
            this.EarliestTimeWithData = null;
            this.AvailableDataForRequest = null;
        }
    }
}
