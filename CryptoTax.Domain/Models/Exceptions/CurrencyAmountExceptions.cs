﻿using System;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class CurrencyAmountOverdrawException : Exception
    {
        public CurrencyAmount CurrencyAmount { get; set; }
        public decimal RequestedReduction { get; set; }

        public CurrencyAmountOverdrawException(CurrencyAmount ca, decimal requestedReduction)
            : base($"Attempted to reduce CurrencyAmount {ca.Id} ({ca.Amount} {ca.Currency}) by {requestedReduction}.")
        {
            this.CurrencyAmount = ca;
            this.RequestedReduction = requestedReduction;
        }
    }
}
