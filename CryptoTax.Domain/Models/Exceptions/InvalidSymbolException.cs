﻿using CryptoTax.Shared.Enums;
using System;
using System.Net;

namespace CryptoTax.Domain.Models.Exceptions
{
    public class InvalidSymbolException : Exception
    {
        public Exchange Exchange { get; set; }

        public InvalidSymbolException(Exchange exchange, WebException innerException, string errorString) : base(errorString, innerException)
        {
            this.Exchange = exchange;
        }
    }
}
