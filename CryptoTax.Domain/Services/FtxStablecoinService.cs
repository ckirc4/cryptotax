﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class FtxStablecoinService : IFtxStablecoinService
    {
        private static DString USD = KnownCurrencies.USD.Symbol;

        private readonly IdProviderService _IdProviderService;
        private readonly IRegisteredLogger _Logger;
        private readonly IReportingService _ReportingService;
        private readonly ICurrencyConversionService _CurrencyConversionService;

        public FtxStablecoinService(IdProviderService idProviderService, ILoggingService loggingService, IReportingService reportingService, ICurrencyConversionService currencyConversionService)
        {
            this._IdProviderService = idProviderService;
            this._Logger = loggingService.Register(this, nameof(FtxStablecoinService));
            this._ReportingService = reportingService;
            this._CurrencyConversionService = currencyConversionService;
        }

        public IEnumerable<CurrencyAmount> OnFtxWithdrawal(
            IInventory inventory,
            DateTime time,
            CurrencyAmount intendedWithdrawalAmount,
            Func<IEnumerable<CurrencyAmount>> doWithdrawal,
            CurrencyAmount? fee,
            Action payFee)
        {
            IEnumerable<CurrencyAmount> withdrawn;
            if (!isStablecoin(intendedWithdrawalAmount))
            {
                // normal
                withdrawn = doWithdrawal().ToList();
                if (fee.HasValue && isStablecoin(fee.Value.Currency)) throw new Exception("Cannot do a non-stablecoin withdrawal with stablecoin fees");
                payFee();
            }
            else
            {
                DString withdrawingCurrency = intendedWithdrawalAmount.Currency;
                if (fee.HasValue && fee.Value.Currency != withdrawingCurrency) throw new Exception("Cannot do a stablecoin withdrawal where the fee currency is different than the withdrawing currency");
                decimal withdrawableQty = inventory.GetCurrentInventory()[KnownCurrencies.USD.Symbol].Sum(c => c.Amount);
                decimal feeQty = fee.HasValue ? fee.Value.Amount : 0;
                decimal removeQty = intendedWithdrawalAmount.Amount + feeQty > withdrawableQty ? withdrawableQty : intendedWithdrawalAmount.Amount + feeQty;

                decimal valuePerUsd = this._CurrencyConversionService.GetSingleAudUnitValueAsync(KnownCurrencies.USD.Symbol, time, Exchange.FTX).Result;
                CurrencyAmount mirroredWithdrawal = intendedWithdrawalAmount
                    .WithCurrency(KnownCurrencies.USD.Symbol, Exchange.FTX, time)
                    .WithValue(valuePerUsd * removeQty)
                    .WithAmount(removeQty);
                CurrencyAmount injectedCA; // must be less than or equal to the indended amount to withdraw
                if (removeQty <= withdrawableQty)
                {
                    injectedCA = intendedWithdrawalAmount.WithAmount(removeQty);
                }
                else
                {
                    (CurrencyAmount leftOver, CurrencyAmount reduced) = intendedWithdrawalAmount.ReduceAmount(removeQty, time);
                    ReportingSplitData splitData = new ReportingSplitData(this._IdProviderService, time, intendedWithdrawalAmount, reduced, leftOver);
                    this._ReportingService.AddData(splitData);
                    // note that leftOver is discarded because it has no physical meaning in the conversion/withdrawal
                    injectedCA = reduced;
                }
                injectedCA = injectedCA.WithValue(valuePerUsd * injectedCA.Amount, time);

                // e.g.: to accomodate the {ftx stablecoin withdrawal} from {inventory id}, {1 BUSD valued at 5 AUD} is swapped out with the existing {2 USD}
                ReportingConversionData data = new ReportingConversionData(this._IdProviderService, time, inventory, ConvertReason.FtxStablecoinWithdrawal, injectedCA, mirroredWithdrawal);
                this._ReportingService.AddData(data);

                // swap new currency with old currency
                // note that injectedCA.Amount == mirroredWithdrawal.Amount <= total withdrawing amount
                inventory.Purchase(injectedCA);
                inventory.Disposal(mirroredWithdrawal, 0, out decimal overdrawnDisposalAmount);
                if (overdrawnDisposalAmount > 0) throw new Exception("Overdrawn should be zero for converting stablecoins");

                // our inventory state is now ready to remove the intended CAs
                withdrawn = doWithdrawal().ToList();
                payFee();
            }

            return withdrawn;
        }

        public void OnFtxDeposit(
            IInventory inventory,
            DateTime time,
            IEnumerable<CurrencyAmount> intendedDepositAmount,
            Action doDeposit,
            CurrencyAmount? fee,
            Action payFee)
        {
            DString depositCurrency = intendedDepositAmount.First().Currency;
            decimal depositAmount = intendedDepositAmount.Sum(c => c.Amount);

            if (!isStablecoin(depositCurrency))
            {
                // normal
                if (fee.HasValue && isStablecoin(fee.Value.Currency)) throw new Exception("Cannot do a non-stablecoin deposit with stablecoin fees");
                doDeposit();
                payFee();
            }
            else
            {
                if (fee.HasValue && fee.Value.Currency != depositCurrency) throw new Exception("Cannot do a stablecoin deposit where the fee currency is different than the depositing currency");
                doDeposit();
                payFee();

                decimal netAmount = depositAmount - fee.GetValueOrDefault().Amount;
                decimal valuePerUsd = this._CurrencyConversionService.GetSingleAudUnitValueAsync(KnownCurrencies.USD.Symbol, time, Exchange.FTX).Result;
                CurrencyAmount toRemove = new CurrencyAmount(this._IdProviderService, depositCurrency, netAmount, valuePerUsd * netAmount, time);
                CurrencyAmount toAdd = toRemove.WithCurrency(KnownCurrencies.USD.Symbol, Exchange.FTX);

                // e.g.: to accomodate the {ftx stablecoin deposit} to {inventory id}, {1 USD valued at 5 AUD} is swapped out with the existing {2 BUSD}
                ReportingConversionData data = new ReportingConversionData(this._IdProviderService, time, inventory, ConvertReason.FtxStablecoinDeposit, toAdd, toRemove);
                this._ReportingService.AddData(data);

                // swap new currency with old currency
                // note that injectedCA.Amount == mirroredWithdrawal.Amount <= total withdrawing amount
                inventory.Purchase(toAdd);
                inventory.Disposal(toRemove, 0, out decimal overdrawnDisposalAmount);
                if (overdrawnDisposalAmount > 0) throw new Exception("Overdrawn should be zero for converting stablecoins");
            }
        }

        private static bool isStablecoin(CurrencyAmount amount)
        {
            return isStablecoin(amount.Currency);
        }

        private static bool isStablecoin(NString currency)
        {
            return KnownCurrencies.IsFtxStablecoin(currency, false);
        }
    }
}
