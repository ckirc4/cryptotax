﻿using CryptoTax.Domain.Models;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Shared;
using UtilityLibrary.Types;
using CryptoTax.Domain.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Models.Exceptions;

namespace CryptoTax.Domain.Services
{
    public class TransferService
    {
        public static readonly TimeSpan MAX_TRANSFER_DURATION = new TimeSpan(2, 0, 0, 0);

        private readonly IdProviderService _IdProviderService;
        private readonly ITransactionWhitelistService _TransactionWhitelistService;
        private readonly IRegisteredLogger _Logger;

        private IEnumerable<PartialTransfer> _PartialTransfers;
        // if we have many transactions (which we wouldn't... only on the order of a few hundred in a busy year) we need to start thinking about using ids for lookup instead, which probably involves making a domain version of TransactionDTO - i.e. the PROPER way of doing things :)
        private Dictionary<TransactionDTO, (Transfer, TransferSide)> _Transfers;

        public TransferService(IdProviderService idProviderService, ITransactionWhitelistService transactionWhitelistService, ILoggingService loggingService)
        {
            this._IdProviderService = idProviderService;
            this._TransactionWhitelistService = transactionWhitelistService;
            this._Logger = loggingService.Register(this, "TransferService");

            this._PartialTransfers = new List<PartialTransfer>();
        }

        /// <summary>
        /// Must call before starting processing transactions. Goes through all transactions and pieces them together to form transfers. Will not do any modifications to transactions. Returns the list of transfers that were found, in no particular order.
        /// </summary>
        public IEnumerable<Transfer> DoTransferDiscovery(IEnumerable<TransactionDTO> allTransactions)
        {
            if (this._Transfers != null) throw new TransferDiscoveryAlreadyDoneException("TransferDiscovery has already been completed.");
            this._Transfers = new Dictionary<TransactionDTO, (Transfer, TransferSide)>();

            DateTime lastTime = DateTime.MinValue;
            foreach (TransactionDTO tx in allTransactions)
            {
                if (this.requireTwoParts(tx))
                {
                    Transfer transferObject = this.tryMatchTransaction(tx);

                    if (transferObject != null)
                    {
                        this._Transfers.Add(transferObject.OriginalSourceTransaction, (transferObject, TransferSide.Source));
                        this._Transfers.Add(transferObject.OriginalDestinationTransaction, (transferObject, TransferSide.Destination));
                    }
                }

                this.purgeOldData(tx.Time, false);
                lastTime = tx.Time;
            }

            this.purgeOldData(lastTime, true);
            return this._Transfers.Values.Select(v => v.Item1).Distinct();
        }

        /// <summary>
        /// May return null if there is no transfer associated with that transaction.
        /// </summary>
        public (Transfer, TransferSide)? GetAssociatedTransfer(TransactionDTO tx)
        {
            if (this._Transfers == null) throw new TransferDiscoveryNotDoneException("Transfer discovery has not yet been done, so cannot get transfer from transaction.");

            if (this._Transfers.ContainsKey(tx)) return this._Transfers[tx];
            else return null;
        }

        /// <summary>
        /// Removes partial transfers for which we never found a sibling, as would be judged at the given time. financialYearComplete should be set to false if we haven't yet analysed all transactions for the current financial year.
        /// <br/><br/>
        /// This method is useless if not called within the Setup() method.
        /// </summary>
        private IEnumerable<PartialTransfer> purgeOldData(DateTime time, bool financialYearComplete)
        {
            if (this._Transfers == null) throw new TransferDiscoveryNotDoneException("Transfer discovery has not yet been done, so cannot get transfer from transaction.");

            IEnumerable<PartialTransfer> toPurge = this._PartialTransfers.Where(pt =>
                financialYearComplete
                || pt.StartTime.HasValue && pt.StartTime.Value.Add(MAX_TRANSFER_DURATION) < time
                || pt.FinishTime.HasValue && pt.FinishTime.Value.Add(MAX_TRANSFER_DURATION) < time);

            if (toPurge.Any())
            {
                this.doPurge(toPurge, time, financialYearComplete);
            }
            return toPurge;
        }

        /// <summary>
        /// Removes the given partial transfers from the store and prints any non-whitelisted transactions.
        /// </summary>
        private void doPurge(IEnumerable<PartialTransfer> toPurge, DateTime time, bool financialYearComplete)
        {
            // most of the code here is for logging - we want to show messages initially when matching transfers, so that
            // we don't have to wait for the whitelist exceptions to be thrown later on in the application and investigate
            // them one-by-one.
            List<DString> softBlockedTx = new List<DString>();
            List<DString> hardBlockedMsg = new List<DString>();

            foreach (PartialTransfer partialTransfer in toPurge)
            {
                try
                {
                    bool allow = this._TransactionWhitelistService.VerifyWhitelist(partialTransfer.OriginalDTO);

                    if (!allow) softBlockedTx.Add(partialTransfer.PublicId.Value ?? "Unspecified public TX");
                }
                catch (BlockedTransactionException e)
                {
                    hardBlockedMsg.Add(e.Message);
                    continue;
                }
            }
            this._Logger.Log(LogMessage.MSG_DEBUG_TRANSFER_PURGE, toPurge.Count(), time, financialYearComplete, softBlockedTx, hardBlockedMsg);

            // DO NOT remove the toList() here unless you want to wait all day for things to progress.
            this._PartialTransfers = this._PartialTransfers.Except(toPurge).ToList();
        }

        /// <summary>
        /// Returns false if the complete transaction-transfer information is provided by the single provided object (e.g. funding payment), otherwise returns true (e.g. sending from wallet to exchange account). If it doesn't require two parts, all available information about this transfer is already contained in the TransactionDTO.
        /// </summary>
        private bool requireTwoParts(TransactionDTO tx)
        {
            return tx.Type == TransactionType.CryptoDeposit
                || tx.Type == TransactionType.CryptoWithdrawal
                || tx.Type == TransactionType.OnChainTransfer;
        }

        /// <summary>
        /// Attempts to match a partial transfer with this transaction. If none is found, returns null and adds the provided transaction as a partial transfer to the internal state to possibly be matched later with another transaction.
        /// <br/><br/>
        /// Note: The provided transaction must be a type that requires two parts, otherwise will throw an exception.
        /// </summary>
        private Transfer tryMatchTransaction(TransactionDTO tx)
        {
            // the idea behind this is that a complete transfer of funds consists of two distinct events - the event when it was dispatched (e.g. withdrawn), and the event when it was received (e.g. deposited). the two events must be linked to create a single transfer object.

            if (!this.requireTwoParts(tx)) throw new ArgumentException($"The given transaction's type {tx.Type} does not require two parts, so it cannot be matched with another transaction.");

            PartialTransfer thisPartialTransfer = this.createPartialTransferFromTx(tx);

            // special case: immediately purge burn transactions - it should never be matched with another transaction
            if (tx.Counterparty.Value == KnownWallets.ETH_BURN_ADDRESS.Address)
            {
                this._PartialTransfers = this._PartialTransfers.Append(thisPartialTransfer);
                this.doPurge(new List<PartialTransfer>() { thisPartialTransfer }, tx.Time, false);
                return null;
            }

            // if we have any matches, then we know for a fact that the partial transfers match.
            // this is currently limited to partial transfers whose ids match.
            IEnumerable<PartialTransfer> exactCandidates = this._PartialTransfers.Where(tr =>
                tr.PublicId.HasValue && thisPartialTransfer.PublicId.HasValue && tr.PublicId == thisPartialTransfer.PublicId // public id matching takes precedence over internal id matching
                || tr.InternalId.HasValue && thisPartialTransfer.InternalId.HasValue && tr.InternalId == thisPartialTransfer.InternalId);

            IEnumerable<PartialTransfer> candidates;
            if (exactCandidates.Any())
            {
                candidates = exactCandidates;
            }
            else
            {
                // the following candidates are _inferred_ matches based on the same currency and approximate time and amounts.
                // in a busy period it is entirely possible that we get several matches using this method

                // Pass 1: Infer transfer out of BTCMarkets
                IEnumerable<PartialTransfer> inferredBtcMarketsTransfer = this._PartialTransfers.Where(tr =>
                    includesExchange(tr, thisPartialTransfer, Exchange.BTCMarkets) // only limit inferred matching to BTCMarkets for now 
                    && partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && isAmountApproximatelySame(tr.AmountTransferred.Currency, tr.AmountTransferred.Amount, thisPartialTransfer.AmountTransferred.Amount) // leave room for fees
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                );

                // Pass 2: Infer transfer from Binance or MercatoX to BitMex using the known wallet address
                IEnumerable<PartialTransfer> inferredBitMexTransfer = this._PartialTransfers.Where(tr =>
                    (includesExchange(tr, thisPartialTransfer, Exchange.Binance, Exchange.BitMex) || includesExchange(tr, thisPartialTransfer, Exchange.MercatoX, Exchange.BitMex))
                    && partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && tr.AmountTransferred.Amount == thisPartialTransfer.AmountTransferred.Amount
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                    && (tr.OriginalDTO.Counterparty == KnownWallets.BTC_BITMEX.Address || thisPartialTransfer.OriginalDTO.Counterparty == KnownWallets.BTC_BITMEX.Address)
                );
                if (inferredBitMexTransfer.Count() > 1) throw new Exception("Found multiple partial transfers relating to a BitMex transfer... this is unreasonable and something probably went wrong.");

                // Pass 3: Infer transfer from Binance to FTX using the limited Binance wallet transaction data
                IEnumerable<PartialTransfer> inferredBinanceTransfer = this._PartialTransfers.Where(tr =>
                    includesExchange(tr, thisPartialTransfer, Exchange.Binance, Exchange.FTX)
                    && partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && isAmountApproximatelySame(tr.AmountTransferred.Currency, tr.AmountTransferred.Amount, thisPartialTransfer.AmountTransferred.Amount)
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                );

                // Pass 4: Infer ETH -> Arb Nova bridge
                IEnumerable<PartialTransfer> inferredArbNovaBridgeTransfer = this._PartialTransfers.Where(tr =>
                    partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && isAmountApproximatelySame(tr.AmountTransferred.Currency, tr.AmountTransferred.Amount, thisPartialTransfer.AmountTransferred.Amount)
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                    && tr.OriginalDTO.Counterparty == KnownWallets.ETH_MAIN_ARB_NOVA_BRIDGE_CONTRACT.Address
                    && thisPartialTransfer.OriginalDTO.Counterparty == KnownWallets.ETH_ARB_NOVA_MAIN_BRIDGE_CONTRACT.Address
                );

                // Pass 5: Infer Arb Nova -> ETH bridge (via Hop Protocol)
                IEnumerable<PartialTransfer> inferredArbNovaMainnetBridgeTransfer = this._PartialTransfers.Where(tr =>
                    partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && isAmountApproximatelySame(tr.AmountTransferred.Currency, tr.AmountTransferred.Amount, thisPartialTransfer.AmountTransferred.Amount)
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                    && tr.OriginalDTO.Counterparty == KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_ARB_NOVA.Address
                    && thisPartialTransfer.OriginalDTO.Counterparty == KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_MAINNET.Address
                );

                // Pass 6: Infer transfer from either Ethereum chain to Kraken
                IEnumerable<PartialTransfer> inferredKrakenDeposits = this._PartialTransfers.Where(tr =>
                    partialsAreOnOppositeSides(tr, thisPartialTransfer)
                    && partialsHaveSameCurrency(tr, thisPartialTransfer)
                    && isAmountApproximatelySame(tr.AmountTransferred.Currency, tr.AmountTransferred.Amount, thisPartialTransfer.AmountTransferred.Amount)
                    && isDurationLessThan(tr, thisPartialTransfer, MAX_TRANSFER_DURATION)
                    && (tr.OriginalDTO.Counterparty == KnownWallets.ETH_KRAKEN_DEPOSIT_1.Address || tr.OriginalDTO.Counterparty == KnownWallets.ETH_KRAKEN_DEPOSIT_2.Address || tr.OriginalDTO.Counterparty == KnownWallets.ETH_ARB_NOVA_KRAKEN_DEPOSIT.Address)
                    && thisPartialTransfer.OriginalDTO.Exchange == Exchange.Kraken
                );

                candidates = inferredBtcMarketsTransfer
                    .Concat(inferredBitMexTransfer)
                    .Concat(inferredBinanceTransfer)
                    .Concat(inferredArbNovaBridgeTransfer)
                    .Concat(inferredArbNovaMainnetBridgeTransfer)
                    .Concat(inferredKrakenDeposits);
            }


            if (candidates.Count() == 0)
            {
                this._PartialTransfers = this._PartialTransfers.Append(thisPartialTransfer);
                return null;
            }
            else if (candidates.Count() == 1)
            {
                PartialTransfer match = candidates.First();

                this._PartialTransfers = this._PartialTransfers.Where(tr => tr != match);
                Transfer completeTransfer = match.IsSourceSide ? Transfer.FromPartials(match, thisPartialTransfer) : Transfer.FromPartials(thisPartialTransfer, match);
                return completeTransfer;
            }
            else
            {
                throw new Exception($"Found multiple pending partial transfers matching internal id '{thisPartialTransfer.InternalId.Value}' or public id '{thisPartialTransfer.PublicId.Value}'");
            }
        }

        private static bool isAmountApproximatelySame(DString currency, decimal amount1, decimal amount2)
        {
            // for now includes only BTCMarket currencies - add more as required

            decimal reasonableDiscrepancy;
            switch (currency.Value)
            {
                case "BCH":
                    reasonableDiscrepancy = 0.005m;
                    break;
                case "BTC":
                    reasonableDiscrepancy = 0.005m;
                    break;
                case "ETC":
                    reasonableDiscrepancy = 0.5m;
                    break;
                case "ETH":
                    reasonableDiscrepancy = 0.025m;
                    break;
                case "LTC":
                    reasonableDiscrepancy = 0.05m;
                    break;
                case "OMG":
                    reasonableDiscrepancy = 5;
                    break;
                case "XRP":
                    // accounts for 20 XRP activation fee for intermediate wallet
                    // e.g. Binance withdrawal from 2017_4 to intermediate to BTCMarkets, over two days, eating up 20 XRP
                    reasonableDiscrepancy = 25;
                    break;
                case "BUSD":
                    reasonableDiscrepancy = 2;
                    break;
                case "MOON":
                    // fees are paid in ETH, but allow rounding error
                    reasonableDiscrepancy = 0.0001m;
                    break;
                default:
                    throw new AssertUnreachable(currency.Value, "Invalid currency");
            }

            decimal difference = Math.Abs(amount1 - amount2);
            bool acceptable = difference <= reasonableDiscrepancy;
            return acceptable;
        }

        private PartialTransfer createPartialTransferFromTx(TransactionDTO tx)
        {
            ContainerEventDTO ev = tx.ContainerEvent;
            bool isSourceSide = ev.AmountSent.HasValue && ev.CurrencySent.HasValue;
            bool isDestinationSide = ev.AmountReceived.HasValue && ev.CurrencyReceived.HasValue;
            if (isSourceSide && isDestinationSide) throw new ArgumentException("Transaction cannot be both on the source and destination side simultaneously.");

            DString currency;
            decimal amount;
            Container? sourceContainer = null, destinationContainer = null;
            DateTime? startTime = null, finishTime = null;
            CurrencyAmount? feeToSend = null, feeToReceive = null;

            if (isSourceSide)
            {
                currency = ev.CurrencySent.Value;
                amount = ev.AmountSent.Value;
                sourceContainer = ev.Container;
                startTime = ev.EventTime;
                if (ev.FeeCurrency.HasValue && ev.FeeAmount.HasValue)
                {
                    feeToSend = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, ev.EventTime);
                }
            }
            else
            {
                currency = ev.CurrencyReceived.Value;
                amount = ev.AmountReceived.Value;
                destinationContainer = ev.Container;
                finishTime = ev.EventTime;
                if (ev.FeeCurrency.HasValue && ev.FeeAmount.HasValue)
                {
                    feeToReceive = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, ev.EventTime);
                }
            }

            CurrencyAmount amountTransferred = new CurrencyAmount(this._IdProviderService, currency, amount, 0, ev.EventTime);

            return new PartialTransfer(tx, tx.InternalTransactionId, tx.PublicTransactionId, amountTransferred, sourceContainer, startTime, feeToSend, destinationContainer, finishTime, feeToReceive);
        }

        private static bool partialsAreOnOppositeSides(PartialTransfer partial1, PartialTransfer partial2)
        {
            return partial1.IsSourceSide != partial2.IsSourceSide && partial1.IsDestinationSide != partial2.IsDestinationSide;
        }

        private static bool partialsHaveSameCurrency(PartialTransfer partial1, PartialTransfer partial2)
        {
            return partial1.AmountTransferred.Currency == partial2.AmountTransferred.Currency;
        }

        private static bool includesExchange(PartialTransfer partial1, PartialTransfer partial2, Exchange exchange1, Exchange? exchange2 = null)
        {
            bool firstExchangeMatched = partial1.OriginalDTO.Exchange == exchange1 || partial2.OriginalDTO.Exchange == exchange1;
            if (exchange2.HasValue)
            {
                return firstExchangeMatched && (partial1.OriginalDTO.Exchange == exchange2.Value || partial2.OriginalDTO.Exchange == exchange2.Value);
            }
            return firstExchangeMatched;
        }

        private static bool isDurationLessThan(PartialTransfer partial1, PartialTransfer partial2, TimeSpan duration)
        {
            DateTime start = partial1.StartTime ?? partial2.StartTime.Value;
            DateTime end = partial1.FinishTime ?? partial2.FinishTime.Value;
            return end - start <= duration;
        }
    }
}
