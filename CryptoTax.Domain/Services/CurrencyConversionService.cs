﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class CurrencyConversionService : ICurrencyConversionService
    {
        /// <summary>
        /// The assets that can be used to continue a chain cross-exchange to BTCMarkets.
        /// </summary>
        private readonly Currency[] _TransferrableAssets = new[]
        {
            KnownCurrencies.BTC,
            KnownCurrencies.ETH,
            KnownCurrencies.XRP,
            KnownCurrencies.LTC,
            KnownCurrencies.FLR
        };
        private readonly Currency[] _StableCoins = new[]
        {
            KnownCurrencies.USDT,
            KnownCurrencies.TUSD,
            KnownCurrencies.BUSD,
            KnownCurrencies.USDC
        };

        private readonly IExchangeApiService _BtcMarketsApiService;
        private readonly IExchangeApiService _BinanceSpotApiService;
        private readonly IExchangeApiService _FtxApiService;
        private readonly IExchangeApiService _KrakenApiService;
        private readonly IReportingService _ReportingService;
        private readonly IdProviderService _IdProvider;
        private readonly IRunService _RunService;
        private readonly CandlestickInterval _Interval;
        private readonly IPriceCalculator _PriceCalculator;

        public CurrencyConversionService(IExchangeApiService btcMarketsApiService, IExchangeApiService binanceSpotApitService, IExchangeApiService ftxApiService, IExchangeApiService krakenApiService, IReportingService reportingService, IdProviderService idProvider, IRunService runService, IPriceCalculator priceCalculator)
        {
            this._BtcMarketsApiService = btcMarketsApiService;
            this._BinanceSpotApiService = binanceSpotApitService;
            this._FtxApiService = ftxApiService;
            this._KrakenApiService = krakenApiService;
            this._ReportingService = reportingService;
            this._IdProvider = idProvider;
            this._RunService = runService;
            this._Interval = runService.GetCandlestickInterval();
            this._PriceCalculator = priceCalculator;
        }

        /// <summary>
        /// Get the current price of both of the currencies of the given pair, and given the price (ratio) of the pair.
        /// </summary>
        public async Task<(decimal quoteCurrencyValue, decimal baseCurrencyValue)> GetAudUnitValueAsync(ExchangeSymbol symbol, decimal price, DateTime time)
        {
            return await this.getUnitValue(symbol, price, time);
        }

        /// <summary>
        /// Get the current price of the specified currency. Unsafe, as only certain currencies are supported. Automatically determines best exchange to use - if that fails, and an exchange has been provided, attempts to use exchange to determine the most likely symbol.
        /// </summary>
        public async Task<decimal> GetSingleAudUnitValueAsync(DString currency, DateTime time, Exchange? fallbackExchange)
        {
            Currency knownCurrency = KnownCurrencies.TryGetCurrency(currency);
            if (knownCurrency == KnownCurrencies.AUD)
            {
                return 1;
            }

            // TAX-69: we _could_ add a CoinGecko API service, but that's lots of work for little gain
            if (fallbackExchange == Exchange.OnChain && currency == "MOON")
            {
                throw new NoPriceDataException("Cannot get price data for MOON", null);
            }

            ExchangeSymbol? maybeSymbol = this.TryGetExchangeSymbolFromCurrency(currency, fallbackExchange);
            if (maybeSymbol == null && fallbackExchange == null) throw new Exception($"{nameof(GetSingleAudUnitValueAsync)}() failed for currency '{currency}' because an appropriate ExchangeSymbol could not be constructed. Consider providing an exchange to fall back to.");
            else if (maybeSymbol == null) throw new Exception($"{nameof(GetSingleAudUnitValueAsync)}() failed for currency '{currency}' because an appropriate ExchangeSymbol could not be constructed, even when considering falling back to the {fallbackExchange.Value} exchange.");

            ExchangeSymbol symbol = maybeSymbol.Value;
            (decimal quoteValue, decimal baseValue) = await this.getUnitValue(symbol, null, time);
            if (symbol.Symbol.QuoteCurrency == knownCurrency)
            {
                return quoteValue;
            }
            else if (symbol.Symbol.BaseCurrency == knownCurrency)
            {
                return baseValue;
            }
            else throw new Exception($"Constructed ExchangeSymbol '{symbol.Name}' does not contain the currency '{knownCurrency.Symbol}'");
        }

        public ExchangeSymbol? TryGetExchangeSymbolFromCurrency(DString currency, Exchange? fallbackExchange)
        {
            // note: to do this exhaustively, we *could* use exchange symbols. But it is probably a waste of time, and besides, we wouldn't know which pairs are "safe" (since some pairs on Binance were added only recently, and thus historical data may not exist). So for now, just hardcode and see what happens.
            Currency knownCurrency = KnownCurrencies.TryGetCurrency(currency);
            ExchangeSymbol? symbol = null;

            // pass 1: try to infer from scratch
            if (knownCurrency == KnownCurrencies.AUD)
            {
                return null;
            }
            else if (this.isTransferrable(knownCurrency))
            {
                // tradable on BTCMarkets against AUD as quote
                symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, knownCurrency), Exchange.BTCMarkets);
            }
            else if (this.isStablecoin(knownCurrency))
            {
                // tradable on Binance against BTC as base (BTC is safest)
                symbol = new ExchangeSymbol(new GenericSymbol(knownCurrency, KnownCurrencies.BTC), Exchange.Binance);
            }
            else if (knownCurrency == KnownCurrencies.BNB)
            {
                // tradable on Binance against BTC as quote
                symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.BTC, knownCurrency), Exchange.Binance);
            }
            else if (knownCurrency == KnownCurrencies.USD)
            {
                // only tradeable on FTX (but now using Kraken as the fallback exchange)
                symbol = new ExchangeSymbol(new GenericSymbol(knownCurrency, KnownCurrencies.AUD), Exchange.Kraken);
            }

            // pass two: use fallback exchange if provided
            if (symbol == null && fallbackExchange != null)
            {
                if (fallbackExchange.Value == Exchange.Binance)
                {
                    // high level of confidence that a BTC pair will exist
                    symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.BTC, knownCurrency), Exchange.Binance);
                }
                else if (fallbackExchange.Value == Exchange.FTX)
                {
                    throw new Exception("FTX is not supported as a fallback exchange.");
                    // high level of confidence that a USD pair will exist
                    // symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, knownCurrency), Exchange.FTX);
                }
                else { /* result is null */}
            }

            return symbol;
        }

        /// <summary>
        /// Price may be "hardcoded" (price != null) for the specified symbol (if known beforehand, and/or to override the price that would be returned from the API) or, if the symbol is from an API-supported exchange, determined dynamically (price == null).
        /// </summary>
        private async Task<(decimal quoteCurrencyValue, decimal baseCurrencyValue)> getUnitValue(ExchangeSymbol symbol, decimal? price, DateTime time)
        {
            IEnumerable<ExchangeSymbol> chain = this.GetChainToAud(symbol);
            Dictionary<DString, decimal> prices = new Dictionary<DString, decimal>() { { "AUD", 1 } };
            IEnumerable<(ExchangeSymbol symbol, decimal price)> reportData = new List<(ExchangeSymbol symbol, decimal price)>();

            foreach (ExchangeSymbol chainLink in chain.Reverse())
            {
                decimal chainLinkPrice;

                if (chainLink == symbol && price.HasValue)
                {
                    // reached end
                    chainLinkPrice = price.Value;
                }
                else
                {
                    IExchangeApiService apiService;
                    switch (chainLink.Exchange)
                    {
                        case Exchange.Binance:
                            apiService = this._BinanceSpotApiService;
                            break;
                        case Exchange.FTX:
                            throw new Exception($"Using FTX for fetching price data is no longer supported (attempting to get price data for {symbol.Name}).");
                            //apiService = this._FtxApiService;
                            //break;
                        case Exchange.BTCMarkets:
                            apiService = this._BtcMarketsApiService;
                            break;
                        case Exchange.Kraken:
                            apiService = this._KrakenApiService;
                            break;
                        case Exchange.MercatoX:
                        case Exchange.BitMex:
                        case Exchange.OnChain:
                        case Exchange.Undefined:
                        default:
                            string exceptionMessage = $"Cannot get price data for exchange {chainLink.Exchange}.";
                            if (chainLink == symbol) exceptionMessage += " When no price is 'hardcoded', ensure that the symbol passed into this function is from one of the API-supported exchanges.";
                            throw new AssertUnreachable(chainLink.Exchange, exceptionMessage);
                    }

                    Candlestick candlestick = (await apiService.GetCandlesticksAsync(this._Interval, chainLink, time, 1, false)).First();
                    chainLinkPrice = this._PriceCalculator.CalculatePrice(time, candlestick);
                }

                // we now have the price of the base relative to the quote. e.g. a price of 10 means that we can obtain one unit of base currency by paying 10 unites of quote currency.
                // since we are going backwards, the first quote currency is always AUD (which has a value of 1).
                // we don't know the order of the pair, so we try to add both
                // either the base or quote currency must already be in the dictionary (this is why we call it a chain!)
                // example:
                // BTC / AUD: 50000-> 1 BTC = 50000 AUD [second condition below]
                // BTC / USDT: 40000-> 1 BTC = 40000 USDT-> 1 USDT = 50000 / 40000 AUD [second condition below]
                // ENJ / USDT: 0.1-> 1 ENJ = 0.1 USDT-> 1 ENJ = 0.1 * 50000 / 40000 AUD [first condition below]
                DString baseCurrency = chainLink.Symbol.BaseCurrency.Symbol;
                DString quoteCurrency = chainLink.Symbol.QuoteCurrency.Symbol;
                if (!prices.ContainsKey(baseCurrency) && prices.ContainsKey(quoteCurrency))
                {
                    decimal thisPrice = prices[quoteCurrency] * chainLinkPrice;
                    addPrice(prices, baseCurrency, thisPrice);
                }
                else if (!prices.ContainsKey(quoteCurrency) && prices.ContainsKey(baseCurrency))
                {
                    // alternatively, we can pay 1/10 units of the base currency to obtain 1 unit of the quote currency
                    decimal thisPrice = prices[baseCurrency] / chainLinkPrice;
                    addPrice(prices, quoteCurrency, thisPrice);
                }
                else if ((quoteCurrency == "ETH" && baseCurrency == "WETH" || baseCurrency == "ETH" && quoteCurrency == "WETH") && prices.ContainsKey("ETH") && prices.ContainsKey("WETH"))
                {
                    // when wrapping/unwrapping ETH, we will already know its and WETH's price and so we can skip ahead here
                    continue;
                }
                else throw new Exception($"Unable to calculate the price of the base currency ({baseCurrency}) or quote currency ({quoteCurrency}) because both or neither exist in the prices dictionary.");

                reportData = reportData.Append((chainLink, chainLinkPrice));
            }

            ReportingPriceData data = new ReportingPriceData(this._IdProvider, time, reportData.ToArray());
            this._ReportingService.AddData(data);
            return (quoteCurrencyValue: prices[symbol.Symbol.QuoteCurrency.Symbol],
                baseCurrencyValue: prices[symbol.Symbol.BaseCurrency.Symbol]);
        }

        /// <summary>
        /// Returns the most efficient chain of trading pairs, possibly across exchanges, that needs to be traversed to be able to convert the given currency to AUD.
        /// First: starting symbol (currency may be quote or base). Last: symbol with a quote currency in AUD.
        /// Uses currency and exchange weightings to quickly converge to the most reliable exchanges and liquid trading pairs.
        /// Examples: 
        /// USDT (binance): BTC/USDT (binance) -> BTC/AUD (BTCMarkets)
        /// BULLSHIT (FTX): BULLSHIT/USDT (FTX) -> BTC/USDT (binance) -> BTC/AUD (BTCMarkets)
        /// </summary>
        public IEnumerable<ExchangeSymbol> GetChainToAud(ExchangeSymbol symbol)
        {
            List<ExchangeSymbol> chain = new List<ExchangeSymbol>();

            ExchangeSymbol? nextStep = symbol;
            do
            {
                chain.Add((ExchangeSymbol)nextStep);
                nextStep = this.getNextStep(chain);
            } while (nextStep != null);

            return chain;
        }

        private ExchangeSymbol? getNextStep(IEnumerable<ExchangeSymbol> chain)
        {
            ExchangeSymbol lastSymbol = chain.Last();
            if (lastSymbol.Symbol.BaseCurrency == KnownCurrencies.AUD || lastSymbol.Symbol.QuoteCurrency == KnownCurrencies.AUD)
            {
                return null;
            }

            Exchange exchange = lastSymbol.Exchange;
            switch (exchange)
            {
                case Exchange.Binance:
                    return this.getBinanceSpotConversion(lastSymbol);
                case Exchange.FTX:
                    return this.getFtxConversion(lastSymbol);
                case Exchange.MercatoX:
                    return this.getMercatoXConversion(lastSymbol);
                case Exchange.BitMex:
                    return this.getBitmexConversion(lastSymbol);
                case Exchange.BTCMarkets:
                    return this.getBTCMarketsConversion(lastSymbol);
                case Exchange.OnChain:
                    return this.getOnChainConversion(lastSymbol);
                case Exchange.Kraken:
                    return this.getKrakenConversion(lastSymbol);
                case Exchange.Undefined:
                default:
                    throw new AssertUnreachable(exchange);
            }
        }

        private bool isTransferrable(Currency currency)
        {
            return this._TransferrableAssets.Contains(currency);
        }
        private bool isStablecoin(Currency currency)
        {
            return this._StableCoins.Contains(currency);
        }

        private ExchangeSymbol? getBTCMarketsConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            if (quoteAsset == KnownCurrencies.AUD)
            {
                return null;
            }
            else if (quoteAsset == KnownCurrencies.BTC)
            {
                // e.g. ENJ-BTC -> BTC-AUD
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = KnownCurrencies.BTC;
                nextExchange = Exchange.BTCMarkets;
            }
            else throw new Exception($"Chain for BTC Markets not implemented for quote assets other than AUD and BTC. Quote: {quoteAsset.Symbol}, base: {symbol.Symbol.BaseCurrency.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private ExchangeSymbol? getBinanceSpotConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Currency baseAsset = symbol.Symbol.BaseCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            if (baseAsset == KnownCurrencies.AUD || quoteAsset == KnownCurrencies.AUD)
            {
                // special case, e.g. AUDUSDT. don't want AUDUSDT -> BTCUSDT -> AUDBTC as that would cause an error
                return null;
            }
            else if (this.isTransferrable(quoteAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = quoteAsset;
            }
            else if (this.isTransferrable(baseAsset))
            {
                // e.g. BTCUSDT
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = baseAsset;
            }
            else if (quoteAsset == KnownCurrencies.BNB)
            {
                // e.g. ENJ-BNB -> BNB-BTC -> BTC-AUD
                nextQuoteAsset = KnownCurrencies.BTC;
                nextBaseAsset = quoteAsset;
                nextExchange = Exchange.Binance;
            }
            else if (this.isStablecoin(quoteAsset))
            {
                // e.g. ENJ-USDT => USDT-BTC -> BTC-AUD
                // this works every time because btc can be traded against all stablecoins on Binance
                nextQuoteAsset = quoteAsset;
                nextBaseAsset = KnownCurrencies.BTC;
                nextExchange = Exchange.Binance;
            }
            else throw new Exception($"Chain for Binance Spot not implemented for quote assets other than BTC, ETH, XRP, LTC or stablecoins. Quote: {quoteAsset.Symbol}, base: {baseAsset.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private ExchangeSymbol getFtxConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Currency baseAsset = symbol.Symbol.BaseCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            // the overwhelming majority (all?) of FTX derivatives are traded against USD or USDT
            if (this.isTransferrable(quoteAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = quoteAsset;
            }
            else if (this.isTransferrable(baseAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = baseAsset;
            }
            else if (quoteAsset == KnownCurrencies.USDT || baseAsset == KnownCurrencies.USDT)
            {
                // we *could* get data from FTX, but chances are we already have the required data from Binance, so try to use this
                nextExchange = Exchange.Binance;
                nextQuoteAsset = KnownCurrencies.USDT;
                nextBaseAsset = KnownCurrencies.BTC;
            }
            else if (quoteAsset == KnownCurrencies.USD)
            {
                nextExchange = Exchange.Kraken;
                nextQuoteAsset = quoteAsset;
                nextBaseAsset = KnownCurrencies.AUD;
            }
            else throw new Exception($"Chain for FTX not implemented for base or quote assets other than stablecoins and not transferrable. Quote: {quoteAsset.Symbol}, base: {baseAsset.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private ExchangeSymbol? getKrakenConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Currency baseAsset = symbol.Symbol.BaseCurrency;

            if (baseAsset == KnownCurrencies.AUD || quoteAsset == KnownCurrencies.AUD)
            {
                return null;
            }
            else if (quoteAsset == KnownCurrencies.USD)
            {
                return new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.AUD), Exchange.Kraken);
            }
            else
            {
                throw new NotImplementedException($"Can't convert quote {quoteAsset.Symbol} and base {baseAsset.Symbol} on Kraken.");
            }
        }

        private ExchangeSymbol getMercatoXConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Currency baseAsset = symbol.Symbol.BaseCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            if (this.isTransferrable(quoteAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = quoteAsset;
            }
            else if (this.isTransferrable(baseAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = baseAsset;
            }
            else if (this.isStablecoin(quoteAsset))
            {
                // we know that we can trade BTC against every stablecoin (except USD, but that doesn't exist on MercatoX anyway)
                nextExchange = Exchange.Binance;
                nextQuoteAsset = quoteAsset;
                nextBaseAsset = KnownCurrencies.BTC;
            }
            else if (this.isStablecoin(baseAsset))
            {
                nextExchange = Exchange.Binance;
                nextQuoteAsset = baseAsset;
                nextBaseAsset = KnownCurrencies.BTC;
            }
            else throw new Exception($"Chain for MercatoX is not implemented for assets that are not transferrable or stablecoins. Quote: {quoteAsset.Symbol}, base: {baseAsset.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private ExchangeSymbol getBitmexConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            // Note: Bitmex has 3 types of futures contracts:
            // 1. Inverse (e.g. XBTUSD)
            // 2. Quanto (e.g. XRP/USD)
            // 3. Linear (e.g. ETH/XBT)
            // We could calculate the BTC value of buying one of each type of contract at a certain price (can be somewhat complicated), but Bitmex already does this for us in the execution history. Since BTC that is deposited turns into XBT at a 1:1, and can also be withdrawn at the same ratio, we treat XBT and BTC interchangeably, thus we can jump straight to BTC markets.
            if (quoteAsset == KnownCurrencies.BTC)
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = quoteAsset;
            }
            else throw new Exception($"Chain for Bitmex not implemented for quote assets other than BTC (XBT/XBt). Quote: {quoteAsset.Symbol}, base: {symbol.Symbol.BaseCurrency.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private ExchangeSymbol getOnChainConversion(ExchangeSymbol symbol)
        {
            Currency quoteAsset = symbol.Symbol.QuoteCurrency;
            Currency baseAsset = symbol.Symbol.BaseCurrency;
            Exchange nextExchange;
            Currency nextQuoteAsset;
            Currency nextBaseAsset;

            if (quoteAsset == KnownCurrencies.WETH || baseAsset == KnownCurrencies.WETH)
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = KnownCurrencies.ETH;
            }
            else if (this.isTransferrable(quoteAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = quoteAsset;
            }
            else if (this.isTransferrable(baseAsset))
            {
                nextExchange = Exchange.BTCMarkets;
                nextQuoteAsset = KnownCurrencies.AUD;
                nextBaseAsset = baseAsset;
            }
            else throw new Exception($"Chain for OnChain not implemented for quote or base assets that are not transferrable. Quote: {quoteAsset.Symbol}, base: {baseAsset.Symbol}");

            return new ExchangeSymbol(new GenericSymbol(nextQuoteAsset, nextBaseAsset), nextExchange);
        }

        private static void addPrice(Dictionary<DString, decimal> existingPrices, DString thisCurrency, decimal thisPrice)
        {
            existingPrices.Add(thisCurrency, thisPrice);

            // hack: 1 WETH == 1 ETH
            if (thisCurrency == "ETH") existingPrices.Add("WETH", thisPrice);
        }
    }
}
