﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Attributes;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class RunService : IRunService
    {
        private const string _REF_KEY = "ref";
        private const string _STATE_KEY = "state";
        private const string _CONT_KEY = "cont";
        private static readonly string[] _PROPERTY_KEY =
            typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties
            .Where(p => p.GetCustomAttribute<ArgKeyAttribute>() != null)
            .SelectMany(p => p.GetCustomAttribute<ArgKeyAttribute>().Names)
            .ToArray();
        private static readonly Type[] _STRING_TYPES = new[] { typeof(string), typeof(DString), typeof(NString) };

        private readonly IdProviderService _IdProviderService;
        private readonly IRunPersistorService _RunPersistorService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Log;
        /// <summary>
        /// May be null
        /// </summary>
        private IReportingService _ReportingService;

        /// <summary>
        /// The current run state (mutable).
        /// </summary>
        private RunStateDTO _RunState;
        /// <summary>
        /// The application run info (immutable).
        /// </summary>
        private RunInfoDTO _RunInfo;

        /// <summary>
        /// To initialise properties, call <see cref="RunService.Initialise(string[])"/>.
        /// </summary>
        public RunService(IdProviderService idProviderService, IRunPersistorService runPersistorService, ILoggingService loggingService)
        {
            this._IdProviderService = idProviderService;
            this._RunPersistorService = runPersistorService;
            this._LoggingService = loggingService;
            this._Log = loggingService.Register(this, "RunService");
        }

        public void Initialise(string[] args)
        {
            /*
                Uses default configuration if no args provided.
                
                Hierarchy of settings RunInfo props:
                - ref=<runId> use settings from that run, excluding output state. error if not exists.
                - <key>=<value> syntax to set RunInfoDTO properties (overrides settings from above). error if invalid property/value (can only set properties with the ArgsName attribute).
                - state=<runId> to use the output state from that run (overrides state from ref). error if not exists.
                OR
                - cont=<runId> continue on from the specified run. short for:
                    ref=<run> financialYear=<run.financialYear++> state=<run>

                Please ensure all changes are back-compatible and the docs (Readme.md) are kept up-to-date.
            */

            // note: yes, I understand that DTOs are grossly misused. there is no point changing it now.

            DString newRunId = this._IdProviderService.GetNext(IdType.Run);
            ResolvedArgs resolvedArgs = parseArgs(args);
            NString @ref = resolvedArgs.Cont.HasValue ? resolvedArgs.Cont : resolvedArgs.Ref; // todo UTIL-16: create NString.First(params NString[] args)
            NString state = resolvedArgs.Cont.HasValue ? resolvedArgs.Cont : resolvedArgs.State;

            // default settings
            RunInfoDTO runInfo = new RunInfoDTO(newRunId, args);

            // load ref if specified
            if (@ref != null)
            {
                runInfo = this._RunPersistorService.Load(@ref.Value, newRunId, args);

                if (resolvedArgs.Cont != null) // todo UTIL-17: create NString implicit conversion to bool
                {
                    runInfo.FinancialYear++;
                }
            }

            // load beginning state if specified
            IEnumerable<InventoryDTO> inventories = new List<InventoryDTO>();
            if (state != null)
            {
                RunStateDTO loadedState = this._RunPersistorService.LoadState(state.Value);
                runInfo.BeginningState = loadedState; // immutable
                inventories = loadedState.Inventories;
            }
            this._RunState = new RunStateDTO(newRunId, inventories); // mutable and separate to beginning state, but same value at the moment

            // apply custom properties
            setProperties(runInfo, resolvedArgs.CustomProperties);
            this._RunInfo = runInfo;
        }

        public void Save()
        {
            this._RunPersistorService.Save(this._RunInfo);
        }

        public void SaveState()
        {
            this._RunPersistorService.SaveState(this._RunState);
        }

        public void SaveResults(CalculationResults results)
        {
            this._RunPersistorService.SaveResults(results.ToDTO(), this._RunInfo.Id);
        }

        public IEnumerable<IInventory> LoadBeginningInventories(IInventoryFactory factory, IReportingService reportingService)
        {
            this._ReportingService = reportingService;

            List<IInventory> result = new List<IInventory>();
            if (this.GetBeginningState()?.Inventories?.Any() != true) return result;

            IEnumerable<InventoryDTO> beginningDTOs = this.GetBeginningState().Inventories;
            IEnumerable<IInventory> loadedInventories = beginningDTOs.Select(dto => factory.Create(this._IdProviderService, this._ReportingService, this._LoggingService, dto)).ToList(); // toList is required since we are updating each element below, at which point the IEnumerable must be concrete/instantiated

            // we should not assume that the inventory management type is the same as in the previous run, and also the inventory type.
            RunInfoDTO stateRunInfo = this._RunPersistorService.Load(this.GetBeginningState().RunId, this.GetBeginningState().RunId, null);

            InventoryType fromInventoryType = stateRunInfo.InventoryType;
            InventoryType toInventoryType = this.GetInventoryType();
            if ((fromInventoryType == InventoryType.Average) != (toInventoryType == InventoryType.Average)) throw new NotImplementedException("Cannot continue from a previous state because the inventory types are incompatible.");

            InventoryManagementType fromManagement = stateRunInfo.InventoryManagementType;
            InventoryManagementType toManagement = this.GetInventoryManagementType();

            if (fromManagement == toManagement)
            {
                // great! don't need to do any extra work
            }
            else if (fromManagement == InventoryManagementType.Single && toManagement == InventoryManagementType.OnePerExchangeType)
            {
                // management scope is wider, attempt to split up CurrencyAmounts based on their purchase exchange.
                throw new NotImplementedException();
            }
            else if (fromManagement == InventoryManagementType.OnePerExchangeType && toManagement == InventoryManagementType.Single)
            {
                // collapse once
                throw new NotImplementedException();
            }

            // also reset capital gains - these do not carry over
            loadedInventories.ToList().ForEach(inv => inv.NetCapitalGains.Reset());

            this.UpdateCurrentInventories(loadedInventories);
            ReportingLoadInventoriesData row = new ReportingLoadInventoriesData(this._IdProviderService, DateTime.Now, this.GetBeginningState().RunId, loadedInventories);
            this._ReportingService.AddData(row);
            return loadedInventories;
        }

        public void UpdateCurrentInventories(IEnumerable<IInventory> inventories)
        {
            this._RunState = new RunStateDTO(this.GetRunId(), inventories.Select(inv => inv.ToDTO()).ToList());
        }

        public DString GetRunId() => this._RunInfo.Id;
        public string[] GetOriginalArgs() => this._RunInfo.OriginalArgs;
        public NString GetName() => this._RunInfo.Name;
        public FinancialYear GetFinancialYear() => this._RunInfo.FinancialYear;
        public InventoryType GetInventoryType() => this._RunInfo.InventoryType;
        public CandlestickInterval GetCandlestickInterval() => this._RunInfo.CandlestickInterval;

        public PriceCalculationType GetPriceCalculationType() => this._RunInfo.PriceCalculationType;
        public InventoryManagementType GetInventoryManagementType() => this._RunInfo.InventoryManagementType;
        public int? GetDecimalPlaces() => this._RunInfo.DecimalPlaces;
        public RoundingType GetRoundingType() => this._RunInfo.RoundingType;
        public bool GetEnableReporting() => this._RunInfo.EnableReporting;
        public LoggingTemplate? GetConsoleLoggingTemplate() => this._RunInfo.ConsoleLoggingTemplate;
        public LoggingTemplate? GetPersistorLoggingTemplate() => this._RunInfo.PersistorLoggingTemplate;
        public RunStateDTO GetBeginningState() => this._RunInfo.BeginningState;

        private static ResolvedArgs parseArgs(string[] args)
        {
            args = args
                .Select(a => a.ToLower().Trim())
                .Where(a => !string.IsNullOrEmpty(a))
                .ToArray();

            Dictionary<DString, NString> pairs = getKeyValuePairs(args);

            // extract special keys
            if (pairs.TryGetValue(_REF_KEY, out NString refId))
            {
                pairs.Remove(_REF_KEY);
            }
            if (pairs.TryGetValue(_STATE_KEY, out NString stateId))
            {
                pairs.Remove(_STATE_KEY);
            }
            if (pairs.TryGetValue(_CONT_KEY, out NString contId))
            {
                pairs.Remove(_CONT_KEY);
            }

            // ensure property keys are valid
            IEnumerable<DString> invalidKeys = pairs.Keys.Where(key => !_PROPERTY_KEY.Contains(key.Value));
            if (invalidKeys.Any()) throw new ArgumentException($"One or more keys are invalid: {string.Join(",", invalidKeys.Select(key => $"'{key}'"))}");

            return new ResolvedArgs(refId, stateId, contId, pairs);
        }

        private static Dictionary<DString, NString> getKeyValuePairs(string[] args)
        {
            Dictionary<DString, NString> pairs = new Dictionary<DString, NString>();

            for (int i = 0; i < args.Length; i++)
            {
                string arg = args[i];

                if (!arg.Contains("=")) throw new ArgumentException($"Key-value pairs must be formatted as <key>=<value>, violated by '{arg}'");

                int separatorIndex = arg.IndexOf('=');
                string key = arg.Substring(0, separatorIndex);
                NString value = arg.Substring(separatorIndex + 1);

                if (string.IsNullOrEmpty(key)) throw new ArgumentException($"Keys must be non-empty and defined strings, but key at position {i} is not.");

                try
                {
                    pairs.Add(key, value);
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException($"Keys must be unique, but '{key}' was specified multiple times.");
                }
            }

            return pairs;
        }

        private static void setProperties(RunInfoDTO runInfo, Dictionary<DString, NString> customProperties)
        {
            IEnumerable<PropertyInfo> properties = typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties.Where(prop => prop.GetCustomAttribute<ArgKeyAttribute>() != null);

            foreach (KeyValuePair<DString, NString> kvp in customProperties)
            {
                string key = kvp.Key;
                string value = kvp.Value == "null" ? null : kvp.Value;

                PropertyInfo prop = properties.First(p => p.GetCustomAttribute<ArgKeyAttribute>().Names.Contains(key)); // prop is never null since keys have already been verified to exist

                Type type = prop.PropertyType;
                if (type.IsGenericType)
                {
                    Type genericType = type.GetGenericTypeDefinition();
                    Type[] genericArguments = type.GetGenericArguments();
                    if (genericType != typeof(Nullable<>) || genericArguments.Length != 1) throw new ArgumentException($"Only Nullable<T> generics are permitted, but the given generic type was '{genericType.Name}' with {genericArguments.Length} generic arguments.");

                    if (value == null)
                    {
                        prop.SetValue(runInfo, null);
                        continue;
                    }
                    else
                    {
                        // unwrap the nullable
                        type = genericArguments[0];
                    }
                }

                if (type.IsPrimitive)
                {
                    // parse value to this type and simply set
                    prop.SetValue(runInfo, Convert.ChangeType(value, type));
                }
                else if (type.IsEnum)
                {
                    // convert value to the correct enum field by matching it to a name in ArgEnumValueAttribute
                    FieldInfo enumFieldInfo = type.GetFields(BindingFlags.Static | BindingFlags.Public)
                        .FirstOrDefault(field => field.GetCustomAttribute<ArgEnumValueAttribute>().Names.Contains(value));
                    if (enumFieldInfo == null)
                    {
                        throw new ArgumentException($"Enum value '{value}' cannot be converted to type '{type.Name}'.");
                    }

                    object enumFieldValue = enumFieldInfo.GetValue(null); // static field - no object required
                    prop.SetValue(runInfo, enumFieldValue);
                }
                else
                {
                    // find a constructor that takes only a string and invoke it
                    ConstructorInfo constructor = _STRING_TYPES.Select(t => type.GetConstructor(new[] { t })).Where(c => c != null).FirstOrDefault();
                    if (constructor != null)
                    {
                        Type stringType = constructor.GetParameters().First().ParameterType;
                        // for some reason, we can't invoke constructors that take a DString or NString with a string (causes implicit cast error, even though implicit casting is implemented for those types...)
                        object stringifiedValue = stringType == typeof(string) ? value : stringType.GetConstructor(new[] { typeof(string) }).Invoke(new[] { value });

                        object instantiated = constructor.Invoke(new object[] { stringifiedValue });
                        prop.SetValue(runInfo, instantiated);
                    }
                    else
                    {
                        // there is no other way we could expect to set this property - can't proceed
                        throw new ArgumentException($"Cannot assign value to property '{key}' because no constructor of object type {type} takes only a single string.");
                    }
                }
            }
        }
    }
}
