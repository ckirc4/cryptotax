﻿using CryptoTax.Domain.Inventory;
using System;
using System.Collections.Generic;

namespace CryptoTax.Domain.Services
{
    public interface IFtxStablecoinService
    {
        void OnFtxDeposit(IInventory inventory, DateTime time, IEnumerable<CurrencyAmount> intendedDepositAmount, Action doDeposit, CurrencyAmount? fee, Action payFee);
        IEnumerable<CurrencyAmount> OnFtxWithdrawal(IInventory inventory, DateTime time, CurrencyAmount intendedWithdrawalAmount, Func<IEnumerable<CurrencyAmount>> doWithdrawal, CurrencyAmount? fee, Action payFee);
    }
}