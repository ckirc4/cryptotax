﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public interface IRunService
    {
        /// <summary>
        /// Initialises all data given the application input args of the form key=value.
        /// </summary>
        void Initialise(string[] args);

        /// <summary>
        /// Saves the underlying run info to the file system. Throws error if run info already exists.
        /// </summary>
        void Save();
        /// <summary>
        /// Saves the current run state to the file system. May overwrite an existing state, if it exists.
        /// </summary>
        void SaveState();
        void SaveResults(CalculationResults results);
        DString GetRunId();
        string[] GetOriginalArgs();
        NString GetName();
        FinancialYear GetFinancialYear();
        InventoryType GetInventoryType();
        CandlestickInterval GetCandlestickInterval();
        PriceCalculationType GetPriceCalculationType();
        InventoryManagementType GetInventoryManagementType();
        int? GetDecimalPlaces();
        RoundingType GetRoundingType();
        bool GetEnableReporting();
        LoggingTemplate? GetConsoleLoggingTemplate();
        LoggingTemplate? GetPersistorLoggingTemplate();
        RunStateDTO GetBeginningState();

        /// <summary>
        /// Loads the inventories based on the beginning state, if required.
        /// </summary>
        IEnumerable<IInventory> LoadBeginningInventories(IInventoryFactory factory, IReportingService reportingService);

        /// <summary>
        /// Updates the current run state that is managed by the RunService.
        /// </summary>
        void UpdateCurrentInventories(IEnumerable<IInventory> inventories);
    }
}
