﻿using CryptoTax.Domain.Enums;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class IdProviderService
    {
        private readonly IdProviderPersistorService _IdProviderPersistorService;
        private readonly object _Lock;
        /// <summary>
        /// The id that was last used.
        /// </summary>
        private readonly Dictionary<IdType, long> _PreviousId;

        /// <summary>
        /// Persistent id provider with the ability to load and save its state.
        /// </summary>
        public IdProviderService(IdProviderPersistorService persistorService)
        {
            this._IdProviderPersistorService = persistorService;
            this._Lock = new object();
            this._PreviousId = dictFromDTO(this._IdProviderPersistorService.Read(), 0);

            // run check
            foreach (IdType type in Enum.GetValues(typeof(IdType)))
            {
                getPrefixFromType(type);
            }
        }

        /// <summary>
        /// In-memory id provider. Uses the given start value for all types of ids.
        /// </summary>
        public IdProviderService(long startValue)
        {
            this._IdProviderPersistorService = null;
            this._Lock = new object();
            this._PreviousId = dictFromDTO(IdProviderDTO.Empty, startValue - 1);
        }

        /// <summary>
        /// Generate and get the next id.
        /// </summary>
        public DString GetNext(IdType type)
        {
            lock (this._Lock)
            {
                long nextId = this._PreviousId[type] + 1;
                this._PreviousId[type] = nextId;

                // TAX-64 we don't want to write to the disk every time there is a change
                // (there are a LOT of changes), so only persist the run number when it is incremented
                // everything else will be saved in one go at the end of the application by calling this.Flush()
                if (this._IdProviderPersistorService != null && type == IdType.Run)
                {
                    this._IdProviderPersistorService.Write(this.toDTO());
                }
                return $"#{getPrefixFromType(type)}{nextId}";
            }
        }

        /// <summary>
        /// Call this at the end of the application to persist all outstanding data.
        /// </summary>
        public void Flush()
        {
            this._IdProviderPersistorService.Write(this.toDTO());
        }

        public static DString FixFormat(DString id, IdType type)
        {
            string originalId = id;
            string prefix = getPrefixFromType(type);

            if (id.Value[0] != '#') id = "#" + id;

            if (char.IsDigit(id.Value[1]))
            {
                // currently of the form #123
                id = id.Value.Insert(1, prefix);
            }
            else if (id.Value[1].ToString() != prefix)
            {
                // currently of the form #x123
                id = id.Value.Remove(1, 1).Insert(1, prefix);
            }

            // check
            if (id.Value[0] != '#' || id.Value[1].ToString() != prefix || TryConvertToNumeric(id) == null) throw new Exception($"Unable to format given id '{originalId}'.");

            return id;
        }

        public static int? TryConvertToNumeric(DString id)
        {
            if (int.TryParse(id.Value.Substring(2), out int value))
            {
                return value;
            } else
            {
                return null;
            }
        }

        /// <summary>
        /// Generates the DTO object of the current id state.
        /// </summary>
        private IdProviderDTO toDTO()
        {
            return new IdProviderDTO(
                inventoryId: this._PreviousId[IdType.Inventory],
                currencyAmountId: this._PreviousId[IdType.CurrencyAmount],
                reportDataRowId: this._PreviousId[IdType.ReportRowData],
                reportRowId: this._PreviousId[IdType.ReportRow],
                runId: this._PreviousId[IdType.Run]);
        }

        private static DString getPrefixFromType(IdType type)
        {
            switch (type)
            {
                case IdType.Inventory:
                    return "i";
                case IdType.CurrencyAmount:
                    return "c";
                case IdType.ReportRowData:
                    return "d";
                case IdType.ReportRow:
                    return "r";
                case IdType.Run:
                    return "n";
                default:
                    throw new AssertUnreachable(type, "Id prefix is not implemented");
            }
        }

        private static Dictionary<IdType, long> dictFromDTO(IdProviderDTO dto, long defaultValue)
        {
            Dictionary<IdType, long> dict = new Dictionary<IdType, long>();
            foreach (IdType type in Enum.GetValues(typeof(IdType)))
            {
                long? value;
                switch (type)
                {
                    case IdType.Inventory:
                        value = dto.InventoryId;
                        break;
                    case IdType.CurrencyAmount:
                        value = dto.CurrencyAmountId;
                        break;
                    case IdType.ReportRowData:
                        value = dto.ReportDataRowId;
                        break;
                    case IdType.ReportRow:
                        value = dto.ReportRowId;
                        break;
                    case IdType.Run:
                        value = dto.RunId;
                        break;
                    default:
                        throw new AssertUnreachable(type, "Can't get dict from DTO");
                }
                dict.Add(type, value ?? defaultValue);
            }
            return dict;
        }
    }
}
