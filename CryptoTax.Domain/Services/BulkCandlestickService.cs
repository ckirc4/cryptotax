﻿using CryptoTax.Domain.Models;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class BulkCandlestickService : IBulkCandlestickService
    {
        /// <summary>
        /// The maximum gap between candlesticks that we don't mind filling (getting extra data for) when fetching data.
        /// </summary>
        private const int _MAX_GAP = 20;

        private readonly IExchangeApiService _BtcMarketsApiService;
        private readonly IExchangeApiService _BinanceSpotApiService;
        private readonly IExchangeApiService _FtxApiService;
        private readonly IExchangeApiService _KrakenApiService;
        private readonly IRunService _RunService;
        private readonly CandlestickInterval _Interval;
        private readonly IFileService _FileService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;
        private readonly ExchangeCache _ExchangeCache;
        private readonly BulkCandlestickKeyPersistorService _KeyPersistor;

        private Dictionary<ExchangeSymbol, HashSet<DateTime>> _RequiredTimes;

        public BulkCandlestickService(IExchangeApiService btcMarketsApiService, IExchangeApiService binanceSpotApitService, IExchangeApiService ftxApiService, IExchangeApiService krakenApiService, IRunService runService, IFileService fileService, ILoggingService loggingService, ExchangeCache exchangeCache, BulkCandlestickKeyPersistorService keyPersistor)
        {
            this._BtcMarketsApiService = btcMarketsApiService;
            this._BinanceSpotApiService = binanceSpotApitService;
            this._FtxApiService = ftxApiService;
            this._KrakenApiService = krakenApiService;
            this._RunService = runService;
            this._Interval = this._RunService.GetCandlestickInterval();
            this._FileService = fileService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "BulkCandlestickService");
            this._ExchangeCache = exchangeCache;
            this._KeyPersistor = keyPersistor;

            this._RequiredTimes = new Dictionary<ExchangeSymbol, HashSet<DateTime>>();
        }

        public void AddRequired(ExchangeSymbol symbol, DateTime continuousTime)
        {
            switch (symbol.Exchange)
            {
                case Exchange.Binance:
                case Exchange.BTCMarkets:
                case Exchange.FTX:
                case Exchange.Kraken:
                    DateTime startTime = this.getStartTime(continuousTime);
                    this.addPair(symbol, startTime);
                    break;
                case Exchange.MercatoX:
                case Exchange.BitMex:
                case Exchange.OnChain:
                case Exchange.Undefined:
                default:
                    throw new NotImplementedException("Cannot require candlestick of an exchange whose API has not been inmplemented.");
            }
        }

        public async Task FetchAndSaveAll()
        {
            Dictionary<ExchangeSymbol, IEnumerable<IEnumerable<DateTime>>> groups = this._RequiredTimes.ToDictionary(kvp => kvp.Key, kvp => this.groupTimes(kvp.Value));

            this._Logger.Log(LogMessage.MSG_BULKCANDLESTICK_INFO_REQUIRING_CANDLESTICKS, this._RequiredTimes.Select(s => s.Value.Count).Sum(), this._RequiredTimes.Count, groups.Select(g => g.Value.Count()).Sum());

            List<Task> tasks = new List<Task>();
            foreach (KeyValuePair<ExchangeSymbol, IEnumerable<IEnumerable<DateTime>>> groupKvp in groups)
            {
                ExchangeSymbol symbol = groupKvp.Key;
                IEnumerable<IEnumerable<DateTime>> timeGroups = groupKvp.Value;
                DString key = this.getGroupKey(symbol, timeGroups);

                if (!this._KeyPersistor.HasKey(key))
                {
                    tasks.Add(this.getCandlesticksForExchangeSymbol(key, timeGroups, symbol));
                }
            }

            // don't catch - we don't expect any errors to bubble up.
            await Task.WhenAll(tasks);
            if (this._FileService is FileServiceCached fileServiceCached) fileServiceCached.FlushAll();

            // reset
            this._RequiredTimes = new Dictionary<ExchangeSymbol, HashSet<DateTime>>();
        }

        private async Task getCandlesticksForExchangeSymbol(DString key, IEnumerable<IEnumerable<DateTime>> timeGroups, ExchangeSymbol symbol)
        {
            IExchangeApiService api = this.getApiService(symbol);
            List<Task<IEnumerable<Candlestick>>> tasks = new List<Task<IEnumerable<Candlestick>>>();

            // reverse order so that, should we fill in many blank candlesticks, we may be able to skip several groups (mostly for 1 Minute intervals).
            foreach (IEnumerable<DateTime> group in timeGroups.Reverse())
            {
                DateTime start = group.First();
                DateTime end = group.Last();
                try
                {
                    // we need to get these sequentially to avoid overlaps and duplicates when filling in leading blanks.
                    // note that "getOnly" is set to false - let the api service do the work for us.
                    IEnumerable<Candlestick> result = await api.GetCandlesticksAsync(this._Interval, symbol, start, end, false);
                }
                catch (AggregateException e)
                {
                    // ideally this doesn't happen, but it shouldn't necessarily be a fatal error
                    this._Logger.Log(LogMessage.MSG_BULKCANDLESTICK_WARNING_ERRORS, 1, key, new IEnumerable<DString>[] { e.InnerExceptions.Select(ex => new DString(ex.Message)) });
                }
                catch (Exception e)
                {
                    // don't need to explicitly handle NoPriceData, api service already does this.
                    this._Logger.Log(LogMessage.MSG_BULKCANDLESTICK_WARNING_ERRORS, 1, key, new IEnumerable<DString>[] { new DString[] { e.Message } });
                }
            }

            this._KeyPersistor.AddKey(key);
        }

        private IExchangeApiService getApiService(ExchangeSymbol symbol)
        {
            switch (symbol.Exchange)
            {
                case Exchange.Binance:
                    return this._BinanceSpotApiService;
                case Exchange.FTX:
                    return this._FtxApiService;
                case Exchange.BTCMarkets:
                    return this._BtcMarketsApiService;
                case Exchange.Kraken:
                    return this._KrakenApiService;
                case Exchange.MercatoX:
                case Exchange.BitMex:
                case Exchange.OnChain:
                case Exchange.Undefined:
                default:
                    throw new AssertUnreachable(symbol.Exchange);
            }
        }

        /// <summary>
        /// Given any time, returns the start time of a candlestick with the current interval.
        /// </summary>
        private DateTime getStartTime(DateTime continuousTime)
        {
            return CandlestickHelpers.GetStartTime(this._Interval, continuousTime);
        }

        /// <summary>
        /// Adds the given pair to the required times, if it doesn't already exist.
        /// </summary>
        private void addPair(ExchangeSymbol symbol, DateTime startTime)
        {
            if (!this._RequiredTimes.ContainsKey(symbol)) this._RequiredTimes.Add(symbol, new HashSet<DateTime>());
            this._RequiredTimes[symbol].Add(startTime);
        }

        private IEnumerable<IEnumerable<DateTime>> groupTimes(HashSet<DateTime> times)
        {
            // if the difference between two times is larger than this, start a new group
            TimeSpan maxGap = new TimeSpan(CandlestickHelpers.IntervalToTicks(this._Interval) * (_MAX_GAP + 1));

            List<List<DateTime>> groups = new List<List<DateTime>>();
            List<DateTime> thisGroup = new List<DateTime>();
            DateTime previousTime = times.Min();

            foreach (DateTime time in times.OrderBy(t => t))
            {
                if (time.Subtract(previousTime) > maxGap && thisGroup.Any())
                {
                    groups.Add(thisGroup.ToList());
                    thisGroup.Clear();
                }
                thisGroup.Add(time);
                previousTime = time;
            }

            if (thisGroup.Any()) groups.Add(thisGroup.ToList());

            return groups;
        }

        private DString getGroupKey(ExchangeSymbol symbol, IEnumerable<IEnumerable<DateTime>> groups)
        {
            string baseString = $"{this._Interval}-{symbol.Exchange}-{symbol.Symbol.BaseCurrency.Symbol}-{symbol.Symbol.QuoteCurrency.Symbol}";
            IEnumerable<string> groupStrings = groups.Select(g => $"{g.Min().ToString("dd-MM-yyyy HH:mm")} ({g.Count()})");
            return $"{baseString} & {string.Join(" & ", groupStrings)}";
        }
    }
}
