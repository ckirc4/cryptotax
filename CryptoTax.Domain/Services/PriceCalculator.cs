﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;

namespace CryptoTax.Domain.Services
{
    public interface IPriceCalculatorFactory
    {
        IPriceCalculator Create(PriceCalculationType type);
    }

    public class PriceCalculatorFactory : IPriceCalculatorFactory
    {
        public IPriceCalculator Create(PriceCalculationType type)
        {
            switch (type)
            {
                case PriceCalculationType.Open:
                    return new PriceCalculatorOpen();
                case PriceCalculationType.Close:
                    return new PriceCalculatorClose();
                case PriceCalculationType.ArithmeticMean:
                    return new PriceCalculatorArithmeticMean();
                case PriceCalculationType.TimeWeightedAverage:
                    return new PriceCalculatorTimeWeightedAverage();
                default:
                    throw new AssertUnreachable(type);
            }
        }
    }

    public interface IPriceCalculator
    {
        PriceCalculationType Type { get; }

        /// <summary>
        /// Calculates the price at the supplied time, given the candlestick that encloses this time (i.e. it is assumed that time >= candlestick.StartTime && time <= candlestick.EndTime)
        /// </summary>
        decimal CalculatePrice(DateTime time, Candlestick candlestick);
    }

    public class PriceCalculatorClose : IPriceCalculator
    {
        public PriceCalculationType Type { get; } = PriceCalculationType.Close;

        public decimal CalculatePrice(DateTime time, Candlestick candlestick)
        {
            return candlestick.Close;
        }
    }

    public class PriceCalculatorOpen : IPriceCalculator
    {
        public PriceCalculationType Type { get; } = PriceCalculationType.Open;

        public decimal CalculatePrice(DateTime time, Candlestick candlestick)
        {
            return candlestick.Open;
        }
    }

    public class PriceCalculatorArithmeticMean : IPriceCalculator
    {
        public PriceCalculationType Type { get; } = PriceCalculationType.ArithmeticMean;

        public decimal CalculatePrice(DateTime time, Candlestick candlestick)
        {
            return (candlestick.Open + candlestick.Close) / 2;
        }
    }

    public class PriceCalculatorTimeWeightedAverage : IPriceCalculator
    {
        public PriceCalculationType Type { get; } = PriceCalculationType.TimeWeightedAverage;

        public decimal CalculatePrice(DateTime time, Candlestick candlestick)
        {
            decimal start = candlestick.StartTime.Ticks;
            decimal end = candlestick.GetNextTime().Ticks - 1;
            decimal frac = (time.Ticks - start) / (end - start);
            return frac * (candlestick.Close - candlestick.Open) + candlestick.Open;
        }
    }
}
