﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Services.Logging;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    /* 
    Rationale:
    Given the current implementation of transfers, it is not possible to match two partial transfers (i.e. the event at the source with the event at the destination)
    to create a full transfer. So, we go through all partial transfers that couldn't find a source/destination (that is not a third party) and see whether we are ok
    with it, and, if not, make changes to the transfer matching algorithm.
    Most of the transactions here were due to some exchanges not yet being implementing, which lead to incomplete transfers - when it was identified that adding the
    exchange should complete the transfer in the future, we set the status to Whitelist_Warning (with a helpful message). Then, once those exchanges were implemented,
    twe set the status to Assert_Unreachable - it should never be the case that those partial transfers cannot find a match.

    What remains are partial transfers that really and truly are partial in our eyes, and not a bug.
    */
    public class TransactionWhitelistService : ITransactionWhitelistService
    {
        // XRPL explorer: https://bithomp.com/explorer/<tx>
        // BTC explorer: https://blockchair.com/bitcoin/transaction/<tx>
        // ETC explorer: https://blockexplorer.one/etc/mainnet/tx/<tx>
        // ETH explorer: https://etherscan.io/tx/<tx>

        private IRegisteredLogger _Logger;

        public TransactionWhitelistService(ILoggingService loggingService)
        {
            this._Logger = loggingService.Register(this, "TransactionWhitelistService");
        }

        /// <summary>
        /// Should only check transactions that couldn't be matched to a Transfer object (i.e. those with an unknown/third-party source/destination). Returns true if the transaction should be processed, false otherwise.
        /// </summary>
        /// <exception cref="BlockedTransactionException"/>
        public bool VerifyWhitelist(TransactionDTO tx)
        {
            (TransactionWhitelistStatus status, NString msg) = this.getWhitelistStatus(tx.PublicTransactionId, tx.InternalTransactionId);

            if (status == TransactionWhitelistStatus.Whitelisted_Info)
            {
                this._Logger.Log(LogMessage.MSG_WHITELISTED_INFO, printTxSummary(tx), msg);
            }
            else if (status == TransactionWhitelistStatus.Whitelisted_Warning)
            {
                this._Logger.Log(LogMessage.MSG_WHITELISTED_WARNING, printTxSummary(tx), msg);
            }
            else if (status == TransactionWhitelistStatus.Hard_Blocked)
            {
                throw new BlockedTransactionException($"Transaction {printTxSummary(tx)} is blocked because it has not been whitelisted: {msg}");
            }
            else if (status == TransactionWhitelistStatus.Assert_Unreachable)
            {
                throw new BlockedTransactionException($"Transaction {printTxSummary(tx)} should not have been processed by itself: {msg}");
            }

            return status != TransactionWhitelistStatus.Soft_Blocked;
        }

        // note: instead of removing transaction from this list once more exchanges have been added and sources identified, set the status to Assert_Unreachable.
        private (TransactionWhitelistStatus, NString) getWhitelistStatus(NString publicId, NString internalId)
        {
            TransactionWhitelistStatus? status = null;
            NString msg = null;

            if (publicId.HasValue)
            {
                switch (publicId.Value)
                {
                    #region MercatoX and DDEX
                    case "0xa04315f998a4c60b958c027dbba8faa93f78fd0e1028453a81e248a43654ede0":
                    case "0x87f667f90d3d55cee841bdbd71e720ba1ba492454d40aba5c2a5573b860a5878":
                        msg = "From Binance to main wallet - currently created out of thin air";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "0x360fe9fda16862a554a537ba9d3912c651338fb5df7f869a72c45cc117c01cf3":
                        msg = "Approved WETH for trade on Hydro: ERC20 Proxy";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x163813766e4ba402d491cabc9c9366358cd790f7bf35f1413489a5ef4692daf0":
                        msg = "Approved BOMB for trade on Hydro: ERC20 Proxy";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0xed6f1f43f84190cf525fc1c37a7bf9acc255fcc4e6825de4263d8c69a7979f05":
                    case "0xaafa3db9612f6d7fb2d4e6404ce35d49e0114ca89500854c648f9fdd9ffd7bb7":
                    case "0x59df5b6092a5d6fcb91e075c4da82e6efa73d059cae7a0c410664d4fcff38aa8":
                    case "0x51feff2df291cc5159cc17eb5b14f242fe7db2040522c346335fc9c626bfc070":
                        msg = "BOMB fee burned";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0xca57e2138dc5a2f8ebba77179cce7c662aabab4e2e5d91933fa1032d1d3a60fd":
                    case "0x5ef7c5d1bda5dcd9943d0ae0273fa9b2aff73f34a374b24aa902d1f03cc9eba7":
                    case "0xcd681cd8811a7ea5824802c09393f78b531d8bbf26bcdb0c389a90675ca89cb7":
                    case "0x2e9ba279f9b542463ff8c823585b0efb2d92247ad9c8b8849df09eb6cdf64dd0":
                        msg = "Trade on DDEX, this is either the lone burn transaction or fee to DDEX (either way, bye bye funds)";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x6aa7566651bc3ce833e3b4b454128511edf38f51b1b32e1f8c41723652977e4a":
                        msg = "Withdrawn from MercatoX into a relay wallet (https://etherscan.io/address/0x0e15a8e0e91814b8b66c0146b823a48105318e88), which is then forwarded to Binance";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "0x22c83e602209d2c9adf960d10ed68d612d249cb6aad42de6563634f8ed7e65c9":
                    case "0x2cefe00816cbbe1e762978f8a021fdd63d7f9413b16f91cd6f8d4ab8842e1665":
                    case "0x0fc7abaa3637dcb171dcc24efdc36856cd3db2f46fe20a4f62057ef234777874":
                    case "0xe01613bae03bf67271a3e2888b78e26027fe7df1acb6bb9af8b9a9e056530629":
                        msg = "Withdrawal from MercatoX";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "e25f6899fe0cc112ced334a3c8b5bfebde811f450c09fce1ae8944f196233356":
                        msg = "Withdrawal from MercatoX to Bitmex";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "0xda09df1f3fe3e0b96854e94d07d21847ff9b7d29d08be74b3cbb11633edf832c":
                    case "0x8cd05dbfcc6690ac252f119a288f2e34a3a31418e6dc2c8f6fcd767faceb80f2":
                        msg = "Approved MBT for trade on Uniswap V2: Router 2";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x1cfceec1d49fcee454fcacd31d5ea7238fbb125ea6ed61d5acf455321be395bc":
                    case "0xb932a6bf220cdd712f6d1828fb0741416d3c40fff17043ac5ba99e445b6c01fb":
                        msg = "Random airdrops";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;
                    #endregion

                    #region BTCMarkets
                    case "6ec39448d527895576764d9ef90459608dc5ff9f4c5dcb441a26f1e62f601206": // 12th of June 2017
                    case "15ea787eff3a4c0fa64180f1bed2f198fb5010fba5d3378dd7fd30dd7d0e4789": // 12th of June 2017
                        // 9/2/2016: Income from Bit Trade for AUD$155.73 trading between 540 and 550 on BTCMarkets, so sold between 0.283 and 0.288 BTC 
                        // would be good to get a cost base going for these if possible, but likely that I neither have access to the BitTrade account, nor do the bank statememnts show more than the amount of money transferred. either way since BTC was trading so low the cost basis would be close to zero anyway
                        msg = "BTC deposit to BTCMarkets from unkonwn address - probably from BitradeAustralia (or whatever it was called)";
                        status = TransactionWhitelistStatus.Whitelisted_Warning;
                        break;

                    case "b84209c4712b5faf4ef4284615c1cb59d728d28689f6f928973be6b484a637fa": // 24th of June 2017 0.4995 BTC
                        // from chris coinspot -> will coinspot -> [tx b84209c4712b5faf4ef4284615c1cb59d728d28689f6f928973be6b484a637fa] to btcmarkets (address 143zY9S2qeh9jAd9H7T8sVRnzNzLq4zR3z)
                        // chris coinspot received multiple small tx in January 2016.
                        // chris coinspot wallet address is 1AT4douauzF6yn6JjtpF2mqKCfb2vrr6am
                        // on the 29/01/2016, it sent 0.041887 BTC to 14uYKTh2JLJwdEzNESSnVPyMuE3b6Vyswh
                        // from the Google Drive/Other/Bitcoin Wallet.txt, it appears that 14uYKT belongs to me indeed, it was a Blockchain.info wallet
                        // on 15/06/2017 it received the 0.5 BTC from above [tx cb330013776278ec22c7967b67007782c5fcf96a1271fae375f9a17af7c3ebdf]

                        /*
                        Found in bank statements:
                        16/12/2015: Transferred $350 to Classic account, referenced "Btc"
	                        - Might correspond with https://blockchair.com/bitcoin/address/14uYKTh2JLJwdEzNESSnVPyMuE3b6Vyswh
	                        - 15/12/2015 sent 0.0101 BTC
	                        - 16/12/2015 received 0.012267 BTC
	                        - 16/12/2016 sent 0.0201 BTC
                        9/2/2016: Income from Bit Trade for AUD$155.73 trading between 540 and 550 on BTCMarkets, so sold between 0.283 and 0.288 BTC 
                        */
                        msg = "BTC deposit from Coinspot, zero cost basis";
                        status = TransactionWhitelistStatus.Whitelisted_Warning;
                        break;

                    case "1fcd14da8666bf5882ca4bf53e757b78166af8c79cc942c14d385caee3d0fcb8":
                        msg = "BTC gift for Stefan";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "c3b93c206ff360d2f89ce2fdcbf806569b5ad98b5fb18bb7539e8c491437df17":
                    case "10e825998ce64e4b23560a487f7f8d909970e9d951cd460e841c6a23f6b1bb21":
                    case "1f7cacc9488cb60b42ae9555ab97c82796179e7fb5a499c0e763fba6072f876c":
                    case "8ea58b0571bd6841b2d761871b3d951c528dc385918bfe427d828736d8b02619":
                        msg = "BTC withdrawal from BTCMarkets to unknown wallet (1Acf6APjHKRff3wbnsCzr6WUVk2gY9ZmSn).";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "2FF7DAC3A55D2C58BF0D8499CEB1399E4BDFD433A45B96C9CD3BC628BA50DA2B":
                    case "D2461E62A10D0E20D07FD818594DD53C0757E15BFAFE87366A5CA1100B63EFE5":
                    case "36CA516EFFD057733B6DDDC907EBF6C823B51ED57A6555AC351DBBE74E7B51B8":
                    case "C3FCCDC39BBCAA4C67E44E9C670CFE490CAAC6551CA4BE01A270611A8F66C240":
                    case "75DFC4A3780C27DA1193E6C17B667CE2CD6E8659C431B87DD57C0D55FC3E75AF":
                    case "ACAAB38F88E4232CBEB75E5F5C37FD3316595305D33DD7AFC200050F36EDA591":
                    case "586A59E71A96A0131EDA4FEE78BCCA0B16205AC43B04F2FE6F5B17E189C15245":
                    case "359550759E108C5B372A7196D13E67A28F895E1FE2AC516E51EA2DE99B2D359D":
                    case "E1D170F1B703CF137145E0014AF97F5D3C3508918FA0530EBBCA15D0D8E22817":
                    case "002F03F28D0896A7DDEFBF71E6D1D37CD59DF253A963A4411B93B5BD030CC50B":
                    case "612069364AFAA5D412C5CADAA017933A33D1D1AED79ACA9AF23832BE53CCA03F":
                    case "60F6A51E5C7C594BAC73C5D2C1A16BB46064EFCAC486106AA3EC6F38349650AC":
                    case "FF7DB96C57A5324EDF28B98F9D03D66A4A7636E9829D4BA6E2E1107A9B3A3250":
                    case "C75895DF8369DFE92A4A6184E8C3C2819A2411B5DC59351662EF991DCD7CF363":
                    case "9ADFB4CE2EB4A4DFCE3FD6BC1F485AE854CDA4C92A7E45B2F0D876EE64B0C792":
                    case "594A42D60A0633BC1022EAF7480AA9AF5547ECC20784324574D65908D73BAF58":
                    case "26F0137612F09E4E939A2DBFA16C2E24D0F9E974613EC080D35B183C0223C7DD":
                    case "8156999FB4A975FCB0BFAE98D798D9D088549BAF0907ABDEC74AB2E52EC53513":
                    case "CA8C235F2E2CCE7D48B102524C03B5E8EC861CACB1E2C1E5512C7872027DB4A6":
                        msg = "XRP withdrawal from BTCMarkets to Binance.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "159B871BE2C8A2500709F86B871E7EDCE1AD2E10F1DB4B82995FB429BC125DA9":
                        msg = "XRP withdrawal from BTCMarkets to FTX.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "0x4a88fe127ebd4fe99b688671def8195ec250f7e391d2bcfa49532cb67fa977ba": // all to 0x0e15a8e0e91814b8b66c0146b823a48105318e88
                    case "0x857af90a97b4ad5c476c3aafaa9ced2037c4e95f0a2ef6ffffd4ccf2a1b72009":
                    case "0x38fe7a7ae80c6a2279f9fc8f149cc818da8dd70d97a828c809569b72f562a2b8":
                    case "0x21f541de24a418a84e2e2e51cf46b2d197e527e7b7d1cbd82ec5a873a85ab339":
                    case "0xb05ccffe62ce82bfb0cc70506afbfb503f7549521f21d09790694f4ffccfaecb":
                    case "0xe882e66bdbbfc11fb600c2e8ed994b82215f3514fb74184e331fb5371a5af940":
                    case "0xa0e6b6c58dbf05712b4ec75837a17c45d01ed7e9819a35ab97cc32ac900994e8":
                    case "0x2adde4b6603d4ac3f0927d03474b7c63bed725d948ba81b89581998d81862b86":
                    case "0x77237782a3a99be066e581d4c2826699913d34f2c19d07980285ae6da277f9a4":
                        msg = "ETC withdrawal from BTCMarkets to some wallet, probably Binance.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "0xb69eeec3f7211cd5531cacc7d899964e41976fbc2aebb245caa34d5f412f358c":
                    case "0xf819039f8c12de10bb5b378492ef0f252977339aa670a9dac56caac1cb4e6284":
                    case "0xa3e190a92864aeb01175e202e9e7f09a922ca2b4427d9ff71d23a70d40d97b9f":
                        msg = "ETH withdrawal from BTCMarkets to FTX.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;
                    #endregion

                    #region Binance
                    case "b406f495ba1565e84b70ce0c87c3bdac6faa4ef7f3ec109f6823113ff8a671d2":
                        // this was around mid 2018 (while playing around with StrayaCoin I remember finding an old bitcoin software wallet that still had funds - maybe it's from that? either way, it's only 0.025 BTC)
                        // comes from the 14uYKTh2JLJwdEzNESSnVPyMuE3b6Vyswh address - emptied wallet that used to be associated with coinspot and probably bittrade
                        msg = "BTC deposit to Binance - maybe from old wallet?";
                        status = TransactionWhitelistStatus.Whitelisted_Warning;
                        break;
                    #endregion

                    #region OnChain (ETH Mainnet)
                    case "0x260d2a57e5fc6f797b21085267c52037327e028cfb5d805ead0b0195b9e5a0d4":
                        msg = "Approved AMPL for trading on Uniswap V3";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x7bd86fbcfe20e66dc20c5e89abc2ea8bdc20c2e63aca548364f1bd0c6fe8409d":
                        msg = "Approved AMPL for trading on Uniswap V2";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x61db32b4897c64985d386d5469a388cb267c649f8c3cd1ea2ebaa02a5ca00385":
                    case "0x3c1076ca87012150c3f078f24d8e24057d5d45301ae7cf744db1bea887270899":
                        msg = "Spam transaction";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;
                    #endregion

                    #region OnChain (Arbitrum Nova)
                    case "0x44591ffdb278b699da1a311b1310026c50ba410b27a7bc8d62a175ab173fa8b2":
                    case "0x9dd87229b78b29c19929f35fedef1b5913fae1d4195bc9726dcb8e5890309c89":
                        msg = "Failed transaction";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0x4d907b6157091f33b863add4fd8e86195433371c856a9521d48421d55238e98b":
                        msg = "Approved MOON for trading on Sushiswap";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;

                    case "0xef83406e4c8d9442def3a400d1cdaefdc006fb7d9500bb10e6687c40a46f9d46":
                        msg = "Spam transaction";
                        status = TransactionWhitelistStatus.Whitelisted_Explained;
                        break;
                    #endregion

                    default:
                        // this will cause an exception later on
                        break;
                }
            }

            // if we couldn't find the transaction using the public id, try the internal id
            if (internalId.HasValue && !status.HasValue)
            {
                switch (internalId.Value)
                {
                    #region MercatoX and DDEX
                    case "e25f6899fe0cc112ced334a3c8b5bfebde811f450c09fce1ae8944f196233356":
                        msg = "Failed withdrawal from MercatoX";
                        status = TransactionWhitelistStatus.Soft_Blocked;
                        break;
                    #endregion

                    #region BTCMarkets
                    case "1228787114":
                        msg = "BTC deposit from unknown wallet. Currently created out of thin air.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "1055931346":
                        msg = "XRP deposit to BTCMarkets from unkonwn wallet. Currently created out of thin air.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;

                    case "1985937987":
                    case "1986280771":
                        // 11/7/2018
                        // can't find where it's coming from. was it an airdrop maybe? searched trades and transactions (from dto cache), and there's also nothing.
                        msg = "OMG deposit (amount of 0.08739418) from unknown wallet. Currently created out of thin air.";
                        status = TransactionWhitelistStatus.Whitelisted_Warning;
                        break;

                    case "4803122063":
                    case "4812803978":
                    case "4818368131":
                        msg = "BTC deposit into BTCMarkets from unknown wallet. Currently created out of thin air.";
                        status = TransactionWhitelistStatus.Assert_Unreachable;
                        break;
                    #endregion

                    default:
                        // this will cause an exception later on
                        break;
                }
            }

            if (!status.HasValue)
            {
                msg = "Whitelist for transaction not found";
                status = Constants.DRY_RUN ? TransactionWhitelistStatus.Whitelisted_Warning : TransactionWhitelistStatus.Hard_Blocked;
            }

            return (status.Value, msg);
        }

        private static DString printTxSummary(TransactionDTO tx)
        {
            return $"{tx.Exchange} {tx.Type} ({tx.ContainerEvent.AmountReceived}{tx.ContainerEvent.AmountSent} {tx.ContainerEvent.CurrencyReceived}{tx.ContainerEvent.CurrencySent}): {tx.PublicTransactionId.Value ?? (tx.InternalTransactionId.Value + "[internal]")}";
        }
    }
}
