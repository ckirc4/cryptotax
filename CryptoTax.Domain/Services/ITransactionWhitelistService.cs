﻿using CryptoTax.DTO;

namespace CryptoTax.Domain.Services
{
    public interface ITransactionWhitelistService
    {
        /// <summary>
        /// Should only check transactions that couldn't be matched to a Transfer object (i.e. those with an unknown/third-party source/destination). Returns true if the transaction should be processed, false otherwise.
        /// </summary>
        /// <exception cref="BlockedTransactionException"/>
        bool VerifyWhitelist(TransactionDTO tx);
    }
}