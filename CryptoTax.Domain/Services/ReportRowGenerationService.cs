﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class ReportRowGenerationService
    {
        private readonly IdProviderService _IdProviderService;

        private static readonly ReportRowCategory Action = ReportRowCategory.Action;
        private static readonly ReportRowCategory State = ReportRowCategory.State;
        private static readonly ReportRowCategory Environment = ReportRowCategory.Environment;
        private static readonly ReportRowCategory Admin = ReportRowCategory.Admin;

        // to make the reporting output more implementation agnostic, we may want to substitute some type names with more common words.
        public const string INVENTORY_NAME = "group";
        public const string INVENTORY_NAME_PLURAL = "groups";
        public const string TAX_CURRENCY_AMOUNT_NAME = "parcel"; // an amount of currency used in the tax sense, usually in the context of inventory state changes.
        public const string GENERAL_CURRENCY_AMOUNT_NAME = "amount"; // an amount of currency used in the general sense. note that it doesn't usually interact with
                                                                     // tax CAs, e.g. you don't split tax CA into a general CA.
        public const string CURRENCY_AMOUNT_NAME_PLURAL = "parcels";
        public const string PHANTOM_NAME = "unidentified";
        public const string RUN_NAME = "report";
        public const string STANDARD_CAPITAL_GAINS_NAME = "standard capital gains";
        public const string DISCOUNTED_CAPITAL_GAINS_NAME = "discounted capital gains";
        public const string CAPITAL_LOSSES_NAME = "capital losses";

        /// <summary>
        /// For each ReportRowType, represents all combos (messages made up from a combination of ordered row types) and the associated message invoker (whose .Invoke method can be called with the data types that correspond to the combo).
        /// </summary>
        private static readonly Dictionary<ReportRowType, Dictionary<ReportRowType[], RowInvoker>> RowCombos = new Dictionary<ReportRowType, Dictionary<ReportRowType[], RowInvoker>>()
        {
            // YUCK!
            // update: upon further consideration, this whole notion of "combos" is utterly ridiculous and this whole section is completely over-engineered. one data per row. nothing wrong with that, no need to combine rows. especially how we have row categorisation now.
            // but I will leave this here as a warning to what can happen if things are not properly thought through, and because it is a somewhat impressive bit of engineering.
            {
                ReportRowType.Split, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Split }, new RowInvoker((i, d) => generateSplitRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Merge, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Merge }, new RowInvoker((i, d) => generateMergeRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Add, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Add }, new RowInvoker((i, d) => generateAddRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Remove, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Remove }, new RowInvoker((i, d) => generateRemoveRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Purchase, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Purchase }, new RowInvoker((i, d) => generatePurchaseRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Dispose, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Dispose }, new RowInvoker((i, d) => generateDisposeRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Create, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Create }, new RowInvoker((i, d) => generateCreateRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Burn, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Burn }, new RowInvoker((i, d) => generateBurnRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Transfer, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Transfer }, new RowInvoker((i, d) => generateTransferRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Convert, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Convert }, new RowInvoker((i, d) => generateConvertRow(i, d))
                    }
                }
            },
            {
                ReportRowType.Price, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.Price }, new RowInvoker((i, d) => generatePriceRow(i, d))
                    }
                }
            },
            {
                ReportRowType.StartRun, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.StartRun }, new RowInvoker((i, d) => generateStartRunRow(i, d))
                    }
                }
            },
            {
                ReportRowType.EndRun, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.EndRun }, new RowInvoker((i, d) => generateEndRunRow(i, d))
                    }
                }
            },
            {
                ReportRowType.LoadInventories, new Dictionary<ReportRowType[], RowInvoker>()
                {
                    {
                        new [] { ReportRowType.LoadInventories }, new RowInvoker((i, d) => generateLoadInventoriesRow(i, d))
                    }
                }
            }
        };

        public ReportRowGenerationService(IdProviderService idProvider)
        {
            this._IdProviderService = idProvider;
        }

        public static IEnumerable<ReportRowType[]> GetAllowedCombos(ReportRowType type)
        {
            return RowCombos[type].Keys;
        }

        public static bool IsValidCombo(ReportRowType[] combo)
        {
            if (combo.Length == 0) return false;

            return GetAllowedCombos(combo[0]).Any(c => c.SequenceEqual(combo));
        }

        /// <summary>
        /// Returns true ONLY IF there there are combos that begin with the given sequence, and are longer.
        /// </summary>
        public static bool IsPartialCombo(ReportRowType[] combo)
        {
            if (combo.Length == 0) return false;

            return GetAllowedCombos(combo[0]).Any(c =>
                c.Length > combo.Length &&
                c.Take(combo.Length).SequenceEqual(combo));
        }

        /// <summary>
        /// Generate one or more messages from the provided data
        /// </summary>
        /// <exception cref="ArgumentException">Thrown if the data array corresponds to an invalid combo.</exception>
        public ReportRowDTO GenerateRow(params ReportingBaseData[] data)
        {
            ReportRowType baseType = data[0].Type;
            ReportRowType[] combo =
                // out of all the possible combos
                GetAllowedCombos(baseType)
                // get the first combo that matches with the provided data typ
                .FirstOrDefault(c => c.SequenceEqual(data.Select(d => d.Type)))
                ?.ToArray();

            if (combo == null || combo.Length != data.Length)
            {
                throw new ArgumentException($"Invalid combo. Please ensure you provide the correct ordered reporting data types based on {nameof(ReportRowGenerationService.GetAllowedCombos)}");
            }

            RowInvoker invoker = getInvoker(combo);
            return invoker.Invoke(this._IdProviderService, data);
        }

        /// <summary>
        /// Assumes combo is a valid combo.
        /// </summary>
        private static RowInvoker getInvoker(ReportRowType[] combo)
        {
            return RowCombos[combo[0]].First(kvp => kvp.Key.SequenceEqual(combo)).Value;
        }

        /// <summary>
        /// E.g. "10.5 BTC valued at 100 AUD"
        /// </summary>
        private static DString generateCaSummary(CurrencyAmount ca, bool includeValue)
        {
            // make sure we don't write nonsense like "50 AUD valued at 50 AUD"
            if (ca.Currency == KnownCurrencies.AUD.Symbol) includeValue = false;

            string message = $"{decimalToString(ca.Amount)} {ca.Currency}";
            if (includeValue)
            {
                message += $" valued at {decimalToString(ca.Value)} AUD";
            }
            return message;
        }

        /// <summary>
        /// E.g. "10.5 BTC"
        /// </summary>
        private static DString printCaAmountAndCurrency(CurrencyAmount ca)
        {
            return $"{decimalToString(ca.Amount)} {ca.Currency}";
        }

        private static DString writeExchange(CurrencyAmount ca)
        {
            return writeExchange(ca.ExchangeAcquired);
        }

        private static DString writeExchange(Exchange? exchange)
        {
            return exchange == null ? "unknown exchange" : $"exchange {exchange}";
        }

        private static DString decimalToString(decimal value)
        {
            string valueStr = value.ToString();
            if (!valueStr.Contains('.')) return valueStr; // is int

            string str = value.ToString().TrimEnd('0').TrimEnd('.');
            return string.IsNullOrEmpty(str) ? "0" : str;
        }

        private static ReportRowDTO generateSplitRow(IdProviderService idProvider, ReportingSplitData data)
        {
            return standardRowWithMessage(idProvider, data, State, $"Splitting {TAX_CURRENCY_AMOUNT_NAME} {data.From.Id} into {data.To1.Id} ({generateCaSummary(data.To1, true)}) and {data.To2.Id} ({generateCaSummary(data.To2, true)})");
        }

        private static ReportRowDTO generateMergeRow(IdProviderService idProvider, ReportingMergeData data)
        {
            return standardRowWithMessage(idProvider, data, State, $"Merging {TAX_CURRENCY_AMOUNT_NAME} {data.From1.Id} and {data.From2.Id} into {data.To.Id} ({generateCaSummary(data.To, true)})");
        }

        private static ReportRowDTO generateAddRow(IdProviderService idProvider, ReportingAddData data)
        {
            string message = $"Added {TAX_CURRENCY_AMOUNT_NAME} {data.CurrencyAmount.Id} to {INVENTORY_NAME} {data.Inventory.Id}";

            if (data.OriginalAmount != null)
            {
                message += $" as part of transferring {GENERAL_CURRENCY_AMOUNT_NAME} {data.OriginalAmount.Value.Id}";
            }

            return standardRowWithMessage(idProvider, data, State, message);
        }

        private static ReportRowDTO generateRemoveRow(IdProviderService idProvider, ReportingRemoveData data)
        {
            string message;
            if (data.IsPhantomRemoval)
            {
                // must print summary, since no "phantom" currency amount will have been referenced (and thus "intialised") before.
                message = $"Removed {PHANTOM_NAME} {TAX_CURRENCY_AMOUNT_NAME} ({generateCaSummary(data.RemovedAmount, true)}) from {INVENTORY_NAME} {data.Inventory.Id}";
            }
            else
            {
                message = $"Removed {TAX_CURRENCY_AMOUNT_NAME} {data.RemovedAmount.Id} from {INVENTORY_NAME} {data.Inventory.Id}";
            }

            if (data.OriginalAmount != null)
            {
                string removeType = data.CapitalGainsDelta == null ? (data.IsTransfer ? "transferring" : "discarding") : "disposing";
                message += $" as part of {removeType} {GENERAL_CURRENCY_AMOUNT_NAME} {data.OriginalAmount.Value.Id}";
            }
            return standardRowWithMessage(idProvider, data, State, message);
        }

        private static ReportRowDTO generatePurchaseRow(IdProviderService idProvider, ReportingPurchaseData data)
        {
            return standardRowWithMessage(idProvider, data, Action, $"Purchasing {GENERAL_CURRENCY_AMOUNT_NAME} {data.PurchasedAmount.Id} ({generateCaSummary(data.PurchasedAmount, true)}) from {writeExchange(data.PurchasedAmount)}");
        }

        private static ReportRowDTO generateDisposeRow(IdProviderService idProvider, ReportingDisposeData data)
        {
            string message = $"Disposing {GENERAL_CURRENCY_AMOUNT_NAME} {data.DisposedAmount.Id} ({generateCaSummary(data.DisposedAmount, true)}) from {writeExchange(data.DisposedAmount)}";

            if (data.FeeAmount.HasValue && data.FeeAmount.Value.Value != 0)
            {
                // need to identify fees by id since they are referred to again during the subsequent burn (removal from inventory)
                message += $", with fee {data.FeeAmount.Value.Id} ({generateCaSummary(data.FeeAmount.Value, false)})";
            }

            switch (data.DisposalType)
            {
                case DisposalType.SpotSale:
                    message += " due to spot sale";
                    break;
                case DisposalType.FuturesPnlLoss:
                    message += " due to futures loss";
                    break;
                case DisposalType.OptionsPnlLoss:
                    message += " due to options loss";
                    break;
                case DisposalType.FuturesFundingPayment:
                    message += " due to perpetual futures funding";
                    break;
                case DisposalType.DustConversionPayment:
                    message += " due to small assets (dust) conversion to BNB";
                    break;
                default:
                    throw new AssertUnreachable(data.DisposalType);
            }

            return standardRowWithMessage(idProvider, data, Action, message);
        }

        private static ReportRowDTO generateTransferRow(IdProviderService idProvider, ReportingTransferData data)
        {
            string message = $"Transferring {GENERAL_CURRENCY_AMOUNT_NAME} {data.CurrencyAmount.Id} ({printCaAmountAndCurrency(data.CurrencyAmount)})";
            
            if (data.From.Id != data.To.Id)
            {
                message += $" from {INVENTORY_NAME} {data.From.Id} ({writeExchange(data.From.Exchange)}) to {data.To.Id} ({writeExchange(data.To.Exchange)})";
            }
            else
            {
                message += $" from {INVENTORY_NAME} {data.From.Id} to itself";
            }

            if (data.Duration.HasValue && data.Duration.Value > TimeSpan.Zero)
            {
                IEnumerable<(int value, string label)> pairs = data.Duration.Value.ToString(@"d\.h\.m\.s\.fff").Split('.').Zip(new[] { "day", "hour", "minute", "second", "millisecond" }, (valueStr, label) =>
                {
                    int value = int.Parse(valueStr);
                    if (value != 1) label += "s";
                    return (value, label);
                }).Where(kvp => kvp.value != 0);

                // e.g. 3 days, 1 hour, and 5 seconds
                string timeSpanString = string.Join(", ", pairs.Select((kvp, i) =>
                {
                    string prefix = i == pairs.Count() - 1 && pairs.Count() > 1 ? "and " : "";
                    return $"{prefix}{kvp.value} {kvp.label}";
                }));
                message += $" over a duration of {timeSpanString}";
            }
            else
            {
                message += " instantly";
            }

            // need to identify fees by id since they are referred to again during the subsequent burn (removal from inventory)
            if (data.WithdrawalFees.GetValueOrDefault().Amount != 0)
            {
                message += $", with withdrawal fee {data.WithdrawalFees.Value.Id} ({generateCaSummary(data.WithdrawalFees.Value, false)})";
            }
            if (data.DepositFees.GetValueOrDefault().Amount != 0)
            {
                message += (data.WithdrawalFees?.Value != 0) ? " and " : ", with ";
                message += $"deposit fee {data.DepositFees.Value.Id} ({generateCaSummary(data.DepositFees.Value, false)})";
            }

            if (data.TransactionId.HasValue)
            {
                message += $" (transaction id: {data.TransactionId.Value})";
            }

            return standardRowWithMessage(idProvider, data, Action, message);
        }
        
        private static ReportRowDTO generateConvertRow(IdProviderService idProvider, ReportingConversionData data)
        {
            string reason;
            switch (data.Reason)
            {
                case ConvertReason.FtxStablecoinWithdrawal:
                    reason = "FTX stablecoin withdrawal from";
                    break;
                case ConvertReason.FtxStablecoinDeposit:
                    reason = "FTX stablecoin deposit to";
                    break;
                default:
                    throw new AssertUnreachable(data.Reason);
            }

            string message = $"To accomodate the {reason} {INVENTORY_NAME} {data.Inventory.Id}, {GENERAL_CURRENCY_AMOUNT_NAME} {data.InjectedAmount.Id} ({generateCaSummary(data.InjectedAmount, true)}) is swapped out with the existing {GENERAL_CURRENCY_AMOUNT_NAME} {data.DiscardedAmount.Id} ({generateCaSummary(data.DiscardedAmount, false)})";

            return standardRowWithMessage(idProvider, data, Action, message);
        }

        private static ReportRowDTO generateCreateRow(IdProviderService idProvider, ReportingCreateData data)
        {
            string message = $"Receiving {GENERAL_CURRENCY_AMOUNT_NAME} {data.CreatedAmount.Id} ({generateCaSummary(data.CreatedAmount, true)})";

            // only mention exchange if we know it in some way
            if (data.CreatedAmount.ExchangeAcquired != null)
            {
                message += $"on {writeExchange(data.CreatedAmount)}";
            }
            else if (data.RelevantCA?.ExchangeAcquired != null)
            {
                message += $"on {writeExchange(data.RelevantCA.Value)}";
            }

            switch (data.Reason)
            {
                case CreateReason.FuturesFundingIncome:
                    message += " from futures funding";
                    break;
                case CreateReason.FuturesPnlIncome:
                    message += " from futures PnL";
                    break;
                case CreateReason.OptionsPnlIncome:
                    message += " from options PnL";
                    break;
                case CreateReason.StakingIncome:
                    message += " from staking";
                    if (data.RelevantCA.HasValue) message += $" {printCaAmountAndCurrency(data.RelevantCA.Value)}";
                    break;
                case CreateReason.Fork:
                    if (data.RelevantCA.HasValue) message += $" from the forking of {printCaAmountAndCurrency(data.RelevantCA.Value)}";
                    else message += " from fork";
                    break;
                case CreateReason.LoanInterestIncome:
                    message += " in loan interest";
                    if (data.RelevantCA.HasValue) message += $" from lending out {printCaAmountAndCurrency(data.RelevantCA.Value)}";
                    break;
                case CreateReason.DustConversionIncome:
                    message += " from doing a small assets (dust) conversion to BNB";
                    break;
                case CreateReason.BankDeposit:
                    message += " from bank account";
                    break;
                case CreateReason.None:
                    break;
                case CreateReason.TransferWithExternalSource:
                    message += " as part of a transfer from an external source";
                    break;
                case CreateReason.DepositWithExternalSource:
                    message += " as part of a deposit from an external source";
                    break;
                default:
                    throw new AssertUnreachable(data.Reason);
            }

            return standardRowWithMessage(idProvider, data, Action, message);
        }

        private static ReportRowDTO generateBurnRow(IdProviderService idProvider, ReportingBurnData data)
        {
            // burned amount may have value, but it is meaningless to include it in the report.
            string message = $"Discarding {GENERAL_CURRENCY_AMOUNT_NAME} {data.BurnedAmount.Id} ({generateCaSummary(data.BurnedAmount, false)}) from {writeExchange(data.BurnedAmount)}";

            switch (data.Reason)
            {
                case BurnReason.Disposal:
                    if (data.RelevantCA.HasValue) message += $" as part of disposing {GENERAL_CURRENCY_AMOUNT_NAME} {data.RelevantCA.Value.Id}";
                    else message += " as part of a disposal";
                    break;
                case BurnReason.Transfer:
                    if (data.RelevantCA.HasValue) message += $" as part of transferring {GENERAL_CURRENCY_AMOUNT_NAME} {data.RelevantCA.Value.Id}";
                    else message += " as part of a transfer";
                    break;
                case BurnReason.BankDepositFee:
                    if (data.RelevantCA.HasValue) message += $" as part of depositing {GENERAL_CURRENCY_AMOUNT_NAME} {data.RelevantCA.Value.Id}";
                    else message += " as part of a deposit";
                    break;
                case BurnReason.FuturesFundingPayment:
                    message += " as part of a futures funding payment";
                    break;
                case BurnReason.FuturesTradeFee:
                    message += " as fees of a futures trade";
                    break;
                case BurnReason.OptionsTradeFee:
                    message += " as fees of an options trade";
                    break;
                case BurnReason.BankWithdrawal:
                    message += " as funds have been moved to a bank account";
                    break;
                case BurnReason.None:
                    break;
                case BurnReason.TransferWithExternalDestination:
                    message += " as funds are transferred to an external destination";
                    break;
                case BurnReason.WithdrawalWithExternalDestination:
                    message += " as funds are withdrawn to an external destination";
                    break;
                default:
                    throw new AssertUnreachable(data.Reason);
            }

            return standardRowWithMessage(idProvider, data, Action, message);
        }

        private static ReportRowDTO generatePriceRow(IdProviderService idProvider, ReportingPriceData data)
        {
            IEnumerable<string> stringifiedChain = data.Chain.Select(pair =>
            {
                Exchange exchange = pair.symbol.Exchange;
                GenericSymbol symbol = pair.symbol.Symbol;
                decimal price = pair.price;
                return $"{decimalToString(price)} {symbol.QuoteCurrency} per 1 {symbol.BaseCurrency} on {exchange}";
            });
            return standardRowWithMessage(idProvider, data, Environment, $"The current exchange rates are: {string.Join("; ", stringifiedChain)}");
        }

        private static ReportRowDTO generateStartRunRow(IdProviderService idProvider, ReportingStartRunData data)
        {
            return standardRowWithMessage(idProvider, data, Admin, $"Starting {RUN_NAME} {data.RunId} for the financial year {data.FinancialYear.Name}");
        }

        private static ReportRowDTO generateEndRunRow(IdProviderService idProvider, ReportingEndRunData data)
        {
            return standardRowWithMessage(idProvider, data, Admin, $"Ending {RUN_NAME} {data.RunId} with {STANDARD_CAPITAL_GAINS_NAME} of {decimalToString(data.CapitalGains.StandardCapitalGains)} AUD, {DISCOUNTED_CAPITAL_GAINS_NAME} of {decimalToString(data.CapitalGains.DiscountableCapitalGains)} AUD, and {CAPITAL_LOSSES_NAME} of {decimalToString(data.CapitalGains.CapitalLosses)} AUD resulting in total capital gains of {decimalToString(data.CapitalGains.GetTotalGainsOrLosses())} AUD, and with an extra assessable income of {decimalToString(data.AssessableIncome)} AUD for the financial year {data.FinancialYear.Name}");
        }

        private static ReportRowDTO generateLoadInventoriesRow(IdProviderService idProvider, ReportingLoadInventoriesData data)
        {
            string inventoryString = string.Join("; ", data.Inventories.Select(inv =>
            {
                IEnumerable<CurrencyAmount> currencyAmounts = inv.GetCurrentInventory().SelectMany(kvp => kvp.Value);

                string currencyAmountsString = string.Join(", ", currencyAmounts.Select((ca, i) =>
                {
                    string prefix = "";
                    if (i == 0)
                    {
                        prefix = currencyAmounts.Count() == 1 ? $"{TAX_CURRENCY_AMOUNT_NAME} " : $"{CURRENCY_AMOUNT_NAME_PLURAL} ";
                    }
                    return $"{prefix}{ca.Id} ({generateCaSummary(ca, true)})";
                }));

                return $"{INVENTORY_NAME} {inv.Id} containing {currencyAmountsString}";
            }));

            return standardRowWithMessage(idProvider, data, Admin, $"Loading the following {INVENTORY_NAME_PLURAL} from {RUN_NAME} {data.LoadedFromRunId}: {inventoryString}");
        }

        private static ReportRowDTO standardRowWithMessage(IdProviderService idProvider, ReportingBaseData data, ReportRowCategory category, DString message)
        {
            CapitalGainsDelta? delta = data.GetCapitalGainsDelta();

            return new ReportRowDTO(
                idProvider.GetNext(IdType.ReportRow),
                data.Time,
                category,
                new[] { data.Type },
                message,
                delta?.StandardCapitalGains,
                delta?.DiscountableCapitalGains,
                delta?.CapitalLosses,
                data.GetAssessableIncomeDelta());
        }
    }

    public struct RowInvoker
    {
        public delegate ReportRowDTO Callable(IdProviderService idProvider, params dynamic[] data);
        public delegate ReportRowDTO Callable_1Args(IdProviderService idProvider, dynamic data);
        public delegate ReportRowDTO Callable_2Args(IdProviderService idProvider, dynamic data1, dynamic data2);
        public delegate ReportRowDTO Callable_3Args(IdProviderService idProvider, dynamic data1, dynamic data2, dynamic data3);
        public delegate ReportRowDTO Callable_4Args(IdProviderService idProvider, dynamic data1, dynamic data2, dynamic data3, dynamic data4);
        public delegate ReportRowDTO Callable_5Args(IdProviderService idProvider, dynamic data1, dynamic data2, dynamic data3, dynamic data4, dynamic data5);

        private readonly Callable _Callable;

        private RowInvoker(Callable callable)
        {
            this._Callable = (i, d) => callable(i, d);
        }
        public RowInvoker(Callable_1Args callable)
        {
            this._Callable = (i, d) => callable(i, d[0]);
        }
        public RowInvoker(Callable_2Args callable)
        {
            this._Callable = (i, d) => callable(i, d[0], d[1]);
        }
        public RowInvoker(Callable_3Args callable)
        {
            this._Callable = (i, d) => callable(i, d[0], d[1], d[2]);
        }
        public RowInvoker(Callable_4Args callable)
        {
            this._Callable = (i, d) => callable(i, d[0], d[1], d[2], d[3]);
        }
        public RowInvoker(Callable_5Args callable)
        {
            this._Callable = (i, d) => callable(i, d[0], d[1], d[2], d[3], d[4]);
        }

        public ReportRowDTO Invoke(IdProviderService idProvider, params ReportingBaseData[] data)
        {
            if (data.Length == 1) return this._Callable(idProvider, data[0].Transform());
            if (data.Length == 2) return this._Callable(idProvider, data[0].Transform(), data[1].Transform());
            if (data.Length == 3) return this._Callable(idProvider, data[0].Transform(), data[1].Transform(), data[2].Transform());
            if (data.Length == 4) return this._Callable(idProvider, data[0].Transform(), data[1].Transform(), data[2].Transform(), data[3].Transform());
            if (data.Length == 5) return this._Callable(idProvider, data[0].Transform(), data[1].Transform(), data[2].Transform(), data[3].Transform(), data[4].Transform());

            throw new Exception("Provided data array has unexpected number of elements");
        }
    }
}
