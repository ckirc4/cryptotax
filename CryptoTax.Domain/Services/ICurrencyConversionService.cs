﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public interface ICurrencyConversionService
    {
        /// <summary>
        /// Get the current price of both of the currencies of the given pair, and given the price (ratio) of the pair.
        /// </summary>
        Task<(decimal quoteCurrencyValue, decimal baseCurrencyValue)> GetAudUnitValueAsync(ExchangeSymbol symbol, decimal price, DateTime time);

        /// <summary>
        /// Get the current price of the specified currency. Unsafe, as only certain currencies are supported. Automatically determines best exchange to use - if that fails, and an exchange has been provided, attempts to use exchange to determine the most likely symbol.
        /// </summary>
        Task<decimal> GetSingleAudUnitValueAsync(DString currency, DateTime time, Exchange? exchangeFallback);

        /// <summary>
        /// Returns the most efficient chain of trading pairs, possibly across exchanges, that needs to be traversed to be able to convert the given currency to AUD.
        /// First: starting symbol (currency may be quote or base). Last: symbol with a quote currency in AUD.
        /// </summary>
        IEnumerable<ExchangeSymbol> GetChainToAud(ExchangeSymbol symbol);

        /// <summary>
        /// Attempts to guess the best exchange symbol (for price data purposes) that this currency is part of. If none are found and a fallback exchange is provided, attempts to use the fallback exchange, otherwise returns null.
        /// </summary>
        ExchangeSymbol? TryGetExchangeSymbolFromCurrency(DString currency, Exchange? exchangeFallback);
    }
}
