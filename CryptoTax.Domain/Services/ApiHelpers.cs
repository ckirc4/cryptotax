﻿using CryptoTax.Domain.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Shared.Services;
using UtilityLibrary.Types;
using CryptoTax.Shared.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Shared;
using System.Threading;

namespace CryptoTax.Domain.Services
{
    public static class ApiHelpers
    {
        private static DateTime _LastRequest = DateTime.MinValue;
        private static readonly SemaphoreSlim _AsyncLock = new SemaphoreSlim(1, 1);

        /// <summary>
        /// Makes a rate-limited GET request to the specified URL with the given query parameters.
        /// </summary>
        public static async Task<dynamic> MakeGetRequestAsync(IWebService webService, DString url, Dictionary<string, string> queryParameters = null, int? minMsSinceLastRequest = null)
        {
            DString fullUrl = GenerateFullUrl(url, queryParameters);

            // if specified, await time until request is allowed
            await _AsyncLock.WaitAsync(); // only one thread may enter the below area at the same time
            if (minMsSinceLastRequest != null)
            {
                double msSinceLastRequest = DateTime.Now.Subtract(_LastRequest).TotalMilliseconds;
                if (msSinceLastRequest < minMsSinceLastRequest)
                {
                    // Console.WriteLine("Waiting for " + (minMsSinceLastRequest - msSinceLastRequest).ToString());
                    await Task.Delay(TimeSpan.FromMilliseconds((double)minMsSinceLastRequest - msSinceLastRequest));
                }
            }

            _LastRequest = DateTime.Now;
            _AsyncLock.Release(); // don't want to lock during the actual request
            string result = await webService.DownloadStringAsync(fullUrl);

            if (result[0] == '[' && result[result.Length - 1] == ']')
            {
                return JArray.Parse(result);
            }
            else
            {
                return JObject.Parse(result);
            }
        }

        public static DString GenerateFullUrl(DString baseUrl, Dictionary<string, string> queryParameters)
        {
            DString query = queryParameters != null && queryParameters.Any() ? string.Join("&", queryParameters.Select(kvp => $"{kvp.Key}={kvp.Value}")) : "";
            DString fullUrl = $"{baseUrl}{(string.IsNullOrEmpty(query) ? "" : $"?{query}")}";
            return fullUrl;
        }

        /// <summary>
        /// Gets the symbols of the specified exchange. Uses the cache if symbols already exist, and sets the cache otherwise.
        /// </summary>
        /// <param name="url">The URL to call, excluding query parameters.</param>
        public static async Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync(IWebService webService, ExchangeCache cache, Exchange exchange, IExchangeApiAdapter adapter, string url, int? minMsSinceLastRequest = null)
        {
            if (cache.HasSymbols(exchange)) return cache.GetSymbols(exchange);

            dynamic response = await MakeGetRequestAsync(webService, url, null, minMsSinceLastRequest);
            IEnumerable<dynamic> results = adapter.ParseSymbolCollection(response);
            IEnumerable<ExchangeSymbol> symbols = results.Select(adapter.ParseSymbol);

            cache.SetSymbols(exchange, symbols);
            return symbols;
        }

        /// <summary>
        /// Gets the candlesticks of specific interval for the given time period. Uses the cache for existing candlesticks, and updates the cache otherwise. Candlesticks are returned in order with no gaps.
        /// </summary>
        /// <param name="url">The URL to call, excluding query parameters.</param>
        /// <param name="getOnly">If true, does not save results to the exchange cache.</param>
        /// <exception cref="NoPriceDataException"></exception>
        public static async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(IWebService webService, IRegisteredLogger logger, ExchangeCache cache, Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime fromUTC, DateTime toUTC, IExchangeApiAdapter adapter, int candlestickLimit, string url, bool getOnly, int? minMsSinceLastRequest = null)
        {
            if (symbol.Name == null) throw new ArgumentNullException("Symbol name must be provided, as it is used to poll the API");

            // ensure our times are sanitised, else the API will throw an error
            fromUTC = CandlestickHelpers.GetStartTime(interval, fromUTC);
            toUTC = CandlestickHelpers.GetStartTime(interval, toUTC);

            IEnumerable<Candlestick> candlesticks = await getAllCandlesticksInRange(webService, logger, cache, exchange, symbol, interval, fromUTC, toUTC, adapter, candlestickLimit, url, minMsSinceLastRequest, false);
            if (candlesticks.First().StartTime > fromUTC || candlesticks.Last().StartTime < toUTC || candlesticks.Count() < CandlestickHelpers.GetIntervalsBetween(fromUTC, toUTC, interval) + 1) throw new Exception("The candlesticks that were retrieved are insufficient to cover the requested range and/or are not ordered correctly.");

            if (getOnly)
            {
                // already ordered, but may have more than we asked for
                return candlesticks.Where(c => c.StartTime >= fromUTC && c.StartTime <= toUTC);
            }
            else
            {
                // potentially add extra to cache
                cache.SetCandlesticks(exchange, symbol, interval, candlesticks);

                // get only what we need - this is now guaranteed to be a contiguous collection
                return cache.GetCandlesticks(exchange, symbol, interval, fromUTC, toUTC);
            }
        }

        /// <summary>
        /// Gets a sequence of candlesticks from the API and cache with from <= fromUTC and to = toUTC. Internal and trailing "blanks" (unavailable candlesticks according to the API response) are filled in automatically by carrying over the previous candlestick's close price.
        /// If there is a leading blank and ignoreLeadingBlanks is false, we will make additional requests, shifting the request range lower until we find a candlestick.
        /// Then, if ignoreLeadingBlanks is true, we fill in all internal and trailing blanks and return those candlesticks, but ignore the leading blanks.
        /// </summary>
        /// <exception cref="NoPriceDataException"></exception>
        private static async Task<IEnumerable<Candlestick>> getAllCandlesticksInRange(
            IWebService webService,
            IRegisteredLogger logger,
            ExchangeCache cache,
            Exchange exchange,
            ExchangeSymbol symbol,
            CandlestickInterval interval,
            DateTime fromUTC,
            DateTime toUTC,
            IExchangeApiAdapter adapter,
            int candlestickLimit,
            string url,
            int? minMsSinceLastRequest,
            bool ignoreLeadingBlanks // if true, it means we are already recursive and looking for a candlestick. if we find one, pick the earliest one of the lot, and fill in internal/trailing blanks.
        )
        {
            // firstly, check if there are any entries in the MissingData lookup between fromUTC and toUTC (both inclusive) - if so, we can throw the NoPriceData exception immediately
            if (cache.IncludesKnownMissingData(symbol, fromUTC, toUTC, out DateTime? unknownDataUntilTime))
            {
                NoPriceDataException e = new NoPriceDataException("No price data exists (from missing data persistor)", fromUTC);
                e.AvailableDataForRequest = new List<Candlestick>();
                e.EarliestTimeWithData = null;

                if (unknownDataUntilTime.Value < toUTC)
                {
                    // missing data does not span the complete range of the request. we assume this exact request has already been made,
                    // and therefore the other data should already be cached.
                    DateTime existingDataFrom = CandlestickHelpers.GetNextTime(interval, unknownDataUntilTime.Value);
                    DateTime existingDataTo = toUTC;
                    e.AvailableDataForRequest = cache.GetCandlesticks(exchange, symbol, interval, existingDataFrom, existingDataTo).ToList();
                    e.EarliestTimeWithData = e.AvailableDataForRequest.Any() ? e.AvailableDataForRequest.First().StartTime : (DateTime?)null;
                    int expectedCount = CandlestickHelpers.GetIntervalsBetween(existingDataFrom, existingDataTo, interval) + 1;
                    if (e.AvailableDataForRequest.Count() != expectedCount)
                    {
                        throw new Exception("Had persisted that no price data exists for a part of the requested range, but no data was persisted for the remainder of the range.", e);
                    }
                }

                throw e;
            }

            // the previous candlestick is special: if it is null and we have leading blanks, we must make another call to get the next range of candlesticks.
            // if it is not null and we have leading blanks, we can use it to fill in those blanks.
            Candlestick? previousCandlestick = cache.GetCandlestick(exchange, symbol, interval, CandlestickHelpers.GetPreviousTime(interval, fromUTC));

            IEnumerable<IEnumerable<DateTime>> missingTimesGroups = cache.GetMissingCandlestickTimesGrouped(exchange, symbol, interval, fromUTC, toUTC);
            IEnumerable<Candlestick> candlesticks = new List<Candlestick>();

            // add each group to the cache.
            // at first it may be tempting to "optimise" this by always getting the max number of candlesticks at once (since this does not cost more api points)
            // but it turns out this leads to a LOT of unnecessary data and much longer request times.
            // this is why BulkCandlestickService already does some grouping of nearby candlestick requests.
            foreach (IEnumerable<DateTime> missingTimes in EnforceMaximumGroupSize(missingTimesGroups, candlestickLimit))
            {
                Dictionary<string, string> parameters = adapter.CreateCandlestickParameters(interval, symbol, missingTimes);
                logger.Log(LogMessage.MSG_API_INFO_GET_MISSING_CANDLESTICKS, missingTimes.Count(), missingTimes.First(), interval, symbol.Name, exchange);
                dynamic response = await adapter.ObserveResponse(() => MakeGetRequestAsync(webService, url, parameters, minMsSinceLastRequest), url, parameters);
                IEnumerable<dynamic> results = adapter.ParseCandlestickCollection(response);
                IEnumerable<Candlestick> candlesticksInGroup = results.Select(candle => (Candlestick)adapter.ParseCandlestick(candle, interval, symbol));
                candlesticks = candlesticks.Concat(candlesticksInGroup);
            }

            IEnumerable<Candlestick> existingCandlesticks = cache.GetCandlesticks(exchange, symbol, interval, fromUTC, toUTC);
            candlesticks = candlesticks.Concat(existingCandlesticks).OrderBy(c => c.StartTime);

            // at this point, we might have gaps in the candlesticks
            // possible problem: gaps could be from missingTimes being separated - we must also check cache!
            candlesticks = fillInInternalAndTrailingBlanks(candlesticks, toUTC, previousCandlestick, cache);
            if (!ignoreLeadingBlanks)
            {
                try
                {
                    // this could result in a lot of new candlesticks - but that's ok! we will simply add them to the cache, and maybe we can make use of them one day
                    candlesticks = await fillInLeadingBlanks(candlesticks, webService, logger, cache, exchange, symbol, interval, fromUTC, toUTC, adapter, candlestickLimit, url, minMsSinceLastRequest);
                }
                catch (NoPriceDataException e)
                {
                    // don't bother caching if there is no data at all
                    if (!e.EarliestTimeChecked.HasValue) throw e;

                    // add to lookup that there is no candlestick data between fromUTC (inclusive) and candlesticks.First().Previous()
                    DateTime noDataFrom = e.EarliestTimeChecked.Value;
                    DateTime noDataTo = candlesticks.Any() ? candlesticks.First().GetPreviousTime() : toUTC;
                    cache.AddKnownMissingData(symbol, noDataFrom, noDataTo);

                    DateTime? earliestTimeWithData = candlesticks.Any() ? candlesticks.First().StartTime : (DateTime?)null;
                    e.EarliestTimeWithData = earliestTimeWithData;
                    e.AvailableDataForRequest = candlesticks;
                    throw e;
                }
            }

            return candlesticks;
        }

        /// <summary>
        /// Fills in internal and trailing blanks within the time range from = candlesticks.First().StartTime and to = toUTC using data from the cache (if defined) or, if the cache does not have the required value, by carrying over candlesticks. If previousCandlestick is defined, uses this one as the "zero" candlestick and from = previousCandlestick.GetNextTime(). Returns a time-ordered collection of candlesticks with no blanks between from and to.
        /// Assumes that all candlesticks are of the same type and in order.
        /// </summary>
        private static IEnumerable<Candlestick> fillInInternalAndTrailingBlanks(IEnumerable<Candlestick> candlesticks, DateTime toUTC, Candlestick? previousCandlestick, ExchangeCache cache)
        {
            // if the missing candlestick is internal (i.e. there are other sticks on either side, temporally) or at the end (i.e. there are other sticks before it), we can use the latest available close that comes before the candlestick as its open and close (carry over price).

            if (!candlesticks.Any() && previousCandlestick == null) return candlesticks;

            if (previousCandlestick != null)
            {
                candlesticks = new List<Candlestick>() { (Candlestick)previousCandlestick }.Concat(candlesticks);
            }
            List<Candlestick> newCandlesticks = new List<Candlestick>();
            LinkedList<Candlestick> list = new LinkedList<Candlestick>(candlesticks);
            LinkedListNode<Candlestick> current = list.First;
            DateTime fromUTC = current.Value.StartTime;
            CandlestickInterval interval = current.Value.Interval;
            DateTime nextTime;

            // going in steps of next time, fill in blanks
            do
            {
                Candlestick currentCandlestick = current.Value;
                DateTime currentTime = currentCandlestick.StartTime;
                nextTime = CandlestickHelpers.GetNextTime(interval, currentTime);

                // stop at the next non-blank candlestick or the end
                DateTime stopAtTime = current.Next != null ? current.Next.Value.StartTime : CandlestickHelpers.GetNextTime(interval, toUTC);
                while (nextTime < stopAtTime)
                {
                    // the cache may have a value for the blank - if so, use that one
                    Candlestick? cacheCandlestick = cache?.GetNextCandlestick(currentCandlestick);
                    currentCandlestick = cacheCandlestick ?? new Candlestick(currentCandlestick);
                    if (currentCandlestick.StartTime != nextTime) throw new Exception("Candlestick and nextTime are out of sync");
                    newCandlesticks.Add(currentCandlestick);

                    // iterate to the next candlestick slot
                    nextTime = CandlestickHelpers.GetNextTime(interval, nextTime);
                }

                // done for this loop. check that everything is fine
                current = current.Next;
                if (nextTime > toUTC && current != null || nextTime <= toUTC && current == null)
                {
                    throw new Exception("Something went wrong while filling in internal and trailing blanks.");
                }
            } while (nextTime <= toUTC);

            // first candlestick is previousCandlestick, which is not part of the sequence of candlesticks between to and from
            if (previousCandlestick != null) candlesticks = candlesticks.Skip(1);

            // combine new with existing, in order
            candlesticks = candlesticks.Concat(newCandlesticks).OrderBy(c => c.StartTime);

            // verify that we have not made a mistake
            if (candlesticks.GroupBy(c => c.StartTime).Select(g => g.Count()).Any(groupSize => groupSize != 1)) throw new Exception("Tried to fill in internal/trailing candlestick blanks, but ended up with duplicates.");
            else if (candlesticks.First().StartTime != (previousCandlestick == null ? fromUTC : CandlestickHelpers.GetNextTime(interval, fromUTC)) || candlesticks.Last().StartTime != toUTC) throw new Exception("Tried to fill in internal/trailing candlestick blanks, but ended up with times beyond the expected range.");

            return candlesticks;
        }

        /// <summary>
        /// Given a candlestick collection with from >= fromUTC and to = UTC with no internal or trailing blanks, returns a candlestick sequence with from <= fromUTC and to = toUTC with no blanks.
        /// </summary>
        /// <exception cref="NoPriceDataException"></exception>
        private static async Task<IEnumerable<Candlestick>> fillInLeadingBlanks(IEnumerable<Candlestick> candlesticks, IWebService webService, IRegisteredLogger logger, ExchangeCache cache, Exchange exchange, ExchangeSymbol symbol, CandlestickInterval interval, DateTime fromUTC, DateTime toUTC, IExchangeApiAdapter adapter, int candlestickLimit, string url, int? minMsSinceLastRequest, int recursiveDepth = 0)
        {
            // make sure we don't go too deep or too far back in time - if so, assume there is no price data and let the caller deal with the mess.
            TimeSpan timeSpan = new TimeSpan(CandlestickHelpers.IntervalToTicks(interval) * recursiveDepth * candlestickLimit);
            if (recursiveDepth > 100 || recursiveDepth > 5 && timeSpan.TotalDays > 100)
            {
                // recursive depth - 1 because we are never getting data for the current depth
                DateTime earliestRangeCheckedTo = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, -1 - (candlestickLimit * (recursiveDepth - 1)));
                DateTime earliestRangeCheckedFrom = CandlestickHelpers.GetTimeAfterIntervals(interval, earliestRangeCheckedTo, -(candlestickLimit - 1));
                throw new NoPriceDataException($"Could not fetch leading candlestick blanks before {fromUTC.ToString("dd/MM/yyyy HH:mm")} of interval type {interval}", recursiveDepth - 1, earliestRangeCheckedFrom);
            }

            IEnumerable<Candlestick> allCandlesticks = candlesticks;

            if (!candlesticks.Any() || candlesticks.First().StartTime > fromUTC)
            {
                // shift from and to downwards and get an earlier range of candlesticks
                DateTime toUTC_Shifted = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, -1 - (candlestickLimit * recursiveDepth));
                DateTime fromUTC_Shifted = CandlestickHelpers.GetTimeAfterIntervals(interval, toUTC_Shifted, -(candlestickLimit - 1));

                // optimisation: if we are checking a previous chunk for which we already have data in the cache, reduce the chunk size (i.e. start later) so that it covers only the missing data afterwards - at that point we also KNOW that we will not have to go deeper (well, one more level, but it will just return the candlesticks back to us).
                IEnumerable<IEnumerable<DateTime>> missingTimes = cache.GetMissingCandlestickTimesGrouped(exchange, symbol, interval, fromUTC_Shifted, toUTC_Shifted);
                if (missingTimes.Any())
                {
                    IEnumerable<DateTime> lastGroup = missingTimes.Last();
                    if (lastGroup.Last() != toUTC_Shifted) throw new Exception("Something went wrong");
                    DateTime firstMissing = lastGroup.First();
                    if (firstMissing < fromUTC_Shifted) throw new Exception("Something went wrong");
                    fromUTC_Shifted = firstMissing; // fromUTC_Shifted may now occur later, thus saving some work!
                }

                logger.Log(LogMessage.MSG_API_DEBUG_FILL_IN_LEADING_CANDLESTICKS, CandlestickHelpers.GetIntervalsBetween(fromUTC_Shifted, toUTC_Shifted, interval) + 1, recursiveDepth, fromUTC_Shifted, toUTC_Shifted, symbol.Name, exchange);
                IEnumerable<Candlestick> prevRangeCandlesticks = await getAllCandlesticksInRange(webService, logger, cache, exchange, symbol, interval, fromUTC_Shifted, toUTC_Shifted, adapter, candlestickLimit, url, minMsSinceLastRequest, true);
                allCandlesticks = prevRangeCandlesticks.Concat(candlesticks);

                // recursive!
                // this means that the only way we can proceed past this line is if we have a candlestick that comes somewhere BEFORE the original start time.
                // we don't care about leading blanks here, so this is desirable.
                // allCandlesticks is either extended (downwards, if there it is deemed that there are trailing blanks) or returned as-is (if there are no trailing blanks)
                allCandlesticks = await fillInLeadingBlanks(allCandlesticks, webService, logger, cache, exchange, symbol, interval, fromUTC, toUTC, adapter, candlestickLimit, url, minMsSinceLastRequest, recursiveDepth + 1);

                if (recursiveDepth == 0)
                {
                    // now that we have collected potentially several ranges worth of candlesticks, it's possible that we have many blanks internally - fix these
                    // (for the record, these arise from the fact that we keep concatenating ranges with leading blanks, which do not get filled in by the getAllCandlestickInRange function because, at that point, we don't know what to fill them in with)
                    // no point in checking them all during every recursive call, so just wait until the end. also, no point using the cache because we know that they don't exist in the cache, otherwise they wouldn't be blanks at this point
                    allCandlesticks = fillInInternalAndTrailingBlanks(allCandlesticks, toUTC, null, null);

                    // verify that we have not made a mistake
                    if (allCandlesticks.GroupBy(c => c.StartTime).Select(g => g.Count()).Any(groupSize => groupSize != 1)) throw new Exception("Tried to fill in leading candlestick blanks, but ended up with duplicates.");
                    else if (allCandlesticks.First().StartTime > fromUTC || allCandlesticks.Last().StartTime != toUTC) throw new Exception("Tried to fill in leading candlestick blanks, but ended up with times beyond the expected range.");
                }
            }
            else
            {
                // there are no leading blanks!
                // at this point we may or may not be deep in recursion. either way, we will not do anything and simply return the collection to the caller.
            }

            return allCandlesticks;
        }

        /// <summary>
        /// Ensures that each inner collection contains at most the number of elements specified. Splits groups if required. Retains ordering.
        /// </summary>
        public static IEnumerable<IEnumerable<T>> EnforceMaximumGroupSize<T>(IEnumerable<IEnumerable<T>> groups, int maxGroupSize)
        {
            List<IEnumerable<T>> newGroups = new List<IEnumerable<T>>();
            foreach (IEnumerable<T> group in groups)
            {
                int N = group.Count();
                for (int i = 0; i < N / maxGroupSize + 1; i++)
                {
                    int startIndex = i * maxGroupSize;
                    if (startIndex >= N) continue; // this would add an empty list to the group
                    // if less than maxGroupSize are left to take, Take() will return the rest with no problems
                    newGroups.Add(group.Skip(startIndex).Take(maxGroupSize));
                }
            }

            return newGroups;
        }
    }
}
