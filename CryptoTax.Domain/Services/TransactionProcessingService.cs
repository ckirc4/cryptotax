﻿using CryptoTax.Domain.Models;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CryptoTax.Shared;
using UtilityLibrary.Types;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Stores;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Inventory;
using System.Linq;

namespace CryptoTax.Domain.Services
{
    public class TransactionProcessingService
    {
        private readonly IdProviderService _IdProviderService;
        private readonly ICurrencyConversionService _CurrencyConversionService;
        private readonly IReportingService _ReportingService;
        private readonly IInventoryManagerService _InventoryManagerService;
        private readonly TransferService _TransferService;
        private readonly TimeEventService _TimeEventService;
        private readonly AssessableIncomeStore _AssessableIncomeStore;
        private readonly ITransactionWhitelistService _TransactionWhitelistService;
        private readonly IBulkCandlestickService _BulkCandlestickService;
        private readonly IRegisteredLogger _Logger;
        private readonly IFtxStablecoinService _FtxStablecoinService;

        private bool _HasSetup;

        public TransactionProcessingService(IdProviderService idProviderService, ICurrencyConversionService currencyConversionService, IReportingService reportingService, IInventoryManagerService inventoryManagerService, TransferService transferService, TimeEventService timeEventService, AssessableIncomeStore assessableIncomeStore, ITransactionWhitelistService transferWhitelistService, IBulkCandlestickService bulkCandlestickService, ILoggingService loggingService, IFtxStablecoinService ftxStablecoinService)
        {
            this._IdProviderService = idProviderService;
            this._CurrencyConversionService = currencyConversionService;
            this._ReportingService = reportingService;
            this._InventoryManagerService = inventoryManagerService;
            this._TransferService = transferService;
            this._TimeEventService = timeEventService;
            this._AssessableIncomeStore = assessableIncomeStore;
            this._TransactionWhitelistService = transferWhitelistService;
            this._BulkCandlestickService = bulkCandlestickService;
            this._Logger = loggingService.Register(this, "TransactionProcessingService");
            this._FtxStablecoinService = ftxStablecoinService;

            this._HasSetup = false;
        }

        /// <summary>
        /// Collects all required candlesticks and connects transactions to generate transfers for later.
        /// </summary>
        public void Setup(IEnumerable<TransactionDTO> allTx)
        {
            if (this._HasSetup) throw new TransactionSetupException("Setup() has already been called");

            // collect transfers
            IEnumerable<Transfer> transfers = this._TransferService.DoTransferDiscovery(allTx);
            foreach (Transfer transfer in transfers)
            {
                this.scheduleTransfer(transfer);
            }

            // collect candlesticks where possible
            foreach (TransactionDTO tx in allTx)
            {
                if (tx.Type == TransactionType.FuturesFunding
                    || tx.Type == TransactionType.FuturesPnL
                    || tx.Type == TransactionType.OptionsPnL
                    || tx.Type == TransactionType.StakingOrAirdrops
                    || tx.Type == TransactionType.DustConversion)
                {
                    DString currency = tx.ContainerEvent.CurrencyReceived.Value ?? tx.ContainerEvent.CurrencySent.Value;
                    ExchangeSymbol? ExchangeSymbol = this._CurrencyConversionService.TryGetExchangeSymbolFromCurrency(currency, tx.Exchange);

                    if (ExchangeSymbol.HasValue)
                    {
                        // note that unlike TAX-66 we DO add the first symbol since we are going in completely "cold handed"
                        IEnumerable<ExchangeSymbol> chain = this._CurrencyConversionService.GetChainToAud(ExchangeSymbol.Value);
                        foreach (ExchangeSymbol symbol in chain)
                        {
                            this._BulkCandlestickService.AddRequired(symbol, tx.Time);
                        }
                    }
                    else
                    {
                        // it is entirely possible that ExchangeSymbol is null, e.g. if we know in advance that it doesn't exist. just ignore it here.
                    }
                }
            }

            this._HasSetup = true;
        }

        public async Task ProcessTransactionAsync(TransactionDTO tx)
        {
            if (!this._HasSetup) throw new TransactionSetupException("Before processing transactions, must first call Setup()");

            // we already have functions set up that are firing automatically
            if (this._TransferService.GetAssociatedTransfer(tx) != null) return;
            ContainerEventDTO ev = tx.ContainerEvent;

            if (ev.AmountReceived.HasValue != ev.CurrencyReceived.HasValue || ev.AmountSent.HasValue != ev.CurrencySent.HasValue || ev.FeeAmount.HasValue != ev.FeeCurrency.HasValue) throw new IncompleteTransactionException("Both an amount and a currency must be specified");
            else if (ev.AmountReceived.HasValue == ev.AmountSent.HasValue) throw new TransactionSideException("Transaction must be on one side (send or receive)");

            bool isPositive = ev.AmountReceived.HasValue;
            if (isPositive && ev.AmountReceived.Value <= 0) throw new TransactionOutOfRangeException("Amount received must be a positive number");
            else if (!isPositive && ev.AmountSent.Value < 0) throw new TransactionOutOfRangeException("Amount sent must be a positive number");
            else if (ev.FeeAmount.HasValue && ev.FeeAmount.Value < 0) throw new TransactionOutOfRangeException("Fee amount must be at least zero or null");

            // not everything will require fee handling - implement as we go along and need it
            bool handlesFees = 
                tx.Type == TransactionType.CryptoDeposit || 
                tx.Type == TransactionType.CryptoWithdrawal || 
                tx.Type == TransactionType.OnChainTransfer || 
                tx.Type == TransactionType.FiatDeposit || 
                tx.Type == TransactionType.FuturesPnL ||
                tx.Type == TransactionType.OptionsPnL;
            if (ev.FeeAmount.HasValue && !handlesFees) throw new NotImplementedException($"Currently does not handle fees for transactions of type {tx.Type}");

            DateTime time = ev.EventTime;
            Container container = ev.Container;
            DString currency = isPositive ? ev.CurrencyReceived.Value : ev.CurrencySent.Value;
            decimal amount = isPositive ? ev.AmountReceived.Value : ev.AmountSent.Value;
            Exchange exchange = tx.Exchange;
            CurrencyAmount ca(decimal value) => new CurrencyAmount(this._IdProviderService, currency, amount, value, time, exchange);

            switch (tx.Type)
            {
                // for the following, positive and negative refer to the flux of the funds relative to the container of interest, i.e. funds are created or burned.
                // todo: the current TransactionDTO model does not allow collecting additional data (e.g. relevantCA) - add extra fields to do this, such as Position (for futures) or forked currency and amount (not necessarily 1:1 fork)
                case TransactionType.FuturesFunding:
                case TransactionType.FuturesPnL:
                case TransactionType.OptionsPnL:
                    // any exception would be a critical problem, so allow them to bubble up
                    decimal pnlUnitValue = await this._CurrencyConversionService.GetSingleAudUnitValueAsync(currency, time, tx.Exchange);
                    decimal pnlTotalValue = amount * pnlUnitValue;
                    CurrencyAmount pnlCa = ca(pnlTotalValue);

                    if (isPositive) {
                        CreateReason reason;
                        if (tx.Type == TransactionType.FuturesFunding) reason = CreateReason.FuturesFundingIncome;
                        else if (tx.Type == TransactionType.FuturesPnL) reason = CreateReason.FuturesPnlIncome;
                        else if (tx.Type == TransactionType.OptionsPnL) reason = CreateReason.OptionsPnlIncome;
                        else throw new UnreachableCodeException($"Invalid transaction type '{tx.Type}'");
                        this.handlePositive(time, container, reason, pnlCa, null);
                    }
                    else
                    {
                        // for disposals of derivatives due to negative pnl, we don't associate fees directly with the disposal since it is treated as a transaction and not a trade.
                        // we also treat funding payments as pnl for consistency (since exchanges that automatically calculate pnl include funding in the result)
                        DisposalType type;
                        if (tx.Type == TransactionType.FuturesFunding) type = DisposalType.FuturesFundingPayment;
                        else if (tx.Type == TransactionType.FuturesPnL) type = DisposalType.FuturesPnlLoss;
                        else if (tx.Type == TransactionType.OptionsPnL) type = DisposalType.OptionsPnlLoss;
                        else throw new UnreachableCodeException($"Invalid transaction type '{tx.Type}'");
                        this.handleNegative_Disposal(time, container, type, pnlCa);
                    }

                    if (ev.FeeAmount.GetValueOrDefault() > 0)
                    {
                        BurnReason reason;
                        if (tx.Type == TransactionType.FuturesPnL) reason = BurnReason.FuturesTradeFee;
                        else if (tx.Type == TransactionType.OptionsPnL) reason = BurnReason.OptionsTradeFee;
                        else throw new UnreachableCodeException($"Invalid transaction type '{tx.Type}'");

                        CurrencyAmount feeCA = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, time);
                        this.handleNegative_Burn(time, container, reason, feeCA, pnlCa);
                    }
                    break;

                case TransactionType.StakingOrAirdrops:
                    if (!isPositive) throw new InvalidStakingSideTransactionException("Staking must always be positive");
                    // when trying to calculate the value of e.g. forked coin, there may not be any previous data (because the coin might not be trading yet at the time), so if currency conversion service throws the above exception, we know to assign a value of zero. perhaps this can be done automatically by taking another argument when getting AUD unit value, e.g. bool allowZeroIfNoData 
                    // special case: this is considered "income" and must be declared as such. [the difference between this and capital gains is that this income is not subject to CGT, and does not count towards gains/losses until it is sold]
                    decimal stakingUnitValue = 0;
                    try
                    {
                        stakingUnitValue = await this._CurrencyConversionService.GetSingleAudUnitValueAsync(currency, time, tx.Exchange);
                    }
                    catch (NoPriceDataException e)
                    {
                        this._Logger.Log(LogMessage.MSG_WARNING_AIRDROP_PRICE_NOT_FOUND, currency, e.Message);
                        // ignore
                    }
                    catch (InvalidSymbolException e)
                    {
                        // symbol no longer exists on Binance (and historical data of removed symbols is not allowed... thanks Binance!)
                        this._Logger.Log(LogMessage.MSG_WARNING_AIRDROP_PRICE_NOT_FOUND, currency, e.Message);
                    }
                    decimal stakingTotalValue = amount * stakingUnitValue;
                    this._AssessableIncomeStore.AddAssessableIncome(stakingTotalValue);
                    this.handlePositive(time, container, CreateReason.StakingIncome, ca(stakingTotalValue), null, stakingTotalValue);
                    break;

                case TransactionType.DustConversion:
                    // converting small amounts of other assets into BNB - similar treatment to futures PNL ("sell" dust at current price, then "buy" bnb at current price)
                    // if we can't get the price, throw for now
                    decimal dustUnitValue = await this._CurrencyConversionService.GetSingleAudUnitValueAsync(currency, time, tx.Exchange);
                    decimal dustTotalValue = amount * dustUnitValue;
                    CurrencyAmount dustCa = ca(dustTotalValue);

                    if (isPositive) this.handlePositive(time, container, CreateReason.DustConversionIncome, dustCa, null);
                    else this.handleNegative_Disposal(time, container, DisposalType.DustConversionPayment, dustCa);
                    break;

                case TransactionType.Fork:
                    if (!isPositive) throw new InvalidForkSideTransactionException("Forks must always be positive");
                    this.handlePositive(time, container, CreateReason.Fork, ca(0), null);
                    break;

                case TransactionType.LoanInterest:
                    if (!isPositive) throw new InvalidLoanSideTransactionException("Borrower side has not been implemented for loan interests");
                    this.handlePositive(time, container, CreateReason.LoanInterestIncome, ca(0), null);
                    break;

                case TransactionType.FiatDeposit:
                    if (!isPositive) throw new InvalidFiatSideTransactionException("Fiat deposits must always be positive");
                    if (currency != "AUD") throw new InvalidFiatCurrencyTransactionException("Fiat bank deposits only support AUD");

                    CurrencyAmount fiatDepositCA = ca(amount);
                    this.handlePositive(time, container, CreateReason.BankDeposit, fiatDepositCA, null);
                    if (ev.FeeAmount.GetValueOrDefault() > 0)
                    {
                        CurrencyAmount feeCA = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, time);
                        this.handleNegative_Burn(time, container, BurnReason.BankDepositFee, feeCA, fiatDepositCA);
                    }
                    break;

                case TransactionType.FiatWithdrawal:
                    if (isPositive) throw new InvalidFiatSideTransactionException("Fiat withdrawals must always be negative");
                    if (currency != "AUD") throw new InvalidFiatCurrencyTransactionException("Fiat bank withdrawals only support AUD");
                    this.handleNegative_Burn(time, container, BurnReason.BankWithdrawal, ca(0), null);
                    break;

                case TransactionType.CryptoDeposit:
                    if (!isPositive) throw new InvalidDepositSideTransactionException("Crypto deposits must always be positive");
                    if (!this._TransactionWhitelistService.VerifyWhitelist(tx)) return;

                    CurrencyAmount depositCA = ca(0);
                    this.handlePositive(time, container, CreateReason.DepositWithExternalSource, depositCA, null);
                    if (ev.FeeAmount.GetValueOrDefault() > 0)
                    {
                        CurrencyAmount feeCA = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, time);
                        this.handleNegative_Burn(time, container, BurnReason.Transfer, feeCA, depositCA);
                    }
                    break;

                case TransactionType.CryptoWithdrawal:
                    if (isPositive) throw new InvalidWithdrawalSideTransactionException("Crypto withdrawals must always be negative");
                    if (!this._TransactionWhitelistService.VerifyWhitelist(tx)) return;

                    CurrencyAmount withdrawalCA = ca(0);
                    this.handleNegative_Burn(time, container, BurnReason.WithdrawalWithExternalDestination, withdrawalCA, null);
                    if (ev.FeeAmount.GetValueOrDefault() > 0)
                    {
                        CurrencyAmount feeCA = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, time);
                        this.handleNegative_Burn(time, container, BurnReason.Transfer, feeCA, withdrawalCA);
                    }
                    break;

                case TransactionType.OnChainTransfer:
                    if (!this._TransactionWhitelistService.VerifyWhitelist(tx)) return;

                    CurrencyAmount transferCA = ca(0);
                    if (isPositive) this.handlePositive(time, container, CreateReason.TransferWithExternalSource, transferCA, null);
                    else this.handleNegative_Burn(time, container, BurnReason.TransferWithExternalDestination, transferCA, null);
                    if (ev.FeeAmount.GetValueOrDefault() > 0)
                    {
                        CurrencyAmount feeCA = new CurrencyAmount(this._IdProviderService, ev.FeeCurrency.Value, ev.FeeAmount.Value, 0, time);
                        this.handleNegative_Burn(time, container, BurnReason.Transfer, feeCA, transferCA);
                    }

                    break;

                default:
                    throw new AssertUnreachable(tx.Type);
            }
        }

        /// <summary>
        /// Schedules the transfer start and finish methods that should be called at the correct time.
        /// </summary>
        private void scheduleTransfer(Transfer transfer)
        {
            if (transfer.Transferred.Amount <= 0) throw new ArgumentException("Must transfer a positive amount");

            // since both callback methods need to share data, we define some variables outside of those functions
            DateTime start = transfer.Start;
            DateTime end = transfer.End;
            TimeSpan duration = end.Subtract(start);
            IEnumerable<CurrencyAmount> transferred = new List<CurrencyAmount>();
            CurrencyAmount toTransfer = transfer.Transferred;

            bool initialFtxStablecoinConversion = transfer.Source.Exchange == Exchange.FTX && KnownCurrencies.IsFtxStablecoin(toTransfer.Currency, false);
            bool finalFtxStablecoinConversion = transfer.Destination.Exchange == Exchange.FTX && KnownCurrencies.IsFtxStablecoin(toTransfer.Currency, false);

            // note: it is safe to get/create these inventories even if the transfer may happen later in the year because an inventory-container connection is immutable during the course of the year.
            IInventory from = this._InventoryManagerService.GetInventory(transfer.Source);
            IInventory to = this._InventoryManagerService.GetInventory(transfer.Destination);

            // generally we want to be unnecessarily verbose and do things by the book, however there is a special case where we transfer from one inventory to itself instantly in which we can skip the transfer
            bool requiresActualTransfer = !(from.Id == to.Id && duration == TimeSpan.Zero) || initialFtxStablecoinConversion || finalFtxStablecoinConversion;

            void onTransferStart()
            {
                this._ReportingService.AddData(new ReportingTransferData(this._IdProviderService, start, toTransfer, from, to, transfer.PublicId, transfer.FeeToSend, transfer.FeeToReceive, duration));

                Func<IEnumerable<CurrencyAmount>> doWithdrawal = () =>
                {
                    IEnumerable<CurrencyAmount> _transferred = new List<CurrencyAmount>();
                    if (requiresActualTransfer)
                    {
                        _transferred = from.TransferOut(toTransfer, TransferOutReason.Transfer, out decimal overdrawnAmount);
                        if (overdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, from.Id, overdrawnAmount, toTransfer.Currency, "transfer out");
                    }
                    return _transferred;
                };

                Action payFee = () =>
                {
                    if (transfer.FeeToSend.HasValue)
                    {
                        // action report
                        this._ReportingService.AddData(new ReportingBurnData(this._IdProviderService, start, transfer.FeeToSend.Value, BurnReason.Transfer, toTransfer));
                        IEnumerable<CurrencyAmount> _ = from.TransferOut(transfer.FeeToSend.Value, TransferOutReason.Burn, out decimal feeOverdrawnAmount);
                        if (feeOverdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, from.Id, feeOverdrawnAmount, transfer.FeeToSend.Value.Currency, "transfer out fee");
                    }
                };

                if (initialFtxStablecoinConversion)
                {
                    transferred = this._FtxStablecoinService.OnFtxWithdrawal(from, start, toTransfer, doWithdrawal, transfer.FeeToSend, payFee).ToList();
                }
                else
                {
                    // standard withdrawal
                    transferred = doWithdrawal().ToList();
                    payFee();
                }
            }

            void onTransferEnd()
            {
                Action doDeposit = () =>
                {
                    if (requiresActualTransfer)
                    {
                        foreach (CurrencyAmount amount in transferred)
                        {
                            to.TransferIn(amount, end);
                        }
                    }
                };

                Action payFee = () =>
                {
                    if (transfer.FeeToReceive.HasValue)
                    {
                        this._ReportingService.AddData(new ReportingBurnData(this._IdProviderService, end, transfer.FeeToReceive.Value, BurnReason.Transfer, toTransfer));
                        to.TransferOut(transfer.FeeToReceive.Value, TransferOutReason.Burn, out decimal overdrawnAmount);
                        if (overdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, to.Id, overdrawnAmount, transfer.FeeToReceive.Value.Currency, "transfer in fee");
                    }
                };

                if (finalFtxStablecoinConversion)
                {
                    this._FtxStablecoinService.OnFtxDeposit(to, end, transferred, doDeposit, transfer.FeeToReceive, payFee);
                }
                else
                {
                    // standard deposit
                    doDeposit();
                    payFee();
                }
            }

            this._TimeEventService.AddAction(onTransferStart, start, true);
            this._TimeEventService.AddAction(onTransferEnd, end, true);
        }

        private void handlePositive(DateTime time, Container container, CreateReason reason, CurrencyAmount income, CurrencyAmount? relevantCA, decimal? assessableIncome = null)
        {
            ReportingCreateData data;
            if (assessableIncome.HasValue)
            {
                data = new ReportingCreateData(this._IdProviderService, time, income, reason, relevantCA, assessableIncome.Value);
            }
            else if (relevantCA.HasValue)
            {
                data = new ReportingCreateData(this._IdProviderService, time, income, reason, relevantCA.Value);
            }
            else
            {
                data = new ReportingCreateData(this._IdProviderService, time, income, reason);
            }

            this._ReportingService.AddData(data);

            IInventory inv = this._InventoryManagerService.GetInventory(container);
            inv.TransferIn(income, time);
        }

        private void handleNegative_Burn(DateTime time, Container container, BurnReason reason, CurrencyAmount burnAmount, CurrencyAmount? relevantCA)
        {
            ReportingBurnData data = relevantCA.HasValue ? new ReportingBurnData(this._IdProviderService, time, burnAmount, reason, relevantCA.Value) : new ReportingBurnData(this._IdProviderService, time, burnAmount, reason);
            this._ReportingService.AddData(data);

            IInventory inv = this._InventoryManagerService.GetInventory(container);
            inv.TransferOut(burnAmount, TransferOutReason.Burn, out decimal overdrawnAmount);
            if (overdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, inv.Id, overdrawnAmount, burnAmount.Currency, $"{reason} burn");
        }

        private void handleNegative_Disposal(DateTime time, Container container, DisposalType type, CurrencyAmount disposal)
        {
            ReportingDisposeData data = new ReportingDisposeData(this._IdProviderService, time, disposal, type, null);
            this._ReportingService.AddData(data);

            IInventory inv = this._InventoryManagerService.GetInventory(container);
            inv.Disposal(disposal, 0, out decimal overdrawnAmount);
            if (overdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, inv.Id, overdrawnAmount, disposal.Currency, $"{type} disposal");
        }
    }
}
