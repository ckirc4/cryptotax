﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Inventory;

namespace CryptoTax.Domain.Services
{
    /// <summary>
    /// Translates containers into the correct inventories for the caller to manipulate.
    /// </summary>
    public class InventoryManagerService : IInventoryManagerService
    {
        private readonly IdProviderService _IdProviderService;
        private readonly IRunService _RunService;
        private readonly IInventoryFactory _InventoryFactory;
        private readonly IReportingService _ReportingService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;

        private IEnumerable<IInventory> _Inventories;

        /// <summary>
        /// As part of initialisation, automatically loads inventories from the RunService.
        /// </summary>
        public InventoryManagerService(IdProviderService idProviderService, IRunService runService, IInventoryFactory inventoryFactory, IReportingService reportingService, ILoggingService loggingService)
        {
            this._IdProviderService = idProviderService;
            this._RunService = runService;
            this._InventoryFactory = inventoryFactory;
            this._ReportingService = reportingService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "InventoryManagerService");

            this._Inventories = this._RunService.LoadBeginningInventories(this._InventoryFactory, this._ReportingService).ToList();
        }

        /// <summary>
        /// Gets the existing inventory associated with the given container, or creates a new one.
        /// <br/>
        /// <br/>
        /// Note: for the management type of OnePerUniqueTransferDestination, ensure that it makes sense for the provided container to be associated with an inventory (e.g. don't provide burn wallets or BankAccount), otherwise we will end up with a large number of "useless" inventories with which we will never interact again.
        /// </summary>
        public IInventory GetInventory(Container container)
        {
            IInventory inventory;
            InventoryManagementType type = this._RunService.GetInventoryManagementType();
            switch (type)
            {
                case InventoryManagementType.Single:
                    inventory = this.getPrimaryInventory();
                    break;
                case InventoryManagementType.OnePerExchangeType:
                    inventory = this.getExchangeInventory(container);
                    break;
                default:
                    throw new AssertUnreachable(type);
            }

            return inventory;
        }

        /// <summary>
        /// This should be called after the loaded inventories have changed in some way. It is optional, but MUST be called before saving inventories in the RunService.
        /// </summary>
        public void UpdateRunState()
        {
            this._RunService.UpdateCurrentInventories(this._Inventories);
        }

        public CapitalGains GetAggregateCapitalGains()
        {
            return CapitalGains.Sum(this._Inventories.Select(inv => inv.NetCapitalGains));
        }

        public void OnProgress(int progressCount, int N)
        {
            decimal percentDone = (decimal)progressCount / N * 100;
            int numInventories = this._Inventories.Count();
            int numItems = this._Inventories.Sum(inv => inv.GetCurrentInventory().Values.Sum(caCollection => caCollection.Count()));
            int numAssets = this._Inventories.SelectMany(inv => inv.GetCurrentInventory().Keys).Distinct().Count();
            decimal totalCG = CapitalGains.GetTotalGainsOrLosses(this._Inventories.Select(inv => inv.NetCapitalGains));

            this._Logger.Log(LogMessage.MSG_DEBUG_PROGRESS_UPDATE, progressCount, percentDone, numInventories, numItems, numAssets, totalCG);
        }

        /// <summary>
        /// We don't want to create inventories for every single type of container. Use this method to check BEFORE getting/creating an inventory.
        /// </summary>
        public bool IsAssociatedWithInventory(Container container)
        {
            switch (container.Type)
            {
                case ContainerType.OwnedWallet:
                    return true;
                case ContainerType.ThirdPartyWallet:
                    return false;
                case ContainerType.ExchangeWallet:
                    // it is likely that we want to transfer from ContainerType.OwnedWallet to ContainerType.ExchangeWallet, and an automatic transfer then deposits this in the ContainerType.ExchangeAccount - we could just take a shortcut and transfer directly from the OwnedWallet to the ExchangeAccount, but that is a concern of the TransferService
                    // refer to ideas put forth in TransferService
                    return false;
                case ContainerType.BankAccount:
                    return false;
                case ContainerType.ExchangeAccount:
                    return true;
                default:
                    throw new AssertUnreachable(container.Type);
            }
        }

        /// <summary>
        /// To be used when we are only using a single inventory.
        /// </summary>
        private IInventory getPrimaryInventory()
        {
            IEnumerable<IInventory> candidates = this._Inventories.Where(inv => inv.Exchange == Exchange.Undefined);
            if (candidates.Count() == 0) return this.createInventory(Exchange.Undefined, "Primary Inventory");
            else if (candidates.Count() == 1) return candidates.First();
            else throw new Exception("Cannot get primary inventory because there are multiple possible candidates currently loaded.");
        }

        private IInventory getExchangeInventory(Container container)
        {
            Exchange exchange;
            if (container.Exchange.HasValue)
            {
                exchange = container.Exchange.Value;
            }
            else
            {
                // must be wallet - just set to on-chain exchange
                exchange = Exchange.OnChain;
            }

            IEnumerable<IInventory> candidates = this._Inventories.Where(inv => inv.Exchange == exchange);
            if (candidates.Count() == 0) return this.createInventory(exchange, $"{exchange} Inventory");
            else if (candidates.Count() == 1) return candidates.First();
            else throw new Exception($"Cannot get exchange inventory because there are multiple possible candidates for exchange {exchange}.");
        }

        private IInventory createInventory(Exchange exchange, DString name)
        {
            IInventory inventory = this._InventoryFactory.Create(this._IdProviderService, this._ReportingService, this._LoggingService, exchange, name);
            this.withNewInventory(inventory);
            return inventory;
        }

        private DString getNameFromContainer(Container container)
        {
            return $"{container.Type} {container.Id}";
        }

        /// <summary>
        /// Call this whenever a new inventory was created - do not manually add it to the collection.
        /// </summary>
        private void withNewInventory(IInventory inventory)
        {
            if (this._Inventories.Contains(inventory) || this._Inventories.Where(inv => inv.Id == inventory.Id).Any() || this._Inventories.Where(inv => inv.Name == inventory.Name).Any()) throw new Exception($"Cannot create inventory with id {inventory.Id} and name {inventory.Name} because it already exists.");

            this._Inventories = this._Inventories.Append(inventory);
            this.UpdateRunState();
        }
    }
}
