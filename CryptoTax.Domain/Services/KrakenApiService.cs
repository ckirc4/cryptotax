﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class KrakenApiService : IExchangeApiService, IExchangeApiAdapter
    {
        private const string BASE_URL = "https://api.kraken.com/0";
        private const int REQUESTS_PER_SECOND = 1; // considering they don't even expose historical candlestick, this rate limit pisses me off
        private const int CANDLESTICK_LIMIT = 100; // kind of arbitrary...
        private readonly IWebService _WebService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;
        private readonly ExchangeCache _Cache;

        public const Exchange EXCHANGE = Exchange.Kraken;

        public KrakenApiService(IWebService webService, ILoggingService loggingService, ExchangeCache cache)
        {
            this._WebService = webService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "KrakenApiService");
            this._Cache = cache;
        }

        public async Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync()
        {
            // see https://docs.kraken.com/rest/#tag/Market-Data/operation/getTradableAssetPairs
            return await ApiHelpers.GetSymbolsAsync(this._WebService, this._Cache, EXCHANGE, this, BASE_URL + "/public/AssetPairs");
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly)
        {
            // see https://docs.kraken.com/rest/#tag/Market-Data/operation/getOHLCData
            // actually scrap that, they don't allow accessing historical data. instead, they want us to re-create candlesticks from trades lol
            // https://support.kraken.com/hc/en-us/articles/218198197-How-to-retrieve-historical-time-and-sales-trading-history-using-the-REST-API-Trades-endpoint-
            // e.g. https://api.kraken.com/0/public/Trades?pair=AUDUSD&since=1594211240000000000 (append 9 zeros to the end of the timestamp for consistency)
            var candles = await ApiHelpers.GetCandlesticksAsync(this._WebService, this._Logger, this._Cache, EXCHANGE, symbol, interval, fromUTC, toUTC, this, CANDLESTICK_LIMIT, BASE_URL + $"/public/Trades", getOnly);
            return candles;
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly)
        {
            DateTime toUTC = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, count - 1);
            return await this.GetCandlesticksAsync(interval, symbol, fromUTC, toUTC, getOnly);
        }

        public ExchangeSymbol ParseSymbol(dynamic obj)
        {
            /*
            Example object:
            {
	            "altname": "AUDUSD",
	            "wsname": "AUD/USD",
	            "aclass_base": "currency",
	            "base": "ZAUD",
	            "aclass_quote": "currency",
	            "quote": "ZUSD",
	            "lot": "unit",
	            "cost_decimals": 5,
	            "pair_decimals": 5,
	            "lot_decimals": 8,
	            "lot_multiplier": 1,
	            "leverage_buy": [],
	            "leverage_sell": [],
	            "fees": [...],
	            "fees_maker": [...],
	            "fee_volume_currency": "ZUSD",
	            "margin_call": 80,
	            "margin_stop": 40,
	            "ordermin": "10",
	            "costmin": "0.5",
	            "tick_size": "0.00001",
	            "status": "online"
            }
            */
            DString marketName = (string)obj.altname;

            // https://support.kraken.com/hc/en-us/articles/360001185506-How-to-interpret-asset-codes
            // kraken devs wanted to be special so they prepended SOME cryptos with X and SOME fiats with Z...
            // to more reliably get the currencies involved in this symbol, use the wsname which seems to be of the form <base>/<quote>.
            // what does ws stand for? we don't know and we don't care!
            string[] pair = ((string)obj.wsname).Split('/');
            if (pair.Length != 2) throw new Exception($"Cannot parse Kraken pair {obj.wsname} because it is formatted in an unexpected way.");
            DString baseAsset = pair[0];
            DString quoteAsset = pair[1];

            return new ExchangeSymbol(
                new GenericSymbol(
                    KnownCurrencies.TryGetCurrency(quoteAsset),
                    KnownCurrencies.TryGetCurrency(baseAsset)),
                EXCHANGE,
                marketName);
        }

        public Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            Candlestick candlestick = obj;
            candlestick.Symbol = symbol;
            return candlestick;

            /*
            Example object:
		    [
			    1694128560, // 0: timestamp (seconds)
			    "26233.3", // 1: open
			    "26235.2", // 2: high
			    "26224.9", // 3: low
			    "26224.9", // 4: close
			    "26230.0", // 5: vwap (?)
			    "16.50493830", // 6: volume
			    37 // 7: count (trade count?)
		    ]
            */
            // by default, DateTime objects are created within the local time zone
            //DateTime time = new DateTime().FromUnixSeconds((long)obj[0]);
            //decimal open = decimal.Parse((string)obj[1]);
            //decimal close = decimal.Parse((string)obj[4]);
            //return new Candlestick(
            //    time,
            //    interval,
            //    symbol,
            //    open,
            //    close);
        }

        public Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes)
        {
            // the `since` parameter is exclusive....
            long intervalSeconds =
                interval == CandlestickInterval.Minute_1 ? 60 :
                interval == CandlestickInterval.Hour_1 ? 3600 :
                interval == CandlestickInterval.Day_1 ? (3600 * 24) :
                throw new AssertUnreachable(interval);
            long since = sortedTimes.First().ToUnixSeconds() - intervalSeconds;

            Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    { "pair", symbol.Name },
                    {
                        "interval",
                        interval == CandlestickInterval.Minute_1 ? "1" :
                            interval == CandlestickInterval.Hour_1 ? "60" :
                            interval == CandlestickInterval.Day_1 ? "1440" :
                            throw new AssertUnreachable(interval) },
                    { "since", since.ToString() },

                    // hack for providing data to the ObserverResponse method
                    { "fromUTC", sortedTimes.First().ToString() },
                    { "toUTC", sortedTimes.Last().ToString() }
                };

            return parameters;
        }

        public IEnumerable<dynamic> ParseSymbolCollection(dynamic response)
        {
            /*
            Response model:
            {
              "error": [],
              "result": {
                "Symbol1": {...},
                "Symbol2": {...},
                ...
              }
            }
            */
            return (response.result as JObject).Properties().Select(prop => prop.Value);
        }

        public IEnumerable<dynamic> ParseCandlestickCollection(dynamic response)
        {
            return ((List<Candlestick>)response).Select(c => c as dynamic);

            /*
            Response model:
            {
                "error": [],
                "result": {
                    "AUDUSD": [
                        [...],
                        [...],
                        ...
                    ],
                "last": 12345689
                }
            }
            */
            // JObject inherits from JContainer which does explicitly implement IList<JToken>
            // do we need to cast result[0] to JValue?
            //return (response.result as JObject).Properties().Where(p => p.Name != "last").Single().Value;
        }

        public async Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters)
        {
            if (!url.EndsWith("Trades"))
            {
                return await apiCall();
            }

            // we have to manually construct candelsticks from the trade data. since we don't know how many requests are required, we will
            // do all the work in this method.
            /*
            Response model:
            {
	            "error": [],
	            "result": {
		            "AUDUSD": [
			            [
				            "0.69175", // price
				            "0.00001000", // volume
				            1594211315.783422, // timestamp (seconds)
				            "b", // b = buy, s = sell
				            "l", // m = market, l = limit
				            "", // misc
				            1606 // trade ID
			            ],
                        [...],
                        ...
		            ],
		            "last": "1594213687367916260" // use this as the `since` parameter in the next request to retrieve a continuous series of trades
	            }
            }
            */

            string pair = parameters["pair"];
            CandlestickInterval interval =
                parameters["interval"] == "1" ? CandlestickInterval.Minute_1 :
                parameters["interval"] == "60" ? CandlestickInterval.Hour_1 :
                parameters["interval"] == "1440" ? CandlestickInterval.Day_1 :
                throw new AssertUnreachable(parameters["interval"], "Invalid interval from parameters");
            string since = parameters["since"];
            DateTime fromUTC = DateTime.Parse(parameters["fromUTC"]);
            DateTime toUTC = DateTime.Parse(parameters["toUTC"]);

            // note: the symbol will be replaced later on, since we don't know it here
            CandlestickBuilder builder = new CandlestickBuilder(interval, fromUTC, toUTC, new ExchangeSymbol());
            bool keepGoing = true;
            while (keepGoing)
            {
                Dictionary<string, string> queryParams = new Dictionary<string, string>
                {
                    { "pair", pair },
                    { "since", since }
                };
                dynamic response = await ApiHelpers.MakeGetRequestAsync(this._WebService, BASE_URL + $"/public/Trades", queryParams, 1000 / REQUESTS_PER_SECOND);
                
                IEnumerable<JProperty> properties = (response.result as JObject).Properties();

                dynamic trades = properties.Where(p => p.Name != "last").Single().Value;
                bool requiresEarlierData = false;
                foreach (dynamic trade in trades)
                {
                    decimal price = (decimal)trade[0];
                    DateTime time = new DateTime().FromUnixSeconds((long)trade[2]);
                    if (!builder.hasCurrent() && time > fromUTC)
                    {
                        requiresEarlierData = true;
                        break;
                    }

                    if (builder.AddTrade(time, price))
                    {
                        keepGoing = false;
                        break;
                    }
                }

                if (requiresEarlierData)
                {
                    // just retrieve from one hour earlier
                    since = (long.Parse(since) - 3600).ToString();
                }
                else
                {
                    since = (string)properties.Where(p => p.Name == "last").Single().Value;
                }
            }

            return builder.GetResults();
        }
    }
}
