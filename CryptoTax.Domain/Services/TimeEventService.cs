﻿using CryptoTax.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Domain.Services
{
    /// <summary>
    /// Used to call methods at a given "time". Due to the discrete nature of how we handle times (only when processing an item), we will execute subscribed actions either right before or after and item will be/has been processed
    /// </summary>
    public class TimeEventService : ITimeEventService
    {
        private int _PrevId;
        private readonly Dictionary<long, ExecutionModel> _Actions;

        public TimeEventService()
        {
            this._PrevId = 0;
            this._Actions = new Dictionary<long, ExecutionModel>();
        }

        /// <summary>
        /// Call this just before processing an item.
        /// </summary>
        public void OnItemWillProcess(DateTime? previousTime, DateTime time)
        {
            if (previousTime.HasValue && previousTime.Value > time) throw new ArgumentException("Previous time cannot be after the current time.");
            this.execute(model => model.TriggerTime > (previousTime ?? DateTime.MinValue) && model.TriggerTime <= time);
        }

        /// <summary>
        /// Call this right after the last item was processed.
        /// </summary>
        public void OnLastItemDidProcess(DateTime time)
        {
            this.execute(model => model.ExecuteOnLastItem && model.TriggerTime > time);
        }

        /// <summary>
        /// Subscribes to the specified event type. Returns the unique id. Set executeAfterLastItem to true if the provided action (whose execution time is far in the future) should be executed after the last item has been processed.
        /// </summary>
        public long AddAction(Action action, DateTime executionTime, bool executeAfterLastItem)
        {
            long newId = this._PrevId++;
            ExecutionModel model = new ExecutionModel(action, executionTime, executeAfterLastItem);
            this._Actions.Add(newId, model);
            return newId;
        }

        /// <summary>
        /// Returns true if removal was successful.
        /// </summary>
        public bool TryRemoveAction(long id)
        {
            return this._Actions.Remove(id);
        }

        private void execute(Func<ExecutionModel, bool> selector)
        {
            IEnumerable<long> keys = this._Actions
                .Where(kvp => selector(kvp.Value))
                .OrderBy(kvp => kvp.Value.TriggerTime)
                .ThenBy(kvp => kvp.Key)
                .Select(kvp => kvp.Key);

            foreach (long key in keys)
            {
                if (this._Actions.ContainsKey(key))
                {
                    ExecutionModel model = this._Actions[key];
                    this._Actions.Remove(key);
                    model.Action();
                }
            }
        }
    }
}
