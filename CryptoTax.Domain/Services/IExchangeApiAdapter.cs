﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoTax.Domain.Services
{
    public interface IExchangeApiAdapter
    {
        /// <summary>
        /// Parses a dynamic JObject into a Candlestick struct.
        /// </summary>
        Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol);

        IEnumerable<dynamic> ParseSymbolCollection(dynamic response);
        IEnumerable<dynamic> ParseCandlestickCollection(dynamic response);

        /// <summary>
        /// Parses a dynamic JObject into a ExchangeSymbol struct.
        /// </summary>
        ExchangeSymbol ParseSymbol(dynamic obj);

        /// <summary>
        /// Creates a nullable dictionary of query parameters.
        /// </summary>
        /// <param name="interval">The interval of candlesticks to fetch.</param>
        /// <param name="sortedTimes">The starting times, in ascending order, of the candlesticks to fetch</param>
        Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes);

        /// <summary>
        /// Wrap this around API calls to handle exchange-specific errors.
        /// </summary>
        /// <exception cref="InvalidSymbolException" />
        Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters);
    }
}
