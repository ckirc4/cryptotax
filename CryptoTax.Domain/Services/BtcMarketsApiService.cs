﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class BtcMarketsApiService : IExchangeApiService, IExchangeApiAdapter
    {
        private const string BASE_URL = "https://api.btcmarkets.net/v3/";
        private const int REQUESTS_PER_10_SECONDS = 50;
        private const int MIN_MS_SINCE_LAST_REQUEST = 200; // 1000 / (REQUESTS_PER_10_SECONDS / 10);
        private const int CANDLESTICK_LIMIT = 1000;
        private readonly IWebService _WebService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;
        private readonly ExchangeCache _Cache;

        public const Exchange EXCHANGE = Exchange.BTCMarkets;

        public BtcMarketsApiService(IWebService webService, ILoggingService loggingService, ExchangeCache cache)
        {
            this._WebService = webService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "BtcMarketsApiService");
            this._Cache = cache;
        }

        public async Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync()
        {
            return await ApiHelpers.GetSymbolsAsync(this._WebService, this._Cache, EXCHANGE, this, BASE_URL + "markets", MIN_MS_SINCE_LAST_REQUEST);
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly)
        {
            return await ApiHelpers.GetCandlesticksAsync(this._WebService, this._Logger, this._Cache, EXCHANGE, symbol, interval, fromUTC, toUTC, this, CANDLESTICK_LIMIT, BASE_URL + $"markets/{symbol.Name}/candles", getOnly, MIN_MS_SINCE_LAST_REQUEST);
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly)
        {
            DateTime toUTC = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, count - 1);
            return await this.GetCandlesticksAsync(interval, symbol, fromUTC, toUTC, getOnly);
        }

        public ExchangeSymbol ParseSymbol(dynamic obj)
        {
            /*
            Example object:
                {
                "marketId": "XRP-AUD",
                "baseAssetName": "XRP",
                "quoteAssetName": "AUD",
                "minOrderAmount": "0.001",
                "maxOrderAmount": "1000000",
                "amountDecimals": "8",
                "priceDecimals": "4"
                }
            */
            DString quoteAsset = (string)obj.quoteAssetName;
            DString baseAsset = (string)obj.baseAssetName;
            DString marketName = (string)obj.marketId;
            return new ExchangeSymbol(
                new GenericSymbol(
                    KnownCurrencies.TryGetCurrency(quoteAsset),
                    KnownCurrencies.TryGetCurrency(baseAsset)),
                EXCHANGE,
                marketName);
        }

        public Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            /*
            Example object:
            [
                ["2020-12-15T00:00:00.000000Z", "0.6624", "0.6739", "0.6384", "0.6444", "1202951.52519389"],
                ["2020-12-14T00:00:00.000000Z", "0.6793", "0.6886", "0.6507", "0.661", "1623322.04829148"]
            ]

            -> [time, open, high, low, close, volume]
            */
            DateTime time = (DateTime)obj[0];
            decimal open = (decimal)obj[1];
            decimal close = (decimal)obj[4];
            return new Candlestick(
                time,
                interval,
                symbol,
                open,
                close);
        }

        public Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    {"timeWindow", interval == CandlestickInterval.Minute_1 ? "1m" : interval == CandlestickInterval.Hour_1 ? "1h" : interval == CandlestickInterval.Day_1 ? "1d" : throw new AssertUnreachable(interval) },
                    { "from", sortedTimes.First().ToIso8601() },
                    { "to", sortedTimes.Last().ToIso8601() }
                };

            return parameters;
        }

        public IEnumerable<dynamic> ParseSymbolCollection(dynamic response)
        {
            // response is a raw array
            return response as IEnumerable<dynamic>;
        }

        public IEnumerable<dynamic> ParseCandlestickCollection(dynamic response)
        {
            // response is a raw array
            return response as IEnumerable<dynamic>;
        }

        public async Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters)
        {
            return await apiCall();
        }
    }
}
