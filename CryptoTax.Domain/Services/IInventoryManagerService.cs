﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Shared;

namespace CryptoTax.Domain.Services
{
    public interface IInventoryManagerService
    {
        CapitalGains GetAggregateCapitalGains();
        IInventory GetInventory(Container container);
        void OnProgress(int progressCount, int N);
        void UpdateRunState();
    }
}