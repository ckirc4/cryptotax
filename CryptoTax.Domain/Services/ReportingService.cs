﻿using CryptoTax.Domain.Models.Reporting;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Domain.Services
{
    // the rationale of reporting is to have a human readable sequence of events that we can point to and say "look, here's my math!". They should show, step by step, what we are doing to calculate everything and how our state changes. It doesn't have to be pretty or easy to follow, but it does have to be exhaustive. 
    public class ReportingService : IReportingService
    {
        private readonly IRegisteredLogger _Logger;
        private readonly IRunService _RunService;
        private readonly IReportingPersistorService _ReportingPersistorService;
        private readonly ReportRowGenerationService _ReportRowGenerationService;

        // combos are valid only at the beginning of the sequence. as soon as a non-combo item is added, the potential combo is broken (and new, smaller ones might now exist at the beginning of the sequence).
        private IEnumerable<ReportingBaseData> _PendingRows = new List<ReportingBaseData>();

        /// <summary>
        /// File name should not have an extension.
        /// </summary>
        public ReportingService(ILoggingService loggingService, IRunService runService, IReportingPersistorService reportingPersistor, ReportRowGenerationService reportRowGenerationService)
        {
            this._Logger = loggingService.Register(this, "ReportingService");
            this._RunService = runService;
            this._ReportingPersistorService = reportingPersistor;
            this._ReportRowGenerationService = reportRowGenerationService;
        }

        /// <summary>
        /// If flushImmediately is true, reporting service will not wait for further data to try to generate a combo message.
        /// </summary>
        public void AddData(ReportingBaseData data, bool flushImmediately = false)
        {
            this.handleNewData(data, flushImmediately);
        }

        /// <summary>
        /// Writes all pending rows to the report.
        /// </summary>
        public void Flush()
        {
            while (this._PendingRows.Any())
            {
                this.tryWriteAllComboRows();
                if (!this._PendingRows.Any()) break;

                // we don't know if there are any *internal* combos, so we have to recheck every time we remove the beginning row
                ReportingBaseData data = this._PendingRows.First();
                this._PendingRows = this._PendingRows.Skip(1);
                ReportRowDTO row = this._ReportRowGenerationService.GenerateRow(data);
                this._ReportingPersistorService.AddRow(row);
            }

            this._ReportingPersistorService.Flush();
        }

        /// <summary>
        /// If writeImmediately is true, will not store the row and wait for a potential combo.
        /// </summary>
        private void handleNewData(ReportingBaseData data, bool writeImmediately)
        {
            if (!this._RunService.GetEnableReporting()) return;

            if (writeImmediately || true)
            {
                ReportRowDTO row = this._ReportRowGenerationService.GenerateRow(data);
                if (row.Category == ReportRowCategory.Action)
                {
                    this._Logger.Log(LogMessage.MSG_DEBUG_REPORT_ROW, row.Id);
                }
                this._ReportingPersistorService.AddRow(row);
                return;
            }
            else
            {
                this._PendingRows = this._PendingRows.Append(data);
                this.tryWriteAllComboRows();
                return;
            }
        }

        /// <summary>
        /// Deprecated.
        /// <br/>
        /// Tries to write all combo rows contained in the current pending data. Always starts using the first item, and looks for the biggest combo possible. Modifies this._PendingRows.
        /// </summary>
        /// <returns></returns>
        private void tryWriteAllComboRows()
        {
            throw new NotImplementedException("tryWriteAllComboRows() is a deprecated method");
#pragma warning disable CS0162 // Unreachable code detected
            if (!this._PendingRows.Any()) return;
#pragma warning restore CS0162 // Unreachable code detected

                // legacy code to handle combos: for some reason, .Take() and .Skip() is extremely slow and results in a possible memory leak (application gets slower and slower)
            if (ReportRowGenerationService.IsPartialCombo(this._PendingRows.Select(c => c.Type).ToArray()))
            {
                // do nothing, since we may eventually be able to get a longer combo
                return;
            }
            else
            {
                for (int i = this._PendingRows.Count(); i > 0; i--)
                {
                    // use the earliest and longest combo possible
                    IEnumerable<ReportingBaseData> possibleCombo = this._PendingRows.Take(i);
                    if (ReportRowGenerationService.IsValidCombo(possibleCombo.Select(c => c.Type).ToArray()))
                    {
                        // found one - this MUST be at most when i == 1
                        this._PendingRows = this._PendingRows.Skip(i);
                        ReportRowDTO row = this._ReportRowGenerationService.GenerateRow(possibleCombo.ToArray());
                        this._ReportingPersistorService.AddRow(row);

                        if (this._PendingRows.Any())
                        {
                            this._ReportingPersistorService.AddRow(row);
                        }

                        // there may be more valid combos left-over
                        this.tryWriteAllComboRows();
                        return;
                    }
                    else
                    {
                        // keep looking by truncating possible combo
                    }
                }

                return;
            }
        }
    }
}
