﻿using System;

namespace CryptoTax.Domain.Services
{
    public interface ITimeEventService
    {
        long AddAction(Action action, DateTime executionTime, bool executeAfterLastItem);
        void OnItemWillProcess(DateTime? previousTime, DateTime time);
        void OnLastItemDidProcess(DateTime time);
        bool TryRemoveAction(long id);
    }
}