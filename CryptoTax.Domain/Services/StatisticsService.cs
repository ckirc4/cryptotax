﻿namespace CryptoTax.Domain.Services
{
    /// <summary>
    /// If enabled, keeps track of non-critical metrics such as trading volume.
    /// </summary>
    internal class StatisticsService
    {
        /*
        - trading volume in AUD
        - trading volume of quote assets
        - trading volume of base assets
        - total number of trades
        - all the above are tracked as a time series (e.g. aggregated per day)
        */
    }
}
