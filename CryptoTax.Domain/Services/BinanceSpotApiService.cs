﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Models.WebResponse;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class BinanceSpotApiService : IExchangeApiService, IExchangeApiAdapter
    {
        private const string BASE_URL = "https://api.binance.com/api/";
        private const int REQUESTS_PER_MINUTE = 1200;
        private const int MIN_MS_SINCE_LAST_REQUEST = 200; //1000 / (REQUESTS_PER_MINUTE / 60);
        private const int CANDLESTICK_LIMIT = 1000;
        private readonly IWebService _WebService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;
        private readonly ExchangeCache _Cache;

        public const Exchange EXCHANGE = Exchange.Binance;

        public BinanceSpotApiService(IWebService webService, ILoggingService loggingService, ExchangeCache cache)
        {
            this._WebService = webService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "BinanceApiService");
            this._Cache = cache;
        }

        public async Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync()
        {
            return await ApiHelpers.GetSymbolsAsync(this._WebService, this._Cache, EXCHANGE, this, BASE_URL + "v3/exchangeInfo", MIN_MS_SINCE_LAST_REQUEST);
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly)
        {
            return await ApiHelpers.GetCandlesticksAsync(this._WebService, this._Logger, this._Cache, EXCHANGE, symbol, interval, fromUTC, toUTC, this, CANDLESTICK_LIMIT, BASE_URL + $"v3/klines", getOnly, MIN_MS_SINCE_LAST_REQUEST);
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly)
        {
            DateTime toUTC = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, count - 1);
            return await this.GetCandlesticksAsync(interval, symbol, fromUTC, toUTC, getOnly);
        }

        public ExchangeSymbol ParseSymbol(dynamic obj)
        {
            /*
            Example object:
            {
                "symbol": "ETHBTC",
                "status": "TRADING",
                "baseAsset": "ETH",
                "baseAssetPrecision": 8,
                "quoteAsset": "BTC",
                ...
            }
            */
            DString quoteAsset = (string)obj.quoteAsset;
            DString baseAsset = (string)obj.baseAsset;
            DString marketName = (string)obj.symbol;
            return new ExchangeSymbol(
                new GenericSymbol(
                    KnownCurrencies.TryGetCurrency(quoteAsset),
                    KnownCurrencies.TryGetCurrency(baseAsset)),
                EXCHANGE,
                marketName);
        }

        public Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            /*
            Example object:
            [
              [
                1499040000000,      // 0 Open time
                "0.01634790",       // 1 Open
                "0.80000000",       // 2 High
                "0.01575800",       // 3 Low
                "0.01577100",       // 4 Close
                "148976.11427815",  // 5 Volume
                1499644799999,      // 6 Close time
                "2434.19055334",    // 7 Quote asset volume
                308,                // 8 Number of trades
                "1756.87402397",    // 9 Taker buy base asset volume
                "28.46694368",      // 10 Taker buy quote asset volume
                "17928899.62484339" // 11 Ignore
              ]
            ]
            */

            DateTime time = new DateTime().FromUnix((long)obj[0]);
            decimal open = (decimal)obj[1];
            decimal close = (decimal)obj[4];
            return new Candlestick(
                time,
                interval,
                symbol,
                open,
                close);
        }

        public Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    {"symbol", symbol.Name },
                    {"interval", interval == CandlestickInterval.Minute_1 ? "1m" : interval == CandlestickInterval.Hour_1 ? "1h" : interval == CandlestickInterval.Day_1 ? "1d" : throw new AssertUnreachable(interval) },
                    { "startTime", sortedTimes.First().ToUnix().ToString() },
                    { "endTime", sortedTimes.Last().ToUnix().ToString() },
                };

            return parameters;
        }

        public IEnumerable<dynamic> ParseSymbolCollection(dynamic response)
        {
            /*
               {
                  "timezone": "UTC",
                  "serverTime": 1565246363776,
                  "rateLimits": [{}],
                  "exchangeFilters": [],
                  "symbols": [
                    {
                      "symbol": "ETHBTC",
                      "status": "TRADING",
                      "baseAsset": "ETH",
                      "baseAssetPrecision": 8,
                      "quoteAsset": "BTC",
                      ...
                    }
                  ]
                }
            */
            return response.symbols as IEnumerable<dynamic>;
        }

        public IEnumerable<dynamic> ParseCandlestickCollection(dynamic response)
        {
            // response is a raw array
            return response as IEnumerable<dynamic>;
        }

        /// <exception cref="InvalidSymbolException" />
        public async Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters)
        {
            // check if we know that this symbol is invalid
            if (this._Cache.TryGetCachedException(EXCHANGE, url, parameters, out InvalidSymbolException cachedException)) {
                throw cachedException;
            }

            try
            {
                return await apiCall();
            }
            catch (WebException e)
            {
                string message;
                if (e.Response != null)
                {
                    using (StreamReader reader = new StreamReader(e.Response.GetResponseStream()))
                    {
                        message = reader.ReadToEnd();
                    }

                    if (message.TryDeserialiseObject(out BinanceError binanceError))
                    {
                        if (binanceError.Code == -1121)
                        {
                            InvalidSymbolException exception = new InvalidSymbolException(Exchange.Binance, e, binanceError.ToString());
                            this._Cache.CacheException(EXCHANGE, url, parameters, exception);
                            throw exception;
                        }
                    }
                }

                throw e;
            }
        }
    }
}
