﻿using CryptoTax.Domain.Models.Reporting;

namespace CryptoTax.Domain.Services
{
    public interface IReportingService
    {
        void AddData(ReportingBaseData data, bool flushImmediately = false);
        void Flush();
    }
}
