﻿using System;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;

namespace CryptoTax.Domain.Services
{
    /// <summary>
    /// Modifies <see cref="PartialTransfer"/>s based on information that is implicit to Exchanges/Containers such that, when used to create a <see cref="Transfer"/>, the transfer data is complete enough to allow tracking of funds from one Inventory to another.
    /// </summary>
    public class RelayService
    {
        public RelayService()
        {
            throw new NotImplementedException("RelayService is currently in the conceptional phase and may not yet work correctly, or even be required at all.");
        }

        public PartialTransfer RelayPartialTransferIfRequired(PartialTransfer transfer)
        {
            if (transfer.OriginalDTO.Exchange == Exchange.MercatoX)
            {
                this.relayMercatoX(transfer);
            }

            return transfer;
        }

        private void relayMercatoX(PartialTransfer transfer)
        {
            if (transfer.IsDestinationSide
                    && transfer.OriginalDTO.Type == TransactionType.CryptoDeposit
                    && transfer.Destination?.Type == ContainerType.ExchangeWallet)
            {
                // the transferred amount reported by the source-side partial transfer is further reduced because of this relay step. this means that feeToReceive is now non-null, but here we don't know this fee yet. so must also do some relay cleanup once we have the whole partial transfer.
                // the same might be true with withdrawing
                // perhaps use chain of partial transfers? since we need to care about relaying once the final transfer object has been created.
                // could add a 'right' and 'left' relay property (nullable) which is only valid for destination/source side partial transfers respectively. this allows us to later reconstruct the whole chain correctly.

                // is this relay even required? the partial transfer already has the correct destination container, and any "fees to receive" are applied automatically when the final Transfer object is constructed.
                // even then, there should be no harm caused by adding this relay (it will attach "smoothly" to the destination side, so no extra fees or anything.
                transfer.WithRelay(TransferSide.Destination, Container.FromExchange(Exchange.MercatoX));
            }
            else if (transfer.IsSourceSide
                && transfer.OriginalDTO.Type == TransactionType.CryptoWithdrawal
                && transfer.Source?.Type == ContainerType.ExchangeWallet)
            {
                // don't think there's a relay required here... also, see above for reason why we probably won't need any at all
            }
        }
    }
}
