﻿using CryptoTax.Shared;
using System;
using System.Threading.Tasks;

namespace CryptoTax.Domain.Services
{
    public interface IBulkCandlestickService
    {
        /// <summary>
        /// Call this when a trade requires price data of a certain symbol at some time. Time here is treated as a continuous variable (does not have to be the beginning time of a candlestick).
        /// </summary>
        void AddRequired(ExchangeSymbol symbol, DateTime time);

        /// <summary>
        /// Once all required candlesticks have been added, call this function to bulk-fetch the required data and save it to the exchange cache.
        /// </summary>
        Task FetchAndSaveAll();
    }
}
