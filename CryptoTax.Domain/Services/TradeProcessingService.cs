﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Mathematics;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class TradeProcessingService
    {
        private readonly IdProviderService _IdProviderService;
        private readonly ICurrencyConversionService _CurrencyConversionService;
        private readonly IReportingService _ReportingService;
        private readonly IInventoryManagerService _InventoryManagerService;
        private readonly IBulkCandlestickService _BulkCandlestickService;
        private readonly IRegisteredLogger _Logger;

        private bool _HasSetup;

        public TradeProcessingService(IdProviderService idProviderService, ICurrencyConversionService currencyConversionService, IReportingService reportingService, IInventoryManagerService inventoryManagerService, IBulkCandlestickService bulkCandlestickService, ILoggingService loggingService)
        {
            this._IdProviderService = idProviderService;
            this._CurrencyConversionService = currencyConversionService;
            this._ReportingService = reportingService;
            this._InventoryManagerService = inventoryManagerService;
            this._BulkCandlestickService = bulkCandlestickService;
            this._Logger = loggingService.Register(this, "TradeProcessingService");

            this._HasSetup = false;
        }

        /// <summary>
        /// Collects the required candlesticks for calculating AUD values of all currencies present in the trades, possibly over a large range of exchanges, symbols, and times.
        /// </summary>
        public void Setup(IEnumerable<TradeDTO> allTrades)
        {
            if (this._HasSetup) throw new TradeSetupException("Setup() has already been called");

            foreach (TradeDTO trade in allTrades)
            {
                // add main trade symbol and symbols required for conversion to AUD
                // TAX-66 we don't add the first symbol because we already know the price based on the trade
                ExchangeSymbol thisSymbol = new ExchangeSymbol(trade.Symbol.QuoteCurrency, trade.Symbol.BaseCurrency, trade.Exchange);
                IEnumerable<ExchangeSymbol> chain = this._CurrencyConversionService.GetChainToAud(thisSymbol);
                foreach (ExchangeSymbol symbol in chain.Skip(1))
                {
                    this._BulkCandlestickService.AddRequired(symbol, trade.Time);
                }

                // try to add fee symbol as well
                // TAX-66 don't include fee symbol if it is the quote or base since we already know the price based on the trade
                NString feeCurrency = trade.ContainerEvent.FeeCurrency;
                if (feeCurrency.HasValue && feeCurrency.Value != trade.Symbol.QuoteCurrency && feeCurrency.Value != trade.Symbol.BaseCurrency)
                {
                    ExchangeSymbol? symbolForFee = this._CurrencyConversionService.TryGetExchangeSymbolFromCurrency(feeCurrency.Value, trade.Exchange);
                    if (symbolForFee.HasValue)
                    {
                        IEnumerable<ExchangeSymbol> feeChain = this._CurrencyConversionService.GetChainToAud(symbolForFee.Value);
                        foreach (ExchangeSymbol symbol in feeChain)
                        {
                            this._BulkCandlestickService.AddRequired(symbol, trade.Time);
                        }
                    }
                    else
                    {
                        throw new UnreachableCodeException($"Unable to establish ExchangeSymbol from fee currency {feeCurrency.Value} - this should not happen");
                    }
                }
            }

            this._HasSetup = true;
        }

        public async Task ProcessTradeAsync(TradeDTO trade)
        {
            if (!this._HasSetup) throw new TradeSetupException("Before processing transactions, must first call Setup()");

            /* note about fees:
                Disposed and received amounts SHOULD NOT INCLUDE ANY FEES even if the currencies match.

                Fees should always be treated as if they had a sepparate currency. Therefore, if we buy 1 BTC at a price of 10000 BTC/AUD (the marked exchange rate), the disposed amount is 10000 AUD, the received amount is 1 BTC, regardless of the fees.
                After the trade, we burn the fee amount, whichever currency it may be, and add its AUD value to the basis cost. This means that the received and disposed amount must always work out to give the price, otherwise the data is found to be inconsistent.

                If we don't do it this way, the price will be slightly off (higher or lower, whichever is not in favour of us) resulting in different value calculations. 
            */
            ContainerEventDTO ev = trade.ContainerEvent;

            DateTime time = ev.EventTime;
            DString currencyReceived = ev.CurrencyReceived.Value;
            decimal amountReceived = (decimal)ev.AmountReceived;
            DString currencyDisposed = ev.CurrencySent.Value;
            decimal amountDisposed = (decimal)ev.AmountSent;
            DString feeCurrency = ev.FeeCurrency != null ? ev.FeeCurrency.Value : "AUD";
            decimal feeAmount = ev.FeeAmount ?? 0;

            // check consistency of data - amounts must be correct to within 8 decimal places
            decimal actualAmount = trade.Side == OrderSide.Buy ? amountDisposed : amountReceived;
            decimal inferredAmount = trade.Side == OrderSide.Buy ? trade.Price * amountReceived : trade.Price * amountDisposed;
            if (Numerics.Round(inferredAmount, 8) != Numerics.Round(actualAmount, 8) &&
                Math.Truncate(1e8m * inferredAmount) != Math.Truncate(1e8m * actualAmount))
            {
                throw new InconsistentTradeDataException(trade);
            }

            (decimal quoteCurrencyValue, decimal baseCurrencyValue) = await this._CurrencyConversionService.GetAudUnitValueAsync(new ExchangeSymbol(trade.Symbol.QuoteCurrency, trade.Symbol.BaseCurrency, trade.Exchange), trade.Price, trade.Time);
            decimal valueReceived = amountReceived * (trade.Side == OrderSide.Buy ? baseCurrencyValue : quoteCurrencyValue);
            decimal valueSent = amountDisposed * (trade.Side == OrderSide.Buy ? quoteCurrencyValue : baseCurrencyValue);

            decimal feeValue = 0;
            if (feeAmount != 0)
            {
                if (feeCurrency == trade.Symbol.QuoteCurrency)
                {
                    feeValue = quoteCurrencyValue * feeAmount;
                }
                else if (feeCurrency == trade.Symbol.BaseCurrency)
                {
                    feeValue = baseCurrencyValue * feeAmount;
                }
                else
                {
                    // this should not fail - fee currencies are pretty standard
                    decimal unitValue = await this._CurrencyConversionService.GetSingleAudUnitValueAsync(feeCurrency, trade.Time, null);
                    feeValue = unitValue * feeAmount;
                }
            }

            CurrencyAmount purchasedCurrency = new CurrencyAmount(this._IdProviderService, currencyReceived, amountReceived, valueReceived, time, trade.Exchange);
            CurrencyAmount disposedCurrency = new CurrencyAmount(this._IdProviderService, currencyDisposed, amountDisposed, valueSent, time, trade.Exchange);
            CurrencyAmount? feeCA = feeAmount == 0 ? (CurrencyAmount?)null : new CurrencyAmount(this._IdProviderService, feeCurrency, feeAmount, feeValue, time, trade.Exchange);

            // inventories must handle merge/splits and adds/removes reporting, but we KNOW that we bought and sold some amount, so we handle the reporting for this ourselves.
            // generally, purchase/dispose events create/destroy currency amounts, add/remove only references them. it's a little awkward to provide the capital gains in the remove data (when it would be expected in the dispose data), but I can't think of a better way to handle this (dispose event comes first, then inventory is updated including capital gains generation, so where is the capital gains best reported?)
            // does disposing need to receive special treatment when it is AUD we are disposing? can we add the fees to the AUD cost basis? is it valid to generate a disposed row in the report?
            IInventory inv = this._InventoryManagerService.GetInventory(ev.Container);

            // action report
            this._ReportingService.AddData(new ReportingPurchaseData(this._IdProviderService, purchasedCurrency.TimeModified, purchasedCurrency));
            inv.Purchase(purchasedCurrency);

            // action report
            this._ReportingService.AddData(new ReportingDisposeData(this._IdProviderService, purchasedCurrency.TimeModified, disposedCurrency, DisposalType.SpotSale, feeCA));
            inv.Disposal(disposedCurrency, feeValue, out decimal overdrawnAmount);
            if (overdrawnAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, inv.Id, overdrawnAmount, disposedCurrency.Currency, "trading disposal");

            if (feeCA.HasValue)
            {
                // action report
                this._ReportingService.AddData(new ReportingBurnData(this._IdProviderService, feeCA.Value.TimeModified, feeCA.Value, BurnReason.Disposal, disposedCurrency));
                inv.TransferOut(feeCA.Value, TransferOutReason.Burn, out decimal overdrawnFeeAmount);
                if (overdrawnFeeAmount > 0) this._Logger.Log(LogMessage.MSG_INV_WARNING_OVERDRAWN, inv.Id, overdrawnFeeAmount, feeCA.Value.Currency, "trading fee");
            }
        }
    }
}
