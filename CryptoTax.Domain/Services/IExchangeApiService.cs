﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Shared;

namespace CryptoTax.Domain.Services
{
    public interface IExchangeApiService
    {
        Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync();

        /// <summary>
        /// Gets N consecutive candlesticks for the specified symbol, starting at the given open time.
        /// </summary>
        /// <param name="openTime">The open time of the first candlestick to fetch. If it does not correspond to an open time, will be rounded down automatically.</param>
        /// <param name="getOnly">If true, will not save results to cache, but still read from cache. If true, can safely run multiple requests in parallel (as long as they don't overlap each other).</param>
        /// <exception cref="NoPriceDataException"></exception>
        Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly);
        Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly);
    }
}
