﻿using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Domain.Services
{
    public class FtxApiService : IExchangeApiService, IExchangeApiAdapter
    {
        private const string BASE_URL = "https://ftx.com/api/";
        private const int REQUESTS_PER_SECOND = 30;
        private const int MIN_MS_SINCE_LAST_REQUEST = 200; // 1000 / REQUESTS_PER_SECOND;
        private const int CANDLESTICK_LIMIT = 5000;
        private readonly IWebService _WebService;
        private readonly ILoggingService _LoggingService;
        private readonly IRegisteredLogger _Logger;
        private readonly ExchangeCache _Cache;

        public const Exchange EXCHANGE = Exchange.FTX;

        public FtxApiService(IWebService webService, ILoggingService loggingService, ExchangeCache cache)
        {
            this._WebService = webService;
            this._LoggingService = loggingService;
            this._Logger = loggingService.Register(this, "FtxApiService");
            this._Cache = cache;
        }

        public async Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync()
        {
            // see https://docs.ftx.com/#get-markets
            return await ApiHelpers.GetSymbolsAsync(this._WebService, this._Cache, EXCHANGE, this, BASE_URL + "markets", MIN_MS_SINCE_LAST_REQUEST);
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly)
        {
            // see https://docs.ftx.com/#get-historical-prices
            var candles = await ApiHelpers.GetCandlesticksAsync(this._WebService, this._Logger, this._Cache, EXCHANGE, symbol, interval, fromUTC, toUTC, this, CANDLESTICK_LIMIT, BASE_URL + $"markets/{symbol.Name}/candles", getOnly, MIN_MS_SINCE_LAST_REQUEST);
            return candles;
        }

        public async Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly)
        {
            DateTime toUTC = CandlestickHelpers.GetTimeAfterIntervals(interval, fromUTC, count - 1);
            return await this.GetCandlesticksAsync(interval, symbol, fromUTC, toUTC, getOnly);
        }

        public ExchangeSymbol ParseSymbol(dynamic obj)
        {
            /*
            Example object:
            {
                "name": "BTC-0628",
                "baseCurrency": null,
                "quoteCurrency": null,
                "type": "future",
                "underlying": "BTC",
                "enabled": true,
                "ask": 3949.25,
                "bid": 3949,
                "last": 10579.52,
                "postOnly": false,
                "priceIncrement": 0.25,
                "sizeIncrement": 0.001,
                "restricted": false
            }
            */
            DString marketName = (string)obj.name;
            DString quoteAsset = (string)obj.quoteCurrency ?? "USD";
            DString baseAsset = (string)obj.baseCurrency ?? marketName;
            return new ExchangeSymbol(
                new GenericSymbol(
                    KnownCurrencies.TryGetCurrency(quoteAsset),
                    KnownCurrencies.TryGetCurrency(baseAsset)),
                EXCHANGE,
                marketName);
        }

        public Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            /*
            Example object:
            {
                "close": 11055.25,
                "high": 11089.0,
                "low": 11043.5,
                "open": 11059.25,
                "startTime": "2019-06-24T17:15:00+00:00",
                "volume": 464193.95725
            }
            */
            // by default, DateTime objects are created within the local time zone
            DateTime time = ((DateTime)obj.startTime).ToUniversalTime();
            decimal open = (decimal)obj.open;
            decimal close = (decimal)obj.close;
            return new Candlestick(
                time,
                interval,
                symbol,
                open,
                close);
        }

        public Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    { "market_name", symbol.Name },
                    { "resolution", interval == CandlestickInterval.Minute_1 ? "60" : interval == CandlestickInterval.Hour_1 ? "3600" : interval == CandlestickInterval.Day_1 ? "86400" : throw new AssertUnreachable(interval) },
                    { "start_time", sortedTimes.First().ToUnixSeconds().ToString() },
                    { "end_time", sortedTimes.Last().ToUnixSeconds().ToString() }
                };

            return parameters;
        }

        public IEnumerable<dynamic> ParseSymbolCollection(dynamic response)
        {
            /*
            Response model:
            {
              "success": true,
              "result": [
                ...
              ]
            }
            */
            return response.result as IEnumerable<dynamic>;
        }

        public IEnumerable<dynamic> ParseCandlestickCollection(dynamic response)
        {
            /*
            Response model:
            {
                "success": true,
                "result": [
                ...
                ]
            }
            */
            return response.result as IEnumerable<dynamic>;
        }

        public async Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters)
        {
            return await apiCall();
        }
    }
}
