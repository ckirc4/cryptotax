﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE0071:Simplify interpolation", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Domain.Services.BulkCandlestickService.getGroupKey(CryptoTax.Domain.Models.ExchangeSymbol,System.Collections.Generic.IEnumerable{System.Collections.Generic.IEnumerable{System.DateTime}})~UtilityLibrary.Types.DString")]
[assembly: SuppressMessage("Style", "IDE0071:Simplify interpolation", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Domain.Services.ApiHelpers.getAllCandlesticksInRange(CryptoTax.Shared.Services.IWebService,CryptoTax.Domain.Models.ExchangeCache,CryptoTax.Shared.Enums.Exchange,CryptoTax.Domain.Models.ExchangeSymbol,CryptoTax.Shared.Enums.CandlestickInterval,System.DateTime,System.DateTime,CryptoTax.Domain.Services.IExchangeApiAdapter,System.Int32,System.String,System.Nullable{System.Int32},System.Boolean)~System.Threading.Tasks.Task{System.Collections.Generic.IEnumerable{CryptoTax.Domain.Models.Candlestick}}")]
[assembly: SuppressMessage("Style", "IDE0071:Simplify interpolation", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Domain.Services.ApiHelpers.fillInLeadingBlanks(System.Collections.Generic.IEnumerable{CryptoTax.Domain.Models.Candlestick},CryptoTax.Shared.Services.IWebService,CryptoTax.Domain.Models.ExchangeCache,CryptoTax.Shared.Enums.Exchange,CryptoTax.Domain.Models.ExchangeSymbol,CryptoTax.Shared.Enums.CandlestickInterval,System.DateTime,System.DateTime,CryptoTax.Domain.Services.IExchangeApiAdapter,System.Int32,System.String,System.Nullable{System.Int32},System.Int32)~System.Threading.Tasks.Task{System.Collections.Generic.IEnumerable{CryptoTax.Domain.Models.Candlestick}}")]
