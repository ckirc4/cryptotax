﻿using System;

namespace CryptoTax.Persistence.Schema
{
    internal class EnumColumn<T> : Column<T> where T : Enum
    {
        /// <summary>
        /// Uses the default enum conversion, where the string and typed representations of the enum are identical (case independent unless specified otherwise).
        /// </summary>
        public EnumColumn(string columnName, bool mandatory, bool parseExactOnly = false) : base(columnName, mandatory, constructParseFnc(parseExactOnly))
        { }

        /// <summary>
        /// Returns the function that converts the string input to the correct enum.
        /// </summary>
        private static Func<string, T> constructParseFnc(bool parseExactOnly)
        {
            T func(string input)
            {
                if (!parseExactOnly) input = input.Replace(" ", "").Replace("_", "").Replace("-", "");
                return (T)Enum.Parse(typeof(T), input, !parseExactOnly);
            }

            return func;
        }
    }
}
