﻿using CryptoTax.Persistence.Schema.Exchange.Binance;
using CryptoTax.Persistence.Schema.Exchange.BitMex;
using CryptoTax.Persistence.Schema.Exchange.BTCMarkets;
using CryptoTax.Persistence.Schema.Exchange.FTX;
using CryptoTax.Persistence.Schema.Exchange.MercatoX;
using CryptoTax.Persistence.Schema.Exchange.OnChain;
using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;
using System.Linq;
using System.Collections.Generic;
using CryptoTax.Persistence.Schema.Exchange.Kraken;

namespace CryptoTax.Persistence.Schema
{
    public static class SchemaFactory
    {
        /// <exception cref="NotImplementedException">Thrown if no schema exists yet for the provided identifier.</exception>
        public static Schema Create(SchemaIdentifier identifier, DString fileName)
        {
            Schema schema;

            // this is essentially the mapping between the identifier and schema.
            switch (identifier)
            {
                case SchemaIdentifier.MercatoX:
                    schema = new MercatoXSchema();
                    break;
                case SchemaIdentifier.OnChain_General:
                    // the legacy file name is used for this schema identifier only for the old ETH wallet.
                    // everything else is in the form `OnChain\{FinancialYear.StartYear}\{ChainType}\{WalletAddress}.csv
                    if (fileName.Value.Contains(KnownWallets.ETH_MY_WALLET_OLD.Address))
                    {
                        schema = new OnChain_GeneralSchema(KnownWallets.ETH_MY_WALLET_OLD.Address, BlockchainType.Ethereum);
                    }
                    else
                    {
                        IEnumerable<string> parts = fileName.Value.Split('\\').Reverse().Take(2).Reverse();

                        string chainTypeString = parts.ElementAt(0);
                        BlockchainType chainType;
                        if (chainTypeString == "EthereumMainnet") chainType = BlockchainType.Ethereum;
                        else if (chainTypeString == "EthereumArbitrumNova") chainType = BlockchainType.ArbitrumNova;
                        else if (chainTypeString == "XRPL") chainType = BlockchainType.XRPL;
                        else throw new Exception($"Cannot parse file parts for the OnChain_General schema. Unrecognised chain type '{chainTypeString}'.");

                        string file = parts.ElementAt(1);
                        if (!file.EndsWith(".csv")) throw new Exception("Cannot parse file parts for the OnChain_General schema. It must end in .csv");
                        string address = file.Substring(0, file.Length - ".csv".Length);

                        schema = new OnChain_GeneralSchema(address, chainType);
                    }
                    break;
                case SchemaIdentifier.OnChain_0xe5dd:
                    schema = new OnChain_0xe5ddSchema("0xe5dd240231608367fb2d61d5348693a7cbf5396e");
                    break;
                case SchemaIdentifier.BtcMarkets:
                    schema = new BTCMarketsSchema();
                    break;
                case SchemaIdentifier.BitMex:
                    schema = new BitMexSchema();
                    break;
                case SchemaIdentifier.Ftx:
                    schema = new FtxSchema();
                    break;
                case SchemaIdentifier.Binance_Deposits:
                case SchemaIdentifier.Binance_Withdrawals:
                    schema = new BinanceTransferSchema(identifier);
                    break;
                case SchemaIdentifier.Binance_Withdrawals_V2:
                    schema = new BinanceTransferV2Schema();
                    break;
                case SchemaIdentifier.Binance_FiatDeposits:
                    schema = new BinanceFiatDepositsSchema();
                    break;
                case SchemaIdentifier.Binance_Distributions:
                    schema = new BinanceDistributionsSchema();
                    break;
                case SchemaIdentifier.Binance_SpotTrades:
                    schema = new BinanceSpotTradeSchema();
                    break;
                case SchemaIdentifier.Binance_BuyCrypto:
                    schema = new BinanceBuyCryptoSchema();
                    break;
                case SchemaIdentifier.Binance_FuturesTrades:
                    schema = new BinanceFuturesTradesSchema();
                    break;
                case SchemaIdentifier.Binance_Options:
                    schema = new BinanceOptionsSchema();
                    break;
                case SchemaIdentifier.Binance_WalletTransactions:
                    schema = new BinanceWalletTransactionsSchema();
                    break;
                case SchemaIdentifier.Binance_WalletTransactions_V2:
                    schema = new BinanceWalletTransactionsV2Schema();
                    break;
                case SchemaIdentifier.Binance_Convert:
                    schema = new BinanceConvertSchema();
                    break;
                case SchemaIdentifier.Kraken_Trades:
                    schema = new KrakenTradeSchema();
                    break;
                case SchemaIdentifier.Kraken_Ledgers:
                    schema = new KrakenLedgerSchema();
                    break;
                default:
                    throw new AssertUnreachable(identifier);
            }

            if (schema == null) throw new NotImplementedException($"A schema could not be created for identifier '{identifier}' because it has not been implemented yet.");
            else if (schema.Identfier != identifier) throw new Exception($"A schema was created, but its identifier '{schema.Identfier}' did not match the required identifier '{identifier}'.");

            return schema;
        }
    }
}
