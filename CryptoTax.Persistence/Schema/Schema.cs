﻿using System.Collections.Generic;

namespace CryptoTax.Persistence.Schema
{
    public abstract class Schema // You might be tempted to make this generic, where T : Record. Don't. The generics bubble up uncontrollably and make the code too complicated.
    {
        /// <summary>
        /// From now on, there must be a 1-to-1 map between a schema and identifier.
        /// </summary>
        public abstract SchemaIdentifier Identfier { get; }

        public abstract Column[] Columns { get; }

        /// <summary>
        /// Called for each row. Implement this to assign typed data. May not return null.
        /// </summary>
        public abstract Record ProcessOne(object[] row);

        /// <summary>
        /// Called once only. Implement this if records are interdependent in some way. May return null.
        /// </summary>
        public virtual IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords) { return null; }
    }
}
