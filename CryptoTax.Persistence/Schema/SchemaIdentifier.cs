﻿namespace CryptoTax.Persistence.Schema
{
    /// <summary>
    /// Every schema must have an identifier so we can match it with an exchange and purpose.
    /// </summary>
    public enum SchemaIdentifier
    {
        MercatoX,
        /// <summary>
        /// For all on-chain addresses except the Reddit Vault.
        /// </summary>
        OnChain_General,
        /// <summary>
        /// For the 0xe5dd240231608367fb2d61d5348693a7cbf5396e (Reddit Vault) address
        /// </summary>
        OnChain_0xe5dd,
        BtcMarkets,
        BitMex,
        Ftx,
        Kraken_Trades,
        Kraken_Ledgers,

        Binance_Withdrawals,
        /// <summary>
        /// Replaces Binance_Withdrawals in 2022+
        /// </summary>
        Binance_Withdrawals_V2,
        Binance_Deposits,
        Binance_FiatDeposits,
        Binance_Distributions,
        Binance_SpotTrades,
        Binance_BuyCrypto,
        Binance_Convert,
        Binance_WalletTransactions,
        /// <summary>
        /// Replaces Binance_WalletTransactions in 2022+
        /// </summary>
        Binance_WalletTransactions_V2,
        Binance_FuturesTrades,
        Binance_Options
    }
}
