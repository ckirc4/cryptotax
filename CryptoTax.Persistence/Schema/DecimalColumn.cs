﻿using System;

namespace CryptoTax.Persistence.Schema
{
    internal class DecimalColumn : Column<decimal>
    {
        /// <summary>
        /// The format is used to parse the string to the <see cref="decimal"/> type. Accepts either normal notation or exponential notation.
        /// </summary>
        public DecimalColumn(string columnName, bool mandatory) : base(columnName, mandatory, decimalParser)
        { }

        /// <summary>
        /// Converts the string input to a decimal, accepting exponential notation.
        /// </summary>
        private static decimal decimalParser(string input)
        {
            if (decimal.TryParse(input, out decimal result))
            {
                return result;
            }
            else if (input.ToLower().Contains("e"))
            {
                string[] parts = input.ToLower().Split('e');
                decimal number = decimalParser(parts[0]);
                int exponent = int.Parse(parts[1]);
                decimal multiplier = (decimal)Math.Pow(10, exponent);
                return number * multiplier;
            }
            else throw new ArgumentException($"Cannot convert the input '{input}' to a decimal");
        }
    }
}
