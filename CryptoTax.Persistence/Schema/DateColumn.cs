﻿using System;
using System.Globalization;

namespace CryptoTax.Persistence.Schema
{
    internal class DateColumn : Column<DateTime>
    {
        /// <summary>
        /// The format is used to parse the string to the DateTime type. All formats in the array are tried (there may be different formats because e.g. single- vs double-digit days/hours).
        /// The offset is the offset in hours relative to UTC in which the input is presented (e.g. Brisbane time: offset = 10)
        /// </summary>
        public DateColumn(string columnName, bool mandatory, string[] format, int offset = 0) : base(columnName, mandatory, constructParseFnc(format, offset))
        { }

        /// <summary>
        /// Uses the default parsing method. Only recommended if it is known that time formats are well-behaved.<br/>
        /// The offset is the offset in hours relative to UTC in which the input is presented (e.g. Brisbane time: offset = 10)
        /// </summary>
        public DateColumn(string columnName, bool mandatory, int offset = 0) : base(columnName, mandatory, constructParseFnc(null, offset))
        { }

        /// <summary>
        /// Uses the default parsing method. Only recommended if it is known that times and time zones are well-behaved.
        /// </summary>
        public DateColumn(string columnName, bool mandatory) : base(columnName, mandatory, str => DateTime.Parse(str).ToUniversalTime())
        { }

        /// <summary>
        /// Returns the function that converts the string input to a UTC time.
        /// </summary>
        private static Func<string, DateTime> constructParseFnc(string[] format, int offset)
        {
            DateTime func(string input)
            {
                DateTime date;
                if (format == null || format.Length == 0)
                {
                    if (!DateTime.TryParse(input, out date))
                    {
                        throw new Exception($"Unable to convert string into a date: {input}. Please provide a custom format in the schema.");
                    }
                }
                else
                {
                    // if there is ever an inexplicable exception here, it may be worth allowing custom culture info to be specified for each schema
                    date = DateTime.ParseExact(input, format, new CultureInfo("en-US"), DateTimeStyles.None);
                }
                date = DateTime.SpecifyKind(date, DateTimeKind.Utc);
                return date.Subtract(TimeSpan.FromHours(offset));
            }

            return func;
        }
    }
}
