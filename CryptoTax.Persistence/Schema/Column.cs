﻿using System;

namespace CryptoTax.Persistence.Schema
{
    // note: abstract means that it is not intended to be instantiated, but must be overriden by base classes (as opposed to virtual, which MAY be overriden)
    public abstract class Column
    {
        /// <summary>
        /// The name of the column, as it appears on the CSV
        /// </summary>
        public abstract string ColumnName { get; set; }
        public abstract bool Mandatory { get; set; }
        public abstract object ParseString(string value);
    }

    public class Column<T> : Column
    {
        /// <summary>
        /// The name of the column, as it appears on the CSV
        /// </summary>
        public override string ColumnName { get; set; }
        public override bool Mandatory { get; set; }
        /// <summary>
        /// Function that parses the column value to type T
        /// </summary>
        public Func<string, T> Parse { get; set; }

        public Column(string columnName, bool mandatory, Func<string, T> parseFnc = null)
        {
            this.ColumnName = columnName;
            this.Mandatory = mandatory;
            this.Parse = parseFnc ?? this.getDefaultParser();
        }

        private Func<string, T> getDefaultParser()
        {
            return (input) => (T)Convert.ChangeType(input, typeof(T));
        }

        public override object ParseString(string value)
        {
            return this.Parse(value);
        }
    }
}
