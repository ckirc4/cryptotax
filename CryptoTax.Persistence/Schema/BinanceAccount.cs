﻿namespace CryptoTax.Persistence.Schema
{
    public enum BinanceAccount
    {
        Classic,
        Master,
        Sub1,
        Sub2
    }
}
