﻿using System;
using UtilityLibrary.Timing;

namespace CryptoTax.Persistence.Schema
{
    internal class TimestampColumn : Column<DateTime>
    {
        /// <summary>
        /// The format is used to parse the UNIX timestamp to the <see cref="DateTime"/> type. Specify whether the timestamp uses seconds or milliseconds using <paramref name="msTimestamp"/>.
        /// </summary>
        public TimestampColumn(string columnName, bool mandatory, bool msTimestamp) : base(columnName, mandatory, constructParseFnc(msTimestamp))
        {
        }

        /// <summary>
        /// Returns the function that converts the string input to a UTC time.
        /// </summary>
        private static Func<string, DateTime> constructParseFnc(bool msTimestamp)
        {
            if (msTimestamp)
            {
                // milliseconds
                return (string input) => new DateTime().FromUnix(long.Parse(input));
            }
            else
            {
                // seconds
                return (string input) => new DateTime().FromUnixSeconds(long.Parse(input));
            }
        }
    }
}
