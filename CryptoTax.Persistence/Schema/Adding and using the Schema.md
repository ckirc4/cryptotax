﻿## Definitions
- **Schemas** are used to provide strongly typed exchange data definitions that allow us to verify that the data is processed correctly and use the imported data in a unified way.
- **Columns** are used when reading CSV files and converting individual entries. At this point, the entries are also type-checked so that we can ensure all data is formatted as expected.
- **Records** are typed and processed rows, which can be converted into DTO objects to be used in other parts of the application. This is the only type that is exposed to other parts of the application.
- **RecordGroups** are used to group interdependent records and convert them into DTO objects.

## Adding a new schema
1. Create a new folder `Persistence/Schema/Exchange/{ExchangeName}` for the exchange.
2. Add a `{ExchangeName}Record` class that inherits from the abstract `Record` class.
	- Define typed (possibly nullable) and well-named properties that can be derived from the columns of the CSV.
	- Create new types (e.g. enums) if required. Place them into this folder also.
	- The constructor should accept a `object[] parsedRow` parameter and instantiate all properties by casting the objects to the correct type.
	- Override any of the `To*DTO()` methods. This will include a fair amount of type juggling, but it is important to get this right for ease of use later on.
  - *Optional*: Add a `{ExchangeName}RecordGroup` class that inherits from the abstract `RecordGroup` class. This is only required if a single `Record` does not contain complete information of a `TradeDTO` or `TransactionDTO` - for example, if trading fees are presented in a separate CSV row.
3. Add a `{ExchangeName}Schema` class that inherits from the abstract `Schema` class.
	- Create a new `SchemaIdentifier` and add the corresponding entry to the `SchemaFactory.Create()` method.
  - Update the `ExchangeDataLocationService` to return the file(s) relevant to the new schema.
	- Define `Columns` that correspond to the entries of the CSV file (see below for more info).
	- In the constructor, create an **ordered** array of those columns, and assign it to `this.Columns`.
	- The `ProcessOne()` method should simply instantiate a new `{ExchangeName}Record` object (see step 2). We use this method so we don't have to couple this exchange with its `Record` type outside of the schema.
  - If a `RecordGroup` has been added for this exchange in step 2, also override the `ProcessAll()` method.
4. Add tests in `CryptoTax.Tests.Schema.{ExchangeName}Tests` and ensure that, given a sample raw `string` row, the correct DTO type(s) is/are generated.

Important: When modifying the schema in any way, the `/Data/ExchangeDtoCache` folder MUST be deleted, otherwise the new schema changes may not come into effect.

## `Record` Example Implementation

```csharp
public class SampleExchangeRecord : Record
{
	int Id { get; set; }
	DateTime Time { get; set; }
	decimal? Fee { get; set; }
	SampleType Type { get; set; }

	public SampleExchangeRecord(object[] parsedRow) {
		this.Id = (int)parsedRow[0];
		this.Time = (DateTime)parsedRow[1];
		this.Fee = (decimal?)parsedRow[2];
		this.Type = (SampleType)parsedRow[3];
	}

	public override TradeDTO ToTradeDTO()
	{
    // if implementing SampleExchangeRecordGroup, probably want to return null here
		return new TradeDTO(/* add arguments here */);
	}

	public override TransactionDTO ToTransactionDTO()
	{
    // if implementing SampleExchangeRecordGroup, probably want to return null here

		// can return null if not applicable to this particular record
		if (this.Type != SampleType.Transaction) return null;

		// note that we can return multiple types of DTOs from a single record.
		// for example, a record could contain both a trade and a transaction (fee)
		return new TransactionDTO(/* add arguments here */);
	}
}
```

## `RecordGroup` Example Implementation
Note: If every record can be used to generate a complete DTO object, implementing this type won't be necessary.
In addition, it may be useful to create a `GroupType` enum for this exchange so that data validation can be separated easily from DTO conversion.
```csharp
public class SampleExchangeRecordGroup : RecordGroup
{
    private readonly IEnumerable<SampleExchangeRecord> _Group;
    public override IEnumerable<Record> Group { get { return this._Group; } }
    public readonly GroupType Type;

    public SampleExchangeRecordGroup(IEnumerable<SampleExchangeRecord> group, GroupType type)
    {
      this._Group = group;
      this.Type = type;
    }

    public override TradeDTO ToTradeDTO()
    {
      if (this.Type != GroupType.Trade) return null;

		  return new TradeDTO(/* add arguments here */);
    }

    public override TransactionDTO ToTransactionDTO()
    {
      if (this.Type == GroupType.Trade) return null;
      
      return new TransactionDTO(/* add arguments here */);
    }

    public static IEnumerable<SampleExchangeRecordGroup> ProcessAll(IEnumerable<SampleExchangeRecord> allRecords)
    {
      // go through each record and generate groups here
      // includes validation logic so that the conversion in the overridden methods above can go ahead without checking everything first
    }
}
```

## `Schema` Example Implementation

```csharp
public class SampleExchangeSchema : Schema
{
	// this can be used if the time format is somewhat dynamic (e.g. days can be one or two digits long)
	private static readonly string[] DATE_FORMAT = CsvHelpers.GenerateSimilarDateFormats("MMM {0}, yyyy, {1}:mm:ss tt", new[]{ "d", "dd" }, new[]{ "h", "hh" });

	readonly Column<int> Id = new Column<int>("id", true);
	readonly DateColumn Time = new DateColumn("tx_time", true, DATE_FORMAT, 10);
	readonly Column<decimal> Fee = new Column<decimal>("fee", false);
	readonly EnumColumn<SampleType> Type = new EnumColumn<SampleType>("tx_type", true);

  public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Sample;
  public override Column[] Columns { get; }

	public SampleExchangeSchema()
	{
		this.Columns = new Column[]
    {
			this.Id,
			this.Time,
			this.Fee,
			this.Type
		};
	}
	
  public override Record ProcessOne(object[] parsedRow)
  {
    return new SampleExchangeRecord(parsedRow);
  }

  // optional - only override this if SampleExchangeRecordGroup exists
  public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
  {
    // makes sense to cast here so that SampleExchangeRecordGroup is exposed only to its native record type.
    return SampleExchangeRecordGroup.ProcessAll(allRecords.Cast<SampleExchangeRecord>());
  }
}
```
