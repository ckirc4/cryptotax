﻿using CryptoTax.Shared.Enums;
using CryptoTax.TransactionFetcher.FTX;

namespace CryptoTax.Persistence.Schema.Exchange.FTX
{
    public class FtxSchema : Schema
    {
        /// <summary>
        /// Internal id or on-chain transaction
        /// </summary>
        readonly Column<string> Id = new Column<string>("Id", true);
        readonly DateColumn Time = new DateColumn("Time", true, 0);
        readonly EnumColumn<RowType> Type = new EnumColumn<RowType>("Type", true, true);
        readonly Column<string> CurrencyReceived = new Column<string>("CurrencyReceived", false);
        readonly DecimalColumn AmountReceived = new DecimalColumn("AmountReceived", false);
        readonly Column<string> CurrencySent = new Column<string>("CurrencySent", false);
        readonly DecimalColumn AmountSent = new DecimalColumn("AmountSent", false);
        readonly Column<string> FeeCurrency = new Column<string>("FeeCurrency", false);
        readonly DecimalColumn FeeAmount = new DecimalColumn("FeeAmount", false);

        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        readonly Column<string> WithdrawalAddress = new Column<string>("WithdrawalAddress", false);
        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        readonly Column<string> WithdrawalAddressTag = new Column<string>("WithdrawalAddressTag", false);

        /// <summary>
        /// Defined only for trades
        /// </summary>
        readonly EnumColumn<FtxMarketType> MarketType = new EnumColumn<FtxMarketType>("MarketType", false, true);
        /// <summary>
        /// Defined only for trades
        /// </summary>
        readonly DecimalColumn Price = new DecimalColumn("Price", false);
        /// <summary>
        /// Defined only for trades
        /// </summary>
        readonly EnumColumn<OrderSide> OrderSide = new EnumColumn<OrderSide>("Side", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Ftx;
        public override Column[] Columns { get; }

        public FtxSchema()
        {
            this.Columns = new Column[]
            {
                this.Id,
                this.Time,
                this.Type,
                this.CurrencyReceived,
                this.AmountReceived,
                this.CurrencySent,
                this.AmountSent,
                this.FeeCurrency,
                this.FeeAmount,

                this.WithdrawalAddress,
                this.WithdrawalAddressTag,

                this.MarketType,
                this.Price,
                this.OrderSide
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new FtxRecord(parsedRow);
        }
    }
}
