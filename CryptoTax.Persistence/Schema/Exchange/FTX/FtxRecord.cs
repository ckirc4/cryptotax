﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.TransactionFetcher.FTX;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.FTX
{
    public class FtxRecord : Record
    {
        private static readonly Shared.Enums.Exchange EXCHANGE = Shared.Enums.Exchange.FTX;
        private static readonly Container CONTAINER = Container.FromExchange(EXCHANGE);

        /// <summary>
        /// Internal id or on-chain transaction
        /// </summary>
        public DString Id { get; set; }
        /// <summary>
        /// UTC
        /// </summary>
        public DateTime Time { get; set; }
        public RowType Type { get; set; }
        public NString CurrencyReceived { get; set; }
        public decimal? AmountReceived { get; set; }
        public NString CurrencySent { get; set; }
        public decimal? AmountSent { get; set; }
        public NString FeeCurrency { get; set; }
        public decimal? FeeAmount { get; set; }

        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        public NString WithdrawalAddress { get; set; }
        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        public NString WithdrawalAdressTag { get; set; }

        /// <summary>
        /// Defined only for trades
        /// </summary>
        public FtxMarketType? MarketType { get; set; }
        /// <summary>
        /// Defined only for trades
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// Defined only for trades
        /// </summary>
        public OrderSide? Side { get; set; }


        public FtxRecord(object[] parsedRow)
        {
            this.Id = new DString((string)parsedRow[0]);
            this.Time = (DateTime)parsedRow[1];
            this.Type = (RowType)parsedRow[2];
            this.CurrencyReceived = (string)parsedRow[3];
            this.AmountReceived = (decimal?)parsedRow[4];
            this.CurrencySent = (string)parsedRow[5];
            this.AmountSent = (decimal?)parsedRow[6];
            this.FeeCurrency = (string)parsedRow[7];
            this.FeeAmount = (decimal?)parsedRow[8];

            this.WithdrawalAddress = (string)parsedRow[9];
            this.WithdrawalAdressTag = (string)parsedRow[10];

            this.MarketType = (FtxMarketType?)parsedRow[11];
            this.Price = (decimal?)parsedRow[12];
            this.Side = (OrderSide?)parsedRow[13];

            if (this.Type == RowType.Withdrawal && this.WithdrawalAddress == null) throw new Exception("Withdrawal address must be defined for rows of type Withdrawal");
            else if (this.Type == RowType.Trade && (this.MarketType == null || this.Price == null || this.Side == null)) throw new Exception("MarketType, Price, and Side must be defined for rows of type Trade");
            else if (this.Type == RowType.Trade && (!this.AmountReceived.HasValue || !this.CurrencyReceived.HasValue || !this.AmountSent.HasValue || !this.CurrencySent.HasValue)) throw new Exception("Amount/currency sent/received must all be set for trades");
        }

        public override TradeDTO ToTradeDTO()
        {
            if (this.Type != RowType.Trade) return null;

            // futures trades are not explicitly handled - instead, transactions are generated for the PnlSnapshot type.
            if (this.MarketType.Value == FtxMarketType.Futures) return null;

            DString quoteCurrency, baseCurrency;
            OrderSide side = this.Side.Value;
            if (side == OrderSide.Buy)
            {
                baseCurrency = this.CurrencyReceived.Value;
                quoteCurrency = this.CurrencySent.Value;
            }
            else
            {
                baseCurrency = this.CurrencySent.Value;
                quoteCurrency = this.CurrencyReceived.Value;
            }

            // don't include fee quantities of zero
            NString feeCurrency = null;
            decimal? feeAmount = null;
            if (this.FeeAmount.GetValueOrDefault() > 0)
            {
                feeCurrency = this.FeeCurrency;
                feeAmount = this.FeeAmount.Value;
            }

            SymbolDTO symbol = new SymbolDTO(quoteCurrency, baseCurrency);
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, CONTAINER, this.CurrencySent, this.AmountSent, this.CurrencyReceived, this.AmountReceived, feeCurrency, feeAmount);

            return new TradeDTO(this.Time, EXCHANGE, this.Id, symbol, Shared.Enums.MarketType.Spot, side, this.Price.Value, containerEvent);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type == RowType.Trade) return null;

            // funding payments are already included in PnlSnapshots
            if (this.Type == RowType.FundingPayment) return null;

            NString internalId = this.Id;
            NString publicId = null;
            NString counterparty = null;
            TransactionType type;
            switch (this.Type)
            {
                case RowType.Deposit:
                    internalId = null;
                    publicId = this.Id;
                    type = this.CurrencyReceived == "AUD" ? TransactionType.FiatDeposit :  TransactionType.CryptoDeposit;
                    break;
                case RowType.Withdrawal:
                    internalId = null;
                    publicId = this.Id;
                    counterparty = this.WithdrawalAddress;
                    if (this.WithdrawalAdressTag.HasValue) counterparty += "?t=" + this.WithdrawalAdressTag.Value;
                    type = TransactionType.CryptoWithdrawal;
                    break;
                case RowType.LendingPayment:
                    type = TransactionType.LoanInterest;
                    break;
                case RowType.PnlSnapshot:
                    type = TransactionType.FuturesPnL;
                    break;
                case RowType.Trade:
                case RowType.FundingPayment:
                default:
                    throw new UnreachableCodeException($"Invalid FTX transaction row type: {this.Type}");
            }
            
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, CONTAINER, this.CurrencySent, this.AmountSent, this.CurrencyReceived, this.AmountReceived, this.FeeCurrency, this.FeeAmount);
            return new TransactionDTO(this.Time, EXCHANGE, internalId, publicId, counterparty, type, containerEvent);
        }
    }
}
