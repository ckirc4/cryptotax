﻿using CryptoTax.Persistence.Services.Csv;

namespace CryptoTax.Persistence.Schema.Exchange.MercatoX
{
    internal class MercatoXSchema : Schema
    {
        private static readonly string[] DATE_FORMAT = CsvHelpers.GenerateSimilarDateFormats("MMM {0}, yyyy, {1}:mm:ss tt", new[] { "d", "dd" }, new[] { "h", "hh" });

        /// <summary>
        /// The unique Id for any type of transaction.
        /// </summary>
        private readonly Column<string> TransactionId = new Column<string>("MX Transaction Id", true);

        /// <summary>
        /// On-chain transaction ids. Specified only for deposists or withdrawals.
        /// </summary>
        private readonly Column<string> ChainTransactionId = new Column<string>("NT Transaction Id", false);

        /// <summary>
        /// The address to which funds where withdrawn. Specified only for withdrawals.
        /// </summary>

        private readonly Column<string> WithdrawAddress = new Column<string>("Withdraw addr", false);
        private readonly EnumColumn<TransactionType> Type = new EnumColumn<TransactionType>("Type", true);

        /// <summary>
        /// The currency involved in this transaction. Specified only for deposits or withdrawals.
        /// </summary>
        private readonly Column<string> Currency = new Column<string>("Currency", false);

        /// <summary>
        /// The trading symbol in the form BASE/QUOTE. Specified only for deals.
        /// </summary>
        private readonly Column<string> Pair = new Column<string>("Pair", false);

        /// <summary>
        /// The withdrawal fee. Specified only for withdrawals.
        /// </summary>
        private readonly DecimalColumn Fee = new DecimalColumn("Fee", false);

        /// <summary>
        /// The transaction amount in units of Currency (for deposits or withdrawals) or BASE (for deals). If for deposit, it is the amount AFTER the sending fees have been applied.
        /// </summary>
        private readonly DecimalColumn Amount = new DecimalColumn("Amount", true);

        /// <summary>
        /// The trading price. Specified only for deals.
        /// </summary>
        private readonly DecimalColumn Price = new DecimalColumn("Price", false);

        /// <summary>
        /// The total amount of quote currency involved in the transaction. Specified only for deals and withdrawals (where Fee + Amount = Total).
        /// </summary>
        private readonly DecimalColumn Total = new DecimalColumn("Total", false);

        /// <summary>
        /// Order side. Specified only for deals.
        /// </summary>
        private readonly EnumColumn<ActionType> Side = new EnumColumn<ActionType>("Action", false);

        /// <summary>
        /// Unused.
        /// </summary>
        private readonly Column<object> From = new Column<object>("From", false);

        /// <summary>
        /// Unused
        /// </summary>
        private readonly Column<object> To = new Column<object>("To", false);

        /// <summary>
        /// Time time of the transaction, in UTC.
        /// </summary>
        private readonly DateColumn Time = new DateColumn("Time", true, DATE_FORMAT, 0);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.MercatoX;
        public override Column[] Columns { get; }

        internal MercatoXSchema()
        {
            this.Columns = new Column[] {
                this.TransactionId,
                this.ChainTransactionId,
                this.WithdrawAddress,
                this.Type,
                this.Currency,
                this.Pair,
                this.Fee,
                this.Amount,
                this.Price,
                this.Total,
                this.Side,
                this.From,
                this.To,
                this.Time
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new MercatoXRecord(parsedRow);
        }
    }
}
