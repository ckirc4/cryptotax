﻿namespace CryptoTax.Persistence.Schema.Exchange.MercatoX
{
    internal enum ActionType
    {
        buy,
        sell
    }
}
