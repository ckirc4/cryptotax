﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Mathematics;
using UtilityLibrary.Types;
using GlobalTransactionType = CryptoTax.Shared.Enums.TransactionType;
using ExchangeEnum = CryptoTax.Shared.Enums.Exchange;
using CryptoTax.Shared.Exceptions;

namespace CryptoTax.Persistence.Schema.Exchange.MercatoX
{
    internal class MercatoXRecord : Record
    {
        public string TransactionId { get; set; }
        public string OnChainTransactionId { get; set; }
        public string WithdrawAddress { get; set; }
        public TransactionType Type { get; set; }
        public string Currency { get; set; }
        public string Pair { get; set; }
        public decimal? Fee { get; set; }
        public decimal Amount { get; set; }
        public decimal? Price { get; set; }
        /// <summary>
        /// Amount * Price, DOES NOT INCLUDE 0.25% fee... bastards!
        /// </summary>
        public decimal? Total { get; set; }
        public ActionType? Side { get; set; }
        public object From { get; set; }
        public object To { get; set; }
        public DateTime Time { get; set; }

        public MercatoXRecord(object[] parsedRow)
        {
            this.TransactionId = (string)parsedRow[0];
            this.OnChainTransactionId = (string)parsedRow[1];
            this.WithdrawAddress = (string)parsedRow[2];
            this.Type = (TransactionType)parsedRow[3];
            this.Currency = (string)parsedRow[4];
            this.Pair = (string)parsedRow[5];
            this.Fee = (decimal?)parsedRow[6];
            this.Amount = (decimal)parsedRow[7];
            this.Price = (decimal?)parsedRow[8];
            this.Total = (decimal?)parsedRow[9];
            this.Side = (ActionType?)parsedRow[10];
            this.From = parsedRow[11];
            this.To = parsedRow[12];
            this.Time = (DateTime)parsedRow[13];
        }

        public override TradeDTO ToTradeDTO()
        {
            if (this.Type != TransactionType.Deal) return null;

            DString quoteCurrency = this.Pair.Split('/')[1]; // important: quote currency comes AFTER base currency
            DString baseCurrency = this.Pair.Split('/')[0];
            SymbolDTO symbol = new SymbolDTO(quoteCurrency, baseCurrency);

            DString currencyDisposed = this.Side == ActionType.buy ? symbol.QuoteCurrency : symbol.BaseCurrency;
            decimal amountDisposed = this.Side == ActionType.buy ? (decimal)this.Total : this.Amount;
            DString currencyReceived = this.Side == ActionType.buy ? symbol.BaseCurrency : symbol.QuoteCurrency;
            decimal amountReceived = this.Side == ActionType.buy ? this.Amount : (decimal)this.Total;
            DString feeCurrency = symbol.QuoteCurrency;
            decimal feeAmount = Numerics.Round((decimal)this.Total * 0.0025m, 8);
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(ExchangeEnum.MercatoX), currencyDisposed, amountDisposed, currencyReceived, amountReceived, feeCurrency, feeAmount);

            return new TradeDTO(
                this.Time,
                ExchangeEnum.MercatoX,
                this.TransactionId,
                symbol,
                MarketType.Spot,
                this.Side == ActionType.buy ? OrderSide.Buy : OrderSide.Sell,
                (decimal)this.Price,
                containerEvent);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type == TransactionType.Deal) return null;

            GlobalTransactionType type;
            NString currencyReceived = null, currencyDisposed = null, feeCurrency = null;
            decimal? amountReceived = null, amountDisposed = null, feeAmount = null;

            if (this.Type == TransactionType.Deposit)
            {
                type = GlobalTransactionType.CryptoDeposit;
                currencyReceived = new DString(this.Currency);
                amountReceived = this.Amount;
            }
            else if (this.Type == TransactionType.Withdraw)
            {
                if (string.IsNullOrEmpty(this.OnChainTransactionId))
                {
                    // withdrawal failed
                    return null;
                }

                type = GlobalTransactionType.CryptoWithdrawal;
                currencyDisposed = new DString(this.Currency);
                amountDisposed = this.Amount;
                feeCurrency = new DString(this.Currency);
                feeAmount = this.Fee;
            }
            else throw new UnreachableCodeException($"Invalid MercatoX TransactionType '{this.Type}'");

            return new TransactionDTO(
                this.Time,
                ExchangeEnum.MercatoX,
                this.TransactionId,
                this.OnChainTransactionId,
                this.WithdrawAddress,
                type,
                new ContainerEventDTO(this.Time, Container.FromExchange(ExchangeEnum.MercatoX), currencyDisposed, amountDisposed, currencyReceived, amountReceived, feeCurrency, feeAmount));
        }
    }
}
