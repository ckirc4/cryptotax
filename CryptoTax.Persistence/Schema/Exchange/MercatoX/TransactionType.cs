﻿namespace CryptoTax.Persistence.Schema.Exchange.MercatoX
{
    internal enum TransactionType
    {
        Deposit,
        Withdraw,
        Deal
    }
}
