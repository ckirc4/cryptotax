﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexWalletRecord : BitMexBaseRecord
    {
        public WalletEventType WalletEventType { get; set; }
        public NString Address { get; set; }
        public NString Tx { get; set; }
        public WalletEventStatus Status { get; set; }

        public BitMexWalletRecord(object[] parsedRow) : base(parsedRow)
        {
            this.WalletEventType = (WalletEventType)parsedRow[14];
            this.Address = (string)parsedRow[15];
            this.Tx = (string)parsedRow[16];
            this.Status = (WalletEventStatus)parsedRow[17];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.WalletEventType == WalletEventType.RealisedPNL)
            {
                return null; // see record group
            }
            else if (this.WalletEventType == WalletEventType.Deposit)
            {
                if (this.Status != WalletEventStatus.Completed) throw new NotImplementedException("Status must be completed");

                Shared.Enums.Exchange exchange = Shared.Enums.Exchange.BitMex;
                decimal amountBTC = Helpers.ConvertToBTC(this.Amount, this.Currency);
                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(exchange), null, null, "BTC", amountBTC, null, null);
                return new TransactionDTO(this.Time, exchange, this.Id, null, null, TransactionType.CryptoDeposit, containerEvent);
            }
            else throw new NotImplementedException("Only support realised PNL and deposits at the moment");
        }
    }
}
