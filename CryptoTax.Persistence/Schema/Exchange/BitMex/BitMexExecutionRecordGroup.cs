﻿using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexExecutionRecordGroup : RecordGroup
    {
        private readonly BitMexExecutionRecord _Record;
        private readonly PositionDeltaType _PositionDeltaType;

        public override IEnumerable<Record> Group { get { return new List<BitMexExecutionRecord>() { this._Record }; } }

        public BitMexExecutionRecordGroup(BitMexExecutionRecord record, PositionDeltaType positionDeltaType)
        {
            this._Record = record;
            this._PositionDeltaType = positionDeltaType;
        }

        public override TradeDTO ToTradeDTO()
        {
            return BitMexExecutionRecord.ToTradeDTO(this._Record, this._PositionDeltaType);
        }

        public static IEnumerable<BitMexExecutionRecordGroup> ProcessAll(IEnumerable<BitMexExecutionRecord> allRecords)
        {
            // there will be one "group" per record. we just need to do this intermediate step because a record must be viewed in the context of records that came before it
            Dictionary<BitMexSymbol, decimal> positions = new Dictionary<BitMexSymbol, decimal>();
            foreach (BitMexSymbol symbol in Enum.GetValues(typeof(BitMexSymbol)))
            {
                positions.Add(symbol, 0);
            }

            List<BitMexExecutionRecordGroup> groups = new List<BitMexExecutionRecordGroup>();
            foreach (BitMexExecutionRecord record in allRecords.Where(r => r.ExecutionType == ExecutionType.Trade))
            {
                decimal positionDelta = record.Side == OrderSide.Buy ? record.Amount : -record.Amount;
                decimal currentPosition = positions[record.Symbol];

                PositionDeltaType positionDeltaType;
                if (currentPosition == 0 || Math.Sign(currentPosition) == Math.Sign(positionDelta))
                {
                    positionDeltaType = PositionDeltaType.Increasing;
                }
                else if (Math.Abs(positionDelta) <= Math.Abs(currentPosition))
                {
                    positionDeltaType = PositionDeltaType.Decreasing;
                }
                else
                {
                    // trade crosses position direction (i.e. skips position close). must split record
                    (BitMexExecutionRecord first, BitMexExecutionRecord second) = record.SplitAtAmount(Math.Abs(currentPosition));
                    positions[record.Symbol] = currentPosition + positionDelta;
                    groups.Add(new BitMexExecutionRecordGroup(first, PositionDeltaType.Decreasing));
                    groups.Add(new BitMexExecutionRecordGroup(second, PositionDeltaType.Increasing));
                    if (first.Amount + second.Amount != Math.Abs(positionDelta)) throw new Exception("Amounts do not add up");
                    if (first.Cost + second.Cost != record.Cost) throw new Exception("Costs do not add up");
                    if (record.Fee.HasValue && first.Fee.Value + second.Fee.Value != record.Fee.Value) throw new Exception("Fees do not add up");
                    continue;
                }

                positions[record.Symbol] = currentPosition + positionDelta;
                groups.Add(new BitMexExecutionRecordGroup(record, positionDeltaType));
            }

            if (positions.Values.Any(pos => pos != 0)) throw new Exception("One or more positions were not neutralised");
            return groups;
        }
    }
}
