﻿The position itself can be considered a special type of inventory to which we can add and subtract and derive a profit/loss when subtracting. It would allow us to deal with the problem of whether position changes are considered "trades" - normally we consider a trade to be an event in which we spend an asset to receive another asset, triggering a GCT. However, in our case, we are not buying an asset, but a contract. By explicitly treating them separately, we can handle the desired treatment.

Considering this is a model that we will probably re-use (because the spend-more-than-you-have model is relevant to other exchanges, even if settlement is done differently), might be a good idea to plan this out properly before beginning.

Let's call this inventory-like model a Position.
We can apply a separate `InventoryManagementType` to Positions.
`Positions` will have the following artifacts:
- **Exchange**
- **Symbol**: Uniquely identifies the position on this exchange
- **SettlementCurrency**: With what currency are profits/losses paid out?
- **Closed**: Whether the position is closed. If true, will not be able to add more position amounts.
- **IEnumerable<PositionAmount>**: The individual trades that this position is made up of, must all have the same direction
- **decimal GetPositionSize()**: Aggregates position amounts
- **Direction GetPositionDirection()**: Long or short
- **void IncreasePosition(positionAmount)**: Add to the position
- **decimal DecreasePosition(positionAmount)**: Remove from the position. Must not exceed position size, otherwise throws - caller may need to split up position amount for this to be possible. Uses difference in cost to calculate and return RealisedPNL in units of `SettlementCurrency`.

`PositionAmount` will have the following artifacts:
- **Exchange**
- **Symbol**
- **DateAcquired**
- **Size**: Negative if short
- **Cost**: Positive if value removd from account

When opening position:
- Instantiate new Position object, where we record the position size (number of contracts, negative if short), symbol, and cost (in BTC).
- Handle fees by directly adding/subtracting from the inventory as we do for spot trades already.

When increasing position:
- Add to the position size and increase the cost.
- Handle fees as above.

When decreasing position:
- Choose which position(s) to discard that offset the decreased position size, and calculate the profit using the difference in costs and converting to AUD.
- Handle fees as above.

When closing position:
- Same as decreasing position, except also invalidate/discard/reset the position object.

To communicate a contract trade, we can continue to use the `TradeDTO` model and specify the type of market. This way, we can let the trade processor handle it, and easily re-use the ContainerEventModel as well: the container works as before, as do fees (with the option of having negative fees), but the amount and currency sent and received are a little bit different:
- When going long on a contract, amount sent is the cost (currency SettlementCurrency) and amount received is the position size (currency Symbol)
- When going short on a contract, amount sent is the position size (currency Symbol) and amount received is the cost (currency SettlementCurrency)

Will need to do the following:
- PositionAmount model
- Position model
- A position manager that keeps track of all positions of all exchanges and handles opening/closing position as well as calculations
  - must be able to save and load positions
  - adds to global capital gains/income
