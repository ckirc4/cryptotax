﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexWalletRecordGroup : RecordGroup
    {
        private readonly List<BitMexWalletRecord> _Group;
        public override IEnumerable<Record> Group { get { return this._Group; } }

        public BitMexWalletRecordGroup(IEnumerable<BitMexWalletRecord> records)
        {
            this._Group = records.ToList();
        }

        public override TransactionDTO ToTransactionDTO()
        {
            DateTime time = this._Group.First().Time;
            DString id = this._Group.First().Id;

            decimal amount = Helpers.ConvertToBTC(this._Group.Sum(g => g.Amount), this._Group.First().Currency);
            if (amount == 0) return null;

            // negative amounts == outgoing (loss)
            string currencySent = amount < 0 ? "BTC" : null;
            decimal? amountSent = amount < 0 ? Math.Abs(amount) : (decimal?)null;
            string currencyReceived = amount > 0 ? "BTC" : null;
            decimal? amountReceived = amount > 0 ? Math.Abs(amount) : (decimal?)null;

            ContainerEventDTO containerEvent = new ContainerEventDTO(time, Container.FromExchange(Shared.Enums.Exchange.BitMex), currencySent, amountSent, currencyReceived, amountReceived, null, null);
            return new TransactionDTO(time, Shared.Enums.Exchange.BitMex, id, null, null, Shared.Enums.TransactionType.FuturesPnL, containerEvent);
        }

        public static IEnumerable<BitMexWalletRecordGroup> ProcessAll(IEnumerable<BitMexWalletRecord> allRecords)
        {
            // Every day, a PnL event is emitted for each open position (that is, one per symbol)
            // we want to aggregate them into a single PnL TransactionDTO

            List<BitMexWalletRecordGroup> groups = new List<BitMexWalletRecordGroup>();
            foreach (IGrouping<DateTime, BitMexWalletRecord> group in allRecords.Where(r => r.WalletEventType == WalletEventType.RealisedPNL).GroupBy(r => r.Time.Round(TimeSpan.FromHours(1))))
            {
                if (group.Select(g => g.Time).Distinct().Count() != 1) throw new Exception("All BitMex PnL payments should be occurring at the exact same time");
                else if (group.Select(g => g.Currency).Distinct().Count() != 1) throw new Exception("All BitMex PnL payments should be paid in the same currency");
                groups.Add(new BitMexWalletRecordGroup(group));
            }

            return groups;
        }
    }
}
