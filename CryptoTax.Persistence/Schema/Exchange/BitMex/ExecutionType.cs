﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum ExecutionType
    {
        Funding,
        Trade
    }
}