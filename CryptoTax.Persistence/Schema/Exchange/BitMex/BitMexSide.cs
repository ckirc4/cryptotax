﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    internal enum BitMexSide
    {
        Buy,
        Sell
    }
}