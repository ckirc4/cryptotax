﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexSchema : Schema
    {
        readonly Column<string> Id = new Column<string>("Id", true);
        /// <summary>
        /// In UTC.
        /// </summary>
        readonly DateColumn Time = new DateColumn("Time", true, new string[] { "d/MM/yyyy h:mm:ss tt" }, 0);
        readonly EnumColumn<RowType> Type = new EnumColumn<RowType>("RowType", true);
        readonly DecimalColumn Amount = new DecimalColumn("Amount", true);
        readonly DecimalColumn Fee = new DecimalColumn("Fee", false);
        readonly EnumColumn<BitMexCurrency> Currency = new EnumColumn<BitMexCurrency>("Currency", true, true);
        readonly Column<string> Description = new Column<string>("Text", false);

        readonly EnumColumn<ExecutionType> ExecutionType = new EnumColumn<ExecutionType>("ExecutionType", false);
        readonly EnumColumn<BitMexSymbol> Symbol = new EnumColumn<BitMexSymbol>("Symbol", false, true);
        readonly EnumColumn<BitMexSettlementCurrency> SettlementCurrency = new EnumColumn<BitMexSettlementCurrency>("SettlCurrency", false);
        readonly EnumColumn<BitMexSide> Side = new EnumColumn<BitMexSide>("Side", false);
        readonly DecimalColumn Price = new DecimalColumn("Price", false);
        readonly DecimalColumn Cost = new DecimalColumn("Cost", false);
        readonly EnumColumn<OrderType> OrderType = new EnumColumn<OrderType>("OrderType", false);

        readonly EnumColumn<WalletEventType> WalletEventType = new EnumColumn<WalletEventType>("WalletEventType", false);
        readonly Column<string> Address = new Column<string>("Address", false);
        readonly Column<string> Tx = new Column<string>("Tx", false);
        readonly EnumColumn<WalletEventStatus> WalletEventStatus = new EnumColumn<WalletEventStatus>("Status", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.BitMex;
        public override Column[] Columns { get; }

        public BitMexSchema()
        {
            this.Columns = new Column[]
            {
                this.Id,            // 0
                this.Time,          // 1
                this.Type,          // 2
                this.Amount,        // 3
                this.Fee,           // 4
                this.Currency,      // 5
                this.Description,   // 6
                
                this.ExecutionType,      // 7
                this.Symbol,             // 8
                this.SettlementCurrency, // 9
                this.Side,               // 10
                this.Price,              // 11
                this.Cost,               // 12
                this.OrderType,          // 13
                
                this.WalletEventType,    // 14
                this.Address,            // 15
                this.Tx,                 // 16
                this.WalletEventStatus   // 17
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            // this currently handles only WalletEvent.WalletEvent records
            RowType type = (RowType)parsedRow[2];

            if (type == RowType.Execution) return new BitMexEmptyRecord(); // new BitMexExecutionRecord(parsedRow);
            else if (type == RowType.Wallet) return new BitMexWalletRecord(parsedRow);
            else throw new NotImplementedException();
        }

        public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
        {
            // this currently handles only WalletEvent.RealisedPnl records
            return new List<RecordGroup>()
                //.Concat(BitMexExecutionRecordGroup.ProcessAll(allRecords.Where(r => r is BitMexExecutionRecord).Cast<BitMexExecutionRecord>()))
                .Concat(BitMexWalletRecordGroup.ProcessAll(allRecords.Where(r => r is BitMexWalletRecord).Cast<BitMexWalletRecord>()));
        }
    }
}
