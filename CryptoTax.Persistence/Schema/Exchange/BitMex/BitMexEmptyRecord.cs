﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    /// <summary>
    /// Use this instead of execution record for now.
    /// </summary>
    public class BitMexEmptyRecord : Record { }
}
