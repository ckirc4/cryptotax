﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexBaseRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public RowType RowType { get; set; }
        public decimal Amount { get; set; }
        public decimal? Fee { get; set; }
        public BitMexCurrency Currency { get; set; }
        public NString Description { get; set; }

        public BitMexBaseRecord(object[] parsedRow)
        {
            this.Id = (string)parsedRow[0];
            this.Time = (DateTime)parsedRow[1];
            this.RowType = (RowType)parsedRow[2];
            this.Amount = (decimal)parsedRow[3];
            this.Fee = (decimal?)parsedRow[4];
            this.Currency = (BitMexCurrency)parsedRow[5];
            this.Description = (string)parsedRow[6];
        }

        protected BitMexBaseRecord(
            DString id,
            DateTime time,
            RowType rowType,
            decimal amount,
            decimal? fee,
            BitMexCurrency currency,
            NString description)
        {
            this.Id = id;
            this.Time = time;
            this.RowType = rowType;
            this.Amount = amount;
            this.Fee = fee;
            this.Currency = currency;
            this.Description = description;
        }
    }
}
