﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum BitMexCurrency
    {
        USD,
        /// <summary>
        /// Bitcoin
        /// </summary>
        XBT,
        /// <summary>
        /// Satoshi
        /// </summary>
        XBt
    }
}