﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum BitMexSymbol
    {
        // perpetual contracts
        XBTUSD,
        ETHUSD,

        // futures contracts
        ADAU19,
        BCHU19,
        BCHZ19,
        EOSU19,
        EOSZ19,
        ETHU19,
        ETHZ19,
        LTCU19,
        LTCZ19,
        TRXM19,
        TRXU19,
        TRXZ19,
        XBTH20,
        XBTU19,
        XBTZ19,
        XRPM19,
        XRPU19,
        XRPZ19,
    }
}
