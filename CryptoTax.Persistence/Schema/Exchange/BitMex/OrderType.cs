﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum OrderType
    {
        Limit,
        Market,
        Stop,
        StopLimit
    }
}