﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum WalletEventType
    {
        Deposit,
        /// <summary>
        /// Positive means profit, negative means loss. Confirmed to includes fees and funding payments. Not affected by unrealised pnl.
        /// </summary>
        RealisedPNL,
    }
}