﻿using CryptoTax.DTO;
using System;
using UtilityLibrary.Types;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public class BitMexExecutionRecord : BitMexBaseRecord
    {
        private static Shared.Enums.Exchange _Exchange = Shared.Enums.Exchange.BitMex;

        public ExecutionType ExecutionType { get; set; }
        public BitMexSymbol Symbol { get; set; }
        public BitMexSettlementCurrency SettlementCurrency { get; set; }
        public OrderSide? Side { get; set; }
        public decimal Price { get; set; }
        public decimal Cost { get; set; }
        public OrderType OrderType { get; set; }

        public BitMexExecutionRecord(object[] parsedRow) : base(parsedRow)
        {
            this.ExecutionType = (ExecutionType)parsedRow[7];
            this.Symbol = (BitMexSymbol)parsedRow[8];
            this.SettlementCurrency = (BitMexSettlementCurrency)parsedRow[9];
            BitMexSide? side = (BitMexSide?)parsedRow[10];
            this.Side = side == null ? (OrderSide?)null : side == BitMexSide.Buy ? OrderSide.Buy : OrderSide.Sell;
            this.Price = (decimal)parsedRow[11];
            this.Cost = (decimal)parsedRow[12];
            this.OrderType = (OrderType)parsedRow[13];
        }

        private BitMexExecutionRecord(
            DString id,
            DateTime time,
            RowType rowType,
            decimal amount,
            decimal? fee,
            BitMexCurrency currency,
            NString description,
            ExecutionType executionType,
            BitMexSymbol symbol,
            BitMexSettlementCurrency settlementCurrency,
            OrderSide? side,
            decimal price,
            decimal cost,
            OrderType orderType) : base(id, time, rowType, amount, fee, currency, description)
        {
            this.ExecutionType = executionType;
            this.Symbol = symbol;
            this.SettlementCurrency = settlementCurrency;
            this.Side = side;
            this.Price = price;
            this.Cost = cost;
            this.OrderType = orderType;
        }

        /// <summary>
        /// Can't override the Record.ToTradeDTO because we don't know whether the position is increasing or decreasing only by the information in a single record - must take into account records before it, hence this must be called by the BitMexRecordGroup.
        /// </summary>
        public static TradeDTO ToTradeDTO(BitMexExecutionRecord record, PositionDeltaType positionDelta)
        {
            if (record.ExecutionType != ExecutionType.Trade) return null;

            OrderSide side = record.Side.Value;
            SymbolDTO symbol = record.Symbol.ToDTO();
            MarketType marketType = record.Symbol.GetMarketType();

            // amountSent and amountReceived are purposefully always positive to avoid confusion. position delta can be used if, for example "buying shorts" has to be modelled (sell order side with increasing position delta)
            decimal cost = Helpers.SatsToBTC(Math.Abs(record.Cost));

            DString currencySent = positionDelta == PositionDeltaType.Increasing ? "BTC" : record.Symbol.ToString();
            decimal amountSent = positionDelta == PositionDeltaType.Increasing ? cost : record.Amount;
            DString currencyReceived = positionDelta == PositionDeltaType.Increasing ? record.Symbol.ToString() : "BTC";
            decimal amountReceived = positionDelta == PositionDeltaType.Increasing ? record.Amount : cost;
            DString feeCurrency = "BTC";
            decimal feeAmount = Helpers.SatsToBTC(record.Fee.Value);
            ContainerEventDTO containerEvent = new ContainerEventDTO(record.Time, Container.FromExchange(_Exchange), currencySent, amountSent, currencyReceived, amountReceived, feeCurrency, feeAmount);

            return new TradeDTO(record.Time, _Exchange, record.Id, symbol, marketType, side, record.Price, containerEvent);
        }

        /// <summary>
        /// Splits the record such that the Amount of the first record is amount, the size of the second record is the left-over amount, and all other values are re-proportioned such that first + original = second. 
        /// </summary>
        public (BitMexExecutionRecord first, BitMexExecutionRecord second) SplitAtAmount(decimal amount)
        {
            if (amount <= 0 || amount >= this.Amount) throw new ArgumentException("Amount must be at least zero and at most the existing amount");
            decimal frac = amount / this.Amount;

            DString id1 = this.Id + "_split1";
            decimal amount1 = amount;
            decimal cost1 = Math.Round(this.Cost * frac);
            decimal? fee1 = this.Fee.HasValue ? Math.Round(this.Fee.Value * frac) : (decimal?)null;

            DString id2 = this.Id + "_split2";
            decimal amount2 = this.Amount - amount;
            decimal cost2 = this.Cost - cost1;
            decimal? fee2 = this.Fee.HasValue ? this.Fee.Value - fee1.Value : (decimal?)null;

            /*
            // reduce position by `amount` to close, then increase position by remainder.
            decimal cost1, fee1, cost2, fee2;
            if (frac == 0.5m) throw new Exception("Indeterminate behaviour");
            else if (frac < 0.5m)
            {
                // closing small position, opening large position (i.e. net outgoing)
                // (know net because this trade is happening at the same price, so cost is proportional to amount)
                
            }
            */


            if (amount1 == 0 || amount2 == 0 || Math.Abs(cost1) == 0 || Math.Abs(cost2) == 0 || fee1.HasValue && Math.Abs(fee1.Value) == 0 || fee2.HasValue && Math.Abs(fee2.Value) == 0) throw new Exception("Values of a record cannot be zero");

            BitMexExecutionRecord first = new BitMexExecutionRecord(id1, this.Time, this.RowType, amount1, fee1, this.Currency, this.Description, this.ExecutionType, this.Symbol, this.SettlementCurrency, this.Side, this.Price, cost1, this.OrderType);
            BitMexExecutionRecord second = new BitMexExecutionRecord(id2, this.Time, this.RowType, amount2, fee2, this.Currency, this.Description, this.ExecutionType, this.Symbol, this.SettlementCurrency, this.Side, this.Price, cost2, this.OrderType);

            return (first, second);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.ExecutionType != ExecutionType.Funding) return null;

            bool received = this.Fee < 0;
            decimal amount = Math.Abs(this.Fee.Value);
            NString currencyReceived = received ? "BTC" : null;
            decimal? amountReceived = received ? Helpers.SatsToBTC(amount): (decimal?)null;
            NString currencySent = received ? null : "BTC";
            decimal? amountSent = received ? (decimal?)null : Helpers.SatsToBTC(amount);

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(_Exchange), currencySent, amountSent, currencyReceived, amountReceived, null, null);

            return new TransactionDTO(this.Time, _Exchange, this.Id, null, null, TransactionType.FuturesFunding, containerEvent);
        }
    }
}

