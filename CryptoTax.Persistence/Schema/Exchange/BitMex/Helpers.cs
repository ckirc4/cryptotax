﻿using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;

namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public static class Helpers
    {
        public static decimal ConvertToBTC(decimal amount, BitMexCurrency currency)
        {
            if (currency == BitMexCurrency.XBT) return amount;
            else if (currency == BitMexCurrency.XBt) return amount / 100_000_000;
            else throw new ArgumentException("Cannot statically convert USD to BTC");
        }

        public static SymbolDTO ToDTO(this BitMexSymbol symbol)
        {
            return new SymbolDTO("BTC", symbol.ToString());
        }

        public static MarketType GetMarketType(this BitMexSymbol symbol)
        {
            switch (symbol)
            {
                case BitMexSymbol.XBTUSD:
                case BitMexSymbol.ETHUSD:
                    return MarketType.DerivativesFuturesPerpetual;

                case BitMexSymbol.ADAU19:
                case BitMexSymbol.BCHU19:
                case BitMexSymbol.BCHZ19:
                case BitMexSymbol.EOSU19:
                case BitMexSymbol.EOSZ19:
                case BitMexSymbol.ETHU19:
                case BitMexSymbol.ETHZ19:
                case BitMexSymbol.LTCU19:
                case BitMexSymbol.LTCZ19:
                case BitMexSymbol.TRXM19:
                case BitMexSymbol.TRXU19:
                case BitMexSymbol.TRXZ19:
                case BitMexSymbol.XBTH20:
                case BitMexSymbol.XBTU19:
                case BitMexSymbol.XBTZ19:
                case BitMexSymbol.XRPM19:
                case BitMexSymbol.XRPU19:
                case BitMexSymbol.XRPZ19:
                    return MarketType.DerivativesFutures;
                default:
                    throw new AssertUnreachable(symbol);
            }

            throw new NotImplementedException();
        }

        public static decimal SatsToBTC(decimal sats)
        {
            return sats / 100_000_000;
        }
    }
}
