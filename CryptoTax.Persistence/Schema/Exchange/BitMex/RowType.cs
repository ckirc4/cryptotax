﻿namespace CryptoTax.Persistence.Schema.Exchange.BitMex
{
    public enum RowType
    {
        /// <summary>
        /// Row relates to an execution that has changed the balance in some way, such as a trade or funding payment.
        /// </summary>
        Execution,
        /// <summary>
        /// Row relates directly to the wallet, such as a deposit or withdrawal, but also daily summary of realised profit and loss.
        /// </summary>
        Wallet
    }
}