﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public class KrakenLedgerRecord : Record
    {
        string Id { get; set; }
        DateTime Time { get; set; }
        string Asset { get; set; }
        decimal Amount { get; set; }
        KrakenLedgerType Type { get; set; }

        public KrakenLedgerRecord(object[] parsedRow)
        {
            this.Id = (string)parsedRow[0];
            this.Time = (DateTime)parsedRow[2];
            this.Type = (KrakenLedgerType)parsedRow[3];

            object subtype = parsedRow[4];
            if (subtype != null)
            {
                throw new Exception($"Expected subtype to be null but received {subtype}");
            }

            KrakenLedgerAClass aclass = (KrakenLedgerAClass)parsedRow[5];
            if (aclass != KrakenLedgerAClass.currency)
            {
                throw new Exception($"Expected subtype to be 'currency' but received {aclass}");
            }

            string wallet = (string)parsedRow[7];
            if (wallet != "spot / main")
            {
                throw new Exception($"Expected wallet to be 'spot / main' but received {wallet}");
            }

            this.Asset = (string)parsedRow[6];
            this.Amount = (decimal)parsedRow[8];

            decimal fee = (decimal)parsedRow[9];
            if (fee > 0 && this.Type != KrakenLedgerType.trade)
            {
                throw new Exception($"Expected fee of 0 for a non-trade but received {fee}");
            }
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type == KrakenLedgerType.trade) return null;

            TransactionType type;
            NString currencySent = null;
            decimal? amountSent = null;
            NString currencyReceived = null;
            decimal? amountReceived = null;
            if (this.Type == KrakenLedgerType.withdrawal && this.Asset == KnownCurrencies.AUD.Symbol)
            {
                // only support AUD withdrawals, no crypto withdrawals
                type = TransactionType.FiatWithdrawal;
                currencySent = this.Asset;
                amountSent = -this.Amount;

                if (amountSent <= 0)
                {
                    throw new Exception($"Amount sent cannot be negative for a widthdrawal, but was {amountSent}");
                }
            }
            else if (this.Type == KrakenLedgerType.deposit && this.Asset != KnownCurrencies.AUD.Symbol)
            {
                // only support crypto deposits, not fiat deposits
                type = TransactionType.CryptoDeposit;
                currencyReceived = this.Asset;
                amountReceived = this.Amount;
            }
            else
            {
                throw new Exception($"Invalid type {this.Type} for asset {this.Asset}");
            }

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Kraken), currencySent, amountSent, currencyReceived, amountReceived, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Kraken,
                this.Id, null, null, type, containerEvent);
        }
    }
}
