﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Mathematics;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public class KrakenTradeRecord : Record
    {
        string Id { get; set; }
        DateTime Time { get; set; }
        DString CurrencyReceived { get; set; }
        decimal AmountReceived { get; set; }
        DString CurrencySent { get; set; }
        decimal AmountSent { get; set; }
        DString FeeCurrency { get; set; }
        decimal Fee { get; set; }
        KrakenTradeType Type{ get; set; }
        decimal Price { get; set; }
        SymbolDTO Symbol { get; set; }

        public KrakenTradeRecord (object[] parsedRow)
        {
            this.Id = (string)parsedRow[0];
            this.Time = (DateTime)parsedRow[3];

            string pair = (string)parsedRow[2];
            string[] splitPair = pair.Split('/');
            string baseAsset = splitPair[0];
            string quoteAsset = splitPair[1];
            this.Symbol = new SymbolDTO(quoteAsset, baseAsset);
            this.Type = (KrakenTradeType)parsedRow[4];

            decimal reportedPrice = (decimal)parsedRow[6];
            decimal cost = (decimal)parsedRow[7];
            decimal fee = (decimal)parsedRow[8];
            decimal vol = (decimal)parsedRow[9];

            decimal calculatedPrice = cost / vol;
            if (Numerics.Round(calculatedPrice, 5) != Numerics.Round(reportedPrice, 5))
            {
                throw new Exception($"Expected price {calculatedPrice} but got {reportedPrice} for id {this.Id}");
            }

            // using the calculated price gives us extra precision to prevent false positives later on when checking the trade's consistency
            this.Price = calculatedPrice;

            if (this.Type == KrakenTradeType.buy)
            {
                this.CurrencySent = quoteAsset;
                this.CurrencyReceived = baseAsset;
                this.AmountSent = cost;
                this.AmountReceived = vol;
                this.FeeCurrency = quoteAsset;
                this.Fee = fee;
            }
            else if (this.Type == KrakenTradeType.sell)
            {
                this.CurrencySent = baseAsset;
                this.CurrencyReceived = quoteAsset;
                this.AmountSent = vol;
                this.AmountReceived = cost;
                this.FeeCurrency = quoteAsset;
                this.Fee = fee;
            }
            else
            {
                throw new Exception($"Invalid trade type ${this.Type}");
            }

            decimal margin = (decimal)parsedRow[10];
            if (margin > 0)
            {
                throw new Exception($"Expected margin to be 0 but received {margin}");
            }

            string misc = (string)parsedRow[11];
            if (misc != null && misc != "initiated")
            {
                throw new Exception($"Expected misc to be null or 'initiated' but received {misc}");
            }
        }

        public override TradeDTO ToTradeDTO()
        {
            OrderSide side = this.Type == KrakenTradeType.buy ? OrderSide.Buy : OrderSide.Sell;
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Kraken), this.CurrencySent, this.AmountSent, this.CurrencyReceived, this.AmountReceived, this.FeeCurrency, this.Fee);
            return new TradeDTO(this.Time, Shared.Enums.Exchange.Kraken,
                this.Id, this.Symbol, MarketType.Spot, side, this.Price, containerEvent);
        }
    }
}
