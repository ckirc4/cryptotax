﻿namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public enum KrakenTradeType
    {
        buy, sell
    }
}
