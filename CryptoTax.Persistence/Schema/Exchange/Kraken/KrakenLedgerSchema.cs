﻿namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public class KrakenLedgerSchema : Schema
    {
        readonly Column<string> txid = new Column<string>("txid", true);
        readonly Column<string> refid = new Column<string>("refid", true);
        readonly DateColumn time = new DateColumn("time", true, 0);
        readonly EnumColumn<KrakenLedgerType> type = new EnumColumn<KrakenLedgerType>("type", true);
        readonly EnumColumn<KrakenLedgerSubType> subtype = new EnumColumn<KrakenLedgerSubType>("subtype", false);
        readonly EnumColumn<KrakenLedgerAClass> aclass = new EnumColumn<KrakenLedgerAClass>("aclass", true);
        readonly Column<string> asset = new Column<string>("asset", true);
        readonly Column<string> wallet = new Column<string>("wallet", true);
        readonly DecimalColumn amount = new DecimalColumn("amount", true);
        readonly DecimalColumn fee = new DecimalColumn("fee", true);
        readonly DecimalColumn balance = new DecimalColumn("balance", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Kraken_Ledgers;
        public override Column[] Columns { get; }

        public KrakenLedgerSchema()
        {
            this.Columns = new Column[]
            {
                this.txid, // 0
                this.refid, // 1
                this.time, // 2
                this.type, // 3
                this.subtype, // 4
                this.aclass, // 5
                this.asset, // 6
                this.wallet, // 7
                this.amount, // 8
                this.fee, // 9
                this.balance // 10
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new KrakenLedgerRecord(parsedRow);
        }
    }
}
