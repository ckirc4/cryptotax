﻿namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public enum KrakenLedgerType
    {
        deposit,
        trade,
        withdrawal
    }
}
