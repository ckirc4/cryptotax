﻿namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public enum KrakenTradeOrderType
    {
        market, limit
    }
}
