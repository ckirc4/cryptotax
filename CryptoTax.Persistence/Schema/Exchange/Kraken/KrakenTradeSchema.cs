﻿namespace CryptoTax.Persistence.Schema.Exchange.Kraken
{
    public class KrakenTradeSchema : Schema
    {
        readonly Column<string> txid = new Column<string>("txid", true);
        readonly Column<string> ordertxid = new Column<string>("ordertxid", true);
        readonly Column<string> pair = new Column<string>("pair", true);
        readonly DateColumn time = new DateColumn("time", true, 0);
        readonly Column<KrakenTradeType> type = new EnumColumn<KrakenTradeType>("type", true);
        readonly Column<KrakenTradeOrderType> ordertype = new EnumColumn<KrakenTradeOrderType>("ordertype", true);
        readonly DecimalColumn price = new DecimalColumn("price", true);
        readonly DecimalColumn cost = new DecimalColumn("cost", true);
        readonly DecimalColumn fee = new DecimalColumn("fee", true);
        readonly DecimalColumn vol = new DecimalColumn("vol", true);
        readonly DecimalColumn margin = new DecimalColumn("margin", true);
        readonly Column<string> misc = new Column<string>("misc", false);
        readonly Column<string> ledgers = new Column<string>("ledgers", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Kraken_Trades;
        public override Column[] Columns { get; }

        public KrakenTradeSchema()
        {
            this.Columns = new Column[]
            {
                this.txid, // 0
                this.ordertxid, // 1
                this.pair, // 2
                this.time, // 3
                this.type, // 4
                this.ordertype, // 5
                this.price, // 6
                this.cost, // 7
                this.fee, // 8
                this.vol, // 9
                this.margin, // 10
                this.misc, // 11
                this.ledgers // 12
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new KrakenTradeRecord(parsedRow);
        }
    }
}
