﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public class BTCMarketsSchema : Schema
    {
        /// <summary>
        /// Internal id. Each row has a unique id.
        /// </summary>
        private readonly Column<long> Id = new Column<long>("transactionId", true);

        /// <summary>
        /// UTC time of the transaction.
        /// </summary>
        private readonly DateColumn Time = new DateColumn("creationTime", true);

        /// <summary>
        /// Unused.
        /// </summary>
        private readonly Column<object> RecordType = new Column<object>("recordType", true);

        /// <summary>
        /// The type of action.
        /// </summary>
        private readonly EnumColumn<ActionType> ActionType = new EnumColumn<ActionType>("action", true);

        /// <summary>
        /// The currency symbol that is added or subtracted. Must be one of: AUD, BCH, BTC, ETC, ETH, LTC, OMG, XRP.
        /// </summary>
        private readonly Column<string> Currency = new Column<string>("currency", true);

        /// <summary>
        /// The amount of `currency` that is added (positive) or subtracted (negative) - never zero.
        /// </summary>
        private readonly DecimalColumn Amount = new DecimalColumn("amount", true);

        /// <summary>
        /// Fully/partially matched: the outgoing funds. Trade settled: the incoming funds.
        /// Example: When buying A XRP at price P, A*P AUD will be subtracted in the "matched" row, then A XRP will be added in the "settled" row, then some additional AUD will be subtracted in the "fee" row. The description amount may not be 100% accurate due to rounding - always use the `amount` column instead.
        /// </summary>
        private readonly Column<string> Description = new Column<string>("description", true);

        /// <summary>
        ///  The new balance of the `currency` after the `amount` has been added/subtracted.
        /// </summary>
        private readonly DecimalColumn Balance = new DecimalColumn("balance", true);

        /// <summary>
        /// The event that this row refers to. A single event can lead to multiple rows (e.g. a trade consists of a partial/full match, settle, and fee row that all reference the same event). If there are multiple fills of an order, they will refer to the same event but the timestamps may be spread out.
        /// </summary>
        private readonly Column<long> Reference = new Column<long>("referenceId", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.BtcMarkets;
        public override Column[] Columns { get; }

        public BTCMarketsSchema()
        {
            this.Columns = new Column[] {
            this.Id, // 0
            this.Time, // 1
            this.RecordType, // 2
            this.ActionType, // 3
            this.Currency, // 4
            this.Amount, // 5
            this.Description, // 6
            this.Balance, // 7
            this.Reference // 8
        };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BTCMarketsRecord(parsedRow);
        }

        public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
        {
            return BTCMarketsRecordGroup.ProcessAll(allRecords.Cast<BTCMarketsRecord>());
        }
    }
}
