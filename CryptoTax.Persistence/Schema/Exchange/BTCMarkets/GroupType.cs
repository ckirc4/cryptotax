﻿namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public enum GroupType
    {
        /// <summary>
        /// A buy, buy, and trading fee; or sell, sell and trading fee.
        /// </summary>
        TradeWithFee,
        /// <summary>
        /// A buy, buy; or sell, sell.
        /// </summary>
        TradeWithoutFee,
        /// <summary>
        /// Deposit immediately followed by withdrawal (Poli only).
        /// </summary>
        DepositWithFee,
        /// <summary>
        /// Single deposit or withdrawal.
        /// </summary>
        FundsTransfer
    }
}
