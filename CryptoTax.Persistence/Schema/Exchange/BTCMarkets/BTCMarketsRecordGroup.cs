﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public class BTCMarketsRecordGroup : RecordGroup
    {
        private const Shared.Enums.Exchange _EXCHANGE = Shared.Enums.Exchange.BTCMarkets;
        private const MarketType _MARKET_TYPE = MarketType.Spot;
        private readonly IEnumerable<BTCMarketsRecord> _Group;
        private readonly Container _Container;

        public readonly DateTime Time;
        public readonly GroupType Type;

        public override IEnumerable<Record> Group { get { return this._Group; } }

        /// <summary>
        /// Expects that there is only one configuration of records for each group type, and thus does not do any argument checking.
        /// </summary>
        public BTCMarketsRecordGroup(IEnumerable<BTCMarketsRecord> group, GroupType type)
        {
            if (group.Max(r => r.Time).Subtract(group.Min(r => r.Time)) > TimeSpan.FromSeconds(1)) throw new ArgumentException("Records in a BTCMarketsRecordGroup should all occur at the exact same time.");
            else if (group.Select(r => r.Reference).Distinct().Count() != 1) throw new ArgumentException("Records in a BTCMarketsRecordGroup should all reference the same event.");

            this._Group = group;
            this.Type = type;
            this.Time = group.First().Time;
            this._Container = Container.FromExchange(_EXCHANGE);
        }

        public bool TryAddLonelyRecord(BTCMarketsRecord lonelyRecord)
        {
            if (this.Type == GroupType.FundsTransfer || this.Type == GroupType.DepositWithFee) throw new Exception("Cannot add a lonely record to a funds transfer group");

            // check that amount sign, type and currency match, then add (by replacing with new record) and return true. otherwise, return false.
            bool added = false;

            foreach (BTCMarketsRecord record in this._Group.ToList())
            {
                if (record.Action == lonelyRecord.Action
                    && record.Currency == lonelyRecord.Currency
                    && Math.Sign(record.Amount) == Math.Sign(lonelyRecord.Amount))
                {
                    this._Group.Single(r => r == record).AddAmount(lonelyRecord.Amount);
                    added = true;
                    break;
                }
            }

            return added;
        }

        public override TradeDTO ToTradeDTO()
        {
            if (this.Type == GroupType.TradeWithFee || this.Type == GroupType.TradeWithoutFee)
            {
                BTCMarketsRecord outgoing = this._Group.ElementAt(0); // when buying, outgoing currency is quote currency, otherwise base currency
                BTCMarketsRecord incoming = this._Group.ElementAt(1); // when buying, incoming currency is base currency, otherwise quote currency
                BTCMarketsRecord fee = this.Type == GroupType.TradeWithFee ? this._Group.ElementAt(2) : null; // always negative amount

                OrderSide side = outgoing.Action == ActionType.BuyOrder ? OrderSide.Buy : OrderSide.Sell;
                string quoteCurrency = side == OrderSide.Buy ? outgoing.Currency : incoming.Currency;
                string baseCurrency = side == OrderSide.Buy ? incoming.Currency : outgoing.Currency;
                SymbolDTO symbol = new SymbolDTO(quoteCurrency, baseCurrency);
                decimal price = side == OrderSide.Buy ? -outgoing.Amount / incoming.Amount : incoming.Amount / -outgoing.Amount;

                string currencySent = outgoing.Currency;
                decimal amountSent = -outgoing.Amount;

                string currencyReceived = incoming.Currency;
                decimal amountReceived = incoming.Amount;

                NString feeCurrency = fee == null ? null : (NString)fee.Currency;
                decimal? feeAmount = -fee?.Amount ?? null;

                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, this._Container, currencySent, amountSent, currencyReceived, amountReceived, feeCurrency, feeAmount);

                return new TradeDTO(this.Time, _EXCHANGE, outgoing.Id.ToString(), symbol, _MARKET_TYPE, side, price, containerEvent);
            }
            else
            {
                return null;
            }
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type == GroupType.DepositWithFee)
            {
                return this.handleDepositWithFee(this._Group.Single(r => r.Action == ActionType.Deposit), this._Group.Single(r => r.Action == ActionType.Withdraw));
            }
            else if (this.Type == GroupType.FundsTransfer)
            {
                return this.handleFundsTransfer(this._Group.Single());
            }
            else
            {
                return null;
            }
        }

        private TransactionDTO handleFundsTransfer(BTCMarketsRecord record)
        {
            NString publicId = null; // on-chain transaction hash
            TransactionType txType;
            NString currencySent, currencyReceived;
            decimal? amountSent, amountReceived;

            if (record.Action == ActionType.Deposit)
            {
                // we never get any sort of transaction info for deposits.

                currencySent = null;
                amountSent = null;
                currencyReceived = record.Currency;
                amountReceived = record.Amount;

                if (record.Currency == KnownCurrencies.AUD.Symbol)
                {
                    txType = TransactionType.FiatDeposit;
                }
                else
                {
                    if (DescriptionParser.IsForkDeposit(record.Description))
                    {
                        txType = TransactionType.Fork;
                    }
                    else if (DescriptionParser.IsAirdropDeposit(record.Description))
                    {
                        txType = TransactionType.StakingOrAirdrops;
                    }
                    else
                    {
                        txType = TransactionType.CryptoDeposit;
                    }

                    // may be null
                    publicId = DescriptionParser.TryGetTransactionHash(record.Action, record.Description);
                }
            }
            else if (record.Action == ActionType.Withdraw)
            {
                // crypto withdrawals should contain the transaction hash

                currencySent = record.Currency;
                amountSent = -record.Amount;
                currencyReceived = null;
                amountReceived = null;

                if (record.Currency == KnownCurrencies.AUD.Symbol)
                {
                    txType = TransactionType.FiatWithdrawal;
                }
                else
                {
                    txType = TransactionType.CryptoWithdrawal;
                    publicId = DescriptionParser.TryGetTransactionHash(record.Action, record.Description);
                    if (publicId == null) throw new Exception($"Could not find transaction hash in description {record.Description}");
                }
            }
            else
            {
                throw new NotImplementedException($"Cannot process transfer for action of type {record.Action}");
            }


            // note: fees are not explicitly stated in the trade logs - must be inferred later on when the Transfer object is generated.
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, this._Container, currencySent, amountSent, currencyReceived, amountReceived, null, null);

            // the counterparty is never known because the trade logs don't expose that information. will have to either poll various chain explorers to get this info (+ will also show fees)
            // or modify Transfer matching logic so that we can infer when we sent funds from one container to another.
            TransactionDTO transfer = new TransactionDTO(this.Time, _EXCHANGE, record.Id.ToString(), publicId, null, txType, containerEvent);
            return transfer;
        }

        private TransactionDTO handleDepositWithFee(BTCMarketsRecord deposit, BTCMarketsRecord fee)
        {
            TransactionDTO transfer = this.handleFundsTransfer(deposit);
            transfer.ContainerEvent.FeeAmount = -fee.Amount;
            transfer.ContainerEvent.FeeCurrency = fee.Currency;

            return transfer;
        }

        public static IEnumerable<BTCMarketsRecordGroup> ProcessAll(IEnumerable<BTCMarketsRecord> allRecords)
        {
            List<BTCMarketsRecordGroup> groups = new List<BTCMarketsRecordGroup>();
            Queue<BTCMarketsRecord> stack = new Queue<BTCMarketsRecord>(allRecords);

            foreach (IGrouping<long, BTCMarketsRecord> eventGroup in allRecords.GroupBy(r => r.Reference))
            {
                // go through each very small amount, and combine this record with the one before (if possible - try first) or after (if possible), and if not possible throw.
                // possible combinations are those where the currency, type and amount matches. we do not care about times.

                ActionType firstAction = eventGroup.First().Action;

                if (firstAction == ActionType.BuyOrder || firstAction == ActionType.SellOrder)
                {
                    // we expect all trades to be made up of
                    // - either a collection of 3 records, which follows the pattern of [firstAction, firstAction, tradingFee]
                    // - or a collection of 2 records, which follows the pattern of [firstAction, firstAction]
                    // a sequence of trades can be a mix between the two above types.

                    // sometimes, if a very very small amount is bought/sold, an AUD record will not be triggered until later.
                    // we want to discard these records (known as lonely records), but add the amounts to records that are part of actual groups.
                    // to detect these, we look for jumps greater than 1 in the id.
                    IEnumerable<BTCMarketsRecord> lonelyRecords = new List<BTCMarketsRecord>();
                    BTCMarketsRecord prev = null;
                    int prevLength = 0;
                    foreach (BTCMarketsRecord record in eventGroup)
                    {
                        if (prev == null)
                        {
                            prev = record;
                            prevLength++;
                            continue;
                        }

                        if (record.Id - prev.Id > 1)
                        {
                            // this record is detached from its previous one - if the previous record was not part of a group, it must be a lonely record
                            if (prevLength == 1 && Math.Abs(prev.Amount) < 1e-5m)
                            {
                                lonelyRecords = lonelyRecords.Append(prev);
                            }

                            // reset group - this is not always 100% correct, because jumps in ids are not unheard of - hence the Amount check above
                            prevLength = 0;
                        }

                        prev = record;
                        prevLength++;
                    }

                    // trailing lonely record
                    if (prevLength == 1 && Math.Abs(prev.Amount) < 1e-5m)
                    {
                        lonelyRecords = lonelyRecords.Append(prev);
                    }

                    List<BTCMarketsRecordGroup> thisGroups = new List<BTCMarketsRecordGroup>();
                    List<BTCMarketsRecord> currentGroup = new List<BTCMarketsRecord>();

                    foreach (BTCMarketsRecord record in eventGroup)
                    {
                        if (lonelyRecords.Contains(record))
                        {
                            // Console.WriteLine($"BTCMarkets: Ignoring lonely record {record.ToInlineString()}");
                            continue;
                        };

                        if ((currentGroup.Count == 0 || currentGroup.Count == 1) && record.Action != firstAction)
                        {
                            if (currentGroup.Count == 1 && record.Action == ActionType.TradingFee)
                            {
                                // we missed two lonely records because they are linked
                                lonelyRecords = lonelyRecords.Append(currentGroup.Single());
                                lonelyRecords = lonelyRecords.Append(record);
                                currentGroup.Clear();
                                continue;
                            }
                            throw new Exception($"BTCMarkets: Action in a trading group must be consistent: {record.ToInlineString()}");
                        }
                        else if (currentGroup.Count == 2 && record.Action == firstAction)
                        {
                            // we just completed a trade without fees - add group then continue.
                            thisGroups.Add(new BTCMarketsRecordGroup(currentGroup.ToList(), GroupType.TradeWithoutFee));
                            currentGroup.Clear();
                        }
                        else if (currentGroup.Count == 2 && record.Action != ActionType.TradingFee)
                        {
                            throw new Exception($"Cannot add a third record to a trading group that is not a fee: {record.ToInlineString()}");
                        }

                        currentGroup.Add(record);

                        if (currentGroup.Count == 3)
                        {
                            thisGroups.Add(new BTCMarketsRecordGroup(currentGroup.ToList(), GroupType.TradeWithFee));
                            currentGroup.Clear();
                        }
                    }

                    // clean up remainder, which could be a TradeWithoutFee
                    if (currentGroup.Count == 2)
                    {
                        if (currentGroup.Select(g => g.Action).Distinct().Count() == 1)
                        {
                            // there could be a finished trade without fees let-over that we have to add. since they don't end with a distinctive type (e.g. fee type), 
                            // this is the only way to reliably find the trailing TradeWithoutFee
                            thisGroups.Add(new BTCMarketsRecordGroup(currentGroup.ToList(), GroupType.TradeWithoutFee));
                            currentGroup.Clear();
                        }
                    }
                    else if (currentGroup.Count != 0)
                    {
                        throw new Exception("BTCMarkets: Something went wrong - unexpected remainder");
                    }

                    // now we still might have lonely records left-over. add them to the existing groups.
                    // try to pick the group that came just before, otherwise pick the one that came just after
                    foreach (BTCMarketsRecord lonelyRecord in lonelyRecords)
                    {
                        bool added = false;

                        // try to add to the first group that came just before/at same time
                        if (!added && thisGroups.Any(g => g.Time <= lonelyRecord.Time))
                        {
                            BTCMarketsRecordGroup group = thisGroups.Last(g => g.Time <= lonelyRecord.Time);
                            if (group != null)
                            {
                                added = group.TryAddLonelyRecord(lonelyRecord);
                            }
                        }

                        // try to add to the first group that came just after/at same time
                        if (!added && thisGroups.Any(g => g.Time >= lonelyRecord.Time))
                        {
                            BTCMarketsRecordGroup group = thisGroups.First(g => g.Time >= lonelyRecord.Time);
                            if (group != null)
                            {
                                added = group.TryAddLonelyRecord(lonelyRecord);
                            }
                        }

                        if (!added) throw new Exception($"Unable to find home for lonely record {lonelyRecord.ToInlineString()}");
                    }

                    groups.AddRange(thisGroups);
                }

                else if (firstAction == ActionType.Deposit && eventGroup.Count() == 2)
                {
                    // POLI deposit results in fee being subtracted as a withdrawal
                    if (eventGroup.ElementAt(1).Action != ActionType.Withdraw) throw new Exception($"POLI deposit should result in fee being subtracted, but action type was {eventGroup.ElementAt(1).Action}");
                    groups.Add(new BTCMarketsRecordGroup(eventGroup, GroupType.DepositWithFee));
                }

                else
                {
                    // must be vanilla deposit or withdrawal
                    if (eventGroup.Count() != 1) throw new Exception($"First action of this group is neither {ActionType.BuyOrder}, {ActionType.SellOrder}, or {ActionType.Deposit}, but there are not exactly one events in the group");
                    else if (firstAction == ActionType.TradingFee) throw new Exception($"First action of a group of events may not be {ActionType.TradingFee}");

                    groups.Add(new BTCMarketsRecordGroup(eventGroup, GroupType.FundsTransfer));
                }
            }

            return groups;
        }
    }
}
