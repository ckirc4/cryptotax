﻿namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public enum ActionType
    {
        BuyOrder,
        SellOrder,
        TradingFee,
        Deposit,
        Withdraw
    }
}
