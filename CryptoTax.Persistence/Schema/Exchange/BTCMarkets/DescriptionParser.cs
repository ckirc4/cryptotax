﻿using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public static class DescriptionParser
    {
        /// <summary>
        /// Attempts to parse the transaction hash from the description of a deposit or withdrawal action.
        /// </summary>
        public static NString TryGetTransactionHash(ActionType action, DString description)
        {
            if (action != ActionType.Deposit && action != ActionType.Withdraw) return null;

            // split by white space and punctuation, then look for a long string - that is probably our hash
            const int MIN_LENGTH = 16;
            IEnumerable<string> candidates = description.Value
                .Split(' ', '.', '-', ':', ',', '/')
                .Where(s => s.Length >= MIN_LENGTH)
                .Where(s => !s.ToLower().Any(c =>
                    !char.IsDigit(c)
                    && c != 'a'
                    && c != 'b'
                    && c != 'c'
                    && c != 'd'
                    && c != 'e'
                    && c != 'f'
                    && c != 'x')
                );

            if (!candidates.Any()) return null;
            else return candidates.Single(); // there really should only be a single match here
        }

        public static bool IsForkDeposit(DString description)
        {
            return description.Value.ToLower().Contains("fork");
        }

        public static bool IsAirdropDeposit(DString description)
        {
            return description.Value.ToLower().Contains("airdrop");
        }
    }
}
