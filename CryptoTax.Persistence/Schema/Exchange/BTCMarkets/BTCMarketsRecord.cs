﻿using CryptoTax.DTO;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.BTCMarkets
{
    public class BTCMarketsRecord : Record
    {
        public long Id { get; }

        /// <summary>
        /// Event reference that this record belongs to.
        /// </summary>
        public long Reference { get; }

        /// <summary>
        /// UTC.
        /// </summary>
        public DateTime Time { get; }
        public ActionType Action { get; }
        public DString Currency { get; }
        /// <summary>
        /// Positive if added to account, negative if taken away.
        /// </summary>
        public decimal Amount { get; private set; }
        public DString Description { get; }

        /// <summary>
        /// Whether this record has been modified (for example when adding a lonely record)
        /// </summary>
        public bool IsOriginal { get; private set; }

        public BTCMarketsRecord(object[] parsedRow)
        {
            this.Id = (long)parsedRow[0];
            this.Reference = (long)parsedRow[8];
            this.Time = (DateTime)parsedRow[1];
            this.Action = (ActionType)parsedRow[3];
            this.Currency = (string)parsedRow[4];
            this.Amount = (decimal)parsedRow[5];
            this.Description = (string)parsedRow[6];
            this.IsOriginal = true;
        }

        public void AddAmount(decimal amountToAdd)
        {
            this.IsOriginal = false;
            this.Amount += amountToAdd;
        }

        public DString ToInlineString()
        {
            return $"#{this.Id} (event #{this.Reference}): {this.Action} {this.Amount} {this.Currency} at {this.Time}";
        }

        public override TradeDTO ToTradeDTO()
        {
            // trades are always grouped and handled by the BTCMarketsRecordGroup
            return null;
        }

        public override TransactionDTO ToTransactionDTO()
        {
            // withdrawals/deposits are rarely grouped, and handled by the BTCMarketsRecordGroup
            return null;
        }
    }
}
