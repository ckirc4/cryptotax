﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.OnChain
{
    public class OnChain_0xe5ddSchema : Schema
    {
        private readonly DString _WalletAddress;

        readonly Column<string> Hash = new Column<string>("Txhash", true);
        readonly TimestampColumn UnixTimestamp = new TimestampColumn("UnixTimestamp", true, false);
        readonly DateColumn Time = new DateColumn("DateTime", true, 0);
        readonly Column<string> FromAddress = new Column<string>("From", true);
        readonly Column<string> ToAddress = new Column<string>("To", true);
        /// <summary>
        /// e.g. '1,070,634,016,181,930,000' is equivalent to 1.07...
        /// </summary>
        readonly Column<string> Value = new Column<string>("Value", true);
        readonly Column<string> ContractAddress = new Column<string>("ContractAddress", true);
        readonly Column<string> TokenName = new Column<string>("TokenName", true);
        readonly Column<string> TokenSymbol = new Column<string>("TokenSymbol", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.OnChain_0xe5dd;
        public override Column[] Columns { get; }

        public OnChain_0xe5ddSchema(DString walletAddress)
        {
            walletAddress = walletAddress.Value.ToLower();
            if (walletAddress != "0xe5dd240231608367fb2d61d5348693a7cbf5396e") throw new ArgumentException($"Invalid address supplied to {nameof(OnChain_GeneralSchema)}");

            this.Columns = new Column[]
            {
                this.Hash, // 0
                this.UnixTimestamp, // 1
                this.Time, // 2
                this.FromAddress, // 3
                this.ToAddress, // 4
                this.Value, // 5
                this.ContractAddress, // 6
                this.TokenName, // 7
                this.TokenSymbol // 8
            };
            this._WalletAddress = walletAddress;
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new OnChain_0xe5ddRecord(parsedRow, this._WalletAddress);
        }
    }
}
