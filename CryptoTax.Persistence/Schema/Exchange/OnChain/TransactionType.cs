﻿namespace CryptoTax.Persistence.Schema.Exchange.OnChain
{
    public enum TransactionType
    {
        ReceivedFromCounterparty,
        SentToCounterparty,
        TradedWithCounterparty
    }
}