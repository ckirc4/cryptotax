﻿using System;
using Ex = CryptoTax.Shared.Enums.Exchange;
using CryptoTax.DTO;
using UtilityLibrary.Types;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System.Linq;
using CryptoTax.Shared.Helpers;

namespace CryptoTax.Persistence.Schema.Exchange.OnChain
{
    public class OnChain_GeneralRecord : Record
    {
        public DString TxHash { get; set; }
        public DateTime Time { get; set; }
        public DString Counterparty { get; set; }
        public TransactionType TransactionType { get; set; }
        public decimal? QuantityReceived { get; set; }
        public NString CurrencyReceived { get; set; }
        public decimal? QuantitySent { get; set; }
        public NString CurrencySent { get; set; }
        /// <summary>
        /// Transaction fee in the native currency (not necessarily paid by me)
        /// </summary>
        public decimal Fee { get; set; }
        public DString WalletAddress { get; set; }
        public BlockchainType BlockchainType { get; set; }

        public OnChain_GeneralRecord(object[] parsedRow, DString walletAddress, BlockchainType blockchainType)
        {
            if (!OnChain_GeneralSchema.SUPPORTED_ADDRESSES.Any(x => x == walletAddress)) throw new ArgumentException($"Unsupported address supplied to {nameof(OnChain_GeneralRecord)}: {walletAddress}");

            this.TxHash = (string)parsedRow[0];
            this.Time = (DateTime)parsedRow[1];
            this.Counterparty = (string)parsedRow[2];
            this.TransactionType = (TransactionType)parsedRow[3];
            this.QuantityReceived = (decimal?)parsedRow[4];
            this.CurrencyReceived = (string)parsedRow[5];
            this.QuantitySent = (decimal?)parsedRow[6];
            this.CurrencySent = (string)parsedRow[7];
            this.Fee = (decimal)parsedRow[8];
            this.WalletAddress = walletAddress;
            this.BlockchainType = blockchainType;
        }

        public override TradeDTO ToTradeDTO()
        {
            if (this.TransactionType != TransactionType.TradedWithCounterparty) return null;

            DString currencySent = (DString)this.CurrencySent;
            DString currencyReceived = (DString)this.CurrencyReceived;
            Container container = Container.FromWallet(this.WalletAddress, this.BlockchainType);

            // the order of the symbol does not matter, as long as it's consistent with Buy/Sell and sending/receiving the correct currency
            SymbolDTO symbol = new SymbolDTO(currencySent, currencyReceived);

            // e.g. if we have sent WETH and received BOMB, then the equivalent symbol (as defined above) is BOMB/WETH
            // and the price is the inverse ratio, because the price denotes the quantity to send in order to receive one unit:
            decimal effectivePrice = (decimal)this.QuantitySent / (decimal)this.QuantityReceived;

            DString feeCurrency = BlockchainHelpers.GetNativeCurrency(this.BlockchainType).Symbol;

            ContainerEventDTO containerEvent = new ContainerEventDTO(
                this.Time,
                container,
                currencySent,
                this.QuantitySent,
                currencyReceived,
                this.QuantityReceived,
                feeCurrency,
                this.Fee);

            return new TradeDTO(
                this.Time,
                Ex.OnChain,
                this.TxHash,
                symbol,
                MarketType.Spot,
                OrderSide.Buy,
                effectivePrice,
                containerEvent);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.TransactionType == TransactionType.TradedWithCounterparty) return null;
            NString currencySent = null;
            decimal? amountSent = null;
            NString currencyReceived = null;
            decimal? amountReceived = null;
            NString feeCurrency = BlockchainHelpers.GetNativeCurrency(this.BlockchainType).Symbol;
            decimal? feeAmount = this.Fee;

            Container container = Container.FromWallet(this.WalletAddress, this.BlockchainType);

            if (this.TransactionType == TransactionType.ReceivedFromCounterparty)
            {
                amountReceived = this.QuantityReceived;
                currencyReceived = (DString)this.CurrencyReceived;

                // inbound transaction fees are paid by the sender
                feeAmount = 0;
            }
            else if (this.TransactionType == TransactionType.SentToCounterparty)
            {
                amountSent = this.QuantitySent;
                currencySent = (DString)this.CurrencySent; 
            }
            else throw new UnreachableCodeException($"Illegal OnChain transaction type {this.TransactionType}");

            ContainerEventDTO containerEvent = new ContainerEventDTO(
                this.Time,
                container,
                currencySent,
                amountSent,
                currencyReceived,
                amountReceived,
                feeCurrency,
                feeAmount);

            return new TransactionDTO(
                this.Time,
                Ex.OnChain,
                null,
                this.TxHash,
                this.Counterparty,
                Shared.Enums.TransactionType.OnChainTransfer,
                containerEvent);
        }
    }
}
