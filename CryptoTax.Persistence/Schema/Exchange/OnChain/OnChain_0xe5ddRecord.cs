﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.OnChain
{
    public class OnChain_0xe5ddRecord : Record
    {
        public DString TxHash { get; set; }
        public DateTime Time { get; set; }
        public DString Counterparty { get; set; }
        public TransactionType TransactionType { get; set; }
        public decimal? QuantityReceived { get; set; }
        public NString CurrencyReceived { get; set; }
        public decimal? QuantitySent { get; set; }
        public NString CurrencySent { get; set; }
        /// <summary>
        /// Transaction fee in ETH (not necessarily paid by me)
        /// </summary>
        public decimal Fee { get; set; }
        public DString WalletAddress { get; set; }

        public OnChain_0xe5ddRecord(object[] parsedRow, DString walletAddress)
        {
            if (walletAddress != "0xe5dd240231608367fb2d61d5348693a7cbf5396e") throw new ArgumentException($"Invalid address supplied to {nameof(OnChain_0xe5ddRecord)}");
            this.WalletAddress = walletAddress;

            this.TxHash = (string)parsedRow[0];
            this.Time = (DateTime)parsedRow[1];

            string from = ((string)parsedRow[3]).ToLower();
            string to = ((string)parsedRow[4]).ToLower();
            string valueString = (string)parsedRow[5];
            decimal amount = parseValue(valueString);
            string symbol = (string)parsedRow[8];

            if (to == walletAddress)
            {
                this.TransactionType = TransactionType.ReceivedFromCounterparty;
                this.Counterparty = from;
                this.QuantityReceived = amount;
                this.CurrencyReceived = symbol;
            }
            else if (from == walletAddress)
            {
                this.TransactionType = TransactionType.SentToCounterparty;
                this.Counterparty = to;
                this.QuantitySent = amount;
                this.CurrencySent = symbol;
            }
            else throw new Exception("Wallet address must be involved in the schema row.");
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.TransactionType == TransactionType.TradedWithCounterparty) throw new NotImplementedException($"Trading is not supported for {nameof(OnChain_GeneralRecord)}");
            else if (this.TransactionType == TransactionType.SentToCounterparty)
            {
                if (this.TxHash == "0x817a0c906d329d8b24e488d68859c0db9fb7aa010d69ddd456822e692cf8c822") return null; // ignore MOON migration
                else throw new NotImplementedException($"Sending funds not yet implemented for {nameof(OnChain_GeneralRecord)}");
            }
            
            Container container = Container.FromWallet(this.WalletAddress);
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, container, null, null, this.CurrencyReceived, this.QuantityReceived, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.OnChain, null, this.TxHash, this.Counterparty, Shared.Enums.TransactionType.StakingOrAirdrops, containerEvent);
        }

        private static decimal parseValue(string str)
        {
            return decimal.Parse(str.Replace(",", "")) / (decimal)Math.Pow(10, 18);
        }
    }
}
