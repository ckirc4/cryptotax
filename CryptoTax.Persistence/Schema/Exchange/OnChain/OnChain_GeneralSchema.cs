﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.OnChain
{
    public class OnChain_GeneralSchema : Schema
    {
        public static readonly string[] SUPPORTED_ADDRESSES = new string[] {
            KnownWallets.ETH_MY_WALLET_OLD.Address,
            KnownWallets.ETH_MY_WALLET.Address,
            KnownWallets.ETH_MY_WALLET_ARB_NOVA.Address,
            KnownWallets.XRP_MY_WALLET_SAVINGS.Address,
            KnownWallets.XRP_MY_WALLET_SAVINGS_NEW.Address
        };

        private readonly DString _WalletAddress;
        private readonly BlockchainType _BlockchainType;

        /// <summary>
        /// The hash of the underlying on-chain transaction.
        /// </summary>
        private readonly Column<string> TxHash = new Column<string>("TxHash", true);

        /// <summary>
        /// UNIX millsecond timestamp of the transaction.
        /// </summary>
        private readonly TimestampColumn Time = new TimestampColumn("Time", true, true);

        /// <summary>
        /// Address of sender, receipient, or trade partner.
        /// </summary>
        private readonly Column<string> Counterparty = new Column<string>("Counterparty", true);

        /// <summary>
        /// The type of transaction (fund transfer or trade).
        /// </summary>
        private readonly EnumColumn<TransactionType> TransactionType = new EnumColumn<TransactionType>("TransactionType", true);

        /// <summary>
        /// The amount of currency received in the transaction, if any.
        /// </summary>
        private readonly DecimalColumn QuantityReceived = new DecimalColumn("QuantityReceived", false);

        /// <summary>
        /// The symbol of the currency received in the transaction, if any.
        /// </summary>
        private readonly Column<string> CurrencyReceived = new Column<string>("CurrencyReceived", false);

        /// <summary>
        /// The amount of currency sent in the transaction, if any.
        /// </summary>
        private readonly DecimalColumn QuantitySent = new DecimalColumn("QuantitySent", false);

        /// <summary>
        /// The symbol of the currency sent in the transaction, if any.
        /// </summary>
        private readonly Column<string> CurrencySent = new Column<string>("CurrencySent", false);

        /// <summary>
        /// The fee (in ETH) of the on-chain transaction. May be zero.
        /// </summary>
        private readonly DecimalColumn Fee = new DecimalColumn("Fee", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.OnChain_General;
        public override Column[] Columns { get; }

        public OnChain_GeneralSchema(DString walletAddress, BlockchainType blockchainType)
        {
            // xrp addresses are case-sensitive
            walletAddress = blockchainType == BlockchainType.XRPL ? walletAddress.Value : walletAddress.Value.ToLower();
            if (!SUPPORTED_ADDRESSES.Any(x => x == walletAddress)) throw new ArgumentException($"Unsupported address supplied to {nameof(OnChain_GeneralSchema)}: {walletAddress}");

            this.Columns = new Column[]
            {
                this.TxHash,
                this.Time,
                this.Counterparty,
                this.TransactionType,
                this.QuantityReceived,
                this.CurrencyReceived,
                this.QuantitySent,
                this.CurrencySent,
                this.Fee
            };
            this._WalletAddress = walletAddress;
            this._BlockchainType = blockchainType;
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new OnChain_GeneralRecord(parsedRow, this._WalletAddress, this._BlockchainType);
        }
    }
}
