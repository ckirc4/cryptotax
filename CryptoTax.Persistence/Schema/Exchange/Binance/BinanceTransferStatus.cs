﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public enum BinanceTransferStatus
    {
        Completed,
        Cancelled
    }
}
