﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceOptionsRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public BinanceOptionsType Type { get; set; }
        public decimal Amount { get; set; }

        public BinanceOptionsRecord(object[] parsedRow)
        {
            this.Id = "";
            this.Time = (DateTime)parsedRow[0];
            this.Type = (BinanceOptionsType)parsedRow[1];

            string amount = (string)parsedRow[2];
            (decimal amountAmount, DString amountCurrency) = Helpers.DestructureAmount(amount);
            this.Amount = amountAmount;

            string asset = (string)parsedRow[3];

            if (asset != "USDT" || amountCurrency != "USDT") throw new NotImplementedException("Only supports USDT for Binance options");
        }
    }
}
