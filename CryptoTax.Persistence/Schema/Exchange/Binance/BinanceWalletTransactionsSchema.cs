﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("UTC_Time", true, 0);
        readonly Column<string> Account = new Column<string>("Account", true);
        readonly Column<string> Type = new Column<string>("Operation", true);
        readonly Column<string> Asset = new Column<string>("Coin", true);
        readonly DecimalColumn Amount = new DecimalColumn("Change", true);
        readonly Column<string> Remark = new Column<string>("Remark", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_WalletTransactions;
        public override Column[] Columns { get; }

        public BinanceWalletTransactionsSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Account, // 1
                this.Type, // 2
                this.Asset, // 3
                this.Amount, // 4
                this.Remark // 5
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceWalletTransactionsRecord(parsedRow);
        }

        public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
        {
            return BinanceWalletTransactionsRecordGroup.ProcessAll(allRecords.Cast<BinanceWalletTransactionsRecord>());
        }
    }
}
