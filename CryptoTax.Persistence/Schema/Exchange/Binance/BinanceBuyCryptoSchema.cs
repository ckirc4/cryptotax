﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceBuyCryptoSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly EnumColumn<BinanceBuyCryptoMethod> Method = new EnumColumn<BinanceBuyCryptoMethod>("Method", true);
        /// <summary>
        /// Fiat amount used to make "trade", before fees taken away
        /// </summary>
        readonly Column<string> Amount = new Column<string>("Amount", true);
        /// <summary>
        /// The effective trading price AFTER `Fee` has been subtracted from `Amount`
        /// </summary>
        readonly Column<string> Price = new Column<string>("Price", true);
        readonly Column<string> Fee = new Column<string>("Fees", true);
        /// <summary>
        /// The crypto amount received after the "trade"
        /// </summary>
        readonly Column<string> FinalAmount = new Column<string>("Final Amount", true);
        readonly EnumColumn<BinanceBuyCryptoStatus> Status = new EnumColumn<BinanceBuyCryptoStatus>("Status", true);
        readonly Column<string> Id = new Column<string>("Transaction ID", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_BuyCrypto;
        public override Column[] Columns { get; }

        public BinanceBuyCryptoSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Method, // 1
                this.Amount, // 2
                this.Price, // 3
                this.Fee, // 4
                this.FinalAmount, // 5
                this.Status, // 6
                this.Id // 7
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceBuyCryptoRecord(parsedRow);
        }
    }
}
