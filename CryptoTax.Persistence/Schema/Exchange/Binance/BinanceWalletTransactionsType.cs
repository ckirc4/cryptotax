﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public enum BinanceWalletTransactionsType
    {
        TransferSubAccount,
        Fee,
        RealisedPnl,
        FundingFee,
        /// <summary>
        /// Maybe left-over funds from liquidation? Or counterparty liquidation beyond backruptcy?
        /// </summary>
        InsuranceFundCompensation,
        Deposit,
        Withdrawal,
        Distribution,
        TransactionRelated,
        /// <summary>
        /// Buying/selling on spot market (yes, this is a bug on Binance lol)
        /// </summary>
        Buy,
        DustConversionToBnb,
        /// <summary>
        /// Somehow coupled with <see cref="TransferSubAccount"/> and spot/futures account types
        /// </summary>
        TransferIn,
        /// <summary>
        /// Somehow coupled with <see cref="TransferSubAccount"/> and spot/futures account types
        /// </summary>
        TransferOut
    }
}
