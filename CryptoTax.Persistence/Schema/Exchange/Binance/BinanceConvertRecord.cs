﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceConvertRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal Price { get; set; }
        public DString BaseCurrency { get; set; }
        public DString QuoteCurrency { get; set; }
        public decimal AmountReceived { get; set; }
        public decimal AmountSent { get; set; }

        public BinanceConvertRecord(object[] parsedRow)
        {
            this.Time = (DateTime)parsedRow[0];
            this.Id = $"BinanceConvert-{this.Time:dd/MM/yyyy HH:mm:ss}";

            string sell = new DString((string)parsedRow[4]);
            string buy = new DString((string)parsedRow[5]);
            string price = new DString((string)parsedRow[6]);

            (decimal amountSent, DString quoteCurrency) = Helpers.DestructureAmount(sell);
            (decimal amountReceived, DString baseCurrency) = Helpers.DestructureAmount(buy);
            (decimal orderPrice, DString priceQuoteCurrency, DString priceBaseCurrency) = Helpers.DestructurePriceV2(price);

            if (baseCurrency == quoteCurrency) throw new Exception("Base and quote currency must differ");
            else if (amountSent <= 0) throw new Exception("Amount sent must be positive");
            else if (amountReceived <= 0) throw new Exception("Quantity received must be positive");
            else if (orderPrice <= 0) throw new Exception("Price must be positive");
            else if (quoteCurrency != priceQuoteCurrency) throw new Exception("Quote currency of price must be the same as the order value");
            else if (baseCurrency != priceBaseCurrency) throw new Exception("Base currency of price must be the same as the order quantity");
            else if (quoteCurrency != "AUD") throw new Exception("Currently, only AUD is supported");

            this.Price = orderPrice;
            this.AmountReceived = amountReceived;
            this.BaseCurrency = baseCurrency;
            this.AmountSent = amountSent;
            this.QuoteCurrency = quoteCurrency;
        }

        public override TradeDTO ToTradeDTO()
        {
            DString currencyReceived = this.BaseCurrency;
            decimal amountReceived = this.AmountReceived;

            DString currencySent = this.QuoteCurrency;
            decimal amountSent = this.AmountSent;

            DateTime tradeTime = this.Time;
            ContainerEventDTO containerEvent = new ContainerEventDTO(tradeTime, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);

            // calculate the price ourselves to make the TradeProcessingService happy - this should be more or less equal to the indicated price.
            decimal price = amountSent / amountReceived;
            if (Math.Abs((price - this.Price) / this.Price) > 0.001m) throw new Exception("Price discrepancy");

            SymbolDTO symbol = new SymbolDTO(this.QuoteCurrency, this.BaseCurrency);
            return new TradeDTO(tradeTime, Shared.Enums.Exchange.Binance, this.Id, symbol, MarketType.Spot, OrderSide.Buy, price, containerEvent);
        }
    }
}
