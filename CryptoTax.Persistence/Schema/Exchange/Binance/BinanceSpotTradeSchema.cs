﻿using CryptoTax.Shared.Enums;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceSpotTradeSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly Column<string> Pair = new Column<string>("Pair", true);
        readonly EnumColumn<OrderSide> Side = new EnumColumn<OrderSide>("Side", true, false);
        readonly DecimalColumn Price = new DecimalColumn("Price", true);
        /// <summary>
        /// Base quantity
        /// </summary>
        readonly Column<string> Quantity = new Column<string>("Executed", true);
        /// <summary>
        /// Quote value
        /// </summary>
        readonly Column<string> Value = new Column<string>("Amount", true);
        /// <summary>
        /// Any currency
        /// </summary>
        readonly Column<string> Fee = new Column<string>("Fee", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_SpotTrades;
        public override Column[] Columns { get; }

        public BinanceSpotTradeSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Pair, // 1
                this.Side, // 2
                this.Price, // 3
                this.Quantity, // 4
                this.Value, // 5
                this.Fee // 6
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceSpotTradeRecord(parsedRow);
        }
    }
}
