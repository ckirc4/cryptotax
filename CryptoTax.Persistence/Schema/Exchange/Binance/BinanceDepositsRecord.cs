﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceDepositsRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public DString Coin { get; set; }
        public decimal Amount { get; set; }

        public BinanceDepositsRecord(object[] parsedRow)
        {
            BinanceTransferStatus status = (BinanceTransferStatus)parsedRow[8];
            if (status != BinanceTransferStatus.Completed) throw new Exception($"Deposit status must be completed but is '{status}'");

            this.Id = new DString((string)parsedRow[5]);
            this.Time = (DateTime)parsedRow[0];
            this.Coin = new DString((string)parsedRow[1]);
            this.Amount = (decimal)parsedRow[2];
            decimal fee = (decimal)parsedRow[3];
            if (fee != 0) throw new NotImplementedException("Fees not supported for BinanceDeposits");
        }

        public override TransactionDTO ToTransactionDTO()
        {
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, this.Coin, this.Amount, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, null, this.Id, null, Shared.Enums.TransactionType.CryptoDeposit, containerEvent);
        }
    }
}
