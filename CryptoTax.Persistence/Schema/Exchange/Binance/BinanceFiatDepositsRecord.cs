﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceFiatDepositsRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        /// <summary>
        /// Before deposit fees were subtracted
        /// </summary>
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }

        public BinanceFiatDepositsRecord(object[] parsedRow)
        {
            DString currency = new DString((string)parsedRow[1]);
            if (currency != "AUD") throw new NotImplementedException("FiatDeposits only supports AUD");

            this.Id = new DString((string)parsedRow[7]);
            this.Time = (DateTime)parsedRow[0];
            this.Amount = (decimal)parsedRow[5];
            this.Fee = (decimal)parsedRow[6];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, "AUD", this.Amount, "AUD", this.Fee);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.FiatDeposit, containerEvent);
        }
    }
}
