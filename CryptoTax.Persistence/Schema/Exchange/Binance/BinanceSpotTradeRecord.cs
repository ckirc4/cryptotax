﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceSpotTradeRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public OrderSide Side { get; set; }
        public decimal Price { get; set; }
        public DString BaseCurrency { get; set; }
        public DString QuoteCurrency { get; set; }
        public NString FeeCurrency { get; set; }
        public decimal OrderQuantity { get; set; }
        public decimal OrderValue { get; set; }
        public decimal? FeeAmount { get; set; }

        public BinanceSpotTradeRecord(object[] parsedRow)
        {
            this.Id = ""; // not implemented - some rows are identical, so it may not be possible to generate an id deterministically
            this.Time = (DateTime)parsedRow[0];
            this.Side = (OrderSide)parsedRow[2];
            this.Price = (decimal)parsedRow[3];

            string quantity = new DString((string)parsedRow[4]);
            string value = new DString((string)parsedRow[5]);
            string fee = new NString((string)parsedRow[6]);

            (decimal orderQuantity, DString baseCurrency) = Helpers.DestructureAmount(quantity);
            (decimal orderValue, DString quoteCurrency) = Helpers.DestructureAmount(value);
            (decimal? feeAmount, DString? feeCurrency) = fee == null ? ((decimal?)null, (DString?)null) : Helpers.DestructureAmount(fee);

            if (baseCurrency == quoteCurrency) throw new Exception("Base and quote currency must differ");
            else if (orderQuantity <= 0) throw new Exception("Order quantity must be positive");
            else if (orderValue <= 0) throw new Exception("Order value must be positive");
            else if (feeAmount < 0) throw new Exception("Fee amount must not be negative");

            this.OrderQuantity = orderQuantity;
            this.BaseCurrency = baseCurrency;
            this.OrderValue = orderValue;
            this.QuoteCurrency = quoteCurrency;
            this.FeeAmount = feeAmount;
            this.FeeCurrency = feeCurrency == null ? (NString)null : feeCurrency.Value;
        }

        public override TradeDTO ToTradeDTO()
        {
            DString currencyReceived, currencySent;
            decimal amountReceived, amountSent;
            NString feeCurrency = this.FeeAmount == 0 ? null : (NString)this.FeeCurrency;
            decimal? feeAmount = this.FeeAmount == 0 ? (decimal?)null : this.FeeAmount;

            if (this.Side == OrderSide.Buy)
            {
                currencyReceived = this.BaseCurrency;
                amountReceived = this.OrderQuantity;
                currencySent = this.QuoteCurrency;
                amountSent = this.OrderValue;
            }
            else
            {
                currencyReceived = this.QuoteCurrency;
                amountReceived = this.OrderValue;
                currencySent = this.BaseCurrency;
                amountSent = this.OrderQuantity;
            }

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, feeCurrency, feeAmount);
            SymbolDTO symbol = new SymbolDTO(this.QuoteCurrency, this.BaseCurrency);
            return new TradeDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, symbol, MarketType.Spot, this.Side, this.Price, containerEvent);
        }
    }
}
