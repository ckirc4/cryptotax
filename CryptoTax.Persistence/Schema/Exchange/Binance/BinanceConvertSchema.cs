﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceConvertSchema : Schema
    {
        readonly DateColumn Date = new DateColumn("Date", true, 0);
        readonly EnumColumn<BinanceConvertWallet> Wallet = new EnumColumn<BinanceConvertWallet>("Wallet", true);
        /// <summary>
        /// E.g. ETHAUD
        /// </summary>
        readonly Column<string> Pair = new Column<string>("Pair", true);
        readonly EnumColumn<BinanceConvertType> Type = new EnumColumn<BinanceConvertType>("Type", true);
        /// <summary>
        /// Of the format "{amount} {symbol}"
        /// </summary>
        readonly Column<string> Sell = new Column<string>("Sell", true);
        /// <summary>
        /// Of the format "{amount} {symbol}"
        /// </summary>
        readonly Column<string> Buy = new Column<string>("Buy", true);
        /// <summary>
        /// Of the format "1 {sellSymbol} = {amount} {buySymbol}"
        /// </summary>
        readonly Column<string> Price = new Column<string>("Price", true);
        /// <summary>
        /// Of the format "1 {buySymbol} = {amount} {sellSymbol}"
        /// </summary>
        readonly Column<string> InversePrice = new Column<string>("Inverse Price", true);
        /// <summary>
        /// Presumably the time at which the trade was executed.
        /// </summary>
        readonly DateColumn DateUpdated = new DateColumn("Date Updated", true, 0);
        readonly EnumColumn<BinanceConvertStatus> Status = new EnumColumn<BinanceConvertStatus>("Status", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_Convert;
        public override Column[] Columns { get; }

        public BinanceConvertSchema()
        {
            this.Columns = new Column[]
            {
                this.Date, // 0
                this.Wallet, // 1
                this.Pair, // 2
                this.Type, // 3
                this.Sell, // 4
                this.Buy, // 5
                this.Price, // 6
                this.InversePrice, // 7
                this.DateUpdated, // 8
                this.Status, // 9
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceConvertRecord(parsedRow);
        }
    }
}
