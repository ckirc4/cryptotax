﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsRecordGroup : RecordGroup
    {
        private readonly IEnumerable<BinanceWalletTransactionsRecord> _Group;
        public override IEnumerable<Record> Group { get { return this._Group; } }

        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal Amount { get; set; }
        public BinanceWalletTransactionsRecordGroupType Type { get; set; }

        public BinanceWalletTransactionsRecordGroup(IEnumerable<BinanceWalletTransactionsRecord> group, BinanceWalletTransactionsRecordGroupType type)
        {
            if (type != BinanceWalletTransactionsRecordGroupType.FundingPayment) throw new NotImplementedException();

            this._Group = group;
            this.Type = type;
            this.Time = group.First().Time;
            this.Id = $"AggregateFundingFee-{this.Time.Round(TimeSpan.FromHours(1)).ToString("dd/MM/yyyy HH:mm:ss")}";
            this.Amount = group.Sum(r => r.Amount);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type != BinanceWalletTransactionsRecordGroupType.FundingPayment) throw new NotImplementedException();

            if (this.Amount == 0) return null;

            string currencyReceived = this.Amount < 0 ? null : "USDT";
            decimal? amountReceived = this.Amount < 0 ? (decimal?)null : Math.Abs(this.Amount);
            string currencySent = this.Amount > 0 ? null : "USDT";
            decimal? amountSent = this.Amount > 0 ? (decimal?)null : Math.Abs(this.Amount);

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.FuturesFunding, containerEvent);
        }

        public static IEnumerable<BinanceWalletTransactionsRecordGroup> ProcessAll(IEnumerable<BinanceWalletTransactionsRecord> allRecords)
        {
            List<BinanceWalletTransactionsRecordGroup> groups = new List<BinanceWalletTransactionsRecordGroup>();

            foreach (IGrouping<DateTime, BinanceWalletTransactionsRecord> group in allRecords.Where(r => r.Type == BinanceWalletTransactionsType.FundingFee).GroupBy(r => r.Time.Round(TimeSpan.FromHours(1))))
            {
                if (group.Select(r => r.Currency).Any(c => c != "USDT")) throw new NotImplementedException("All FundingFee WalletTransactions must be in USDT");

                // ensure all funding fees are within 1 minute of each other
                TimeSpan range = group.Max(r => r.Time).Subtract(group.Min(r => r.Time));
                if (range > TimeSpan.FromMinutes(1)) throw new Exception("Funding fees must fall within 1 minutes of each other for them to be grouped");

                groups.Add(new BinanceWalletTransactionsRecordGroup(group.ToList(), BinanceWalletTransactionsRecordGroupType.FundingPayment));
            }

            return groups;
        }
    }
}
