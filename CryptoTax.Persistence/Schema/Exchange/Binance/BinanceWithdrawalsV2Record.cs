﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWithdrawalsV2Record : Record
    {
        public NString Id { get; set; }
        public DateTime Time { get; set; }
        public DString Coin { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public DString Counterparty { get; set; }
        public BinanceTransferStatus? Status { get; set; }

        public BinanceWithdrawalsV2Record(object[] parsedRow)
        {
            this.Id = (string)parsedRow[6];
            this.Time = (DateTime)parsedRow[0];
            this.Coin = new DString((string)parsedRow[1]);
            this.Amount = (decimal)parsedRow[3];
            this.Fee = (decimal)parsedRow[4];
            this.Counterparty = (string)parsedRow[5];
            this.Status = (BinanceTransferStatus?)parsedRow[9];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Status == BinanceTransferStatus.Cancelled) return null;
            else if (this.Status == BinanceTransferStatus.Completed || this.Status == null)
            {
                DString id = new DString(this.Id);
                NString feeCurrency = null;
                decimal? feeAmount = null;
                if (this.Fee > 0)
                {
                    feeCurrency = this.Coin;
                    feeAmount = this.Fee;
                }

                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), this.Coin, this.Amount, null, null, feeCurrency, feeAmount);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, null, id, this.Counterparty, Shared.Enums.TransactionType.CryptoWithdrawal, containerEvent);
            }
            else throw new AssertUnreachable(this.Status);
        }
    }
}
