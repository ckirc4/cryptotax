﻿using System;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceFiatDepositsSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly Column<string> Currency = new Column<string>("Coin", true);
        /// <summary>
        /// After fees, i.e. increase in account balance
        /// </summary>
        readonly DecimalColumn Amount = new DecimalColumn("Amount", true);
        readonly EnumColumn<BinanceFiatTransferStatus> Status = new EnumColumn<BinanceFiatTransferStatus>("Status", true);
        readonly Column<string> PaymentMethod = new Column<string>("Payment Method", false);
        /// <summary>
        /// Before fees, i.e. amount sent
        /// </summary>
        readonly DecimalColumn OriginalAmount = new DecimalColumn("Indicated Amount", true);
        readonly DecimalColumn Fee = new DecimalColumn("Fee", true);
        readonly Column<string> Id = new Column<string>("Order ID", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_FiatDeposits;
        public override Column[] Columns { get; }

        public BinanceFiatDepositsSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Currency, // 1
                this.Amount, // 2
                this.Status, // 3
                this.PaymentMethod, // 4
                this.OriginalAmount, // 5
                this.Fee, // 6
                this.Id, // 7
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceFiatDepositsRecord(parsedRow);
        }
    }
}
