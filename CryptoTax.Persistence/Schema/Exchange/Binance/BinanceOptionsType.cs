﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public enum BinanceOptionsType
    {
        Transfer,
        OptionCost,
        Fee
    }
}
