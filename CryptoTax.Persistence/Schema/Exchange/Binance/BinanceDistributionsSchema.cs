﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceDistributionsSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Time(UTC+10)", true, 10);
        readonly Column<string> Coin = new Column<string>("Coin", true);
        readonly DecimalColumn Amount = new DecimalColumn("Amount", true);
        readonly Column<string> Account = new Column<string>("Account", true);
        readonly EnumColumn<BinanceDistributionType> Type = new EnumColumn<BinanceDistributionType>("Type", true);
        readonly Column<string> Note = new Column<string>("Note", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_Distributions;
        public override Column[] Columns { get; }

        public BinanceDistributionsSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Coin, // 1
                this.Amount, // 2
                this.Account, // 3
                this.Type, // 4
                this.Note // 5
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceDistributionsRecord(parsedRow);
        }
    }
}
