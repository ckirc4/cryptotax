﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsV2Record : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public BinanceAccountType Account { get; set; }
        public BinanceWalletTransactionsV2Type Type { get; set; }
        public DString Currency { get; set; }
        public decimal Amount { get; set; }
        public NString Note { get; set; }

        public BinanceWalletTransactionsV2Record(object[] parsedRow)
        {
            this.Id = "";
            this.Time = (DateTime)parsedRow[1];
            string account = (string)parsedRow[2];
            this.Account = parseAccount(account);
            string type = (string)parsedRow[3];
            this.Type = parseType(type);
            this.Currency = (string)parsedRow[4];
            this.Amount = (decimal)parsedRow[5];
            this.Note = (string)parsedRow[6];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Type == BinanceWalletTransactionsV2Type.Deposit)
            {
                if (this.Currency != "AUD") throw new NotImplementedException();
                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, "AUD", this.Amount, null, null);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.FiatDeposit, containerEvent);
            }
            else if (this.Type == BinanceWalletTransactionsV2Type.Withdrawal)
            {
                if (this.Currency != "BUSD") throw new NotImplementedException();
                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), this.Currency, -this.Amount, null, null, null, null);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.CryptoWithdrawal, containerEvent);
            }

            return null;
        }

        private static BinanceWalletTransactionsV2Type parseType(string type)
        {
            switch (type)
            {
                case "Deposit":
                    return BinanceWalletTransactionsV2Type.Deposit;
                case "Withdraw":
                    return BinanceWalletTransactionsV2Type.Withdrawal;
                case "Large OTC trading":
                    return BinanceWalletTransactionsV2Type.Trading;
                default:
                    throw new AssertUnreachable(type, $"Value is incompatible with type {typeof(BinanceWalletTransactionsV2Type)}");
            }
        }

        private static BinanceAccountType parseAccount(string account)
        {
            switch (account)
            {
                case "Spot":
                    return BinanceAccountType.Spot;
                default:
                    throw new AssertUnreachable(account, $"Value is incompatible with type {typeof(BinanceAccountType)}");
            }
        }
    }
}
