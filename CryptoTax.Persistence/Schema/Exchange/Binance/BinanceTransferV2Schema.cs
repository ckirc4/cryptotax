﻿using CryptoTax.Shared.Exceptions;
using System;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceTransferV2Schema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly Column<string> Coin = new Column<string>("Coin", true);
        readonly Column<string> _Network = new Column<string>("Network", true);
        readonly DecimalColumn Amount = new DecimalColumn("Amount", true);
        readonly DecimalColumn Fee = new DecimalColumn("TransactionFee", true);
        /// <summary>
        /// Destination address (no address tag available), for deposits [i.e. Binance address] or withdrawals [careful: may be transferred to intermediate wallet first.]
        /// </summary>
        readonly Column<string> Address = new Column<string>("Address", true);
        readonly Column<string> Tx = new Column<string>("TXID", false);
        /// <summary>
        /// Unused?
        /// </summary>
        readonly Column<string> _SourceAddress = new Column<string>("SourceAddress", false);
        /// <summary>
        /// Only for some transactions
        /// </summary>
        readonly Column<string> _PaymentId = new Column<string>("PaymentID", false);
        /// <summary>
        /// Only defined sometimes for some reason.
        /// </summary>
        readonly EnumColumn<BinanceTransferStatus> Status = new EnumColumn<BinanceTransferStatus>("Status", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_Withdrawals_V2;
        public override Column[] Columns { get; }

        public BinanceTransferV2Schema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Coin, // 1
                this._Network, // 2
                this.Amount, // 3
                this.Fee, // 4
                this.Address, // 5
                this.Tx, // 6
                this._SourceAddress, // 7
                this._PaymentId, // 8
                this.Status // 9
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceWithdrawalsV2Record(parsedRow);
        }
    }
}
