﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public static class Helpers
    {
        /// <summary>
        /// Destructures 30.5 BTC into (30.5, "BTC")
        /// </summary>
        public static (decimal quantity, DString currency) DestructureAmount(string amount)
        {
            // may have comma thousands separator
            amount = amount.Replace(",", "");

            string quantityStr = "";
            string currencyStr = "";
            for (int i = 0; i < amount.Length; i++)
            {
                char c = amount[i];
                if (currencyStr.Length > 0 || char.IsLetter(c))
                {
                    currencyStr += c;
                    continue;
                }
                else
                {
                    quantityStr += c;
                }
            }

            if (quantityStr.Length == 0) throw new Exception("Could not destructure quantity");
            else if (currencyStr.Length == 0) throw new Exception("Could not destructure currency");

            return (decimal.Parse(quantityStr.Trim()), new DString(currencyStr.Trim()));
        }

        /// <summary>
        /// Destructures 10000 BTC/AUD into (10000, "AUD", "BTC")
        /// </summary>
        public static (decimal price, DString quoteCurrency, DString baseCurrency) DestructurePrice(string amount)
        {
            (decimal price, DString symbol) = DestructureAmount(amount);
            string[] currencies = symbol.Value.Split('/');
            if (currencies.Length != 2) throw new Exception($"Could not destructure price because symbol '{symbol}' is in the incorrect format");

            return (price, currencies[1], currencies[0]);
        }

        /// <summary>
        /// Destructures 1 AUD = 0.01 ETH into (100, "AUD", "ETH")
        /// </summary>
        public static (decimal price, DString quoteCurrency, DString baseCurrency) DestructurePriceV2(string priceStr)
        {
            string[] sides = priceStr.Split('=');
            (decimal quoteAmount, DString quoteCurrency) = DestructureAmount(sides[0].Trim());
            (decimal baseAmount, DString baseCurrency) = DestructureAmount(sides[1].Trim());

            if (quoteAmount != 1) throw new Exception($"Expected quote amount to be 1 but got {quoteAmount}");

            return (1 / baseAmount, quoteCurrency, baseCurrency);
        }
    }
}
