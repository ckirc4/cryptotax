﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceBuyCryptoRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal Price { get; set; }
        public DString BaseCurrency { get; set; }
        public DString QuoteCurrency { get; set; }
        public DString FeeCurrency { get; set; }
        public decimal OrderQuantity { get; set; }
        public decimal OrderValue { get; set; }
        public decimal FeeAmount { get; set; }

        public BinanceBuyCryptoRecord(object[] parsedRow)
        {
            this.Time = (DateTime)parsedRow[0];
            this.Id = (string)parsedRow[7];

            string value = new DString((string)parsedRow[2]);
            string price = new DString((string)parsedRow[3]);
            string fee = new DString((string)parsedRow[4]);
            string quantity = new DString((string)parsedRow[5]);

            (decimal orderValue, DString quoteCurrency) = Helpers.DestructureAmount(value);
            (decimal orderPrice, DString priceQuoteCurrency, DString priceBaseCurrency) = Helpers.DestructurePrice(price);
            (decimal feeAmount, DString feeCurrency) = Helpers.DestructureAmount(fee);
            (decimal orderQuantity, DString baseCurrency) = Helpers.DestructureAmount(quantity);

            if (baseCurrency == quoteCurrency) throw new Exception("Base and quote currency must differ");
            else if (orderQuantity <= 0) throw new Exception("Order quantity must be positive");
            else if (orderValue <= 0) throw new Exception("Order value must be positive");
            else if (feeAmount < 0) throw new Exception("Fee amount must not be negative");
            else if (orderPrice <= 0) throw new Exception("Price must be positive");
            else if (quoteCurrency != priceQuoteCurrency) throw new Exception("Quote currency of price must be the same as the order value");
            else if (baseCurrency != priceBaseCurrency) throw new Exception("Base currency of price must be the same as the order quantity");
            else if (quoteCurrency != "AUD") throw new Exception("Currently, only AUD is supported");

            this.Price = orderPrice;
            this.OrderQuantity = orderQuantity;
            this.BaseCurrency = baseCurrency;
            this.OrderValue = orderValue;
            this.QuoteCurrency = quoteCurrency;
            this.FeeAmount = feeAmount;
            this.FeeCurrency = feeCurrency;
        }

        public override TradeDTO ToTradeDTO()
        {
            DString currencyReceived = this.BaseCurrency;
            decimal amountReceived = this.OrderQuantity;

            DString currencySent = this.QuoteCurrency;
            if (currencySent != this.FeeCurrency) throw new Exception("'Order' part's value should be equal to deposited value minus fee, but the currencies differ");
            decimal amountSent = this.OrderValue - this.FeeAmount;
            // fees have been delegated to the "deposit" part.

            // hack: to ensure that the "deposit" part is processed first, slightly delay the "trade" part
            DateTime tradeTime = this.Time.AddSeconds(1);
            ContainerEventDTO containerEvent = new ContainerEventDTO(tradeTime, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);

            // calculate the price ourselves to make the TradeProcessingService happy - this should be more or less equal to the indicated price.
            decimal price = amountSent / amountReceived;
            if (Math.Abs((price - this.Price) / this.Price) > 0.001m) throw new Exception("Price discrepancy");

            SymbolDTO symbol = new SymbolDTO(this.QuoteCurrency, this.BaseCurrency);
            return new TradeDTO(tradeTime, Shared.Enums.Exchange.Binance, this.Id + "_trade", symbol, MarketType.Spot, OrderSide.Buy, price, containerEvent);
        }

        public override TransactionDTO ToTransactionDTO()
        {
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, this.QuoteCurrency, this.OrderValue, this.FeeCurrency, this.FeeAmount);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id + "_tx", null, null, TransactionType.FiatDeposit, containerEvent);
        }
    }
}
