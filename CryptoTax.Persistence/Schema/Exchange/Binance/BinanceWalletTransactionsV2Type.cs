﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public enum BinanceWalletTransactionsV2Type
    {
        Deposit,
        Withdrawal,
        Trading,
    }
}
