﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsV2Schema : Schema
    {
        readonly Column<string> UserId = new Column<string>("User_ID", true);
        readonly DateColumn Time = new DateColumn("UTC_Time", true, 0);
        readonly Column<string> Account = new Column<string>("Account", true);
        readonly Column<string> Type = new Column<string>("Operation", true);
        readonly Column<string> Asset = new Column<string>("Coin", true);
        readonly DecimalColumn Amount = new DecimalColumn("Change", true);
        readonly Column<string> Remark = new Column<string>("Remark", false);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_WalletTransactions_V2;
        public override Column[] Columns { get; }

        public BinanceWalletTransactionsV2Schema()
        {
            this.Columns = new Column[]
            {
                this.UserId, // 0
                this.Time, // 1
                this.Account, // 2
                this.Type, // 3
                this.Asset, // 4
                this.Amount, // 5
                this.Remark // 6
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceWalletTransactionsV2Record(parsedRow);
        }

        public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
        {
            return BinanceWalletTransactionsV2RecordGroup.ProcessAll(allRecords.Cast<BinanceWalletTransactionsV2Record>());
        }
    }
}
