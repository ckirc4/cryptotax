﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceOptionsSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Time(UTC+10)", true, 10);
        readonly EnumColumn<BinanceOptionsType> Type = new EnumColumn<BinanceOptionsType>("Type", true);
        readonly Column<string> Amount = new Column<string>("Amount", true);
        readonly Column<string> Asset = new Column<string>("Asset", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_Options;
        public override Column[] Columns { get; }

        public BinanceOptionsSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Type, // 1
                this.Amount, // 2
                this.Asset // 3
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceOptionsRecord(parsedRow);
        }

        public override IEnumerable<RecordGroup> ProcessAll(IEnumerable<Record> allRecords)
        {
            return BinanceOptionsRecordGroup.ProcessAll(allRecords.Cast<BinanceOptionsRecord>());
        }
    }
}
