﻿using CryptoTax.Shared.Exceptions;
using System;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceTransferSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly Column<string> Coin = new Column<string>("Coin", true);
        readonly DecimalColumn Amount = new DecimalColumn("Amount", true);
        readonly DecimalColumn Fee = new DecimalColumn("TransactionFee", true);
        /// <summary>
        /// Destination address (no address tag available), for deposits [i.e. Binance address] or withdrawals [careful: may be transferred to intermediate wallet first.]
        /// </summary>
        readonly Column<string> Address = new Column<string>("Address", true);
        readonly Column<string> Tx = new Column<string>("TXID", false);
        /// <summary>
        /// Unused?
        /// </summary>
        readonly Column<string> _SourceAddress = new Column<string>("SourceAddress", false);
        /// <summary>
        /// Only for some transactions
        /// </summary>
        readonly Column<string> _PaymentId = new Column<string>("PaymentID", false);
        readonly EnumColumn<BinanceTransferStatus> Status = new EnumColumn<BinanceTransferStatus>("Status", true);

        public override SchemaIdentifier Identfier { get; }
        public override Column[] Columns { get; }

        public BinanceTransferSchema(SchemaIdentifier depositsOrWithdrawals)
        {
            if (depositsOrWithdrawals != SchemaIdentifier.Binance_Deposits && depositsOrWithdrawals != SchemaIdentifier.Binance_Withdrawals) throw new ArgumentException($"{nameof(BinanceTransferSchema)} identifier must be either of type {SchemaIdentifier.Binance_Deposits} or {SchemaIdentifier.Binance_Withdrawals}");
            this.Identfier = depositsOrWithdrawals;

            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Coin, // 1
                this.Amount, // 2
                this.Fee, // 3
                this.Address, // 4
                this.Tx, // 5
                this._SourceAddress, // 6
                this._PaymentId, // 7
                this.Status // 8
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            if (this.Identfier == SchemaIdentifier.Binance_Deposits)
            {
                return new BinanceDepositsRecord(parsedRow);
            }
            else if (this.Identfier == SchemaIdentifier.Binance_Withdrawals)
            {
                return new BinanceWithdrawalsRecord(parsedRow);
            }
            else throw new UnreachableCodeException($"{nameof(BinanceTransferSchema)} identifier can never be {this.Identfier}");
        }
    }
}
