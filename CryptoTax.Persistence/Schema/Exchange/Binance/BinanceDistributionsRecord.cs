﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceDistributionsRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public DString Currency { get; set; }
        public decimal Amount { get; set; }

        public BinanceDistributionsRecord(object[] parsedRow)
        {
            this.Time = (DateTime)parsedRow[0];
            string note = (string)parsedRow[5];
            if (string.IsNullOrEmpty(note)) note = "Unknown";
            this.Id = $"{this.Time.ToString("dd/MM/yyyy")}: {note}";
            this.Currency = new DString((string)parsedRow[1]);
            this.Amount = (decimal)parsedRow[2];

            if (this.Amount <= 0) throw new NotImplementedException("Distribution amount must be positive.");
        }

        public override TransactionDTO ToTransactionDTO()
        {
            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, this.Currency, this.Amount, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.StakingOrAirdrops, containerEvent);
        }
    }
}
