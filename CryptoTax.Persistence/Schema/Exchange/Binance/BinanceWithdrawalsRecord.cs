﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWithdrawalsRecord : Record
    {
        public NString Id { get; set; }
        public DateTime Time { get; set; }
        public DString Coin { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public DString Counterparty { get; set; }
        public BinanceTransferStatus Status { get; set; }

        public BinanceWithdrawalsRecord(object[] parsedRow)
        {
            this.Id = (string)parsedRow[5];
            this.Time = (DateTime)parsedRow[0];
            this.Coin = new DString((string)parsedRow[1]);
            this.Amount = (decimal)parsedRow[2];
            this.Fee = (decimal)parsedRow[3];
            this.Counterparty = (string)parsedRow[4];
            this.Status = (BinanceTransferStatus)parsedRow[8];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.Status == BinanceTransferStatus.Cancelled) return null;
            else if (this.Status == BinanceTransferStatus.Completed)
            {
                DString id = new DString(this.Id);
                NString feeCurrency = null;
                decimal? feeAmount = null;
                if (this.Fee > 0)
                {
                    feeCurrency = this.Coin;
                    feeAmount = this.Fee;
                }

                // at first glance, this might not make sense: https://bithomp.com/explorer/93F23718FC0E4DC7AAD919FC326F0C99E98E835916E7372FE229CA214F0D8F49
                // the "outcome" property in the json response claims the source address withdrew -7147.129825 XRP, and the destination address gained 7147.128825 XRP (the amount stated in the CSV). However, the withdrawal fee on Binance is 0.15 XRP - so we pay the fee such that Binance withdraws an amount from its wallet such that, after transaction fees, the received amount is exactly what we wanted. So no problem here.
                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), this.Coin, this.Amount, null, null, feeCurrency, feeAmount);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, null, id, this.Counterparty, Shared.Enums.TransactionType.CryptoWithdrawal, containerEvent);
            }
            else throw new AssertUnreachable(this.Status);
        }
    }
}
