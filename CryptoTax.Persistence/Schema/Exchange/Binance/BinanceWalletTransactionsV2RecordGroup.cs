﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsV2RecordGroup : RecordGroup
    {
        private readonly IEnumerable<BinanceWalletTransactionsV2Record> _Group;
        public override IEnumerable<Record> Group { get { return this._Group; } }

        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal AmountIn { get; set; }
        public DString CurrencyIn { get; set; }
        /// <summary>
        /// Positive.
        /// </summary>
        public decimal AmountOut { get; set; }
        public DString CurrencyOut { get; set; }

        public BinanceWalletTransactionsV2RecordGroupType Type { get; set; }

        public BinanceWalletTransactionsV2RecordGroup(IEnumerable<BinanceWalletTransactionsV2Record> group, BinanceWalletTransactionsV2RecordGroupType type)
        {
            if (type != BinanceWalletTransactionsV2RecordGroupType.Trade) throw new NotImplementedException();
            BinanceWalletTransactionsV2Record recordIn = group.First().Amount > 0 ? group.First() : group.Last();
            BinanceWalletTransactionsV2Record recordOut = group.First().Amount < 0 ? group.First() : group.Last();

            if (recordIn.Account != BinanceAccountType.Spot || recordOut.Account != BinanceAccountType.Spot) throw new NotImplementedException();

            this._Group = group;
            this.Type = type;
            this.Time = group.First().Time;
            this.Id = $"Trade-{this.Time.ToString("dd/MM/yyyy HH:mm:ss")}";
            this.AmountIn = recordIn.Amount;
            this.CurrencyIn = recordIn.Currency;
            this.AmountOut = -recordOut.Amount;
            this.CurrencyOut = recordOut.Currency;
        }

        public override TradeDTO ToTradeDTO()
        {
            if (this.Type != BinanceWalletTransactionsV2RecordGroupType.Trade) throw new NotImplementedException();

            // note: there are no fees in the "OTC trade" - I guess they are baked into the price

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), this.CurrencyOut, this.AmountOut, this.CurrencyIn, this.AmountIn, null, null);

            // we do not get information on the symbol or trade side
            SymbolDTO symbol = new SymbolDTO(this.CurrencyOut, this.CurrencyIn);
            Shared.Enums.OrderSide side = Shared.Enums.OrderSide.Buy;
            decimal price = this.AmountOut / this.AmountIn; // quote per base
            return new TradeDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, symbol, Shared.Enums.MarketType.Spot, side, price, containerEvent);
        }

        public static IEnumerable<BinanceWalletTransactionsV2RecordGroup> ProcessAll(IEnumerable<BinanceWalletTransactionsV2Record> allRecords)
        {
            List<BinanceWalletTransactionsV2RecordGroup> groups = new List<BinanceWalletTransactionsV2RecordGroup>();

            foreach (IGrouping<DateTime, BinanceWalletTransactionsV2Record> group in allRecords.Where(r => r.Type == BinanceWalletTransactionsV2Type.Trading).GroupBy(r => r.Time))
            {
                TimeSpan range = group.Max(r => r.Time).Subtract(group.Min(r => r.Time));
                if (range > TimeSpan.FromMinutes(1)) throw new Exception("Buy/sell parts must fall within 1 minutes of each other for them to be grouped");
                else if (group.Count() != 2) throw new Exception("Expected 2 records to belong to a single trade");

                groups.Add(new BinanceWalletTransactionsV2RecordGroup(group.ToList(), BinanceWalletTransactionsV2RecordGroupType.Trade));
            }

            return groups;
        }
    }
}
