﻿using CryptoTax.Shared.Enums;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceFuturesTradesSchema : Schema
    {
        readonly DateColumn Time = new DateColumn("Date(UTC)", true, 0);
        readonly Column<string> Pair = new Column<string>("Pair", true);
        readonly EnumColumn<OrderSide> Side = new EnumColumn<OrderSide>("Side", true);
        readonly DecimalColumn Price = new DecimalColumn("Price", true);
        readonly DecimalColumn Quantity = new DecimalColumn("Quantity", true);
        readonly Column<string> Amount = new Column<string>("Amount", true);
        readonly Column<string> Fee = new Column<string>("Fee", true);
        readonly DecimalColumn RealisedPnl = new DecimalColumn("Realized Profit", true);

        public override SchemaIdentifier Identfier { get; } = SchemaIdentifier.Binance_FuturesTrades;
        public override Column[] Columns { get; }

        public BinanceFuturesTradesSchema()
        {
            this.Columns = new Column[]
            {
                this.Time, // 0
                this.Pair, // 1
                this.Side, // 2
                this.Price, // 3
                this.Quantity, // 4
                this.Amount, // 5
                this.Fee, // 6
                this.RealisedPnl // 7
            };
        }

        public override Record ProcessOne(object[] parsedRow)
        {
            return new BinanceFuturesTradesRecord(parsedRow);
        }
    }
}
