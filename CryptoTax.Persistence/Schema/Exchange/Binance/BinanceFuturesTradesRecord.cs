﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceFuturesTradesRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal RealisedPnl { get; set; }

        public BinanceFuturesTradesRecord(object[] parsedRow)
        {
            this.Id = "";
            this.Time = (DateTime)parsedRow[0];
            string amount = (string)parsedRow[5];
            string fee = (string)parsedRow[6];
            decimal realisedPnl = (decimal)parsedRow[7];

            (decimal feeAmount, DString feeCurrency) = Helpers.DestructureAmount(fee);
            (decimal amountAmount, DString amountCurrency) = Helpers.DestructureAmount(amount);

            if (feeCurrency != "USDT" || amountCurrency != "USDT") throw new NotImplementedException("Only supports USDT for binance futures");

            this.RealisedPnl = feeAmount + realisedPnl;
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.RealisedPnl == 0) return null;
            string currencyReceived = this.RealisedPnl < 0 ? null : "USDT";
            decimal? amountReceived = this.RealisedPnl < 0 ? (decimal?)null : Math.Abs(this.RealisedPnl);
            string currencySent = this.RealisedPnl > 0 ? null : "USDT";
            decimal? amountSent = this.RealisedPnl > 0 ? (decimal?)null : Math.Abs(this.RealisedPnl);

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.FuturesPnL, containerEvent);
        }
    }
}
