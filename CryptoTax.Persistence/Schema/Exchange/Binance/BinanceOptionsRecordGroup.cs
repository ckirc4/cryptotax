﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceOptionsRecordGroup : RecordGroup
    {
        private readonly IEnumerable<BinanceOptionsRecord> _Group;
        public override IEnumerable<Record> Group { get { return this._Group; } }

        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public decimal RealisedPnl { get; set; }

        public BinanceOptionsRecordGroup(IEnumerable<BinanceOptionsRecord> group)
        {
            this._Group = group;
            this.Time = group.First().Time;
            this.Id = "Options-" + this.Time.ToString("dd/MM/yyyy HH:mm:ss");

            BinanceOptionsRecord fee = group.Single(r => r.Type == BinanceOptionsType.Fee);
            BinanceOptionsRecord cost = group.Single(r => r.Type == BinanceOptionsType.OptionCost);
            this.RealisedPnl = fee.Amount + cost.Amount;
        }

        public override TransactionDTO ToTransactionDTO()
        {
            if (this.RealisedPnl == 0) return null;

            string currencyReceived = this.RealisedPnl < 0 ? null : "USDT";
            decimal? amountReceived = this.RealisedPnl < 0 ? (decimal?)null : Math.Abs(this.RealisedPnl);
            string currencySent = this.RealisedPnl > 0 ? null : "USDT";
            decimal? amountSent = this.RealisedPnl > 0 ? (decimal?)null : Math.Abs(this.RealisedPnl);

            ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);
            return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.OptionsPnL, containerEvent);
        }

        public static IEnumerable<BinanceOptionsRecordGroup> ProcessAll(IEnumerable<BinanceOptionsRecord> allRecords)
        {
            List<BinanceOptionsRecordGroup> groups = new List<BinanceOptionsRecordGroup>();

            foreach (IGrouping<DateTime, BinanceOptionsRecord> group in allRecords.Where(r => r.Type != BinanceOptionsType.Transfer).GroupBy(r => r.Time))
            {
                if (group.Count() != 2) throw new Exception("Groups must be pairs");
                else if (group.Select(x => x.Type).Distinct().Count() != 2) throw new Exception("Groups must be of differing types");

                groups.Add(new BinanceOptionsRecordGroup(group.ToList()));
            }

            return groups;
        }
    }
}
