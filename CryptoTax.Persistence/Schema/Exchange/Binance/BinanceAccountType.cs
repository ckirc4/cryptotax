﻿namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public enum BinanceAccountType
    {
        Spot,
        FuturesUSDT
    }
}
