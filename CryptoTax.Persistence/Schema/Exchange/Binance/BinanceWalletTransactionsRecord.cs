﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Schema.Exchange.Binance
{
    public class BinanceWalletTransactionsRecord : Record
    {
        public DString Id { get; set; }
        public DateTime Time { get; set; }
        public BinanceAccountType Account { get; set; }
        public BinanceWalletTransactionsType Type { get; set; }
        public DString Currency { get; set; }
        public decimal Amount { get; set; }
        public NString Note { get; set; }

        public BinanceWalletTransactionsRecord(object[] parsedRow)
        {
            this.Id = "";
            this.Time = (DateTime)parsedRow[0];
            string account = (string)parsedRow[1];
            this.Account = parseAccount(account);
            string type = (string)parsedRow[2];
            this.Type = parseType(type);
            this.Currency = (string)parsedRow[3];
            this.Amount = (decimal)parsedRow[4];
            this.Note = (string)parsedRow[5];
        }

        public override TransactionDTO ToTransactionDTO()
        {
            // note: a lot of data in WalletTransactions can already be found in other files (e.g. trades, withdrawals). Make sure we don't count them twice!

            if (this.Type == BinanceWalletTransactionsType.InsuranceFundCompensation)
            {
                // this is a weird one that is only recorded in wallet transactions, but not in futures transactions... it seems to inject very small amounts (on the order of 10 cents) into the account a handful of times.
                if (this.Amount <= 0) throw new NotImplementedException("InsuranceFundCompensation must be a positive amount");
                else if (this.Currency != "USDT") throw new NotImplementedException("InsuranceFundCompensation is expected to be in USDT");

                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), null, null, this.Currency,  this.Amount, null, null);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.StakingOrAirdrops, containerEvent);
            }
            else if (this.Type == BinanceWalletTransactionsType.DustConversionToBnb)
            {
                NString currencySent = null, currencyReceived = null;
                decimal? amountSent = null, amountReceived = null;
                if (this.Amount == 0) throw new Exception("Amounts in a dust conversion must not be zero");
                else if (this.Amount < 0)
                {
                    currencySent = this.Currency;
                    amountSent = Math.Abs(this.Amount);
                }
                else
                {
                    if (this.Currency != "BNB") throw new Exception($"Currency received during dust conversion was expected to be BNB, but was actually '{this.Currency}'");
                    currencyReceived = this.Currency;
                    amountReceived = this.Amount;
                }

                ContainerEventDTO containerEvent = new ContainerEventDTO(this.Time, Container.FromExchange(Shared.Enums.Exchange.Binance), currencySent, amountSent, currencyReceived, amountReceived, null, null);
                return new TransactionDTO(this.Time, Shared.Enums.Exchange.Binance, this.Id, null, null, Shared.Enums.TransactionType.DustConversion, containerEvent);
            }

            return null;
        }

        private static BinanceWalletTransactionsType parseType(string type)
        {
            switch (type)
            {
                case "Sub-account transfer":
                    return BinanceWalletTransactionsType.TransferSubAccount;
                case "Fee":
                    return BinanceWalletTransactionsType.Fee;
                case "Realize profit and loss":
                    return BinanceWalletTransactionsType.RealisedPnl;
                case "Funding Fee":
                    return BinanceWalletTransactionsType.FundingFee;
                case "Insurance fund compensation":
                    return BinanceWalletTransactionsType.InsuranceFundCompensation;
                case "Deposit":
                    return BinanceWalletTransactionsType.Deposit;
                case "Withdraw":
                    return BinanceWalletTransactionsType.Withdrawal;
                case "Distribution":
                    return BinanceWalletTransactionsType.Distribution;
                case "Transaction Related":
                    return BinanceWalletTransactionsType.TransactionRelated;
                case "Buy":
                case "Sell": // not a mistake!
                    return BinanceWalletTransactionsType.Buy;
                case "Small assets exchange BNB":
                    return BinanceWalletTransactionsType.DustConversionToBnb;
                case "transfer_in": // thanks for the consistency, Binance!
                    return BinanceWalletTransactionsType.TransferIn;
                case "transfer_out":
                    return BinanceWalletTransactionsType.TransferOut;
                default:
                    throw new AssertUnreachable(type, $"Value is incompatible with type {typeof(BinanceWalletTransactionsType)}");
            }
        }

        private static BinanceAccountType parseAccount(string account)
        {
            switch (account)
            {
                case "Spot":
                    return BinanceAccountType.Spot;
                case "USDT-Futures":
                    return BinanceAccountType.FuturesUSDT;
                default:
                    throw new AssertUnreachable(account, $"Value is incompatible with type {typeof(BinanceAccountType)}");
            }
        }
    }
}
