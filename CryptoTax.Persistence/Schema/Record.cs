﻿using CryptoTax.DTO;
using System.Collections.Generic;

namespace CryptoTax.Persistence.Schema
{
    // reminder: virtual methods CAN be overridden.
    // and abstract classes cannot be directly instantiated (i.e. no constructor) but instead provide base functionality for its children.
    public abstract class Record
    {
        /// <summary>
        /// Convert a single record (row) into a trade object. Return value may be null.
        /// </summary>
        public virtual TradeDTO ToTradeDTO() { return null; }

        /// <summary>
        /// Convert a single record (row) into a transaction object. Return value may be null.
        /// </summary>
        public virtual TransactionDTO ToTransactionDTO() { return null; }
    }

    public abstract class RecordGroup
    {
        public abstract IEnumerable<Record> Group { get; }

        /// <summary>
        /// Convert a group of records (rows) into a trade object. Return value may be null.
        /// </summary>
        public virtual TradeDTO ToTradeDTO() { return null; }

        /// <summary>
        /// Convert a group of records (rows) into a transaction object. Return value may be null.
        /// </summary>
        public virtual TransactionDTO ToTransactionDTO() { return null; }
    }
}
