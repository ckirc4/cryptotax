﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System.Collections.Generic;

namespace CryptoTax.Persistence.Services
{
    /// <summary>
    /// Saves trades and transactions for a financial year and allows them to be loaded if exchange data has not changed.
    /// </summary>
    public interface IDtoCachePersistorService
    {
        void CacheAll(FinancialYear year, IEnumerable<TradeDTO> trades, IEnumerable<TransactionDTO> transactions);

        /// <summary>
        /// Ensure you check if RequiresRefresh() is false first, otherwise this may result in undesirable behaviour (exceptions).
        /// </summary>
        (IEnumerable<TradeDTO> trades, IEnumerable<TransactionDTO> transactions) LoadAll(FinancialYear year);

        bool RequiresRefresh(FinancialYear year);
    }
}