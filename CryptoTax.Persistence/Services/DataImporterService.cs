﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class DataImporterService
    {
        private readonly IFileService _FileService;
        private readonly IExchangeDataLocationService _ExchangeDataLocationService;
        private readonly IDtoCachePersistorService _DtoCachePersistorService;
        private readonly ICsvDataReaderServiceFactory _CsvDataReaderServiceFactory;

        public DataImporterService(IFileService fileService, IExchangeDataLocationService exchangeDataLocationService, IDtoCachePersistorService dtoCachePersistorService, ICsvDataReaderServiceFactory csvDataReaderServiceFactory)
        {
            this._FileService = fileService;
            this._ExchangeDataLocationService = exchangeDataLocationService;
            this._DtoCachePersistorService = dtoCachePersistorService;
            this._CsvDataReaderServiceFactory = csvDataReaderServiceFactory;
        }

        /// <summary>
        /// Imports the data for the specified year, generating either from the raw exchange data or by loading an existing cache.
        /// </summary>
        public (IEnumerable<TradeDTO> trades, IEnumerable<TransactionDTO> transactions) ImportAll(FinancialYear year)
        {
            // load cache if it doesn't require refresh
            // otherwise, load and parse data and then save to cache

            if (this._DtoCachePersistorService.RequiresRefresh(year))
            {
                IEnumerable<TradeDTO> trades = new List<TradeDTO>();
                IEnumerable<TransactionDTO> transactions = new List<TransactionDTO>();

                foreach (Exchange exchange in Enum.GetValues(typeof(Exchange)))
                {
                    try
                    {
                        ICsvDataReaderService dataProvider = this.getDataProvider(exchange, year);
                        
                        // no data exist
                        if (dataProvider == null) continue;

                        trades = trades.Concat(dataProvider.GetTrades());
                        transactions = transactions.Concat(dataProvider.GetTransactions());
                    }
                    catch (NotImplementedException)
                    {
                        // exchange not implemented yet
                        continue;
                    }
                }

                trades = trades.OrderBy(t => t.Time);
                transactions = transactions.OrderBy(t => t.Time);
                this._DtoCachePersistorService.CacheAll(year, trades, transactions);

                return (trades, transactions);
            }
            else
            {
                return this._DtoCachePersistorService.LoadAll(year);
            }
        }

        /// <summary>
        /// Get the data service for the given exchange. Returns null if no data exists.
        /// </summary>
        /// <exception cref="NotImplementedException">Thrown if no data exists for the exchange.</exception>
        private ICsvDataReaderService getDataProvider(Exchange exchange, FinancialYear financialYear)
        {
            Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> schemaFiles = this._ExchangeDataLocationService.GetDataSources(exchange, financialYear);

            IEnumerable<(SchemaIdentifier id, IEnumerable<IEnumerable<DString>> files)> tuples = schemaFiles.Select(x => (x.Key, x.Value));

            // create and extend reader service, depending on the files/schema types for this exchange.
            ICsvDataReaderService dataService = null;
            foreach ((SchemaIdentifier id, IEnumerable<IEnumerable<DString>> separatedFiles) in tuples)
            {
                int group = 0;
                foreach (IEnumerable<DString> files in separatedFiles)
                {
                    foreach (DString file in files)
                    {
                        if (dataService == null)
                        {
                            dataService = this._CsvDataReaderServiceFactory.Create(this._FileService, new CsvInfo(file), SchemaFactory.Create(id, file), financialYear, group);
                        }
                        else
                        {
                            dataService.WithAdditionalSource(new CsvInfo(file), SchemaFactory.Create(id, file), group);
                        }
                    }

                    group++;
                }
            }

            return dataService;
        }
    }
}
