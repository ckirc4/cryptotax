﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System.Collections.Generic;

namespace CryptoTax.Persistence.Services
{
    public interface ICandlestickPersistorService
    {
        /// <summary>
        /// Wether any data is persisted on the disk for this exchange-symbol-interval pair.
        /// </summary>
        bool HasData(ExchangeSymbol symbol, CandlestickInterval interval);

        List<CandlestickDTO> ReadCandlesticks(ExchangeSymbol symbol, CandlestickInterval interval);

        /// <summary>
        /// Writes the given collection of candlesticks to the disk using the given exchange, symbol, and interval name as index. Does not verify that those three parameters agree with the information contained within each candlestick. <br/><br/>
        /// IMPORTANT: OVERWRITES ANY EXISTING DATA - NEVER PROVIDE ONLY A PARTIAL COLLECTION
        /// </summary>
        void WriteCandlesticks(ExchangeSymbol symbol, CandlestickInterval interval, List<CandlestickDTO> candlesticks);
    }
}