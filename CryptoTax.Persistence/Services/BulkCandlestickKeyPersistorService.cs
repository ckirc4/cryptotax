﻿using CryptoTax.Shared.Services;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class BulkCandlestickKeyPersistorService
    {
        private readonly IFileService _FileService;
        private readonly DString _Folder;
        private readonly DString _FileName;
        private readonly object _Lock;

        private IEnumerable<DString> _Cache;

        public BulkCandlestickKeyPersistorService(IFileService fileService, DString folder)
        {
            this._FileService = fileService;
            this._Folder = ((string)folder).EndsWith(@"\") ? folder : folder += @"\";
            this._FileName = this._Folder + "bulkCandlestickKeys.json";
            this._Lock = new object();

            lock (this._Lock)
            {
                this._Cache = this.readKeys();
            }
        }

        public bool HasKey(DString key)
        {
            lock (this._Lock)
            {
                return this._Cache.Contains(key);
            }
        }

        public void AddKey(DString key)
        {
            lock (this._Lock)
            {
                IEnumerable<DString> keys = this._Cache.Append(key);
                this.writeKeys(keys);
                this._Cache = keys;
            }
        }

        private IEnumerable<DString> readKeys()
        {
            if (this._FileService.GetFilesInFolder(this._Folder).Contains(this._FileName))
            {
                return this._FileService.ReadLines(this._FileName);
            }
            else
            {
                return new List<DString>();
            }
        }

        private void writeKeys(IEnumerable<DString> keys)
        {
            this._FileService.WriteLines(this._FileName, keys.Select(key => new NString(key)), false);
        }
    }
}
