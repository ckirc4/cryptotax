﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;

namespace CryptoTax.Persistence.Services
{
    public interface IExchangeResponsePersistorService
    {
        /// <summary>
        /// Adds the given exception to the cache, overwriting the existing value if one already exists.
        /// </summary>
        void CacheException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, T exception) where T : Exception;

        /// <summary>
        /// Attempts to get a cached exception for the given request details. Returns true if an exception was found of the given type and outputs that exception. Returns false if no exception was cached.
        /// </summary>
        /// <exception cref="InvalidTypeException">Thrown if an exception was cached for the given request details, but was of a different type.</exception>
        bool TryGetCachedException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, out T exception) where T : Exception;
    }
}