﻿using CryptoTax.Persistence.Schema;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public interface IExchangeDataLocationService
    {
        /// <summary>
        /// The outer collection is to separate sub-accounts (e.g. Binance Master vs Classic), while the inner collection contains a list of related file names.
        /// </summary>
        Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> GetDataSources(Exchange exchange, FinancialYear year);
    }
}