﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class MissingCandlestickPersistorService : IMissingCandlestickPersistorService
    {
        private readonly IFileService _FileService;
        private readonly DString _FileName;
        private Dictionary<string, IEnumerable<DateTime>> _MissingDataCache;
        private readonly object _DataLock;
        private readonly CandlestickInterval _Interval;

        public MissingCandlestickPersistorService(IFileService fileService, DString folder, CandlestickInterval interval)
        {
            this._FileService = fileService;
            folder = folder.Value.EndsWith(@"\") ? folder : folder + @"\";
            this._FileName = folder + $"MissingCandlestickData{interval}.json";
            this._DataLock = new object();
            this._Interval = interval;

            lock (this._DataLock)
            {
                this._MissingDataCache = this.readSavedData();
            }
        }

        /// <summary>
        /// Returns true if we know that there is missing data for all or some of the given range of times (inclusive). Also returns the upper bound (inclusive) of the missing data.
        /// </summary>
        public bool IncludesKnownMissingData(Exchange exchange, DString symbol, DateTime fromUTC, DateTime toUTC, out DateTime? unknownDataUntilTime)
        {
            string key = generateKey(exchange, symbol);
            lock (this._DataLock)
            {
                IOrderedEnumerable<DateTime> missing = this.get(key).Where(t => t >= fromUTC && t <= toUTC).OrderBy(t => t);
                if (missing.Any())
                {
                    unknownDataUntilTime = missing.Last();
                    return true;
                }
            }

            unknownDataUntilTime = null;
            return false;
        }

        /// <summary>
        /// Add a range of times (inclusive) for which there is no candlestick data.
        /// </summary>
        public void AddKnownMissingData(Exchange exchange, DString symbol, DateTime noDataFrom, DateTime noDataTo)
        {
            string key = generateKey(exchange, symbol);
            lock (this._DataLock)
            {
                IEnumerable<DateTime> missing = this.get(key);
                IEnumerable<DateTime> relevantExisting = missing.Where(t => t >= noDataFrom && t <= noDataTo);
                DateTime nextTime = noDataFrom;
                do
                {
                    // don't duplicate times
                    if (!relevantExisting.Contains(nextTime)) missing = missing.Append(nextTime);
                    nextTime = CandlestickHelpers.GetNextTime(this._Interval, nextTime);
                } while (nextTime <= noDataTo);

                this.set(key, missing.OrderBy(t => t));
                this.writeAllData(this._MissingDataCache);
            }
        }

        private IEnumerable<DateTime> get(string key)
        {
            if (this._MissingDataCache.TryGetValue(key, out IEnumerable<DateTime> result))
            {
                return result;
            }
            else
            {
                return new List<DateTime>();
            }
        }

        private void set(string key, IEnumerable<DateTime> value)
        {
            if (this._MissingDataCache.ContainsKey(key))
            {
                this._MissingDataCache[key] = value;
            }
            else
            {
                this._MissingDataCache.Add(key, value);
            }
        }

        private Dictionary<string, IEnumerable<DateTime>> readSavedData()
        {
            try
            {
                return this._FileService.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(this._FileName)
                    ?? new Dictionary<string, IEnumerable<DateTime>>();
            }
            catch (FileNotFoundException)
            {
                return new Dictionary<string, IEnumerable<DateTime>>();
            }
        }

        private void writeAllData(Dictionary<string, IEnumerable<DateTime>> data)
        {
            string contents = JsonConvert.SerializeObject(data);
            this._FileService.WriteFile(this._FileName, contents, false);
        }

        private static string generateKey(Exchange exchange, DString symbol)
        {
            return $"{exchange}:{symbol}";
        }
    }
}
