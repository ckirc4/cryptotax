﻿using CryptoTax.DTO;
using CryptoTax.Shared.Services;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class ReportingPersistorService : FlushablePersistorService<ReportRowDTO>, IReportingPersistorService
    {
        private const int _MAX_ROWS_PER_REPORT = 200_000;
        private const int _MAX_PENDING_ROWS = 100;

        public ReportingPersistorService(IFileService fileService, DString folder)
            : base(fileService,
                  generateLineString,
                  new NString[] { generateHeader() },
                  (index) => $"Report_{index}.csv",
                  _MAX_ROWS_PER_REPORT,
                  _MAX_PENDING_ROWS,
                  folder)
        { }

        /// <summary>
        /// Appends the row to the current run's report - if saving is required, please also call <see cref="Flush"/>.
        /// </summary>
        public void AddRow(ReportRowDTO row)
        {
            this.AddObject(row);
        }

        private static NString generateHeader()
        {
            return sanitiseCsv(new DString[]
            {
                "id",
                "time",
                "category",
                "types",
                "message",
                "standardCapitalGains",
                "discountableCapitalGains",
                "capitalLosses",
                "assessableIncome"
            });
        }

        private static NString generateLineString(ReportRowDTO row)
        {
            string types = string.Join(";", row.Types.Select(t => t.ToString()));
            DString[] csv = new DString[] {
                $"{row.Id}",
                $"{row.Time.ToUniversalTime().ToString("dd/MM/yyyy HH:mm:ss.fff")}",
                $"{row.Category}",
                $"{types}",
                $"{row.Message}",
                $"{row.StandardCapitalGains}",
                $"{row.DiscountableCapitalGains}",
                $"{row.CapitalLosses}",
                $"{row.AssessableIncome}"
            };
            return sanitiseCsv(csv);
        }

        private static NString sanitiseCsv(DString[] csv)
        {
            DString sanitisedCsv = string.Join(",", csv.Select(value =>
            {
                DString escapedValue = value.Value
                    .Replace("\"", "\"\"") // double quotes must be escaped using an extra double quote
                    .Replace("\n", "\\\n"); // newlines must be escaped using an extra backslash
                return string.IsNullOrEmpty(escapedValue) ? escapedValue.Value : $"\"{escapedValue}\"";
            }));

            return sanitisedCsv;
        }
    }
}
