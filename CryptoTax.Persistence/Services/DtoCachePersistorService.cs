﻿using CryptoTax.DTO;
using CryptoTax.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using UtilityLibrary.Types;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.DTO.Serialised;

namespace CryptoTax.Persistence.Services
{
    /// <summary>
    /// Saves trades and transactions for a financial year and allows them to be loaded if exchange data has not changed.
    /// </summary>
    public class DtoCachePersistorService : IDtoCachePersistorService
    {
        private readonly IFileService _FileService;
        private readonly ICryptoService _CryptoService;
        private readonly IExchangeDataLocationService _ExchangeDataLocationService;

        private readonly DString _CacheFolder;
        private readonly Func<FinancialYear, DString> _FileNameTrades;
        private readonly Func<FinancialYear, DString> _FileNameTransactions;
        private readonly DString _FileNameHashes;

        public DtoCachePersistorService(IFileService fileService, ICryptoService cryptoService, IExchangeDataLocationService exchangeDataLocationService, DString cacheFolder)
        {
            this._FileService = fileService;
            this._CryptoService = cryptoService;
            this._ExchangeDataLocationService = exchangeDataLocationService;

            this._CacheFolder = cacheFolder.Value.EndsWith(@"\") ? cacheFolder : cacheFolder += @"\";
            this._FileNameTrades = (fy) => $"{this._CacheFolder}{fy.Name}_Trades.json";
            this._FileNameTransactions = (fy) => $"{this._CacheFolder}{fy.Name}_Transactions.json";
            this._FileNameHashes = this._CacheFolder + "Hashes.json";
        }

        public bool RequiresRefresh(FinancialYear year)
        {
            if (this._FileService.GetFilesInFolder(this._CacheFolder).Contains(this._FileNameHashes))
            {
                DString contents = this._FileService.ReadFile(this._FileNameHashes);
                Dictionary<string, string> hashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(contents);

                if (hashes.ContainsKey(year.Name))
                {
                    // we have a previous version available... now make sure exchange data has not changed
                    DString currentHash = this.calculateHash(year);
                    bool requiresRefresh = hashes[year.Name] != currentHash;
                    return requiresRefresh;
                }
                else
                {
                    return true;
                }
            }

            // no cached data
            return true;
        }

        /// <summary>
        /// Ensure you check if RequiresRefresh() is false first, otherwise this may result in undesirable behaviour (exceptions).
        /// </summary>
        public (IEnumerable<TradeDTO> trades, IEnumerable<TransactionDTO> transactions) LoadAll(FinancialYear year)
        {
            IEnumerable<SerialisedTradeDTO> trades = this._FileService.ReadAndDeserialiseFile<IEnumerable<SerialisedTradeDTO>>(this._FileNameTrades(year));
            IEnumerable<SerialisedTransactionDTO> transactions = this._FileService.ReadAndDeserialiseFile<IEnumerable<SerialisedTransactionDTO>>(this._FileNameTransactions(year));

            return (trades.Select(t => t.DeserialiseTrade()), transactions.Select(t => t.DeserialiseTx()));
        }

        public void CacheAll(FinancialYear year, IEnumerable<TradeDTO> trades, IEnumerable<TransactionDTO> transactions)
        {
            string serialisedTrades = JsonConvert.SerializeObject(trades.Select(t => t.Serialise()));
            this._FileService.WriteFile(this._FileNameTrades(year), serialisedTrades, false);

            string serialisedTransactions = JsonConvert.SerializeObject(transactions.Select(t => t.Serialise()));
            this._FileService.WriteFile(this._FileNameTransactions(year), serialisedTransactions, false);

            // once all data saved, also save hash to signify that we have cached data for this year for this exchange data
            DString hash = this.calculateHash(year);
            this.updateHashFile(year, hash);

            if (this._FileService is FileServiceCached fileServiceCached) fileServiceCached.FlushAll();
        }

        private DString calculateHash(FinancialYear year)
        {
            List<DString> allHashes = new List<DString>();
            foreach (Exchange exchange in Enum.GetValues(typeof(Exchange)))
            {
                IEnumerable<DString> fileNames;
                try
                {
                    fileNames = this._ExchangeDataLocationService.GetDataSources(exchange, year).SelectMany(s => s.Value.SelectMany(t => t));
                }
                catch (NotImplementedException)
                {
                    // ignore
                    continue;
                }

                // compute hash for each file's contents
                foreach (DString fileName in fileNames)
                {
                    DString hash = this._FileService.GetFingerprint(fileName);
                    allHashes.Add(hash);
                }
            }

            // compute a "master hash" of all data
            string joinedHashes = string.Join("", allHashes);
            DString masterHash = this._CryptoService.CalculateHash(joinedHashes);
            return masterHash;
        }

        private void updateHashFile(FinancialYear year, DString hash)
        {
            Dictionary<string, string> existingHashes = new Dictionary<string, string>();

            if (this._FileService.GetFilesInFolder(this._CacheFolder).Contains(this._FileNameHashes))
            {
                DString contents = this._FileService.ReadFile(this._FileNameHashes);
                existingHashes = JsonConvert.DeserializeObject<Dictionary<string, string>>(contents);
            }

            if (existingHashes.ContainsKey(year.Name)) existingHashes[year.Name] = hash;
            else existingHashes.Add(year.Name, hash);

            string serialised = JsonConvert.SerializeObject(existingHashes);
            this._FileService.WriteFile(this._FileNameHashes, serialised, false);
        }
    }
}
