﻿using CryptoTax.DTO;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public interface IRunPersistorService
    {
        /// <summary>
        /// Returns if the RunInfo with the specified ID exists in the file system.
        /// </summary>
        bool Exists(DString runInfoId);
        RunInfoDTO Load(DString oldRunInfoId, DString newRunInfoId, string[] newArgs);
        RunStateDTO LoadState(DString runInfoId);
        void Save(RunInfoDTO runInfo);
        void SaveResults(CalculationResultsDTO results, DString runId);
        void SaveState(RunStateDTO runState);
    }
}