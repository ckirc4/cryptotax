﻿using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using System.Collections.Generic;
using UtilityLibrary.Types;
using Newtonsoft.Json;
using System.Linq;
using System.IO;
using CryptoTax.DTO.Serialised;
using CryptoTax.Shared;

namespace CryptoTax.Persistence.Services
{
    public class CandlestickPersistorService : ICandlestickPersistorService
    {
        private readonly IFileService _FileService;
        private readonly DString _Path;
        /// <summary>
        /// All full file names that correspond to an exchange-symbol-interval for which we have data persisted to the disk.
        /// </summary>
        private IEnumerable<DString> _Index;

        public CandlestickPersistorService(IFileService fileService, DString candlestickDataPath)
        {
            this._FileService = fileService;
            this._Path = candlestickDataPath;
            this._Index = this._FileService.GetFilesInFolder(this._Path);
        }

        public List<CandlestickDTO> ReadCandlesticks(ExchangeSymbol symbol, CandlestickInterval interval)
        {
            DString fileName = this.getFullFileName(symbol, interval);
            if (!this._Index.Contains(fileName)) return new List<CandlestickDTO>();

            SerialisedCandlestickDtoCollection collection = this._FileService.ReadAndDeserialiseFile<SerialisedCandlestickDtoCollection>(fileName);
            return collection.Deserialise().ToList();
        }

        /// <summary>
        /// Writes the given collection of candlesticks to the disk using the given exchange, symbol, and interval name as index. Does not verify that those three parameters agree with the information contained within each candlestick. <br/><br/>
        /// IMPORTANT: OVERWRITES ANY EXISTING DATA - NEVER PROVIDE ONLY A PARTIAL COLLECTION
        /// </summary>
        public void WriteCandlesticks(ExchangeSymbol symbol, CandlestickInterval interval, List<CandlestickDTO> candlesticks)
        {
            // we don't currently allow inserting data
            DString fileName = this.getFullFileName(symbol, interval);
            if (!this._Index.Contains(fileName)) this._Index = this._Index.Append(fileName);

            SerialisedCandlestickDtoCollection collection = new SerialisedCandlestickDtoCollection(candlesticks);
            DString contents = JsonConvert.SerializeObject(collection, Formatting.None);
            this._FileService.WriteFile(fileName, contents, false);
        }

        /// <summary>
        /// Wether any data is persisted on the disk for this exchange-symbol-interval pair.
        /// </summary>
        public bool HasData(ExchangeSymbol symbol, CandlestickInterval interval)
        {
            return this._Index.Contains(this.getFullFileName(symbol, interval));
        }

        private DString getFullFileName(ExchangeSymbol symbol, CandlestickInterval interval)
        {
            string fileName = $"{symbol.Exchange}_{symbol.Name}_{interval}.json";
            Path.GetInvalidFileNameChars().ToList().ForEach(c => fileName = fileName.Replace(c.ToString(), ""));
            return this._Path + fileName;
        }
    }
}
