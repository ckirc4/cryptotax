﻿using CryptoTax.DTO;

namespace CryptoTax.Persistence.Services
{
    public interface IReportingPersistorService
    {
        void AddRow(ReportRowDTO row);
        void Flush();
    }
}