﻿using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public interface IMissingCandlestickPersistorService
    {
        /// <summary>
        /// Returns true if we know that there is missing data for all or some of the given range of times (inclusive). Also returns the upper bound (inclusive) of the missing data.
        /// </summary>
        void AddKnownMissingData(Exchange exchange, DString symbol, DateTime noDataFrom, DateTime noDataTo);

        /// <summary>
        /// Add a range of times (inclusive) for which there is no candlestick data.
        /// </summary>
        bool IncludesKnownMissingData(Exchange exchange, DString symbol, DateTime fromUTC, DateTime toUTC, out DateTime? unknownDataUntilTime);
    }
}