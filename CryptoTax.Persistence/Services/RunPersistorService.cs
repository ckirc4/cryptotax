﻿using CryptoTax.DTO;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class RunPersistorService : IRunPersistorService
    {
        private readonly IFileService _FileService;
        private readonly Func<DString, DString> _OutputFolder;

        /// <summary>
        /// Output folder should return the folder for a run given a run id.
        /// </summary>
        public RunPersistorService(IFileService fileService, Func<DString, DString> outputFolder)
        {
            this._FileService = fileService;
            this._OutputFolder = (id) =>
            {
                // sanitise folder name
                DString folder = outputFolder(id);
                return folder.Value.EndsWith("\\") ? folder : folder + "\\";
            };
        }

        public void Save(RunInfoDTO runInfo)
        {
            string contents = JsonConvert.SerializeObject(runInfo);
            string filePath = this.getRunInfoFilePath(runInfo.Id);
            this._FileService.WriteFile(filePath, contents, true);
        }

        public void SaveState(RunStateDTO runState)
        {
            string contents = JsonConvert.SerializeObject(runState);
            string filePath = this.getRunStateFilePath(runState.RunId);
            this._FileService.WriteFile(filePath, contents, false);
        }

        public void SaveResults(CalculationResultsDTO results, DString runId)
        {
            string contents = JsonConvert.SerializeObject(results);
            string filePath = this.getCalculationResultsFilePath(runId);
            this._FileService.WriteFile(filePath, contents, true);
        }

        public RunInfoDTO Load(DString oldRunInfoId, DString newRunInfoId, string[] newArgs)
        {
            if (!this.Exists(oldRunInfoId)) throw new ArgumentException($"Run info with id {oldRunInfoId} does not exist.");

            string contents = this._FileService.ReadFile(this.getRunInfoFilePath(oldRunInfoId));
            RunInfoDTO loadedRunInfo = JsonConvert.DeserializeObject<RunInfoDTO>(contents);

            loadedRunInfo.Id = newRunInfoId;
            loadedRunInfo.OriginalArgs = newArgs;
            return loadedRunInfo;
        }

        public RunStateDTO LoadState(DString runInfoId)
        {
            // if we can't reproduce a run state (using its associated run info) then it's invalid
            if (!this.Exists(runInfoId)) throw new ArgumentException($"Could not load run state because run info with id {runInfoId} does not exist.");

            string contents = this._FileService.ReadFile(this.getRunStateFilePath(runInfoId));
            return JsonConvert.DeserializeObject<RunStateDTO>(contents);
        }

        public bool Exists(DString runInfoId)
        {
            string outputFolder = this._OutputFolder(runInfoId);
            string filePath = this.getRunInfoFilePath(runInfoId);
            return this._FileService.GetFilesInFolder(outputFolder).Any(f => f == filePath);
        }

        private DString getRunInfoFilePath(DString runInfoId)
        {
            return this._OutputFolder(runInfoId) + "RunInfo.json";
        }

        private DString getRunStateFilePath(DString runInfoId)
        {
            return this._OutputFolder(runInfoId) + "RunState.json";
        }

        private DString getCalculationResultsFilePath(DString runInfoId)
        {
            return this._OutputFolder(runInfoId) + "Results.json";
        }
    }
}
