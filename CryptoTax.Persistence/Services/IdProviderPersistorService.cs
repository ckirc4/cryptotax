﻿using CryptoTax.DTO;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class IdProviderPersistorService
    {
        private readonly IFileService _FileService;
        private readonly DString _Folder;
        private readonly DString _FileName;

        public IdProviderPersistorService(IFileService fileService, DString folder)
        {
            this._FileService = fileService;
            this._Folder = ((string)folder).EndsWith(@"\") ? folder : folder += @"\";
            this._FileName = this._Folder + "ids.json";
        }

        public IdProviderDTO Read()
        {
            if (this._FileService.GetFilesInFolder(this._Folder).Contains(this._FileName))
            {
                string contents = this._FileService.ReadFile(this._FileName);
                IdProviderDTO model = JsonConvert.DeserializeObject<IdProviderDTO>(contents);
                return model;
            }
            else
            {
                return IdProviderDTO.Empty;
            }
        }

        public void Write(IdProviderDTO dto)
        {
            string contents = JsonConvert.SerializeObject(dto);
            this._FileService.WriteFile(this._FileName, contents, false);
        }
    }
}
