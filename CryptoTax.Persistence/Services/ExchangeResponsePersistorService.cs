﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    public class ExchangeResponsePersistorService : IExchangeResponsePersistorService
    {
        private readonly IFileService _FileService;
        private readonly object _ExceptionFileLock;
        private readonly DString _ExceptionFile;
        /// <summary>
        /// Cache exceptions in memory so we don't have to keep reading from the file for every get request, and persist every time there is a change.
        /// </summary>
        private readonly Dictionary<DString, SerialisedException> _ExceptionCache;

        public ExchangeResponsePersistorService(IFileService fileService, DString folder)
        {
            this._FileService = fileService;
            this._ExceptionFileLock = new object();
            folder = folder.Value.EndsWith(@"\") ? folder : folder + @"\";
            this._ExceptionFile = folder + "ExchangeResponseException.json";

            lock (this._ExceptionFileLock)
            {
                this._ExceptionCache = this.loadAllExceptions();
            }
        }

        public void CacheException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, T exception) where T : Exception
        {
            DString key = GetExceptionCacheKey(exchange, url, parameters);
            SerialisedException value = SerialisedException.FromException(exception);

            lock (this._ExceptionFileLock)
            {
                if (this._ExceptionCache.ContainsKey(key))
                {
                    this._ExceptionCache[key] = value;
                }
                else
                {
                    this._ExceptionCache.Add(key, value);
                }
                this.writeAllExceptions(this._ExceptionCache);
            }
        }

        public bool TryGetCachedException<T>(Exchange exchange, string url, Dictionary<string, string> parameters, out T exception) where T : Exception
        {
            DString key = GetExceptionCacheKey(exchange, url, parameters);

            if (this._ExceptionCache.TryGetValue(key, out SerialisedException serialised)) {
                exception = serialised.ToException<T>();
                return true;
            }
            else
            {
                exception = null;
                return false;
            }
        }

        public static string GetExceptionCacheKey(Exchange exchange, string url, Dictionary<string, string> parameters)
        {
            if (parameters != null && parameters.Any())
            {
                return $"{exchange}:{url}?{string.Join("&", parameters.Select(kvp => $"{kvp.Key}={kvp.Value}"))}";
            }
            else
            {
                return $"{exchange}:{url}";
            }
        }

        private void writeAllExceptions(Dictionary<DString, SerialisedException> exceptions)
        {
            DString contents = JsonConvert.SerializeObject(exceptions);
            this._FileService.WriteFile(this._ExceptionFile, contents, false);
        }

        private Dictionary<DString, SerialisedException> loadAllExceptions()
        {
            try
            {
                return this._FileService.ReadAndDeserialiseFile<Dictionary<DString, SerialisedException>>(this._ExceptionFile) ?? new Dictionary<DString, SerialisedException>();
            }
            catch (FileNotFoundException)
            {
                return new Dictionary<DString, SerialisedException>();
            }
        }
    }
}
