﻿using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.Binance;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services
{
    /// <summary>
    /// Locates the files for exchange data.
    /// </summary>
    public class ExchangeDataLocationService : IExchangeDataLocationService
    {
        private readonly DString _Folder;

        public ExchangeDataLocationService(DString folder)
        {
            this._Folder = folder.Value.EndsWith(@"\") ? folder : folder + @"\";
        }

        /// <summary>
        /// Gets the filenames relevant to the specified exchange and year, for possibly multiple types of schemas for that exchange.
        /// </summary>
        public Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> GetDataSources(Exchange exchange, FinancialYear year)
        {
            string folder = this._Folder;
            // save room by assigning single collections to the simple dict.
            Dictionary<SchemaIdentifier, IEnumerable<DString>> simpleDict = new Dictionary<SchemaIdentifier, IEnumerable<DString>>();
            Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> fullDict = new Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>>();

            switch (exchange)
            {
                case Exchange.MercatoX:
                    if (year == FinancialYear.Y2018 || year == FinancialYear.Y2020) simpleDict.Add(SchemaIdentifier.MercatoX, new DString[] { folder + @"MercatoX\MercatoX.csv" });
                    break;
                case Exchange.OnChain:
                    if (year >= FinancialYear.Y2018 && year <= FinancialYear.Y2020) simpleDict.Add(SchemaIdentifier.OnChain_General, new DString[] { folder + @"OnChain\0x7042db4c642f6ac868df6717b65be6d4590ea720.csv" });
                    if (year == FinancialYear.Y2019) simpleDict.Add(SchemaIdentifier.OnChain_0xe5dd, new DString[] { folder + @"OnChain\0xe5dd240231608367fb2d61d5348693a7cbf5396e.csv" });
                    if (year == FinancialYear.Y2022) fullDict.Add(SchemaIdentifier.OnChain_General, new DString[][]
                        {
                            new DString[] { folder + @"OnChain\2022\EthereumMainnet\0xa5478cab569fee520a63d6b43e5faef45ff3ef99.csv" },
                            new DString[] { folder + @"OnChain\2022\EthereumArbitrumNova\0xa5478cab569fee520a63d6b43e5faef45ff3ef99.csv" },
                            new DString[] { folder + @"OnChain\2022\XRPL\rPTHVbcCBiwrqBr4cH8UnUcPgSQLxojWeg.csv" },
                        });
                    if (year == FinancialYear.Y2023) fullDict.Add(SchemaIdentifier.OnChain_General, new DString[][]
                        {
                            new DString[] { folder + @"OnChain\2023\EthereumMainnet\0xa5478cab569fee520a63d6b43e5faef45ff3ef99.csv" },
                            new DString[] { folder + @"OnChain\2023\EthereumArbitrumNova\0xa5478cab569fee520a63d6b43e5faef45ff3ef99.csv" },
                            new DString[] { folder + @"OnChain\2023\XRPL\rPTHVbcCBiwrqBr4cH8UnUcPgSQLxojWeg.csv" },
                            new DString[] { folder + @"OnChain\2023\XRPL\rfnDhAFPgAfFnatVGRBiWHXZeFjqK4sVFL.csv" },
                        });
                    break;
                case Exchange.BTCMarkets:
                    if (year >= FinancialYear.Y2016 && year <= FinancialYear.Y2020) simpleDict.Add(SchemaIdentifier.BtcMarkets, new DString[] { folder + @"BTCMarkets\BTCMarkets.csv" });
                    if (year == FinancialYear.Y2022) simpleDict.Add(SchemaIdentifier.BtcMarkets, new DString[] { folder + @"BTCMarkets\BTCMarkets_2022-2023.csv" });
                    if (year == FinancialYear.Y2023) simpleDict.Add(SchemaIdentifier.BtcMarkets, new DString[] { folder + @"BTCMarkets\BTCMarkets_2023-2024.csv" });
                    break;
                case Exchange.BitMex:
                    if (year == FinancialYear.Y2018 || year == FinancialYear.Y2019) simpleDict.Add(SchemaIdentifier.BitMex, new DString[] { folder + @"BitMex\BitMex.csv" });
                    break;
                case Exchange.FTX:
                    if (year == FinancialYear.Y2019) simpleDict.Add(SchemaIdentifier.Ftx, new DString[] { folder + @"FTX\FTX_2019-2020.csv" });
                    else if (year == FinancialYear.Y2020) simpleDict.Add(SchemaIdentifier.Ftx, new DString[] { folder + @"FTX\FTX_2020-2021.csv" });
                    else if (year == FinancialYear.Y2021) simpleDict.Add(SchemaIdentifier.Ftx, new DString[] { folder + @"FTX\FTX_2021-2022.csv" });
                    else if (year == FinancialYear.Y2022) simpleDict.Add(SchemaIdentifier.Ftx, new DString[] { folder + @"FTX\FTX_2022-2023.csv" });
                    break;
                case Exchange.Binance:
                    fullDict = getBinanceFiles(year);
                    break;
                case Exchange.Kraken:
                    if (year == FinancialYear.Y2023)
                    {
                        simpleDict.Add(SchemaIdentifier.Kraken_Trades, new DString[] { folder + @"Kraken\Kraken_Trades_2023-2024.csv" });
                        simpleDict.Add(SchemaIdentifier.Kraken_Ledgers, new DString[] { folder + @"Kraken\Kraken_Ledgers_2023-2024.csv" });
                    }
                    break;
                case Exchange.Undefined:
                    // no data
                    break;
                default:
                    throw new AssertUnreachable(exchange);
            }

            // merge simpleDict with fullDict
            foreach (KeyValuePair<SchemaIdentifier, IEnumerable<DString>> kvp in simpleDict)
            {
                if (fullDict.ContainsKey(kvp.Key)) fullDict[kvp.Key] = fullDict[kvp.Key].Append(kvp.Value);
                else fullDict.Add(kvp.Key, new List<IEnumerable<DString>>() { kvp.Value });
            }
            return fullDict;
        }

        private Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> getBinanceFiles(FinancialYear year)
        {
            List<(DString fileName, SchemaIdentifier schemaId, BinanceAccount accountType, FinancialYear fy)> allFiles = new List<(DString, SchemaIdentifier, BinanceAccount, FinancialYear)>()
            {
                ("Binance_Classic_Deposits_2017_4.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Deposits_2018_1.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Deposits_2018_3.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Deposits_2018_4.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Deposits_2019_1.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Deposits_2019_2.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Deposits_2019_3.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2019),
                ("Binance_Classic_Deposits_2019_4.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Classic, FinancialYear.Y2019),

                ("Binance_Classic_Distributions.csv", SchemaIdentifier.Binance_Distributions, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Distributions.csv", SchemaIdentifier.Binance_Distributions, BinanceAccount.Classic, FinancialYear.Y2018),

                ("Binance_Classic_Trades_2017_4.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Trades_2018_1.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Trades_2018_2.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Trades_2018_3.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Trades_2018_4.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Trades_2019_1-2019_2.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Trades_2019_3-2019_4.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Classic, FinancialYear.Y2019),

                ("Binance_Classic_WalletTransactions_2017_4.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_WalletTransactions_2018_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_WalletTransactions_2018_2.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_WalletTransactions_2018_3.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_WalletTransactions_2018_4.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_WalletTransactions_2019_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_WalletTransactions_2019_2.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Classic, FinancialYear.Y2018),

                ("Binance_Classic_Withdrawals_2017_4.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Withdrawals_2018_1.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2017),
                ("Binance_Classic_Withdrawals_2019_2.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2018),
                ("Binance_Classic_Withdrawals_2019_3.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2019),
                ("Binance_Classic_Withdrawals_2019_4.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2019),
                ("Binance_Classic_Withdrawals_2020_1.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Classic, FinancialYear.Y2019),

                ("Binance_Master_Deposits_2020_1.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Master, FinancialYear.Y2019),

                ("Binance_Master_FiatDeposits_2020_1.csv", SchemaIdentifier.Binance_FiatDeposits, BinanceAccount.Master, FinancialYear.Y2019),
                ("Binance_Master_FiatDeposits_2021_1.csv", SchemaIdentifier.Binance_FiatDeposits, BinanceAccount.Master, FinancialYear.Y2020),
                ("2022-2023/Binance_Master_FiatDeposits_2022-2023.csv", SchemaIdentifier.Binance_FiatDeposits, BinanceAccount.Master, FinancialYear.Y2022),

                ("Binance_Master_Options_2021_1.csv", SchemaIdentifier.Binance_Options, BinanceAccount.Master, FinancialYear.Y2020),

                ("Binance_Master_Trades_2019_3-2020_2.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Master, FinancialYear.Y2019),
                ("Binance_Master_Trades_2020_3-2021_2.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Master, FinancialYear.Y2020),
                ("2022-2023/Binance_Master_Trades_2022-2023.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Master, FinancialYear.Y2022),

                ("Binance_Master_BuyCrypto_2020_1.csv", SchemaIdentifier.Binance_BuyCrypto, BinanceAccount.Master, FinancialYear.Y2019),

                ("2022-2023/Binance_Master_Convert_2022-2023.csv", SchemaIdentifier.Binance_Convert, BinanceAccount.Master, FinancialYear.Y2022),

                ("Binance_Master_WalletTransactions_2020_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Master, FinancialYear.Y2019),
                ("Binance_Master_WalletTransactions_2020_2.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Master, FinancialYear.Y2019),
                ("Binance_Master_WalletTransactions_2021_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Master, FinancialYear.Y2020),
                ("Binance_Master_WalletTransactions_2022_1.csv", SchemaIdentifier.Binance_WalletTransactions_V2, BinanceAccount.Master, FinancialYear.Y2021),
                ("Binance_Master_WalletTransactions_2023_1.csv", SchemaIdentifier.Binance_WalletTransactions_V2, BinanceAccount.Master, FinancialYear.Y2021),

                ("Binance_Master_Withdrawals_2020_1.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Master, FinancialYear.Y2019),
                ("Binance_Master_Withdrawals_2020_2.csv", SchemaIdentifier.Binance_Withdrawals, BinanceAccount.Master, FinancialYear.Y2019),
                ("2022-2023/Binance_Master_Withdrawals_2022-2023.csv", SchemaIdentifier.Binance_Withdrawals_V2, BinanceAccount.Master, FinancialYear.Y2022),

                ("Binance_Sub1_Deposits_2020_1.csv", SchemaIdentifier.Binance_Deposits, BinanceAccount.Sub1, FinancialYear.Y2019),

                ("Binance_Sub1_FuturesTrades_2020_1-2020_2.csv", SchemaIdentifier.Binance_FuturesTrades, BinanceAccount.Sub1, FinancialYear.Y2019),

                ("Binance_Sub1_Trades_2020_1-2020_2.csv", SchemaIdentifier.Binance_SpotTrades, BinanceAccount.Sub1, FinancialYear.Y2019),

                ("Binance_Sub1_WalletTransactions_2020_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Sub1, FinancialYear.Y2019),
                ("Binance_Sub1_WalletTransactions_2020_2.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Sub1, FinancialYear.Y2019),

                ("Binance_Sub2_FuturesTrades_2020_1-2020_2.csv", SchemaIdentifier.Binance_FuturesTrades, BinanceAccount.Sub2, FinancialYear.Y2019),

                ("Binance_Sub2_WalletTransactions_2020_1.csv", SchemaIdentifier.Binance_WalletTransactions, BinanceAccount.Sub2, FinancialYear.Y2019)
            };

            // this looks more complicated than it is...
            // for every schema identifier, we want to get a list of files. However, we want each account associated
            // with that list of files to be a separate collection, hence the nesting.
            Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> result = new Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>>();

            foreach (IGrouping<BinanceAccount, (DString fileName, SchemaIdentifier schemaId, BinanceAccount accountType, FinancialYear fy)> group in allFiles.GroupBy(f => f.accountType))
            {
                Dictionary<SchemaIdentifier, IEnumerable<DString>> groupResult = new Dictionary<SchemaIdentifier, IEnumerable<DString>>();

                foreach ((DString fileName, SchemaIdentifier schemaId, BinanceAccount accountType, FinancialYear fy) in group)
                {
                    if (fy != year) continue;

                    DString filePath = this._Folder + "Binance\\" + fileName;
                    if (groupResult.ContainsKey(schemaId)) groupResult[schemaId] = groupResult[schemaId].Append(filePath);
                    else groupResult.Add(schemaId, new List<DString>() { filePath });
                }

                // add to result
                foreach (KeyValuePair<SchemaIdentifier, IEnumerable<DString>> kvp in groupResult)
                {
                    if (result.ContainsKey(kvp.Key)) result[kvp.Key] = result[kvp.Key].Append(kvp.Value);
                    else result.Add(kvp.Key, new List<IEnumerable<DString>>() { kvp.Value });
                }
            }

            return result;
        }
    }
}
