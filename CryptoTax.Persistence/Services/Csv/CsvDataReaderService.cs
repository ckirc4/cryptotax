﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Persistence.Services.Csv
{
    /// <summary>
    /// Reads and parses CSV data.
    /// </summary>
    public class CsvDataReaderService : ICsvDataReaderService
    {
        private readonly IEnumerable<Record> _ParsedRecords;
        private readonly IEnumerable<RecordGroup> _RecordGroups;
        private readonly IFileService _FileService;
        private readonly ICsvDataReaderServiceFactory _Factory;
        private readonly FinancialYear _FinancialYear;
        private readonly IEnumerable<TradeDTO> _Trades;
        private readonly IEnumerable<TransactionDTO> _Transactions;
        private IEnumerable<ICsvDataReaderService> _AdditionalSources;

        public SchemaIdentifier SchemaIdentifier { get; }
        /// <summary>
        /// Files in different group ids will be processed independently.
        /// </summary>
        public int GroupId { get; }

        public CsvDataReaderService(IFileService fileService, ICsvDataReaderServiceFactory factory, CsvInfo csv, Schema.Schema schema, FinancialYear financialYear, int groupId)
        {
            this.SchemaIdentifier = schema.Identfier;
            this.GroupId = groupId;

            this._AdditionalSources = new List<ICsvDataReaderService>();
            this._FileService = fileService;
            this._Factory = factory;
            this._FinancialYear = financialYear;
            this._ParsedRecords = CsvHelpers.ParseCsv(this._FileService, csv, schema.Columns).Select(row => schema.ProcessOne(row));
            this._RecordGroups = schema.ProcessAll(this._ParsedRecords) ?? new List<RecordGroup>();

            this._Trades = this._ParsedRecords
                .Select(row => row.ToTradeDTO())
                .Concat(this._RecordGroups
                    .Select(group => group.ToTradeDTO()))
                .Where(x => x != null && x.Time >= financialYear.Start && x.Time <= financialYear.End);
            this._Transactions = this._ParsedRecords
                .Select(row => row.ToTransactionDTO())
                .Concat(this._RecordGroups
                    .Select(group => group.ToTransactionDTO()))
                .Where(x => x != null && x.Time >= financialYear.Start && x.Time <= financialYear.End);
        }

        public void WithAdditionalSource(CsvInfo csv, Schema.Schema schema, int groupId)
        {
            ICsvDataReaderService additionalReader = this._Factory.Create(this._FileService, csv, schema, this._FinancialYear, groupId);
            this._AdditionalSources = this._AdditionalSources.Append(additionalReader);
        }

        public IOrderedEnumerable<TradeDTO> GetTrades()
        {
            if (this._AdditionalSources.Any())
            {
                // this will only be run for the primary source
                return GetObjectsWithoutOverlap(
                    this._AdditionalSources.Prepend(this),
                    // avoid infinite recursion
                    reader => reader == this ? this._Trades : reader.GetTrades(),
                    trade => trade.Time,
                    (trade1, trade2) => trade1.Equals(trade2));
            }
            else
            {
                return this._Trades.OrderBy(t => t.Time);
            }
        }

        public IOrderedEnumerable<TransactionDTO> GetTransactions()
        {
            if (this._AdditionalSources.Any())
            {
                return GetObjectsWithoutOverlap(
                    this._AdditionalSources.Prepend(this),
                    reader => reader == this ? this._Transactions : reader.GetTransactions(),
                    tx => tx.Time,
                    (tx1, tx2) => tx1.Equals(tx2));
            }
            else
            {
                return this._Transactions.OrderBy(t => t.Time);
            }
        }

        /// <summary>
        /// Ensures that objects of <see cref="CsvDataReaderService"/> with the same <see cref="Schema.SchemaIdentifier"/> are unique and, if they do overlap in time, are exactly duplicated within the overlap period as determined by the <paramref name="objectComparer"/>. Results are aggregated for all readers and sorted according to the <paramref name="timeGetter"/>.
        /// </summary>
        public static IOrderedEnumerable<T> GetObjectsWithoutOverlap<T>(IEnumerable<ICsvDataReaderService> readers, Func<ICsvDataReaderService, IEnumerable<T>> objectGetter, Func<T, DateTime> timeGetter, Func<T, T, bool> objectComparer)
        {
            List<T> allObjects = new List<T>();

            foreach (IEnumerable<ICsvDataReaderService> schemaGroup in readers.GroupBy(r => r.SchemaIdentifier))
            {
                foreach (IEnumerable<ICsvDataReaderService> group in schemaGroup.GroupBy(s => s.GroupId))
                {
                    List<T> objectsInGroup = new List<T>();
                    DateTime maxTime = DateTime.MinValue;

                    // subgroups are ordered such that we get sequential, possibly overlapping objects
                    List<List<T>> subGroups = group.Select(r => objectGetter(r))
                        .Where(sg => sg.Any())
                        .Select(sg => sg.OrderBy(obj => timeGetter(obj)).ToList())
                        .OrderBy(sg => timeGetter(sg.First())).ToList(); // using First() instead of Min(), since we have already done ordering
                    foreach (List<T> objectsInSubgroup in subGroups)
                    {
                        DateTime thisMin = timeGetter(objectsInSubgroup.First());
                        DateTime thisMax = timeGetter(objectsInSubgroup.Last());

                        // instead of just blindly adding what appears to be duplicate data, let's be thorough and verify that it really is.
                        // use arrays for efficiency
                        T[] duplicateData = objectsInSubgroup.Where(obj => timeGetter(obj) <= maxTime).ToArray();
                        T[] duplicatingData = objectsInGroup.Where(obj => timeGetter(obj) >= thisMin && timeGetter(obj) <= thisMax).ToArray();

                        if (duplicateData.Length != duplicatingData.Length) throw new Exception("Overlapping data must have the same number of objects in both data sources.");

                        // this may look like overkill, but the original implementation did at least O(N^2) object comparisons which was slowing the application to a standstill.
                        bool[] alreadyDuped = new bool[duplicateData.Count()];
                        for (int i = 0; i < duplicateData.Count(); i++)
                        {
                            T dupe = duplicateData[i];
                            bool foundDupe = false;
                            for (int j = 0; j < duplicatingData.Count(); j++)
                            {
                                if (!alreadyDuped[j])
                                {
                                    // since data is already sorted, there should never be more than a couple comparisons per j
                                    T maybeMatch = duplicatingData[j];
                                    if (objectComparer(dupe, maybeMatch))
                                    {
                                        alreadyDuped[j] = true;
                                        foundDupe = true;
                                        break;
                                    }
                                }
                            }

                            if (!foundDupe) throw new Exception("Each data item in an overlapping region must duplicate exactly one other item of the other data source.");
                        }

                        // at this point we have verified the data and can continue
                        objectsInGroup.AddRange(objectsInSubgroup.Where(obj => timeGetter(obj) > maxTime));

                        if (thisMax > maxTime)
                        {
                            maxTime = thisMax;
                        }
                    }

                    allObjects.AddRange(objectsInGroup);
                }
            }

            return allObjects.OrderBy(obj => timeGetter(obj));
        }
    }

    public class CsvDataReaderServiceFactory : ICsvDataReaderServiceFactory
    {
        public ICsvDataReaderService Create(IFileService fileService, CsvInfo csv, Schema.Schema schema, FinancialYear financialYear, int groupId)
        {
            return new CsvDataReaderService(fileService, this, csv, schema, financialYear, groupId);
        }
    }
}
