﻿using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services.Csv
{
    public struct CsvInfo
    {
        public DString FilePath { get; set; }
        public char Separator { get; set; }
        public bool ContainsHeader { get; set; }
        public bool TrimOuterQuotes { get; set; }

        public CsvInfo(DString filePath, char separator = ',', bool containsHeader = true, bool trimOuterQuotes = true)
        {
            this.FilePath = filePath;
            this.Separator = separator;
            this.ContainsHeader = containsHeader;
            this.TrimOuterQuotes = trimOuterQuotes;
        }
    }
}
