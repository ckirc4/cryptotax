﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using System.Linq;

namespace CryptoTax.Persistence.Services.Csv
{
    /// <summary>
    /// Reads and parses CSV data.
    /// </summary>
    public interface ICsvDataReaderService
    {
        SchemaIdentifier SchemaIdentifier { get; }
        int GroupId { get; }

        IOrderedEnumerable<TradeDTO> GetTrades();
        IOrderedEnumerable<TransactionDTO> GetTransactions();
        void WithAdditionalSource(CsvInfo csv, Schema.Schema schema, int groupId);
    }

    public interface ICsvDataReaderServiceFactory
    {
        ICsvDataReaderService Create(IFileService fileService, CsvInfo csv, Schema.Schema schema, FinancialYear financialYear, int groupId);
    }
}
