﻿using CryptoTax.Persistence.Schema;
using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Persistence.Services.Csv
{
    public static class CsvHelpers
    {
        /// <summary>
        /// Helper method to easily generated variations of date formats. The template contains placeholder fields denoted by {0}, {1}, etc.
        /// Each placeholder is connected to the corresponding ordered param of type string[] which specifies the variations that may be accepted in place of the placeholder.
        /// The total number of formats generated is given by the product of the number of variations of each param.
        /// Example: GenerateSimilarDateFormats("{0}/{1}/yyyy", { "d", "dd" }, { "M", "MM" }) generates a string array { "d/M/yyyy", "dd/M/yyyy", "d/MM/yyyy", "dd/MM/yyyy" } (unordered)
        /// </summary>
        public static string[] GenerateSimilarDateFormats(string formatTemplate, params string[][] allowedVariations)
        {
            List<string> formats = new List<string>() { formatTemplate };

            // this is an interesting algorithm. moving through each variation array,
            // make copies of the existing formats list and replace the placeholders
            // such that every copy will have a different value of the placeholder.
            // eventually, all placeholders will be filled in with all combinations of
            // variations.
            for (int i = 0; i < allowedVariations.Length; i++)
            {
                string[] variations = allowedVariations[i];
                List<string> existingFormats = new List<string>(formats);
                formats.Clear();

                for (int j = 0; j < variations.Length; j++)
                {
                    string variation = variations[j];
                    foreach (string format in existingFormats)
                    {
                        formats.Add(format.Replace($"{{{i}}}", variation));
                    }
                }
            }

            return formats.ToArray();
        }

        /// <summary>
        /// Parses the CSV file, whose ordered columns are identified by the provided array. All types are converted using SchemaColumn.ParseString() method.
        /// </summary>
        public static IEnumerable<object[]> ParseCsv(IFileService fileService, CsvInfo csv, Column[] columns)
        {
            // it is likely that this breaks for some CSV files. for some examples, see https://en.wikipedia.org/wiki/Comma-separated_values#Basic_rules
            List<object[]> result = new List<object[]>();

            int row = 0;
            foreach (DString line in fileService.ReadLines(csv.FilePath))
            {
                DString[] values = getColumnContents(columns.Length, line, csv.Separator, csv.TrimOuterQuotes);
                object[] parsedRow = new object[columns.Length];

                if (values.Length != columns.Count()) throw new Exception($"Unexpected number of columns. Expected {columns.Count()} but received {values.Length}");

                for (int col = 0; col < values.Length; col++) // for each column
                {
                    string value = values[col].Value.Trim();
                    Column column = columns[col];

                    if (row == 0 && csv.ContainsHeader)
                    {
                        // check that headers line up
                        if (value.Trim().ToLower() != column.ColumnName.Trim().ToLower()) throw new Exception($"Unexpected column name: Expected '{column.ColumnName.Trim().ToLower()}' but received '{value.Trim().ToLower()}'");
                        continue;

                    }
                    else
                    {
                        // if empty, check if allowed to be empty
                        if (string.IsNullOrWhiteSpace(value))
                        {
                            if (column.Mandatory == true) throw new Exception($"Column value for column '{column.ColumnName}' is mandatory, but was blank.");

                            parsedRow[col] = null;
                            continue;
                        }

                        // parse data, implemented by the particular schema column
                        parsedRow[col] = column.ParseString(value);
                    }
                }

                if (parsedRow.Any(v => v != null)) result.Add(parsedRow);
                row++;
            }

            return result;
        }

        /// <summary>
        /// Given the raw CSV line, infers the column contents, ensuring that entries within quotes are treated as one. e.g. "Hello, world" would be a single column if the separator is ','.
        /// </summary>
        private static DString[] getColumnContents(int expectedColumns, string line, char separator, bool trimOuterQuotes)
        {
            const char QUOTE = '"';
            if (separator == QUOTE) throw new Exception($"'{QUOTE}' is an unreasonable separator...");

            DString[] values = new DString[expectedColumns];
            int valuesCompleted = 0;
            string currentValue = "";
            bool quotesActive = false;

            // by intelligently managing our state, we only need to do a single pass. this is quite fast :)
            for (int i = 0; i < line.Length; i++)
            {
                char c = line[i];
                if (c == QUOTE) quotesActive = !quotesActive;
                if (c == separator && !quotesActive)
                {
                    // reached the end of the value
                    if (valuesCompleted >= expectedColumns) throw new Exception($"Cannot parse line... expected {expectedColumns} columns but found more.");
                    values[valuesCompleted] = currentValue;
                    currentValue = "";
                    valuesCompleted++;
                }
                else if (c != QUOTE || c == QUOTE && !trimOuterQuotes)
                {
                    // character is acceptable to add
                    currentValue += c;
                }
            }

            // we are yet to add the last value
            values[expectedColumns - 1] = currentValue;
            valuesCompleted++;

            if (valuesCompleted != expectedColumns) throw new Exception($"Error parsing line... expected {expectedColumns} columns but found {valuesCompleted}.");

            return values;
        }
    }
}
