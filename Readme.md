# Getting started
Run `CryptoTax.exe` and provide key-value pairs as arguments to adjust settings. The syntax of a key-value pair is `<key>=<value>` with no spaces or quotes. All arguments are optional - if a setting type is not specified, the default value will be used. See below for a list of possible keys and possible values. Note that all input is case-insensitive.

Default arguments are highlighted in bold.

| Name | Key | Type | Default |  Description | 
| :--: | :-: | :--: | :-: | ----- |
| Run name | `name` | `NString` | `null` | The name of the run. Currently unused. |
| Financial year | `financialYear` | [`FinancialYear`](#financialyear) | `2016-2017` | The financial year over which to calculate crypto tax. |
| Inventory type | `inventoryType` | [`InventoryType`](#inventorytype) | `FILO` | The type of inventory to use, which affects the handling of incoming and outgoing tax packets. For now, it is **undefined behaviour** to use a different inventory type than what was used in the `state` (see below). |
| Candlestick interval | `candlestickInterval` | [`CandlestickInterval`](#candlestickinterval) | `Day_1` | The candlestick interval to use for calculations involving historical prices. It affects the granularity and precision of price data. |
| Price calculation type | `priceCalculationType` | [`PriceCalculationType`](#pricecalculationtype) | `Open` | Determines how the price at an arbitrary time should be calculated, given that we only have candlestick price data available. |
| Inventory management type | `inventoryManagementType` | [`InventoryManagementType`](#inventorymanagementtype) | `Single` | Sets how inventories are managed in regards to exchange containers. For now, it is **undefined behaviour** to use a different inventory management type than what was used in the `state` (see below). |
| ~~Decimal places~~ | ~~`decimalPlaces`~~ | ~~`int?~`~~ | ~~`null`~~ | **Unused.** ~~The number of decimal places that `decimal` values should strictly be rounded to.~~ |
| ~~Rounding type~~ | ~~`roundingType`~~ | ~~[`RoundingType`](#roundingtype)~~ | ~~`None`~~ | **Unused.** ~~How numbers should be rounded.~~ |
| Enable reporting | `enableReporting` | `bool` | `true` | Whether a [report](#output) should be generated during this run. Recommended `false` for dry-runs (configuration discovery) to speed up the program. |
| Console logging template | `consoleLoggingTemplate` | [`LoggingTemplate`](#loggingtemplate)`?` | `ConsoleVerbose` | The template that should be used for logging to the console, if any. If redirecting the console stream, code formatting (such as colours) will not be shown. Recommended `null` for wet-runs to speed up the program. |
| Persistor logging template | `persistorLoggingTemplate` | [`LoggingTemplate`](#loggingtemplate)`?` | `null` | The template that should be used for [logging to the file system](#output), if any. Does not contain any formatting codes. Recommended `null` for wet-runs to speed up the program. |
| Input state | `state` | [`runId`](#runid) | `null` | Use the [output state](#output) of the given run as the current run's input state. Currently, the state consists of the inventories and their contents (packets). |

## Types
### **`FinancialYear`**
`string` type. Denotes the 1-year period starting on the 1st of July (UTC+10) on the starting year and ending 1 tick before the 1st of July (UTC+10) of the subsequent year. `FinancialYear`s are represented via strings as `"YYYY-YYYY"`.

### **`InventoryType`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `FILO` | First-in-first-out | Inventories use a `stack` model to manage incoming and outgoing tax packets. |

### **`CandlestickInterval`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `day_1` | 1 Day | Interval starts at midnight UTC and lasts for exactly 1 day. |
| `hour_1` | 1 Hour | Interval starts on the hour and lasts for exactly 1 hour. |
| `minute_1` | 1 Minute | Interval starts on the minute and lasts for exactly 1 minute. |

### **`PriceCalculationType`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `open` | Open | Use the open price of a candlestick for the duration of that candlestick. |
| `close` | Close | Use the close price of a candlestick for the duration of that candlestick. |
| `arithmeticMean` | Arithmetic mean | Use the arithmetic mean (average) of the open and close price of a candlestick for the duration of that candlestick. Calculated as `(open + close) / 2`. |
| `timeWeightedAverage` | Time-weighted average | Linearly interpolate in time between the open and close price of a candlestick to determine a continuous price gradient that can be used when polling price data at any time. Calculated as `timeFrac * (close - open) + open`. |

### **`InventoryManagementType`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `single` | Single | Use only a single inventory application-wide. Transfers will have the same source and destination, but the transfer duration and fee are still respected. | 
| `onePerExchange` | One per exchange | Use one inventory per exchange. Binance Spot, Binance Futures and Binance Options are treated as a single exchange. Sub-accounts all belong to the same inventory as their parent exchange. Note that all inventories will have the same type, see [`InventoryType`](#inventorytype). | 

### **`RoundingType`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `none` | None | No rounding. |
| `nearest` | Nearest | Round up or down, whichever is closer. |
| `up` | Up | Always round up. |
| `down` | Down | Always round down. |
| `minimiseGains` | Maximise gains | Attempts to automatically decide the rounding direction on a case-by-case basis such that gains are maximised. |
| `maximiseGains` | Minimise gains | Attempts to automatically decide the rounding direction on a case-by-case basis such that gains are minimised. |

### **`LoggingTemplate`**
`enum` type with the following values:
| Value | Name | Description |
| :--: | :-: | ----- |
| `consoleVerbose` | Console verbose | Shows all logging messages in the console. May include formatting such as colours. |
| `consoleProduction` | Console production | Shows only important logging messages in the console. May include formatting such as colours. |
| `persistorVerbose` | Persistor verbose | Writes all logging messages to the [log file](#output). May include formatting such as colours. |

### **`RunId`**
`string` type. It is the ID of the run that is being referenced. For example, `#n15` refers to the run with numeric ID 15. The numeric run ID can be obtained [using the exit code](#obtaining-runids) of the program. The name of the output data folder is also the run ID.

## Special Key-Value Pairs
A number of special pairs can be used as configuration shortcuts that refernce a previous run.

- `ref=<runId>`: Use settings from a previous run as the base, **excluding** the previous run's output state as the new input state (if required, this has to be set explicitly using `state=<runId>`). Regular key-value pairs can then be provided to override individual settings from this base configuration.
- `cont=<runId>`: Continue on from the given run. Useful to easily run calculations for a range of years. Short for `ref=<runId> financialYear=<run.financialYear++> state=<runId>`. **No other args are accepted.**

## Obtaining `runId`s
Upon exiting, the application will return an exit code that indicates the `runId` that has just ran. A negative value is returned if the program did not run to completion - refer to the log file for more info (as long as a `PersistorLoggingTemplate` is provided). Zero is returned if a problem was encountered very early during startup. A positive value is returned for successful runs. The absolute value of the exit code denotes the numeric component of the `runId` - the full id string can then be constructed using **`#n<exitCode>`**.

## Output
Once the [`runId` has been obtained](#obtaining-runids), the `/Output/RunId/` folder can be accessed. It contains the following items:
- **`Log_N.log`**: *([`persistorLoggingTemplate`](#getting-started) must not be `null`.)* A collection of sequential log files. Logs are formatted according to the `PersistorLoggingTemplate` that is used. A new log file is generated once a filesize limit is reached.
- **`Report_N.csv`**: *([`enableReporting`](#getting-started) must be `true`.)* A collection of sequential report files. Reports contain detailed step-by-step information about actions derived from exchange data, price information and calculations, and state management, that would allow a third party to exactly recreate the program's result. A new report file is generated once a filesize limit is reached. The CSV columns are `id`, `time`, `category`, `types`, `message`, `standardCapitalGains`, `discountableCapitalGains`, `capitalLosses`, `assessableIncome`. All values are enclosed by double-quotes (`"`).
- **`RunInfo.json`**: A serialised `RunInfoDTO` object that represents the configuration that was used during the run.
- **`RunState.json`**: A serialised `RunStateDTO` object that represents the output state of the run. At the moment, output state consists of inventories and their contents.
- **`Results.json`**: A serialised `CalculationResultsDTO` object that summarises the final gains/losses during the financial year over which the run occurred.

<br/>
<br/>

# Unit Tests
Run unit tests via the Visual Studio integrated framework. To exclude integration tests, long-running tests, or not-yet-implemented tests, use the filter `-"Trait:LiveAPI" -"Trait:NYI" -"Trait:LONG"`.

# Tax Rules

From https://blog.coinjar.com/crypto-tax-everything-you-need-to-know

## Forks
Received forked token has a cost base of $0, and no CGT event is triggered until it is sold.


## Airdrops and staking
Received tokens have a cost base of their value at the time of the airdrop. **Treated as assessable income at the time of the airdrop [but NOT a CGT event]**, and subject to the CGT tax upon selling.


## Loan Interest
Received tokens have a cost basis of $0, and no CGT event is triggered until it is sold.


## Wrapping
Wrapping / unwrapping ETH is considered a taxable event. See https://community.ato.gov.au/t5/Cryptocurrency/Does-quot-wrapping-quot-a-cryptocurrency-trigger-a-capital-gains/td-p/107162


# Preparing a Tax Report
Ensure you collect all trade logs and reference them in the `ExchangeDataLocationService`. Run the `TransactionFetcher` project to fetch logs from supported sources such as exchanges or on-chain markets. Furthermore, ensure any dangling on-chain transactions (those that cannot be matched with another transaction to construct a complete `Transfer` object) are whitelisted in the `TransactionWhitelistService`. Dangling transactions can arise when sending on-chain transactions to exchanges, where the exchange does not expose the incoming transaction. We require manual whitelisting of these transactions as an extra layer of rigidity, and you may need to make changes to the `TransferService` matching algorithm to allow a `Transfer` object to be inferred. Generally, the easiest way to find any required manual interventions is to do a dry-run by setting `Constats.DRY_RUN = true`, then checking the warnings printed to the console and any potential inventory overdrawing.

Run the `CryptoTax.Presentation` project with the command line argument `cont=<previous financial year's run id>` to generate a new tax report. To specify arguments, right click on the project and navigate to the Debug tab. 

How to interpret results from the `Results.json` file:
- Standard capital gains: Taxable capital gains which are ineligible for the 1 year discount.
- Discountable capital gains: Taxable capital gains which are ineligible for the 1 year discount.
- Capital losses: Total losses of the current year.
- Net capital gains: The total capital gains on which to pay tax, derived from the standard capital gains, discountable capital gains, and capital losses.
- Assessable income: Non-capital gain income that is treated as additional assessable income.

Note: The ATO requires reporting of the following:
- "Total current year capital gains": Standard capital gains + discountable capital gains
- "Net capital gain": Net capital gains
  - If declaring a net gain of more than $10,000, a more detailed section unlocks.
    - In the first section under "Other Assets", enter the total current year capital gains amount
    - Enter the capital losses in the section below
    - Enter the discountable capital gains in the section below that
- Assessable income must be declared elsewhere