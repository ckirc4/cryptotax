﻿using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    public class MockCurrencyConversionService : ICurrencyConversionService
    {
        private readonly Dictionary<DString, decimal> _Values;

        public MockCurrencyConversionService() : this(new Dictionary<DString, decimal>())
        { }

        /// <summary>
        /// Set the value of the given symbols.
        /// </summary>
        public MockCurrencyConversionService(Dictionary<DString, decimal> values)
        {
            this._Values = values;
            if (!this._Values.ContainsKey("AUD")) this._Values.Add("AUD", 1);
        }

        /// <summary>
        /// Will always use the values injected via the constructor. If only one is available, will use the price that is provided to this method to calculate values. Otherwise, cannot work out the value and will throw exception.
        /// </summary>
        public Task<(decimal quoteCurrencyValue, decimal baseCurrencyValue)> GetAudUnitValueAsync(ExchangeSymbol symbol, decimal price, DateTime time)
        {
            DString baseCurrency = symbol.Symbol.BaseCurrency.Symbol;
            DString quoteCurrency = symbol.Symbol.QuoteCurrency.Symbol;

            if (!this._Values.ContainsKey(baseCurrency) && !this._Values.ContainsKey(quoteCurrency)) throw new Exception($"Value for currencies {baseCurrency} and {quoteCurrency} was not specified in the constructor.");

            decimal baseValue = 0, quoteValue = 0;
            if (this._Values.ContainsKey(baseCurrency) && this._Values.ContainsKey(quoteCurrency))
            {
                baseValue = this._Values[baseCurrency];
                quoteValue = this._Values[quoteCurrency];
            }
            else if (this._Values.ContainsKey(baseCurrency))
            {
                baseValue = this._Values[baseCurrency];
                quoteValue = baseValue / price;
            }
            else if (this._Values.ContainsKey(quoteCurrency))
            {
                quoteValue = this._Values[quoteCurrency];
                baseValue = quoteValue * price;
            }

            return Task.FromResult((quoteValue, baseValue));
        }

        public IEnumerable<ExchangeSymbol> GetChainToAud(ExchangeSymbol symbol)
        {
            // hack to test TradeProcessingServiceTests.SetupWorksCorrectly (lol)
            return new CurrencyConversionService(null, null, null, null, null, null, new Mock<IRunService>().Object, null).GetChainToAud(symbol);
        }

        /// <summary>
        /// Currency value must have been specified in constructor, otherwise will throw exception.
        /// </summary>
        public Task<decimal> GetSingleAudUnitValueAsync(DString currency, DateTime time, Exchange? exchangeFallback)
        {
            if (!this._Values.ContainsKey(currency)) throw new Exception($"Value for currency {currency} was not specified in the constructor.");
            else return Task.FromResult(this._Values[currency]);
        }

        public ExchangeSymbol? TryGetExchangeSymbolFromCurrency(DString currency, Exchange? fallback)
        {
            return new CurrencyConversionService(null, null, null, null, null, null, new Mock<IRunService>().Object, null).TryGetExchangeSymbolFromCurrency(currency, fallback);
        }
    }
}
