﻿using CryptoTax.Domain;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using System;

namespace CryptoTax.Tests.Mocks
{
    public class MockInventoryManagerService : IInventoryManagerService
    {
        private readonly Func<Container, IInventory> _Function;

        /// <summary>
        /// Always returns the same inventory.
        /// </summary>
        public MockInventoryManagerService(IInventory inventory) : this(_ => inventory)
        { }

        /// <summary>
        /// Uses the provided function to choose which inventory to return given a Container.
        /// </summary>
        public MockInventoryManagerService(Func<Container, IInventory> getInventoryImplementation)
        {
            this._Function = getInventoryImplementation;
        }

        public CapitalGains GetAggregateCapitalGains()
        {
            throw new NotImplementedException();
        }

        public IInventory GetInventory(Container container)
        {
            return this._Function(container);
        }

        public void OnProgress(int progressCount, int N) { }

        public void UpdateRunState()
        {
            throw new NotImplementedException();
        }
    }
}
