﻿using CryptoTax.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Tracks the method calls and parameters.
    /// </summary>
    public class MockTrackingTimeEventService : ITimeEventService
    {
        public IEnumerable<(Action action, DateTime executionTime, bool executeAfterLastItem)> AddActionCalls { get; private set; }
        public IEnumerable<(DateTime? previousTime, DateTime time)> OnItemWillProcessCalls { get; private set; }
        public IEnumerable<DateTime> OnLastItemDidProcessCalls { get; private set; }
        public IEnumerable<long> TryRemoveActionCalls { get; private set; }

        public MockTrackingTimeEventService()
        {
            this.AddActionCalls = new List<(Action, DateTime, bool)>();
            this.OnItemWillProcessCalls = new List<(DateTime?, DateTime)>();
            this.OnLastItemDidProcessCalls = new List<DateTime>();
            this.TryRemoveActionCalls = new List<long>();
        }

        public long AddAction(Action action, DateTime executionTime, bool executeAfterLastItem)
        {
            this.AddActionCalls = this.AddActionCalls.Append((action, executionTime, executeAfterLastItem));
            return 0;
        }

        public void OnItemWillProcess(DateTime? previousTime, DateTime time)
        {
            this.OnItemWillProcessCalls = this.OnItemWillProcessCalls.Append((previousTime, time));
        }

        public void OnLastItemDidProcess(DateTime time)
        {
            this.OnLastItemDidProcessCalls = this.OnLastItemDidProcessCalls.Append(time);
        }

        public bool TryRemoveAction(long id)
        {
            this.TryRemoveActionCalls = this.TryRemoveActionCalls.Append(id);
            return true;
        }
    }
}
