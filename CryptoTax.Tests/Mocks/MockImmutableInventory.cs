﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Represents an inventory whose contents are set at initialisation.
    /// None of the modification functions will change the inventory.
    /// </summary>
    public class MockImmutableInventory : IInventory
    {
        private readonly IdProviderService _IdProviderService;
        private readonly Dictionary<DString, IEnumerable<CurrencyAmount>> _Inventory;

        public Exchange Exchange { get; }
        public DString Name { get; }
        public DString Id { get; }
        public InventoryType Type { get; } = InventoryType.MockImmutable;

        public CapitalGains NetCapitalGains { get; }

        public MockImmutableInventory(IdProviderService idProvider, Exchange exchange, DString name, Dictionary<DString, IEnumerable<CurrencyAmount>> inventory, CapitalGains gains = null)
        {
            this._IdProviderService = idProvider;
            this.Exchange = exchange;
            this.Name = name;
            this.Id = idProvider.GetNext(IdType.Inventory);
            this.NetCapitalGains = gains;
            this._Inventory = inventory;
        }

        public MockImmutableInventory(IdProviderService idProvider, InventoryDTO dto)
        {
            this._IdProviderService = idProvider;
            this.Name = dto.Name;
            this.Type = dto.Type;
            this.Id = dto.Id;
            this.Exchange = dto.Exchange;
            this.NetCapitalGains = new CapitalGains(dto.CapitalGains);
            this._Inventory = new Dictionary<DString, IEnumerable<CurrencyAmount>>();
            foreach (KeyValuePair<string, IEnumerable<InventoryItemDTO>> kvp in dto.Items)
            {
                this._Inventory.Add(kvp.Key, kvp.Value.Select(item => new CurrencyAmount(this._IdProviderService, item)));
            }
        }

        public CapitalGainsDelta Disposal(CurrencyAmount disposedCurrency, decimal additionalCostBasis, out decimal overdrawnAmount)
        {
            overdrawnAmount = 0;
            return CapitalGainsDelta.ZERO;
        }

        public Dictionary<DString, IEnumerable<CurrencyAmount>> GetCurrentInventory()
        {
            return this._Inventory;
        }

        public void Purchase(CurrencyAmount purchasedCurrency)
        {
            // do nothing
        }

        public InventoryDTO ToDTO()
        {
            Dictionary<string, IEnumerable<InventoryItemDTO>> dict = new Dictionary<string, IEnumerable<InventoryItemDTO>>();
            foreach (KeyValuePair<DString, IEnumerable<CurrencyAmount>> kvp in this._Inventory)
            {
                dict.Add(kvp.Key, kvp.Value.Select(v => v.ToDTO()));
            }
            return new InventoryDTO(
                this.Name,
                this.Type,
                this.Id,
                this.Exchange,
                dict,
                this.NetCapitalGains.ToDTO());
        }

        public void TransferIn(CurrencyAmount transferredCurrency, DateTime time)
        {
            // do nothing
        }

        public IEnumerable<CurrencyAmount> TransferOut(CurrencyAmount currencyAmount, TransferOutReason reason, out decimal overdrawnAmount)
        {
            overdrawnAmount = 0;
            return new List<CurrencyAmount>();
        }
    }

    public class MockImmutableInventoryFactory : IInventoryFactory
    {
        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new MockImmutableInventory(idProvider, exchange, name, new Dictionary<DString, IEnumerable<CurrencyAmount>>(), null);
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            return new MockImmutableInventory(idProvider, dto);
        }
    }
}
