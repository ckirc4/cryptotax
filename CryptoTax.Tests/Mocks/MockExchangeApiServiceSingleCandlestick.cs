﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Supports single-candlestick retrieval only.
    /// </summary>
    public class MockExchangeApiServiceSingleCandlestick : IExchangeApiService
    {
        private readonly Dictionary<(DString, DateTime), Candlestick> _ReturnValues;

        /// <summary>
        /// The key is $"{baseSymbol}{quoteSymbol}", e.g. BTCUSDT for a certain time
        /// </summary>
        public MockExchangeApiServiceSingleCandlestick(Dictionary<(DString, DateTime), Candlestick> returnValues)
        {
            this._ReturnValues = returnValues;
        }

        /// <summary>
        /// Retrieves the candlestick whose start time equals the provided fromUTC time.
        /// </summary>
        public Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, DateTime toUTC, bool getOnly)
        {
            Candlestick candlestick = this._ReturnValues[(this.getKey(symbol), fromUTC)];
            IEnumerable<Candlestick> collection = new List<Candlestick>() { candlestick };
            return Task.FromResult(collection);
        }

        /// <summary>
        /// Retrieves the candlestick whose start time equals the provided fromUTC time.
        /// </summary>
        public Task<IEnumerable<Candlestick>> GetCandlesticksAsync(CandlestickInterval interval, ExchangeSymbol symbol, DateTime fromUTC, int count, bool getOnly)
        {
            Candlestick candlestick = this._ReturnValues[(this.getKey(symbol), fromUTC)];
            IEnumerable<Candlestick> collection = new List<Candlestick>() { candlestick };
            return Task.FromResult(collection);
        }

        /// <summary>
        /// Not implemented.
        /// </summary>
        public Task<IEnumerable<ExchangeSymbol>> GetSymbolsAsync()
        {
            // not used for anything
            throw new NotImplementedException();
        }

        private DString getKey(ExchangeSymbol symbol)
        {
            return $"{symbol.Symbol.BaseCurrency.Symbol}{symbol.Symbol.QuoteCurrency.Symbol}";
        }
    }
}
