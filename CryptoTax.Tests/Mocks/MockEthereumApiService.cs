﻿using CryptoTax.Shared;
using CryptoTax.TransactionFetcher.Ethereum;
using System.Collections.Generic;
using System.Threading.Tasks;
using NormalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainNormalTransaction;
using TokenTx = CryptoTax.TransactionFetcher.Ethereum.OnChainTokenTransferTransaction;
using InternalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainInternalTransaction;

namespace CryptoTax.Tests.Mocks
{
    internal class MockEthereumApiService : IEthereumApiService
    {
        private readonly IEnumerable<NormalTx> _NormalTx;
        private readonly IEnumerable<TokenTx> _TokenTx;
        private readonly IEnumerable<InternalTx> _InternalTx;

        public MockEthereumApiService(IEnumerable<NormalTx> normalTx, IEnumerable<TokenTx> tokenTx, IEnumerable<InternalTx> internalTx)
        {
            this._NormalTx = normalTx;
            this._TokenTx = tokenTx;
            this._InternalTx = internalTx;
        }

        public Task<IEnumerable<NormalTx>> GetNormalTransactions(Wallet wallet, FinancialYear financialYear)
        {
            return Task.FromResult(this._NormalTx);
        }

        public Task<IEnumerable<TokenTx>> GetTokenTransferTransactions(Wallet wallet, FinancialYear financialYear)
        {
            return Task.FromResult(this._TokenTx);
        }

        public Task<IEnumerable<InternalTx>> GetInternalTransactions(Wallet wallet, FinancialYear financialYear)
        {
            return Task.FromResult(this._InternalTx);
        }
    }
}
