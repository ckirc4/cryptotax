﻿using System.Collections.Generic;
using System.Linq;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// This reporting service does not do anything other than track which data was added.
    /// </summary>
    public class MockTrackingReportingService : IReportingService
    {
        /// <summary>
        /// The ordered collection of data, where added data is appended to the end of the sequence.
        /// </summary>
        public IEnumerable<ReportingBaseData> AddedData;

        public MockTrackingReportingService()
        {
            this.AddedData = new List<ReportingBaseData>();
        }

        public void AddData(ReportingBaseData data, bool flushImmediately = false)
        {
            this.AddedData = this.AddedData.Append(data);
        }

        public void ClearData()
        {
            this.AddedData = new List<ReportingBaseData>();
        }

        public void Flush() { }
    }
}
