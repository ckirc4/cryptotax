﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// No state management, only tracks method calls and stores the provided arguments. Return values are hardcoded defaults and should not be used for anything.
    /// </summary>
    internal class MockTrackingInventory : IInventory
    {
        public IEnumerable<(CurrencyAmount disposedCurrency, decimal additionalCostBasis)> DisposalCalls { get; private set; }
        public IEnumerable<CurrencyAmount> PurchaseCalls { get; private set; }
        public IEnumerable<CurrencyAmount> TransferInCalls { get; private set; }
        public IEnumerable<(CurrencyAmount, TransferOutReason)> TransferOutCalls { get; private set; }

        public InventoryType Type { get; }
        public DString Name { get; }
        public DString Id { get; }
        public Exchange Exchange { get; }
        public CapitalGains NetCapitalGains { get; }

        public MockTrackingInventory()
        {
            this.DisposalCalls = new List<(CurrencyAmount, decimal)>();
            this.PurchaseCalls = new List<CurrencyAmount>();
            this.TransferInCalls = new List<CurrencyAmount>();
            this.TransferOutCalls = new List<(CurrencyAmount, TransferOutReason)>();

            this.Type = InventoryType.MockTracking;
            this.Name = "Tracking mock inventory";
            this.Id = "Mock Id";
            this.Exchange = Exchange.Undefined;
            this.NetCapitalGains = new CapitalGains();
        }

        public CapitalGainsDelta Disposal(CurrencyAmount disposedCurrency, decimal additionalCostBasis, out decimal overdrawnAmount)
        {
            this.DisposalCalls = this.DisposalCalls.Append((disposedCurrency, additionalCostBasis));
            overdrawnAmount = 0;
            return CapitalGainsDelta.ZERO;
        }

        public Dictionary<DString, IEnumerable<CurrencyAmount>> GetCurrentInventory()
        {
            throw new NotImplementedException();
        }

        public void Purchase(CurrencyAmount purchasedCurrency)
        {
            this.PurchaseCalls = this.PurchaseCalls.Append(purchasedCurrency);
        }

        public InventoryDTO ToDTO()
        {
            throw new NotImplementedException();
        }

        public void TransferIn(CurrencyAmount transferredCurrency, DateTime time)
        {
            this.TransferInCalls = this.TransferInCalls.Append(transferredCurrency);
        }

        public IEnumerable<CurrencyAmount> TransferOut(CurrencyAmount currencyAmount, TransferOutReason reason, out decimal overdrawnAmount)
        {
            this.TransferOutCalls = this.TransferOutCalls.Append((currencyAmount, reason));
            overdrawnAmount = 0;
            return null;
        }
    }

    public class MockTrackingInventoryFactory : IInventoryFactory
    {
        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, Exchange exchange, DString name)
        {
            return new MockTrackingInventory();
        }

        public IInventory Create(IdProviderService idProvider, IReportingService reportingService, ILoggingService loggingService, InventoryDTO dto)
        {
            throw new NotImplementedException("Cannot instantiate the load-constructor for the mock tracking inventory.");
        }
    }
}
