﻿using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Used for holding run info properties, does NOT handle arg parsing or interaction with the file service.
    /// </summary>
    public class MockRunService : IRunService
    {
        private RunService _LiveRunService;
        private readonly RunInfoDTO _UnderlyingRunInfo;
        private readonly RunStateDTO _CurrentState;

        private readonly IdProviderService _IdProviderService;
        private readonly RunPersistorService _RunPersistorService;
        private readonly ILoggingService _LoggingService;

        private readonly DString _RunId;
        private readonly string[] _OriginalArgs;
        private readonly NString _Name;
        private readonly FinancialYear _FinancialYear;
        private readonly InventoryType _InventoryType;
        private readonly CandlestickInterval _CandlestickInterval;
        private readonly PriceCalculationType _PriceCalculationType;
        private readonly InventoryManagementType _InventoryManagementType;
        private readonly int? _DecimalPlaces;
        private readonly RoundingType _RoundingType;
        private readonly bool _EnableReporting;
        private readonly LoggingTemplate? _ConsoleLoggingTemplate;
        private readonly LoggingTemplate? _PersistorLoggingTemplate;
        private readonly RunStateDTO _BeginningState;

        public MockRunService(
            IdProviderService idProviderService,
            RunPersistorService persistor,
            ILoggingService loggingService,
            DString runId,
            string[] originalArgs,
            NString name = default,
            FinancialYear financialYear = default,
            InventoryType inventoryType = InventoryType.FILO,
            CandlestickInterval candlestickInterval = CandlestickInterval.Minute_1,
            PriceCalculationType priceCalculationType = PriceCalculationType.Close,
            InventoryManagementType inventoryManagementType = InventoryManagementType.Single,
            int? decimalPlaces = null,
            RoundingType roundingType = RoundingType.None,
            bool enableReporting = false,
            LoggingTemplate? consoleLoggingTemplate = LoggingTemplate.Console_Verbose,
            LoggingTemplate? persistorLoggingTemplate = null,
            RunStateDTO beginningState = null,
            RunStateDTO currentState = null)
        {
            this._LiveRunService = null;
            this._UnderlyingRunInfo = null;
            this._CurrentState = currentState ?? new RunStateDTO(runId, new List<InventoryDTO>());

            this._IdProviderService = idProviderService;
            this._RunPersistorService = persistor;
            this._LoggingService = loggingService;

            this._RunId = runId;
            this._OriginalArgs = originalArgs;
            this._Name = name;
            this._FinancialYear = financialYear;
            this._InventoryType = inventoryType;
            this._CandlestickInterval = candlestickInterval;
            this._PriceCalculationType = priceCalculationType;
            this._InventoryManagementType = inventoryManagementType;
            this._DecimalPlaces = decimalPlaces;
            this._RoundingType = roundingType;
            this._EnableReporting = enableReporting;
            this._ConsoleLoggingTemplate = consoleLoggingTemplate;
            this._PersistorLoggingTemplate = persistorLoggingTemplate;
            this._BeginningState = beginningState;
        }

        public MockRunService(IdProviderService idProviderService, RunPersistorService persistor, ILoggingService loggingService, RunInfoDTO runInfo, RunStateDTO currentState) : this(
            idProviderService,
            persistor,
            loggingService,
            runInfo.Id,
            runInfo.OriginalArgs,
            runInfo.Name,
            runInfo.FinancialYear,
            runInfo.InventoryType,
            runInfo.CandlestickInterval,
            runInfo.PriceCalculationType,
            runInfo.InventoryManagementType,
            runInfo.DecimalPlaces,
            runInfo.RoundingType,
            runInfo.EnableReporting,
            runInfo.ConsoleLoggingTemplate,
            runInfo.PersistorLoggingTemplate,
            runInfo.BeginningState,
            currentState)
        {
            this._UnderlyingRunInfo = runInfo;
        }

        public void Initialise(string[] args)
        {
            throw new NotImplementedException();

#pragma warning disable CS0162 // Unreachable code detected
            this._LiveRunService = new RunService(this._IdProviderService, this._RunPersistorService, this._LoggingService);
#pragma warning restore CS0162 // Unreachable code detected
            this._LiveRunService.Initialise(args);
        }

        public void Save()
        {
            if (this._UnderlyingRunInfo == null) throw new Exception("Cannot call MockRunService.Save() because the underlying RunInfo object was not provided in the constructor.");

            this._RunPersistorService.Save(this._UnderlyingRunInfo);
        }

        public void SaveState()
        {
            this._RunPersistorService.SaveState(this._CurrentState);
        }

        public void SaveResults(CalculationResults results)
        {
            this._RunPersistorService.SaveResults(results.ToDTO(), this._RunId);
        }

        public IEnumerable<IInventory> LoadBeginningInventories(IInventoryFactory factory, IReportingService reportingService)
        {
            throw new NotImplementedException();
        }

        public void UpdateCurrentInventories(IEnumerable<IInventory> inventories)
        {
            throw new NotImplementedException();
        }

        public DString GetRunId() => this._RunId;
        public string[] GetOriginalArgs() => this._OriginalArgs;
        public NString GetName() => this._Name;
        public FinancialYear GetFinancialYear() => this._FinancialYear;
        public InventoryType GetInventoryType() => this._InventoryType;
        public CandlestickInterval GetCandlestickInterval() => this._CandlestickInterval;

        public PriceCalculationType GetPriceCalculationType() => this._PriceCalculationType;
        public InventoryManagementType GetInventoryManagementType() => this._InventoryManagementType;
        public int? GetDecimalPlaces() => this._DecimalPlaces;
        public RoundingType GetRoundingType() => this._RoundingType;
        public bool GetEnableReporting() => this._EnableReporting;
        public LoggingTemplate? GetConsoleLoggingTemplate() => this._ConsoleLoggingTemplate;
        public LoggingTemplate? GetPersistorLoggingTemplate() => this._PersistorLoggingTemplate;
        public RunStateDTO GetBeginningState() => this._BeginningState;
    }
}
