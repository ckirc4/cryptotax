﻿using System;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Price is always zero.
    /// </summary>
    internal class MockPriceCalculatorZero : IPriceCalculator
    {
        public PriceCalculationType Type { get; }

        public decimal CalculatePrice(DateTime time, Candlestick candlestick)
        {
            return 0;
        }
    }
}
