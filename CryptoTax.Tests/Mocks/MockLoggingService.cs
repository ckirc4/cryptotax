﻿using CryptoTax.Shared.Services.Logging;
using Moq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    /// <summary>
    /// Use this instead of <see cref="Moq.Mock"/> so that the logger registration automatically returns an instantiated object.
    /// </summary>
    public class MockLoggingService : ILoggingService
    {
        public void Flush() { }

        public IRegisteredLogger Register(object obj, DString name)
        {
            return new Mock<IRegisteredLogger>().Object;
        }

        public void Unregister(object obj) { }

        public void UpdateSettings(ILoggingSettings settings) { }
    }
}
