﻿using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    public class MockWebService : IWebService
    {
        private readonly Dictionary<DString, DString> _RequestResponseDict;
        private readonly Func<DString, DString> _RequestHandler;
        public IEnumerable<DString> CallsMade { get; private set; }

        /// <summary>
        /// Use predefined responses to requests.
        /// </summary>
        public MockWebService(Dictionary<DString, DString> requestResponseDict)
        {
            this._RequestResponseDict = requestResponseDict;
        }

        /// <summary>
        /// Handle requests using a custom implementation.
        /// </summary>
        public MockWebService(Func<DString, DString> requestHandler)
        {
            this._RequestHandler = requestHandler;
        }

        public Task<DString> DownloadStringAsync(DString request)
        {
            DString response = this._RequestResponseDict != null ? this._RequestResponseDict[request] : this._RequestHandler(request);
            return Task.FromResult(response);
        }
    }
}
