﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using Newtonsoft.Json;
using CryptoTax.Shared.Enums;
using System.Threading.Tasks;
using CryptoTax.Shared;

namespace CryptoTax.Tests.Mocks
{
    public class MockExchangeApiAdapter : IExchangeApiAdapter
    {
        private readonly bool _IntervalInParams;
        private readonly bool _ExchangeInParams;
        private readonly bool _SymbolInParams;

        /// <summary>
        /// Specifiy which values to include in the parameters dictionary. By default, includes "to" and "from" values, which are just DateTime.Ticks.ToString() for easy parsing later on.
        /// </summary>
        /// <param name="intervalInParams">If true, adds a CandlestickInterval.ToString() value to the "interval" key.</param>
        /// <param name="exchangeInParams">If true, adds an Exchange.ToString() value to the "exchange" key.</param>
        /// <param name="symbolInParams">If true, adds a GenericSymbol.ToString() value to the "symbol" key.</param>
        public MockExchangeApiAdapter(bool intervalInParams = false, bool exchangeInParams = false, bool symbolInParams = false)
        {
            this._IntervalInParams = intervalInParams;
            this._ExchangeInParams = exchangeInParams;
            this._SymbolInParams = symbolInParams;
        }

        public Dictionary<string, string> CreateCandlestickParameters(CandlestickInterval interval, ExchangeSymbol symbol, IEnumerable<DateTime> sortedTimes)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                { "from", sortedTimes.First().Ticks.ToString() },
                { "to", sortedTimes.Last().Ticks.ToString() }
            };

            if (this._IntervalInParams) parameters.Add("interval", interval.ToString());
            if (this._ExchangeInParams) parameters.Add("exchange", symbol.Exchange.ToString());
            if (this._SymbolInParams) parameters.Add("symbol", symbol.Symbol.ToString());

            return parameters;
        }

        /// <summary>
        /// Given a collection of from-to date request parameters and the associated response, and base link, generates the url-response dictionary to be injected into the web service. Each response is an ordered collection (json array) of serialised candlesticks of potentially variable length and may include blanks.
        /// </summary>
        public Dictionary<DString, DString> GenerateResponseDict(IEnumerable<(DateTime requestFrom, DateTime requestTo, IEnumerable<Candlestick> response)> data, DString baseUrl, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            Dictionary<DString, DString> dict = new Dictionary<DString, DString>();
            if (!data.Any()) return dict;

            foreach ((DateTime requestFrom, DateTime requestTo, IEnumerable<Candlestick> response) datum in data)
            {
                DateTime from = datum.requestFrom;
                DateTime to = datum.requestTo;
                IEnumerable<Candlestick> response = datum.response;

                Dictionary<string, string> queryParameters = this.CreateCandlestickParameters(interval, symbol, new[] { from, to });
                DString requestString = ApiHelpers.GenerateFullUrl(baseUrl, queryParameters);
                DString responseString = JsonConvert.SerializeObject(response, Formatting.None);
                dict.Add(requestString, responseString);
            }

            return dict;
        }

        public Candlestick ParseCandlestick(dynamic obj, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            // it's that simple mate
            return JsonConvert.DeserializeObject<Candlestick>(obj.ToString());
        }

        public IEnumerable<dynamic> ParseCandlestickCollection(dynamic response)
        {
            return response as IEnumerable<dynamic>;
        }

        public ExchangeSymbol ParseSymbol(dynamic obj)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<dynamic> ParseSymbolCollection(dynamic response)
        {
            throw new NotImplementedException();
        }

        public async Task<dynamic> ObserveResponse(Func<Task<dynamic>> apiCall, string url, Dictionary<string, string> parameters)
        {
            return await apiCall();
        }
    }
}
