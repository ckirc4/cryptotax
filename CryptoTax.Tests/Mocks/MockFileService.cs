﻿using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Collections;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Mocks
{
    internal class MockFileService : IFileService
    {
        private static readonly DString PATH_SEPARATOR = @"\";

        private readonly ICryptoService _CryptoService;

        /// <summary>
        /// Key: fileName. Value: contents.
        /// Content written to an existing file overwrites the older content.
        /// </summary>
        public Dictionary<DString, IEnumerable<DString>> FileSystem_Lines;
        /// <summary>
        /// Key: fileName. Value: Collection of lines written to the file.
        /// Content written to an existing file overwrites the older content.
        /// </summary>
        public Dictionary<DString, DString> FileSystem_Files;

        /// <summary>
        /// Specify exactly what can be retrieved from the mock file system.
        /// </summary>
        public MockFileService(ICryptoService cryptoService = null, Dictionary<DString, IEnumerable<DString>> readableLines = null, Dictionary<DString, DString> readableFiles = null)
        {
            this._CryptoService = cryptoService;
            this.FileSystem_Lines = readableLines ?? new Dictionary<DString, IEnumerable<DString>>();
            this.FileSystem_Files = readableFiles ?? new Dictionary<DString, DString>();
        }

        public DString ReadFile(DString fileName)
        {
            if (this.FileSystem_Files.ContainsKey(fileName))
            {
                return this.FileSystem_Files[fileName];
            }
            else throw new System.IO.FileNotFoundException($"Mock file {fileName} does not exist.");
        }

        public T ReadAndDeserialiseFile<T>(DString fileName)
        {
            return JsonConvert.DeserializeObject<T>(this.ReadFile(fileName));
        }

        public void WriteLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.writeLines(fileName, lines, newFileOnly, false);
        }
        public void AppendLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.writeLines(fileName, lines, newFileOnly, true);
        }

        private void writeLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly, bool append)
        {
            // check if mock file exists
            if (!this.FileSystem_Lines.ContainsKey(fileName))
            {
                if (newFileOnly) throw new FileAlreadyExistsException(fileName);
                else if (!append) this.FileSystem_Lines.Remove(fileName);
            }

            // update our mock file system
            IEnumerable<DString> definedLines = lines.Select(line => new DString(line.Value ?? ""));
            if (!this.FileSystem_Lines.ContainsKey(fileName))
            {
                // doesn't matter if appending or not
                this.FileSystem_Lines.Add(fileName, definedLines);
            }
            else
            {
                if (append)
                {
                    this.FileSystem_Lines[fileName] = this.FileSystem_Lines[fileName].AppendMany(definedLines.ToArray());
                }
                else
                {
                    // probably redundant, but leave just in case
                    this.FileSystem_Lines[fileName] = definedLines;
                }
            }
        }

        public IEnumerable<DString> ReadLines(DString fileName)
        {
            if (this.FileSystem_Lines.ContainsKey(fileName))
            {
                return this.FileSystem_Lines[fileName];
            }
            else throw new System.IO.FileNotFoundException($"Mock lines {fileName} do not exist.");
        }

        public void WriteFile(DString fileName, DString contents, bool newFileOnly)
        {
            // update our mock file system
            if (!this.FileSystem_Files.ContainsKey(fileName))
            {
                this.FileSystem_Files.Add(fileName, contents);
            }
            else
            {
                if (newFileOnly) throw new FileAlreadyExistsException(fileName);
                this.FileSystem_Files[fileName] = contents;
            }
        }

        public IEnumerable<DString> GetFilesInFolder(DString folder)
        {
            if (!folder.Value.EndsWith(PATH_SEPARATOR)) folder += PATH_SEPARATOR;
            List<DString> files = new List<DString>();

            foreach (string file in this.FileSystem_Files.Keys.Concat(this.FileSystem_Lines.Keys))
            {
                if (file.StartsWith(folder))
                {
                    if (!file.Substring(folder.Value.Length).Contains(PATH_SEPARATOR))
                    {
                        files.Add(file);
                    }
                }
            }

            return files;
        }

        /// <summary>
        /// This may be slow.
        /// </summary>
        public DString GetFingerprint(DString fileName)
        {
            string contents = this.ReadFile(fileName);
            return this._CryptoService.CalculateHash(contents);
        }
    }
}
