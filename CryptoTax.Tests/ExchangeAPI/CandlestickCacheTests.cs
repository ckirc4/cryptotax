﻿using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class CandlestickCacheTests
    {
        private ExchangeSymbol BTCAUD = new ExchangeSymbol(
            new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC),
            Exchange.Undefined,
            "BTCAUD");

        [TestMethod]
        public void EmptyCacheReturnsEmptyCollection()
        {
            ExchangeCache cache = new ExchangeCache();

            Candlestick? candle = cache.GetCandlestick(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, DateTime.MinValue);
            IEnumerable<Candlestick> candles = cache.GetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, DateTime.MinValue, DateTime.MaxValue);

            Assert.IsNull(candle);
            Assert.IsFalse(candles.Any());
        }

        [TestMethod]
        public void CacheReturnsCorrectCandlesticks()
        {
            ExchangeCache cache = new ExchangeCache();
            DateTime startTime = new DateTime(2020, 1, 1);
            IEnumerable<Candlestick> candlesticks = this.CreateDayCandlesticks(startTime, 3);

            cache.SetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, candlesticks);
            IEnumerable<Candlestick> cachedCandlesticks = cache.GetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, startTime, startTime.AddDays(5));

            Assert.AreEqual(3, cachedCandlesticks.Count());
            Assert.AreEqual(startTime, cachedCandlesticks.First().StartTime);
            Assert.AreEqual(startTime.AddDays(2), cachedCandlesticks.Last().StartTime);

            // add 3 more, with 1 day gap BEFORE others
            IEnumerable<Candlestick> newCandlesticks = this.CreateDayCandlesticks(startTime.AddDays(-4), 3);

            cache.SetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, newCandlesticks);
            IEnumerable<Candlestick> newCachedCandlesticks = cache.GetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, startTime.AddDays(-10), startTime.AddDays(10));

            Assert.AreEqual(6, newCachedCandlesticks.Count());
            Assert.AreEqual(startTime.AddDays(-4), newCachedCandlesticks.First().StartTime);
            Assert.AreEqual(startTime.AddDays(2), newCachedCandlesticks.Last().StartTime);
        }

        [TestMethod]
        public void CacheFilterWorks()
        {
            ExchangeCache cache = new ExchangeCache();
            DateTime startTime = new DateTime(2020, 1, 1);
            IEnumerable<Candlestick> candlesticks = this.CreateDayCandlesticks(startTime, 10);

            cache.SetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, candlesticks);
            IEnumerable<Candlestick> cachedCandlesticks = cache.GetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, startTime.AddDays(1), startTime.AddDays(3));

            Assert.AreEqual(3, cachedCandlesticks.Count());
            Assert.AreEqual(startTime.AddDays(1), cachedCandlesticks.First().StartTime);
            Assert.AreEqual(startTime.AddDays(3), cachedCandlesticks.Last().StartTime);
        }

        [TestMethod]
        public void CacheReturnsCorrectMissingTimes()
        {
            ExchangeCache cache = new ExchangeCache();
            DateTime startTime = new DateTime(2020, 1, 1);
            IEnumerable<Candlestick> candlestick1 = this.CreateDayCandlesticks(startTime, 1);
            IEnumerable<Candlestick> candlestick2 = this.CreateDayCandlesticks(startTime.AddDays(2), 1);

            cache.SetCandlesticks(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, new List<Candlestick>() { candlestick1.First(), candlestick2.First() });
            IEnumerable<DateTime> missingTimes = cache.GetMissingCandlestickTimes(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, startTime, startTime.AddDays(3));

            Assert.AreEqual(2, missingTimes.Count());
            Assert.AreEqual(startTime.AddDays(1), missingTimes.First());
            Assert.AreEqual(startTime.AddDays(3), missingTimes.Last());

            // try grouped
            IEnumerable<IEnumerable<DateTime>> missingTimesGrouped = cache.GetMissingCandlestickTimesGrouped(Exchange.Undefined, this.BTCAUD, CandlestickInterval.Day_1, startTime, startTime.AddDays(3));

            Assert.AreEqual(2, missingTimesGrouped.Count());
            Assert.AreEqual(1, missingTimesGrouped.First().Count());
            Assert.AreEqual(1, missingTimesGrouped.Last().Count());
            Assert.AreEqual(startTime.AddDays(1), missingTimesGrouped.First().First());
            Assert.AreEqual(startTime.AddDays(3), missingTimesGrouped.Last().First());
        }

        private IEnumerable<Candlestick> CreateDayCandlesticks(DateTime from, int count)
        {
            List<Candlestick> candlesticks = new List<Candlestick>();

            DateTime nextTime = from;
            for (int i = 0; i < count; i++)
            {
                candlesticks.Add(new Candlestick(nextTime, CandlestickInterval.Day_1, this.BTCAUD, i + 1, i + 0.90m));
                nextTime = CandlestickHelpers.GetNextTime(CandlestickInterval.Day_1, nextTime);
            }

            return candlesticks;
        }
    }
}
