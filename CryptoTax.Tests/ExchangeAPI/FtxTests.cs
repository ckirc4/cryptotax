﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class FtxTests
    {
        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsExchangeSymbolForBTCUSDTSpot()
        {
            IExchangeApiService api = new FtxApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<ExchangeSymbol> allSymbols = await api.GetSymbolsAsync();
            ExchangeSymbol btcUsd = allSymbols.First(s => s.Name.Equals("BTC/USD"));
            Assert.AreEqual(Exchange.FTX, btcUsd.Exchange);
            Assert.AreEqual("BTC/USD", (string)btcUsd.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.BTC), btcUsd.Symbol);
        }

        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsCorrectCandle()
        {
            DateTime from = new DateTime(2020, 12, 18, 0, 19, 30, DateTimeKind.Local).ToUniversalTime();
            DateTime to = new DateTime(2020, 12, 18, 5, 35, 0, DateTimeKind.Local).ToUniversalTime();
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, new Currency(null, "XRP-PERP", false)), Exchange.FTX, "XRP-PERP");

            IExchangeApiService api = new FtxApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<Candlestick> candlesticks = await api.GetCandlesticksAsync(CandlestickInterval.Hour_1, symbol, from, to, false);

            Assert.AreEqual(6, candlesticks.Count());
            Candlestick lastCandlestick = candlesticks.Last();
            Assert.AreEqual(0.619775m, lastCandlestick.Open);
            Assert.AreEqual(0.600325m, lastCandlestick.Close);
            Assert.AreEqual(new DateTime(2020, 12, 18, 5, 0, 0, DateTimeKind.Local).ToUniversalTime(), lastCandlestick.StartTime);
            Assert.AreEqual(CandlestickInterval.Hour_1, lastCandlestick.Interval);
            Assert.AreEqual(Exchange.FTX, lastCandlestick.Symbol.Exchange);
            Assert.AreEqual("XRP-PERP", (string)lastCandlestick.Symbol.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USD, new Currency(null, "XRP-PERP", false)), lastCandlestick.Symbol.Symbol);
        }
    }
}
