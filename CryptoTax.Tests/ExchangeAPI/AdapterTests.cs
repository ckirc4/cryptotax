﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Services;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class AdapterTests
    {
        [TestMethod]
        public async Task BinanceAdapter_ParsesAndThrowsInvalidSymbolError()
        {
            // wow this actually worked..
            // thanks to https://stackoverflow.com/a/9823224

            // build response data
            string errorMsg = "{\"code\":-1121,\"msg\":\"Invalid symbol.\"}";
            byte[] errorBytes = Encoding.UTF8.GetBytes(errorMsg);
            MemoryStream responseStream = new MemoryStream(errorBytes);

            // setup mock beahviour
            Mock<HttpWebResponse> responseMock = new Mock<HttpWebResponse>();
            responseMock.Setup(c => c.GetResponseStream()).Returns(responseStream);
            WebException exceptionToThrow = new WebException("test", null, WebExceptionStatus.ProtocolError, responseMock.Object);

            Mock<IFileService> mockFileService = new Mock<IFileService>();
            CandlestickPersistorService candlestickPersistorService = new CandlestickPersistorService(mockFileService.Object, "DataFolder\\");
            Mock<IExchangeResponsePersistorService> mockResponsePersistor = new Mock<IExchangeResponsePersistorService>();
            ExchangeCache exchangeCache = new ExchangeCache(candlestickPersistorService, mockResponsePersistor.Object, null);

            // initialise
            BinanceSpotApiService adapter = new BinanceSpotApiService(null, new MockLoggingService(), exchangeCache);

            // act and assert
            await Assert.ThrowsExceptionAsync<InvalidSymbolException>(() => adapter.ObserveResponse(() => throw exceptionToThrow, "testUrl", null));
            InvalidSymbolException e;
            mockResponsePersistor.Verify(r => r.TryGetCachedException(Exchange.Binance, "testUrl", null, out e));
            mockResponsePersistor.Verify(r => r.CacheException(Exchange.Binance, "testUrl", null, It.IsAny<InvalidSymbolException>()));
        }
    }
}
