﻿using CryptoTax.Domain.Models;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class ExchangeCacheTests
    {
        // if you are here for candlestick-related tests then you might want to head over to PersistentCandlestickCacheTests.cs

        private Mock<IExchangeResponsePersistorService> _MockResponsePersistor;
        private Mock<IMissingCandlestickPersistorService> _MockMissingCandlestickPersistor;

        [TestInitialize]
        public void Setup()
        {
            this._MockResponsePersistor = new Mock<IExchangeResponsePersistorService>();
            this._MockMissingCandlestickPersistor = new Mock<IMissingCandlestickPersistorService>();
        }

        [TestMethod]
        public void InMemoryCache_CacheException_CachesCorrectly()
        {
            ExchangeCache cache = new ExchangeCache();
            Exception e1 = new Exception();
            ArgumentException e2 = new ArgumentException();

            cache.CacheException(Exchange.Binance, "testUrl", null, e1);
            cache.CacheException(Exchange.Binance, "testUrl", null, e2);
            bool result = cache.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException cachedException);

            Assert.IsTrue(result);
            Assert.AreEqual(e2, cachedException);
        }

        [TestMethod]
        public void InMemoryCache_TryGetCachedException_ReturnsFalseIfDoesntExist()
        {
            ExchangeCache cache = new ExchangeCache();

            bool result = cache.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException cachedException);

            Assert.IsFalse(result);
            Assert.IsNull(cachedException);
        }

        [TestMethod]
        public void InMemoryCache_TryGetCachedException_ThrowsIfDifferentType()
        {
            ExchangeCache cache = new ExchangeCache();
            Exception e = new Exception();

            cache.CacheException(Exchange.Binance, "testUrl", null, e);

            Assert.ThrowsException<InvalidTypeException>(() => cache.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException _));
        }

        [TestMethod]
        public void PersistentCache_TryGetCachedException_UsesPersistorMethod()
        {
            ExchangeCache cache = new ExchangeCache(null, this._MockResponsePersistor.Object, null);

            cache.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException _);

            ArgumentException e;
            this._MockResponsePersistor.Verify(r => r.TryGetCachedException(Exchange.Binance, "testUrl", null, out e));
        }

        [TestMethod]
        public void PersistentCache_CacheException_UsesPersistorMethod()
        {
            ExchangeCache cache = new ExchangeCache(null, this._MockResponsePersistor.Object, null);

            ArgumentException e = new ArgumentException();
            cache.CacheException(Exchange.Binance, "testUrl", null, e);

            this._MockResponsePersistor.Verify(r => r.CacheException(Exchange.Binance, "testUrl", null, e));
        }

        [TestMethod]
        public void PersistentCache_AddKnownMissingData_UsesPersistorMethod()
        {
            ExchangeCache cache = new ExchangeCache(null, null, this._MockMissingCandlestickPersistor.Object);
            DateTime noDataFrom = new DateTime(2021, 1, 1);
            DateTime noDataTo = new DateTime(2021, 1, 28);
            ExchangeSymbol exchangeSymbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
            
            cache.AddKnownMissingData(exchangeSymbol, noDataFrom, noDataTo);

            this._MockMissingCandlestickPersistor.Verify(m => m.AddKnownMissingData(exchangeSymbol.Exchange, exchangeSymbol.Name, noDataFrom, noDataTo));
        }

        [TestMethod]
        public void PersistentCache_IncludesKnownMissingData_UsesPersistorMethod()
        {
            ExchangeCache cache = new ExchangeCache(null, null, this._MockMissingCandlestickPersistor.Object);
            DateTime from = new DateTime(2021, 1, 1);
            DateTime to = new DateTime(2021, 1, 28);
            ExchangeSymbol exchangeSymbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);

            bool result = cache.IncludesKnownMissingData(exchangeSymbol, from, to, out DateTime? output);

            DateTime? verifyOut;
            this._MockMissingCandlestickPersistor.Verify(m => m.IncludesKnownMissingData(exchangeSymbol.Exchange, exchangeSymbol.Name, from, to, out verifyOut));
        }
    }
}
