﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CryptoTax.Domain.Models;
using CryptoTax.DTO.Serialised;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ExchangeAPI
{
    // while looking through these I suddenly started feeling sick
    [TestClass]
    public class PersistentCandlestickCacheTests
    {
        private static readonly DString _Path = Constants.CANDLESTICK_DATA_PATH;
        private static readonly ExchangeSymbol[] _Symbols = new[]
        {
            new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), Exchange.BTCMarkets),
            new ExchangeSymbol(new GenericSymbol(KnownCurrencies.BOMB, KnownCurrencies.ETH), Exchange.MercatoX),
            new ExchangeSymbol(new GenericSymbol(KnownCurrencies.ETH, KnownCurrencies.BTC), Exchange.BTCMarkets),
        };
        private static readonly CandlestickInterval[] _Intervals = new[]
        {
            CandlestickInterval.Minute_1,
            CandlestickInterval.Hour_1,
            CandlestickInterval.Day_1
        };
        private readonly Func<CandlestickInterval, ExchangeSymbol, Candlestick> _ZeroCandlestick = (interval, symbol) => new Candlestick(new DateTime(2021, 1, 1), interval, symbol, 1, 1);
        private Mock<IExchangeResponsePersistorService> _MockResponsePersistor;

        [TestInitialize]
        public void Setup()
        {
            this._MockResponsePersistor = new Mock<IExchangeResponsePersistorService>();
        }

        [TestMethod]
        public void NoPersistentCandlesticksDoNotAffectCache()
        {
            MockFileService fileService = this.generateMockFileService();
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            Assert.IsNotNull(cache);
        }

        [TestMethod]
        public void SingleTypePersistentCandlesticksLoadedIntoEmptyCache()
        {
            CandlestickInterval interval = _Intervals[0];
            ExchangeSymbol symbol = _Symbols[0];
            IEnumerable<Candlestick> candlesticks = this.generateCandlesticks(0, 1000, interval, symbol);
            MockFileService fileService = this.generateMockFileService(candlesticks);
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            Assert.IsTrue(this.compareFilesWithExpected(fileService, candlesticks));
            List<Candlestick> actual = cache.GetCandlesticks(symbol.Exchange, symbol, interval, candlesticks.First().StartTime, candlesticks.Last().StartTime);
            Assert.IsTrue(this.compareCandlesticks(candlesticks, actual));
        }

        [TestMethod]
        public void MultipleTypePersistentCandlesticksLoadedIntoEmptyCache()
        {
            CandlestickInterval interval1 = _Intervals[0];
            ExchangeSymbol symbol1 = _Symbols[0];
            IEnumerable<Candlestick> candlesticks1 = this.generateCandlesticks(0, 1000, interval1, symbol1);
            CandlestickInterval interval2 = _Intervals[1];
            ExchangeSymbol symbol2 = _Symbols[0];
            IEnumerable<Candlestick> candlesticks2 = this.generateCandlesticks(0, 100, interval2, symbol2);
            CandlestickInterval interval3 = _Intervals[0];
            ExchangeSymbol symbol3 = _Symbols[1];
            IEnumerable<Candlestick> candlesticks3 = this.generateCandlesticks(0, 10, interval3, symbol3);
            MockFileService fileService = this.generateMockFileService(candlesticks1, candlesticks2, candlesticks3);
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            Assert.IsTrue(this.compareFilesWithExpected(fileService, candlesticks1, candlesticks2, candlesticks3));
            List<Candlestick> actual1 = cache.GetCandlesticks(symbol1.Exchange, symbol1, interval1, candlesticks1.First().StartTime, candlesticks1.Last().StartTime);
            List<Candlestick> actual2 = cache.GetCandlesticks(symbol2.Exchange, symbol2, interval2, candlesticks2.First().StartTime, candlesticks2.Last().StartTime);
            List<Candlestick> actual3 = cache.GetCandlesticks(symbol3.Exchange, symbol3, interval3, candlesticks3.First().StartTime, candlesticks3.Last().StartTime);
            Assert.IsTrue(this.compareCandlesticks(candlesticks1, actual1));
            Assert.IsTrue(this.compareCandlesticks(candlesticks2, actual2));
            Assert.IsTrue(this.compareCandlesticks(candlesticks3, actual3));
        }

        [TestMethod]
        public void SingleTypeCacheCandlesticksArePersisted()
        {
            CandlestickInterval interval = _Intervals[0];
            ExchangeSymbol symbol = _Symbols[0];
            IEnumerable<Candlestick> candlesticks = this.generateCandlesticks(0, 1000, interval, symbol);
            MockFileService fileService = this.generateMockFileService();
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            cache.SetCandlesticks(symbol.Exchange, symbol, interval, candlesticks);

            Assert.IsTrue(this.compareFilesWithExpected(fileService, candlesticks));
            List<Candlestick> actual = cache.GetCandlesticks(symbol.Exchange, symbol, interval, candlesticks.First().StartTime, candlesticks.Last().StartTime);
            Assert.IsTrue(this.compareCandlesticks(candlesticks, actual));
        }

        [TestMethod]
        public void MultipleTypeCacheCandlesticksArePersisted()
        {
            CandlestickInterval interval1 = _Intervals[0];
            ExchangeSymbol symbol1 = _Symbols[0];
            IEnumerable<Candlestick> candlesticks1 = this.generateCandlesticks(0, 1000, interval1, symbol1);
            CandlestickInterval interval2 = _Intervals[1];
            ExchangeSymbol symbol2 = _Symbols[0];
            IEnumerable<Candlestick> candlesticks2 = this.generateCandlesticks(0, 100, interval2, symbol2);
            CandlestickInterval interval3 = _Intervals[0];
            ExchangeSymbol symbol3 = _Symbols[1];
            IEnumerable<Candlestick> candlesticks3 = this.generateCandlesticks(0, 10, interval3, symbol3);
            MockFileService fileService = this.generateMockFileService();
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            cache.SetCandlesticks(symbol1.Exchange, symbol1, interval1, candlesticks1);
            cache.SetCandlesticks(symbol2.Exchange, symbol2, interval2, candlesticks2);
            cache.SetCandlesticks(symbol3.Exchange, symbol3, interval3, candlesticks3);

            Assert.IsTrue(this.compareFilesWithExpected(fileService, candlesticks1, candlesticks2, candlesticks3));
            List<Candlestick> actual1 = cache.GetCandlesticks(symbol1.Exchange, symbol1, interval1, candlesticks1.First().StartTime, candlesticks1.Last().StartTime);
            List<Candlestick> actual2 = cache.GetCandlesticks(symbol2.Exchange, symbol2, interval2, candlesticks2.First().StartTime, candlesticks2.Last().StartTime);
            List<Candlestick> actual3 = cache.GetCandlesticks(symbol3.Exchange, symbol3, interval3, candlesticks3.First().StartTime, candlesticks3.Last().StartTime);
            Assert.IsTrue(this.compareCandlesticks(candlesticks1, actual1));
            Assert.IsTrue(this.compareCandlesticks(candlesticks2, actual2));
            Assert.IsTrue(this.compareCandlesticks(candlesticks3, actual3));
        }

        [TestMethod]
        public void CacheCandlesticksArePersistedOverwritingExistingOnes()
        {
            CandlestickInterval interval = _Intervals[0];
            ExchangeSymbol symbol = _Symbols[0];

            IEnumerable<Candlestick> existingCandlesticks = this.generateCandlesticks(0, 1000, interval, symbol);
            MockFileService fileService = this.generateMockFileService(existingCandlesticks);
            ExchangeCache cache = new ExchangeCache(new CandlestickPersistorService(fileService, _Path), this._MockResponsePersistor.Object, null);

            IEnumerable<Candlestick> newCandlesticks1 = this.generateCandlesticks(500, 1000, interval, symbol);
            IEnumerable<Candlestick> newCandlesticks2 = this.generateCandlesticks(2000, 10, interval, symbol);
            cache.SetCandlesticks(symbol.Exchange, symbol, interval, newCandlesticks1);
            cache.SetCandlesticks(symbol.Exchange, symbol, interval, newCandlesticks2);

            IEnumerable<Candlestick> expectedCandlesticks = existingCandlesticks.Take(500).Concat(newCandlesticks1).Concat(newCandlesticks2);
            Assert.IsTrue(this.compareFilesWithExpected(fileService, expectedCandlesticks));
            List<Candlestick> actual = cache.GetCandlesticks(symbol.Exchange, symbol, interval, existingCandlesticks.First().StartTime, expectedCandlesticks.Last().StartTime);
            Assert.IsTrue(this.compareCandlesticks(expectedCandlesticks, actual));
        }

        /// <summary>
        /// Quickly generate candlesticks. Times are determined using the time offset, symbol and interval must be one of index 0-2. All prices are 1.
        /// </summary>
        private IEnumerable<Candlestick> generateCandlesticks(int offsetFrom, int count, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            List<Candlestick> candlesticks = new List<Candlestick>();
            Candlestick next = this._ZeroCandlestick(interval, symbol);

            // yes, this is not very efficient, but it doesn't matter as long as we are dealing with small offsets only
            for (int i = 0; i < offsetFrom + count; i++)
            {
                if (i >= offsetFrom)
                {
                    candlesticks.Add(next);
                }

                next = new Candlestick(next);
            }

            return candlesticks;
        }

        /// <summary>
        /// Save collections of candlesticks to the mock file service.
        /// </summary>
        private MockFileService generateMockFileService(Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>> candlesticks)
        {
            Dictionary<DString, DString> files = new Dictionary<DString, DString>();
            foreach (KeyValuePair<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>> kvp in candlesticks)
            {
                DString contents = JsonConvert.SerializeObject(new SerialisedCandlestickDtoCollection(kvp.Value.Select(c => c.ToDTO())));
                DString fileName = this.getFullFileName(kvp.Key.Item1, kvp.Key.Item2, kvp.Key.Item3);
                files.Add(fileName, contents);
            }

            MockFileService fileService = new MockFileService(readableFiles: files);
            return fileService;
        }

        /// <summary>
        /// Quickly save combined collections of candlesticks to the mock file service.
        /// </summary>
        private MockFileService generateMockFileService(IEnumerable<IEnumerable<Candlestick>> candlesticks)
        {
            Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>> dict = new Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>>();
            foreach (IEnumerable<Candlestick> group in candlesticks)
            {
                Candlestick first = group.First();
                dict.Add((first.Symbol.Exchange, first.Symbol.Name, first.Interval), group);
            }

            return this.generateMockFileService(dict);
        }

        /// <summary>
        /// Quickly save individual collections of candlesticks to the mock file service.
        /// </summary>
        private MockFileService generateMockFileService(params IEnumerable<Candlestick>[] candlesticks)
        {
            return this.generateMockFileService(candlesticks.ToList());
        }

        private DString getFullFileName(Exchange exchange, DString symbolName, CandlestickInterval interval)
        {
            // copied from CandlestickPersistorService.cs
            string fileName = $"{exchange}_{symbolName}_{interval}.json";
            Path.GetInvalidFileNameChars().ToList().ForEach(c => fileName = fileName.Replace(c.ToString(), ""));
            return _Path + fileName;
        }

        private bool compareFilesWithExpected(MockFileService fileService, Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>> candlesticks)
        {
            foreach (var kvp in candlesticks)
            {
                DString expected = JsonConvert.SerializeObject(new SerialisedCandlestickDtoCollection(kvp.Value.Select(c => c.ToDTO())));
                DString fileName = this.getFullFileName(kvp.Key.Item1, kvp.Key.Item2, kvp.Key.Item3);

                if (!fileService.FileSystem_Files.ContainsKey(fileName)) return false;
                DString actual = fileService.FileSystem_Files[fileName];
                if (expected != actual) return false;
            }

            return true;
        }
        private bool compareFilesWithExpected(MockFileService fileService, params IEnumerable<Candlestick>[] candlesticks)
        {
            Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>> dict = new Dictionary<(Exchange, DString, CandlestickInterval), IEnumerable<Candlestick>>();
            foreach (IEnumerable<Candlestick> group in candlesticks)
            {
                Candlestick first = group.First();
                dict.Add((first.Symbol.Exchange, first.Symbol.Name, first.Interval), group);
            }

            return this.compareFilesWithExpected(fileService, dict);
        }

        private bool compareCandlesticks(IEnumerable<Candlestick> expected, IEnumerable<Candlestick> actual)
        {
            // cute little hack because I don't want to do work
            return JsonConvert.SerializeObject(expected.Select(c => c.ToDTO())) == JsonConvert.SerializeObject(actual.Select(c => c.ToDTO()));
        }
    }
}
