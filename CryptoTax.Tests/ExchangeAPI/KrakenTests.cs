﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibrary.Timing;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class KrakenTests
    {
        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsExchangeSymbolForAUDUSD()
        {
            IExchangeApiService api = new KrakenApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<ExchangeSymbol> allSymbols = await api.GetSymbolsAsync();
            ExchangeSymbol audUsd = allSymbols.First(s => s.Name.Equals("AUDUSD"));
            Assert.AreEqual(Exchange.Kraken, audUsd.Exchange);
            Assert.AreEqual("AUDUSD", (string)audUsd.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.AUD), audUsd.Symbol);
        }

        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsCorrectCandle()
        {
            // https://api.kraken.com/0/public/OHLC?pair=AUDUSD&since=1694211240 (note that `since` starts 1 minute before `from`)
            DateTime from = new DateTime().FromUnixSeconds(1694211300);
            DateTime to = new DateTime().FromUnixSeconds(1694211360L); // the next one
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.AUD), Exchange.Kraken, "AUDUSD");

            IExchangeApiService api = new KrakenApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<Candlestick> candlesticks = await api.GetCandlesticksAsync(CandlestickInterval.Minute_1, symbol, from, to, false);

            Assert.AreEqual(2, candlesticks.Count());
            Candlestick firstCandlestick = candlesticks.First();
            Assert.AreEqual(0.63732m, firstCandlestick.Open);
            Assert.AreEqual(0.63731m, firstCandlestick.Close);
            Assert.AreEqual(from, firstCandlestick.StartTime);
            Assert.AreEqual(CandlestickInterval.Minute_1, firstCandlestick.Interval);
            Assert.AreEqual(Exchange.Kraken, firstCandlestick.Symbol.Exchange);
            Assert.AreEqual("AUDUSD", (string)firstCandlestick.Symbol.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.AUD), firstCandlestick.Symbol.Symbol);
        }

        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestHandlesLargeRange()
        {
            // ensures that multiple requests are made to the trade endpoint to cover a large range.
            // in this case, the first request will retrieve about 1h 45min worth of trades, so a second request must be made
            DateTime from = new DateTime().FromUnixSeconds(1594211220);
            DateTime to = new DateTime().FromUnixSeconds(1594211220 + 3600 * 2 - 60); // 1h 59min after `from`
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.BTC), Exchange.Kraken, "BTCUSD");

            IExchangeApiService api = new KrakenApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<Candlestick> candlesticks = await api.GetCandlesticksAsync(CandlestickInterval.Minute_1, symbol, from, to, false);

            Assert.AreEqual(120, candlesticks.Count());

            Candlestick firstCandlestick = candlesticks.First();
            Assert.AreEqual(9296.7m, firstCandlestick.Open);
            Assert.AreEqual(9294.8m, firstCandlestick.Close);
            Assert.AreEqual(from, firstCandlestick.StartTime);

            Candlestick lastCandlestick = candlesticks.Last();
            Assert.AreEqual(9433.4m, lastCandlestick.Open);
            Assert.AreEqual(9432.8m, lastCandlestick.Close);
            Assert.AreEqual(to, lastCandlestick.StartTime);

            Assert.AreEqual(CandlestickInterval.Minute_1, firstCandlestick.Interval);
            Assert.AreEqual(Exchange.Kraken, firstCandlestick.Symbol.Exchange);
            Assert.AreEqual("BTCUSD", (string)firstCandlestick.Symbol.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.BTC), firstCandlestick.Symbol.Symbol);
        }
    }
}
