﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class BinanceSpotTests
    {
        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsExchangeSymbolForBTCUSDT()
        {
            IExchangeApiService api = new BinanceSpotApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<ExchangeSymbol> allSymbols = await api.GetSymbolsAsync();
            ExchangeSymbol btcUsdt = allSymbols.First(s => s.Name.Equals("BTCUSDT"));
            Assert.AreEqual(Exchange.Binance, btcUsdt.Exchange);
            Assert.AreEqual("BTCUSDT", (string)btcUsdt.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), btcUsdt.Symbol);
        }

        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsCorrectCandle()
        {
            DateTime from = new DateTime(2020, 12, 17, 16, 30, 30, DateTimeKind.Local).ToUniversalTime();
            DateTime to = new DateTime(2020, 12, 17, 16, 35, 30, DateTimeKind.Local).ToUniversalTime();
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.XRP), Exchange.Binance, "XRPUSDT");

            IExchangeApiService api = new BinanceSpotApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<Candlestick> candlesticks = await api.GetCandlesticksAsync(CandlestickInterval.Minute_1, symbol, from, to, false);

            Assert.AreEqual(6, candlesticks.Count());
            Candlestick lastCandlestick = candlesticks.Last();
            Assert.AreEqual(0.55760m, lastCandlestick.Close);
            Assert.AreEqual(0.55849m, lastCandlestick.Open);
            Assert.AreEqual(new DateTime(2020, 12, 17, 16, 35, 0, DateTimeKind.Local).ToUniversalTime(), lastCandlestick.StartTime);
            Assert.AreEqual(CandlestickInterval.Minute_1, lastCandlestick.Interval);
            Assert.AreEqual(Exchange.Binance, lastCandlestick.Symbol.Exchange);
            Assert.AreEqual("XRPUSDT", (string)lastCandlestick.Symbol.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.XRP), lastCandlestick.Symbol.Symbol);
        }
    }
}
