﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoTax.Domain.Services;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Tests.Mocks;
using System.Threading.Tasks;
using CryptoTax.Shared.Enums;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Services;
using Moq;
using CryptoTax.Shared.Helpers;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.DTO;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class APIHelpersTests
    {
        #region Group Tests
        [TestMethod]
        public void EnforceMaximumGroupSizeWhenEmpty()
        {
            List<List<int>> groups = new List<List<int>>();

            IEnumerable<IEnumerable<int>> result = ApiHelpers.EnforceMaximumGroupSize(groups, 10);

            Assert.IsFalse(result.Any());
        }

        [TestMethod]
        public void EnforceMaximumGroupSizeWhenSmall()
        {
            List<List<int>> groups = new List<List<int>>()
            {
                new List<int>() { 1, 2, 3 },
                new List<int>() {4, 5 }
            };

            IEnumerable<IEnumerable<int>> result = ApiHelpers.EnforceMaximumGroupSize(groups, 10);

            Assert.IsTrue(result.Count() == 2);
            CollectionAssert.AreEqual(new[] { 1, 2, 3 }, result.First().ToArray());
            CollectionAssert.AreEqual(new[] { 4, 5 }, result.Last().ToArray());
        }

        [TestMethod]
        public void EnforceMaximumGroupSizeWhenReachedLimit()
        {
            List<List<int>> groups = new List<List<int>>()
            {
                new List<int>() { 1, 2, 3 },
                new List<int>() { 4, 5 }
            };

            IEnumerable<IEnumerable<int>> result = ApiHelpers.EnforceMaximumGroupSize(groups, 3);

            Assert.IsTrue(result.Count() == 2);
            CollectionAssert.AreEqual(new[] { 1, 2, 3 }, result.First().ToArray());
            CollectionAssert.AreEqual(new[] { 4, 5 }, result.Last().ToArray());
        }

        [TestMethod]
        public void EnforceMaximumGroupSizeWhenTooLarge()
        {
            List<List<int>> groups = new List<List<int>>()
            {
                new List<int>() { 1, 2, 3, 4, 5, 6 },
                new List<int>() { 7, 8, 9, 10 }
            };

            IEnumerable<IEnumerable<int>> result = ApiHelpers.EnforceMaximumGroupSize(groups, 3);

            Assert.IsTrue(result.Count() == 4);
            CollectionAssert.AreEqual(new[] { 1, 2, 3 }, result.ElementAt(0).ToArray());
            CollectionAssert.AreEqual(new[] { 4, 5, 6 }, result.ElementAt(1).ToArray());
            CollectionAssert.AreEqual(new[] { 7, 8, 9 }, result.ElementAt(2).ToArray());
            CollectionAssert.AreEqual(new[] { 10 }, result.ElementAt(3).ToArray());
        }
        #endregion

        #region Blank Candlestick Handling Tests
        private const Exchange _DEFAULT_EXCHANGE = Exchange.Undefined;
        private const CandlestickInterval _DEFAULT_INTERVAL = CandlestickInterval.Minute_1;
        private readonly DateTime _DefaultZerothTime = new DateTime(2021, 1, 1);
        private readonly DString _DefaultBaseUrl = "https://api.example.com/api/v1/";
        private readonly ExchangeSymbol _DefaultSymbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), _DEFAULT_EXCHANGE, "MockPair");
        private readonly Func<int, decimal> _DefaultOpenPrice = (nIntervalFromZerothTime) => nIntervalFromZerothTime;
        private readonly Func<int, decimal> _DefaultClosePrice = (nIntervalFromZerothTime) => nIntervalFromZerothTime + 0.5m;

        [TestMethod]
        public async Task GetCandlesticksAsyncFillsInteriorCandlesticks()
        {
            // two candlesticks in the middle are missing

            (int, int, int[])[] data = new[]
            {
                // candlestick 2 and 3 are missing
                (0, 4,  new[] { 0, 1, 4 })
            };
            DateTime from = this.offsetToTime(0);
            DateTime to = this.offsetToTime(4);

            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 5, false);

            Assert.AreEqual(5, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c1 = candlesticks.ElementAt(1); // have
            Candlestick c2 = candlesticks.ElementAt(2); // missing
            Candlestick c3 = candlesticks.ElementAt(3); // missing
            Candlestick c4 = candlesticks.ElementAt(4); // have
            Assert.AreEqual(this._DefaultClosePrice(1), c1.Close);
            Assert.AreEqual(c1.Close, c2.Open);
            Assert.AreEqual(c1.Close, c2.Close);
            Assert.AreEqual(c1.Close, c3.Open);
            Assert.AreEqual(c1.Close, c3.Close);
            Assert.AreEqual(this._DefaultOpenPrice(4), c4.Open);
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncFillsInTrailingCandlesticks()
        {
            // the candlestick at "to" does not exist

            (int, int, int[])[] data = new[]
            {
                // candlestick 5 is missing
                (0, 4,  new[] { 0, 1, 2, 3 })
            };
            DateTime from = this.offsetToTime(0);
            DateTime to = this.offsetToTime(4);

            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 5, false);

            Assert.AreEqual(5, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c3 = candlesticks.ElementAt(3); // have
            Candlestick c4 = candlesticks.ElementAt(4); // missing
            Assert.AreEqual(this._DefaultClosePrice(3), c3.Close);
            Assert.AreEqual(c3.Close, c4.Open);
            Assert.AreEqual(c3.Close, c4.Close);
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncFillsInLeadingBlankWhenPreviousLotHasCandlestick()
        {
            // the candlestick at "from" does not exist.
            // a candlestick exists at the beginning in the batch before.

            (int, int, int[])[] data = new[]
            {
                // batch 1 has a candlestick at time 0
                (0, 2,  new[] { 0 }),

                // batch 2 is missing candlestick 3
                (3, 5, new[] { 4, 5 })
            };
            DateTime from = this.offsetToTime(3);
            DateTime to = this.offsetToTime(5);

            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 3, false);

            Assert.AreEqual(3, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c3 = candlesticks.ElementAt(0); // missing
            Candlestick c4 = candlesticks.ElementAt(1); // have
            Assert.AreEqual(this._DefaultClosePrice(0), c3.Open);
            Assert.AreEqual(this._DefaultClosePrice(0), c3.Close);
            Assert.AreEqual(this._DefaultOpenPrice(4), c4.Open);
            Assert.AreEqual(this._DefaultClosePrice(4), c4.Close);
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncIgnoresLeadingBlankInPreviousBatch()
        {
            // the candlestick at "from" does not exist.
            // a candlestick exists in the middle in the batch before.
            // make sure service knows not to fill in leading blank (i.e. does not get stuck in infinite loop)

            (int, int, int[])[] data = new[]
            {
                // batch 1 has a candlestick at time 1
                (0, 2,  new[] { 1 }),

                // batch 2 is missing candlestick 3
                (3, 5, new[] { 4, 5 })
            };
            DateTime from = this.offsetToTime(3);
            DateTime to = this.offsetToTime(5);

            ExchangeCache cache = new ExchangeCache();
            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 3, false, cache);

            Assert.AreEqual(5, cache.GetCandlesticks(_DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, this.offsetToTime(0), this.offsetToTime(6)).Count()); // 1 interval padding
            Assert.AreEqual(3, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c3 = candlesticks.ElementAt(0); // missing
            Candlestick c4 = candlesticks.ElementAt(1); // have
            Assert.AreEqual(this._DefaultClosePrice(1), c3.Open);
            Assert.AreEqual(this._DefaultClosePrice(1), c3.Close);
            Assert.AreEqual(this._DefaultOpenPrice(4), c4.Open);
            Assert.AreEqual(this._DefaultClosePrice(4), c4.Close);
        }

        [TestMethod]
        public void GetCandlesticksAsyncFillsInLeadingBlanksWhenPreviousBatchHasNoCandlesticks()
        {
            // the candlestick at "from" does not exist.
            // no candlesticks exist in the batch before.
            // a candlestick does exist in the batch before that.

            (int, int, int[])[] data = new[]
            {
                // batch 1 has a candlestick at time 1
                (0, 2,  new[] { 1 }),

                // batch 2 is missing all candlesticks
                (3, 5, new int[] { }),

                // batch 3 is missing candlestick 6 and 7
                (6, 8, new[] { 8 })
            };
            DateTime from = this.offsetToTime(6);
            DateTime to = this.offsetToTime(8);

            IEnumerable<Candlestick> candlesticks = this.setupServicesAndGetCandlesticks(data, from, to, 3, false).Result;

            Assert.AreEqual(3, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c6 = candlesticks.ElementAt(0); // missing
            Candlestick c7 = candlesticks.ElementAt(1); // missing
            Candlestick c8 = candlesticks.ElementAt(2); // have
            Assert.AreEqual(this._DefaultClosePrice(1), c6.Open);
            Assert.AreEqual(this._DefaultClosePrice(1), c6.Close);
            Assert.AreEqual(this._DefaultClosePrice(1), c7.Open);
            Assert.AreEqual(this._DefaultClosePrice(1), c7.Close);
            Assert.AreEqual(this._DefaultOpenPrice(8), c8.Open);
            Assert.AreEqual(this._DefaultClosePrice(8), c8.Close);
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncFillsInLeadingBlanksWhenCacheHasPreviousCandlestick()
        {
            // the candlestick at "from" does not exist.
            // the cache contains the very previous candlestick

            (int, int, int[])[] data = new[]
            {
                // batch 2 is missing candlestick 3
                (1, 3, new[] { 3 })
            };
            DateTime from = this.offsetToTime(1);
            DateTime to = this.offsetToTime(3);

            ExchangeCache cache = new ExchangeCache();
            cache.SetCandlesticks(_DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, new List<Candlestick>() { new Candlestick(this.offsetToTime(0), _DEFAULT_INTERVAL, this._DefaultSymbol, this._DefaultOpenPrice(0), this._DefaultClosePrice(0)) });
            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 3, false, cache);

            Assert.AreEqual(4, cache.GetCandlesticks(_DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, this.offsetToTime(-1), this.offsetToTime(4)).Count()); // 1 interval padding
            Assert.AreEqual(3, candlesticks.Count());
            Assert.AreEqual(from, candlesticks.First().StartTime);
            Assert.AreEqual(to, candlesticks.Last().StartTime);

            Candlestick c1 = candlesticks.ElementAt(0); // missing
            Candlestick c2 = candlesticks.ElementAt(1); // missing
            Candlestick c3 = candlesticks.ElementAt(2); // have
            Assert.AreEqual(this._DefaultClosePrice(0), c1.Open);
            Assert.AreEqual(this._DefaultClosePrice(0), c1.Close);
            Assert.AreEqual(this._DefaultClosePrice(0), c2.Open);
            Assert.AreEqual(this._DefaultClosePrice(0), c2.Close);
            Assert.AreEqual(this._DefaultOpenPrice(3), c3.Open);
            Assert.AreEqual(this._DefaultClosePrice(3), c3.Close);
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncThrowsNoPricesException()
        {
            (int, int, int[])[] data = new (int, int, int[])[1000];
            for (int i = 0; i < data.Length; i++)
            {
                // candlesticks are missing
                data[i] = (i, i, new int[] { });
            }

            DateTime from = this.offsetToTime(999);
            DateTime to = this.offsetToTime(999);

            await Assert.ThrowsExceptionAsync<NoPriceDataException>(() => this.setupServicesAndGetCandlesticks(data, from, to, 1, false));
        }

        [TestMethod]
        public async Task GetCandlesticksAsyncDoesNotSaveIfGetOnlyTrue()
        {
            // the candlestick at time 0 exists in the cache, but the one at 1 does not.
            // we want to ensure that we can get all candlesticks, but since getOnly is true the cache will still only have a single candlestick at the end.

            (int, int, int[])[] data = new[]
            {
                (1, 1, new[] { 1 })
            };
            DateTime from = this.offsetToTime(0);
            DateTime to = this.offsetToTime(1);

            Mock<IFileService> fileService = new Mock<IFileService>();
            CandlestickPersistorService persistor = new CandlestickPersistorService(fileService.Object, "dataPath");
            ExchangeCache cache = new ExchangeCache(persistor, null, null);
            cache.SetCandlesticks(_DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, new List<Candlestick>() { new Candlestick(this.offsetToTime(0), _DEFAULT_INTERVAL, this._DefaultSymbol, this._DefaultOpenPrice(0), this._DefaultClosePrice(0)) });
            fileService.Verify(fs => fs.WriteFile(It.IsAny<DString>(), It.IsAny<DString>(), It.IsAny<bool>()), Times.Once()); // called to setup
            fileService.Reset();

            IEnumerable<Candlestick> candlesticks = await this.setupServicesAndGetCandlesticks(data, from, to, 1, true, cache);
            fileService.Verify(fs => fs.WriteFile(It.IsAny<DString>(), It.IsAny<DString>(), It.IsAny<bool>()), Times.Never());

            Assert.AreEqual(2, candlesticks.Count());
            Candlestick c1 = candlesticks.ElementAt(0); // have
            Candlestick c2 = candlesticks.ElementAt(1); // missing
            Assert.AreEqual(this._DefaultOpenPrice(0), c1.Open);
            Assert.AreEqual(this._DefaultClosePrice(0), c1.Close);
            Assert.AreEqual(this._DefaultOpenPrice(1), c2.Open);
            Assert.AreEqual(this._DefaultClosePrice(1), c2.Close);
        }

        [TestMethod]
        public async Task GetCandlesticksAsync_SavesMissingCandlesticks()
        {
            // what a sticky mess...
            // -> return data for times 4-5 for first request
            // -> requests will then be made to get leading candlestick data
            // -> always return empty for subsequent requests
            // -> NoDataException will fire
            // -> should call missing service method for time `from` to `this.offsetToTime(3)`
            DateTime from = this.offsetToTime(1);
            DateTime to = this.offsetToTime(5);
            Candlestick c4 = new Candlestick(this.offsetToTime(4), _DEFAULT_INTERVAL, this._DefaultSymbol, 4, 5);
            Candlestick c5 = new Candlestick(this.offsetToTime(5), _DEFAULT_INTERVAL, this._DefaultSymbol, 5.1m, 6);

            Mock<IWebService> webService = new Mock<IWebService>();
            Mock<IExchangeResponsePersistorService> exchangeResponsePersistorService = new Mock<IExchangeResponsePersistorService>();
            Mock<IMissingCandlestickPersistorService> missingCandlestickPersistorService = new Mock<IMissingCandlestickPersistorService>();
            Mock<IRegisteredLogger> registeredLogger = new Mock<IRegisteredLogger>();

            Mock<IExchangeApiAdapter> adapter = new Mock<IExchangeApiAdapter>();
            Dictionary<string, string> firstParams = new Dictionary<string, string>();
            Dictionary<string, string> otherParams = new Dictionary<string, string>();
            object firstResponse = new object();
            object otherResponse = new object();
            adapter.Setup(a => a.CreateCandlestickParameters(_DEFAULT_INTERVAL, this._DefaultSymbol, It.IsAny<IEnumerable<DateTime>>()))
                .Returns<CandlestickInterval, ExchangeSymbol, IEnumerable<DateTime>>((a, b, times) => times.First() == from && times.Last() == to ? firstParams : otherParams);
            adapter.Setup(a => a.ObserveResponse(It.IsAny<Func<Task<dynamic>>>(), It.IsAny<string>(), It.IsAny<Dictionary<string, string>>()))
                .Returns<Func<Task<dynamic>>, string, Dictionary< string, string>>((a, b, param) => 
                    Task.FromResult(param == firstParams ? firstResponse : param == otherParams ? otherResponse : throw new Exception("Unknown params")));
            adapter.Setup(a => a.ParseCandlestickCollection(It.Is<object>(o => o == firstResponse)))
                .Returns(new List<dynamic>() { c4, c5 }); // first request: return candlesticks for times 4-5
            adapter.Setup(a => a.ParseCandlestickCollection(It.Is<object>(o => o == otherResponse)))
                .Returns(new List<dynamic>()); // subsequent requests: return no data
            adapter.Setup(a => a.ParseCandlestick(It.IsAny<It.IsAnyType>(), _DEFAULT_INTERVAL, this._DefaultSymbol))
                .Returns<dynamic, CandlestickInterval, ExchangeSymbol>((a, b, c) => (Candlestick)a); // dynamic object is already of type Candlestick

            // try to get data
            NoPriceDataException e = await Assert.ThrowsExceptionAsync<NoPriceDataException>(() => ApiHelpers.GetCandlesticksAsync(webService.Object, registeredLogger.Object, new ExchangeCache(null, exchangeResponsePersistorService.Object, missingCandlestickPersistorService.Object), _DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, from, to, adapter.Object, 5, this._DefaultBaseUrl, false));

            // check exception parameters
            Assert.IsTrue(e.EarliestTimeChecked < from);
            Assert.AreEqual(this.offsetToTime(4), e.EarliestTimeWithData);
            CollectionAssert.AreEquivalent(new[] { c4, c5 }, e.AvailableDataForRequest.ToArray());

            // make sure missing data was saved
            missingCandlestickPersistorService.Verify(s => s.AddKnownMissingData(_DEFAULT_EXCHANGE, this._DefaultSymbol.Name, e.EarliestTimeChecked.Value, this.offsetToTime(3)));
        }

        [TestMethod]
        public async Task GetCandlesticksAsync_LoadsMissingCandlesticks()
        {
            // -> get data for times 1-5
            // -> according to persistor, data for times 1-3 does not exist: throw exception
            // -> attach data for times 4-5 to exception
            DateTime from = this.offsetToTime(1);
            DateTime to = this.offsetToTime(5);
            Candlestick c4 = new Candlestick(this.offsetToTime(4), _DEFAULT_INTERVAL, this._DefaultSymbol, 4, 5);
            Candlestick c5 = new Candlestick(this.offsetToTime(5), _DEFAULT_INTERVAL, this._DefaultSymbol, 5.1m, 6);

            Mock<IWebService> webService = new Mock<IWebService>();
            Mock<IExchangeResponsePersistorService> exchangeResponsePersistorService = new Mock<IExchangeResponsePersistorService>();
            Mock<IRegisteredLogger> registeredLogger = new Mock<IRegisteredLogger>();
            Mock<IExchangeApiAdapter> adapter = new Mock<IExchangeApiAdapter>();

            Mock<IMissingCandlestickPersistorService> missingCandlestickPersistorService = new Mock<IMissingCandlestickPersistorService>();
            DateTime? unknownDataUntil = this.offsetToTime(3);
            missingCandlestickPersistorService.Setup(s => s.IncludesKnownMissingData(_DEFAULT_EXCHANGE, this._DefaultSymbol.Name, from, to, out unknownDataUntil))
                .Returns(true);

            Mock<ICandlestickPersistorService> candlestickPersistorService = new Mock<ICandlestickPersistorService>();
            candlestickPersistorService.Setup(s => s.HasData(this._DefaultSymbol, _DEFAULT_INTERVAL)).Returns(true);
            candlestickPersistorService.Setup(s => s.ReadCandlesticks(this._DefaultSymbol, _DEFAULT_INTERVAL))
                .Returns(new List<CandlestickDTO>() { c4.ToDTO(), c5.ToDTO() });

            ExchangeCache cache = new ExchangeCache(candlestickPersistorService.Object, exchangeResponsePersistorService.Object, missingCandlestickPersistorService.Object);

            // try to get data
            NoPriceDataException e = await Assert.ThrowsExceptionAsync<NoPriceDataException>(() => ApiHelpers.GetCandlesticksAsync(webService.Object, registeredLogger.Object, cache, _DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, from, to, adapter.Object, 5, this._DefaultBaseUrl, false));

            // check exception parameters
            Assert.AreEqual(from, e.EarliestTimeChecked);
            Assert.AreEqual(this.offsetToTime(4), e.EarliestTimeWithData);
            CollectionAssert.AreEquivalent(new[] { c4.StartTime, c5.StartTime }, e.AvailableDataForRequest.Select(c => c.StartTime).ToArray());
        }

        [TestMethod]
        public async Task GetCandlesticksAsync_ThrowsIfMissingLeadingCandlesticksButOtherCandlesticksNotAlreadyCached()
        {
            // Consider the case where we want to get data for times 1-5. We have already persisted that 1-3 has no data.
            // We still want to complete the NoPriceDataException properties, so we add to those the data for times 4-5.
            // If that data is not already cached, throw an exception. This is because we can assume that this exact
            // request has already been made *before* missing data was cached, meaning that the other data *should* be
            // available. 
            // It is certainly possible to add some kind of recursive behaviour so that we fetch the other data for the
            // requested range, but that will never be used given the current implementation of the application and thus
            // adds unnecessary complexity.
            // Coming to think of it, "other data" will probably never be available anyway - either we have all the data,
            // or none of the data in the requested range. Oh well, too late now.

            // -> get data for times 1-5
            // -> according to persistor, data for times 1-3 does not exist: throw NoPriceDataException
            // -> however, times 4-5 also do not exist - this should throw Exception
            DateTime from = this.offsetToTime(1);
            DateTime to = this.offsetToTime(5);

            Mock<IWebService> webService = new Mock<IWebService>();
            Mock<IExchangeResponsePersistorService> exchangeResponsePersistorService = new Mock<IExchangeResponsePersistorService>();
            Mock<IRegisteredLogger> registeredLogger = new Mock<IRegisteredLogger>();
            Mock<IExchangeApiAdapter> adapter = new Mock<IExchangeApiAdapter>();

            Mock<IMissingCandlestickPersistorService> missingCandlestickPersistorService = new Mock<IMissingCandlestickPersistorService>();
            DateTime? unknownDataUntil = this.offsetToTime(3);
            missingCandlestickPersistorService.Setup(s => s.IncludesKnownMissingData(_DEFAULT_EXCHANGE, this._DefaultSymbol.Name, from, to, out unknownDataUntil))
                .Returns(true);

            Mock<ICandlestickPersistorService> candlestickPersistorService = new Mock<ICandlestickPersistorService>();
            candlestickPersistorService.Setup(s => s.HasData(this._DefaultSymbol, _DEFAULT_INTERVAL)).Returns(false);

            ExchangeCache cache = new ExchangeCache(candlestickPersistorService.Object, exchangeResponsePersistorService.Object, missingCandlestickPersistorService.Object);

            // try to get data
            await Assert.ThrowsExceptionAsync<Exception>(() => ApiHelpers.GetCandlesticksAsync(webService.Object, registeredLogger.Object, cache, _DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, from, to, adapter.Object, 5, this._DefaultBaseUrl, false));
        }

        /// <summary>
        /// Given an array of a request's from and to offset and its corresponding response interval offset array, and a time to offset against, generates the request-response dictionary to be injected into the web service.
        /// Uses default interval, exchange, symbol, and open and close prices.
        /// </summary>
        private Dictionary<DString, DString> createQuickRequestResponseDict(MockExchangeApiAdapter adapter, (int requestFromOffset, int requestToOffset, int[] responseCandlestickOffsets)[] requestResponseData)
        {
            IEnumerable<(DateTime, DateTime, IEnumerable<Candlestick>)> responses = requestResponseData.Select(data =>
            {
                DateTime from = this.offsetToTime(data.requestFromOffset);
                DateTime to = this.offsetToTime(data.requestToOffset);

                int[] offsets = data.responseCandlestickOffsets;
                IEnumerable<Candlestick> response = offsets.Select(n => // map each time in the response into a candlestick using default values
                {
                    DateTime startTime = this.offsetToTime(n);
                    decimal open = this._DefaultOpenPrice(n);
                    decimal close = this._DefaultClosePrice(n);
                    return new Candlestick(startTime, _DEFAULT_INTERVAL, this._DefaultSymbol, open, close);
                });

                return (from, to, response);
            });

            return adapter.GenerateResponseDict(responses, this._DefaultBaseUrl, _DEFAULT_INTERVAL, this._DefaultSymbol);
        }

        /// <summary>
        /// Uses default parameters to determine the start time of the candlestick that has an offset of n intervals from the zeroth time.
        /// </summary>
        private DateTime offsetToTime(int offset)
        {
            return CandlestickHelpers.GetTimeAfterIntervals(_DEFAULT_INTERVAL, this._DefaultZerothTime, offset);
        }

        private async Task<IEnumerable<Candlestick>> setupServicesAndGetCandlesticks((int from, int to, int[] responseCandlestickOffsets)[] requestResponseData, DateTime from, DateTime to, int limit, bool getOnly, ExchangeCache injectedCache = null)
        {
            MockExchangeApiAdapter adapter = new MockExchangeApiAdapter();
            Dictionary<DString, DString> requestResponseDict = this.createQuickRequestResponseDict(adapter, requestResponseData);
            MockWebService webService = new MockWebService(requestResponseDict);
            ExchangeCache cache = injectedCache ?? new ExchangeCache();
            Mock<IRegisteredLogger> logger = new Mock<IRegisteredLogger>();

            IEnumerable<Candlestick> candlesticks = await ApiHelpers.GetCandlesticksAsync(webService, logger.Object, cache, _DEFAULT_EXCHANGE, this._DefaultSymbol, _DEFAULT_INTERVAL, from, to, adapter, limit, this._DefaultBaseUrl, getOnly);
            return candlesticks;
        }
        #endregion
    }
}
