﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoTax.Tests.ExchangeAPI
{
    [TestClass]
    public class BTCMarketsTests
    {
        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsExchangeSymbolForBTCAUD()
        {
            IExchangeApiService api = new BtcMarketsApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<ExchangeSymbol> allSymbols = await api.GetSymbolsAsync();
            ExchangeSymbol btcAud = allSymbols.First(s => s.Name.Equals("BTC-AUD"));
            Assert.AreEqual(Exchange.BTCMarkets, btcAud.Exchange);
            Assert.AreEqual("BTC-AUD", (string)btcAud.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), btcAud.Symbol);
        }

        [TestMethod]
        [TestCategory(Tests.LIVE_API)]
        public async Task APIRequestContainsCorrectCandle()
        {
            DateTime from = new DateTime(2020, 12, 16, 9, 40, 30, DateTimeKind.Local).ToUniversalTime();
            DateTime to = new DateTime(2020, 12, 16, 9, 44, 30, DateTimeKind.Local).ToUniversalTime();
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets, "XRP-AUD");

            IExchangeApiService api = new BtcMarketsApiService(new WebService(), new MockLoggingService(), new ExchangeCache());
            IEnumerable<Candlestick> candlesticks = await api.GetCandlesticksAsync(CandlestickInterval.Minute_1, symbol, from, to, false);

            Assert.AreEqual(5, candlesticks.Count());
            Candlestick lastCandlestick = candlesticks.Last();
            Assert.AreEqual(0.62m, lastCandlestick.Close);
            Assert.AreEqual(0.6198m, lastCandlestick.Open);
            Assert.AreEqual(new DateTime(2020, 12, 15, 23, 44, 0, DateTimeKind.Utc), lastCandlestick.StartTime);
            Assert.AreEqual(CandlestickInterval.Minute_1, lastCandlestick.Interval);
            Assert.AreEqual(Exchange.BTCMarkets, lastCandlestick.Symbol.Exchange);
            Assert.AreEqual("XRP-AUD", (string)lastCandlestick.Symbol.Name);
            Assert.AreEqual(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), lastCandlestick.Symbol.Symbol);
        }
    }
}
