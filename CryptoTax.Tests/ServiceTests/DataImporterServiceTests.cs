﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Services;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class DataImporterServiceTests
    {
        private Mock<IFileService> _FileService;
        private Mock<IExchangeDataLocationService> _ExchangeDataLocationService;
        private Mock<IDtoCachePersistorService> _DtoCachePersistorService;
        private Mock<ICsvDataReaderServiceFactory> _CsvDataReaderServiceFactory;
        private DataImporterService _Service;

        [TestInitialize]
        public void Setup()
        {
            this._FileService = new Mock<IFileService>();
            this._ExchangeDataLocationService = new Mock<IExchangeDataLocationService>();
            this._DtoCachePersistorService = new Mock<IDtoCachePersistorService>();
            this._CsvDataReaderServiceFactory = new Mock<ICsvDataReaderServiceFactory>();

            this._Service = new DataImporterService(this._FileService.Object, this._ExchangeDataLocationService.Object, this._DtoCachePersistorService.Object, this._CsvDataReaderServiceFactory.Object);
        }

        [TestMethod]
        public void ImportAll_CorrectlyGroupsOuterExchangeDataCollections()
        {
            FinancialYear year = FinancialYear.Y2021;

            this._DtoCachePersistorService.Setup(d => d.RequiresRefresh(year)).Returns(true);

            // set up data - one file per group
            IEnumerable<IEnumerable<DString>> data = new List<IEnumerable<DString>>()
            {
                new DString[] { "group1.json" },
                new DString[] { "group2.json" }
            };
            Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> dataDict = new Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>>()
            {
                { SchemaIdentifier.Binance_SpotTrades, data }
            };

            this._ExchangeDataLocationService.Setup(e => e.GetDataSources(Exchange.Binance, year))
                .Returns(dataDict);
            this._ExchangeDataLocationService.Setup(e => e.GetDataSources(It.Is<Exchange>(ex => ex != Exchange.Binance), year))
                .Returns(new Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>>());

            // initialising data reader:
            Mock<ICsvDataReaderService> dataReader = new Mock<ICsvDataReaderService>();
            this._CsvDataReaderServiceFactory.Setup(f =>
                f.Create(this._FileService.Object,
                It.Is<CsvInfo>(c => c.FilePath == "group1.json"),
                It.IsAny<Schema>(),
                year,
                0)
                ).Returns(dataReader.Object);

            // extending data reader:
            dataReader.Setup(r => r.WithAdditionalSource(It.Is<CsvInfo>(c => c.FilePath == "group2.json"), It.IsAny<Schema>(), 1));

            // Moq returns null for IOrderedEnumerable<> for some reason
            dataReader.Setup(r => r.GetTrades()).Returns(new List<TradeDTO>().OrderBy(t => t.Time));
            dataReader.Setup(r => r.GetTransactions()).Returns(new List<TransactionDTO>().OrderBy(t => t.Time));

            // act
            this._Service.ImportAll(year);

            // assert
            this._CsvDataReaderServiceFactory.VerifyAll();
            dataReader.VerifyAll();
        }
    }
}
