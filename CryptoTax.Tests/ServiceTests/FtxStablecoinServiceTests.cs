﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class FtxStablecoinServiceTests
    {
        private static readonly Action NO_OP = () => { };
        private static readonly Func<IEnumerable<CurrencyAmount>> NO_OP1 = () => new List<CurrencyAmount>();

        private IInventory  _Inventory;
        private IdProviderService _IdProviderService;
        private Mock<IReportingService> _ReportingService;
        private MockLoggingService _LoggingService;
        private MockCurrencyConversionService _CurrencyConversionService;
        private FtxStablecoinService _Service;

        private readonly DateTime _Time1 = new DateTime(2021, 9, 24);
        private readonly DateTime _Time2 = new DateTime(2021, 9, 25);
        private readonly decimal _UsdValue1 = 1.3m;
        private readonly decimal _UsdValue2 = 1.5m;

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._ReportingService = new Mock<IReportingService>();
            this._LoggingService = new MockLoggingService();
            this._Inventory = new FILOInventoryFactory().Create(this._IdProviderService, this._ReportingService.Object, this._LoggingService, Exchange.FTX, "Test");
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USD", this._UsdValue2 } });
            this._Service = new FtxStablecoinService(this._IdProviderService, new MockLoggingService(), this._ReportingService.Object, this._CurrencyConversionService);
        }

        [TestMethod]
        public void Withdrawal_Throws_IfTransferNotStablecoinButFeesStablecoin()
        {
            CurrencyAmount withdrawal = new CurrencyAmount(this._IdProviderService, "XRP", 1, 1, this._Time1);
            CurrencyAmount fee = new CurrencyAmount(this._IdProviderService, "BUSD", 1, 1, this._Time1);

            Assert.ThrowsException<Exception>(() => this._Service.OnFtxWithdrawal(this._Inventory, this._Time1, withdrawal, NO_OP1, fee, NO_OP));
        }

        [DataTestMethod]
        [DataRow(100, 0)]
        [DataRow(100, 1.5)]
        [DataRow(148.5, 1.5)]
        [DataRow(150, 0)]
        public void Withdrawal_FullInventory_DoesConversionWithCorrectValue(double withdrawnAmount, double feeAmount)
        {
            const decimal INITIAL_AMOUNT = 150;
            IEnumerable<CurrencyAmount> result = this.callOnFtxWithdrawal(INITIAL_AMOUNT, withdrawnAmount, feeAmount, false);

            // withdrawal successful with updated dollar value
            CurrencyAmount singleResult = result.Single();
            Assert.AreEqual("BUSD", singleResult.Currency.Value);
            Assert.AreEqual((decimal)withdrawnAmount, singleResult.Amount);
            Assert.AreEqual(this._UsdValue2 * (decimal)withdrawnAmount, singleResult.Value);
            Assert.AreEqual(this._Time2, singleResult.TimeModified);

            // mock inventory still has leftover USD funds
            decimal expectedRemaining = INITIAL_AMOUNT - (decimal)withdrawnAmount - (decimal)feeAmount;
            if (expectedRemaining < 0) expectedRemaining = 0;
            decimal remainingInInventory = this._Inventory.GetCurrentInventory().Any() ? this._Inventory.GetCurrentInventory().Single().Value.Single().Amount : 0;
            Assert.AreEqual(expectedRemaining, remainingInInventory);
        }

        [DataTestMethod]
        [DataRow(200, 0)]
        [DataRow(200, 1)]
        [DataRow(151, 1)]
        public void Withdrawal_InsufficientInventory_InjectsCorrectAmount(double withdrawnAmount, double feeAmount)
        {
            const decimal INITIAL_AMOUNT = 150;
            IEnumerable<CurrencyAmount> result = this.callOnFtxWithdrawal(INITIAL_AMOUNT, withdrawnAmount, feeAmount, true);

            Assert.AreEqual(2, result.Count());
            CurrencyAmount valuedResult = result.First();
            CurrencyAmount phantomResult = result.Last();

            Assert.AreEqual("BUSD", valuedResult.Currency.Value);
            Assert.AreEqual(INITIAL_AMOUNT, valuedResult.Amount);
            Assert.AreEqual(this._UsdValue2 * INITIAL_AMOUNT, valuedResult.Value);
            Assert.AreEqual(this._Time2, valuedResult.TimeModified);

            Assert.AreEqual("BUSD", phantomResult.Currency.Value);
            Assert.AreEqual((decimal)withdrawnAmount - INITIAL_AMOUNT, phantomResult.Amount);
            Assert.AreEqual(0, phantomResult.Value);
            Assert.AreEqual(this._Time2, phantomResult.TimeModified);

            // mock inventory still has leftover USD funds
            decimal remainingInInventory = this._Inventory.GetCurrentInventory().Any() ? this._Inventory.GetCurrentInventory().Single().Value.Single().Amount : 0;
            Assert.AreEqual(0, remainingInInventory);
        }

        [TestMethod]
        public void Deposit_Throws_IfTransferNotStablecoinButFeesStablecoin()
        {
            List<CurrencyAmount> deposit = new List<CurrencyAmount>() { new CurrencyAmount(this._IdProviderService, "XRP", 1, 1, this._Time1) };
            CurrencyAmount fee = new CurrencyAmount(this._IdProviderService, "BUSD", 1, 1, this._Time1);

            Assert.ThrowsException<Exception>(() => this._Service.OnFtxDeposit(this._Inventory, this._Time1, deposit, NO_OP, fee, NO_OP));
        }

        [TestMethod]
        public void Deposit_ConvertsDepositedAmounts()
        {
            CurrencyAmount deposit = new CurrencyAmount(this._IdProviderService, "BUSD", 100, 100 * _UsdValue1, this._Time1);
            Action doDeposit = () => this._Inventory.TransferIn(deposit, this._Time1);

            CurrencyAmount fee = new CurrencyAmount(this._IdProviderService, "BUSD", 1, 0, this._Time1);
            Action payFee = () =>
            {
                this._Inventory.TransferOut(fee, TransferOutReason.Burn, out decimal overdrawn);
                if (overdrawn > 0) throw new Exception($"Fee overdrawn should be zero");
            };

            // act
            this._Service.OnFtxDeposit(this._Inventory, this._Time1, new List<CurrencyAmount>() { deposit }, doDeposit, fee, payFee);

            // assert
            CurrencyAmount inventoryAmount = this._Inventory.GetCurrentInventory().Single().Value.Single();
            Assert.AreEqual("USD", inventoryAmount.Currency.Value);
            Assert.AreEqual(99, inventoryAmount.Amount);
            Assert.AreEqual(99 * _UsdValue2, inventoryAmount.Value);
            Assert.AreEqual(_Time1, inventoryAmount.TimeModified);

            decimal expectedGains = 99 * _UsdValue2 - 99 * _UsdValue1;
            this._ReportingService.Verify(r => r.AddData(It.Is<ReportingRemoveData>(d => d.GetCapitalGainsDelta().HasValue && d.GetCapitalGainsDelta().Value.StandardCapitalGains == expectedGains), false));
        }

        private IEnumerable<CurrencyAmount> callOnFtxWithdrawal(decimal initialAmount, double withdrawnAmount, double feeAmount, bool shouldOverdraw)
        {
            this._Inventory.TransferIn(new CurrencyAmount(this._IdProviderService, "USD", initialAmount, initialAmount * this._UsdValue1, this._Time1), this._Time1);

            CurrencyAmount withdrawal = new CurrencyAmount(this._IdProviderService, "BUSD", (decimal)withdrawnAmount, 0, this._Time2);
            Func<IEnumerable<CurrencyAmount>> doWithdrawal = () =>
            {
                IEnumerable<CurrencyAmount> transferred = this._Inventory.TransferOut(withdrawal, TransferOutReason.Transfer, out decimal overdrawn);
                if (overdrawn > 0 != shouldOverdraw) throw new Exception($"Withdrawal overdrawn should be {shouldOverdraw}");
                return transferred;
            };

            CurrencyAmount fee = new CurrencyAmount(this._IdProviderService, "BUSD", (decimal)feeAmount, 0, this._Time2);
            Action payFee = () =>
            {
                if (feeAmount > 0)
                {
                    this._Inventory.TransferOut(fee, TransferOutReason.Burn, out decimal overdrawn);
                    if (overdrawn > 0 != shouldOverdraw) throw new Exception($"Fee overdrawn should be {shouldOverdraw}");
                }
            };

            // act
            IEnumerable<CurrencyAmount> result = this._Service.OnFtxWithdrawal(this._Inventory, this._Time2, withdrawal, doWithdrawal, feeAmount == 0 ? (CurrencyAmount?)null : fee, payFee);

            return result;
        }
    }
}
