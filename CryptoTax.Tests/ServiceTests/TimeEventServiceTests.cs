﻿using CryptoTax.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class TimeEventServiceTests
    {
        private TimeEventService _TimeEventService;
        private readonly DateTime _Time1 = new DateTime(2021, 1, 1);
        private readonly DateTime _Time2 = new DateTime(2021, 1, 2);
        private readonly DateTime _Time3 = new DateTime(2021, 1, 3);
        private readonly DateTime _Time4 = new DateTime(2021, 1, 4);
        private readonly DateTime _Time5 = new DateTime(2021, 1, 5);
        private readonly DateTime _Time6 = new DateTime(2021, 1, 6);
        private readonly DateTime _Time7 = new DateTime(2021, 1, 7);

        [TestInitialize]
        public void Setup()
        {
            this._TimeEventService = new TimeEventService();
        }

        [TestMethod]
        public void ExceptionWhenPreviousTimeComesAfterCurrentTime()
        {
            Assert.ThrowsException<ArgumentException>(() => this._TimeEventService.OnItemWillProcess(this._Time2, this._Time1));
        }

        [TestMethod]
        public void SimpleActionExecutesOnceAtCorrectTime()
        {
            int actionExecuted = 0;
            this._TimeEventService.AddAction(() => actionExecuted++, this._Time3, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time2, this._Time4);
            Assert.AreEqual(1, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time4, this._Time5);
            Assert.AreEqual(1, actionExecuted);

            this._TimeEventService.OnLastItemDidProcess(this._Time5);
            Assert.AreEqual(1, actionExecuted);
        }

        [TestMethod]
        public void ExecutionAtBeginningWorks()
        {
            int actionExecuted = 0;
            this._TimeEventService.AddAction(() => actionExecuted++, this._Time1, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time2);
            Assert.AreEqual(1, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time2, this._Time3);
            Assert.AreEqual(1, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time3, this._Time4);
            Assert.AreEqual(1, actionExecuted);
        }

        [TestMethod]
        public void EdgeCaseWhithExecutionTimeSameAsProcessTime()
        {
            int actionExecuted = 0;
            this._TimeEventService.AddAction(() => actionExecuted++, this._Time2, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            Assert.AreEqual(1, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time2, this._Time3);
            Assert.AreEqual(1, actionExecuted);
        }

        [TestMethod]
        public void CorrectlyExecutesAfterLastIfSetToTrue()
        {
            int actionExecuted = 0;
            this._TimeEventService.AddAction(() => actionExecuted++, this._Time3, true);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnLastItemDidProcess(this._Time2);
            Assert.AreEqual(1, actionExecuted);
        }

        [TestMethod]
        public void NeverExecutesAfterLastIfSetToFalse()
        {
            int actionExecuted = 0;
            this._TimeEventService.AddAction(() => actionExecuted++, this._Time3, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnLastItemDidProcess(this._Time2);
            Assert.AreEqual(0, actionExecuted);
        }


        [TestMethod]
        public void ActionsExecutedInCorrectOrder()
        {
            List<int> actionsExecuted = new List<int>();
            this._TimeEventService.AddAction(() => actionsExecuted.Add(1), this._Time3, false);
            this._TimeEventService.AddAction(() => actionsExecuted.Add(2), this._Time4, false);
            this._TimeEventService.AddAction(() => actionsExecuted.Add(3), this._Time2, false);

            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionsExecuted.Count);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time5);
            Assert.AreEqual(3, actionsExecuted.Count);

            this._TimeEventService.OnLastItemDidProcess(this._Time5);
            Assert.AreEqual(3, actionsExecuted.Count);
            CollectionAssert.AreEqual(new[] { 3, 1, 2 }, actionsExecuted);
        }

        [TestMethod]
        public void CanRemoveActionAndWillNoLongerExecute()
        {
            int actionExecuted = 0;
            long id1 = this._TimeEventService.AddAction(() => actionExecuted--, this._Time2, false);
            long id2 = this._TimeEventService.AddAction(() => actionExecuted++, this._Time2, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            bool removed = this._TimeEventService.TryRemoveAction(id1);
            Assert.IsTrue(removed);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time3);

            // make sure second transaction still executed
            Assert.AreEqual(1, actionExecuted);
        }

        [TestMethod]
        public void CannotRemoveActionAfterAlreadyExecuted()
        {
            int actionExecuted = 0;
            long id = this._TimeEventService.AddAction(() => actionExecuted++, this._Time2, false);

            Assert.AreEqual(0, actionExecuted);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            Assert.AreEqual(0, actionExecuted);

            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            Assert.AreEqual(1, actionExecuted);

            bool removed = this._TimeEventService.TryRemoveAction(id);
            Assert.IsFalse(removed);
        }

        [TestMethod]
        public void CannotRemoveActionAfterAlreadyRemoved()
        {
            int actionExecuted = 0;
            long id1 = this._TimeEventService.AddAction(() => actionExecuted--, this._Time1, false);

            bool removed = this._TimeEventService.TryRemoveAction(id1);
            Assert.IsTrue(removed);

            removed = this._TimeEventService.TryRemoveAction(id1);
            Assert.IsFalse(removed);
        }
    }
}
