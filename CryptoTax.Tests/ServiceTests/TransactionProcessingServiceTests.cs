﻿using CryptoTax.Domain;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;
using System.Linq;
using System.Threading.Tasks;
using CryptoTax.Shared;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Stores;
using CryptoTax.Domain.Models;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Inventory;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class TransactionProcessingServiceTests
    {
        private IdProviderService _IdService;
        private ILoggingService _LoggingService;
        private TransferService _TransferService;
        private MockTrackingReportingService _ReportingService;
        private TimeEventService _TimeEventService;
        private ICurrencyConversionService _CurrencyConversionService;
        private MockTrackingInventory _MockInventory;
        private AssessableIncomeStore _AssessableIncomeStore;
        private Mock<IBulkCandlestickService> _BulkCandlestickService;
        private Mock<ITransactionWhitelistService> _TransactionWhitelistService;
        private Mock<IFtxStablecoinService> _FtxStablecoinService;

        private readonly DateTime _Time0 = new DateTime(2021, 2, 4, 2, 0, 0);
        private readonly DateTime _Time1 = new DateTime(2021, 5, 20, 6, 0, 0);
        private readonly DateTime _Time2 = new DateTime(2021, 5, 20, 6, 15, 0);
        private readonly Container _Container = Container.FromExchange(Exchange.BTCMarkets);

        [TestInitialize]
        public void Setup()
        {
            this._IdService = new IdProviderService(0);
            this._LoggingService = new MockLoggingService();
            this._TransactionWhitelistService = new Mock<ITransactionWhitelistService>();
            this._TransactionWhitelistService.Setup(w => w.VerifyWhitelist(It.IsAny<TransactionDTO>())).Returns(true);
            this._TransferService = new TransferService(this._IdService, this._TransactionWhitelistService.Object, this._LoggingService);
            this._ReportingService = new MockTrackingReportingService();
            this._TimeEventService = new TimeEventService();
            this._CurrencyConversionService = new MockCurrencyConversionService();
            this._MockInventory = new MockTrackingInventory();
            this._AssessableIncomeStore = new AssessableIncomeStore();
            this._BulkCandlestickService = new Mock<IBulkCandlestickService>();
            this._FtxStablecoinService = new Mock<IFtxStablecoinService>();
        }

        #region candlestick setup tests
        [TestMethod]
        public void Setup_RequestsCorrectCandlestickData()
        {
            DateTime time = DateTime.Now;
            ContainerEventDTO containerEvent = new ContainerEventDTO(time, Container.FromExchange(Exchange.Binance), null, null, "USDT", 500, null, null);
            TransactionDTO tx = new TransactionDTO(time, Exchange.Binance, "internal", "public", null, TransactionType.FuturesPnL, containerEvent);

            TransactionProcessingService service = this.constructProcessingService();
            service.Setup(new List<TransactionDTO>() { tx });

            this._BulkCandlestickService.Verify(b => b.AddRequired(new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance), time));
            this._BulkCandlestickService.Verify(b => b.AddRequired(new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), Exchange.BTCMarkets), time));
            this._BulkCandlestickService.VerifyNoOtherCalls();
        }
        #endregion

        #region exception tests
        [TestMethod]
        public async Task ThrowsExceptionIfSetupNotCalled()
        {
            await Assert.ThrowsExceptionAsync<TransactionSetupException>(() => this.constructProcessingService().ProcessTransactionAsync(null));
        }

        [TestMethod]
        public void ThrowsExceptionIfSetupCalledAgain()
        {
            TransactionProcessingService service = this.constructProcessingService();
            service.Setup(new List<TransactionDTO>());
            Assert.ThrowsException<TransactionSetupException>(() => service.Setup(new List<TransactionDTO>()));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenTransactionIncomplete()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, "XRP", null, null, null, null, null))));
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, 1, null, null, null, null))));
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", null, null, null))));
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, null, 1, null, null))));
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", 1, "XRP", null))));
            await Assert.ThrowsExceptionAsync<IncompleteTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", 1, null, 1))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenTransactionNotOnOneSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<TransactionSideException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, null, null, null, null))));
            await Assert.ThrowsExceptionAsync<TransactionSideException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, "XRP", 1, "BTC", 1, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenAmountIsLessThanZero()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<TransactionOutOfRangeException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, "XRP", -1, null, null, null, null))));
            await Assert.ThrowsExceptionAsync<TransactionOutOfRangeException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FuturesFunding, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", -1, null, null))));
            await Assert.ThrowsExceptionAsync<TransactionOutOfRangeException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.CryptoDeposit, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", 1, "XRP", -1))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenStakingTransactionOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidStakingSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.StakingOrAirdrops, new ContainerEventDTO(this._Time1, this._Container, "XRP", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenForkTransactionOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidForkSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.Fork, new ContainerEventDTO(this._Time1, this._Container, "XRP", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenLoanInterestTransactionOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidLoanSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.LoanInterest, new ContainerEventDTO(this._Time1, this._Container, "XRP", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenFiatDepositTransactionOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidFiatSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FiatDeposit, new ContainerEventDTO(this._Time1, this._Container, "AUD", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenFiatDepositIsNotAUD()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidFiatCurrencyTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FiatDeposit, new ContainerEventDTO(this._Time1, this._Container, null, null, "USD", 1, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenFiatWithdrawalTransactionOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidFiatSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FiatWithdrawal, new ContainerEventDTO(this._Time1, this._Container, null, null, "AUD", 1, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenFiatWithdrawalIsNotAUD()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidFiatCurrencyTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.FiatWithdrawal, new ContainerEventDTO(this._Time1, this._Container, "USD", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenUnknownCryptoDepositOnNegativeSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidDepositSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.CryptoDeposit, new ContainerEventDTO(this._Time1, this._Container, "XRP", 1, null, null, null, null))));
        }

        [TestMethod]
        public async Task ThrowsExceptionWhenUnknownCryptoWithdrawalOnPositiveSide()
        {
            TransactionProcessingService service = this.constructAndSetupProcessingService();
            await Assert.ThrowsExceptionAsync<InvalidWithdrawalSideTransactionException>(() => service.ProcessTransactionAsync(new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal", "public", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", 1, null, null))));
        }
        #endregion

        #region transfer tests
        [TestMethod]
        public void InstantTransfer1SkippedIfInventoriesSameNoFees()
        {
            // single inventory mode.
            IEnumerable<TransactionDTO> txs = this.getTransfer1Transactions(true);
            Mock<IInventory> inventory = new Mock<IInventory>();
            inventory.SetupGet(i => i.Id).Returns("id1");

            TransactionProcessingService service = this.constructProcessingService(new MockInventoryManagerService(inventory.Object));
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time1);

            inventory.VerifyGet(i => i.Id);
            inventory.VerifyNoOtherCalls();
            ReportingTransferData data = this._ReportingService.AddedData.Single() as ReportingTransferData;
            Assert.IsNotNull(data);
            Assert.AreEqual(this._Time1, data.Time);
            Assert.AreEqual("id1", data.From.Id.Value);
            Assert.AreEqual("id1", data.To.Id.Value);
            Assert.AreEqual(TimeSpan.Zero, data.Duration);
            Assert.AreEqual(new CurrencyAmount(this._IdService, "XRP", 100, 0, this._Time1), data.CurrencyAmount);
        }

        [TestMethod]
        public void InstantTransfer1SkippedIfInventoriesSameButWithdrawalFeesProcessed()
        {
            // single inventory mode. must still subtract fees accordingly.
            IEnumerable<TransactionDTO> txs = this.getTransfer1Transactions(true, sendingFeeCurrency: "XRP", sendingFeeAmount: 1);
            Mock<IInventory> inventory = new Mock<IInventory>();
            inventory.Setup(i => i.Id).Returns("id1");
            CurrencyAmount transferredAmount = new CurrencyAmount(this._IdService, "XRP", 100, 0, this._Time1);
            CurrencyAmount feeAmount = new CurrencyAmount(this._IdService, "XRP", 1, 0, this._Time1);
            decimal overdrawnAmount = 0;
            inventory.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Burn, out overdrawnAmount)).Returns(new List<CurrencyAmount>()); // return value is never used

            TransactionProcessingService service = this.constructProcessingService(new MockInventoryManagerService(inventory.Object));
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time1);

            //inventory.Verify(i => i.TransferOut(transferredAmount, TransferOutReason.Transfer, out overdrawnAmount));
            inventory.VerifyGet(i => i.Id);
            inventory.Verify(i => i.TransferOut(feeAmount, TransferOutReason.Burn, out overdrawnAmount), Times.Once());
            inventory.VerifyNoOtherCalls();

            Assert.AreEqual(2, this._ReportingService.AddedData.Count());
            ReportingTransferData transferData = this._ReportingService.AddedData.ElementAt(0) as ReportingTransferData; // already verified this in previous test
            Assert.IsNotNull(transferData);
            ReportingBurnData burnData = this._ReportingService.AddedData.ElementAt(1) as ReportingBurnData;
            Assert.IsNotNull(burnData);
            Assert.AreEqual(this._Time1, burnData.Time);
            Assert.AreEqual(BurnReason.Transfer, burnData.Reason);
            Assert.AreEqual(transferredAmount, burnData.RelevantCA);
            Assert.AreEqual(feeAmount, burnData.BurnedAmount);
        }

        [TestMethod]
        public void Transfer1FromExchangeToExchangeNoFeesWorksCorrectly()
        {
            decimal overdrawnAmount = 0;
            CurrencyAmount transferredAmount = new CurrencyAmount(this._IdService, "XRP", 100, 0, this._Time1);
            IEnumerable<CurrencyAmount> transferOutResult = new List<CurrencyAmount>()
            {
                // these are the contents of the inventory
                transferredAmount.WithAmount(30, this._Time0),
                transferredAmount.WithAmount(70, this._Time0)
            };
            IEnumerable<TransactionDTO> txs = this.getTransfer1Transactions(false);
            Mock<IInventory> inv1 = new Mock<IInventory>();
            inv1.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Transfer, out overdrawnAmount)).Returns(transferOutResult);
            inv1.SetupGet(i => i.Id).Returns("id1");
            Mock<IInventory> inv2 = new Mock<IInventory>();
            inv2.SetupGet(i => i.Id).Returns("id2");
            MockInventoryManagerService inventoryManager = new MockInventoryManagerService((container) => container.Exchange.Value == Exchange.BTCMarkets ? inv1.Object : inv2.Object);
            TransactionProcessingService service = this.constructProcessingService(inventoryManager);
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time1);

            inv1.VerifyGet(i => i.Id);
            inv1.Verify(i => i.TransferOut(transferredAmount, TransferOutReason.Transfer, out overdrawnAmount), Times.Once());
            inv1.VerifyNoOtherCalls();
            inv2.VerifyGet(i => i.Id);
            inv2.Verify(i => i.TransferIn(transferOutResult.ElementAt(0), this._Time2), Times.Once());
            inv2.Verify(i => i.TransferIn(transferOutResult.ElementAt(1), this._Time2), Times.Once());
            inv2.VerifyNoOtherCalls();
            ReportingTransferData data = this._ReportingService.AddedData.Single() as ReportingTransferData;
            Assert.IsNotNull(data);
            Assert.AreEqual(this._Time1, data.Time);
            Assert.AreEqual("id1", data.From.Id.Value);
            Assert.AreEqual("id2", data.To.Id.Value);
            Assert.AreEqual(this._Time2.Subtract(this._Time1), data.Duration);
            Assert.AreEqual(transferredAmount, data.CurrencyAmount);
        }

        [TestMethod]
        public void Transfer1FromExchangeToExchangeWithFullFeesWorksCorrectly()
        {
            decimal overdrawnAmount = 0;
            CurrencyAmount transferredAmount = new CurrencyAmount(this._IdService, "XRP", 100, 0, this._Time1);
            CurrencyAmount sendingFeeAmount = new CurrencyAmount(this._IdService, "AUD", 1, 0, this._Time1);
            CurrencyAmount receivingFeeAmount = new CurrencyAmount(this._IdService, "XRP", 1, 0, this._Time2);

            IEnumerable<CurrencyAmount> transferOutResult = new List<CurrencyAmount>()
            {
                // these are the contents of the inventory
                transferredAmount.WithAmount(30, this._Time0),
                transferredAmount.WithAmount(70, this._Time0)
            };
            IEnumerable<TransactionDTO> txs = this.getTransfer1Transactions(false, sendingFeeAmount.Currency, sendingFeeAmount.Amount, receivingFeeAmount.Currency, receivingFeeAmount.Amount);
            Mock<IInventory> inv1 = new Mock<IInventory>();
            inv1.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Transfer, out overdrawnAmount)).Returns(transferOutResult);
            inv1.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Burn, out overdrawnAmount));
            inv1.SetupGet(i => i.Id).Returns("id1");
            Mock<IInventory> inv2 = new Mock<IInventory>();
            inv1.Setup(i => i.TransferIn(It.IsAny<CurrencyAmount>(), It.IsAny<DateTime>()));
            inv1.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Burn, out overdrawnAmount));
            inv2.SetupGet(i => i.Id).Returns("id2");
            MockInventoryManagerService inventoryManager = new MockInventoryManagerService((container) => container.Exchange.Value == Exchange.BTCMarkets ? inv1.Object : inv2.Object);
            TransactionProcessingService service = this.constructProcessingService(inventoryManager);
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time1);

            inv1.VerifyGet(i => i.Id);
            inv1.Verify(i => i.TransferOut(transferredAmount, TransferOutReason.Transfer, out overdrawnAmount), Times.Once());
            inv1.Verify(i => i.TransferOut(sendingFeeAmount, TransferOutReason.Burn, out overdrawnAmount), Times.Once());
            inv1.VerifyNoOtherCalls();
            inv2.VerifyGet(i => i.Id);
            inv2.Verify(i => i.TransferIn(transferOutResult.ElementAt(0), this._Time2), Times.Once());
            inv2.Verify(i => i.TransferIn(transferOutResult.ElementAt(1), this._Time2), Times.Once());
            inv2.Verify(i => i.TransferOut(receivingFeeAmount, TransferOutReason.Burn, out overdrawnAmount), Times.Once());
            inv2.VerifyNoOtherCalls();

            ReportingTransferData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingTransferData;
            Assert.IsNotNull(data1);
            Assert.AreEqual(this._Time1, data1.Time);
            Assert.AreEqual("id1", data1.From.Id.Value);
            Assert.AreEqual("id2", data1.To.Id.Value);
            Assert.AreEqual(this._Time2.Subtract(this._Time1), data1.Duration);
            Assert.AreEqual(transferredAmount, data1.CurrencyAmount);

            ReportingBurnData data2 = this._ReportingService.AddedData.ElementAt(1) as ReportingBurnData;
            Assert.IsNotNull(data2);
            Assert.AreEqual(this._Time1, data2.Time);
            Assert.AreEqual(BurnReason.Transfer, data2.Reason);
            Assert.AreEqual(transferredAmount, data2.RelevantCA);
            Assert.AreEqual(sendingFeeAmount, data2.BurnedAmount);

            ReportingBurnData data3 = this._ReportingService.AddedData.ElementAt(2) as ReportingBurnData;
            Assert.IsNotNull(data3);
            Assert.AreEqual(this._Time2, data3.Time);
            Assert.AreEqual(BurnReason.Transfer, data3.Reason);
            Assert.AreEqual(transferredAmount, data3.RelevantCA);
            Assert.AreEqual(receivingFeeAmount, data3.BurnedAmount);
        }

        [TestMethod]
        public void FtxDeposit_UsesStablecoinService()
        {
            ContainerEventDTO withdrawalEvent = new ContainerEventDTO(this._Time1, Container.FromExchange(Exchange.Binance), "BUSD", 100, null, null, null, null);
            ContainerEventDTO depositEvent = new ContainerEventDTO(this._Time2, Container.FromExchange(Exchange.FTX), null, null, "BUSD", 100, "BUSD", 1);
            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(this._Time1, Exchange.Binance, "binance tx", "tx1", null, TransactionType.CryptoWithdrawal, withdrawalEvent),
                new TransactionDTO(this._Time2, Exchange.FTX, "ftx tx", "tx1", null, TransactionType.CryptoDeposit, depositEvent)
            };
            List<CurrencyAmount> transfer = new List<CurrencyAmount>()
            {
                new CurrencyAmount(this._IdService, "BUSD", 25, 25, this._Time1, Exchange.Binance),
                new CurrencyAmount(this._IdService, "BUSD", 75, 75, this._Time1, Exchange.Binance)
            };

            Mock<IInventory> binanceMockInventory = new Mock<IInventory>();
            decimal _ = 0;
            binanceMockInventory.Setup(i => i.TransferOut(It.IsAny<CurrencyAmount>(), TransferOutReason.Transfer, out _)).Returns(transfer);
            Mock<IInventory> ftxMockInvetory = new Mock<IInventory>();
            Mock<IInventoryManagerService> inventoryManagerService = new Mock<IInventoryManagerService>();
            inventoryManagerService.Setup(i => i.GetInventory(It.Is<Container>(c => c.Exchange == Exchange.Binance))).Returns(binanceMockInventory.Object);
            inventoryManagerService.Setup(i => i.GetInventory(It.Is<Container>(c => c.Exchange == Exchange.FTX))).Returns(ftxMockInvetory.Object);

            TransactionProcessingService service = this.constructProcessingService(inventoryManagerService.Object);
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time2);

            this._FtxStablecoinService.Verify(f => f.OnFtxDeposit(ftxMockInvetory.Object, this._Time2, transfer, It.IsAny<Action>(), It.Is<CurrencyAmount>(c => c.Currency == "BUSD" && c.Amount == 1), It.IsAny<Action>()));
            // don't test the callback methods, as we can safely assume that they work fine (since none of the other tests here are failing, and they use the same methods)
        }

        [TestMethod]
        public void FtxWithdrawal_UsesStablecoinService()
        {
            ContainerEventDTO withdrawalEvent = new ContainerEventDTO(this._Time1, Container.FromExchange(Exchange.FTX), "BUSD", 100, null, null, "BUSD", 1);
            ContainerEventDTO depositEvent = new ContainerEventDTO(this._Time2, Container.FromExchange(Exchange.Binance), null, null, "BUSD", 100, null, null);
            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(this._Time1, Exchange.FTX, "ftx tx", "tx1", null, TransactionType.CryptoWithdrawal, withdrawalEvent),
                new TransactionDTO(this._Time2, Exchange.Binance, "binance tx", "tx1", null, TransactionType.CryptoDeposit, depositEvent)
            };
            List<CurrencyAmount> transfer = new List<CurrencyAmount>()
            {
                new CurrencyAmount(this._IdService, "BUSD", 25, 25, this._Time1, Exchange.Binance),
                new CurrencyAmount(this._IdService, "BUSD", 75, 75, this._Time1, Exchange.Binance)
            };

            Mock<IInventory> binanceMockInventory = new Mock<IInventory>();
            binanceMockInventory.Setup(i => i.TransferIn(It.Is<CurrencyAmount>(c => c.Id == transfer.ElementAt(0).Id), this._Time2));
            binanceMockInventory.Setup(i => i.TransferIn(It.Is<CurrencyAmount>(c => c.Id == transfer.ElementAt(1).Id), this._Time2));
            Mock<IInventory> ftxMockInvetory = new Mock<IInventory>();
            Mock<IInventoryManagerService> inventoryManagerService = new Mock<IInventoryManagerService>();
            inventoryManagerService.Setup(i => i.GetInventory(It.Is<Container>(c => c.Exchange == Exchange.FTX))).Returns(ftxMockInvetory.Object);
            inventoryManagerService.Setup(i => i.GetInventory(It.Is<Container>(c => c.Exchange == Exchange.Binance))).Returns(binanceMockInventory.Object);

            this._FtxStablecoinService.Setup(f => f.OnFtxWithdrawal(ftxMockInvetory.Object, this._Time1, It.Is<CurrencyAmount>(c => c.Currency == "BUSD" && c.Amount == 100), It.IsAny<Func<IEnumerable<CurrencyAmount>>>(), It.Is<CurrencyAmount>(c => c.Currency == "BUSD" && c.Amount == 1), It.IsAny<Action>())).Returns(transfer);

            TransactionProcessingService service = this.constructProcessingService(inventoryManagerService.Object);
            service.Setup(txs);
            this._TimeEventService.OnItemWillProcess(null, this._Time1);
            service.ProcessTransactionAsync(txs.ElementAt(0)).Wait();
            this._TimeEventService.OnItemWillProcess(this._Time1, this._Time2);
            service.ProcessTransactionAsync(txs.ElementAt(1)).Wait();
            this._TimeEventService.OnLastItemDidProcess(this._Time2);

            this._FtxStablecoinService.VerifyAll();
            binanceMockInventory.VerifyAll();
            // don't test the callback methods, as we can safely assume that they work fine (since none of the other tests here are failing, and they use the same methods)
        }
        #endregion

        #region transaction tests
        [TestMethod]
        public void PositiveFuturesFundingWorksCorrectly()
        {
            // positive PNL equivalent to purchasing asset at the current value
            decimal usdtPrice = 1.3m;
            decimal usdtAmount = 0.1m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", usdtPrice } });
            decimal value = usdtAmount * usdtPrice;

            this.positiveTransactionTest(Exchange.Binance, TransactionType.FuturesFunding, "USDT", usdtAmount, value, CreateReason.FuturesFundingIncome);
        }

        [TestMethod]
        public void NegativeFuturesFundingWorksCorrectly()
        {
            // negative funding is equivalent to selling asset at the current value.
            decimal usdtPrice = 1.3m;
            decimal usdtAmount = 0.05m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", usdtPrice } });
            decimal value = usdtAmount * usdtPrice;
            this.negativeTransactionTest_Disposal(Exchange.Binance, TransactionType.FuturesFunding, "USDT", usdtAmount, value, DisposalType.FuturesFundingPayment);
        }

        [TestMethod]
        public void PositiveFuturesPnLWorksCorrectly()
        {
            // positive PNL equivalent to purchasing asset at the current value
            decimal btcPrice = 55000;
            decimal btcAmount = 0.015m;
            decimal fee = btcAmount * 0.01m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "BTC", btcPrice } });
            decimal value = btcAmount * btcPrice;
            this.positiveTransactionTest(Exchange.Binance, TransactionType.FuturesPnL, "BTC", btcAmount, value, CreateReason.FuturesPnlIncome, "BTC", fee, BurnReason.FuturesTradeFee);
        }

        [TestMethod]
        public void NegativeFuturesPnLWorksCorrectly()
        {
            // negative PNL: sell asset at the current value.
            decimal usdtPrice = 1.3m;
            decimal usdtAmount = 105.1m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", usdtPrice } });
            decimal value = usdtAmount * usdtPrice;
            this.negativeTransactionTest_Disposal(Exchange.Binance, TransactionType.FuturesPnL, "USDT", usdtAmount, value, DisposalType.FuturesPnlLoss, "USDT", 1, BurnReason.FuturesTradeFee);
        }
        
        [TestMethod]
        public void PositiveOptionsPnLWorksCorrectly()
        {
            // positive PNL equivalent to purchasing asset at the current value
            decimal usdtPrice = 1.3m;
            decimal usdtAmount = 60;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", usdtPrice } });
            decimal value = usdtAmount * usdtPrice;
            this.positiveTransactionTest(Exchange.Binance, TransactionType.OptionsPnL, "USDT", usdtAmount, value, CreateReason.OptionsPnlIncome);
        }

        [TestMethod]
        public void NegativeOptionsPnLWorksCorrectly()
        {
            // negative PNL: sell asset at the current value.
            decimal usdtPrice = 1.3m;
            decimal usdtAmount = 90m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", usdtPrice } });
            decimal value = usdtAmount * usdtPrice;
            this.negativeTransactionTest_Disposal(Exchange.Binance, TransactionType.OptionsPnL, "USDT", usdtAmount, value, DisposalType.OptionsPnlLoss);
        }

        [TestMethod]
        public void PositiveStakingOrAirdropWorksCorrectly()
        {
            decimal npxsPrice = 0.001m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "NPXS", npxsPrice } });
            decimal value = 500000 * npxsPrice;
            this.positiveTransactionTest(Exchange.Binance, TransactionType.StakingOrAirdrops, "NPXS", 500000, value, CreateReason.StakingIncome, assessableIncomeDelta: value);
        }

        [TestMethod]
        public void StakingOrAirdropWithNoPriceWorksCorrectly()
        {
            DString currency = "FLR";
            Mock<ICurrencyConversionService> mockCurrencyConversionService = new Mock<ICurrencyConversionService>(MockBehavior.Strict);
            mockCurrencyConversionService.Setup(c => c.GetSingleAudUnitValueAsync(currency, this._Time1, Exchange.BTCMarkets)).Throws(new NoPriceDataException("Test", 100, DateTime.MinValue));
            this._CurrencyConversionService = mockCurrencyConversionService.Object;
            this.positiveTransactionTest(Exchange.BTCMarkets, TransactionType.StakingOrAirdrops, currency, 19000, 0, CreateReason.StakingIncome, assessableIncomeDelta: 0);
        }

        [TestMethod]
        public void MoonAirdropWorksCorrectly()
        {
            DString currency = "MOON";
            Mock<ICurrencyConversionService> mockCurrencyConversionService = new Mock<ICurrencyConversionService>(MockBehavior.Strict);
            mockCurrencyConversionService.Setup(c => c.GetSingleAudUnitValueAsync(currency, this._Time1, Exchange.OnChain)).Throws(new NoPriceDataException("Test", null));
            this._CurrencyConversionService = mockCurrencyConversionService.Object;
            this.positiveTransactionTest(Exchange.OnChain, TransactionType.StakingOrAirdrops, currency, 6500, 0, CreateReason.StakingIncome, assessableIncomeDelta: 0);
        }

        [TestMethod]
        public void PositiveForkWorksCorrectly()
        {
            this.positiveTransactionTest(Exchange.FTX, TransactionType.Fork, "FLR", 18845, 0, CreateReason.Fork);
        }

        [TestMethod]
        public void PositiveLoanInterestWorksCorrectly()
        {
            this.positiveTransactionTest(Exchange.FTX, TransactionType.LoanInterest, "XRP", 0.01m, 0, CreateReason.LoanInterestIncome);
        }

        [TestMethod]
        public void PositiveBnbDustConversionWorksCorrectly()
        {
            decimal bnbPrice = 350;
            decimal qty = 0.001m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "BNB", bnbPrice } });
            decimal value = qty * bnbPrice;
            this.positiveTransactionTest(Exchange.Binance, TransactionType.DustConversion, "BNB", qty, value, CreateReason.DustConversionIncome);
        }

        [TestMethod]
        public void NegativeBnbDustConversionWorksCorrectly()
        {
            decimal xrpPrice = 1.3m;
            decimal xrpAmount = 0.9m;
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() { { "USDT", xrpPrice } });
            decimal value = xrpAmount * xrpPrice;
            this.negativeTransactionTest_Disposal(Exchange.Binance, TransactionType.DustConversion, "USDT", xrpAmount, value, DisposalType.DustConversionPayment);
        }

        [TestMethod]
        public void PositiveFiatDepositWithoutFeesWorksCorrectly()
        {
            this.positiveTransactionTest(Exchange.BTCMarkets, TransactionType.FiatDeposit, "AUD", 1000, 1000, CreateReason.BankDeposit);
        }

        [TestMethod]
        public void PositiveFiatDepositWithFeesWorksCorrectly()
        {
            // transfer all funds to the account, then subtract fees
            this.positiveTransactionTest(Exchange.BTCMarkets, TransactionType.FiatDeposit, "AUD", 1000, 1000, CreateReason.BankDeposit, "AUD", 5, BurnReason.BankDepositFee);
        }

        [TestMethod]
        public void NegativeFiatWithdrawalWorksCorrectly()
        {
            this.negativeTransactionTest_Burn(Exchange.BTCMarkets, TransactionType.FiatWithdrawal, "AUD", 1000, BurnReason.BankWithdrawal);
        }

        [TestMethod]
        public void PositiveCryptoDepositWorksCorrectly()
        {
            this.positiveTransactionTest(Exchange.BTCMarkets, TransactionType.CryptoDeposit, "BTC", 0.01m, 0, CreateReason.DepositWithExternalSource, "BTC", 0.001m, BurnReason.Transfer);
        }

        [TestMethod]
        public void NegativeCryptoWithdrawalWorksCorrectly()
        {
            this.negativeTransactionTest_Burn(Exchange.BTCMarkets, TransactionType.CryptoWithdrawal, "BTC", 0.1m, BurnReason.WithdrawalWithExternalDestination, "BTC", 0.001m, BurnReason.Transfer);
        }

        [TestMethod]
        public void PositiveTransferInWorksCorrectly()
        {
            this.positiveTransactionTest(Exchange.OnChain, TransactionType.OnChainTransfer, "BTC", 0.1m, 0, CreateReason.TransferWithExternalSource, "BTC", 0.001m, BurnReason.Transfer);
        }

        [TestMethod]
        public void NegativeTransferOutWorksCorrectly()
        {
            this.negativeTransactionTest_Burn(Exchange.OnChain, TransactionType.OnChainTransfer, "BTC", 0.1m, BurnReason.TransferWithExternalDestination, "BTC", 0.001m, BurnReason.Transfer);
        }

        private void positiveTransactionTest(Exchange exchange, TransactionType type, DString currency, decimal amount, decimal expectedValue, CreateReason expectedCreateReason, string feeCurrency = null, decimal? feeAmount = null, BurnReason? expectedFeeBurnReason = null, decimal? assessableIncomeDelta = null)
        {
            TransactionDTO tx = new TransactionDTO(this._Time1, exchange, "id", null, null, type, new ContainerEventDTO(this._Time1, Container.FromExchange(exchange), null, null, currency, amount, feeCurrency, feeAmount));
            Mock<IInventory> inventory = new Mock<IInventory>();

            TransactionProcessingService service = this.constructAndSetupProcessingService(new MockInventoryManagerService(inventory.Object));
            service.ProcessTransactionAsync(tx).Wait();

            CurrencyAmount expectedCA = new CurrencyAmount(this._IdService, currency, amount, expectedValue, this._Time1, exchange);
            inventory.Verify(i => i.TransferIn(expectedCA, this._Time1), Times.Once());

            ReportingCreateData data;
            if (feeCurrency == null)
            {
                data = this._ReportingService.AddedData.Single() as ReportingCreateData;
            }
            else
            {
                data = this._ReportingService.AddedData.First() as ReportingCreateData;
                Assert.AreEqual(2, this._ReportingService.AddedData.Count());
                this.validateFee(inventory, feeCurrency, feeAmount.Value, expectedFeeBurnReason.Value);
            }

            inventory.VerifyNoOtherCalls();
            Assert.IsNotNull(data);
            Assert.AreEqual(this._Time1, data.Time);
            Assert.AreEqual(expectedCA, data.CreatedAmount);
            Assert.AreEqual(null, data.RelevantCA);
            Assert.AreEqual(expectedCreateReason, data.Reason);
            Assert.AreEqual(assessableIncomeDelta, data.AssessableIncomeDelta);

            Assert.AreEqual(assessableIncomeDelta ?? 0, this._AssessableIncomeStore.GetTotalAssessableIncome());
        }

        private void negativeTransactionTest_Disposal(Exchange exchange, TransactionType type, DString currency, decimal amount, decimal expectedValue, DisposalType expectedDisposalType, string feeCurrency = null, decimal? feeAmount = null, BurnReason? expectedFeeBurnReason = null)
        {
            TransactionDTO tx = new TransactionDTO(this._Time1, exchange, "id", null, null, type, new ContainerEventDTO(this._Time1, Container.FromExchange(exchange), currency, amount, null, null, feeCurrency, feeAmount));
            Mock<IInventory> inventory = new Mock<IInventory>();

            TransactionProcessingService service = this.constructAndSetupProcessingService(new MockInventoryManagerService(inventory.Object));
            service.ProcessTransactionAsync(tx).Wait();

            decimal overdrawn = 0;
            CurrencyAmount expectedCA = new CurrencyAmount(this._IdService, currency, amount, expectedValue, this._Time1, exchange);
            inventory.Verify(i => i.Disposal(expectedCA, 0, out overdrawn), Times.Once());
            ReportingDisposeData data;
            if (feeCurrency == null)
            {
                data = this._ReportingService.AddedData.Single() as ReportingDisposeData;
            }
            else
            {
                data = this._ReportingService.AddedData.First() as ReportingDisposeData;
                Assert.AreEqual(2, this._ReportingService.AddedData.Count());
                this.validateFee(inventory, feeCurrency, feeAmount.Value, expectedFeeBurnReason.Value);
            }

            inventory.VerifyNoOtherCalls();
            Assert.IsNotNull(data);
            Assert.AreEqual(this._Time1, data.Time);
            Assert.AreEqual(expectedDisposalType, data.DisposalType);
            Assert.AreEqual(expectedCA, data.DisposedAmount);
            Assert.IsNull(data.FeeAmount); // this is intentional: we don't associate fees directly with a PnL disposal, since it is treated as a transaction and not a trade.
        }

        private void negativeTransactionTest_Burn(Exchange exchange, TransactionType type, DString currency, decimal amount, BurnReason expectedBurnReason, string feeCurrency = null, decimal? feeAmount = null, BurnReason? expectedFeeBurnReason = null)
        {
            TransactionDTO tx = new TransactionDTO(this._Time1, exchange, "id", null, null, type, new ContainerEventDTO(this._Time1, Container.FromExchange(exchange), currency, amount, null, null, feeCurrency, feeAmount));
            Mock<IInventory> inventory = new Mock<IInventory>();

            TransactionProcessingService service = this.constructAndSetupProcessingService(new MockInventoryManagerService(inventory.Object));
            service.ProcessTransactionAsync(tx).Wait();

            decimal overdrawn = 0;
            CurrencyAmount expectedCA = new CurrencyAmount(this._IdService, currency, amount, 0, this._Time1, exchange); // technically exchange should be null, since this is a "reduced" currency amount, but doesn't matter either way.
            inventory.Verify(i => i.TransferOut(expectedCA, TransferOutReason.Burn, out overdrawn), Times.Once());
            ReportingBurnData data;
            if (feeCurrency == null)
            {
                data = this._ReportingService.AddedData.Single() as ReportingBurnData;
            }
            else
            {
                data = this._ReportingService.AddedData.First() as ReportingBurnData;
                Assert.AreEqual(2, this._ReportingService.AddedData.Count());
                this.validateFee(inventory, feeCurrency, feeAmount.Value, expectedFeeBurnReason.Value);
            }

            inventory.VerifyNoOtherCalls();
            Assert.IsNotNull(data);
            Assert.AreEqual(this._Time1, data.Time);
            Assert.AreEqual(expectedCA, data.BurnedAmount);
            Assert.AreEqual(null, data.RelevantCA);
            Assert.AreEqual(expectedBurnReason, data.Reason);
        }

        private void validateFee(Mock<IInventory> inventory, string feeCurrency, decimal feeAmount, BurnReason burnReason)
        {
            decimal overdrawn = 0;
            CurrencyAmount expectedFeeCA = new CurrencyAmount(this._IdService, feeCurrency, feeAmount, 0, this._Time1);
            inventory.Verify(i => i.TransferOut(expectedFeeCA, TransferOutReason.Burn, out overdrawn), Times.Once());

            CurrencyAmount argumentCA = (CurrencyAmount)inventory.Invocations[0].Arguments[0];
            ReportingBurnData feeData = this._ReportingService.AddedData.Last() as ReportingBurnData;
            Assert.IsNotNull(feeData);
            Assert.AreEqual(this._Time1, feeData.Time);
            Assert.AreEqual(expectedFeeCA, feeData.BurnedAmount);
            Assert.AreEqual(argumentCA.Id, feeData.RelevantCA.Value.Id);
            Assert.AreEqual(burnReason, feeData.Reason);
        }
        #endregion

        #region whitelist tests
        [TestMethod]
        public void SoftBlockedTransactionsNotIncluded()
        {
            // ensure soft blocked transactions are not processed by verifying calls to the inventory.

            TransactionDTO tx1 = new TransactionDTO(this._Time1, Exchange.BTCMarkets, null, "hash1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(this._Time1, this._Container, null, null, "XRP", 1, null, null));
            TransactionDTO tx2 = new TransactionDTO(this._Time2, Exchange.BTCMarkets, null, "hash2", null, TransactionType.CryptoDeposit, new ContainerEventDTO(this._Time2, this._Container, null, null, "XRP", 2, null, null));
            List<TransactionDTO> allTx = new List<TransactionDTO>() { tx1, tx2 };

            this._TransactionWhitelistService = new Mock<ITransactionWhitelistService>();
            this._TransactionWhitelistService.Setup(w => w.VerifyWhitelist(tx1)).Returns(false);
            this._TransactionWhitelistService.Setup(w => w.VerifyWhitelist(tx2)).Returns(true);
            this._TransferService = new TransferService(this._IdService, this._TransactionWhitelistService.Object, this._LoggingService);

            Mock<IInventory> inventory = new Mock<IInventory>();
            Mock<IInventoryManagerService> manager = new Mock<IInventoryManagerService>();
            manager.Setup(m => m.GetInventory(It.IsAny<Container>())).Returns(inventory.Object);
            TransactionProcessingService service = this.constructAndSetupProcessingService(manager.Object);

            // act
            service.ProcessTransactionAsync(tx1).Wait();
            service.ProcessTransactionAsync(tx2).Wait();

            // assert
            Assert.AreEqual(1, inventory.Invocations.Count);
            Assert.AreEqual(2, ((CurrencyAmount)inventory.Invocations[0].Arguments[0]).Amount);
            Assert.AreEqual(this._Time2, (DateTime)inventory.Invocations[0].Arguments[1]);
        }
        #endregion

        private TransactionProcessingService constructProcessingService(IInventoryManagerService inventoryManager = null)
        {
            if (inventoryManager == null) inventoryManager = new MockInventoryManagerService(this._MockInventory);
            return new TransactionProcessingService(this._IdService, this._CurrencyConversionService, this._ReportingService, inventoryManager, this._TransferService, this._TimeEventService, this._AssessableIncomeStore, this._TransactionWhitelistService.Object, this._BulkCandlestickService.Object, this._LoggingService, this._FtxStablecoinService.Object);
        }

        private TransactionProcessingService constructAndSetupProcessingService(IInventoryManagerService inventoryManager = null)
        {
            TransactionProcessingService service = this.constructProcessingService(inventoryManager);
            service.Setup(new List<TransactionDTO>());
            return service;
        }

        /// <summary>
        /// 2 txs. Transfer 100 XRP from BTCMarkets (time1) to FTX (time2).
        /// </summary>
        private IEnumerable<TransactionDTO> getTransfer1Transactions(bool instant, string sendingFeeCurrency = null, decimal? sendingFeeAmount = null, string receivingFeeCurrency = null, decimal? receivingFeeAmount = null)
        {
            DateTime time2 = instant ? this._Time1 : this._Time2;

            return new List<TransactionDTO>()
            {
                new TransactionDTO(this._Time1, Exchange.BTCMarkets, null, "xrpl_tx", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(this._Time1, Container.FromExchange(Exchange.BTCMarkets), "XRP", 100, null, null, sendingFeeCurrency, sendingFeeAmount)),
                new TransactionDTO(time2, Exchange.FTX, "ftx_internal", "xrpl_tx", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.FTX), null, null, "XRP", 100, receivingFeeCurrency, receivingFeeAmount))
            };
        }
    }
}
