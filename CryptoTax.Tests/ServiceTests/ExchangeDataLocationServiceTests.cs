﻿using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class ExchangeDataLocationServiceTests
    {
        [TestMethod]
        public void GetDataSources_Binance20192020_ReturnsGroupedData()
        {
            ExchangeDataLocationService service = new ExchangeDataLocationService("Folder\\");

            Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>> data = service.GetDataSources(Exchange.Binance, FinancialYear.Y2019);
            IEnumerable<IEnumerable<DString>> trades = data[SchemaIdentifier.Binance_SpotTrades];

            // 2 outer collections - one collection for 'classic' trades, the other for 'master' trades
            Assert.IsTrue(trades.Count() > 1);
        }
    }
}
