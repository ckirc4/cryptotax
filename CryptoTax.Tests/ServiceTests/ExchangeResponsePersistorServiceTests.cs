﻿using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using System.IO;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class ExchangeResponsePersistorServiceTests
    {
        DString _Folder;
        Mock<IFileService> _MockFileService;
        ExchangeResponsePersistorService _Service;

        [TestInitialize]
        public void Setup()
        {
            this._Folder = "TestFolder\\";
            this._MockFileService = new Mock<IFileService>();
            this._Service = new ExchangeResponsePersistorService(this._MockFileService.Object, this._Folder);
        }

        [TestMethod]
        public void CacheException_WritesToFile()
        {
            ArgumentException e = new ArgumentException();

            this._Service.CacheException(Exchange.Binance, "testUrl", null, e);

            this._MockFileService.Verify(f => f.WriteFile(It.Is<DString>(file => file.Value.StartsWith(this._Folder)), It.IsAny<DString>(), false));
        }

        [TestMethod]
        public void TryGetCachedException_ReturnsSavedException()
        {
            ArgumentException e = new ArgumentException("Test message", "param1");
            DString contents;
            this._MockFileService.Setup(f => f.WriteFile(It.IsAny<DString>(), It.IsAny<DString>(), true))
                .Callback<DString, DString, bool>((f, c, n) => contents = c);

            // test in-memory
            this._Service.CacheException(Exchange.Binance, "testUrl", null, e);
            bool result1 = this._Service.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException exception1);

            Assert.IsTrue(result1);
            Assert.AreEqual(e.Message, exception1.Message);
            Assert.AreEqual(e.ParamName, exception1.ParamName);

            // ensure we actually persisted
            this._MockFileService.Setup(f => f.ReadFile(It.Is<DString>(file => file.Value.StartsWith(this._Folder)))).Returns(contents);
            ExchangeResponsePersistorService newService = new ExchangeResponsePersistorService(this._MockFileService.Object, this._Folder);
            bool result2 = this._Service.TryGetCachedException(Exchange.Binance, "testUrl", null, out ArgumentException exception2);

            Assert.IsTrue(result2);
            Assert.AreEqual(e.Message, exception2.Message);
            Assert.AreEqual(e.ParamName, exception2.ParamName);
        }

        [TestMethod]
        public void TryGetCachedException_ReturnsFalseIfNoFileExists()
        {
            this._MockFileService.Setup(f => f.ReadAndDeserialiseFile<It.IsAnyType>(It.IsAny<DString>())).Throws(new FileNotFoundException());

            ExchangeResponsePersistorService service = new ExchangeResponsePersistorService(this._MockFileService.Object, this._Folder);
            bool result = this._Service.TryGetCachedException(Exchange.Binance, "testUrl", null, out Exception e);

            Assert.IsFalse(result);
            Assert.IsNull(e);
        }

        [TestMethod]
        public void TryGetCachedException_ReturnsFalseIfNoSavedExceptionForKey()
        {
            bool result = this._Service.TryGetCachedException(Exchange.Binance, "testUrl", null, out Exception e);

            Assert.IsFalse(result);
            Assert.IsNull(e);
        }

        [TestMethod]
        public void TryGetCachedException_ThrowsIfIncorrectType()
        {
            ArgumentException e = new ArgumentException("Test message", "param1");

            this._Service.CacheException(Exchange.Binance, "testUrl", null, e);

            Assert.ThrowsException<InvalidTypeException>(() => this._Service.TryGetCachedException(Exchange.Binance, "testUrl", null, out StackOverflowException _));
        }

        [TestMethod]
        public void GetExceptionCacheKey_ReturnsSameKeyForSameArgs()
        {
            DString[] keys = new DString[]
            {
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>() { ["testKey"] = "testValue" }),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>() { ["testKey"] = "testValue" })
            };

            Assert.AreEqual(1, keys.Distinct().Count());
        }

        [TestMethod]
        public void GetExceptionCacheKey_ReturnsDifferntKeyForDifferntArgs()
        {
            DString[] keys = new DString[]
            {
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>()),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testurl", new Dictionary<string, string>()),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.BTCMarkets, "testUrl", new Dictionary<string, string>() { ["testKey"] = "testValue" }),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>() { ["testKey"] = "testValue" }),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>() { ["testkey"] = "testValue" }),
                ExchangeResponsePersistorService.GetExceptionCacheKey(Exchange.Binance, "testUrl", new Dictionary<string, string>() { ["testKey"] = "testValue", ["testkey"] = "testValue" })
            };

            Assert.AreEqual(keys.Count(), keys.Distinct().Count());
        }
    }
}
