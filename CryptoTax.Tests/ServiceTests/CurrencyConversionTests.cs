﻿using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using MockApiService = CryptoTax.Tests.Mocks.MockExchangeApiServiceSingleCandlestick;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibrary.Types;
using System.Threading.Tasks;
using CryptoTax.Domain.Models.Exceptions;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class CurrencyConversionTests
    {
        private IReportingService _ReportingService;
        private IdProviderService _IdProviderService;
        private MockLoggingService _LoggingService;
        private MockRunService _RunService;

        private static readonly CandlestickInterval Interval = CandlestickInterval.Minute_1;
        private static readonly DateTime Time = DateTime.MinValue;
        private static readonly ExchangeSymbol BTCMarkets_BTCAUD = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), Exchange.BTCMarkets);
        private static readonly ExchangeSymbol KRAKEN_AUDUSD = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USD, KnownCurrencies.AUD), Exchange.Kraken);
        private static readonly ExchangeSymbol BTCMarkets_ETHAUD = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.ETH), Exchange.BTCMarkets);
        private static readonly ExchangeSymbol BTCMarkets_XRPAUD = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
        private static readonly ExchangeSymbol Binance_BTCUSDT = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance);
        private static readonly ExchangeSymbol Binance_BNBBTC = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.BTC, KnownCurrencies.BNB), Exchange.Binance);
        private static readonly ExchangeSymbol MercatoX_BOMBUSDT = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BOMB), Exchange.MercatoX);
        private static readonly ExchangeSymbol OnChain_WETHETH = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.ETH, KnownCurrencies.WETH), Exchange.OnChain);

        [TestInitialize]
        public void Init()
        {
            this._ReportingService = new MockTrackingReportingService();
            this._IdProviderService = new IdProviderService(0);
            this._LoggingService = new MockLoggingService();
            this._RunService = new MockRunService(null, null, this._LoggingService, this._IdProviderService.GetNext(IdType.Run), null, candlestickInterval: Interval);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetBTCMarketsTestData), DynamicDataSourceType.Method)]
        public void BTCMarketsConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetBinanceTestData), DynamicDataSourceType.Method)]
        public void BinanceSpotConversionTests(Pair start, Pair[] expectedRoute)
        {
            // we are not testing any futures conversions, since we are only interested in PnL, not actual amounts traded.
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetFtxTestData), DynamicDataSourceType.Method)]
        public void FtxConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetKrakenTestData), DynamicDataSourceType.Method)]
        public void KrakenConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetMercatoXTestData), DynamicDataSourceType.Method)]
        public void MercatoXConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetBitmexTestData), DynamicDataSourceType.Method)]
        public void BitmexConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [DataTestMethod]
        [DynamicData(nameof(GetOnChainTestData), DynamicDataSourceType.Method)]
        public void OnChainConversionTests(Pair start, Pair[] expectedRoute)
        {
            this.conductChainTest(start, expectedRoute);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForLongChain()
        {
            // define prices
            decimal BTCAUD_Price = 50000;
            decimal BTCUSDT_Price = 40000;
            Candlestick BTCAUD_Candle = this.createCandlestick(BTCMarkets_BTCAUD, BTCAUD_Price);
            Candlestick BTCUSDT_Candle = this.createCandlestick(Binance_BTCUSDT, BTCUSDT_Price);
            decimal BOMBUSDT_Price = 0.2m;

            // create service
            MockApiService mockBtcMarkets = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BTCAUD", Time), BTCAUD_Candle } });
            MockApiService mockBinance = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BTCUSDT", Time), BTCUSDT_Candle } });
            CurrencyConversionService conversion = this.createService(mockBtcMarkets, mockBinance);

            // get value
            (decimal quoteCurrencyValue, decimal baseCurrencyValue) = await conversion.GetAudUnitValueAsync(MercatoX_BOMBUSDT, BOMBUSDT_Price, Time);

            // check result
            decimal USDT_Value = BTCAUD_Price / BTCUSDT_Price;
            decimal BOMB_Value = BOMBUSDT_Price * USDT_Value;
            Assert.AreEqual(USDT_Value, quoteCurrencyValue);
            Assert.AreEqual(BOMB_Value, baseCurrencyValue);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForEthWrapTrade()
        {
            // define prices
            decimal ETHAUD_Price = 1000;
            Candlestick ETHAUD_Candle = this.createCandlestick(BTCMarkets_BTCAUD, ETHAUD_Price);

            // create service
            MockApiService mockBtcMarkets = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("ETHAUD", Time), ETHAUD_Candle } });
            CurrencyConversionService conversion = this.createService(mockBtcMarkets);

            // get value
            (decimal quoteCurrencyValue, decimal baseCurrencyValue) = await conversion.GetAudUnitValueAsync(OnChain_WETHETH, 1, Time);

            // check result
            Assert.AreEqual(ETHAUD_Price, quoteCurrencyValue);
            Assert.AreEqual(ETHAUD_Price, baseCurrencyValue);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForAudUsdtTrade()
        {
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.AUD), Exchange.Binance);
            CurrencyConversionService conversion = this.createService();

            (decimal quoteValue, decimal baseValue) = await conversion.GetAudUnitValueAsync(symbol, 0.73m, DateTime.Now);

            Assert.AreEqual(1 / 0.73m, quoteValue);
            Assert.AreEqual(1, baseValue);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForSingleAUDAsset()
        {
            CurrencyConversionService service = this.createService();
            decimal audValue = await service.GetSingleAudUnitValueAsync("AUD", Time, null);
            Assert.AreEqual(1, audValue);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForSingleTransferrableAsset()
        {
            // define prices
            decimal XRPAUD_Price = 1.5m;
            Candlestick XRPAUD_Candle = this.createCandlestick(BTCMarkets_XRPAUD, XRPAUD_Price);

            // create service
            MockApiService mockBtcMarkets = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("XRPAUD", Time), XRPAUD_Candle } });
            CurrencyConversionService conversion = this.createService(mockBtcMarkets);

            // get value
            decimal value = await conversion.GetSingleAudUnitValueAsync("XRP", Time, null);

            // check result
            Assert.AreEqual(XRPAUD_Price, value);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForSingleStablecoinAsset()
        {
            // define prices
            decimal BTCAUD_Price = 70000;
            decimal BTCUSDT_Price = 55000;
            Candlestick BTCAUD_Candle = this.createCandlestick(BTCMarkets_BTCAUD, BTCAUD_Price);
            Candlestick BTCUSDT_Candle = this.createCandlestick(Binance_BTCUSDT, BTCUSDT_Price);

            // create service
            MockApiService mockBtcMarkets = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BTCAUD", Time), BTCAUD_Candle } });
            MockApiService mockBinance = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BTCUSDT", Time), BTCUSDT_Candle } });
            CurrencyConversionService conversion = this.createService(mockBtcMarkets, mockBinance);

            // get value
            decimal value = await conversion.GetSingleAudUnitValueAsync("USDT", Time, null);

            // check result
            decimal USDT_value = BTCAUD_Price / BTCUSDT_Price;
            Assert.AreEqual(USDT_value, value);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForSingleUsd()
        {
            // define prices
            decimal AUDUSD_Price = 0.65m;
            Candlestick AUDUSD_Candle = this.createCandlestick(KRAKEN_AUDUSD, AUDUSD_Price);

            // create service
            MockApiService mockKraken = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("AUDUSD", Time), AUDUSD_Candle } });
            CurrencyConversionService conversion = this.createService(null, null, null, mockKraken);

            // get value
            decimal value = await conversion.GetSingleAudUnitValueAsync("USD", Time, null);

            // check result
            decimal USD_value = 1 / AUDUSD_Price;
            Assert.AreEqual(USD_value, value);
        }

        [TestMethod]
        public async Task ValueCalculationWorksForSingleBNBAsset()
        {
            // define prices
            decimal BTCAUD_Price = 70000;
            decimal BNBBTC_Price = 0.01m;
            Candlestick BTCAUD_Candle = this.createCandlestick(BTCMarkets_BTCAUD, BTCAUD_Price);
            Candlestick BNBBTC_Candle = this.createCandlestick(Binance_BNBBTC, BNBBTC_Price);

            // create service
            MockApiService mockBtcMarkets = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BTCAUD", Time), BTCAUD_Candle } });
            MockApiService mockBinance = new MockApiService(new Dictionary<(DString, DateTime), Candlestick>() { { ("BNBBTC", Time), BNBBTC_Candle } });
            CurrencyConversionService conversion = this.createService(mockBtcMarkets, mockBinance);

            // get value
            decimal value = await conversion.GetSingleAudUnitValueAsync("BNB", Time, null);

            // check result
            decimal USDT_value = BTCAUD_Price * BNBBTC_Price;
            Assert.AreEqual(USDT_value, value);
        }

        [TestMethod]
        public async Task ValueCalculation_ThrowNoDataExceptionForMoon()
        {
            CurrencyConversionService service = this.createService();
            await Assert.ThrowsExceptionAsync<NoPriceDataException>(() => service.GetSingleAudUnitValueAsync("MOON", Time, Exchange.OnChain));
        }

        [TestMethod]
        public async Task ValueCalculationFailsForInvalidAsset()
        {
            CurrencyConversionService service = this.createService();
            await Assert.ThrowsExceptionAsync<Exception>(() => service.GetSingleAudUnitValueAsync("BOMB", Time, null));
        }

        [TestMethod]
        public void TryGetExchangeSymbolFromCurrency_WorksForNPXS()
        {
            CurrencyConversionService service = this.createService();
            ExchangeSymbol? result = service.TryGetExchangeSymbolFromCurrency("NPXS", Exchange.Binance);
            Assert.AreEqual("NPXSBTC", result.Value.Name.Value);
        }

        [TestMethod]
        public void TryGetExchangeSymbolFromCurrency_MOON_ReturnsNull()
        {
            CurrencyConversionService service = this.createService();
            ExchangeSymbol? result = service.TryGetExchangeSymbolFromCurrency("MOON", Exchange.OnChain);
            Assert.IsNull(result);
        }

        /// <summary>
        /// By default, uses the closing prices.
        /// </summary>
        private CurrencyConversionService createService(IExchangeApiService btcService = null, IExchangeApiService binanceService = null, IExchangeApiService ftxService = null, IExchangeApiService krakenService = null, IPriceCalculator priceCalculator = null)
        {
            return new CurrencyConversionService(btcService, binanceService, ftxService, krakenService, this._ReportingService, this._IdProviderService, this._RunService, priceCalculator ?? new PriceCalculatorClose());
        }

        private Candlestick createCandlestick(ExchangeSymbol symbol, decimal price)
        {
            return new Candlestick(Time, Interval, symbol, price, price);
        }

        private void conductChainTest(Pair start, Pair[] expectedRoute)
        {
            CurrencyConversionService conversion = new CurrencyConversionService(null, null, null, null, this._ReportingService, this._IdProviderService, new MockRunService(null, null, this._LoggingService, this._IdProviderService.GetNext(IdType.Run), null, candlestickInterval: CandlestickInterval.Minute_1), new MockPriceCalculatorZero());
            IEnumerable<ExchangeSymbol> route = conversion.GetChainToAud(start.ToExchangeSymbol());
            CollectionAssert.AreEqual(expectedRoute, route.ToArray());
        }

        public static IEnumerable<object[]> GetBTCMarketsTestData()
        {
            yield return new object[] { new Pair("AUD", "BTC"), new[] { new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("AUD", "XRP"), new[] { new Pair("AUD", "XRP") } };
            yield return new object[] { new Pair("AUD", "ENJ"), new[] { new Pair("AUD", "ENJ") } };
            yield return new object[] { new Pair("BTC", "XRP"), new[] { new Pair("BTC", "XRP"), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("BTC", "ENJ"), new[] { new Pair("BTC", "ENJ"), new Pair("AUD", "BTC") } };
        }

        public static IEnumerable<object[]> GetBinanceTestData()
        {
            Exchange B = Exchange.Binance;
            yield return new object[] { new Pair("USDT", "BTC", B), new[] { new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USDT", "XRP", B), new[] { new Pair("USDT", "XRP", B), new Pair("AUD", "XRP") } };
            yield return new object[] { new Pair("BTC", "ENJ", B), new[] { new Pair("BTC", "ENJ", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("ETH", "NPXS", B), new[] { new Pair("ETH", "NPXS", B), new Pair("AUD", "ETH") } };
            yield return new object[] { new Pair("USDT", "NPXS", B), new[] { new Pair("USDT", "NPXS", B), new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("BNB", "NPXS", B), new[] { new Pair("BNB", "NPXS", B), new Pair("BTC", "BNB", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("BNB", "ETH", B), new[] { new Pair("BNB", "ETH", B), new Pair("AUD", "ETH") } };
            yield return new object[] { new Pair("BTC", "ETH", B), new[] { new Pair("BTC", "ETH", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USDT", "AUD", B), new[] { new Pair("USDT", "AUD", B)} };
        }

        public static IEnumerable<object[]> GetFtxTestData()
        {
            Exchange F = Exchange.FTX;
            Exchange B = Exchange.Binance;
            Exchange K = Exchange.Kraken;
            yield return new object[] { new Pair("USD", "BTC", F), new[] { new Pair("USD", "BTC", F), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USDT", "BTC", F), new[] { new Pair("USDT", "BTC", F), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USD", "XRP", F), new[] { new Pair("USD", "XRP", F), new Pair("AUD", "XRP") } };
            yield return new object[] { new Pair("BTC", "ETH", F), new[] { new Pair("BTC", "ETH", F), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USD", "XRP-PERP", F), new[] { new Pair("USD", "XRP-PERP", F), new Pair("USD", "AUD", K) } };
            yield return new object[] { new Pair("USDT", "XRP-PERP", F), new[] { new Pair("USDT", "XRP-PERP", F), new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USD", "BULLSHIT", F), new[] { new Pair("USD", "BULLSHIT", F), new Pair("USD", "AUD", K) } };
            yield return new object[] { new Pair("USDT", "BULLSHIT", F), new[] { new Pair("USDT", "BULLSHIT", F), new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USD", "USDT", F), new[] { new Pair("USD", "USDT", F), new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
        }

        public static IEnumerable<object[]> GetKrakenTestData()
        {
            Exchange K = Exchange.Kraken;
            yield return new object[] { new Pair("USD", "AUD", K), new[] { new Pair("USD", "AUD", K) } };
            yield return new object[] { new Pair("USD", "ETH", K), new[] { new Pair("USD", "ETH", K), new Pair("USD", "AUD", K) } };
        }

        public static IEnumerable<object[]> GetMercatoXTestData()
        {
            Exchange M = Exchange.MercatoX;
            Exchange B = Exchange.Binance;
            yield return new object[] { new Pair("USDT", "BTC", M), new[] { new Pair("USDT", "BTC", M), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("USDT", "TUSD", M), new[] { new Pair("USDT", "TUSD", M), new Pair("USDT", "BTC", B), new Pair("AUD", "BTC") } };
            yield return new object[] { new Pair("BTC", "ETH", M), new[] { new Pair("BTC", "ETH", M), new Pair("AUD", "BTC") } };
        }

        public static IEnumerable<object[]> GetBitmexTestData()
        {
            Exchange M = Exchange.BitMex;
            // inverse contract (BTC/USD perpetual)
            yield return new object[] { new Pair("BTC", "XBTUSD", M), new[] { new Pair("BTC", "XBTUSD", M), new Pair("AUD", "BTC") } };
            // inverse contract (BTC/USD futures)
            yield return new object[] { new Pair("BTC", "XBTZ20", M), new[] { new Pair("BTC", "XBTZ20", M), new Pair("AUD", "BTC") } };
            // quanto contract (ETH/USD perpetual)
            yield return new object[] { new Pair("BTC", "ETHUSD", M), new[] { new Pair("BTC", "ETHUSD", M), new Pair("AUD", "BTC") } };
            // quanto contract (ETH/USD futures)
            yield return new object[] { new Pair("BTC", "ETHUSDH21", M), new[] { new Pair("BTC", "ETHUSDH21", M), new Pair("AUD", "BTC") } };
            // linear contract (ETH/BTC)
            yield return new object[] { new Pair("BTC", "ETHZ20", M), new[] { new Pair("BTC", "ETHZ20", M), new Pair("AUD", "BTC") } };

        }

        public static IEnumerable<object[]> GetOnChainTestData()
        {
            Exchange C = Exchange.OnChain;
            yield return new object[] { new Pair("WETH", "BOMB", C), new[] { new Pair("WETH", "BOMB", C), new Pair("AUD", "ETH") } };
            yield return new object[] { new Pair("BOMB", "WETH", C), new[] { new Pair("BOMB", "WETH", C), new Pair("AUD", "ETH") } };
            yield return new object[] { new Pair("WETH", "BTC", C), new[] { new Pair("WETH", "BTC", C), new Pair("AUD", "ETH") } };
            yield return new object[] { new Pair("ETH", "WETH", C), new[] { new Pair("ETH", "WETH", C), new Pair("AUD", "ETH") } };
        }
    }

    public struct Pair
    {
        public DString Quote { get; }
        public DString Base { get; }
        public Exchange Exchange { get; }

        public Pair(DString quoteAsset, DString baseAsset, Exchange exchange = Exchange.BTCMarkets)
        {
            this.Quote = quoteAsset;
            this.Base = baseAsset;
            this.Exchange = exchange;
        }

        public ExchangeSymbol ToExchangeSymbol()
        {
            Currency quoteCurrency = KnownCurrencies.TryGetCurrency(this.Quote);
            Currency baseCurrency = KnownCurrencies.TryGetCurrency(this.Base);
            GenericSymbol symbol = new GenericSymbol(quoteCurrency, baseCurrency);

            return new ExchangeSymbol(symbol, this.Exchange, $"{this.Base}/{this.Quote}");
        }

        public override bool Equals(object obj)
        {
            if (obj is Pair pair)
            {
                return this.Quote == pair.Quote
                    && this.Base == pair.Base
                    && this.Exchange == pair.Exchange;
            }
            else if (obj is ExchangeSymbol symbol)
            {
                return this.ToExchangeSymbol().Equals(symbol);
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public static implicit operator ExchangeSymbol(Pair pair)
        {
            return pair.ToExchangeSymbol();
        }
    }
}
