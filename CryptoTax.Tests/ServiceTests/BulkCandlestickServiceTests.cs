﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Persistence.Services;
using CryptoTax.Tests.Mocks;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Models.Exceptions;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class BulkCandlestickServiceTests
    {
        private ILoggingService _LoggingService;

        [TestInitialize]
        public void Setup()
        {
            this._LoggingService = new MockLoggingService();
        }

        [TestMethod]
        public void RequestWithGapAreGroupedTogetherIntoSingleRequest()
        {
            // arrange
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
            CandlestickInterval interval = CandlestickInterval.Day_1;

            DateTime time1 = new DateTime(2021, 7, 2, 13, 25, 0);
            DateTime time1Start = new DateTime(2021, 7, 2);
            DateTime time2 = new DateTime(2021, 7, 5, 13, 25, 0);
            DateTime time2Start = new DateTime(2021, 7, 5);
            DateTime time3 = new DateTime(2021, 8, 1, 13, 25, 0);
            DateTime time3Start = new DateTime(2021, 8, 1);

            Candlestick firstCandlestickGroup1 = new Candlestick(time1Start, interval, symbol, 5, 6);
            List<Candlestick> candlesticksGroup1 = new List<Candlestick>() { firstCandlestickGroup1 };
            candlesticksGroup1.Add(new Candlestick(candlesticksGroup1.Last()));
            candlesticksGroup1.Add(new Candlestick(candlesticksGroup1.Last()));
            candlesticksGroup1.Add(new Candlestick(candlesticksGroup1.Last()));
            Candlestick candlestickGroup2 = new Candlestick(time3Start, interval, symbol, 5, 6);
            List<Candlestick> candlesticksGroup2 = new List<Candlestick>() { candlestickGroup2 };

            Mock<IRunService> runService = new Mock<IRunService>(MockBehavior.Strict);
            runService.Setup(s => s.GetCandlestickInterval()).Returns(interval);

            Mock<IExchangeApiService> apiService = new Mock<IExchangeApiService>(MockBehavior.Strict);
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time1Start, time2Start, false)).Returns(Task.FromResult(candlesticksGroup1.AsEnumerable()));
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time3Start, time3Start, false)).Returns(Task.FromResult(candlesticksGroup2.AsEnumerable()));

            ExchangeCache cache = new ExchangeCache();

            BulkCandlestickKeyPersistorService bulkPersistor = new BulkCandlestickKeyPersistorService(new MockFileService(), "mockFolder");
            BulkCandlestickService cs = new BulkCandlestickService(apiService.Object, null, null, null, runService.Object, null, this._LoggingService, cache, bulkPersistor);

            // act
            cs.AddRequired(symbol, time1);
            cs.AddRequired(symbol, time2);
            cs.AddRequired(symbol, time3);
            cs.FetchAndSaveAll().Wait();

            // assert
            apiService.VerifyAll(); // all setup methods were called
            List<Candlestick> cachedCandlesticks1 = cache.GetCandlesticks(symbol.Exchange, symbol, interval, time1Start, time2Start);
            List<Candlestick> cachedCandlesticks2 = cache.GetCandlesticks(symbol.Exchange, symbol, interval, time3Start, time3Start);
        }

        [TestMethod]
        public void BulkRequestKeyIsSavedAndUsedCorrectly()
        {
            // when restarting the application, we don't want to have to reload the same candlestick data that has already been saved in the previous run. test that this actually works.

            // arrange 1
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
            CandlestickInterval interval = CandlestickInterval.Day_1;

            DateTime time1 = new DateTime(2021, 7, 2);
            DateTime time2 = new DateTime(2021, 7, 3);
            DateTime time3 = new DateTime(2021, 8, 1);

            List<Candlestick> candlesticksGroup1 = new List<Candlestick>() { new Candlestick(time1, interval, symbol, 5, 6), new Candlestick(time2, interval, symbol, 5, 6), };
            List<Candlestick> candlesticksGroup2 = new List<Candlestick>() { new Candlestick(time3, interval, symbol, 5, 6) };

            Mock<IRunService> runService = new Mock<IRunService>(MockBehavior.Strict);
            runService.Setup(s => s.GetCandlestickInterval()).Returns(interval);

            Mock<IExchangeApiService> apiService = new Mock<IExchangeApiService>(MockBehavior.Strict);
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time1, time2, false)).Returns(Task.FromResult(candlesticksGroup1.AsEnumerable()));
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time3, time3, false)).Returns(Task.FromResult(candlesticksGroup2.AsEnumerable()));

            ExchangeCache cache = new ExchangeCache();

            MockFileService fileService = new MockFileService();
            BulkCandlestickKeyPersistorService bulkPersistor = new BulkCandlestickKeyPersistorService(fileService, "mockFolder");
            BulkCandlestickService cs = new BulkCandlestickService(apiService.Object, null, null, null, runService.Object, null, this._LoggingService, cache, bulkPersistor);

            // act 1
            cs.AddRequired(symbol, time1);
            cs.AddRequired(symbol, time2);
            cs.AddRequired(symbol, time3);
            cs.FetchAndSaveAll().Wait();

            // assert 1
            apiService.VerifyAll(); // all setup methods were called
            Assert.AreEqual(1, fileService.FileSystem_Lines.Count);

            // arrange 2
            // simulate restarting the application. the cache and file service retain their data.
            // api service should not be called now
            apiService.Reset();
            BulkCandlestickService cs2 = new BulkCandlestickService(apiService.Object, null, null, null, runService.Object, null, this._LoggingService, cache, bulkPersistor);

            // act 2
            cs.AddRequired(symbol, time1);
            cs.AddRequired(symbol, time2);
            cs.AddRequired(symbol, time3);
            cs.FetchAndSaveAll().Wait();

            // assert 2
            apiService.VerifyAll();
        }

        [TestMethod]
        public void HandlesErrorsCorrectly()
        {
            // many requests can be done in parallel, but errors should only affect the request for which they happened - everything else should still complete as expected
            // the failed requests are ignored.

            // arrange
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
            CandlestickInterval interval = CandlestickInterval.Day_1;

            DateTime time1 = new DateTime(2019, 1, 1, 0, 0, 0); // will succeed
            DateTime time2 = new DateTime(2020, 1, 1, 0, 0, 0); // will fail completely
            DateTime time3a = new DateTime(2021, 1, 1, 0, 0, 0); // no data available
            DateTime time3b = new DateTime(2021, 1, 2, 0, 0, 0); // no data available
            DateTime time3c = new DateTime(2021, 1, 3, 0, 0, 0); // data available
            DateTime time4 = new DateTime(2022, 1, 1, 0, 0, 0); // no data available
            IEnumerable<Candlestick> c1 = new List<Candlestick>() { new Candlestick(time1, interval, symbol, 5, 6) };
            IEnumerable<Candlestick> c2 = new List<Candlestick>() { new Candlestick(time2, interval, symbol, 5, 6) };
            IEnumerable<Candlestick> c3 = new List<Candlestick>() { new Candlestick(time3c, interval, symbol, 5, 6) };

            Mock<IRunService> runService = new Mock<IRunService>(MockBehavior.Strict);
            runService.Setup(s => s.GetCandlestickInterval()).Returns(interval);

            Mock<IExchangeApiService> apiService = new Mock<IExchangeApiService>(MockBehavior.Strict);
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time1, time1, false)).Returns(Task.FromResult(c1));
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time2, time2, false)).ThrowsAsync(new Exception("Test exception"));
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time3a, time3c, false))
                .ThrowsAsync(new NoPriceDataException("Test", 0, time3a) { EarliestTimeWithData = time3c, AvailableDataForRequest = c3 });
            apiService.Setup(s => s.GetCandlesticksAsync(interval, symbol, time4, time4, false)).ThrowsAsync(new NoPriceDataException("Test", 0, time4));

            ExchangeCache cache = new ExchangeCache();
            BulkCandlestickKeyPersistorService bulkPersistor = new BulkCandlestickKeyPersistorService(new MockFileService(), "mockFolder");
            BulkCandlestickService cs = new BulkCandlestickService(apiService.Object, null, null, null, runService.Object, null, this._LoggingService, cache, bulkPersistor);

            // act
            cs.AddRequired(symbol, time1);
            cs.AddRequired(symbol, time2);
            cs.AddRequired(symbol, time3a);
            cs.AddRequired(symbol, time3b);
            cs.AddRequired(symbol, time3c);
            cs.AddRequired(symbol, time4);
            cs.FetchAndSaveAll().Wait();

            // assert
            apiService.VerifyAll(); // all setup methods were called
        }
    }
}
