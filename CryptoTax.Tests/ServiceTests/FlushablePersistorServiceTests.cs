﻿using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class FlushablePersistorServiceTests
    {
        private DString _OutputFolder;
        private Func<int, DString> _FileName;
        private Mock<IFileService> _MockFileService;

        [TestInitialize]
        public void Setup()
        {
            this._OutputFolder = "OutputFolder\\";
            this._FileName = (index) => $"File_{index}.txt";
            this._MockFileService = new Mock<IFileService>();
        }

        [TestMethod]
        public void LimitsRowsPerFile()
        {
            // capture filenames
            HashSet<DString> files = new HashSet<DString>();
            this._MockFileService.Setup(f => f.WriteLines(It.IsAny<DString>(), It.IsAny<IEnumerable<NString>>(), true)).Callback<DString, IEnumerable<NString>, bool>((r, _, __) => files.Add(r));
            FlushablePersistorService<DString> service = this.createService(2, 1, this._OutputFolder, "0");

            service.AddObject("1");
            service.AddObject("2");
            service.AddObject("3");
            service.AddObject("4");

            CollectionAssert.AreEqual(new DString[] { this._OutputFolder + this._FileName(1), this._OutputFolder + this._FileName(2) }, files.ToArray());
        }

        [TestMethod]
        public void Flush_WritesImmediately_IfFolderSet()
        {
            FlushablePersistorService<DString> service = this.createService(3, 3, this._OutputFolder);

            service.AddObject("1");
            service.AddObject("2");
            this._MockFileService.VerifyNoOtherCalls();

            service.Flush();
            this._MockFileService.Verify(f => f.AppendLines(It.IsAny<DString>(), It.Is<IEnumerable<NString>>(lines => lines.Count() == 2), It.IsAny<bool>()));
        }

        [TestMethod]
        public void Flush_DoesNotWrite_IfFolderNotSet()
        {
            FlushablePersistorService<DString> service = this.createService(3, 3, null);

            service.AddObject("1");
            service.AddObject("2");
            this._MockFileService.VerifyNoOtherCalls();

            service.Flush();
            this._MockFileService.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void AddObject_AutomaticallyFlushes_IfMaxRowsReached()
        {
            FlushablePersistorService<DString> service = this.createService(6, 3, this._OutputFolder);

            service.AddObject("1");
            service.AddObject("2");
            this._MockFileService.VerifyNoOtherCalls();

            service.AddObject("3");
            this._MockFileService.Verify(f => f.AppendLines(It.IsAny<DString>(), It.Is<IEnumerable<NString>>(lines => lines.Count() == 3), It.IsAny<bool>()));
        }

        private FlushablePersistorService<DString> createService(int maxRowsPerFile, int maxPendingRows, NString folder = default, NString initialContent = default)
        {
            return new FlushablePersistorService<DString>(
                this._MockFileService.Object,
                line => line,
                initialContent == default ? new NString[0] : new NString[1] { initialContent },
                this._FileName,
                maxRowsPerFile,
                maxPendingRows,
                folder);
        }
    }
}
