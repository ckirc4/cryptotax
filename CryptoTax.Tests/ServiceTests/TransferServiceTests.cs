﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Domain.Models;
using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using Moq;
using UtilityLibrary.Types;
using CryptoTax.Tests.Mocks;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Domain.Models.Exceptions;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class TransferServiceTests
    {
        private IdProviderService _IdProviderService;
        private ILoggingService _LoggingService;
        private Mock<ITransactionWhitelistService> _TransactionWhitelistService;
        private TransferService _TransferService;

        private Container _SourceContainer;

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._LoggingService = new MockLoggingService();
            this._TransactionWhitelistService = new Mock<ITransactionWhitelistService>();
            this._TransactionWhitelistService.Setup(w => w.VerifyWhitelist(It.IsAny<TransactionDTO>())).Returns(true);
            this._TransferService = new TransferService(this._IdProviderService, this._TransactionWhitelistService.Object, this._LoggingService);

            this._SourceContainer = Container.FromExchange(Exchange.BTCMarkets);
        }

        [TestMethod]
        public void DoTransferDiscoveryThrowsIfCalledTwice()
        {
            this._TransferService.DoTransferDiscovery(new List<TransactionDTO>());
            Assert.ThrowsException<TransferDiscoveryAlreadyDoneException>(() => this._TransferService.DoTransferDiscovery(new List<TransactionDTO>()));
        }

        [TestMethod]
        public void GetAssociatedTransferThrowsIfDiscoveryNotYetDone()
        {
            Assert.ThrowsException<TransferDiscoveryNotDoneException>(() => this._TransferService.GetAssociatedTransfer(new TransactionDTO(DateTime.Now, Exchange.BTCMarkets, "Test", "Test", null, TransactionType.CryptoDeposit, new ContainerEventDTO(DateTime.Now, this._SourceContainer, "XRP", 10, null, null, null, null))));
        }

        [TestMethod]
        public void SimpleTransferDiscoveryWorksCorrectly()
        {
            DateTime time1 = new DateTime(2021, 6, 1); // start tx1
            DateTime time2 = new DateTime(2021, 6, 2); // start tx2
            DateTime time3 = new DateTime(2021, 6, 3); // finish tx1

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.BTCMarkets, "start tx1", "tx1", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.BTCMarkets), "XRP", 1, null, null, "AUD", 1)),

                new TransactionDTO(time2, Exchange.BTCMarkets, "start tx2", "tx2", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BTCMarkets), "BTC", 0.1m, null, null, "AUD", 100)),

                new TransactionDTO(time3, Exchange.FTX, "finish tx1", "tx1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time3, Container.FromExchange(Exchange.FTX), null, null, "XRP", 1, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer("start tx1", "finish tx1", "tx1", new CurrencyAmount(this._IdProviderService, "XRP", 1, 0, time1),
                Container.FromExchange(Exchange.BTCMarkets), time1, new CurrencyAmount(this._IdProviderService, "AUD", 1, 0, time1),
                Container.FromExchange(Exchange.FTX), time3, null, txs.First(), txs.Last());

            (Transfer, TransferSide)? associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0));
            (Transfer, TransferSide)? associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1));
            (Transfer, TransferSide)? associated3 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(2));
            Assert.AreEqual(actual, associated1.Value.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Value.Item2);
            Assert.IsNull(associated2);
            Assert.AreEqual(actual, associated3.Value.Item1);
            Assert.AreEqual(TransferSide.Destination, associated3.Value.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BTCMarketsDepositDiscoveredCorrectly()
        {
            // we are sending 1000 XRP from FTX to BTCMarkets. We don't know the transaction hash of the deposit into BTCMarkets, so we have to infer it. Also, let's pretend there was a 0.1 XRP deposit fee on BTCMarkets - so we receive only 999.9 XRP.
            // there is also a 0.5 XRP withdrawal fee, so we have to send 1000.5 XRP.

            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.FTX, "123", "tx1", "btcmarkets_address", TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.FTX), "XRP", 1000m, null, null, "XRP", 0.5m)),

                new TransactionDTO(time1, Exchange.BTCMarkets, "456", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BTCMarkets), null, null, "XRP", 999.9m, null, null)) // fee is implied, not explicit
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer("123", "456", "tx1",
                new CurrencyAmount(this._IdProviderService, "XRP", 1000, 0, time1), // transferred amount
                Container.FromExchange(Exchange.FTX), time1, // source
                new CurrencyAmount(this._IdProviderService, "XRP", 0.5m, 0, time1), // fee to send
                Container.FromExchange(Exchange.BTCMarkets), time2, // destination
                new CurrencyAmount(this._IdProviderService, "XRP", 0.1m, 0, time2), // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BTCMarketsWithdrawalDiscoveredCorrectly()
        {
            // we are sending 1000 XRP from BTCMarkest to FTX. Let's pretend there was a 0.1 XRP withdrawal fee on BTCMarkets - so we receive only 999.9 XRP.
            // there is also a 0.5 XRP deposit fee, so we really only receive 999.4 XRP.

            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.BTCMarkets, "123", "tx1", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.BTCMarkets), "XRP", 1000m, null, null, null, null)), // fee is implied, not explicit

                new TransactionDTO(time1, Exchange.FTX, "456", "tx1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.FTX), null, null, "XRP", 999.9m, "XRP", 0.5m)) // small deposit fee
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer("123", "456", "tx1",
                new CurrencyAmount(this._IdProviderService, "XRP", 999.9m, 0, time1), // transferred amount
                Container.FromExchange(Exchange.BTCMarkets), time1, // source
                new CurrencyAmount(this._IdProviderService, "XRP", 0.1m, 0, time1), // fee to send
                Container.FromExchange(Exchange.FTX), time2, // destination
                new CurrencyAmount(this._IdProviderService, "XRP", 0.5m, 0, time2), // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BTCMarketsWithdrawalSimplifiedDiscoveredCorrectly()
        {
            // we are sending 1000 XRP from BTCMarkest to FTX, for a fee of 0.15 XRP. Thus the amount we sent is 1000.15 and received 1000.

            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.BTCMarkets, "123", "tx1", null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.BTCMarkets), "XRP", 1000.15m, null, null, null, null)), // fee is implied, not explicit

                new TransactionDTO(time1, Exchange.FTX, "456", "tx1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.FTX), null, null, "XRP", 1000m, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer("123", "456", "tx1",
                new CurrencyAmount(this._IdProviderService, "XRP", 1000, 0, time1), // transferred amount
                Container.FromExchange(Exchange.BTCMarkets), time1, // source
                new CurrencyAmount(this._IdProviderService, "XRP", 0.15m, 0, time1), // fee to send
                Container.FromExchange(Exchange.FTX), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BTCMarketsTransferShouldFailBecauseAmountDiscrepancy()
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.FTX, "123", "tx1", "btcmarkets_address", TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.FTX), "XRP", 1000m, null, null, null, null)),

                new TransactionDTO(time1, Exchange.BTCMarkets, "456", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BTCMarkets), null, null, "XRP", 900m, null, null))
            };

            Assert.ThrowsException<InvalidOperationException>(() => this._TransferService.DoTransferDiscovery(txs).Single());
        }

        [TestMethod]
        public void BTCMarketsTransferShouldFailBecauseCurrencyDiscrepancy()
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.FTX, "123", "tx1", "btcmarkets_address", TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.FTX), "XRP", 1000m, null, null, null, null)),

                new TransactionDTO(time1, Exchange.BTCMarkets, "456", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BTCMarkets), null, null, "OMG", 900m, null, null))
            };

            Assert.ThrowsException<InvalidOperationException>(() => this._TransferService.DoTransferDiscovery(txs).Single());
        }

        [TestMethod]
        public void BTCMarketsTransferShouldFailBecauseTwoWithdrawals()
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.FTX, "123", "tx1", "btcmarkets_address", TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.FTX), "XRP", 1000m, null, null, null, null)),

                new TransactionDTO(time1, Exchange.BTCMarkets, "456", null, null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BTCMarkets), null, null, "XRP", 100m, null, null))
            };

            Assert.ThrowsException<InvalidOperationException>(() => this._TransferService.DoTransferDiscovery(txs).Single());
        }

        [TestMethod]
        public void MainnetArbNovaBridgingTransactionMatched()
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.OnChain, null, "tx1", KnownWallets.ETH_MAIN_ARB_NOVA_BRIDGE_CONTRACT.Address, TransactionType.OnChainTransfer, new ContainerEventDTO(time1, Container.FromWallet(KnownWallets.ETH_MY_WALLET), "ETH", 0.5m, null, null, null, null)),

                new TransactionDTO(time1, Exchange.OnChain, null, "tx2", KnownWallets.ETH_ARB_NOVA_MAIN_BRIDGE_CONTRACT.Address, TransactionType.OnChainTransfer, new ContainerEventDTO(time2, Container.FromWallet(KnownWallets.ETH_MY_WALLET_ARB_NOVA), null, null, "ETH", 0.5m, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();

            Transfer expected = new Transfer(null, null, "tx1",
                new CurrencyAmount(this._IdProviderService, "ETH", 0.5m, 0, time1), // transferred amount
                Container.FromWallet(KnownWallets.ETH_MY_WALLET), time1, // source
                null, // fee to send
                Container.FromWallet(KnownWallets.ETH_MY_WALLET_ARB_NOVA), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void ArbNovaMainnetBridgingTransactionMatched()
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.OnChain, null, "tx1", KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_ARB_NOVA.Address, TransactionType.OnChainTransfer, new ContainerEventDTO(time1, Container.FromWallet(KnownWallets.ETH_MY_WALLET_ARB_NOVA), "ETH", 0.5m, null, null, "ETH", 0.001m)),

                new TransactionDTO(time2, Exchange.OnChain, null, "tx2", KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_MAINNET.Address, TransactionType.OnChainTransfer, new ContainerEventDTO(time2, Container.FromWallet(KnownWallets.ETH_MY_WALLET), null, null, "ETH", 0.48m, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();

            Transfer expected = new Transfer(null, null, "tx1",
                new CurrencyAmount(this._IdProviderService, "ETH", 0.5m, 0, time1), // transferred amount
                Container.FromWallet(KnownWallets.ETH_MY_WALLET_ARB_NOVA), time1, // source
                new CurrencyAmount(this._IdProviderService, "ETH", 0.001m, 0, time1), // fee to send
                Container.FromWallet(KnownWallets.ETH_MY_WALLET), time2, // destination
                new CurrencyAmount(this._IdProviderService, "ETH", 0.02m, 0, time2), // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [DataTestMethod]
        [DataRow(true)]
        [DataRow(false)]
        public void KrakenEthDeposit(bool mainnet)
        {
            DateTime time1 = new DateTime(2021, 6, 26, 7, 0, 0); // send time
            DateTime time2 = new DateTime(2021, 6, 26, 9, 0, 0); // receive time
            DString counterparty = mainnet ? KnownWallets.ETH_KRAKEN_DEPOSIT_2.Address : KnownWallets.ETH_ARB_NOVA_KRAKEN_DEPOSIT.Address;
            string currency = mainnet ? "ETH" : "MOON";
            Container sourceContainer = mainnet ? Container.FromWallet(KnownWallets.ETH_MY_WALLET) : Container.FromWallet(KnownWallets.ETH_MY_WALLET_ARB_NOVA);

            List <TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.OnChain, null, "tx1", counterparty, TransactionType.OnChainTransfer, new ContainerEventDTO(time1, sourceContainer, currency, 0.5m, null, null, null, null)),

                new TransactionDTO(time2, Exchange.Kraken, "internalTx", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.Kraken), null, null, currency, 0.5m, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();

            Transfer expected = new Transfer(null, "internalTx", "tx1",
                new CurrencyAmount(this._IdProviderService, currency, 0.5m, 0, time1), // transferred amount
                sourceContainer, time1, // source
                null, // fee to send
                Container.FromExchange(Exchange.Kraken), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void SendingBombWithBurnTransactionDiscoveredCorrectly()
        {
            DateTime time1 = new DateTime(2021, 6, 1); // send time
            DateTime time2 = new DateTime(2021, 6, 2); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.OnChain, null, "tx1", "mercato_address", TransactionType.OnChainTransfer, new ContainerEventDTO(time1, Container.FromExchange(Exchange.OnChain), "BOMB", 130, null, null, "ETH", 0.1m)),

                new TransactionDTO(time1, Exchange.OnChain, null, "tx1", KnownWallets.ETH_BURN_ADDRESS.Address, TransactionType.OnChainTransfer, new ContainerEventDTO(time1, Container.FromExchange(Exchange.OnChain), "BOMB", 2, null, null, null, null)),

                new TransactionDTO(time2, Exchange.MercatoX, "finish tx1", "tx1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.MercatoX), null, null, "BOMB", 128, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer(null, "finish tx1", "tx1",
                new CurrencyAmount(this._IdProviderService, "BOMB", 130, 0, time1), // transferred amount
                Container.FromExchange(Exchange.OnChain), time1, // source
                new CurrencyAmount(this._IdProviderService, "ETH", 0.1m, 0, time1), // fee to send
                Container.FromExchange(Exchange.MercatoX), time2, // destination
                new CurrencyAmount(this._IdProviderService, "BOMB", 2, 0, time2), // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide)? associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0));
            (Transfer, TransferSide)? associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1));
            (Transfer, TransferSide)? associated3 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(2));
            Assert.AreEqual(actual, associated1.Value.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Value.Item2);
            Assert.IsNull(associated2);
            Assert.AreEqual(actual, associated3.Value.Item1);
            Assert.AreEqual(TransferSide.Destination, associated3.Value.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BinanceWithdraw_BitMexDeposit()
        {
            DateTime time1 = new DateTime(2019, 6, 26, 20, 16, 20, DateTimeKind.Utc); // send time
            DateTime time2 = new DateTime(2019, 6, 26, 20, 16, 20, DateTimeKind.Utc); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.Binance, null, "tx1", KnownWallets.BTC_BITMEX.Address, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time2, Container.FromExchange(Exchange.Binance), "BTC", 0.11m, null, null, "BTC", 0.0005m)),

                new TransactionDTO(time2, Exchange.BitMex, "abc-2fg", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BitMex), null, null, "BTC", 0.11m, null, null)),
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer(null, "abc-2fg", "tx1",
                new CurrencyAmount(this._IdProviderService, "BTC", 0.11m, 0, time1), // transferred amount
                Container.FromExchange(Exchange.Binance), time1, // source
                new CurrencyAmount(this._IdProviderService, "BTC", 0.0005m, 0, time1), // fee to send
                Container.FromExchange(Exchange.BitMex), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide)? associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0));
            (Transfer, TransferSide)? associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1));
            Assert.AreEqual(actual, associated1.Value.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Value.Item2);
            Assert.AreEqual(actual, associated2.Value.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Value.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void MercatoXWithdraw_BitMexDeposit()
        {
            DateTime time1 = new DateTime(2019, 6, 26, 5, 56, 17, DateTimeKind.Utc); // send time
            DateTime time2 = new DateTime(2019, 6, 26, 12, 38, 58, DateTimeKind.Utc); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.MercatoX, null, "tx1", KnownWallets.BTC_BITMEX.Address, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.MercatoX), "BTC", 0.08931982m, null, null, "BTC", 0.0008m)),

                new TransactionDTO(time2, Exchange.BitMex, "abc-2fg", null, null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.BitMex), null, null, "BTC", 0.08931982m, null, null)),
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer(null, "abc-2fg", "tx1",
                new CurrencyAmount(this._IdProviderService, "BTC", 0.08931982m, 0, time1), // transferred amount
                Container.FromExchange(Exchange.MercatoX), time1, // source
                new CurrencyAmount(this._IdProviderService, "BTC", 0.0008m, 0, time1), // fee to send
                Container.FromExchange(Exchange.BitMex), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide)? associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0));
            (Transfer, TransferSide)? associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1));
            Assert.AreEqual(actual, associated1.Value.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Value.Item2);
            Assert.AreEqual(actual, associated2.Value.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Value.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }

        [TestMethod]
        public void BinanceWithdraw_FTXDeposit()
        {
            DateTime time1 = new DateTime(2022, 8, 26, 11, 0, 0); // send time
            DateTime time2 = new DateTime(2022, 8, 26, 13, 0, 0); // receive time

            List<TransactionDTO> txs = new List<TransactionDTO>()
            {
                new TransactionDTO(time1, Exchange.Binance, "", null, null, TransactionType.CryptoWithdrawal, new ContainerEventDTO(time1, Container.FromExchange(Exchange.Binance), "BUSD", 667, null, null, null, null)), // fee is implied, not explicit

                new TransactionDTO(time2, Exchange.FTX, "123", "tx1", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time2, Container.FromExchange(Exchange.FTX), null, null, "BUSD", 665.5m, null, null))
            };

            Transfer actual = this._TransferService.DoTransferDiscovery(txs).Single();
            Transfer expected = new Transfer("", "123", "tx1",
                new CurrencyAmount(this._IdProviderService, "BUSD", 665.5m, 0, time1), // transferred amount
                Container.FromExchange(Exchange.Binance), time1, // source
                new CurrencyAmount(this._IdProviderService, "BUSD", 1.5m, 0, time1), // fee to send
                Container.FromExchange(Exchange.FTX), time2, // destination
                null, // fee to receive
                txs.First(), txs.Last());

            (Transfer, TransferSide) associated1 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(0)).Value;
            (Transfer, TransferSide) associated2 = this._TransferService.GetAssociatedTransfer(txs.ElementAt(1)).Value;
            Assert.AreEqual(actual, associated1.Item1);
            Assert.AreEqual(TransferSide.Source, associated1.Item2);
            Assert.AreEqual(actual, associated2.Item1);
            Assert.AreEqual(TransferSide.Destination, associated2.Item2);

            NString comparison = Helpers.CompareObjects(expected, actual);
            Assert.IsNull(comparison.Value, "The following properties were not equal: " + comparison);
        }
    }
}
