﻿using CryptoTax.Persistence.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;
using Moq;
using CryptoTax.Shared.Services;
using CryptoTax.Shared.Enums;
using System.Linq;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class ReportingPersistorServiceTests
    {
        private DString _OutputFolder;
        private Mock<IFileService> _MockFileService;
        private ReportingPersistorService _Service;

        [TestInitialize]
        public void Setup()
        {
            this._OutputFolder = "OutputFolder\\";
            this._MockFileService = new Mock<IFileService>();
            this._Service = new ReportingPersistorService(this._MockFileService.Object, this._OutputFolder);
        }

        [TestMethod]
        public void DoesNotSaveIfOnlySingleRowAdded()
        {
            this._Service.AddRow(new DTO.ReportRowDTO("1", DateTime.Now, ReportRowCategory.Action, new ReportRowType[] { ReportRowType.Add }, "TEST"));

            this._MockFileService.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void SavesIfFlushCalled()
        {
            this._Service.AddRow(new DTO.ReportRowDTO("1", DateTime.Now, ReportRowCategory.Action, new ReportRowType[] { ReportRowType.Add }, "TEST"));
            this._Service.Flush();

            this._MockFileService.Verify(f => f.GetFilesInFolder(It.IsAny<DString>())); // checking if a file exists, and if not, writing the headers first
            this._MockFileService.Verify(f => f.WriteLines(It.IsAny<DString>(), It.IsAny<IEnumerable<NString>>(), true));
            this._MockFileService.Verify(f => f.AppendLines(It.IsAny<DString>(), It.IsAny<IEnumerable<NString>>(), false));
            this._MockFileService.VerifyNoOtherCalls();
        }
    }
}
