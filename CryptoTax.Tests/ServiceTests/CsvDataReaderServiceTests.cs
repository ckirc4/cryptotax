﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class CsvDataReaderServiceTests
    {
        private readonly Func<int, DateTime> _TimeFromId = id => new DateTime(2021, 8, id);
        private readonly Exchange _Exchange = Exchange.BTCMarkets;
        private readonly SymbolDTO _Symbol = new SymbolDTO("AUD", "XRP");
        private readonly OrderSide _Side = OrderSide.Buy;
        private readonly MarketType _Type = MarketType.Spot;
        private readonly decimal _Price = 0.8m;

        [TestMethod]
        public void WithAdditionalSource_CorrectlyGetsTradesAndTransactions()
        {
            // Arrange
            FinancialYear year = new FinancialYear(2021);
            CsvInfo csv1 = new CsvInfo("filePath1");
            CsvInfo csv2 = new CsvInfo("filePath2");
            CsvInfo csv3 = new CsvInfo("filePath3");
            CsvInfo csv4 = new CsvInfo("filePath4");

            CsvDataReaderServiceFactory factory = new CsvDataReaderServiceFactory();
            Mock<IFileService> fileService = new Mock<IFileService>();
            fileService.Setup(f => f.ReadLines("filePath1")).Returns(new DString[] { "Id_first", "1", "2", "3" });
            fileService.Setup(f => f.ReadLines("filePath2")).Returns(new DString[] { "Id_second", "7", "8", "9" });
            fileService.Setup(f => f.ReadLines("filePath3")).Returns(new DString[] { "Id_third", "3", "4", "5", "6" }); // note the duplicate 3
            fileService.Setup(f => f.ReadLines("filePath4")).Returns(new DString[] { "Id_third", "10", "11", "12" });

            Mock<Schema> schema1 = new Mock<Schema>();
            schema1.Setup(s => s.Identfier).Returns(SchemaIdentifier.MercatoX);
            schema1.Setup(s => s.Columns).Returns(new Column[] { new Column<int>("Id_first", true) });
            this.setupSchemaProcessOne(schema1, 1);
            this.setupSchemaProcessOne(schema1, 2);
            this.setupSchemaProcessOne(schema1, 3);

            Mock<Schema> schema2 = new Mock<Schema>();
            schema2.Setup(s => s.Identfier).Returns(SchemaIdentifier.Binance_SpotTrades);
            schema2.Setup(s => s.Columns).Returns(new Column[] { new Column<int>("Id_second", true) });
            this.setupSchemaProcessOne(schema2, 7);
            this.setupSchemaProcessOne(schema2, 8);
            this.setupSchemaProcessOne(schema2, 9);

            Mock<Schema> schema3 = new Mock<Schema>();
            schema1.Setup(s => s.Identfier).Returns(SchemaIdentifier.MercatoX);
            schema3.Setup(s => s.Columns).Returns(new Column[] { new Column<int>("Id_third", true) });
            this.setupSchemaProcessOne(schema3, 3);
            this.setupSchemaProcessOne(schema3, 4);
            this.setupSchemaProcessOne(schema3, 5);
            this.setupSchemaProcessOne(schema3, 6);
            this.setupSchemaProcessOne(schema3, 10);
            this.setupSchemaProcessOne(schema3, 11);
            this.setupSchemaProcessOne(schema3, 12);

            // Act
            CsvDataReaderService service = new CsvDataReaderService(fileService.Object, factory, csv1, schema1.Object, year, 0);
            service.WithAdditionalSource(csv2, schema2.Object, 0);
            service.WithAdditionalSource(csv3, schema3.Object, 0);
            service.WithAdditionalSource(csv4, schema3.Object, 0);

            // Assert
            // no transactions
            Assert.AreEqual(0, service.GetTransactions().Count());
            // all ids were retrieved - note the ordering (higher id numbers correspond to a later time)
            CollectionAssert.AreEqual(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }, service.GetTrades().Select(t => int.Parse(t.TradeId)).ToArray());
        }

        [TestMethod]
        public void GetObjectsWithoutOverlap_SingleReader_ReturnsAllObjects()
        {
            Mock<ICsvDataReaderService> mockReader = new Mock<ICsvDataReaderService>();
            int[] times = getRange(0, 5);

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader.Object, times }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader.Object };

            int[] result = invokeGetObjects(dict, readers);

            CollectionAssert.AreEquivalent(times, result);
        }

        [TestMethod]
        public void GetObjectsWithoutOverlap_IdenticalItemsInOverlapRegion_ReturnsAllIdenticalItems()
        {
            Mock<ICsvDataReaderService> mockReader1 = new Mock<ICsvDataReaderService>();
            int[] times1 = new[] { 1, 2, 3, 3, 4 };
            Mock<ICsvDataReaderService> mockReader2 = new Mock<ICsvDataReaderService>();
            int[] times2 = new[] { 2, 3, 3, 4, 5 };

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader1.Object, times1 },
                { mockReader2.Object, times2 }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader1.Object, mockReader2.Object };

            int[] result = invokeGetObjects(dict, readers);

            CollectionAssert.AreEquivalent(new[] { 1, 2, 3, 3, 4, 5 }, result);
        }

        [DataTestMethod]
        [DataRow(0, 5, 6, 10)] // No overlap
        [DataRow(0, 5, 3, 7)] // Simple overlap
        [DataRow(0, 5, 5, 10)] // Touching overlap
        [DataRow(0, 5, 0, 10)] // Inclusive overlap
        public void GetObjectsWithoutOverlap_BasicOverlapTests(int from1, int to1, int from2, int to2)
        {
            Mock<ICsvDataReaderService> mockReader1 = new Mock<ICsvDataReaderService>();
            int[] times1 = getRange(from1, to1);
            Mock<ICsvDataReaderService> mockReader2 = new Mock<ICsvDataReaderService>();
            int[] times2 = getRange(from2, to2);

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader1.Object, times1 },
                { mockReader2.Object, times2 }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader1.Object, mockReader2.Object };

            int[] result = invokeGetObjects(dict, readers);

            CollectionAssert.AreEquivalent(getRange(from1, to2), result);
        }

        [TestMethod]
        public void GetObjectsWithoutOverlap_NoDuplicatesInOverlapRegion_Throws()
        {
            Mock<ICsvDataReaderService> mockReader1 = new Mock<ICsvDataReaderService>();
            int[] times1 = getRange(0, 5);
            Mock<ICsvDataReaderService> mockReader2 = new Mock<ICsvDataReaderService>();
            int[] times2 = getRange(3, 10);

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader1.Object, times1 },
                { mockReader2.Object, times2 }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader1.Object, mockReader2.Object };

            Assert.ThrowsException<Exception>(() =>
            {
                CsvDataReaderService.GetObjectsWithoutOverlap(readers, reader => dict[reader], i => getTime(i), (i1, i2) => false);
            });
        }

        [TestMethod]
        public void GetObjectsWithoutOverlap_OverlapOnDifferentSchemas_ReturnsOverlapped()
        {
            Mock<ICsvDataReaderService> mockReader1 = new Mock<ICsvDataReaderService>();
            mockReader1.Setup(r => r.SchemaIdentifier).Returns(SchemaIdentifier.MercatoX);
            int[] times1 = getRange(0, 1);
            Mock<ICsvDataReaderService> mockReader2 = new Mock<ICsvDataReaderService>();
            mockReader2.Setup(r => r.SchemaIdentifier).Returns(SchemaIdentifier.BtcMarkets);
            int[] times2 = getRange(1, 2);

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader1.Object, times1 },
                { mockReader2.Object, times2 }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader1.Object, mockReader2.Object };

            int[] result = invokeGetObjects(dict, readers);

            CollectionAssert.AreEquivalent(new int[] { 0, 1, 1, 2 }, result);
        }

        [TestMethod]
        public void GetObjectWithoutOverlap_OverlapOnDifferentGroupIds_ReturnsOverlapped()
        {
            Mock<ICsvDataReaderService> mockReader1 = new Mock<ICsvDataReaderService>();
            mockReader1.Setup(r => r.SchemaIdentifier).Returns(SchemaIdentifier.BtcMarkets);
            mockReader1.Setup(r => r.GroupId).Returns(1);
            int[] times1 = getRange(0, 1);
            Mock<ICsvDataReaderService> mockReader2 = new Mock<ICsvDataReaderService>();
            mockReader2.Setup(r => r.SchemaIdentifier).Returns(SchemaIdentifier.BtcMarkets);
            mockReader2.Setup(r => r.GroupId).Returns(2);
            int[] times2 = getRange(1, 2);

            Dictionary<ICsvDataReaderService, int[]> dict = new Dictionary<ICsvDataReaderService, int[]>()
            {
                { mockReader1.Object, times1 },
                { mockReader2.Object, times2 }
            };
            List<ICsvDataReaderService> readers = new List<ICsvDataReaderService>() { mockReader1.Object, mockReader2.Object };

            int[] result = invokeGetObjects(dict, readers);

            CollectionAssert.AreEquivalent(new int[] { 0, 1, 1, 2 }, result);

        }

        private void setupSchemaProcessOne(Mock<Schema> schema, int id)
        {
            ContainerEventDTO containerEvent = new ContainerEventDTO(_TimeFromId(id), Container.FromExchange(Exchange.BTCMarkets), "BTC", 2, "XRP", 25500, null, null);
            TradeDTO trade = new TradeDTO(_TimeFromId(id), this._Exchange, id.ToString(), this._Symbol, this._Type, this._Side, this._Price, containerEvent);
            Mock<Record> record = new Mock<Record>();
            record.Setup(r => r.ToTradeDTO()).Returns(trade);
            schema.Setup(s => s.ProcessOne(It.Is<object[]>(row => row.Length == 1 && (int)row[0] == id))).Returns(record.Object);
        }

        private static DateTime getTime(int i)
        {
            return new DateTime(2021, 1, 1).AddDays(i);
        }

        private static int[] getRange(int from, int to)
        {
            int[] x = new int[to - from + 1];

            int index = 0;
            for (int i = from; i <= to; i++)
            {
                x[index] = i;
                index++;
            }

            return x;
        }

        private static int[] invokeGetObjects(Dictionary<ICsvDataReaderService, int[]> dict, IEnumerable<ICsvDataReaderService> readers)
        {
            return CsvDataReaderService.GetObjectsWithoutOverlap(readers, reader => dict[reader], i => getTime(i), (i1, i2) => i1 == i2).ToArray();
        }
    }
}
