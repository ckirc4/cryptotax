﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class CandlestickPersistorServiceTests
    {
        private DString _CandlestickDataPath;
        private ExchangeSymbol _Symbol;
        private CandlestickDTO _Candlestick1;
        private CandlestickDTO _Candlestick2;
        private CandlestickDTO _Candlestick3;

        private Mock<IFileService> _MockFileService;
        private CandlestickPersistorService _Service;

        [TestInitialize]
        public void Setup()
        {
            this._CandlestickDataPath = "CandlestickDataFolder\\";
            this._Symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);
            this._Candlestick1 = new CandlestickDTO(new DateTime(2021, 8, 19), CandlestickInterval.Day_1, this._Symbol, 1, 2);
            this._Candlestick2 = new CandlestickDTO(new DateTime(2021, 8, 20), CandlestickInterval.Day_1, this._Symbol, 1.5m, 2.5m);
            this._Candlestick3 = new CandlestickDTO(new DateTime(2021, 8, 22), CandlestickInterval.Day_1, this._Symbol, 3.14m, 4);

            this._MockFileService = new Mock<IFileService>();
            this._Service = new CandlestickPersistorService(this._MockFileService.Object, this._CandlestickDataPath);
        }

        [TestMethod]
        public void ReadCandlesticks_ReturnsEmptyIfNoDataExists()
        {
            List<CandlestickDTO> result = this._Service.ReadCandlesticks(this._Symbol, CandlestickInterval.Day_1);

            Assert.IsFalse(result.Any());
            this._MockFileService.VerifyAll();
        }

        [TestMethod]
        public void WriteCandlesticks_WritesGivenCandlesticksToFile()
        {
            List<CandlestickDTO> candlesticks = new List<CandlestickDTO>() { this._Candlestick1, this._Candlestick2, this._Candlestick3 };
            this._MockFileService.Reset();

            Assert.IsFalse(this._Service.HasData(this._Symbol, CandlestickInterval.Day_1));
            this._Service.WriteCandlesticks(this._Symbol, CandlestickInterval.Day_1, candlesticks);
            Assert.IsTrue(this._Service.HasData(this._Symbol, CandlestickInterval.Day_1));

            this._MockFileService.Verify(f => f.WriteFile(
                It.Is<DString>(n => n.Value.StartsWith(this._CandlestickDataPath) && n.Value.EndsWith(".json") && n.Value.Length > 20),
                It.IsAny<DString>(),false));
            this._MockFileService.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void Write_ReadTest()
        {
            MockFileService mockFileService = new MockFileService();
            CandlestickPersistorService service = new CandlestickPersistorService(mockFileService, this._CandlestickDataPath);
            service.WriteCandlesticks(this._Symbol, CandlestickInterval.Day_1, new List<CandlestickDTO>() { this._Candlestick1 });

            CandlestickPersistorService newService = new CandlestickPersistorService(mockFileService, this._CandlestickDataPath);
            CandlestickDTO loaded = newService.ReadCandlesticks(this._Symbol, CandlestickInterval.Day_1).Single();

            Assert.AreEqual(this._Candlestick1.Time, loaded.Time);
            Assert.AreEqual(this._Candlestick1.Interval, loaded.Interval);
            Assert.AreEqual(this._Candlestick1.Symbol, loaded.Symbol);
            Assert.AreEqual(this._Candlestick1.Open, loaded.Open);
            Assert.AreEqual(this._Candlestick1.Close, loaded.Close);
        }
    }
}
