﻿using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class IdProviderServiceTests
    {
        private DString _IdFolder = @"Data\TestFolder\";

        [TestMethod]
        public void PersistentIdProviderInstantiatesCorrectly()
        {
            IdProviderService service = new IdProviderService(new IdProviderPersistorService(new MockFileService(), this._IdFolder));

            Assert.IsNotNull(service);
        }

        [TestMethod]
        public void PersistentIdProviderLoadsData()
        {
            MockFileService fileService = new MockFileService();
            IdProviderPersistorService persistorService = new IdProviderPersistorService(fileService, this._IdFolder);
            IdProviderDTO dto = new IdProviderDTO(inventoryId: 1, currencyAmountId: 2, reportDataRowId: 3, reportRowId: 4, runId: 6);
            persistorService.Write(dto); // it is assumed that the persistor service correctly uses the file service

            IdProviderService service = new IdProviderService(persistorService);

            Assert.AreEqual("#i2", service.GetNext(IdType.Inventory).Value);
            Assert.AreEqual("#c3", service.GetNext(IdType.CurrencyAmount).Value);
            Assert.AreEqual("#d4", service.GetNext(IdType.ReportRowData).Value);
            Assert.AreEqual("#r5", service.GetNext(IdType.ReportRow).Value);
            Assert.AreEqual("#n7", service.GetNext(IdType.Run).Value);
        }

        [TestMethod]
        public void PersistentIdProviderSavesDataIfRunIdChanged()
        {
            MockFileService fileService = new MockFileService();
            IdProviderPersistorService persistorService = new IdProviderPersistorService(fileService, this._IdFolder);
            persistorService.Write(new IdProviderDTO(10, 10, 10, 10, 10));

            IdProviderService service = new IdProviderService(persistorService);
            DString next = service.GetNext(IdType.Run);

            Assert.AreEqual(long.Parse(next.Value.Substring(2)), persistorService.Read().RunId);
        }

        [TestMethod]
        public void PersistentIdProviderDoesNotSaveDataIfAnotherIdChanged()
        {
            MockFileService fileService = new MockFileService();
            IdProviderPersistorService persistorService = new IdProviderPersistorService(fileService, this._IdFolder);
            persistorService.Write(new IdProviderDTO(10, 10, 10, 10, 10));

            IdProviderService service = new IdProviderService(persistorService);
            DString next = service.GetNext(IdType.CurrencyAmount);

            Assert.AreEqual(10, persistorService.Read().CurrencyAmountId);
        }

        [TestMethod]
        public void FlushSavesData()
        {
            MockFileService fileService = new MockFileService();
            IdProviderPersistorService persistorService = new IdProviderPersistorService(fileService, this._IdFolder);
            persistorService.Write(new IdProviderDTO(10, 10, 10, 10, 10));

            IdProviderService service = new IdProviderService(persistorService);
            DString next = service.GetNext(IdType.CurrencyAmount);
            service.Flush();

            Assert.AreEqual(long.Parse(next.Value.Substring(2)), persistorService.Read().CurrencyAmountId);
        }

        [TestMethod]
        public void InMemoryIdProviderInstantiatesCorrectly()
        {
            IdProviderService service = new IdProviderService(0);

            Assert.IsNotNull(service);
        }

        [DataTestMethod]
        [DataRow(-5)]
        [DataRow(0)]
        [DataRow(1)]
        [DataRow(5)]
        public void InMemoryIdProviderProvidesCorrectId(long startId)
        {
            IdProviderService service = new IdProviderService(startId);

            for (int i = 0; i < 5; i++)
            {
                long nextId = startId + i;
                Assert.AreEqual($"#c{nextId}", service.GetNext(IdType.CurrencyAmount).Value);
            }
        }

        [DataTestMethod]
        [DataRow("12", "#n12")]
        [DataRow("#12", "#n12")]
        [DataRow("n12", "#n12")]
        [DataRow("#n12", "#n12")]
        public void FixFormatTests(string input, string expectedOutput)
        {
            string actualOutput = IdProviderService.FixFormat(input, IdType.Run);

            Assert.AreEqual(expectedOutput, actualOutput);
        }
    }
}
