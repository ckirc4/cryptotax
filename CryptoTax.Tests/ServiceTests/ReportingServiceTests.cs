﻿using CryptoTax.Domain;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class ReportingServiceTests
    {
        private MockLoggingService _LoggingService;
        private Mock<IRunService> _RunServiceMock;
        private Mock<IReportingPersistorService> _ReportingPersistorMock;
        private IdProviderService _IdProvider;
        private ReportingService _Service;

        [TestInitialize]
        public void Setup()
        {
            this._LoggingService = new MockLoggingService();
            this._RunServiceMock = new Mock<IRunService>(MockBehavior.Strict);
            this._ReportingPersistorMock = new Mock<IReportingPersistorService>();
            this._IdProvider = new IdProviderService(0);
            this._Service = new ReportingService(this._LoggingService, this._RunServiceMock.Object, this._ReportingPersistorMock.Object, new ReportRowGenerationService(this._IdProvider));
        }

        [TestMethod]
        public void DoesNotCallPersistorIfReportingDisabled()
        {
            this._RunServiceMock.Setup(r => r.GetEnableReporting()).Returns(false);
            this._Service.AddData(new ReportingPurchaseData(this._IdProvider, DateTime.Now, new CurrencyAmount(this._IdProvider, "BTC", 1, 50000, DateTime.Now)));

            this._ReportingPersistorMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void CorrectlyRelaysDataToPersistor()
        {
            this._RunServiceMock.Setup(r => r.GetEnableReporting()).Returns(true);
            this._Service.AddData(new ReportingPurchaseData(this._IdProvider, DateTime.Now, new CurrencyAmount(this._IdProvider, "BTC", 1, 50000, DateTime.Now)));

            this._ReportingPersistorMock.Verify(p => p.AddRow(It.IsAny<ReportRowDTO>()));
            this._ReportingPersistorMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void CorrectlyRelaysFlushCallToPersistor()
        {
            this._Service.Flush();

            this._ReportingPersistorMock.Verify(p => p.Flush());
            this._ReportingPersistorMock.VerifyNoOtherCalls();
        }
    }
}
