﻿using CryptoTax.DTO;
using CryptoTax.DTO.Serialised;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class DtoCachePersistorServiceTests
    {
        private static readonly DString _CacheFolder = "TestCacheFolder\\";
        private static readonly DString _HashFile = _CacheFolder + "Hashes.json";
        private static readonly DString _TradesFile = _CacheFolder + "2018-2019_Trades.json";
        private static readonly DString _TransactionsFile = _CacheFolder + "2018-2019_Transactions.json";
        private DtoCachePersistorService _Service;
        private Mock<IExchangeDataLocationService> _MockExchangeDataLocationService;
        private Mock<IFileService> _MockFileService;
        private Mock<ICryptoService> _MockCryptoService;

        private TradeDTO _Trade;
        private TransactionDTO _Transaction;

        [TestInitialize]
        public void Setup()
        {
            this._MockExchangeDataLocationService = new Mock<IExchangeDataLocationService>();
            this._MockExchangeDataLocationService.Setup(s => s.GetDataSources(It.IsAny<Exchange>(), It.IsAny<FinancialYear>())).Returns(new Dictionary<SchemaIdentifier, IEnumerable<IEnumerable<DString>>>());
            this._MockFileService = new Mock<IFileService>();
            this._MockCryptoService = new Mock<ICryptoService>();

            this._Service = new DtoCachePersistorService(this._MockFileService.Object, this._MockCryptoService.Object, this._MockExchangeDataLocationService.Object, _CacheFolder);

            this._Trade = new TradeDTO(DateTime.Now, Exchange.Binance, "123456", new SymbolDTO("AUD", "XRP"), MarketType.Spot, OrderSide.Buy, 1.3m, new ContainerEventDTO(DateTime.Now, Container.FromExchange(Exchange.Binance), "AUD", 100, "XRP", 130, "AUD", 0.01m));
            this._Transaction = new TransactionDTO(DateTime.Now, Exchange.Binance, "9876543", null, null, TransactionType.StakingOrAirdrops, new ContainerEventDTO(DateTime.Now, Container.FromExchange(Exchange.Binance), null, null, "FLR", 18845, null, null));
        }

        [TestMethod]
        public void RequiresRefresh_ReturnsTrueIfNoHashesSaved()
        {
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>());

            bool requiresRefresh = this._Service.RequiresRefresh(FinancialYear.Y2018);

            Assert.IsTrue(requiresRefresh);
        }

        [TestMethod]
        public void RequiresRefresh_ReturnsTrueIfHashForYearDoesNotExist()
        {
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>() { _HashFile });
            this._MockFileService.Setup(f => f.ReadFile(_HashFile)).Returns("{\"2016-2017\":\"irrelevantHash\"}");

            bool requiresRefresh = this._Service.RequiresRefresh(FinancialYear.Y2018);

            Assert.IsTrue(requiresRefresh);
        }

        [TestMethod]
        public void HashCalculationUsesAllFilesRegardlessOfGroup()
        {
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>() { _HashFile });
            this._MockFileService.Setup(f => f.ReadFile(_HashFile)).Returns("{\"2018-2019\":\"oldHash\"}");

            DString dataFileGroup1 = "Data\\DtoDataFile1.json";
            DString dataFileGroup2 = "Data\\DtoDataFile2.json";
            this.setupBinanceDataLocation(FinancialYear.Y2018, dataFileGroup1, dataFileGroup2);

            this._Service.RequiresRefresh(FinancialYear.Y2018);

            this._MockFileService.Verify(f => f.GetFingerprint(dataFileGroup1), Times.Once());
            this._MockFileService.Verify(f => f.GetFingerprint(dataFileGroup2), Times.Once());
        }

        [TestMethod]
        public void RequiresRefresh_ReturnsTrueIfHashesDifferent()
        {
            // saved hash does not match the file content's hash
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>() { _HashFile });
            this._MockFileService.Setup(f => f.ReadFile(_HashFile)).Returns("{\"2018-2019\":\"oldHash\"}");

            DString dataFile = "Data\\DtoDataFile.json";
            this.setupBinanceDataLocation(FinancialYear.Y2018, dataFile);
            this._MockFileService.Setup(f => f.GetFingerprint(dataFile)).Returns("partHash");
            this._MockCryptoService.Setup(f => f.CalculateHash("partHash")).Returns("newHash");

            bool requiresRefresh = this._Service.RequiresRefresh(FinancialYear.Y2018);

            Assert.IsTrue(requiresRefresh);
        }

        [TestMethod]
        public void RequiresRefresh_ReturnsFalseIfHashesSame()
        {
            // saved hash matches the file content's hash
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>() { _HashFile });
            this._MockFileService.Setup(f => f.ReadFile(_HashFile)).Returns("{\"2018-2019\":\"fullHash\"}");

            DString dataFile = "Data\\DtoDataFile.json";
            this.setupBinanceDataLocation(FinancialYear.Y2018, dataFile);
            this._MockFileService.Setup(f => f.GetFingerprint(dataFile)).Returns("partHash");
            this._MockCryptoService.Setup(f => f.CalculateHash("partHash")).Returns("fullHash");

            bool requiresRefresh = this._Service.RequiresRefresh(FinancialYear.Y2018);

            Assert.IsFalse(requiresRefresh);
        }

        [TestMethod]
        public void CacheAll_SavesProvidedDtosAndExtendsHashFile()
        {
            // set up expected DTO files to be saved
            List<SerialisedTradeDTO> trades = new List<SerialisedTradeDTO>() { this._Trade.Serialise() };
            List<SerialisedTransactionDTO> transactions = new List<SerialisedTransactionDTO>() { this._Transaction.Serialise() };
            DString tradesContents = JsonConvert.SerializeObject(trades);
            DString transactionsContents = JsonConvert.SerializeObject(transactions);
            this._MockFileService.Setup(f => f.WriteFile(_TradesFile, tradesContents, false)).Verifiable();
            this._MockFileService.Setup(f => f.WriteFile(_TransactionsFile, transactionsContents, false)).Verifiable();

            // set up existing hash
            DString prevHashContents = "{\"2017-2018\":\"prevHash\"}";
            this._MockFileService.Setup(f => f.GetFilesInFolder(_CacheFolder)).Returns(new List<DString>() { _HashFile });
            this._MockFileService.Setup(f => f.ReadFile(_HashFile)).Returns(prevHashContents);

            // set up expected new hash file
            DString newHashContents = "{\"2017-2018\":\"prevHash\",\"2018-2019\":\"fullHash\"}";
            this._MockFileService.Setup(f => f.WriteFile(_HashFile, newHashContents, false)).Verifiable();

            // set up CSV data hash calculation
            this.setupBinanceDataLocation(FinancialYear.Y2018, "Data\\data.csv");
            this._MockFileService.Setup(f => f.GetFingerprint("Data\\data.csv")).Returns("partHash");
            this._MockCryptoService.Setup(c => c.CalculateHash("partHash")).Returns("fullHash");

            // Act
            this._Service.CacheAll(FinancialYear.Y2018, new TradeDTO[] { this._Trade }, new TransactionDTO[] { this._Transaction });

            // Assert - all methods that were setup have been called
            this._MockFileService.VerifyAll();
            this._MockCryptoService.VerifyAll();
            this._MockExchangeDataLocationService.VerifyAll();
        }

        [TestMethod]
        public void LoadAll_LoadsSavedDtos()
        {
            // set up DTO data to be loaded
            List<SerialisedTradeDTO> trades = new List<SerialisedTradeDTO>() { this._Trade.Serialise() };
            List<SerialisedTransactionDTO> transactions = new List<SerialisedTransactionDTO>() { this._Transaction.Serialise() };
            this._MockFileService.Setup(f => f.ReadAndDeserialiseFile<IEnumerable<SerialisedTradeDTO>>(_TradesFile)).Returns(trades);
            this._MockFileService.Setup(f => f.ReadAndDeserialiseFile<IEnumerable<SerialisedTransactionDTO>>(_TransactionsFile)).Returns(transactions);

            (IEnumerable<TradeDTO> loadedTrades, IEnumerable<TransactionDTO> loadedTransactions) = this._Service.LoadAll(FinancialYear.Y2018);

            this._MockFileService.VerifyAll();
            Assert.AreEqual(this._Trade.TradeId, loadedTrades.Single().TradeId);
            Assert.AreEqual(this._Transaction.InternalTransactionId, loadedTransactions.Single().InternalTransactionId);
        }

        private void setupBinanceDataLocation(FinancialYear inputYear, DString outputFileGroup1, NString outputFileGroup2 = default)
        {
            List<IEnumerable<DString>> returnCollection = new List<IEnumerable<DString>>() { new DString[] { outputFileGroup1 } };
            if (outputFileGroup2 != null) returnCollection.Add(new DString[] { outputFileGroup2.Value });
            this._MockExchangeDataLocationService
                .Setup(s => s.GetDataSources(Exchange.Binance, inputYear))
                .Returns(new Dictionary<SchemaIdentifier,  IEnumerable<IEnumerable<DString>>>() { { SchemaIdentifier.Binance_Deposits, returnCollection } });
        }
    }
}
