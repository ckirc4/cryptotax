﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class TradeProcessingServiceTests
    {
        private ILoggingService _LoggingService;
        private IdProviderService _IdProviderService;
        private ICurrencyConversionService _CurrencyConversionService;
        private MockTrackingReportingService _ReportingService;
        private MockInventoryManagerService _InventoryManagerService;
        private TradeProcessingService _ProcessingService;
        private Mock<IBulkCandlestickService> _BulkCandlestickServiceMock;

        private MockTrackingInventory _InventoryMock;
        private readonly DateTime _Time = DateTime.Now;
        private Container _BinanceContainer;
        private SymbolDTO _XRPAUD;
        private SymbolDTO _BTCAUD;
        private SymbolDTO _BNBBTC;
        private SymbolDTO _XRPUSDT;

        private const decimal _VALUE_XRP = 1.8m;
        private const decimal _VALUE_USDT = 1.3m;
        private const decimal _VALUE_BNB = 800;
        private const decimal _PRICE_XRPAUD = _VALUE_XRP;
        private const decimal _PRICE_XRPUSDT = _VALUE_XRP / _VALUE_USDT;
        private const decimal _PRICE_BNBUSDT = _VALUE_BNB / _VALUE_USDT;
        private const decimal _FEE = 0.001m;

        [TestInitialize]
        public void Setup()
        {
            this._InventoryMock = new MockTrackingInventory();
            this._BinanceContainer = Container.FromExchange(Exchange.Binance);
            this._XRPAUD = new SymbolDTO(KnownCurrencies.AUD.Symbol, KnownCurrencies.XRP.Symbol);
            this._BTCAUD = new SymbolDTO(KnownCurrencies.AUD.Symbol, KnownCurrencies.BTC.Symbol);
            this._BNBBTC = new SymbolDTO(KnownCurrencies.BTC.Symbol, KnownCurrencies.BNB.Symbol);
            this._XRPUSDT = new SymbolDTO(KnownCurrencies.USDT.Symbol, KnownCurrencies.XRP.Symbol);

            this._IdProviderService = new IdProviderService(0);
            this._CurrencyConversionService = new MockCurrencyConversionService(new Dictionary<DString, decimal>() {
                { "XRP", _VALUE_XRP },
                { "USDT", _VALUE_USDT },
                { "BNB", _VALUE_BNB }
            });
            this._LoggingService = new MockLoggingService();
            this._ReportingService = new MockTrackingReportingService();
            this._InventoryManagerService = new MockInventoryManagerService(this._InventoryMock);
            this._BulkCandlestickServiceMock = new Mock<IBulkCandlestickService>();
            this._ProcessingService = new TradeProcessingService(this._IdProviderService, this._CurrencyConversionService, this._ReportingService, this._InventoryManagerService, this._BulkCandlestickServiceMock.Object, this._LoggingService);
            this._ProcessingService.Setup(new List<TradeDTO>());
        }

        [TestMethod]
        public void SetupSequenceWorksCorrectly()
        {
            TradeDTO trade = new TradeDTO(this._Time, Exchange.BTCMarkets, "123", this._XRPAUD, MarketType.Spot, OrderSide.Buy, 1m, new ContainerEventDTO(this._Time, this._BinanceContainer, "AUD", 1, "XRP", 1, null, null));

            Mock<IBulkCandlestickService> bulkCandlestickServiceMock = new Mock<IBulkCandlestickService>();
            TradeProcessingService processingService = new TradeProcessingService(this._IdProviderService, this._CurrencyConversionService, this._ReportingService, this._InventoryManagerService, bulkCandlestickServiceMock.Object, this._LoggingService);

            // first make sure we can't continue without calling setup
            Assert.ThrowsExceptionAsync<TradeSetupException>(() => processingService.ProcessTradeAsync(trade)).Wait();

            // now make sure setup is doing the correct things
            List<TradeDTO> allTrades = new List<TradeDTO>() { trade };
            processingService.Setup(allTrades);

            bulkCandlestickServiceMock.Verify(s => s.FetchAndSaveAll(), Times.Never());
            bulkCandlestickServiceMock.VerifyNoOtherCalls();

            // and finally make sure we can't call setup again
            Assert.ThrowsException<TradeSetupException>(() => processingService.Setup(allTrades));
        }

        [TestMethod]
        public void SetupRequiresCorrectCandlesticks()
        {
            // tests TAX-66: we don't want to get candlesticks for the same symbol as the trade, because we already have the price (inherent to the trade itself)
            // similarly, don't try to valuate fee currencies if the currency is the trade's quote or base currency, for the same reason as above

            TradeDTO trade1 = new TradeDTO(this._Time, Exchange.Binance, "123", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, 1, new ContainerEventDTO(this._Time, this._BinanceContainer, "USDT", 1, "XRP", 1, null, null)); // no fees: should only require XRP/AUD
            TradeDTO trade2 = new TradeDTO(this._Time, Exchange.Binance, "456", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, 1, new ContainerEventDTO(this._Time, this._BinanceContainer, "USDT", 1, "XRP", 1, "USDT", 1)); // fee is quote: should only require XRP/AUD
            TradeDTO trade3 = new TradeDTO(this._Time, Exchange.Binance, "789", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, 1, new ContainerEventDTO(this._Time, this._BinanceContainer, "USDT", 1, "XRP", 1, "BNB", 1)); // fee is unrelated: should require XRP/AUD and BNB/BTC and BTC/AUD

            Mock<IBulkCandlestickService> bulkCandlestickServiceMock = new Mock<IBulkCandlestickService>();
            TradeProcessingService processingService = new TradeProcessingService(this._IdProviderService, this._CurrencyConversionService, this._ReportingService, this._InventoryManagerService, bulkCandlestickServiceMock.Object, this._LoggingService);

            // now make sure setup is doing the correct things
            List<TradeDTO> allTrades = new List<TradeDTO>() { trade1, trade2, trade3 };
            processingService.Setup(allTrades);

            ExchangeSymbol XRPAUD = new ExchangeSymbol(this._XRPAUD.QuoteCurrency, this._XRPAUD.BaseCurrency, Exchange.BTCMarkets);
            ExchangeSymbol BNBBTC = new ExchangeSymbol(this._BNBBTC.QuoteCurrency, this._BNBBTC.BaseCurrency, Exchange.Binance);
            ExchangeSymbol BTCAUD = new ExchangeSymbol(this._BTCAUD.QuoteCurrency, this._BTCAUD.BaseCurrency, Exchange.BTCMarkets);
            DateTime expectedTime = this._Time;
            bulkCandlestickServiceMock.Verify(s => s.AddRequired(XRPAUD, expectedTime), Times.Exactly(3));
            bulkCandlestickServiceMock.Verify(s => s.AddRequired(BNBBTC, expectedTime), Times.Once());
            bulkCandlestickServiceMock.Verify(s => s.AddRequired(BTCAUD, expectedTime), Times.Once());
            bulkCandlestickServiceMock.Verify(s => s.FetchAndSaveAll(), Times.Never());
            bulkCandlestickServiceMock.VerifyNoOtherCalls();
        }

        [TestMethod]
        public void ErrorThrownWhenAmountsAndPriceDoNotMatch()
        {
            // there is some redundancy in the data which has to be consistent
            // let's buy some XRP using AUD, but we incorrectly add the fee amount to the sent amount
            DString currencyReceived = "XRP";
            DString currencySent = "USDT";
            DString feeCurrency = currencySent;

            decimal price = 1.5m; // this was the limit price of the order
            decimal amountReceived = 1500;
            decimal orderValue = amountReceived * price;
            decimal fee = orderValue * _FEE;
            decimal amountSent = orderValue + fee;

            // the inferred price is now a little higher; we have spent more money to receive the same amount

            ContainerEventDTO ev = new ContainerEventDTO(this._Time, this._BinanceContainer, currencySent, amountSent, currencyReceived, amountReceived, null, null);
            TradeDTO trade = new TradeDTO(this._Time, Exchange.BTCMarkets, "123", this._XRPAUD, MarketType.Spot, OrderSide.Buy, price, ev);

            Assert.ThrowsExceptionAsync<InconsistentTradeDataException>(() => this._ProcessingService.ProcessTradeAsync(trade)).Wait();
        }

        [TestMethod]
        public void CorrectReportingDataAddedXRPUSDTWithNoFees()
        {
            // only action-type, ignore state-type
            // ignore capital gains since we used a mock inventory. the "CorrectInventoryUsage" tests are used to ensure that the correct arguments are passed to the inventory, and inventory tests themselves ensure that it works correctly. so we don't actually need to check capital gains at all here, if all tests pass we know it works! (good architecture!)
            DString currencyReceived = "XRP";
            DString currencySent = "USDT";
            decimal price = _PRICE_XRPUSDT;
            decimal amountReceived = 8000;
            decimal valueReceived = amountReceived * _VALUE_XRP;
            decimal amountSent = amountReceived * price;
            decimal valueSent = amountSent * _VALUE_USDT;
            ContainerEventDTO ev = new ContainerEventDTO(this._Time, this._BinanceContainer, currencySent, amountSent, currencyReceived, amountReceived, null, null);
            TradeDTO trade = new TradeDTO(this._Time, Exchange.Binance, "123", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, price, ev);

            this._ProcessingService.ProcessTradeAsync(trade).Wait();
            IEnumerable<ReportingBaseData> reportingData = this._ReportingService.AddedData;
            Assert.IsTrue(reportingData.Count() >= 2);

            CurrencyAmount expectedPurchaseCA = new CurrencyAmount(this._IdProviderService, currencyReceived, amountReceived, valueReceived, this._Time, Exchange.Binance);
            CurrencyAmount expectedDisposalCA = new CurrencyAmount(this._IdProviderService, currencySent, amountSent, valueSent, this._Time, Exchange.Binance);

            // purchase event
            reportingData = this.discardUntilType(reportingData, ReportRowType.Purchase, out ReportingBaseData purchaseDataRaw, out _);
            ReportingPurchaseData purchaseData = purchaseDataRaw as ReportingPurchaseData;
            Assert.IsNotNull(purchaseData);
            Assert.AreEqual(this._Time, purchaseData.Time);
            Assert.IsTrue(expectedPurchaseCA == purchaseData.PurchasedAmount);

            // disposal event
            this.discardUntilType(reportingData, ReportRowType.Dispose, out ReportingBaseData disposeDataRaw, out _);
            ReportingDisposeData disposeData = disposeDataRaw as ReportingDisposeData;
            Assert.IsNotNull(disposeData);
            Assert.AreEqual(this._Time, disposeData.Time);
            Assert.IsTrue(expectedDisposalCA == disposeData.DisposedAmount);
            Assert.IsNull(disposeData.FeeAmount);
        }

        [TestMethod]
        public void CorrectReportingDataAddedXRPUSDTWithFees()
        {
            // only action-type, ignore state-type
            DString currencyReceived = "XRP";
            DString currencySent = "USDT";
            DString currencyFee = "BNB";
            decimal price = _PRICE_XRPUSDT;
            decimal amountReceived = 8000;
            decimal valueReceived = amountReceived * _VALUE_XRP;
            decimal amountSent = amountReceived * price;
            decimal valueSent = amountSent * _VALUE_USDT;
            decimal valueFee = valueSent * _FEE;
            decimal amountFee = valueFee / _VALUE_BNB;
            ContainerEventDTO ev = new ContainerEventDTO(this._Time, this._BinanceContainer, currencySent, amountSent, currencyReceived, amountReceived, currencyFee, amountFee);

            TradeDTO trade = new TradeDTO(this._Time, Exchange.Binance, "123", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, price, ev);

            this._ProcessingService.ProcessTradeAsync(trade).Wait();
            IEnumerable<ReportingBaseData> reportingData = this._ReportingService.AddedData;
            Assert.IsTrue(reportingData.Count() >= 2);

            CurrencyAmount expectedPurchaseCA = new CurrencyAmount(this._IdProviderService, currencyReceived, amountReceived, valueReceived, this._Time, Exchange.Binance);
            CurrencyAmount expectedDisposalCA = new CurrencyAmount(this._IdProviderService, currencySent, amountSent, valueSent, this._Time, Exchange.Binance);
            CurrencyAmount expectedFeeCA = new CurrencyAmount(this._IdProviderService, currencyFee, amountFee, valueFee, this._Time, Exchange.Binance);

            // purchase event
            reportingData = this.discardUntilType(reportingData, ReportRowType.Purchase, out ReportingBaseData purchaseDataRaw, out _);
            ReportingPurchaseData purchaseData = purchaseDataRaw as ReportingPurchaseData;
            Assert.IsNotNull(purchaseData);
            Assert.AreEqual(this._Time, purchaseData.Time);
            Assert.IsTrue(expectedPurchaseCA == purchaseData.PurchasedAmount);

            // disposal event
            reportingData = this.discardUntilType(reportingData, ReportRowType.Dispose, out ReportingBaseData disposeDataRaw, out _);
            ReportingDisposeData disposeData = disposeDataRaw as ReportingDisposeData;
            Assert.IsNotNull(disposeData);
            Assert.AreEqual(this._Time, disposeData.Time);
            Assert.IsTrue(expectedDisposalCA == disposeData.DisposedAmount);
            Assert.IsNotNull(disposeData.FeeAmount);
            Assert.IsTrue(expectedFeeCA == disposeData.FeeAmount.Value);

            // burn event
            this.discardUntilType(reportingData, ReportRowType.Burn, out ReportingBaseData burnDataRaw, out _);
            ReportingBurnData burnData = burnDataRaw as ReportingBurnData;
            Assert.IsNotNull(burnData);
            Assert.AreEqual(this._Time, burnData.Time);
            Assert.IsTrue(expectedDisposalCA == burnData.RelevantCA);
            Assert.AreEqual(BurnReason.Disposal, burnData.Reason);
            Assert.IsTrue(expectedFeeCA == burnData.BurnedAmount);
        }

        [TestMethod]
        public void CorrectInventoryUsageXRPUSDTWithNoFees()
        {
            // must call IInventory.Dispose and provide the correct CurrencyAmount
            DString currencyReceived = "XRP";
            DString currencySent = "USDT";
            decimal price = _PRICE_XRPUSDT;
            decimal amountReceived = 8000;
            decimal valueReceived = amountReceived * _VALUE_XRP;
            decimal amountSent = amountReceived * price;
            decimal valueSent = amountSent * _VALUE_USDT;
            ContainerEventDTO ev = new ContainerEventDTO(this._Time, this._BinanceContainer, currencySent, amountSent, currencyReceived, amountReceived, null, null);
            TradeDTO trade = new TradeDTO(this._Time, Exchange.Binance, "123", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, price, ev);

            this._ProcessingService.ProcessTradeAsync(trade).Wait();

            Assert.IsTrue(this._InventoryMock.TransferInCalls.Count() == 0);
            Assert.IsTrue(this._InventoryMock.TransferOutCalls.Count() == 0);
            Assert.IsTrue(this._InventoryMock.PurchaseCalls.Count() == 1);
            Assert.IsTrue(this._InventoryMock.DisposalCalls.Count() == 1);

            CurrencyAmount expectedPurchaseCA = new CurrencyAmount(this._IdProviderService, currencyReceived, amountReceived, valueReceived, this._Time, Exchange.Binance);
            CurrencyAmount expectedDisposalCA = new CurrencyAmount(this._IdProviderService, currencySent, amountSent, valueSent, this._Time, Exchange.Binance);

            CurrencyAmount purchaseCA = this._InventoryMock.PurchaseCalls.First();
            (CurrencyAmount disposalCA, decimal additionalCostBasis) = this._InventoryMock.DisposalCalls.First();
            Assert.IsTrue(expectedPurchaseCA == purchaseCA);
            Assert.IsTrue(expectedDisposalCA == disposalCA);
            Assert.AreEqual(0, additionalCostBasis);
        }

        [TestMethod]
        public void CorrectInventoryUsageXRPUSDTWithFees()
        {
            // must call IInventory.Dispose and IInventory.TransferOut and provide the correct CurrencyAmounts
            DString currencyReceived = "XRP";
            DString currencySent = "USDT";
            DString currencyFee = "BNB";
            decimal price = _PRICE_XRPUSDT;
            decimal amountReceived = 8000;
            decimal valueReceived = amountReceived * _VALUE_XRP;
            decimal amountSent = amountReceived * price;
            decimal valueSent = amountSent * _VALUE_USDT;
            decimal valueFee = valueSent * _FEE;
            decimal amountFee = valueFee / _VALUE_BNB;
            ContainerEventDTO ev = new ContainerEventDTO(this._Time, this._BinanceContainer, currencySent, amountSent, currencyReceived, amountReceived, currencyFee, amountFee);
            TradeDTO trade = new TradeDTO(this._Time, Exchange.Binance, "123", this._XRPUSDT, MarketType.Spot, OrderSide.Buy, price, ev);

            this._ProcessingService.ProcessTradeAsync(trade).Wait();

            Assert.IsTrue(this._InventoryMock.TransferInCalls.Count() == 0);
            Assert.IsTrue(this._InventoryMock.TransferOutCalls.Count() == 1);
            Assert.IsTrue(this._InventoryMock.PurchaseCalls.Count() == 1);
            Assert.IsTrue(this._InventoryMock.DisposalCalls.Count() == 1);

            CurrencyAmount expectedPurchaseCA = new CurrencyAmount(this._IdProviderService, currencyReceived, amountReceived, valueReceived, this._Time, Exchange.Binance);
            CurrencyAmount expectedDisposalCA = new CurrencyAmount(this._IdProviderService, currencySent, amountSent, valueSent, this._Time, Exchange.Binance);

            (CurrencyAmount transferOutCA, TransferOutReason reason) = this._InventoryMock.TransferOutCalls.First();
            CurrencyAmount purchaseCA = this._InventoryMock.PurchaseCalls.First();
            (CurrencyAmount disposalCA, decimal additionalCostBasis) = this._InventoryMock.DisposalCalls.First();
            Assert.AreEqual(currencyFee, transferOutCA.Currency);
            Assert.AreEqual(amountFee, transferOutCA.Amount);
            Assert.AreEqual(TransferOutReason.Burn, reason);
            Assert.IsTrue(expectedPurchaseCA == purchaseCA);
            Assert.IsTrue(expectedDisposalCA == disposalCA);
            Assert.AreEqual(valueFee, additionalCostBasis);
        }

        /// <summary>
        /// Discards all elements until the given data type is reached (inclusive). Outputs the data element of the given type, if any, and the number of discarded items (exclusive).
        /// </summary>
        private IEnumerable<ReportingBaseData> discardUntilType(IEnumerable<ReportingBaseData> collection, ReportRowType type, out ReportingBaseData data, out int discarded)
        {
            data = null;
            discarded = 0;
            foreach (ReportingBaseData item in collection)
            {
                if (item.Type == type)
                {
                    data = item;
                    break;
                }

                discarded++;
            }

            return collection.Skip(discarded + 1);
        }
    }
}
