﻿using CryptoTax.Persistence.Services;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class MissingCandlestickPersistorServiceTests
    {
        private static readonly CandlestickInterval _Interval = CandlestickInterval.Day_1;
        private static readonly DString _Folder = @"\TestFolder\";
        private static readonly DString _FileName = _Folder + $"MissingCandlestickData{_Interval}.json";
        private static readonly DateTime _Time1 = new DateTime(2021, 9, 9);
        private static readonly DateTime _Time2 = new DateTime(2021, 9, 10);
        private static readonly DateTime _Time3 = new DateTime(2021, 9, 11);
        private static readonly DateTime _Time4 = new DateTime(2021, 9, 12);
        private static readonly Exchange _Exchange = Exchange.Binance;
        private static readonly DString _Symbol = "BTCUSDT";
        private Mock<IFileService> _FileService;

        [TestInitialize]
        public void Setup()
        {
            this._FileService = new Mock<IFileService>();
        }

        [TestMethod]
        public void IncludesKnownMissingData_ReturnsFalseIfFileDoesntExist()
        {
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<It.IsAnyType>(_FileName))
                .Throws(new FileNotFoundException());
            MissingCandlestickPersistorService service = this.constructService();

            bool result = service.IncludesKnownMissingData(_Exchange, _Symbol, _Time1, _Time2, out DateTime? unknownDataUntilTime);

            this._FileService.VerifyAll();
            this._FileService.VerifyNoOtherCalls();
            Assert.IsFalse(result);
            Assert.IsNull(unknownDataUntilTime);
        }

        [TestMethod]
        public void IncludesKnownMissingData_ReturnsFalseIfFileEmpty()
        {
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(_FileName))
                .Returns(new Dictionary<string, IEnumerable<DateTime>>());
            MissingCandlestickPersistorService service = this.constructService();

            bool result = service.IncludesKnownMissingData(_Exchange, _Symbol, _Time1, _Time2, out DateTime? unknownDataUntilTime);

            this._FileService.VerifyAll();
            this._FileService.VerifyNoOtherCalls();
            Assert.IsFalse(result);
            Assert.IsNull(unknownDataUntilTime);
        }

        [TestMethod]
        public void AddKnownMissingData_Saves_Then_IncludesKnownMissingData_ReturnsTrue_IfRequestingFullRange()
        {
            // step 1: save data
            string savedContents = null;
            this._FileService.Setup(f => f.WriteFile(_FileName, It.IsAny<DString>(), false))
                .Callback<DString, DString, bool>((a, b, c) => savedContents = b);
            MissingCandlestickPersistorService service1 = this.constructService();

            service1.AddKnownMissingData(_Exchange, _Symbol, _Time1, _Time2);

            // step 2: retrieve full range
            this._FileService.Reset();
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(_FileName))
                .Returns(JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<DateTime>>>(savedContents));
            MissingCandlestickPersistorService service2 = this.constructService();

            bool result = service2.IncludesKnownMissingData(_Exchange, _Symbol, _Time1, _Time2, out DateTime? unkonwnDataUntilTime);

            Assert.IsTrue(result);
            Assert.AreEqual(_Time2, unkonwnDataUntilTime.Value);
        }

        [TestMethod]
        public void AddKnownMissingData_Saves_Then_IncludesKnownMissingData_ReturnsTrue_IfRequestingPartialRange()
        {
            // step 1: save data
            string savedContents = null;
            this._FileService.Setup(f => f.WriteFile(_FileName, It.IsAny<DString>(), false))
                .Callback<DString, DString, bool>((a, b, c) => savedContents = b);
            MissingCandlestickPersistorService service1 = this.constructService();

            service1.AddKnownMissingData(_Exchange, _Symbol, _Time1, _Time3);

            // step 2: retrieve partial range
            this._FileService.Reset();
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(_FileName))
                .Returns(JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<DateTime>>>(savedContents));
            MissingCandlestickPersistorService service2 = this.constructService();

            bool result = service2.IncludesKnownMissingData(_Exchange, _Symbol, _Time2, _Time4, out DateTime? unkonwnDataUntilTime);

            Assert.IsTrue(result);
            Assert.AreEqual(_Time3, unkonwnDataUntilTime.Value);
        }

        [TestMethod]
        public void AddKnownMissingData_Saves_Then_IncludesKnownMissingData_ReturnsFalse_IfRequestingOutsideOfRange()
        {
            // step 1: save data
            string savedContents = null;
            this._FileService.Setup(f => f.WriteFile(_FileName, It.IsAny<DString>(), false))
                .Callback<DString, DString, bool>((a, b, c) => savedContents = b);
            MissingCandlestickPersistorService service1 = this.constructService();

            service1.AddKnownMissingData(_Exchange, _Symbol, _Time1, _Time2);

            // step 2: retrieve partial range
            this._FileService.Reset();
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(_FileName))
                .Returns(JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<DateTime>>>(savedContents));
            MissingCandlestickPersistorService service2 = this.constructService();

            bool result = service2.IncludesKnownMissingData(_Exchange, _Symbol, _Time3, _Time4, out DateTime? unkonwnDataUntilTime);

            Assert.IsFalse(result);
            Assert.IsNull(unkonwnDataUntilTime);
        }

        [TestMethod]
        public void AddKnownMissingData_CanExtendSavedRange()
        {
            // step 1: save data
            string savedContents = null;
            this._FileService.Setup(f => f.WriteFile(_FileName, It.IsAny<DString>(), false))
                .Callback<DString, DString, bool>((a, b, c) => savedContents = b);
            MissingCandlestickPersistorService service1 = this.constructService();

            service1.AddKnownMissingData(_Exchange, _Symbol, _Time1, _Time2);
            service1.AddKnownMissingData(_Exchange, _Symbol, _Time3, _Time4);

            // step 2: retrieve data
            this._FileService.Reset();
            this._FileService.Setup(f => f.ReadAndDeserialiseFile<Dictionary<string, IEnumerable<DateTime>>>(_FileName))
                .Returns(JsonConvert.DeserializeObject<Dictionary<string, IEnumerable<DateTime>>>(savedContents));
            MissingCandlestickPersistorService service2 = this.constructService();

            bool result = service2.IncludesKnownMissingData(_Exchange, _Symbol, _Time1, _Time4, out DateTime? unkonwnDataUntilTime);

            Assert.IsTrue(result);
            Assert.AreEqual(_Time4, unkonwnDataUntilTime.Value);
        }

        private MissingCandlestickPersistorService constructService()
        {
            return new MissingCandlestickPersistorService(this._FileService.Object, _Folder, _Interval);
        }
    }
}
