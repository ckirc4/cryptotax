﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    internal class PriceCalculatorTests
    {
        private readonly DateTime _Time = new DateTime(2021, 1, 1, 20, 20, 0);
        private readonly CandlestickInterval _Interval = CandlestickInterval.Hour_1;
        private readonly ExchangeSymbol _Symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), Exchange.BTCMarkets);
        private readonly decimal _Open = 10;
        private readonly decimal _Close = 40;
        private Candlestick _Candlestick;

        [ClassInitialize]
        public void Init()
        {
            this._Candlestick = new Candlestick(CandlestickHelpers.GetStartTime(this._Interval, this._Time), this._Interval, this._Symbol, this._Open, this._Close);
        }

        [TestMethod]
        public void AllPriceCalculatorTypesAreImplemented()
        {
            IPriceCalculatorFactory factory = new PriceCalculatorFactory();

            foreach (PriceCalculationType type in Enum.GetValues(typeof(PriceCalculationType)))
            {
                IPriceCalculator priceCalculator = factory.Create(type);
                Assert.AreEqual(type, priceCalculator.Type);
            }
        }

        [TestMethod]
        public void PriceCalculatorOpenReturnsCorrectPrice()
        {
            IPriceCalculator priceCalculator = new PriceCalculatorOpen();

            Assert.AreEqual(10, priceCalculator.CalculatePrice(this._Time, this._Candlestick));
        }

        [TestMethod]
        public void PriceCalculatorCloseReturnsCorrectPrice()
        {
            IPriceCalculator priceCalculator = new PriceCalculatorClose();

            Assert.AreEqual(40, priceCalculator.CalculatePrice(this._Time, this._Candlestick));
        }

        [TestMethod]
        public void PriceCalculatorArithmeticMeanReturnsCorrectPrice()
        {
            IPriceCalculator priceCalculator = new PriceCalculatorArithmeticMean();

            Assert.AreEqual(25, priceCalculator.CalculatePrice(this._Time, this._Candlestick));
        }

        [TestMethod]
        public void PriceCalculatorTimeWeightedAverageReturnsCorrectPrice()
        {
            IPriceCalculator priceCalculator = new PriceCalculatorTimeWeightedAverage();

            Assert.AreEqual(20, priceCalculator.CalculatePrice(this._Time, this._Candlestick));
        }
    }
}
