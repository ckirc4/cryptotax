﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using UtilityLibrary.Collections;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class InventoryManagerServiceTests
    {
        private IdProviderService _IdProviderService;
        private IInventoryFactory _InventoryFactory;
        private IReportingService _ReportingService;
        private Mock<IRunService> _MockRunService;
        private MockLoggingService _LoggingService;
        private IEnumerable<IInventory> _BeginningInventories;

        private readonly Container _Exchange1Container = Container.FromExchange(Exchange.BTCMarkets);
        private readonly Container _Exchange2Container = Container.FromExchange(Exchange.Binance);
        private readonly Container _Wallet1Container = Container.FromWallet(KnownWallets.ETH_MY_WALLET_OLD);

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._InventoryFactory = new MockImmutableInventoryFactory();
            this._ReportingService = new MockTrackingReportingService();
            this.setBeginningInventories();
            this._MockRunService = new Mock<IRunService>();
            this._LoggingService = new MockLoggingService();

            /* Method track example:
            this._MockRunService.Setup(service => service.GetInventoryManagementType()).Returns(InventoryManagementType.Single);
            Assert.IsFalse((runService.Invocations.FirstOrDefault()?.Method.Name ?? "") == nameof(IRunService.GetInventoryManagementType));
            Assert.AreEqual(runService.Object.GetInventoryManagementType(), InventoryManagementType.Single);
            Assert.IsTrue(runService.Invocations.First().Method.Name == nameof(IRunService.GetInventoryManagementType));
            */
            // see also https://stackoverflow.com/questions/39413652/what-is-it-isany-and-what-is-it-is-in-unit-mock-testing for checking if methods were called with certain arguments
        }

        [TestMethod]
        public void InventoryManagerLoadsInventoriesOnInitialisation()
        {
            // ensure RunService load method is called
            // note: explicit mock setup is not necessary because we are already using Loose mode, so it will automatically return an empty list.
            InventoryManagerService inventoryManagerService = this.instantiateManagerService();

            this._MockRunService.Verify(runService => runService.LoadBeginningInventories(It.IsAny<IInventoryFactory>(), It.IsAny<IReportingService>()), Times.Once());
        }

        [TestMethod]
        public void UpdateRunStateCorrectlyCallsRunService()
        {
            this._MockRunService
                .Setup(runService => runService.LoadBeginningInventories(It.IsAny<IInventoryFactory>(), It.IsAny<IReportingService>()))
                .Returns(this._BeginningInventories);
            InventoryManagerService inventoryManagerService = this.instantiateManagerService();

            inventoryManagerService.UpdateRunState();

            this._MockRunService.Verify(runService =>
                runService.UpdateCurrentInventories(
                    It.Is<IEnumerable<IInventory>>((param) => param.Select(inv => inv.Id).CompareSequences(this._BeginningInventories.Select(inv => inv.Id), (a, b) => a == b, true))),
                Times.Once());
        }

        [TestMethod]
        public void InventoryManagerCalculatesCorrectCapitalGains()
        {
            this._MockRunService
                .Setup(runService => runService.LoadBeginningInventories(It.IsAny<IInventoryFactory>(), It.IsAny<IReportingService>()))
                .Returns(this._BeginningInventories);

            InventoryManagerService inventoryManagerService = this.instantiateManagerService();

            CapitalGains cg = inventoryManagerService.GetAggregateCapitalGains();
            // sum up individual components of beginning inventories' capital gains
            Assert.AreEqual(11, cg.StandardCapitalGains);
            Assert.AreEqual(12, cg.DiscountableCapitalGains);
            Assert.AreEqual(-5, cg.CapitalLosses);
        }

        [TestMethod]
        public void InventoryManagerCorrectlyDeterminesWhichContainersRequireInventory()
        {
            InventoryManagerService inventoryManagerService = this.instantiateManagerService();

            ContainerType[] typesThatRequireInventory = new[] { ContainerType.ExchangeAccount, ContainerType.OwnedWallet };
            foreach (ContainerType type in Enum.GetValues(typeof(ContainerType)))
            {
                Container container;
                switch (type)
                {
                    case ContainerType.OwnedWallet:
                        container = Container.FromWallet(new Wallet("wallet-address", KnownCurrencies.ERC20, null, true, BlockchainType.Ethereum));
                        break;
                    case ContainerType.ThirdPartyWallet:
                        container = Container.FromWallet(new Wallet("wallet-address", KnownCurrencies.ERC20, null, false, BlockchainType.Ethereum));
                        break;
                    case ContainerType.ExchangeWallet:
                        container = Container.FromWallet(new Wallet("wallet-address", KnownCurrencies.ERC20, Exchange.BTCMarkets, false, BlockchainType.Ethereum));
                        break;
                    case ContainerType.BankAccount:
                        container = Container.FromBankAccount();
                        break;
                    case ContainerType.ExchangeAccount:
                        container = Container.FromExchange(Exchange.BTCMarkets);
                        break;
                    default:
                        throw new NotImplementedException($"Cannot create a container for type {type}");
                }
                Assert.AreEqual(typesThatRequireInventory.Contains(type), inventoryManagerService.IsAssociatedWithInventory(container), $"Type {type} did not match");
            }
        }

        [TestCategory(Tests.META)]
        [TestMethod]
        public void InventoryManagerCreatesAndRetrieves()
        {
            // each creation/retrieval inventory will consist of three parts:
            // test 1: correctly created
            // test 2: correctly retrieved
            // test 3: throws on duplicates - ideally we can add some kind of FilterService that is used in place of IEnumerable.Where(), but that is a lot of work for little gain.

            foreach (InventoryManagementType type in Enum.GetValues(typeof(InventoryManagementType)))
            {
                switch (type)
                {
                    case InventoryManagementType.Single:
                        Assert.IsNotNull(nameof(this.InventoryManagerCreatesAndRetrievesInventory_SingleManagementType));
                        break;
                    case InventoryManagementType.OnePerExchangeType:
                        Assert.IsNotNull(nameof(this.InventoryManagerCreatesAndRetrievesInventory_ExchangeManagementType));
                        break;
                    default:
                        throw new NotImplementedException($"No inventory creation/retrieval tests have been implemented for management type {type}");
                }
            }
        }

        [TestMethod]
        public void InventoryManagerCreatesAndRetrievesInventory_SingleManagementType()
        {
            IEnumerable<IInventory> currentInventories = null;
            this._MockRunService
                .Setup(runService => runService.GetInventoryManagementType())
                .Returns(() => InventoryManagementType.Single);
            this._MockRunService
                .Setup(runService => runService.UpdateCurrentInventories(It.IsAny<IEnumerable<IInventory>>()))
                .Callback<IEnumerable<IInventory>>(newInventories => currentInventories = newInventories);
            InventoryManagerService service = this.instantiateManagerService();

            // should not change once created - it is enough to compare ids, because this way we know that it must be the same object
            service.GetInventory(this._Exchange1Container);
            service.UpdateRunState();
            Assert.AreEqual(1, currentInventories.Count());
            DString id = currentInventories.First().Id;

            service.GetInventory(this._Exchange2Container);
            service.UpdateRunState();
            Assert.AreEqual(1, currentInventories.Count());
            Assert.AreEqual(id, currentInventories.First().Id);

            service.GetInventory(this._Wallet1Container);
            service.UpdateRunState();
            Assert.AreEqual(1, currentInventories.Count());
            Assert.AreEqual(id, currentInventories.First().Id);

            // check all identifying details
            Assert.AreEqual(Exchange.Undefined, currentInventories.First().Exchange);
        }

        [TestMethod]
        public void InventoryManagerCreatesAndRetrievesInventory_ExchangeManagementType()
        {
            IEnumerable<IInventory> currentInventories = null;
            this._MockRunService
                .Setup(runService => runService.GetInventoryManagementType())
                .Returns(() => InventoryManagementType.OnePerExchangeType);
            this._MockRunService
                .Setup(runService => runService.UpdateCurrentInventories(It.IsAny<IEnumerable<IInventory>>()))
                .Callback<IEnumerable<IInventory>>(newInventories => currentInventories = newInventories);
            InventoryManagerService service = this.instantiateManagerService();

            // should not change once created
            service.GetInventory(this._Exchange1Container);
            service.UpdateRunState();
            Assert.AreEqual(1, currentInventories.Count());
            DString id1 = currentInventories.First().Id;

            service.GetInventory(this._Exchange2Container);
            service.UpdateRunState();
            Assert.AreEqual(2, currentInventories.Count());
            DString id2 = currentInventories.Last().Id;

            service.GetInventory(this._Wallet1Container);
            service.UpdateRunState();
            Assert.AreEqual(3, currentInventories.Count());
            DString id3 = currentInventories.Last().Id;

            // check all identifying details
            CollectionAssert.AreEqual(new[] { id1, id2, id3 }, currentInventories.Select(inv => inv.Id).ToArray());
            Assert.AreEqual(3, new[] { id1, id2, id3 }.Distinct().Count());
            CollectionAssert.AreEqual(new[] { Exchange.BTCMarkets, Exchange.Binance, Exchange.OnChain }, currentInventories.Select(inv => inv.Exchange).ToArray());
        }

        private void setBeginningInventories()
        {
            IEnumerable<InventoryDTO> dtoInventories = new List<InventoryDTO>()
                {
                    new InventoryDTO(
                    "Some mock inventory that was converted to DTO",
                    InventoryType.FILO,
                    this._IdProviderService.GetNext(IdType.Inventory),
                    Exchange.Undefined,
                    new Dictionary<string, IEnumerable<InventoryItemDTO>>()
                    {
                        {  "BTC",
                            new List<InventoryItemDTO>()
                            {
                                new InventoryItemDTO(this._IdProviderService.GetNext(IdType.CurrencyAmount), null, DateTime.Now, "BTC", 1, 1, DateTime.Now, null)
                            }
                        }
                    },
                    new CapitalGainsDTO(1, 5, -1)),

                    new InventoryDTO(
                    "Another mock inventory that was converted to DTO",
                    InventoryType.FILO,
                    this._IdProviderService.GetNext(IdType.Inventory),
                    Exchange.Undefined,
                    new Dictionary<string, IEnumerable<InventoryItemDTO>>()
                    {
                        {  "XRP",
                            new List<InventoryItemDTO>()
                            {
                                new InventoryItemDTO(this._IdProviderService.GetNext(IdType.CurrencyAmount), null, DateTime.Now, "XRP", 100, 14, DateTime.Now, null)
                            }
                        }
                    },
                    new CapitalGainsDTO(10, 7, -4))
                };

            this._BeginningInventories = dtoInventories.Select(dto => this.instantiateInventory(dto));
        }

        private IInventory instantiateInventory(InventoryDTO dto)
        {
            return this._InventoryFactory.Create(this._IdProviderService, this._ReportingService, this._LoggingService, dto);
        }

        private InventoryManagerService instantiateManagerService()
        {
            return new InventoryManagerService(this._IdProviderService, this._MockRunService.Object, this._InventoryFactory, this._ReportingService, this._LoggingService);
        }
    }
}
