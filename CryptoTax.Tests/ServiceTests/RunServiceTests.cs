﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Persistence.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Attributes;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UtilityLibrary.Collections;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ServiceTests
{
    [TestClass]
    public class RunServiceTests
    {
        private static readonly Type[] _STRING_TYPES = new[] { typeof(string), typeof(DString), typeof(NString) };
        private static readonly Func<DString, DString> _OUTPUT_FOLDER = runId => $"Output/{runId}/";

        private RunService _RunService;
        private IdProviderService _IdProviderService;
        private MockFileService _FileService;
        private IReportingService _ReportingService;
        private RunPersistorService _RunPersistor;
        private MockLoggingService _LoggingService;

        private IEnumerable<InventoryDTO> _MockInventoryDTOs;

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._FileService = new MockFileService();
            this._ReportingService = new MockTrackingReportingService();
            this._RunPersistor = new RunPersistorService(this._FileService, Constants.OUTPUT_DATA_PATH);
            this._LoggingService = new MockLoggingService();
            this._RunService = new RunService(this._IdProviderService, this._RunPersistor, this._LoggingService);

            this._MockInventoryDTOs = new List<InventoryDTO>()
                {
                    new InventoryDTO(
                    "Some mock inventory that was converted to DTO",
                    InventoryType.FILO,
                    this._IdProviderService.GetNext(IdType.Inventory),
                    Exchange.Undefined,
                    new Dictionary<string, IEnumerable<InventoryItemDTO>>()
                    {
                        {  "BTC",
                            new List<InventoryItemDTO>()
                            {
                                new InventoryItemDTO(this._IdProviderService.GetNext(IdType.CurrencyAmount), null, DateTime.Now, "BTC", 1, 1, DateTime.Now, null)
                            }
                        }
                    },
                    new CapitalGainsDTO(0, 0, 0)),

                    new InventoryDTO(
                    "Another mock inventory that was converted to DTO",
                    InventoryType.FILO,
                    this._IdProviderService.GetNext(IdType.Inventory),
                    Exchange.Undefined,
                    new Dictionary<string, IEnumerable<InventoryItemDTO>>()
                    {
                        {  "XRP",
                            new List<InventoryItemDTO>()
                            {
                                new InventoryItemDTO(this._IdProviderService.GetNext(IdType.CurrencyAmount), null, DateTime.Now, "XRP", 100, 14, DateTime.Now, null)
                            }
                        }
                    },
                    new CapitalGainsDTO(10, 7, -4))
                };
        }

        [TestMethod]
        public void EnsureRunInfoObjectCanBeSetUsingArgs()
        {
            // each property in RunInfo that has an ArgKeyAttribute must be assignable using args (either by converting a string to the correct value, or calling an object's constructor and providing a single string)

            IEnumerable<PropertyInfo> properties = typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties.Where(prop => prop.GetCustomAttribute<ArgKeyAttribute>() != null);

            foreach (PropertyInfo prop in properties)
            {
                Type type = prop.PropertyType;

                if (type.IsPrimitive || _STRING_TYPES.Contains(type))
                {
                    // all good
                }
                else if (type.IsEnum)
                {
                    // ensure that all enum values have an ArgEnumValueAttribute
                    IEnumerable<FieldInfo> invalidFields = type.GetFields(BindingFlags.Static | BindingFlags.Public)
                        .Where(field => field.GetCustomAttribute<ArgEnumValueAttribute>() == null);
                    Assert.IsFalse(invalidFields.Any(), $"Property '{prop.Name}' is an enum of type '{type.Name}', but one or more enum fields are missing the ArgEnumValueAttribute: {string.Join(", ", invalidFields.Select(f => $"'{f.Name}'"))}");
                }
                else if (type.IsGenericType)
                {
                    // the only generic types we allow are nullable primitives or enums.
                    Assert.AreEqual(typeof(Nullable<>), type.GetGenericTypeDefinition(), $"Property '{prop.Name}' is a generic type other than Nullable<T>. This is not allowed.");

                    Type[] genericArguments = type.GetGenericArguments();
                    Assert.AreEqual(1, genericArguments.Length, $"Property '{prop.Name}' is generic but takes more than one type argument. This is not allowed.");
                    Assert.IsTrue(genericArguments[0].IsPrimitive || genericArguments[0].IsEnum, $"Property '{prop.Name}' is a nullable generic, but its type argument is not primitive.");
                }
                else
                {
                    // ensure a constructor exists that takes only a string
                    ConstructorInfo constructor = _STRING_TYPES.Select(t => type.GetConstructor(new[] { t })).Where(c => c != null).FirstOrDefault();

                    Assert.IsNotNull(constructor, $"Property '{prop.Name}' of type '{type.Name}' is neither a primite nor an enum, and does not have a constructor that takes only a single string.");
                }
            }
        }

        [TestMethod]
        public void SampleArgs()
        {
            string[] args = new[]
            {
                "name=null",
                "financialYear=2019-2020",
                "decimalPlaces=null",
                "roundingType=down",
                "consoleLoggingTemplate=null"
            };

            this._RunService.Initialise(args);

            Assert.AreEqual(args, this._RunService.GetOriginalArgs());
            Assert.AreEqual(null, this._RunService.GetName());
            Assert.AreEqual("2019-2020", this._RunService.GetFinancialYear().Name.Value);
            Assert.IsNull(this._RunService.GetDecimalPlaces());
            Assert.AreEqual(RoundingType.Down, this._RunService.GetRoundingType());
            Assert.AreEqual(null, this._RunService.GetConsoleLoggingTemplate());
        }

        [TestMethod]
        public void IncorrectKeyInArgsThrowsException()
        {
            string[] args = new[] { "interal=a" };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void SecondaryKeyInArgsIsRecognised()
        {
            string[] args = new[] { "interval=1h" };

            this._RunService.Initialise(args);

            Assert.AreEqual(CandlestickInterval.Hour_1, this._RunService.GetCandlestickInterval());
        }

        [TestMethod]
        public void IncorrectValueInArgsThrowsException()
        {
            string[] args = new[] { "candlestickInterval=abcdefg" };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void IncorrectlyForattedRunIdThrowsException()
        {
            string[] args = new[] { "ref=abcdefg" };

            Assert.ThrowsException<Exception>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void NonExistentRunIdForRefThrowsException()
        {
            string[] args = new[] { "ref=#n0" };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void NonExistentRunIdForStateThrowsException()
        {
            string[] args = new[] { "state=#n0" };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void RunExistsButStateDoesNotThrowsExceptionOnStateArg()
        {
            RunPersistorService persistor = new RunPersistorService(this._FileService, _OUTPUT_FOLDER);

            RunInfoDTO oldInfo = new RunInfoDTO(this._IdProviderService.GetNext(IdType.Run), new string[] { }, roundingType: RoundingType.MinimiseGains);
            MockRunService oldRunService = new MockRunService(this._IdProviderService, persistor, this._LoggingService, oldInfo, null);
            oldRunService.Save();

            string[] args = new[] { "state=" + oldInfo.Id.Value };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void StateExistsButRunDoesNotThrowsExceptionOnRefArg()
        {
            RunPersistorService persistor = new RunPersistorService(this._FileService, _OUTPUT_FOLDER);

            RunInfoDTO oldInfo = new RunInfoDTO(this._IdProviderService.GetNext(IdType.Run), new string[] { }, roundingType: RoundingType.MinimiseGains);
            MockRunService oldRunService = new MockRunService(this._IdProviderService, persistor, this._LoggingService, oldInfo, new RunStateDTO(oldInfo.Id, new List<InventoryDTO>() { null }));
            oldRunService.SaveState();

            string[] args = new[] { "ref=#n0" };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [DataTestMethod]
        [DynamicData(nameof(getAllTestCases), DynamicDataSourceType.Method)]
        public void ArgParsingTests(string key, string testValue, Func<IRunService, bool> validation)
        {
            string[] args = new[] { $"{key}={testValue}" };
            this._RunService.Initialise(args);
            Assert.IsTrue(validation(this._RunService));
        }

        [TestMethod]
        public void RefCanBeSetUsingArgs()
        {
            RunInfoDTO oldInfo = new RunInfoDTO(this._IdProviderService.GetNext(IdType.Run), new string[] { }, roundingType: RoundingType.MinimiseGains);
            MockRunService oldRunService = new MockRunService(this._IdProviderService, this._RunPersistor, this._LoggingService, oldInfo, null);
            oldRunService.Save();

            RunService newRunService = this._RunService;
            string[] args = new[]
            {
                $"ref={oldInfo.Id}"
            };
            newRunService.Initialise(args);

            Assert.IsTrue(compareRunInfo(oldInfo, runInfoFromService(newRunService), false, false, true, true));
            Assert.AreNotEqual(oldInfo.Id.Value, newRunService.GetRunId().Value);
            Assert.AreEqual(args, newRunService.GetOriginalArgs());
        }

        [TestMethod]
        public void StateCanBeSetUsingArgs()
        {
            RunInfoDTO oldInfo = new RunInfoDTO(this._IdProviderService.GetNext(IdType.Run), new string[] { }, roundingType: RoundingType.MinimiseGains);
            RunStateDTO oldState = new RunStateDTO(oldInfo.Id, new List<InventoryDTO>() { null });
            MockRunService oldRunService = new MockRunService(this._IdProviderService, this._RunPersistor, this._LoggingService, oldInfo, oldState);
            oldRunService.Save();
            oldRunService.SaveState();

            RunService newRunService = this._RunService;
            string[] args = new[]
            {
                $"state={oldInfo.Id}"
            };
            newRunService.Initialise(args);

            Assert.AreEqual(oldState.RunId.Value, newRunService.GetBeginningState().RunId.Value);
            Assert.AreNotEqual(oldInfo.Id.Value, newRunService.GetRunId().Value);
            Assert.IsTrue(compareRunState(oldState, newRunService.GetBeginningState()));
        }

        [TestMethod]
        public void ContKeywordWorksCorrectly()
        {
            // check: ref is set correctly (same property as oldInfo), new beginning state is same as old state, financial year is incremented
            RunInfoDTO oldInfo = new RunInfoDTO(this._IdProviderService.GetNext(IdType.Run), new string[] { }, roundingType: RoundingType.MinimiseGains, financialYear: new FinancialYear(2018));
            RunStateDTO oldState = new RunStateDTO(oldInfo.Id, new List<InventoryDTO>() { null });
            MockRunService oldRunService = new MockRunService(this._IdProviderService, this._RunPersistor, this._LoggingService, oldInfo, oldState);
            oldRunService.Save();
            oldRunService.SaveState();

            RunService newRunService = this._RunService;
            string[] args = new[]
            {
                $"cont={oldInfo.Id}"
            };
            newRunService.Initialise(args);

            // check that state was carried forward
            Assert.AreEqual(oldState.RunId.Value, newRunService.GetBeginningState().RunId.Value);
            Assert.IsTrue(compareRunState(oldState, newRunService.GetBeginningState()));

            // check that this is indeed a new run object that is based on the old one
            Assert.AreNotEqual(oldInfo.Id.Value, newRunService.GetRunId().Value);
            Assert.IsTrue(compareRunInfo(oldInfo, runInfoFromService(newRunService), false, false, false, false));

            // check that financial year was incremented
            Assert.AreEqual((oldInfo.FinancialYear + 1).Name.Value, newRunService.GetFinancialYear().Name.Value);
        }

        [TestMethod]
        public void UsingContWithMultipleArgsThrowsException()
        {
            string[] args = new[]
            {
                "cont=#n0",
                "name=test"
            };

            Assert.ThrowsException<ArgumentException>(() => this._RunService.Initialise(args));
        }

        [TestMethod]
        public void EnsureAllKeysAreUnique()
        {
            IEnumerable<string> allKeys = typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties
                .Where(prop => prop.GetCustomAttribute<ArgKeyAttribute>() != null)
                .SelectMany(prop => prop.GetCustomAttribute<ArgKeyAttribute>().Names);
            Dictionary<string, int> counts = new Dictionary<string, int>();
            foreach (string key in allKeys)
            {
                if (counts.ContainsKey(key))
                {
                    counts[key]++;
                }
                else
                {
                    counts.Add(key, 1);
                }
            }

            string errorMessage = string.Join(", ", counts.Where(kvp => kvp.Value > 1).Select(kvp => $"{kvp.Key} appears {kvp.Value} times"));
            Assert.IsTrue(string.IsNullOrEmpty(errorMessage), $"RunInfo keys are not unique: {errorMessage}");
        }

        [TestMethod]
        public void EnsureEnumValuesAreUnique()
        {
            IEnumerable<PropertyInfo> enums = typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties
                .Where(prop => prop.GetCustomAttribute<ArgKeyAttribute>() != null && prop.PropertyType.IsEnum);

            foreach (PropertyInfo enumType in enums)
            {
                IEnumerable<string> keys = enumType.PropertyType.GetFields(BindingFlags.Static | BindingFlags.Public)
                    .SelectMany(field => field.GetCustomAttribute<ArgEnumValueAttribute>().Names);

                Assert.AreEqual(keys.Count(), keys.Distinct().Count(), $"ArgEnumValueAttribute in type {enumType.Name} have duplicate names");
            }
        }

        [TestMethod]
        public void LoadedInventoriesHaveCapitalGainsReset()
        {
            DString firstId = this._IdProviderService.GetNext(IdType.Run);
            RunStateDTO firstState = new RunStateDTO(firstId, this._MockInventoryDTOs);
            RunInfoDTO firstRunInfo = new RunInfoDTO(firstId, new string[] { });
            this._RunPersistor.Save(firstRunInfo);
            this._RunPersistor.SaveState(firstState);

            this._RunService.Initialise(new[] { $"state={firstId}" });
            IEnumerable<IInventory> loadedInventories = this._RunService.LoadBeginningInventories(new MockImmutableInventoryFactory(), this._ReportingService);
            Assert.IsFalse(loadedInventories.Where(inv => inv.NetCapitalGains.StandardCapitalGains != 0
                || inv.NetCapitalGains.DiscountableCapitalGains != 0
                || inv.NetCapitalGains.CapitalLosses != 0).Any());
        }

        [TestMethod]
        public void LoadedInventoriesConserveContentsWhenManagementTypeDiffers()
        {
            // for every combination of inventory management types, make sure that the contents of the previous state match those of the loaded inventory.

            // for now, we don't support changing inventory management type during a run sequence.
            foreach (InventoryManagementType firstManagementType in Enum.GetValues(typeof(InventoryManagementType)))
            {
                DString firstId = this._IdProviderService.GetNext(IdType.Run);
                RunStateDTO firstState = new RunStateDTO(firstId, this._MockInventoryDTOs);
                RunInfoDTO firstRunInfo = new RunInfoDTO(firstId, new string[] { }, inventoryManagementType: firstManagementType);
                this._RunPersistor.Save(firstRunInfo);
                this._RunPersistor.SaveState(firstState);

                foreach (InventoryManagementType secondManagementType in Enum.GetValues(typeof(InventoryManagementType)))
                {
                    if (secondManagementType == firstManagementType) continue; // allowed :)

                    this._RunService.Initialise(new[]
                    {
                        $"state={firstId}",
                        $"managementType={secondManagementType}"
                    });
                    Assert.ThrowsException<NotImplementedException>(() => this._RunService.LoadBeginningInventories(new MockImmutableInventoryFactory(), this._ReportingService));
                }
            }
        }

        [TestMethod]
        public void LoadedInventoriesConserveContentsWhenInventoryTypeDiffers()
        {
            // for every combination of inventory types, make sure that the contents of the previous state match those of the loaded inventory.

            // for now, we don't support changing inventory type during a run sequence.
            foreach (InventoryType firstType in Enum.GetValues(typeof(InventoryType)))
            {
                DString firstId = this._IdProviderService.GetNext(IdType.Run);
                RunStateDTO firstState = new RunStateDTO(firstId, this._MockInventoryDTOs);
                RunInfoDTO firstRunInfo = new RunInfoDTO(firstId, new string[] { }, inventoryType: firstType);
                this._RunPersistor.Save(firstRunInfo);
                this._RunPersistor.SaveState(firstState);

                foreach (InventoryType secondType in Enum.GetValues(typeof(InventoryType)))
                {
                    if (secondType == firstType) continue; // allowed :)

                    this._RunService.Initialise(new[]
                    {
                        $"state={firstId}",
                        $"inventoryType={secondType}"
                    });
                    Assert.ThrowsException<NotImplementedException>(() => this._RunService.LoadBeginningInventories(new MockImmutableInventoryFactory(), this._ReportingService));
                }
            }
        }

        [TestMethod]
        public void CurrentStateUpdatesAndPersistsCorrectly()
        {
            // set up
            this._RunService.Initialise(new string[] { });
            this._RunService.Save();
            this._RunService.SaveState();
            RunStateDTO initialState = this._RunPersistor.LoadState(this._RunService.GetRunId());
            Assert.AreEqual(0, initialState.Inventories?.Count() ?? 0);

            // update state
            this._RunService.UpdateCurrentInventories(this._MockInventoryDTOs.Select(dto => new MockImmutableInventory(this._IdProviderService, dto)));
            this._RunService.SaveState();
            RunStateDTO updatedState = this._RunPersistor.LoadState(this._RunService.GetRunId());
            Assert.AreEqual(this._MockInventoryDTOs.Count(), updatedState.Inventories.Count());

            // next run should continue from the previous state
            RunService contRunService = new RunService(this._IdProviderService, this._RunPersistor, this._LoggingService);
            contRunService.Initialise(new string[] { $"cont={this._RunService.GetRunId()}" });
            IEnumerable<IInventory> loadedInventories = contRunService.LoadBeginningInventories(new MockImmutableInventoryFactory(), this._ReportingService);
            List<InventoryDTO> expectedInventories = this._MockInventoryDTOs.ToList();
            expectedInventories.ForEach(dto => dto.CapitalGains.Reset_Do_Not_Use());
            Assert.AreEqual(this._MockInventoryDTOs.Count(),
                loadedInventories.Select(inv => compareInventories(
                    inv.ToDTO(),
                    expectedInventories.Where(dto => dto.Id == inv.Id).Single()))
                .Where(result => result == true).Count());
        }

        [TestMethod]
        public void RunInfoCannotBeOverwritten()
        {
            this._RunService.Initialise(new string[] { });
            this._RunService.Save();
            Assert.ThrowsException<FileAlreadyExistsException>(() => this._RunService.Save());
        }

        [TestMethod]
        public void RunServiceSavesCalculationResultsWithCorrectId()
        {
            decimal assessableIncome = 101;
            CapitalGainsDTO capitalGainsDTO = new CapitalGainsDTO(1, 2, -5);
            CapitalGains capitalGains = new CapitalGains(capitalGainsDTO);
            CalculationResults results = new CalculationResults(capitalGains, assessableIncome);

            RunService runService = new RunService(this._IdProviderService, this._RunPersistor, this._LoggingService);
            runService.Initialise(new string[] { });

            // act
            runService.SaveResults(results);

            // assert
            Dictionary<DString, DString> files = this._FileService.FileSystem_Files;
            string fileName = files.Single().Key;
            string contents = files.Single().Value;
            string expectedContents = JsonConvert.SerializeObject(results.ToDTO());

            Assert.IsTrue(fileName.EndsWith("#n0\\Results.json"));
            Assert.AreEqual(expectedContents, contents);
        }

        [TestMethod]
        public void RunState_OnlyUpdatesIfExplicit()
        {
            // set up
            Mock<IRunPersistorService> runPersistor = new Mock<IRunPersistorService>();
            RunService runService = new RunService(this._IdProviderService, runPersistor.Object, this._LoggingService);
            runService.Initialise(new string[] { });
            IInventory inventory = new FILOInventoryFactory().Create(this._IdProviderService, this._ReportingService, this._LoggingService, Exchange.Undefined, "test inventory");

            // add initial empty inventory
            runService.UpdateCurrentInventories(new List<IInventory>() { inventory });
            runService.SaveState();
            runPersistor.Verify(r => r.SaveState(It.Is<RunStateDTO>(dto => dto.Inventories.Single().Items.Count == 0)));
            runPersistor.Reset();

            // update inventory, but state should still be empty
            inventory.Purchase(new CurrencyAmount(this._IdProviderService, "XRP", 1, 1, DateTime.Now));
            runService.SaveState();
            runPersistor.Verify(r => r.SaveState(It.Is<RunStateDTO>(dto => dto.Inventories.Single().Items.Count == 0)));
            runPersistor.Reset();

            // update state, should now reflect 1 item
            runService.UpdateCurrentInventories(new List<IInventory>() { inventory });
            runService.SaveState();
            runPersistor.Verify(r => r.SaveState(It.Is<RunStateDTO>(dto => dto.Inventories.Single().Items.Count == 1)));
        }

        /// <summary>
        /// Returns a collection of test arg values and verification functions for the given arg key (only uses the first, most verbose, arg key of properties).
        /// </summary>
        private static Dictionary<string, Func<IRunService, bool>> getTestCasesForKey(string key)
        {
            Dictionary<string, Func<IRunService, bool>> dict = new Dictionary<string, Func<IRunService, bool>>();

            switch (key)
            {
                case "name":
                    dict.Add("test", x => x.GetName() == "test");
                    dict.Add("", x => x.GetName() == "");
                    dict.Add("null", x => x.GetName() == null);
                    break;
                case "financialyear":
                    dict.Add("2020", x => x.GetFinancialYear().Name == "2020-2021");
                    dict.Add("2017-2018", x => x.GetFinancialYear().Name == "2017-2018");
                    break;
                case "inventorytype":
                    dict = getTestCasesForEnum(x => x.GetInventoryType());
                    break;
                case "candlestickinterval":
                    dict = getTestCasesForEnum(x => x.GetCandlestickInterval());
                    break;
                case "pricecalculationtype":
                    dict = getTestCasesForEnum(x => x.GetPriceCalculationType());
                    break;
                case "inventorymanagementtype":
                    dict = getTestCasesForEnum(x => x.GetInventoryManagementType());
                    break;
                case "decimalplaces":
                    dict.Add("null", x => x.GetDecimalPlaces() == null);
                    dict.Add("0", x => x.GetDecimalPlaces() == 0);
                    dict.Add("8", x => x.GetDecimalPlaces() == 8);
                    break;
                case "roundingtype":
                    dict = getTestCasesForEnum(x => x.GetRoundingType());
                    break;
                case "enablereporting":
                    dict.Add("false", x => x.GetEnableReporting() == false);
                    dict.Add("true", x => x.GetEnableReporting() == true);
                    break;
                case "consoleloggingtemplate":
                    dict.Add("null", x => x.GetConsoleLoggingTemplate() == null);
                    dict = getTestCasesForEnum(x => x.GetConsoleLoggingTemplate().Value);
                    break;
                case "persistorloggingtemplate":
                    dict.Add("null", x => x.GetPersistorLoggingTemplate() == null);
                    dict = getTestCasesForEnum(x => x.GetPersistorLoggingTemplate().Value);
                    break;
                default:
                    throw new AssertUnreachable(key, $"No test cases exist for key '{key}'");
            }

            return dict;
        }

        /// <summary>
        /// Constructs test cases for every possible enum arg name of the given type.
        /// </summary>
        private static Dictionary<string, Func<IRunService, bool>> getTestCasesForEnum<E>(Func<IRunService, E> getEnumValue) where E : Enum
        {
            Type enumType = typeof(E);

            Dictionary<string, Func<IRunService, bool>> dict = new Dictionary<string, Func<IRunService, bool>>();

            foreach (FieldInfo field in enumType.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                foreach (string name in field.GetCustomAttribute<ArgEnumValueAttribute>().Names)
                {
                    dict.Add(name, x => getEnumValue(x).Equals(field.GetValue(null)));
                }
            }

            return dict;
        }

        /// <summary>
        /// Comprehensive coverage of all applicable properties of RunInfo.
        /// </summary>
        private static IEnumerable<object[]> getAllTestCases()
        {
            // only use the first (most verbose) name of each attribute - assume the other ones work fine.
            IEnumerable<string> keys = typeof(RunInfoDTO).GetTypeInfo().DeclaredProperties
                .Select(prop => prop.GetCustomAttribute<ArgKeyAttribute>()?.Names.First() ?? null)
                .Where(k => k != null);

            foreach (string key in keys)
            {
                foreach (KeyValuePair<string, Func<IRunService, bool>> testCase in getTestCasesForKey(key))
                {
                    string testValue = testCase.Key;
                    Func<IRunService, bool> validation = testCase.Value;

                    yield return new object[]
                    {
                        key,
                        testValue,
                        validation
                    };
                }
            }
        }

        // todo TAX-105: automate object equivalence comparisons using reflection, since this is impossible to maintain when new properties are added. add to UtilityLibrary

        private static bool compareRunInfo(RunInfoDTO first, RunInfoDTO second, bool checkId, bool checkArgs, bool checkState, bool checkFinancialYear)
        {
            bool propertiesSame =
                first.Name == second.Name &&
                first.InventoryType == second.InventoryType &&
                first.CandlestickInterval == second.CandlestickInterval &&
                first.PriceCalculationType == second.PriceCalculationType &&
                first.InventoryManagementType == second.InventoryManagementType &&
                first.DecimalPlaces == second.DecimalPlaces &&
                first.RoundingType == second.RoundingType &&
                first.EnableReporting == second.EnableReporting;

            bool stateOk = checkState ? compareRunState(first.BeginningState, second.BeginningState) : true;
            bool idOk = checkId ? first.Id == second.Id : true;
            bool argsOk = checkArgs ? first.OriginalArgs.CompareSequences(second.OriginalArgs, (x, y) => x == y, true) : true;
            bool yearOk = checkFinancialYear ? first.FinancialYear.Name == second.FinancialYear.Name : true;

            return propertiesSame && stateOk && idOk && argsOk && yearOk;
        }

        private static bool compareRunState(RunStateDTO first, RunStateDTO second)
        {
            if (first == null || second == null) return first == second;

            return
                first.RunId == second.RunId &&
                first.Inventories.CompareSequences(second.Inventories, (x, y) => compareInventories(x, y), true);
        }

        private static bool compareInventories(InventoryDTO first, InventoryDTO second)
        {
            if (first == null || second == null) return first == second;

            return
                first.Id == second.Id &&
                first.Name == second.Name &&
                first.Type == second.Type &&
                first.Exchange == second.Exchange &&
                compareCapitalGains(first.CapitalGains, second.CapitalGains) &&
                first.Items.Keys.CompareSequences(second.Items.Keys, (x, y) => x == y, false) &&
                first.Items.Values.CompareSequences(second.Items.Values, (x, y) => compareInventoryItems(x, y), false);
        }

        private static bool compareCapitalGains(CapitalGainsDTO first, CapitalGainsDTO second)
        {
            return first.StandardCapitalGains == second.StandardCapitalGains
                && first.DiscountableCapitalGains == second.DiscountableCapitalGains
                && first.CapitalLosses == second.CapitalLosses;
        }

        private static bool compareInventoryItems(IEnumerable<InventoryItemDTO> first, IEnumerable<InventoryItemDTO> second)
        {
            return first.CompareSequences(second, (x, y) =>
            {
                return
                    x.Amount == y.Amount &&
                    x.Currency == y.Currency &&
                    x.ExchangeAcquired == y.ExchangeAcquired &&
                    x.Id == y.Id &&
                    x.OriginalId == y.OriginalId &&
                    x.TimeModified == y.TimeModified &
                    x.Value == y.Value;
            }, true);
        }

        private static RunInfoDTO runInfoFromService(RunService service)
        {
            // since we didn't name our parameters, we will get a build error here if the method signature changes (unless a parameter is added to the end of the list...)
            return new RunInfoDTO(
                service.GetRunId(),
                service.GetOriginalArgs(),
                service.GetName(),
                service.GetFinancialYear(),
                service.GetInventoryType(),
                service.GetCandlestickInterval(),
                service.GetPriceCalculationType(),
                service.GetInventoryManagementType(),
                service.GetDecimalPlaces(),
                service.GetRoundingType(),
                service.GetEnableReporting(),
                service.GetConsoleLoggingTemplate(),
                service.GetPersistorLoggingTemplate(),
                service.GetBeginningState());
        }
    }
}
