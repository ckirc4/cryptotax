﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using CryptoTax.Tests.Mocks;
using CryptoTax.Shared.Enums;
using CryptoTax.DTO;
using System.Reflection;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using GenerationService = CryptoTax.Domain.Services.ReportRowGenerationService;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Domain.Inventory;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class ReportingTests
    {
        private static IdProviderService _IdProviderService;
        private static GenerationService _ReportRowGenerationService;
        private static readonly DateTime _Time = new DateTime(2021, 1, 1);
        private static readonly ExchangeSymbol _SymbolBTCUSDT = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance);
        private static readonly ExchangeSymbol _SymbolBTCAUD = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.BTC), Exchange.BTCMarkets);
        private static readonly FinancialYear _FinancialYear = new FinancialYear("2018-2019");

        private static IInventory _SampleInventory1;
        private static IInventory _SampleInventory2;
        private static Func<decimal, CurrencyAmount> _GetCA;

        private static ReportingAddData _ReportingAddData;
        private static ReportingDisposeData _ReportingDisposeData;
        private static ReportingMergeData _ReportingMergeData;
        private static ReportingPurchaseData _ReportingPurchaseData;
        private static ReportingRemoveData _ReportingRemoveData;
        private static ReportingSplitData _ReportingSplitData;
        private static ReportingTransferData _ReportingTransferData;
        private static ReportingConversionData _ReportingConversionData;
        private static ReportingCreateData _ReportingCreateData;
        private static ReportingBurnData _ReportingBurnData;
        private static ReportingPriceData _ReportingPriceData;
        private static ReportingStartRunData _ReportingStartRunData;
        private static ReportingEndRunData _ReportingEndRunData;
        private static ReportingLoadInventoriesData _ReportingLoadInventoriesData;

        [TestInitialize]
        public void Init()
        {
            staticInit();
        }

        private static void staticInit()
        {
            _IdProviderService = new IdProviderService(0);
            _ReportRowGenerationService = new GenerationService(_IdProviderService);

            _GetCA = (i) => new CurrencyAmount(_IdProviderService, "BTC", 10, 100.01m + i, _Time, Exchange.Binance);
            CurrencyAmount ca() => _GetCA(1);

            _SampleInventory1 = createSampleInventory(1, _IdProviderService, _Time);
            _SampleInventory2 = createSampleInventory(2, _IdProviderService, _Time);
            DString previousRunId = _IdProviderService.GetNext(IdType.Run);
            DString runId = _IdProviderService.GetNext(IdType.Run);

            _ReportingSplitData = new ReportingSplitData(_IdProviderService, _Time, ca(), ca(), ca());
            _ReportingMergeData = new ReportingMergeData(_IdProviderService, _Time, ca(), ca(), ca());
            _ReportingAddData = new ReportingAddData(_IdProviderService, _Time, ca(), _SampleInventory1);
            _ReportingRemoveData = new ReportingRemoveData(_IdProviderService, _Time, ca(), _SampleInventory1, ca(), false, false);
            _ReportingPurchaseData = new ReportingPurchaseData(_IdProviderService, _Time, ca());
            _ReportingDisposeData = new ReportingDisposeData(_IdProviderService, _Time, ca(), DisposalType.SpotSale, ca());
            _ReportingTransferData = new ReportingTransferData(_IdProviderService, _Time, ca(), _SampleInventory1, _SampleInventory2, "hash1", ca(), ca());
            _ReportingConversionData = new ReportingConversionData(_IdProviderService, _Time, _SampleInventory1, ConvertReason.FtxStablecoinDeposit, ca(), ca());
            _ReportingCreateData = new ReportingCreateData(_IdProviderService, _Time, ca(), CreateReason.LoanInterestIncome, ca());
            _ReportingBurnData = new ReportingBurnData(_IdProviderService, _Time, ca(), BurnReason.Disposal, ca());
            _ReportingPriceData = new ReportingPriceData(_IdProviderService, _Time, new[] { (_SymbolBTCUSDT, 55000.05m), (_SymbolBTCAUD, 72412.45m) });
            _ReportingStartRunData = new ReportingStartRunData(_IdProviderService, _Time, runId, _FinancialYear);
            _ReportingEndRunData = new ReportingEndRunData(_IdProviderService, _Time, runId, _FinancialYear, new CapitalGains(new CapitalGainsDTO(1050.1m, 123, -0.5m)), 1500.5m);
            _ReportingLoadInventoriesData = new ReportingLoadInventoriesData(_IdProviderService, _Time, previousRunId, new List<IInventory>() { _SampleInventory1, _SampleInventory2 });
        }

        private static IInventory createSampleInventory(int i, IdProviderService idProviderService, DateTime time)
        {
            if (i == 1)
            {
                return new MockImmutableInventory(idProviderService, Exchange.Undefined, "Mock inventory 1", new Dictionary<DString, IEnumerable<CurrencyAmount>>()
                {
                    {
                        "BTC",
                        new List<CurrencyAmount>()
                        {
                            new CurrencyAmount(idProviderService, "BTC", 1, 10000, time),
                            new CurrencyAmount(idProviderService, "BTC", 0.5343m, 8005.01m, time),
                            new CurrencyAmount(idProviderService, "BTC", 0.101m, 7000.50m, time)
                        }
                    },
                    {
                        "XRP",
                        new List<CurrencyAmount>()
                        {
                            new CurrencyAmount(idProviderService, "XRP", 18845, 5000, time)
                        }
                    }
                });
            }
            else if (i == 2)
            {
                return new MockImmutableInventory(idProviderService, Exchange.Undefined, "Mock inventory 2", new Dictionary<DString, IEnumerable<CurrencyAmount>>()
                {
                    {
                        "BTC",
                        new List<CurrencyAmount>()
                        {
                            new CurrencyAmount(idProviderService, "BTC", 0.1m, 9000, time),
                            new CurrencyAmount(idProviderService, "BTC", 0.0043m, 8000, time),
                            new CurrencyAmount(idProviderService, "BTC", 0.05m, 7000, time)
                        }
                    },
                    {
                        "LTC",
                        new List<CurrencyAmount>()
                        {
                            new CurrencyAmount(idProviderService, "LTC", 18845, 5000, time)
                        }
                    }
                });
            }
            else throw new NotImplementedException();
        }

        [TestMethod]
        public void EnsureDataTypeExistsForEachRowType()
        {
            foreach (ReportRowType rowType in Enum.GetValues(typeof(ReportRowType)))
            {
                ReportingBaseData baseData = getReportRowDataInstance(rowType);

                Assert.IsNotNull(baseData, $"No data type implemented for ReportRowType '{rowType}'");
                Assert.AreEqual(rowType, baseData.Type, $"Data model '{nameof(baseData)}' implements ReportRowType '{baseData.Type}' but should be implementing type '{rowType}'");
                Assert.IsNotNull(baseData.Transform(), $"Unable to transform base data of row type '{rowType}' back into its full data type");
            }
        }

        [TestMethod]
        public void SanityCheckAllowedCombos()
        {
            foreach (ReportRowType rowType in Enum.GetValues(typeof(ReportRowType)))
            {
                IEnumerable<ReportRowType[]> allowedCombos = GenerationService.GetAllowedCombos(rowType);
                Assert.IsFalse(allowedCombos.Where(c1 => allowedCombos.Where(c2 => c1.SequenceEqual(c2)).Count() != 1).Any(), "Message combos must be unique");

                foreach (ReportRowType[] combo in allowedCombos)
                {
                    Assert.IsNotNull(combo, "Row type message combo must not be null.");
                    Assert.IsFalse(combo.Length == 0, "Row type message combo must not be empty.");
                    Assert.AreEqual(rowType, combo[0], $"The first row type in this combo must be '{rowType}', but is actually '{combo[0]}'");
                }
            }
        }

        [TestMethod]
        public void EnsureCorrectRowIsGeneratedForEachAllowedCombo()
        {
            foreach (ReportRowType rowType in Enum.GetValues(typeof(ReportRowType)))
            {
                IEnumerable<ReportRowType[]> allowedCombos = GenerationService.GetAllowedCombos(rowType);

                foreach (ReportRowType[] combo in allowedCombos)
                {
                    ReportingBaseData[] data = combo.Select(type => getReportRowDataInstance(type)).ToArray();
                    ReportRowDTO row = _ReportRowGenerationService.GenerateRow(data);

                    CollectionAssert.AreEqual(combo, row.Types);
                    Assert.IsFalse(string.IsNullOrEmpty(row.Id));
                    Assert.IsFalse(string.IsNullOrEmpty(row.Message));
                    Assert.AreEqual(_Time, row.Time);
                    Console.WriteLine($"{rowType} | [{row.Category}] {row.Message}");
                }
            }
        }

        [TestMethod]
        [DataTestMethod]
        [DynamicData(nameof(getConstructorTests), DynamicDataSourceType.Method)]
        public void ReportRowsContainCorrectReferences(ReportingBaseData data, IEnumerable<DString> messageShouldContain, IEnumerable<DString> messageShouldNotContain, bool capitalGainsShouldBeDefined, decimal? assessableIncome)
        {
            // some report rows should reference certain currency amounts. ensure that the generated messages do this. we don't compare the whole message against a predefined message, only search whether it contains the expected string snippets or not.

            ReportRowDTO row = _ReportRowGenerationService.GenerateRow(data);
            this.checkMessage(row, messageShouldContain, true);
            this.checkMessage(row, messageShouldNotContain, false);
            this.checkCapitalGains(row, capitalGainsShouldBeDefined);
            this.checkAssessableIncome(row, assessableIncome);
        }

        private void checkMessage(ReportRowDTO row, IEnumerable<DString> strings, bool shouldContain)
        {
            string message = row.Message;
            foreach (DString item in strings)
            {
                Assert.IsTrue(message.Contains(item) == shouldContain, $"Report row of type {row.Types[0]} should {(shouldContain ? "" : "not ")}contain the string '{item}'. Complete message: '{message}'");
            }
        }

        /// <summary>
        /// Does not check actual values - only null, or non-null
        /// </summary>
        private void checkCapitalGains(ReportRowDTO row, bool capitalGainsShouldBeDefined)
        {
            if (capitalGainsShouldBeDefined)
            {
                Assert.IsNotNull(row.StandardCapitalGains);
                Assert.IsNotNull(row.DiscountableCapitalGains);
                Assert.IsNotNull(row.CapitalLosses);
            }
            else
            {
                Assert.IsNull(row.StandardCapitalGains);
                Assert.IsNull(row.DiscountableCapitalGains);
                Assert.IsNull(row.CapitalLosses);
            }
        }

        private void checkAssessableIncome(ReportRowDTO row, decimal? expectedValue)
        {
            Assert.AreEqual(expectedValue, row.AssessableIncome);
        }

        private static IEnumerable<object[]> getConstructorTests()
        {
            // what a disgusting mess...
            staticInit();
            Type typeCA = typeof(CurrencyAmount);
            Type typeInv = typeof(IInventory);
            Type typeCG = typeof(CapitalGainsDelta);

            CurrencyAmount ca1 = _GetCA(1);
            CurrencyAmount ca2 = _GetCA(2);
            CurrencyAmount ca3 = _GetCA(3);

            foreach (ReportRowType rowType in Enum.GetValues(typeof(ReportRowType)))
            {
                object baseData = getReportRowDataInstance(rowType);
                // note: if baseData is null at this point, make sure to debug the ReportingTests method, NOT just a single test. for some reason, the TestInitialize method isn't called when running a single test.
                foreach (ConstructorInfo constructor in baseData.GetType().GetConstructors().Where(c => c.IsPublic))
                {
                    ParameterInfo[] allTypes = constructor.GetParameters();
                    if (allTypes[0].ParameterType != typeof(IdProviderService) || allTypes[1].ParameterType != typeof(DateTime)) throw new Exception($"Unexpected first two constructor inputs for report type {rowType}");
                    Type[] types = constructor.GetParameters().Skip(2).Select(info => info.ParameterType).ToArray();

                    // invokes the constructor (auto-filling the first two inputs), returning the row data object.
                    object invoke(params object[] parameters) => constructor.Invoke(new object[] { _IdProviderService, _Time }.Concat(parameters).ToArray());

                    List<DString> shouldContain = new List<DString>();
                    List<DString> shouldNotContain = new List<DString>();
                    bool cg = false;
                    decimal? ai = null; // assessable income
                    object data = null;

                    switch (rowType)
                    {
                        case ReportRowType.Split:
                        case ReportRowType.Merge:
                            if (compareTypes(types, typeCA, typeCA, typeCA))
                            {
                                data = invoke(ca1, ca2, ca3);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add(ca3.Id);
                                shouldNotContain.Add("exchange");
                            }
                            break;
                        case ReportRowType.Add:
                            if (compareTypes(types, typeCA, typeInv))
                            {
                                // add when purchasing/creating
                                data = invoke(ca1, _SampleInventory1);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldNotContain.Add("transferring");
                                shouldNotContain.Add("exchange");
                            }
                            else if (compareTypes(types, typeCA, typeInv, typeCA))
                            {
                                // add as part of transferring
                                data = invoke(ca1, _SampleInventory1, ca2);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add("transferring");
                                shouldNotContain.Add("exchange");
                            }
                            break;
                        case ReportRowType.Remove:
                            if (compareTypes(types, typeCA, typeInv, typeCA, typeCG, typeof(bool)))
                            {
                                // used for disposing - one with phantom set to false (normal disposal), the other with phantom set to true (when overdrawing)
                                data = invoke(ca1, _SampleInventory1, ca2, CapitalGainsDelta.ZERO, false);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldNotContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add("exchange");
                                cg = true;
                                yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                data = invoke(ca1, _SampleInventory1, ca2, CapitalGainsDelta.ZERO, true);
                                shouldNotContain.Remove(GenerationService.PHANTOM_NAME);
                                shouldContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add(ca1.Id); // phantom means that the currency amount will not be referenced, since it is only used internally
                                shouldContain.Remove(ca1.Id);
                            }
                            else if (compareTypes(types, typeCA, typeInv, typeCA, typeof(bool), typeof(bool)))
                            {
                                // used for burning
                                data = invoke(ca1, _SampleInventory1, ca2, false, false);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add("discarding");
                                shouldNotContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add("exchange");
                                yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                data = invoke(ca1, _SampleInventory1, ca2, true, false);
                                shouldNotContain.Remove(GenerationService.PHANTOM_NAME);
                                shouldContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add(ca1.Id); // phantom means that the currency amount will not be referenced, since it is only used internally
                                shouldContain.Remove(ca1.Id);
                                yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                shouldContain.Clear();
                                shouldNotContain.Clear();

                                // used for transferring
                                data = invoke(ca1, _SampleInventory1, ca2, false, true);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add("transferring");
                                shouldNotContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add("exchange");
                                yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                data = invoke(ca1, _SampleInventory1, ca2, true, true);
                                shouldNotContain.Remove(GenerationService.PHANTOM_NAME);
                                shouldContain.Add(GenerationService.PHANTOM_NAME);
                                shouldNotContain.Add(ca1.Id);
                                shouldContain.Remove(ca1.Id);
                            }
                            break;
                        case ReportRowType.Purchase:
                            if (compareTypes(types, typeCA))
                            {
                                data = invoke(ca1);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add("exchange");
                            }
                            break;
                        case ReportRowType.Dispose:
                            if (compareTypes(types, typeCA, typeof(DisposalType), typeof(CurrencyAmount?)))
                            {
                                foreach (DisposalType disposalType in Enum.GetValues(typeof(DisposalType)))
                                {
                                    shouldContain.Clear();
                                    shouldNotContain.Clear();

                                    // without fees
                                    data = invoke(ca1, disposalType, null);
                                    shouldContain.Add(ca1.Id);
                                    shouldContain.Add("exchange");
                                    switch (disposalType)
                                    {
                                        case DisposalType.SpotSale:
                                            shouldContain.Add("spot sale");
                                            break;
                                        case DisposalType.FuturesPnlLoss:
                                            shouldContain.Add("futures loss");
                                            break;
                                        case DisposalType.OptionsPnlLoss:
                                            shouldContain.Add("options loss");
                                            break;
                                        case DisposalType.FuturesFundingPayment:
                                            shouldContain.Add("perpetual futures funding");
                                            break;
                                        case DisposalType.DustConversionPayment:
                                            shouldContain.Add("dust");
                                            break;
                                        default:
                                            throw new NotImplementedException("Not implemented");
                                    }
                                    shouldNotContain.Add("fee");
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                    // with fees
                                    data = invoke(ca1, disposalType, ca2);
                                    shouldContain.Add(ca2.Id);
                                    shouldContain.Add("fee");
                                    shouldNotContain.Remove("fee");
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                                }
                                continue;
                            }
                            break;
                        case ReportRowType.Transfer:
                            if (compareTypes(types, typeCA, typeInv, typeInv, typeof(NString), typeof(CurrencyAmount?), typeof(CurrencyAmount?)))
                            {
                                // instant transfer
                                // with fees and with transaction id
                                data = invoke(ca1, _SampleInventory1, _SampleInventory2, (NString)"0x12345", ca2, ca3);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add(_SampleInventory2.Id);
                                shouldContain.Add(ca2.Id);
                                shouldContain.Add("exchange");
                                shouldContain.Add("withdrawal fee");
                                shouldContain.Add(ca3.Id);
                                shouldContain.Add("deposit fee");
                                shouldContain.Add("instantly");
                                shouldContain.Add("transaction id");
                                shouldContain.Add("0x12345");
                                yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();

                                // with fees and without transaction id
                                data = invoke(ca1, _SampleInventory1, _SampleInventory2, (NString)null, null, null);
                                shouldContain.Clear();
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add(_SampleInventory2.Id);
                                shouldContain.Add("exchange");
                                shouldNotContain.Add("withdrawal fee");
                                shouldNotContain.Add("deposit fee");
                                shouldContain.Add("instantly");
                                shouldNotContain.Add("transaction id");
                            }
                            else if (compareTypes(types, typeCA, typeInv, typeInv, typeof(NString), typeof(CurrencyAmount?), typeof(CurrencyAmount?), typeof(TimeSpan)))
                            {
                                // non-instant transfer with transaction id
                                data = invoke(ca1, _SampleInventory1, _SampleInventory2, (NString)"0x12345", ca2, ca3, new TimeSpan(3, 1, 0, 5));
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add(_SampleInventory2.Id);
                                shouldContain.Add("exchange");
                                shouldContain.Add("over a duration of 3 days, 1 hour, and 5 seconds");
                                shouldContain.Add("transaction id");
                                shouldContain.Add("0x12345");
                            }
                            break;
                        case ReportRowType.Convert:
                            if (compareTypes(types, typeInv, typeof(ConvertReason), typeCA, typeCA))
                            {
                                data = invoke(_SampleInventory1, ConvertReason.FtxStablecoinDeposit, ca1, ca2);
                                shouldContain.Add(_SampleInventory1.Id);
                                shouldContain.Add("deposit");
                                shouldContain.Add("FTX");
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add(ca2.Id);
                            }
                            break;
                        case ReportRowType.Create:
                            if (compareTypes(types, typeCA, typeof(CreateReason)))
                            {
                                foreach (CreateReason reason in Enum.GetValues(typeof(CreateReason)))
                                {
                                    shouldContain.Clear();
                                    shouldNotContain.Clear();
                                    data = invoke(ca1, reason);
                                    shouldContain.Add(ca1.Id);
                                    shouldContain.Add("exchange");
                                    switch (reason)
                                    {
                                        case CreateReason.FuturesFundingIncome:
                                            shouldContain.Add("funding");
                                            break;
                                        case CreateReason.FuturesPnlIncome:
                                            shouldContain.Add("futures PnL");
                                            break;
                                        case CreateReason.OptionsPnlIncome:
                                            shouldContain.Add("options PnL");
                                            break;
                                        case CreateReason.StakingIncome:
                                            shouldContain.Add("staking");
                                            break;
                                        case CreateReason.Fork:
                                            shouldContain.Add("fork");
                                            break;
                                        case CreateReason.LoanInterestIncome:
                                            shouldContain.Add("interest");
                                            break;
                                        case CreateReason.DustConversionIncome:
                                            shouldContain.Add("dust");
                                            break;
                                        case CreateReason.BankDeposit:
                                            shouldContain.Add("bank");
                                            break;
                                        case CreateReason.None:
                                            break;
                                        case CreateReason.TransferWithExternalSource:
                                            shouldContain.Add("transfer");
                                            shouldContain.Add("external source");
                                            break;
                                        case CreateReason.DepositWithExternalSource:
                                            shouldContain.Add("deposit");
                                            shouldContain.Add("external source");
                                            break;
                                        default:
                                            throw new NotImplementedException();
                                    }
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                                }
                                continue;
                            }
                            else if (compareTypes(types, typeCA, typeof(CreateReason), typeCA))
                            {
                                foreach (CreateReason reason in Enum.GetValues(typeof(CreateReason)))
                                {
                                    if (reason == CreateReason.FuturesFundingIncome ||
                                        reason == CreateReason.FuturesPnlIncome ||
                                        reason == CreateReason.OptionsPnlIncome || 
                                        reason == CreateReason.DustConversionIncome)
                                    {
                                        // relevantCAs not supported for now
                                        continue;
                                    }

                                    shouldContain.Clear();
                                    shouldNotContain.Clear();
                                    data = invoke(ca1, reason, ca2);
                                    shouldContain.Add(ca1.Id);
                                    shouldContain.Add("exchange");
                                    string amount = ca2.Amount.ToString();
                                    switch (reason)
                                    {
                                        case CreateReason.FuturesFundingIncome:
                                            shouldContain.Add("funding");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.FuturesPnlIncome:
                                            shouldContain.Add("futures PnL");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.OptionsPnlIncome:
                                            shouldContain.Add("options PnL");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.StakingIncome:
                                            shouldContain.Add("staking");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.Fork:
                                            shouldContain.Add("fork");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.LoanInterestIncome:
                                            shouldContain.Add("interest");
                                            shouldContain.Add(amount);
                                            break;
                                        case CreateReason.BankDeposit:
                                            shouldContain.Add("bank");
                                            break;
                                        case CreateReason.None:
                                            break;
                                        case CreateReason.TransferWithExternalSource:
                                            shouldContain.Add("transfer");
                                            shouldContain.Add("external source");
                                            break;
                                        case CreateReason.DepositWithExternalSource:
                                            shouldContain.Add("deposit");
                                            shouldContain.Add("external source");
                                            break;
                                        case CreateReason.DustConversionIncome:
                                            // doesn't support relevantCA
                                        default:
                                            throw new AssertUnreachable(reason);
                                    }
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                                }
                                continue;
                            }
                            else if (compareTypes(types, typeCA, typeof(CreateReason), typeof(CurrencyAmount?), typeof(decimal)))
                            {
                                data = invoke(ca1, CreateReason.StakingIncome, null, 1500.01m);
                                shouldContain.Add(ca1.Id);
                                shouldContain.Add("exchange");
                                shouldContain.Add("stak"); // [sic]
                                ai = 1500.01m;
                            }
                            break;
                        case ReportRowType.Burn:
                            if (compareTypes(types, typeCA, typeof(BurnReason)))
                            {
                                foreach (BurnReason reason in Enum.GetValues(typeof(BurnReason)))
                                {
                                    shouldContain.Clear();
                                    shouldNotContain.Clear();
                                    data = invoke(ca1, reason);
                                    shouldContain.Add(ca1.Id);
                                    shouldContain.Add("exchange");
                                    switch (reason)
                                    {
                                        case BurnReason.Disposal:
                                            shouldContain.Add("dispos");
                                            shouldNotContain.Add("external destination");
                                            break;
                                        case BurnReason.Transfer:
                                            shouldContain.Add("transfer");
                                            shouldNotContain.Add("external destination");
                                            break;
                                        case BurnReason.BankDepositFee:
                                            shouldContain.Add("deposit");
                                            break;
                                        case BurnReason.FuturesFundingPayment:
                                            shouldContain.Add("funding");
                                            break;
                                        case BurnReason.FuturesTradeFee:
                                            shouldContain.Add("fee");
                                            shouldContain.Add("futures trade");
                                            break;
                                        case BurnReason.OptionsTradeFee:
                                            shouldContain.Add("fee");
                                            shouldContain.Add("options trade");
                                            break;
                                        case BurnReason.BankWithdrawal:
                                            shouldContain.Add("bank");
                                            break;
                                        case BurnReason.None:
                                            break;
                                        case BurnReason.TransferWithExternalDestination:
                                            shouldContain.Add("transfer");
                                            shouldContain.Add("external destination");
                                            break;
                                        case BurnReason.WithdrawalWithExternalDestination:
                                            shouldContain.Add("withdraw");
                                            shouldContain.Add("external destination");
                                            break;
                                        default:
                                            throw new NotImplementedException();
                                    }
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                                }
                                continue;
                            }
                            else if (compareTypes(types, typeCA, typeof(BurnReason), typeCA))
                            {
                                foreach (BurnReason reason in Enum.GetValues(typeof(BurnReason)))
                                {
                                    if (reason == BurnReason.FuturesFundingPayment || reason == BurnReason.FuturesTradeFee || reason == BurnReason.OptionsTradeFee)
                                    {
                                        // relevantCAs not supported for futures-related burns for now
                                        continue;
                                    }

                                    shouldContain.Clear();
                                    shouldNotContain.Clear();
                                    data = invoke(ca1, reason, ca2);
                                    shouldContain.Add(ca1.Id);
                                    shouldContain.Add("exchange");
                                    string amount = ca2.Amount.ToString();
                                    switch (reason)
                                    {
                                        case BurnReason.Disposal:
                                            shouldContain.Add("disposing");
                                            shouldContain.Add(ca2.Id);
                                            shouldNotContain.Add("external destination");
                                            break;
                                        case BurnReason.Transfer:
                                            shouldContain.Add("transferring");
                                            shouldContain.Add(ca2.Id);
                                            shouldNotContain.Add("external destination");
                                            break;
                                        case BurnReason.BankDepositFee:
                                            shouldContain.Add("depositing");
                                            shouldContain.Add(ca2.Id);
                                            break;
                                        case BurnReason.FuturesFundingPayment:
                                            shouldContain.Add("funding");
                                            shouldContain.Add(amount);
                                            break;
                                        case BurnReason.FuturesTradeFee:
                                            shouldContain.Add("fee");
                                            shouldContain.Add("futures trade");
                                            shouldContain.Add(amount);
                                            break;
                                        case BurnReason.OptionsTradeFee:
                                            shouldContain.Add("fee");
                                            shouldContain.Add("options trade");
                                            shouldContain.Add(amount);
                                            break;
                                        case BurnReason.BankWithdrawal:
                                            shouldContain.Add("bank");
                                            shouldContain.Add(amount);
                                            break;
                                        case BurnReason.None:
                                            break;
                                        case BurnReason.TransferWithExternalDestination:
                                            shouldContain.Add("transfer");
                                            shouldContain.Add("external destination");
                                            break;
                                        case BurnReason.WithdrawalWithExternalDestination:
                                            shouldContain.Add("withdraw");
                                            shouldContain.Add("external destination");
                                            break;
                                        default:
                                            throw new NotImplementedException();
                                    }
                                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                                }
                                continue;
                            }
                            break;
                        case ReportRowType.Price:
                            if (compareTypes(types, typeof((ExchangeSymbol symbol, decimal price)[])))
                            {
                                data = invoke(new (ExchangeSymbol symbol, decimal price)[] {
                                    (_SymbolBTCUSDT, 50000.1m),
                                    (_SymbolBTCAUD, 70000.1m)
                                });
                                shouldContain.Add(_SymbolBTCUSDT.Exchange.ToString());
                                shouldContain.Add($"{_SymbolBTCUSDT.Symbol.QuoteCurrency.Symbol} per 1 {_SymbolBTCUSDT.Symbol.BaseCurrency.Symbol}");
                                shouldContain.Add(_SymbolBTCAUD.Exchange.ToString());
                                shouldContain.Add($"{_SymbolBTCAUD.Symbol.QuoteCurrency.Symbol} per 1 {_SymbolBTCAUD.Symbol.BaseCurrency.Symbol}");
                            }
                            break;
                        case ReportRowType.StartRun:
                            if (compareTypes(types, typeof(DString), typeof(FinancialYear)))
                            {
                                DString runId = _IdProviderService.GetNext(IdType.Run);
                                data = invoke(runId, _FinancialYear);
                                shouldContain.Add(GenerationService.RUN_NAME);
                                shouldContain.Add(runId);
                                shouldContain.Add(_FinancialYear.Name);
                                shouldNotContain.Add("exchange");
                            }
                            break;
                        case ReportRowType.EndRun:
                            if (compareTypes(types, typeof(DString), typeof(FinancialYear), typeof(CapitalGains), typeof(decimal)))
                            {
                                DString runId = _IdProviderService.GetNext(IdType.Run);
                                CapitalGains capitalGains = new CapitalGains(new CapitalGainsDTO(5.5m, 50.01m, -0.8m));
                                data = invoke(runId, _FinancialYear, capitalGains, 1500.5m);
                                shouldContain.Add(GenerationService.RUN_NAME);
                                shouldContain.Add(runId);
                                shouldContain.Add(_FinancialYear.Name);
                                shouldContain.Add(GenerationService.STANDARD_CAPITAL_GAINS_NAME);
                                shouldContain.Add(GenerationService.DISCOUNTED_CAPITAL_GAINS_NAME);
                                shouldContain.Add(GenerationService.CAPITAL_LOSSES_NAME);
                                shouldContain.Add("assessable income");
                                shouldContain.Add("5.5");
                                shouldContain.Add("50.01");
                                shouldContain.Add("0.8");
                                shouldContain.Add("1500.5");
                                shouldContain.Add(capitalGains.GetTotalGainsOrLosses().ToString());
                                shouldNotContain.Add("exchange");
                            }
                            break;
                        case ReportRowType.LoadInventories:
                            if (compareTypes(types, typeof(DString), typeof(IEnumerable<IInventory>)))
                            {
                                DString loadingFromRunId = _IdProviderService.GetNext(IdType.Run);
                                List<IInventory> inventories = new List<IInventory>() {
                                    _SampleInventory1,
                                    _SampleInventory2
                                };
                                data = invoke(loadingFromRunId, inventories);
                                // should contain references to the id of every currency amount of every inventory that is loaded
                                shouldContain.AddRange(inventories.SelectMany(inv => inv.GetCurrentInventory().SelectMany(kvp => kvp.Value).Select(ca => ca.Id)));
                                shouldContain.AddRange(inventories.Select(inv => inv.Id));
                                shouldContain.Add(loadingFromRunId);
                                shouldNotContain.Add("exchange");
                            }
                            break;
                        default:
                            throw new AssertUnreachable(rowType);
                    }

                    if (data == null) throw new NotImplementedException($"Constructor type for row type {rowType} cannot be checked because no test has been implemented. The specific constructor's expected input types are '{string.Join(", ", types.Select(t => t.Name))}'");

                    yield return new TestData(data, shouldContain, shouldNotContain, cg, ai).ToArray();
                }
            }
        }


        private static ReportingBaseData getReportRowDataInstance(ReportRowType type)
        {
            switch (type)
            {
                case ReportRowType.Split:
                    return _ReportingSplitData;
                case ReportRowType.Merge:
                    return _ReportingMergeData;
                case ReportRowType.Add:
                    return _ReportingAddData;
                case ReportRowType.Remove:
                    return _ReportingRemoveData;
                case ReportRowType.Purchase:
                    return _ReportingPurchaseData;
                case ReportRowType.Dispose:
                    return _ReportingDisposeData;
                case ReportRowType.Transfer:
                    return _ReportingTransferData;
                case ReportRowType.Convert:
                    return _ReportingConversionData;
                case ReportRowType.Create:
                    return _ReportingCreateData;
                case ReportRowType.Burn:
                    return _ReportingBurnData;
                case ReportRowType.Price:
                    return _ReportingPriceData;
                case ReportRowType.StartRun:
                    return _ReportingStartRunData;
                case ReportRowType.EndRun:
                    return _ReportingEndRunData;
                case ReportRowType.LoadInventories:
                    return _ReportingLoadInventoriesData;
                default:
                    throw new AssertUnreachable(type);
            }
        }

        private static bool compareTypes(Type[] expected, params Type[] actual)
        {
            if (expected.Length != actual.Length) return false;

            for (int i = 0; i < expected.Length; i++)
            {
                if (!expected[i].IsInterface && !expected[i].Equals(actual[i])
                        || expected[i].IsInterface && (!expected[i].Equals(actual[i]) && !actual[i].GetInterfaces().Contains(expected[i])))
                {
                    return false;
                }
            }

            return true;
        }

        private class TestData
        {
            private readonly ReportingBaseData _Data;
            private readonly IEnumerable<DString> _MessageShouldContain;
            private readonly IEnumerable<DString> _MessageShouldNotContain;
            private readonly bool _CapitalGainsShouldBeDefined;
            private readonly decimal? _AssessableIncome;

            public TestData(object data, IEnumerable<DString> messageShouldContain, IEnumerable<DString> messageShouldNotContain, bool capitalGainsShouldBeDefined, decimal? assessableIncome)
            {
                this._Data = data as ReportingBaseData;
                if (this._Data == null) throw new ArgumentException("Data must be of type ReportingBaseData but was type " + data.GetType().Name);
                this._MessageShouldContain = messageShouldContain;
                this._MessageShouldNotContain = messageShouldNotContain;
                this._CapitalGainsShouldBeDefined = capitalGainsShouldBeDefined;
                this._AssessableIncome = assessableIncome;
            }

            public object[] ToArray()
            {
                return new object[] { this._Data, this._MessageShouldContain, this._MessageShouldNotContain, this._CapitalGainsShouldBeDefined, this._AssessableIncome };
            }
        }
    }
}
