﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class KnownWalletsTests
    {
        [TestMethod]
        public void AllKnownWalletsCanBeRetrievedUsingAddress()
        {
            // if this fails, ensure that all KnownWallets.<WalletName> are added to the private _KnownWallets collection.
            List<string> failedWalletNames = new List<string>();

            foreach (FieldInfo field in typeof(KnownWallets).GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                if (field.GetValue(null) is Wallet wallet)
                {
                    if (!KnownWallets.GetWallet(wallet.Address, wallet.Blockchain).Equals(wallet))
                    {
                        failedWalletNames.Add(field.Name);
                    }
                }
            }

            string result = string.Join(", ", failedWalletNames);
            Assert.IsTrue(string.IsNullOrEmpty(result), $"The following wallet names could not be matched to a known wallet: {result}");
        }

        [TestMethod]
        public void GetWallet_Throws_IfDuplicateMatches()
        {
            Assert.ThrowsException<Exception>(() => KnownWallets.GetWallet(KnownWallets.ETH_MY_WALLET.Address));
        }

        [TestMethod]
        public void GetWallet_ReturnsNull_IfNoMatch()
        {
            Wallet? result = KnownWallets.GetWallet("abc");

            Assert.IsNull(result);
        }
    }
}
