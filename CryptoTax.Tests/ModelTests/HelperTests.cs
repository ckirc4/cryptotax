﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using H = CryptoTax.Domain.Models.Helpers;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class HelperTests
    {
        [TestMethod]
        public void GetMin_ReturnsItemWithMinimumValue()
        {
            decimal result = H.GetMin(new List<decimal> { 5, 7, 9, 2, 4 }, x => x);

            Assert.AreEqual(2, result);
        }

        [TestMethod]
        public void GetMax_ReturnsItemWithMaximumValue()
        {
            decimal result = H.GetMax(new List<decimal> { 5, 7, 9, 2, 4 }, x => x);

            Assert.AreEqual(9, result);
        }
    }
}
