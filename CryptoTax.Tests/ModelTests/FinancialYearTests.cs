﻿using CryptoTax.Shared;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class FinancialYearTests
    {
        [TestMethod]
        public void FinancialYearCorrectForDateInFirstHalfOfYear()
        {
            DateTime date = new DateTime(2021, 1, 1);
            FinancialYear fy = new FinancialYear(date);
            Assert.AreEqual(new DateTime(2020, 7, 1), fy.Start);
            Assert.AreEqual(new DateTime(2021, 7, 1).Subtract(new TimeSpan(1)), fy.End);
            Assert.AreEqual("2020-2021", fy.Name.Value);
        }

        [TestMethod]
        public void FinancialYearCorrectForDateInSecondHalfOfYear()
        {
            DateTime date = new DateTime(2021, 12, 1);
            FinancialYear fy = new FinancialYear(date);
            Assert.AreEqual(new DateTime(2021, 7, 1), fy.Start);
            Assert.AreEqual(new DateTime(2022, 7, 1).Subtract(new TimeSpan(1)), fy.End);
            Assert.AreEqual("2021-2022", fy.Name.Value);
        }

        [TestMethod]
        public void FinancialYearCorrectForYearInput()
        {
            FinancialYear fy = new FinancialYear(2021);
            Assert.AreEqual(new DateTime(2021, 7, 1), fy.Start);
            Assert.AreEqual(new DateTime(2022, 7, 1).Subtract(new TimeSpan(1)), fy.End);
            Assert.AreEqual("2021-2022", fy.Name.Value);
        }

        [DataTestMethod]
        [DataRow("1234", 1234)]
        [DataRow("0001", 1)]
        [DataRow("2020", 2020)]
        public void FinancialYearWorksForShortStringInput(string input, int expectedStartYear)
        {
            FinancialYear fy = new FinancialYear(input);

            Assert.AreEqual(expectedStartYear, fy.Start.Year);
        }

        [DataTestMethod]
        [DataRow("1000-1001", 1000)]
        [DataRow("9998-9999", 9998)]
        [DataRow("2020-2021", 2020)]
        public void FinancialYearWorksForLongStringInput(string input, int expectedStartYear)
        {
            FinancialYear fy = new FinancialYear(input);

            Assert.AreEqual(expectedStartYear, fy.Start.Year);
            Assert.AreEqual(input, fy.Name.Value);
        }

        [DataTestMethod]
        [DataRow("", DisplayName = "Empty string")]
        [DataRow("    ", DisplayName = "Whitespace")]
        [DataRow("1")]
        [DataRow("1.1")]
        [DataRow("12345")]
        [DataRow("123456789")]
        [DataRow("1234--789")]
        [DataRow("123--6789")]
        [DataRow("1234-678 ")]
        [DataRow("1234-678")]
        [DataRow("1000-1000")]
        [DataRow("1000-1002")]
        public void FinancialYearFailsForMalformedStringInput(string input)
        {
            Assert.ThrowsException<ArgumentException>(() => new FinancialYear(input));
        }

        [TestMethod]
        public void EmptyConstructorGeneratesCustomDefault()
        {
            // todo TAX-102: it is kind of cool that this works... is it possible to reuse this logic and package it up in UtilityLibrary?
            FinancialYear def = default;

            Assert.AreEqual("2016-2017", def.Name.Value);
        }

        [TestMethod]
        public void FinancialYearIncrementsCorrectly()
        {
            FinancialYear first = new FinancialYear(2017);
            first++;

            Assert.AreEqual("2018-2019", first.Name.Value);
        }

        [TestMethod]
        public void FinancialYearAdditionWorksCorrectly()
        {
            FinancialYear first = new FinancialYear(2017);

            FinancialYear second = first + 0;
            FinancialYear third = first + 10;

            Assert.AreEqual("2017-2018", first.Name.Value);
            Assert.AreEqual("2017-2018", second.Name.Value);
            Assert.AreEqual("2027-2028", third.Name.Value);
        }

        [TestMethod]
        public void FinancialYearSubtractionWorksCorrectly()
        {
            FinancialYear first = new FinancialYear(2017);

            FinancialYear second = first - 0;
            FinancialYear third = first - 10;

            Assert.AreEqual("2017-2018", first.Name.Value);
            Assert.AreEqual("2017-2018", second.Name.Value);
            Assert.AreEqual("2007-2008", third.Name.Value);
        }

        [TestMethod]
        public void FinancialYearUsesLocalTimeZone()
        {
            FinancialYear year = new FinancialYear(2021); // should start at July 1 2021, local time
            Assert.IsTrue(new DateTime(2021, 7, 1) - year.Start.ToUniversalTime() == TimeSpan.FromHours(10));
        }

        [TestMethod]
        public void FinancialYearEndsOneTickBeforeNext()
        {
            FinancialYear first = new FinancialYear(2020);
            FinancialYear second = new FinancialYear(2021);

            Assert.AreEqual(1, second.Start.Subtract(first.End).Ticks);
        }

        [TestMethod]
        public void FinancialYearComparisonsWorkCorrectly()
        {
            FinancialYear fy1 = new FinancialYear(2019);
            FinancialYear fy2 = new FinancialYear(2020);
            FinancialYear fy3 = new FinancialYear(2021);

            Assert.IsTrue(fy1 < fy2);
            Assert.IsTrue(fy1 < fy3);
            Assert.IsTrue(fy2 < fy3);

            Assert.IsTrue(fy3 > fy2);
            Assert.IsTrue(fy3 > fy1);
            Assert.IsTrue(fy2 > fy1);

            Assert.IsTrue(fy1 == new FinancialYear(2019));
            Assert.IsTrue(fy2 == new FinancialYear(2020));
            Assert.IsTrue(fy3 == new FinancialYear(2021));

            Assert.IsTrue(fy1 != fy2);
            Assert.IsTrue(fy1 != fy3);
            Assert.IsTrue(fy2 != fy3);

            Assert.IsTrue(fy1 >= FinancialYear.Y2019);
            Assert.IsFalse(fy1 >= FinancialYear.Y2020);
            Assert.IsFalse(fy1 >= FinancialYear.Y2021);

            Assert.IsTrue(fy1 <= FinancialYear.Y2019);
            Assert.IsTrue(fy1 <= FinancialYear.Y2020);
            Assert.IsTrue(fy1 <= FinancialYear.Y2021);
        }
    }
}
