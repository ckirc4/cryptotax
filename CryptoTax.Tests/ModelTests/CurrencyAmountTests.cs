﻿using System;
using CryptoTax.Domain;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class CurrencyAmountTests
    {
        private readonly DateTime _Time1 = new DateTime(2021, 1, 1);
        private readonly DateTime _Time2 = new DateTime(2021, 1, 2);
        private IdProviderService _IdProviderService;

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
        }

        [TestMethod]
        public void CurrencyAmountInstantiatesCorrectly()
        {
            CurrencyAmount ca = new CurrencyAmount(this._IdProviderService, "BTC", 1.005m, 50100.01m, this._Time1, Exchange.BTCMarkets);

            Assert.AreEqual("#c0", ca.Id.Value);
            Assert.IsFalse(ca.OriginalId.HasValue);
            Assert.AreEqual(this._Time1, ca.OriginalTime);
            Assert.AreEqual("BTC", ca.Currency.Value);
            Assert.AreEqual(1.005m, ca.Amount);
            Assert.AreEqual(50100.01m, ca.Value);
            Assert.AreEqual(this._Time1, ca.TimeModified);
            Assert.AreEqual(Exchange.BTCMarkets, ca.ExchangeAcquired);
        }

        [TestMethod]
        public void CurrencyAmountReducesCorrectly()
        {
            decimal originalAmount = 3.0m;
            decimal reduceAmount = 2.0m;
            decimal originalValue = 50100.01m;
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", originalAmount, originalValue, this._Time1, Exchange.BTCMarkets);
            (CurrencyAmount ca2, CurrencyAmount ca3) = ca1.ReduceAmount(reduceAmount, this._Time2, Exchange.Binance);

            decimal leftOverAmount = originalAmount - reduceAmount;
            decimal reduceValue = originalValue * (reduceAmount / originalAmount);
            decimal leftOverValue = originalValue - reduceValue;
            Assert.AreEqual("#c1", ca2.Id.Value);
            Assert.AreEqual("#c0", ca2.OriginalId.Value);
            Assert.AreEqual(this._Time1, ca2.OriginalTime);
            Assert.AreEqual("BTC", ca2.Currency.Value);
            Assert.AreEqual(leftOverAmount, ca2.Amount);
            Assert.AreEqual(leftOverValue, ca2.Value);
            Assert.AreEqual(this._Time2, ca2.TimeModified);
            Assert.AreEqual(Exchange.BTCMarkets, ca2.ExchangeAcquired);

            Assert.AreEqual("#c2", ca3.Id.Value);
            Assert.AreEqual("#c0", ca3.OriginalId.Value);
            Assert.AreEqual(this._Time1, ca2.OriginalTime);
            Assert.AreEqual("BTC", ca3.Currency.Value);
            Assert.AreEqual(reduceAmount, ca3.Amount);
            Assert.AreEqual(reduceValue, ca3.Value);
            Assert.AreEqual(this._Time2, ca3.TimeModified);
            Assert.AreEqual(Exchange.Binance, ca3.ExchangeAcquired);
        }

        [TestMethod]
        public void CurrencyAmountThrowsExceptionIfReducedBelowZero()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 1.005m, 50100.01m, this._Time1, Exchange.BTCMarkets);

            CurrencyAmountOverdrawException e = Assert.ThrowsException<CurrencyAmountOverdrawException>(() => ca1.ReduceAmount(1.006m, this._Time1));
            Assert.AreEqual(e.CurrencyAmount, ca1);
            Assert.AreEqual(e.RequestedReduction, 1.006m);
        }

        [TestMethod]
        public void WithAmountWorksWithoutValueScaling()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 10, this._Time1);

            CurrencyAmount ca2 = ca1.WithAmount(10);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 10, 10, this._Time1), ca2);
        }

        [TestMethod]
        public void WithAmountWorksWithValueScalingWhenValueIsZero()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 0, this._Time1);

            CurrencyAmount ca2 = ca1.WithAmount(10, null, true);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 10, 0, this._Time1), ca2);
        }

        [TestMethod]
        public void WithAmountWorksWithValueScaling()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 10, this._Time1);

            CurrencyAmount ca2 = ca1.WithAmount(10, null, true);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 10, 20, this._Time1), ca2);
        }

        [TestMethod]
        public void WithValueWorksCorrectly()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 20, this._Time1);

            CurrencyAmount ca2 = ca1.WithValue(10);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 5, 10, this._Time1), ca2);
        }

        [TestMethod]
        public void WithCurrencyWorksCorrectly()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 20, this._Time1);

            CurrencyAmount ca2 = ca1.WithCurrency("XRP", Exchange.FTX, this._Time2);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "XRP", 5, 20, this._Time2, Exchange.FTX), ca2);
            Assert.AreEqual(ca1.Id, ca2.OriginalId);
            Assert.AreEqual(Exchange.FTX, ca2.ExchangeAcquired);
        }

        [TestMethod]
        public void AsPhantomWorksCorrectlyNoArgs()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 20, this._Time1, Exchange.BTCMarkets);

            CurrencyAmount ca2 = ca1.AsPhantom();

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 5, 0, this._Time1, null), ca2);
            Assert.IsNull(ca2.OriginalId.Value);
        }

        [TestMethod]
        public void AsPhantomWorksCorrectlyAmountArgs()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 20, this._Time1, Exchange.BTCMarkets);

            CurrencyAmount ca2 = ca1.AsPhantom(1.5m);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 1.5m, 0, this._Time1, null), ca2);
        }

        [TestMethod]
        public void AsPhantomWorksCorrectlyAmountAndDateArgs()
        {
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 5, 20, this._Time1, Exchange.BTCMarkets);

            CurrencyAmount ca2 = ca1.AsPhantom(1.5m, this._Time2);

            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "BTC", 1.5m, 0, this._Time2, null), ca2);
        }

        [TestMethod]
        public void EqualWithinTol_ReturnsTrueIfWithinTolerance()
        {
            decimal tolerance = 0.1m;
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 1.005m, 50100.01m, this._Time1, Exchange.BTCMarkets);
            CurrencyAmount ca2 = new CurrencyAmount(this._IdProviderService, "BTC", 1.010m, 50100.05m, this._Time1, Exchange.BTCMarkets);

            bool result = CurrencyAmount.EqualWithinTol(ca1, ca2, tolerance);

            Assert.IsTrue(result);
        }

        [TestMethod]
        public void EqualWithinTol_ReturnsFalseIfOutsideTolerance()
        {
            decimal tolerance = 0;
            CurrencyAmount ca1 = new CurrencyAmount(this._IdProviderService, "BTC", 1.005m, 50100.01m, this._Time1, Exchange.BTCMarkets);
            CurrencyAmount ca2 = new CurrencyAmount(this._IdProviderService, "BTC", 1.010m, 50100.05m, this._Time1, Exchange.BTCMarkets);

            bool result = CurrencyAmount.EqualWithinTol(ca1, ca2, tolerance);

            Assert.IsFalse(result);
        }
    }
}
