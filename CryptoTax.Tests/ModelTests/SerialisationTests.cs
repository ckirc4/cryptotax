﻿using CryptoTax.DTO;
using CryptoTax.DTO.Serialised;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class SerialisationTests
    {
        // tests in here are a little bit cheaty because I honestly don't want to manually compare everything and write equality operators for every sub-type that we will come across.
        [TestMethod]
        public void SerialisedCurrencyTest()
        {
            Currency currency = new Currency("Some currency", "CURR", false, BlockchainType.Ethereum);

            SerialisedCurrency serialised = new SerialisedCurrency(currency);
            Currency deserialised = serialised.Deserialise();

            Assert.AreEqual(JsonConvert.SerializeObject(currency), JsonConvert.SerializeObject(deserialised));
        }
        
        [TestMethod]
        public void SerialisedWalletTest()
        {
            Wallet wallet = new Wallet("0xabcdefg", KnownCurrencies.ERC20, Exchange.Binance, false, BlockchainType.Ethereum);

            SerialisedWallet serialised = new SerialisedWallet(wallet);
            Wallet deserialised = serialised.Deserialise();

            Assert.AreEqual(JsonConvert.SerializeObject(wallet), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedContainerTest_Wallet()
        {
            Wallet wallet = new Wallet("0xabcdefg", KnownCurrencies.ERC20, Exchange.Binance, false, BlockchainType.Ethereum);
            Container container = Container.FromWallet(wallet);

            SerialisedContainer serialised = new SerialisedContainer(container);
            Container deserialised = serialised.DeserialiseContainer();

            Assert.AreEqual(JsonConvert.SerializeObject(container), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedContainerTest_Exchange()
        {
            Container container = Container.FromExchange(Exchange.Binance);

            SerialisedContainer serialised = new SerialisedContainer(container);
            Container deserialised = serialised.DeserialiseContainer();

            Assert.AreEqual(JsonConvert.SerializeObject(container), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedContainerTest_BankAccount()
        {
            Container container = Container.FromBankAccount();

            SerialisedContainer serialised = new SerialisedContainer(container);
            Container deserialised = serialised.DeserialiseContainer();

            Assert.AreEqual(JsonConvert.SerializeObject(container), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedTradeDtoTest()
        {
            DateTime time = DateTime.Now;
            TradeDTO trade = new TradeDTO(time, Exchange.Binance, "TradeId...", new SymbolDTO("BTC", "XRP"), MarketType.Futures, OrderSide.Buy, 0.00002001m, new ContainerEventDTO(time, Container.FromExchange(Exchange.Binance), "BTC", 0.01m, "XRP", 1000.05m, "BNB", 0.00156m));

            SerialisedTradeDTO serialised = new SerialisedTradeDTO(trade);
            TradeDTO deserialised = serialised.DeserialiseTrade();

            Assert.AreEqual(JsonConvert.SerializeObject(trade), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedTransactionDtoTest()
        {
            DateTime time = DateTime.Now;
            TransactionDTO transaction = new TransactionDTO(time, Exchange.Binance, null, "0xabcdefg", null, TransactionType.CryptoDeposit, new ContainerEventDTO(time, Container.FromExchange(Exchange.Binance), null, null, "ETH", 2.3m, null, null));

            SerialisedTransactionDTO serialised = new SerialisedTransactionDTO(transaction);
            TransactionDTO deserialised = serialised.DeserialiseTx();

            Assert.AreEqual(JsonConvert.SerializeObject(transaction), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedCandlestickDtoTest()
        {
            DateTime baseTime = new DateTime(2021, 1, 1);
            CandlestickInterval interval = CandlestickInterval.Hour_1;
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance);
            CandlestickDTO candlestick = new CandlestickDTO(new DateTime(2021, 8, 19, 6, 0, 0), interval, symbol, 45056.51m, 41005.90m);

            SerialisedCandlestickDTO serialised = new SerialisedCandlestickDTO(candlestick, baseTime);
            CandlestickDTO deserialised = serialised.Deserialise(baseTime, interval, symbol);

            Assert.AreEqual(JsonConvert.SerializeObject(candlestick), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedCandelstickDtoCollectionTest()
        {
            CandlestickInterval interval = CandlestickInterval.Minute_1;
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance);
            Func<int, CandlestickDTO> C = (min) => new CandlestickDTO(new DateTime(2021, 8, 19, 6, min, 0), interval, symbol, min * (decimal)Math.PI, min / (decimal)Math.PI);
            List<CandlestickDTO> candlesticks = new List<CandlestickDTO>() { C(15), C(2), C(10), C(30), C(31), C(55) };

            SerialisedCandlestickDtoCollection serialised = new SerialisedCandlestickDtoCollection(candlesticks);
            IEnumerable<CandlestickDTO> deserialised = serialised.Deserialise();

            Assert.AreEqual(JsonConvert.SerializeObject(candlesticks), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedExchangeSymbolTest()
        {
            ExchangeSymbol symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.USDT, KnownCurrencies.BTC), Exchange.Binance);

            SerialisedExchangeSymbol serialised = new SerialisedExchangeSymbol(symbol);
            ExchangeSymbol deserialised = serialised.Deserialise();

            Assert.AreEqual(JsonConvert.SerializeObject(symbol), JsonConvert.SerializeObject(deserialised));
        }

        [TestMethod]
        public void SerialisedException_ConvertsBackToCorrectObject()
        {
            ArgumentException ex = new ArgumentException("Test exception", "param1");
            SerialisedException serialised = SerialisedException.FromException(ex);

            ArgumentException deserialised = serialised.ToException<ArgumentException>();

            Assert.IsNotNull(deserialised);
            Assert.AreEqual(ex.Message, deserialised.Message);
            Assert.AreEqual(ex.ParamName, deserialised.ParamName);
        }

        [TestMethod]
        public void SerialisedException_ThrowsIfTypesDontMatch()
        {
            ArgumentException ex = new ArgumentException("Test exception", "param1");
            SerialisedException serialised = SerialisedException.FromException(ex);

            Assert.ThrowsException<InvalidTypeException>(() => serialised.ToException<Exception>());
        }
    }
}
