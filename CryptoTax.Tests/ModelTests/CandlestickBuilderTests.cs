﻿using CryptoTax.Domain;
using CryptoTax.Domain.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class CandlestickBuilderTests
    {
        private readonly CandlestickInterval INTERVAL = CandlestickInterval.Minute_1;
        private readonly ExchangeSymbol SYMBOL = new ExchangeSymbol();

        // three minutes = three internal candlesticks
        private readonly DateTime PRE_START_TIME =  new DateTime(2023, 9, 14, 22, 7, 30);
        private readonly DateTime START_TIME =      new DateTime(2023, 9, 14, 22, 8, 0);
        private readonly DateTime INTERNAL_TIME_1 = new DateTime(2023, 9, 14, 22, 8, 30);
        private readonly DateTime INTERNAL_TIME_2 = new DateTime(2023, 9, 14, 22, 9, 30);
        private readonly DateTime INTERNAL_TIME_3 = new DateTime(2023, 9, 14, 22, 10, 30);
        private readonly DateTime END_TIME =        new DateTime(2023, 9, 14, 22, 10, 0);
        private readonly DateTime POST_END_TIME =   new DateTime(2023, 9, 14, 22, 11, 30);

        private readonly DateTime CANDLE_1_START = new DateTime(2023, 9, 14, 22, 8, 0);
        private readonly DateTime CANDLE_2_START = new DateTime(2023, 9, 14, 22, 9, 0);
        private readonly DateTime CANDLE_3_START = new DateTime(2023, 9, 14, 22, 10, 0);

        private readonly decimal PRICE_1 = 1;
        private readonly decimal PRICE_2 = 2;
        private readonly decimal PRICE_3 = 3;
        private readonly decimal PRICE_4 = 4;

        [TestMethod]
        public void CandlestickBuilder_WellBehavedTrades()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            bool x1 = builder.AddTrade(INTERNAL_TIME_1, PRICE_1);
            bool x2 = builder.AddTrade(INTERNAL_TIME_2, PRICE_2);
            bool x3 = builder.AddTrade(INTERNAL_TIME_3, PRICE_3);
            bool x4 = builder.AddTrade(POST_END_TIME, PRICE_4);
            List<Candlestick> results = builder.GetResults();

            CollectionAssert.AreEqual(new[] { false, false, false, true }, new[] { x1, x2, x3, x4 });

            Assert.AreEqual(3, results.Count);
            CollectionAssert.AreEqual(new[] { CANDLE_1_START, CANDLE_2_START, CANDLE_3_START }, results.Select(c => c.StartTime).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_3 }, results.Select(c => c.Close).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_3 }, results.Select(c => c.Open).ToArray());
        }

        [TestMethod]
        public void CandlestickBuilder_InternalBlanks()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            builder.AddTrade(INTERNAL_TIME_1, PRICE_1);
            builder.AddTrade(INTERNAL_TIME_3, PRICE_3);
            builder.AddTrade(POST_END_TIME, PRICE_4);
            List<Candlestick> results = builder.GetResults();

            Assert.AreEqual(3, results.Count);
            CollectionAssert.AreEqual(new[] { CANDLE_1_START, CANDLE_2_START, CANDLE_3_START }, results.Select(c => c.StartTime).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_1, PRICE_3 }, results.Select(c => c.Close).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_1, PRICE_3 }, results.Select(c => c.Open).ToArray());
        }

        [TestMethod]
        public void CandlestickBuilder_LeadingBlanks()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            builder.AddTrade(INTERNAL_TIME_2, PRICE_2);
            builder.AddTrade(INTERNAL_TIME_3, PRICE_3);
            builder.AddTrade(POST_END_TIME, PRICE_4);
            List<Candlestick> results = builder.GetResults();

            Assert.AreEqual(2, results.Count);
            CollectionAssert.AreEqual(new[] { CANDLE_2_START, CANDLE_3_START }, results.Select(c => c.StartTime).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_2, PRICE_3 }, results.Select(c => c.Close).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_2, PRICE_3 }, results.Select(c => c.Open).ToArray());
        }

        [TestMethod]
        public void CandlestickBuilder_TrailingBlanks()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            builder.AddTrade(INTERNAL_TIME_1, PRICE_1);
            builder.AddTrade(INTERNAL_TIME_2, PRICE_2);
            builder.AddTrade(POST_END_TIME, PRICE_4);
            List<Candlestick> results = builder.GetResults();

            Assert.AreEqual(3, results.Count);
            CollectionAssert.AreEqual(new[] { CANDLE_1_START, CANDLE_2_START, CANDLE_3_START }, results.Select(c => c.StartTime).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_2 }, results.Select(c => c.Close).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_2 }, results.Select(c => c.Open).ToArray());
        }

        [TestMethod]
        public void CandlestickBuilder_UsesPreRangeTradeDataToInferLeadingBlanks()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            builder.AddTrade(PRE_START_TIME, PRICE_1);
            builder.AddTrade(INTERNAL_TIME_2, PRICE_2);
            builder.AddTrade(POST_END_TIME, PRICE_4);
            List<Candlestick> results = builder.GetResults();

            Assert.AreEqual(3, results.Count);
            CollectionAssert.AreEqual(new[] { CANDLE_1_START, CANDLE_2_START, CANDLE_3_START }, results.Select(c => c.StartTime).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_2 }, results.Select(c => c.Close).ToArray());
            CollectionAssert.AreEqual(new[] { PRICE_1, PRICE_2, PRICE_2 }, results.Select(c => c.Open).ToArray());
        }

        [TestMethod]
        public void CandlestickBuilder_ThrowsWhenTimeOutOfRange()
        {
            CandlestickBuilder builder = new CandlestickBuilder(INTERVAL, START_TIME, END_TIME, SYMBOL);

            Assert.ThrowsException<Exception>(() => builder.AddTrade(POST_END_TIME, PRICE_1));
        }
    }
}
