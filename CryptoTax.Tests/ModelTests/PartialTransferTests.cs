﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class PartialTransferTests
    {
        private readonly DateTime _Time1 = new DateTime(2021, 1, 1);
        private readonly DateTime _Time2 = new DateTime(2021, 1, 2);
        private IdProviderService _IdProviderService;
        private Func<CurrencyAmount> _GetCA;
        private Container _SourceContainer1 = Container.FromExchange(Exchange.BTCMarkets);
        private Container _SourceContainer2 = Container.FromExchange(Exchange.FTX);
        private Container _DestinationContainer1 = Container.FromExchange(Exchange.Binance);
        private Container _DestinationContainer2 = Container.FromExchange(Exchange.MercatoX);
        private TransactionDTO _TxDTO;
        private PartialTransfer _SourcePartialTransfer;
        private PartialTransfer _SourcePartialTransferOriginal;
        private PartialTransfer _DestinationPartialTransfer;
        private PartialTransfer _DestinationPartialTransferOriginal;
        private DString _WrongCurrency = "ASDFGHJKL";

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._GetCA = () => new CurrencyAmount(this._IdProviderService, "XRP", 100, 10, this._Time1);

            this._TxDTO = new TransactionDTO(this._Time1, Exchange.BTCMarkets, "internal id", "public id", null, TransactionType.CryptoDeposit, null);
            this._SourcePartialTransfer = new PartialTransfer(this._TxDTO, "internal test", "public test", this._GetCA(), this._SourceContainer1, this._Time1, this._GetCA(), null, null, null);
            this._SourcePartialTransferOriginal = new PartialTransfer(this._TxDTO, "internal test", "public test", this._SourcePartialTransfer.AmountTransferred, this._SourceContainer1, this._Time1, this._SourcePartialTransfer.FeeToSend, null, null, null);
            this._DestinationPartialTransfer = new PartialTransfer(this._TxDTO, "internal test", "public test", this._GetCA(), null, null, null, this._DestinationContainer1, this._Time2, this._GetCA());
            this._DestinationPartialTransferOriginal = new PartialTransfer(this._TxDTO, "internal test", "public test", this._DestinationPartialTransfer.AmountTransferred, null, null, null, this._DestinationContainer1, this._Time2, this._DestinationPartialTransfer.FeeToReceive);
        }

        #region normal tests
        [TestMethod]
        public void PartialTransferThrowsAcceptionIfNoIdProvided()
        {
            Assert.ThrowsException<TransferIdArgumentException>(() => new PartialTransfer(this._TxDTO, null, null, this._GetCA(), this._SourceContainer1, this._Time1, this._GetCA(), null, null, null));
        }

        [TestMethod]
        public void PartialTransferThrowsIfOneSideIsNotFullyProvided()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), this._SourceContainer1, null, null, null, null, null));
            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), null, this._Time1, null, null, null, null));

            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), null, null, null, this._DestinationContainer1, null, null));
            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), null, null, null, null, this._Time2, null));
        }

        [TestMethod]
        public void PartialTransferThrowsWhenProvidingFeeOnOppositeSide()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), this._SourceContainer1, this._Time1, this._GetCA(), null, null, this._GetCA()));
            Assert.ThrowsException<TransferSideArgumentException>(() => new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), null, null, this._GetCA(), this._SourceContainer1, this._Time1, this._GetCA()));
        }

        [TestMethod]
        public void PartialTransferAllowsBothSidesToBeProvided()
        {
            Assert.IsNotNull(new PartialTransfer(this._TxDTO, "Test", "Test", this._GetCA(), this._SourceContainer1, this._Time1, this._GetCA(), this._DestinationContainer1, this._Time2, this._GetCA()));
        }
        #endregion

        #region source relay tests
        [TestMethod]
        public void AbbreviatedSourceRelayMethodKeepsAllOtherPropertiesSame()
        {
            PartialTransfer o = this._SourcePartialTransferOriginal;
            this._SourcePartialTransfer.WithRelay(TransferSide.Source, this._SourceContainer2);
            this.assertExpectedProperties(this._SourcePartialTransfer.SourceRelay,
                o.OriginalDTO, null, null,
                o.InternalId, o.PublicId, o.AmountTransferred,
                o.IsSourceSide, this._SourceContainer2, o.StartTime, o.FeeToSend,
                o.IsDestinationSide, o.Destination, o.FinishTime, o.FeeToReceive);
        }

        [TestMethod]
        public void FullSourceRelayMethodSetsCorrectProperties()
        {
            PartialTransfer o = this._SourcePartialTransferOriginal;
            string internalId = "new internal";
            string publicId = "new public";
            CurrencyAmount amountTransferred = this._GetCA().WithAmount(100);
            Container source = this._SourceContainer2;
            DateTime start = this._Time2;
            CurrencyAmount feeToSend = this._GetCA().WithAmount(1).WithValue(0);

            this._SourcePartialTransfer.WithRelay(TransferSide.Source, internalId, publicId, amountTransferred, source, start, feeToSend);
            this.assertExpectedProperties(this._SourcePartialTransfer.SourceRelay,
                o.OriginalDTO, null, null,
                internalId, publicId, amountTransferred,
                o.IsSourceSide, source, start, feeToSend,
                o.IsDestinationSide, o.Destination, o.FinishTime, o.FeeToReceive);
        }

        [TestMethod]
        public void SourceRelayThrowsIfBothIdsNull()
        {
            Assert.ThrowsException<TransferIdArgumentException>(() => this._SourcePartialTransfer.WithRelay(TransferSide.Source, null, null, this._GetCA(), this._SourceContainer2, this._Time2, this._GetCA()));
        }

        [TestMethod]
        public void SourceRelayThrowsIfOriginalSideIsDestination()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => this._DestinationPartialTransfer.WithRelay(TransferSide.Source, "abc", "def", this._GetCA(), this._SourceContainer2, this._Time2, this._GetCA()));
        }

        [TestMethod]
        public void SourceRelayThrowsIfProvidingWrongTransferredCurrency()
        {
            Assert.ThrowsException<TransferCurrencyArgumentException>(() => this._SourcePartialTransfer.WithRelay(TransferSide.Source, "abc", "def", new CurrencyAmount(this._IdProviderService, this._WrongCurrency, 1, 1, this._Time2), this._SourceContainer2, this._Time2, this._GetCA()));
        }

        [TestMethod]
        public void SourceRelayThrowsIfAlreadyExists()
        {
            this._SourcePartialTransfer.WithRelay(TransferSide.Source, this._SourceContainer2);
            Assert.ThrowsException<TransferDuplicateRelayException>(() => this._SourcePartialTransfer.WithRelay(TransferSide.Source, this._SourceContainer2));
        }

        [TestMethod]
        public void ThrowsWhenAddingReceiveFeeToDestinationSide()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => this._DestinationPartialTransfer.AddFee(TransferSide.Source, this._GetCA()));
        }

        [TestMethod]
        public void AddFeeWorksCorrectlyForSourceSide()
        {
            PartialTransfer pt = new PartialTransfer(this._TxDTO, "internal test", "public test", this._GetCA(), this._SourceContainer1, this._Time1, null, null, null, null);
            CurrencyAmount fee1 = this._GetCA();
            CurrencyAmount fee2 = this._GetCA();
            CurrencyAmount fee3 = new CurrencyAmount(this._IdProviderService, this._WrongCurrency, 1, 1, this._Time1);

            Assert.IsNull(pt.FeeToSend);

            pt.AddFee(TransferSide.Source, fee1);
            Assert.AreEqual(fee1, pt.FeeToSend);

            pt.AddFee(TransferSide.Source, fee2);
            Assert.AreEqual(fee1.WithAmount(fee1.Amount + fee2.Amount), pt.FeeToSend);

            Assert.ThrowsException<TransferCurrencyArgumentException>(() => pt.AddFee(TransferSide.Source, fee3));
        }
        #endregion

        #region destination relay tests
        [TestMethod]
        public void AbbreviatedDestinationRelayMethodKeepsAllOtherPropertiesSame()
        {
            PartialTransfer o = this._DestinationPartialTransferOriginal;
            this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, this._DestinationContainer2);
            this.assertExpectedProperties(this._DestinationPartialTransfer.DestinationRelay,
                o.OriginalDTO, null, null,
                o.InternalId, o.PublicId, o.AmountTransferred,
                o.IsSourceSide, o.Source, o.StartTime, o.FeeToSend,
                o.IsDestinationSide, this._DestinationContainer2, o.FinishTime, o.FeeToReceive);
        }

        [TestMethod]
        public void FullDestinationRelayMethodSetsCorrectProperties()
        {
            PartialTransfer o = this._DestinationPartialTransferOriginal;
            string internalId = "new internal";
            string publicId = "new public";
            CurrencyAmount amountTransferred = this._GetCA().WithAmount(100);
            Container destination = this._DestinationContainer2;
            DateTime finish = this._Time1;
            CurrencyAmount feeToReceive = this._GetCA().WithAmount(1).WithValue(0);

            this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, internalId, publicId, amountTransferred, destination, finish, feeToReceive);
            this.assertExpectedProperties(this._DestinationPartialTransfer.DestinationRelay,
                o.OriginalDTO, null, null,
                internalId, publicId, amountTransferred,
                o.IsSourceSide, o.Source, o.StartTime, o.FeeToSend,
                o.IsDestinationSide, destination, finish, feeToReceive);
        }

        [TestMethod]
        public void DestinationRelayThrowsIfBothIdsNull()
        {
            Assert.ThrowsException<TransferIdArgumentException>(() => this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, null, null, this._GetCA(), this._DestinationContainer2, this._Time1, this._GetCA()));
        }

        [TestMethod]
        public void DestinationRelayThrowsIfOriginalSideIsSource()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => this._SourcePartialTransfer.WithRelay(TransferSide.Destination, "abc", "def", this._GetCA(), this._DestinationContainer2, this._Time1, this._GetCA()));
        }

        [TestMethod]
        public void DestinationRelayThrowsIfProvidingWrongTransferredCurrency()
        {
            Assert.ThrowsException<TransferCurrencyArgumentException>(() => this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, "abc", "def", new CurrencyAmount(this._IdProviderService, this._WrongCurrency, 1, 1, this._Time1), this._DestinationContainer2, this._Time1, this._GetCA()));
        }

        [TestMethod]
        public void DestinationRelayThrowsIfAlreadyExists()
        {
            this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, this._DestinationContainer2);
            Assert.ThrowsException<TransferDuplicateRelayException>(() => this._DestinationPartialTransfer.WithRelay(TransferSide.Destination, this._DestinationContainer2));
        }

        [TestMethod]
        public void ThrowsWhenAddingReceiveFeeToSourceSide()
        {
            Assert.ThrowsException<TransferSideArgumentException>(() => this._SourcePartialTransfer.AddFee(TransferSide.Destination, this._GetCA()));
        }

        [TestMethod]
        public void AddFeeWorksCorrectlyForDestinationSide()
        {
            PartialTransfer pt = new PartialTransfer(this._TxDTO, "internal test", "public test", this._GetCA(), null, null, null, this._DestinationContainer1, this._Time2, null);
            CurrencyAmount fee1 = this._GetCA();
            CurrencyAmount fee2 = this._GetCA();
            CurrencyAmount fee3 = new CurrencyAmount(this._IdProviderService, this._WrongCurrency, 1, 1, this._Time1);

            Assert.IsNull(pt.FeeToReceive);

            pt.AddFee(TransferSide.Destination, fee1);
            Assert.AreEqual(fee1, pt.FeeToReceive);

            pt.AddFee(TransferSide.Destination, fee2);
            Assert.AreEqual(fee1.WithAmount(fee1.Amount + fee2.Amount), pt.FeeToReceive);

            Assert.ThrowsException<TransferCurrencyArgumentException>(() => pt.AddFee(TransferSide.Destination, fee3));
        }

        #endregion

        private void assertExpectedProperties(PartialTransfer ptToTest,
            TransactionDTO dto, PartialTransfer sourceRelay, PartialTransfer destinationRelay,
            NString internalId, NString publicId, CurrencyAmount amountTransferred,
            bool isSourceSide, Container? sourceContainer, DateTime? startTime, CurrencyAmount? feeToSend,
            bool isDestinationSide, Container? destinationContainer, DateTime? finishTime, CurrencyAmount? feeToReceive)
        {
            Assert.AreSame(dto, ptToTest.OriginalDTO);
            Assert.AreSame(sourceRelay, ptToTest.SourceRelay);
            Assert.AreSame(destinationRelay, ptToTest.DestinationRelay);

            Assert.AreEqual(internalId, ptToTest.InternalId);
            Assert.AreEqual(publicId, ptToTest.PublicId);
            Assert.AreEqual(amountTransferred, ptToTest.AmountTransferred);

            Assert.AreEqual(isSourceSide, ptToTest.IsSourceSide);
            Assert.AreEqual(sourceContainer, ptToTest.Source);
            Assert.AreEqual(startTime, ptToTest.StartTime);
            Assert.AreEqual(feeToSend, ptToTest.FeeToSend);

            Assert.AreEqual(isDestinationSide, ptToTest.IsDestinationSide);
            Assert.AreEqual(destinationContainer, ptToTest.Destination);
            Assert.AreEqual(finishTime, ptToTest.FinishTime);
            Assert.AreEqual(feeToReceive, ptToTest.FeeToReceive);
        }
    }
}
