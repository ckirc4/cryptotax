﻿using CryptoTax.Domain;
using CryptoTax.Domain.Models;
using CryptoTax.DTO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class CapitalGainsTests
    {
        private CapitalGains _CapitalGains;

        [TestInitialize]
        public void Init()
        {
            this._CapitalGains = new CapitalGains();
        }

        [TestMethod]
        public void CapitalGainsInitialisesEverythingToZero()
        {
            decimal[] properties = typeof(CapitalGains).GetProperties().Select(prop => (decimal)prop.GetValue(this._CapitalGains)).ToArray();
            CollectionAssert.AreEqual(new[] { 0.0m, 0.0m, 0.0m }, properties);
        }

        [TestMethod]
        public void CapitalGainsSetsCorrectValuesFromDTO()
        {
            CapitalGainsDTO dto = new CapitalGainsDTO(1, 2, -1);

            CapitalGains cg = new CapitalGains(dto);

            Assert.AreEqual(1.0m, cg.StandardCapitalGains);
            Assert.AreEqual(2.0m, cg.DiscountableCapitalGains);
            Assert.AreEqual(-1.0m, cg.CapitalLosses);
        }

        [TestMethod]
        public void CapitalGainsGeneratesCorrectDTO()
        {
            this._CapitalGains.Add(new CapitalGainsDelta(1, 2, -1));

            CapitalGainsDTO dto = this._CapitalGains.ToDTO();

            Assert.AreEqual(1.0m, dto.StandardCapitalGains);
            Assert.AreEqual(2.0m, dto.DiscountableCapitalGains);
            Assert.AreEqual(-1.0m, dto.CapitalLosses);
        }

        [TestMethod]
        public void CapitalGainsGeneratesCorrectDeltaWhenStandardEventAdded()
        {
            CapitalGainsDelta delta1 = this._CapitalGains.AddTaxableEvent(100, 150);
            CapitalGainsDelta delta2 = this._CapitalGains.AddTaxableEvent(100, 25);

            Assert.AreEqual(50.0m, delta1.StandardCapitalGains);
            Assert.AreEqual(0, delta1.DiscountableCapitalGains);
            Assert.AreEqual(0, delta1.CapitalLosses);

            Assert.AreEqual(0, delta2.StandardCapitalGains);
            Assert.AreEqual(0, delta2.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, delta2.CapitalLosses);

            Assert.AreEqual(50.0m, this._CapitalGains.StandardCapitalGains);
            Assert.AreEqual(0, this._CapitalGains.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, this._CapitalGains.CapitalLosses);
        }

        [TestMethod]
        public void CapitalGainsGeneratesCorrectDeltaWhenRecentEventAdded()
        {
            CapitalGainsDelta delta1 = this._CapitalGains.AddTaxableEvent(100, 150, DateTime.MinValue, DateTime.MinValue.AddYears(1));
            CapitalGainsDelta delta2 = this._CapitalGains.AddTaxableEvent(100, 25, DateTime.MinValue, DateTime.MinValue.AddYears(1));

            Assert.AreEqual(50.0m, delta1.StandardCapitalGains);
            Assert.AreEqual(0, delta1.DiscountableCapitalGains);
            Assert.AreEqual(0, delta1.CapitalLosses);

            Assert.AreEqual(0, delta2.StandardCapitalGains);
            Assert.AreEqual(0, delta2.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, delta2.CapitalLosses);

            Assert.AreEqual(50.0m, this._CapitalGains.StandardCapitalGains);
            Assert.AreEqual(0, this._CapitalGains.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, this._CapitalGains.CapitalLosses);
        }

        [TestMethod]
        public void CapitalGainsGeneratesCorrectDeltaWhenOldEventAdded()
        {
            DateTime buyTime = DateTime.MinValue;
            DateTime sellTime = buyTime.AddYears(1).AddMilliseconds(1);
            CapitalGainsDelta delta1 = this._CapitalGains.AddTaxableEvent(100, 150, buyTime, sellTime);
            CapitalGainsDelta delta2 = this._CapitalGains.AddTaxableEvent(100, 25, buyTime, sellTime);

            Assert.AreEqual(0, delta1.StandardCapitalGains);
            Assert.AreEqual(50.0m, delta1.DiscountableCapitalGains);
            Assert.AreEqual(0, delta1.CapitalLosses);

            Assert.AreEqual(0, delta2.StandardCapitalGains);
            Assert.AreEqual(0, delta2.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, delta2.CapitalLosses);

            Assert.AreEqual(0, this._CapitalGains.StandardCapitalGains);
            Assert.AreEqual(50.0m, this._CapitalGains.DiscountableCapitalGains);
            Assert.AreEqual(-75.0m, this._CapitalGains.CapitalLosses);
        }

        [TestMethod]
        public void CapitalGainsAddsDeltaCorrectly()
        {
            this._CapitalGains.AddTaxableEvent(100, 150);
            this._CapitalGains.AddTaxableEvent(100, 50);

            CapitalGainsDelta delta = new CapitalGainsDelta(5, 1, -2);
            this._CapitalGains.Add(delta);

            Assert.AreEqual(55.0m, this._CapitalGains.StandardCapitalGains);
            Assert.AreEqual(1.0m, this._CapitalGains.DiscountableCapitalGains);
            Assert.AreEqual(-52.0m, this._CapitalGains.CapitalLosses);
        }

        [DataTestMethod]
        [DataRow(0, 0, 0, 0)]
        [DataRow(1, 0, 0, 1)]
        [DataRow(1, 1, 0, 1.5)]
        [DataRow(0, 1, 0, 0.5)]
        [DataRow(0, 1, -1, 0)]
        [DataRow(0, 1, -0.5, 0.25)]
        [DataRow(1, 1, -1, 0.5)]
        [DataRow(1, 1, -0.5, 1)]
        [DataRow(1, 1, -1.5, 0.25)]
        public void CapitalGainsCalculatesCorrectTotal(double standardGains, double discountableGains, double losses, double expected)
        {
            CapitalGains cg = new CapitalGains(new CapitalGainsDTO((decimal)standardGains, (decimal)discountableGains, (decimal)losses));

            Assert.AreEqual((decimal)expected, cg.GetTotalGainsOrLosses());
        }

        [TestMethod]
        public void CapitalGainsIsZeroWorksCorrectly()
        {
            CapitalGainsDelta d1 = CapitalGainsDelta.ZERO;
            Assert.IsTrue(d1.IsZero());

            CapitalGainsDelta d2 = new CapitalGainsDelta(1, 0, 0);
            Assert.IsFalse(d2.IsZero());

            CapitalGainsDelta d3 = new CapitalGainsDelta(0, 1, 0);
            Assert.IsFalse(d3.IsZero());

            CapitalGainsDelta d4 = new CapitalGainsDelta(0, 0, -1);
            Assert.IsFalse(d4.IsZero());
        }
    }
}
