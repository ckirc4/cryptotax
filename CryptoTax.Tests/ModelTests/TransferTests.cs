﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class TransferTests
    {
        private readonly DateTime _Time1 = new DateTime(2021, 1, 1);
        private readonly DateTime _Time2 = new DateTime(2021, 1, 2);
        private IdProviderService _IdProviderService;
        private Func<CurrencyAmount> _GetCA;
        private Func<CurrencyAmount> _GetCA_WrongCurrency;
        private Container _SourceContainer = Container.FromExchange(Exchange.Undefined);
        private Container _DestinationContainer = Container.FromExchange(Exchange.Binance);

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._GetCA = () => new CurrencyAmount(this._IdProviderService, "XRP", 100, 10, this._Time1);
            this._GetCA_WrongCurrency = () => new CurrencyAmount(this._IdProviderService, "BTC", 10, 0.5m, this._Time1);
        }

        [TestMethod]
        public void TransferIdTests()
        {
            Assert.IsNotNull(new Transfer("Test 1", "Test 2", "Test 3", this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
            Assert.IsNotNull(new Transfer("Test 1", "Test 2", null, this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
            Assert.IsNotNull(new Transfer(null, null, "Test", this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
            Assert.IsNotNull(new Transfer("Test 1", null, "Test 2", this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));

            Assert.ThrowsException<TransferIdArgumentException>(() => new Transfer(null, null, null, this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
            Assert.ThrowsException<TransferIdArgumentException>(() => new Transfer("Test", null, null, this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
            Assert.ThrowsException<TransferIdArgumentException>(() => new Transfer(null, "Test", null, this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null));
        }

        [TestMethod]
        public void TransferStageCyclesCorrectly()
        {
            Transfer transfer = new Transfer("Test", "Test", "Test", this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null);

            Assert.AreEqual(TransferStage.Withdrawal, transfer.NextStage);
            transfer.AdvanceToNextStage();
            Assert.AreEqual(TransferStage.Deposit, transfer.NextStage);
            transfer.AdvanceToNextStage();
            Assert.AreEqual(TransferStage.None, transfer.NextStage);
            transfer.AdvanceToNextStage();
            Assert.AreEqual(TransferStage.None, transfer.NextStage);
        }

        [TestMethod]
        public void GetContinueTimeReturnsCorrectTime()
        {
            Transfer transfer = new Transfer("Test 1", "Test 2", null, this._GetCA(), this._SourceContainer, this._Time1, null, this._DestinationContainer, this._Time2, null, null, null);

            Assert.AreEqual(this._Time1, transfer.GetContinueTime());
            transfer.AdvanceToNextStage();
            Assert.AreEqual(this._Time2, transfer.GetContinueTime());
            transfer.AdvanceToNextStage();
            Assert.IsNull(transfer.GetContinueTime());
        }

        [TestMethod]
        public void FromPartialsThrowsIfNotOrderedCorrectly()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA(), this._SourceContainer, this._Time1, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA(), null, null, null, this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferSideArgumentException>(() => Transfer.FromPartials(pt1, pt1));
            Assert.ThrowsException<TransferSideArgumentException>(() => Transfer.FromPartials(pt2, pt2));
            Assert.ThrowsException<TransferSideArgumentException>(() => Transfer.FromPartials(pt2, pt1));
        }

        [TestMethod]
        public void FromPartialsThrowsIfPublicIdsDoNotMatch()
        {
            Func<NString, PartialTransfer> pt1 = id => new PartialTransfer(null, "Test", id, this._GetCA(), this._SourceContainer, this._Time1, null, null, null, null);
            Func<NString, PartialTransfer> pt2 = id => new PartialTransfer(null, "Test", id, this._GetCA(), null, null, null, this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferIdArgumentException>(() => Transfer.FromPartials(pt1(null), pt2("abc")));
            Assert.ThrowsException<TransferIdArgumentException>(() => Transfer.FromPartials(pt1("abc"), pt2(null)));
            Assert.ThrowsException<TransferIdArgumentException>(() => Transfer.FromPartials(pt1("abc"), pt2("def")));
        }

        [TestMethod]
        public void FromPartialsThrowsIfStartAfterFinish()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA(), this._SourceContainer, this._Time2, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA(), null, null, null, this._DestinationContainer, this._Time1, null);

            Assert.ThrowsException<TransferTimeArgumentException>(() => Transfer.FromPartials(pt1, pt2));
        }

        [TestMethod]
        public void FromPartialsThrowsIfTransferredCurrenciesAreDifferent()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA(), this._SourceContainer, this._Time1, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA_WrongCurrency(), null, null, null, this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferCurrencyArgumentException>(() => Transfer.FromPartials(pt1, pt2));
        }

        [TestMethod]
        public void FromPartialsThrowsIfSourceAmountLessThanDestination()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(1), this._SourceContainer, this._Time1, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(2), null, null, null, this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferAmountException>(() => Transfer.FromPartials(pt1, pt2));
        }

        [TestMethod]
        public void FromPartialsThrowsIfSourceRelayLessThanSource()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(2), this._SourceContainer, this._Time1, null, null, null, null);
            pt1.WithRelay(TransferSide.Source, "Test", "Test", this._GetCA().WithAmount(1), this._SourceContainer, this._Time1, null);

            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(2), null, null, null, this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferAmountException>(() => Transfer.FromPartials(pt1, pt2));
        }

        [TestMethod]
        public void FromPartialsThrowsIfDestinationAmountLessThanDestinationRelay()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(1), this._SourceContainer, this._Time1, null, null, null, null);

            PartialTransfer pt2 = new PartialTransfer(null, "Test", "Test", this._GetCA().WithAmount(1), null, null, null, this._DestinationContainer, this._Time2, null);
            pt2.WithRelay(TransferSide.Destination, "Test", "Test", this._GetCA().WithAmount(2), this._DestinationContainer, this._Time2, null);

            Assert.ThrowsException<TransferAmountException>(() => Transfer.FromPartials(pt1, pt2));
        }

        [TestMethod]
        public void FromPartialsAcceptsNullPublicIdFromBTCMarkets()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", "PublicId", this._GetCA(), this._SourceContainer, this._Time1, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", null, this._GetCA(), null, null, null, Container.FromExchange(Exchange.BTCMarkets), this._Time2, null);

            Transfer transfer = Transfer.FromPartials(pt1, pt2);
            Assert.IsNotNull(transfer);
        }

        [TestMethod]
        public void FromPartialsDoesNotThrowIfBothPublicIdsNull()
        {
            PartialTransfer pt1 = new PartialTransfer(null, "Test", null, this._GetCA(), this._SourceContainer, this._Time1, null, null, null, null);
            PartialTransfer pt2 = new PartialTransfer(null, "Test", null, this._GetCA(), null, null, null, this._DestinationContainer, this._Time2, null);

            Transfer transfer = Transfer.FromPartials(pt1, pt2);
            Assert.IsNotNull(transfer);
        }

        [TestMethod]
        public void SmallTimeOverlap_Accepted()
        {
            // there is a funny issue with XRP transfers where BTCMarkets takes a while to mark a withdrawal as processed, and uses the time at which the transaction was checked as the time of processing. This means that it is possible for a slight overlap in times (since XRPL is so fast) if the receiving exchange detects the deposited funds first.
            DateTime startTime = DateTime.Now;
            DateTime finishTime = startTime.AddSeconds(-30);

            PartialTransfer withdrawal = new PartialTransfer(null, "internal1", "tx", this._GetCA(), this._SourceContainer, startTime, null, null, null, null);
            PartialTransfer deposit = new PartialTransfer(null, "internal2", "tx", this._GetCA(), null, null, null, this._DestinationContainer, finishTime, null);

            Transfer transfer = Transfer.FromPartials(withdrawal, deposit);
            Assert.IsNotNull(transfer);
        }

        [TestMethod]
        public void LargeTimeOverlap_Rejected()
        {
            DateTime startTime = DateTime.Now;
            DateTime finishTime = startTime.AddMinutes(-30);

            PartialTransfer withdrawal = new PartialTransfer(null, "internal1", "tx", this._GetCA(), this._SourceContainer, startTime, null, null, null, null);
            PartialTransfer deposit = new PartialTransfer(null, "internal2", "tx", this._GetCA(), null, null, null, this._DestinationContainer, finishTime, null);

            Assert.ThrowsException<TransferTimeArgumentException>(() => Transfer.FromPartials(withdrawal, deposit));
        }
    }
}
