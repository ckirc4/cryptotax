﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibrary.Types;
using System.Reflection;
using System.Linq;
using System;
using System.Collections.Generic;
using CryptoTax.DTO.Serialised;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class ContainerTests
    {
        [TestMethod]
        [TestCategory(Tests.META)]
        public void EnsureTestsAreImplementedForAllInstantiationMethods()
        {
            IEnumerable<MethodInfo> methods = typeof(Container)
                .GetMethods()
                .Where(m => m.ReturnType == typeof(Container) && m.IsStatic);

            foreach (MethodInfo method in methods)
            {
                switch (method.Name)
                {
                    case "FromExchange":
                        Assert.IsNotNull(nameof(this.FromExchangeWorksCorrectly));
                        break;
                    case "FromBankAccount":
                        Assert.IsNotNull(nameof(this.FromBankAccountWorksCorrectly));
                        break;
                    case "FromWallet":
                        Assert.IsNotNull(nameof(this.FromWalletWorksCorrectly_PrivateWallet));
                        Assert.IsNotNull(nameof(this.FromWalletWorksCorrectly_ExchangeWallet));
                        Assert.IsNotNull(nameof(this.FromWalletWorksCorrectly_ThirdPartyWallet));
                        break;
                    case "FromSerialised":
                        Assert.IsNotNull(nameof(this.ContainerSerialisationTest));
                        break;
                    default:
                        throw new NotImplementedException($"No test exists for the instantiation method Container.{method}");
                }
            }
        }

        [TestMethod]
        public void FromBankAccountWorksCorrectly()
        {
            Container container = Container.FromBankAccount();
            this.containerAssert(container, ContainerType.BankAccount, null, null);
        }

        [TestMethod]
        public void FromWalletWorksCorrectly_PrivateWallet()
        {
            Wallet wallet = KnownWallets.ETH_MY_WALLET_OLD;
            Container container = Container.FromWallet(wallet);
            this.containerAssert(container, ContainerType.OwnedWallet, null, wallet);
            Assert.AreEqual(container, Container.FromWallet(wallet.Address));
        }

        [TestMethod]
        public void FromWalletWorksCorrectly_ExchangeWallet()
        {
            Wallet wallet = KnownWallets.ETH_DDEX_EXCHANGE_WALLET;
            Container container = Container.FromWallet(wallet);
            this.containerAssert(container, ContainerType.ExchangeWallet, null, wallet);
            Assert.AreEqual(container, Container.FromWallet(wallet.Address));
        }

        [TestMethod]
        public void FromWalletWorksCorrectly_ThirdPartyWallet()
        {
            Wallet wallet = KnownWallets.ETH_BURN_ADDRESS;
            Container container = Container.FromWallet(wallet);
            this.containerAssert(container, ContainerType.ThirdPartyWallet, null, wallet);
            Assert.AreEqual(container, Container.FromWallet(wallet.Address));
        }

        [TestMethod]
        public void FromWalletWorksCorrectly_ChainSpecifier()
        {
            Wallet wallet = KnownWallets.ETH_MY_WALLET;

            Assert.ThrowsException<Exception>(() => Container.FromWallet(wallet.Address));

            Container container = Container.FromWallet(wallet.Address, wallet.Blockchain);
            this.containerAssert(container, ContainerType.OwnedWallet, null, wallet);
        }

        [TestMethod]
        public void FromExchangeWorksCorrectly()
        {
            Container container = Container.FromExchange(Exchange.BTCMarkets);
            this.containerAssert(container, ContainerType.ExchangeAccount, Exchange.BTCMarkets, null);
        }

        [TestMethod]
        public void ContainerSerialisationTest()
        {
            Wallet wallet = KnownWallets.ETH_BURN_ADDRESS;
            Container container = Container.FromWallet(wallet);
            SerialisedContainer serialised = new SerialisedContainer(container);
            
            Container deserialised = Container.FromSerialised(serialised.Id, serialised.Type, serialised.Wallet.Deserialise(), serialised.Exchange);
            this.containerAssert(deserialised, container.Type, container.Exchange, container.Wallet, container.Id);
        }

        private void containerAssert(Container container, ContainerType expectedType, Exchange? expectedExchange, Wallet? expectedWallet, NString expectedId = default)
        {
            Assert.AreEqual(expectedType, container.Type);
            Assert.AreEqual(expectedExchange, container.Exchange);
            Assert.AreEqual(expectedWallet, container.Wallet);
            if (expectedId != null)
            {
                Assert.AreEqual(expectedId, container.Id);
            }
        }
    }
}
