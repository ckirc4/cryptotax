﻿using System.Collections.Generic;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoTax.Shared;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class KnownCurrenciesTests
    {
        [TestMethod]
        public void AllKnownCurrenciesCanBeRetrievedUsingSymbol()
        {
            // if this fails, ensure that all KnownCurrencies.<Symbol> are added to the private _KnownCurrencies collection.
            List<string> failedSymbols = new List<string>();

            foreach (FieldInfo field in typeof(KnownCurrencies).GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                if (field.GetValue(null) is Currency currency)
                {
                    if (KnownCurrencies.GetCurrency(currency.Symbol) != currency)
                    {
                        failedSymbols.Add(currency.Symbol);
                    }
                }
            }

            string result = string.Join(", ", failedSymbols);
            Assert.IsTrue(string.IsNullOrEmpty(result), $"The following currency symbols could not be matched to a known currency: {result}");
        }
    }
}
