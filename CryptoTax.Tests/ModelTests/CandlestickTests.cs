﻿using CryptoTax.Domain.Models;
using CryptoTax.Domain.Models.Exceptions;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CryptoTax.Tests.ModelTests
{
    [TestClass]
    public class CandlestickTests
    {
        private readonly ExchangeSymbol _Symbol = new ExchangeSymbol(new GenericSymbol(KnownCurrencies.AUD, KnownCurrencies.XRP), Exchange.BTCMarkets);

        [TestMethod]
        public void Constructor_FromPreviousCandlestick_SetsCorrectTimeAndPrices()
        {
            DateTime prevTime = new DateTime(2021, 9, 17, 11, 0, 0);
            Candlestick prev = new Candlestick(prevTime, CandlestickInterval.Minute_1, this._Symbol, 4, 5);

            Candlestick next = new Candlestick(prev);

            Assert.AreEqual(this._Symbol, next.Symbol);
            Assert.AreEqual(prevTime.AddMinutes(1), next.StartTime);
            Assert.AreEqual(5, next.Open);
            Assert.AreEqual(5, next.Close);
            Assert.AreEqual(CandlestickInterval.Minute_1, next.Interval);
        }

        [TestMethod]
        public void Constructor_ThrowsException_IfStartTimeIvalid()
        {
            DateTime startTime = new DateTime(2021, 9, 17, 11, 0, 30);
            DateTime expectedStartTime = new DateTime(2021, 9, 17, 11, 0, 0);

            InvalidStartTimeException e = Assert.ThrowsException<InvalidStartTimeException>(() => new Candlestick(startTime, CandlestickInterval.Minute_1, this._Symbol, 1, 2));
            Assert.AreEqual(expectedStartTime, e.ExpectedTime);
            Assert.AreEqual(startTime, e.ActualTime);
        }

        [TestMethod]
        public void GetNextTime_ReturnsNextTime()
        {
            DateTime time = new DateTime(2021, 9, 17, 11, 0, 0);
            Candlestick candlestick = new Candlestick(time, CandlestickInterval.Minute_1, this._Symbol, 4, 5);

            DateTime nextTime = candlestick.GetNextTime();

            Assert.AreEqual(time.AddMinutes(1), nextTime);
        }

        [TestMethod]
        public void GetPreviousTime_ReturnsPreviousTime()
        {
            DateTime time = new DateTime(2021, 9, 17, 11, 0, 0);
            Candlestick candlestick = new Candlestick(time, CandlestickInterval.Minute_1, this._Symbol, 4, 5);

            DateTime nextTime = candlestick.GetPreviousTime();

            Assert.AreEqual(time.AddMinutes(-1), nextTime);
        }
    }
}
