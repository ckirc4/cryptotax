﻿namespace CryptoTax.Tests
{
    public class Tests
    {
        // Use these constants to categorise special types of tests, e.g. [TestCategory(Tests.LIVE_API)]
        // We can then filter them out in test runs using `-Trait:"LiveAPI" -Trait:"NYI"`

        /// <summary>
        /// Tests that include live APIs and thus require an internet connection and third party service to complete.
        /// </summary>
        public const string LIVE_API = "LiveAPI";
        /// <summary>
        /// Tests that are not yet implemented.
        /// </summary>
        public const string NYI = "NYI";
        /// <summary>
        /// Tests that test tests.
        /// </summary>
        public const string META = "META";
        /// <summary>
        /// Tests that are computationally expensive and require a long time to complete.
        /// </summary>
        public const string LONG = "LONG";
    }
}
