﻿using CryptoTax.Domain.Stores;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CryptoTax.Tests.StoreTests
{
    [TestClass]
    public class AssessableIncomeStoreTests
    {
        private AssessableIncomeStore _Store;

        [TestInitialize]
        public void Setup()
        {
            this._Store = new AssessableIncomeStore();
        }

        [TestMethod]
        public void StoreThrowsIfNegativeValueProvided()
        {
            Assert.ThrowsException<ArgumentException>(() => this._Store.AddAssessableIncome(-1));
        }

        [TestMethod]
        public void InitialIncomeIsZero()
        {
            Assert.AreEqual(0, this._Store.GetTotalAssessableIncome());
        }

        [TestMethod]
        public void CorrectlyTracksAdditionalIncome()
        {
            this._Store.AddAssessableIncome(10);
            Assert.AreEqual(10, this._Store.GetTotalAssessableIncome());

            this._Store.AddAssessableIncome(100);
            Assert.AreEqual(110, this._Store.GetTotalAssessableIncome());

            this._Store.AddAssessableIncome(0);
            Assert.AreEqual(110, this._Store.GetTotalAssessableIncome());
        }
    }
}
