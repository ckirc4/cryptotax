﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using CryptoTax.TransactionFetcher.XRPL;
using CryptoTax.TransactionFetcher.XRPL.ResponseModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.TransactionFetcher
{
    [TestClass]
    public class XRPLTests
    {
        private static readonly Wallet WALLET = KnownWallets.XRP_MY_WALLET_SAVINGS_NEW;
        private static readonly DString FILE_NAME = "test.csv";
        private static readonly FinancialYear FINANCIAL_YEAR = FinancialYear.Y2022;

        private Mock<IFileService> mockFileService;
        private Mock<IXRPLApiService> mockApiService;
        private XRPLService service;

        [TestInitialize]
        public void Setup()
        {
            this.mockFileService = new Mock<IFileService>();
            this.mockApiService = new Mock<IXRPLApiService>();
            this.service = new XRPLService(this.mockApiService.Object, this.mockFileService.Object);
        }

        [TestMethod]
        public async Task ReceivedXRP_HandledCorrectly()
        {
            Transaction tx = new Transaction()
            {
                hash = "123",
                date = DateTime.Now,
                Amount = new Amount() { Currency = "XRP", Value = 123456 },
                Fee = "654321",
                TransactionType = "Payment",
                Account = "sender",
                Destination = WALLET.Address
            };
            this.mockApiService
                .Setup(s => s.GetTransactions(It.IsAny<Wallet>(), It.IsAny<FinancialYear>()))
                .Returns(Task.FromResult(new List<Transaction>() { tx }));

            await this.service.GetAndWriteAllTransactions(WALLET, FILE_NAME, FINANCIAL_YEAR);

            // TxHash,Time,Counterparty,TransactionType,QuantityReceived,CurrencyReceived,QuantitySent,CurrencySent,Fee
            string expectedRow = $"{tx.hash},{tx.date.ToUnix()},{tx.Account},ReceivedFromCounterparty,0.123456,XRP,,,0.654321";
            ICollection<IEnumerable<NString>> capturedLines = new List<IEnumerable<NString>>();
            this.mockFileService.Verify(fs => fs.WriteLines(
                FILE_NAME,
                Capture.In(capturedLines),
                false
            ));
            Assert.AreEqual(expectedRow, capturedLines.Single().ElementAt(1).Value);
        }

        [TestMethod]
        public async Task SentXRP_HandledCorrectly()
        {
            Transaction tx = new Transaction()
            {
                hash = "123",
                date = DateTime.Now,
                Amount = new Amount() { Currency = "XRP", Value = 123456 },
                Fee = "654321",
                TransactionType = "Payment",
                Account = WALLET.Address,
                Destination = "receiver"
            };
            this.mockApiService
                .Setup(s => s.GetTransactions(It.IsAny<Wallet>(), It.IsAny<FinancialYear>()))
                .Returns(Task.FromResult(new List<Transaction>() { tx }));

            await this.service.GetAndWriteAllTransactions(WALLET, FILE_NAME, FINANCIAL_YEAR);

            // TxHash,Time,Counterparty,TransactionType,QuantityReceived,CurrencyReceived,QuantitySent,CurrencySent,Fee
            string expectedRow = $"{tx.hash},{tx.date.ToUnix()},{tx.Destination},SentToCounterparty,,,0.123456,XRP,0.654321";
            ICollection<IEnumerable<NString>> capturedLines = new List<IEnumerable<NString>>();
            this.mockFileService.Verify(fs => fs.WriteLines(
                FILE_NAME,
                Capture.In(capturedLines),
                false
            ));
            Assert.AreEqual(expectedRow, capturedLines.Single().ElementAt(1).Value);
        }

        [TestMethod]
        public async Task NonXRPTransaction_ThrowsException()
        {
            Transaction tx = new Transaction()
            {
                Amount = new Amount() { Currency = "AUD", Value = 123456 },
                TransactionType = "Payment"
            };
            this.mockApiService
                .Setup(s => s.GetTransactions(It.IsAny<Wallet>(), It.IsAny<FinancialYear>()))
                .Returns(Task.FromResult(new List<Transaction>() { tx }));

            await Assert.ThrowsExceptionAsync<NotImplementedException>(() => this.service.GetAndWriteAllTransactions(WALLET, FILE_NAME, FINANCIAL_YEAR));
        }

        [TestMethod]
        public async Task NonPaymentTransaction_ThrowsException()
        {
            Transaction tx = new Transaction()
            {
                Amount = new Amount() { Currency = "XRP", Value = 123456 },
                TransactionType = "Trade"
            };
            this.mockApiService
                .Setup(s => s.GetTransactions(It.IsAny<Wallet>(), It.IsAny<FinancialYear>()))
                .Returns(Task.FromResult(new List<Transaction>() { tx }));

            await Assert.ThrowsExceptionAsync<NotImplementedException>(() => this.service.GetAndWriteAllTransactions(WALLET, FILE_NAME, FINANCIAL_YEAR));
        }
    }
}
