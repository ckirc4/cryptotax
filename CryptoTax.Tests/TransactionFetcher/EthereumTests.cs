﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using CryptoTax.TransactionFetcher.Ethereum;
using UtilityLibrary.Types;
using TokenTx = CryptoTax.TransactionFetcher.Ethereum.OnChainTokenTransferTransaction;
using NormalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainNormalTransaction;
using InternalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainInternalTransaction;
using CryptoTax.Shared;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using CryptoTax.Shared.Services;
using System.Threading.Tasks;
using CryptoTax.Tests.Mocks;

namespace CryptoTax.Tests.TransactionFetcher
{
    [TestClass]
    public class EthereumTests
    {
        private static readonly DString MY_WALLET_OLD = KnownWallets.ETH_MY_WALLET_OLD.Address;
        private static readonly DString MY_WALLET = KnownWallets.ETH_MY_WALLET.Address;
        private static readonly EthereumChainType CHAIN_TYPE = EthereumChainType.Mainnet;
        private static readonly FinancialYear FINANCIAL_YEAR = FinancialYear.Y2022;

        [TestMethod]
        public void OnChainTokenTransferTransactionIsParsedSuccessfully()
        {
            TokenTx tx = this.getSampleTokenTransferTransaction();
            Assert.AreEqual(1560151686L, tx.TimeStamp);
            Assert.AreEqual("0xca57e2138dc5a2f8ebba77179cce7c662aabab4e2e5d91933fa1032d1d3a60fd", (string)tx.Hash);
            Assert.AreEqual("BOMB", (string)tx.TokenName);
        }

        [TestMethod]
        public void OnChainNormalTransactionIsParsedSuccessfully()
        {
            NormalTx tx = this.getSampleNormalTransaction();
            Assert.AreEqual(1560145963L, tx.TimeStamp);
            Assert.AreEqual("0x9969d40a876e369d9f09c821ed6193d0edeca3d983d88064e5d032c02458c9d7", (string)tx.BlockHash);
            Assert.AreEqual(1, tx.TxReceiptStatus);
        }

        [TestMethod]
        public void SentTransactionCsvRowGeneratedCorrectly()
        {
            NormalTx tx = this.getSampleNormalTransaction();
            double ethValue = 2.5; // 2.5 ETH
            long gasUsed = 130000;
            long gasPrice = (long)(28 * Math.Pow(10, 9)); // 28 GWEI
            tx.Hash = "TEST_HASH";
            tx.TimeStamp = 1608628104L;
            tx.Value = (long)(ethValue * Math.Pow(10, 18));
            tx.From = KnownWallets.ETH_MY_WALLET_OLD.Address;
            tx.To = "ToAddress";
            tx.GasUsed = gasUsed;
            tx.GasPrice = gasPrice;

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.To,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                ethValue.ToString(),
                "ETH",
                (gasPrice / (decimal)Math.Pow(10, 18) * gasUsed).ToString()
            }, parts);
            // output format:
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
            // {TEST,1608628104,ToAddress,Unknown,,,2.5,ETH,0.000588000}
        }

        [TestMethod]
        public void SentTransactionWithErrorIgnoresEthAmount()
        {
            NormalTx tx = this.getSampleNormalTransaction();
            double ethValue = 2.5; // 2.5 ETH
            long gasUsed = 130000;
            long gasPrice = (long)(28 * Math.Pow(10, 9)); // 28 GWEI
            tx.Hash = "TEST_HASH";
            tx.TimeStamp = 1608628104L;
            tx.Value = (long)(ethValue * Math.Pow(10, 18));
            tx.From = KnownWallets.ETH_MY_WALLET_OLD.Address;
            tx.To = "ToAddress";
            tx.GasUsed = gasUsed;
            tx.GasPrice = gasPrice;
            tx.IsError = 1;

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.To,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                "0",
                "ETH",
                (gasPrice / (decimal)Math.Pow(10, 18) * gasUsed).ToString()
            }, parts);
            // output format:
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
            // {TEST,1608628104,ToAddress,Unknown,,,0,ETH,0.000588000}
        }

        [TestMethod]
        public void ReceivedTransactionCsvRowGeneratedCorrectly()
        {
            NormalTx tx = this.getSampleNormalTransaction();
            double ethValue = 2.5; // 2.5 ETH
            long gasUsed = 130000;
            long gasPrice = (long)(28 * Math.Pow(10, 9)); // 28 GWEI
            tx.Hash = "TEST_HASH";
            tx.TimeStamp = 1608628104L;
            tx.Value = (long)(ethValue * Math.Pow(10, 18));
            tx.To = KnownWallets.ETH_MY_WALLET_OLD.Address;
            tx.From = "FromAddress";
            tx.GasUsed = gasUsed;
            tx.GasPrice = gasPrice;

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.From,
                EthereumTransactionType.ReceivedFromCounterparty.ToString(),
                ethValue.ToString(),
                "ETH",
                "",
                "",
                (gasPrice / (decimal)Math.Pow(10, 18) * gasUsed).ToString()
            }, parts);
            // output format:
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
            // {TEST,1608628104,ToAddress,Unknown,2.5,ETH,,,0.000588000}
        }

        [TestMethod]
        public void SimpleTradeCsvRowGeneratedCorrectly()
        {
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, "ContractAddress");

            // let's say we buy 50.51 ENJ from someone for 0.1 WETH.
            double enjAmount = 50.51;
            TokenTx tokenIn = this.constructTokenTransferTransaction(enjAmount, "Enjin Coin", "ENJ", 2, "Trader1", MY_WALLET_OLD);

            double wethAmount = 0.1;
            TokenTx tokenOut = this.constructTokenTransferTransaction(wethAmount, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, "Trader1");

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn, tokenOut }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tokenOut.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                enjAmount.ToString(),
                "ENJ",
                wethAmount.ToString(),
                "WETH",
                parts[8]
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void ThirdPartyTransactionNotSaved()
        {
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, "ContractAddress");

            // same trade as in SimpleTradeCsvRowGeneratedCorrectly() test method
            double enjAmount = 50.51;
            TokenTx tokenIn = this.constructTokenTransferTransaction(enjAmount, "Enjin Coin", "ENJ", 2, "Trader1", MY_WALLET_OLD);
            double wethAmount = 0.1;
            TokenTx tokenOut = this.constructTokenTransferTransaction(wethAmount, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, "Trader1");

            // someone burns 10 bomb
            TokenTx burnTx = this.constructTokenTransferTransaction(10, "BOMB", "BOMB", 0, "Trader1", KnownWallets.ETH_BURN_ADDRESS.Address);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn, tokenOut, burnTx }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            Assert.AreEqual(2, file.Count()); // all we need to know is that the burnTx was not included
        }

        [TestMethod]
        public void FullChainTradeRecognisedAsTrade()
        {
            // it is possible that a trade works as follows:
            // A -> B payment made
            // B -> C -> A assets received
            // e.g. https://etherscan.io/tx/0x51feff2df291cc5159cc17eb5b14f242fe7db2040522c346335fc9c626bfc070, where it looks like the counterparty paid the fee

            // sell ENJ for WETH
            double enjAmount = 50.51;
            double enjBurnAmount = 0.05;
            TokenTx tokenOut = this.constructTokenTransferTransaction(enjAmount, "Enjin Coin", "ENJ", 2, MY_WALLET_OLD, "Trader1");
            TokenTx tokenBurn = this.constructTokenTransferTransaction(enjBurnAmount, "Enjin Coin", "ENJ", 2, MY_WALLET_OLD, KnownWallets.ETH_BURN_ADDRESS.Address);

            double wethAmount = 0.1;
            // counterparty pays 10% fees
            TokenTx tokenInSub = this.constructTokenTransferTransaction(wethAmount * 1.1, "Wrapped Ether", "WETH", 18, "Trader1", "Exchange");
            // we receive the rest
            TokenTx tokenIn = this.constructTokenTransferTransaction(wethAmount, "Wrapped Ether", "WETH", 18, "Exchange", MY_WALLET_OLD);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>(), new List<TokenTx>() { tokenBurn, tokenInSub, tokenIn, tokenOut }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts1 = file.ElementAt(1).Value.Split(',');
            string[] parts2 = file.ElementAt(2).Value.Split(',');
            Assert.AreEqual(3, file.Count());
            Assert.AreEqual(9, parts1.Length);
            CollectionAssert.AreEqual(new string[] {
                parts1[0],
                parts1[1],
                tokenOut.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                wethAmount.ToString(),
                "WETH",
                enjAmount.ToString(),
                "ENJ",
                parts1[8]
            }, parts1);
            CollectionAssert.AreEqual(new string[] {
                parts2[0],
                parts2[1],
                tokenBurn.To,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                enjBurnAmount.ToString(),
                "ENJ",
                parts2[8]
            }, parts2);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void PartialChainTradeRecognisedAsTrade()
        {
            // sell ENJ for WETH
            double enjAmount = 50.51;
            double enjBurnAmount = 0.05;
            TokenTx tokenOut = this.constructTokenTransferTransaction(enjAmount, "Enjin Coin", "ENJ", 2, MY_WALLET_OLD, "Trader1");
            TokenTx tokenBurn = this.constructTokenTransferTransaction(enjBurnAmount, "Enjin Coin", "ENJ", 2, MY_WALLET_OLD, KnownWallets.ETH_BURN_ADDRESS.Address);

            double wethAmount = 0.1;
            // counterparty pays 10% fees - this does not show up in our trades list
            //TokenTx tokenInSub = this.constructTokenTransferTransaction(wethAmount * 1.1, "Wrapped Ether", "WETH", 18, "Trader1", "Exchange");
            // we receive the rest
            TokenTx tokenIn = this.constructTokenTransferTransaction(wethAmount, "Wrapped Ether", "WETH", 18, "Exchange", MY_WALLET_OLD);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>(), new List<TokenTx>() { tokenBurn, tokenIn, tokenOut }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts1 = file.ElementAt(1).Value.Split(',');
            string[] parts2 = file.ElementAt(2).Value.Split(',');
            Assert.AreEqual(3, file.Count());
            Assert.AreEqual(9, parts1.Length);
            CollectionAssert.AreEqual(new string[] {
                parts1[0],
                parts1[1],
                tokenOut.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                wethAmount.ToString(),
                "WETH",
                enjAmount.ToString(),
                "ENJ",
                parts1[8]
            }, parts1);
            CollectionAssert.AreEqual(new string[] {
                parts2[0],
                parts2[1],
                tokenBurn.To,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                enjBurnAmount.ToString(),
                "ENJ",
                parts2[8]
            }, parts2);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void TwoTradesWithSameCounterpartyCombinedIntoOneRow()
        {
            // (impossible scenario?)
        }

        [TestMethod]
        public void TwoTradesWithDifferentCounterpartyAndNoNormalTransactionSplitIntoTwoRows()
        {
            // same trade as in SimpleTradeCsvRowGeneratedCorrectly() test method
            double enjAmount1 = 50.51;
            TokenTx tokenIn1 = this.constructTokenTransferTransaction(enjAmount1, "Enjin Coin", "ENJ", 2, "Trader1", MY_WALLET_OLD);

            double wethAmount1 = 0.1;
            TokenTx tokenOut1 = this.constructTokenTransferTransaction(wethAmount1, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, "Trader1");

            double enjAmount2 = 5771;
            TokenTx tokenIn2 = this.constructTokenTransferTransaction(enjAmount2, "Enjin Coin", "ENJ", 2, "Trader2", MY_WALLET_OLD);

            double wethAmount2 = 10.85512;
            TokenTx tokenOut2 = this.constructTokenTransferTransaction(wethAmount2, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, "Trader2");

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>(), new List<TokenTx>() { tokenIn1, tokenOut2, tokenIn2, tokenOut1 }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts1 = file.ElementAt(1).Value.Split(',');
            string[] parts2 = file.ElementAt(2).Value.Split(',');
            Assert.AreEqual(3, file.Count());
            Assert.AreEqual(9, parts1.Length);
            Assert.AreEqual(9, parts2.Length);
            CollectionAssert.AreEqual(new string[] {
                parts1[0],
                parts1[1],
                tokenOut1.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                enjAmount1.ToString(),
                "ENJ",
                wethAmount1.ToString(),
                "WETH",
                parts1[8]
            }, parts1);
            CollectionAssert.AreEqual(new string[] {
                parts2[0],
                parts2[1],
                tokenOut2.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                enjAmount2.ToString(),
                "ENJ",
                wethAmount2.ToString(),
                "WETH",
                parts2[8]
            }, parts2);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void OneWayTradeAddedAsNewRow()
        {
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, "ContractAddress");

            // same trade as in SimpleTradeCsvRowGeneratedCorrectly() test method
            double enjAmount = 50.51;
            TokenTx tokenIn = this.constructTokenTransferTransaction(enjAmount, "Enjin Coin", "ENJ", 2, "Trader1", MY_WALLET_OLD);
            double wethAmount = 0.1;
            TokenTx tokenOut = this.constructTokenTransferTransaction(wethAmount, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, "Trader1");

            // burn 10 bomb
            TokenTx burnTx = this.constructTokenTransferTransaction(10, "BOMB", "BOMB", 0, MY_WALLET_OLD, KnownWallets.ETH_BURN_ADDRESS.Address);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn, tokenOut, burnTx }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(2).Value.Split(','); // burn transaction
            Assert.AreEqual(3, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                parts[0],
                parts[1],
                KnownWallets.ETH_BURN_ADDRESS.Address,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                "10",
                "BOMB",
                parts[8]
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void UnwrappingEtherAddedAsTrade()
        {
            DString to = KnownWallets.ETH_WRAPPED_ETHER_CONTRACT.Address;
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, to);
            InternalTx internalTx = this.constructInternalTransaction(tx, 0.5);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>() { internalTx });
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "0.5",
                "ETH",
                "0.5",
                "WETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void WrappingEtherAddedAsTrade()
        {
            DString to = KnownWallets.ETH_WRAPPED_ETHER_CONTRACT.Address;
            NormalTx tx = this.constructNormalTransaction(0.5, MY_WALLET_OLD, to);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.To,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "0.5",
                "WETH",
                "0.5",
                "ETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void ApprovingEtherWrapAddedAsSentToCounterparty()
        {
            // e.g. https://etherscan.io/tx/0x360fe9fda16862a554a537ba9d3912c651338fb5df7f869a72c45cc117c01cf3
            DString to = KnownWallets.ETH_WRAPPED_ETHER_CONTRACT.Address;
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, to);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>(), new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                tx.To,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                "0",
                "ETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void Hack_TwoOneWay_DDEX()
        {
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET_OLD, "ContractAddress");

            double receivedBomb = 5;
            TokenTx receivedTx = this.constructTokenTransferTransaction(receivedBomb, "BOMB", "BOMB", 0, "TraderAddress", MY_WALLET_OLD);
            double sentWeth = 0.1;
            TokenTx sentTx = this.constructTokenTransferTransaction(sentWeth, "Wrapped Ether", "WETH", 18, MY_WALLET_OLD, KnownWallets.ETH_DDEX_EXCHANGE_WALLET.Address);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { receivedTx, sentTx }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                "TraderAddress",
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                receivedBomb.ToString(),
                "BOMB",
                sentWeth.ToString(),
                "WETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void Hack_BuyingMBTAddsCorrectTrade_Uniswap()
        {
            double ethAmount = 0.15;
            NormalTx tx = this.constructNormalTransaction(ethAmount, MY_WALLET_OLD, KnownWallets.ETH_UNISWAP_V2_ETH_IN.Address);

            double mbtAmount = 6.1;
            TokenTx tokenIn = this.constructTokenTransferTransaction(mbtAmount, "Microbit", "MBT", 2, KnownWallets.ETH_UNISWAP_V2_MBT_OUT.Address, MY_WALLET_OLD);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET_OLD, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                KnownWallets.ETH_UNISWAP_V2_ETH_IN.Address,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "6.1",
                "MBT",
                "0.15",
                "ETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [DataTestMethod]
        [DataRow("v2")]
        [DataRow("v3")]
        public void BuyingAMPL_AddsCorrectTrade_Uniswap(string uniswapVersion)
        {
            DString uniswapRouterAddress;
            if (uniswapVersion == "v2") uniswapRouterAddress = KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER.Address;
            else if (uniswapVersion == "v3") uniswapRouterAddress = KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER.Address;
            else throw new ArgumentException(nameof(uniswapVersion));

            double ethAmount = 0.15;
            NormalTx tx = this.constructNormalTransaction(ethAmount, MY_WALLET, uniswapRouterAddress);

            double amplAmount = 6.1;
            TokenTx tokenIn = this.constructTokenTransferTransaction(amplAmount, "Ampleforth", "AMPL", 2, KnownWallets.ETH_UNISWAP_V2_AMPL_POOL.Address, MY_WALLET);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                uniswapRouterAddress,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "6.1",
                "AMPL",
                "0.15",
                "ETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void BuyingMOON_AddsCorrectTrade_Sushiswap()
        {
            double ethAmount = 0.15;
            NormalTx tx = this.constructNormalTransaction(ethAmount, MY_WALLET, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address);

            double moonAmount = 6.1;
            TokenTx tokenIn = this.constructTokenTransferTransaction(moonAmount, "Moons", "MOON", 2, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_MOON_POOL.Address, MY_WALLET);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenIn }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "6.1",
                "MOON",
                "0.15",
                "ETH",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }


        [DataTestMethod]
        [DataRow("v2")]
        [DataRow("v3")]
        public void SellingAMPL_AddsCorrectTrade_Uniswap(string uniswapVersion)
        {
            DString uniswapRouterAddress;
            if (uniswapVersion == "v2") uniswapRouterAddress = KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER.Address;
            else if (uniswapVersion == "v3") uniswapRouterAddress = KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER.Address;
            else throw new ArgumentException(nameof(uniswapVersion));

            double amplAmount = 6.1;
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET, KnownWallets.ETH_UNISWAP_V2_AMPL_POOL.Address);
            TokenTx tokenTx = this.constructTokenTransferTransaction(amplAmount, "Ampleforth", "AMPL", 2, MY_WALLET, KnownWallets.ETH_UNISWAP_V2_AMPL_POOL.Address);

            double ethAmount = 0.15;
            InternalTx internalTx = this.constructInternalTransaction(this.constructNormalTransaction(ethAmount, uniswapRouterAddress, MY_WALLET), ethAmount);


            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenTx }, new List<InternalTx>() { internalTx });
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                uniswapRouterAddress,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "0.15",
                "ETH",
                "6.1",
                "AMPL",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void SellingMOON_AddsCorrectTrade_Sushiswap()
        {
            double moonAmount = 6.1;
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_MOON_POOL.Address);
            TokenTx tokenTx = this.constructTokenTransferTransaction(moonAmount, "Moons", "MOON", 2, MY_WALLET, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_MOON_POOL.Address);

            double ethAmount = 0.15;
            InternalTx internalTx = this.constructInternalTransaction(this.constructNormalTransaction(ethAmount, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address, MY_WALLET), ethAmount);


            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenTx }, new List<InternalTx>() { internalTx });
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address,
                EthereumTransactionType.TradedWithCounterparty.ToString(),
                "0.15",
                "ETH",
                "6.1",
                "MOON",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void SendingMOON_AddsCorrectTransaction()
        {
            double moonAmount = 6.1;
            DString contractAddress = "0x0057ac2d777797d31cd3f8f13bf5e927571d6ad0";
            DString address = "0xdd4c883739584ed4ea9795ff71b6ddacbfc83d5b";
            NormalTx tx = this.constructNormalTransaction(0, MY_WALLET, contractAddress);
            TokenTx tokenTx = this.constructTokenTransferTransaction(moonAmount, "Moons", "MOON", 2, MY_WALLET, address);

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>() { tx }, new List<TokenTx>() { tokenTx }, new List<InternalTx>());
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            parts[8] = parts[8].TrimEnd('0');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                address,
                EthereumTransactionType.SentToCounterparty.ToString(),
                "",
                "",
                "6.1",
                "MOON",
                (tx.GasUsed * tx.GasPrice / Math.Pow(10, 18)).ToString()
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        public void BridgingEthFromArbNovaToMainnet_DetectedCorrectly()
        {
            InternalTx tx = this.getSampleInternalTransaction();
            tx.BlockNumber = 18047650;
            tx.ContractAddress = null;
            tx.From = "0xb8901acb165ed027e32754e0ffe830802919727f";
            tx.Gas = 401918;
            tx.GasUsed = 0;
            tx.Hash = "0x0ab8d6fd5883de284dba1d962e0cab862924bf5c5b4d87865e91ec4cee02fb49";
            tx.Input = null;
            tx.TimeStamp = 1693643423;
            tx.To = "0xa5478cab569fee520a63d6b43e5faef45ff3ef99";
            tx.Value = 5649181770327116201;

            MockEthereumApiService apiService = new MockEthereumApiService(new List<NormalTx>(), new List<TokenTx>(), new List<InternalTx>() { tx });
            MockFileService fileService = new MockFileService();
            EthereumService service = new EthereumService(apiService, fileService, CHAIN_TYPE);

            service.GetAndWriteAllTransactions(KnownWallets.ETH_MY_WALLET, "test.csv", FINANCIAL_YEAR).Wait();

            IEnumerable<DString> file = fileService.FileSystem_Lines["test.csv"];
            string[] parts = file.ElementAt(1).Value.Split(',');
            Assert.AreEqual(2, file.Count());
            Assert.AreEqual(9, parts.Length);
            CollectionAssert.AreEqual(new string[] {
                tx.Hash,
                (tx.TimeStamp * 1000).ToString(),
                "0xb8901acb165ed027e32754e0ffe830802919727f",
                EthereumTransactionType.ReceivedFromCounterparty.ToString(),
                "5.649181770327116201",
                "ETH",
                "",
                "",
                "0"
            }, parts);
            //  TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        }

        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task NormalTransactionLiveApiWorks_Mainnet()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.Mainnet);
            IEnumerable<NormalTx> tx = await api.GetNormalTransactions(KnownWallets.ETH_MY_WALLET_OLD, FinancialYear.Y2018);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0xa04315f998a4c60b958c027dbba8faa93f78fd0e1028453a81e248a43654ede0"));
        }

        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task TokenTransferTransactionLiveApiWorks_Mainnet()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.Mainnet);
            IEnumerable<TokenTx> tx = await api.GetTokenTransferTransactions(KnownWallets.ETH_MY_WALLET_OLD, FinancialYear.Y2018);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0xca57e2138dc5a2f8ebba77179cce7c662aabab4e2e5d91933fa1032d1d3a60fd"));
        }

        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task InternalTransactionLiveApiWorks_Mainnet()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.Mainnet);
            IEnumerable<InternalTx> tx = await api.GetInternalTransactions(KnownWallets.ETH_MY_WALLET_OLD, FinancialYear.Y2018);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0x7194063f76f4275c7a08482620b7a56cc3aa90ef4894ac869bd418d99255042e"));
        }


        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task NormalTransactionLiveApiWorks_ArbNova()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.ArbitrumNova);
            IEnumerable<NormalTx> tx = await api.GetNormalTransactions(KnownWallets.ETH_MY_WALLET_ARB_NOVA, FinancialYear.Y2022);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0x3959efe0f32e18819a7bce699849d9072c6fc551eb9746cf94cfb184db25606d"));
        }

        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task TokenTransferTransactionLiveApiWorks_ArbNova()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.ArbitrumNova);
            IEnumerable<TokenTx> tx = await api.GetTokenTransferTransactions(KnownWallets.ETH_MY_WALLET_ARB_NOVA, FinancialYear.Y2022);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0xb8c24b9307a66910b839bacd25807b1925994d60a88e3cd340e466a68b161ac5"));
        }

        [TestMethod]
        [TestCategory("LiveAPI")]
        public async Task InternalTransactionLiveApiWorks_ArbNova()
        {
            EthereumApiService api = new EthereumApiService(new WebService(), EthereumChainType.ArbitrumNova);
            IEnumerable<InternalTx> tx = await api.GetInternalTransactions(KnownWallets.ETH_MY_WALLET_ARB_NOVA, FinancialYear.Y2022);
            Assert.IsNotNull(tx.FirstOrDefault(x => x.Hash == "0x4f1d480ee712fd0684695aa2aad488bd395c361c21320044ea2e4bd60cac43dc"));
        }

        private TokenTx constructTokenTransferTransaction(double tokenAmount, string tokenName, string tokenSymbol, int tokenDecimal, string from, string to, long gasUsed = 130000, long gasPrice = 28000000000, string hash = "TEST_HASH", long timestamp = 1608628104L)
        {
            TokenTx ttx = this.getSampleTokenTransferTransaction();
            ttx.Hash = hash;
            ttx.TimeStamp = timestamp;
            ttx.Value = new BigInteger(tokenAmount * Math.Pow(10, tokenDecimal));
            ttx.From = from;
            ttx.To = to;
            ttx.GasUsed = gasUsed;
            ttx.GasPrice = gasPrice;
            ttx.TokenDecimal = tokenDecimal;
            ttx.TokenName = tokenName;
            ttx.TokenSymbol = tokenSymbol;
            return ttx;
        }

        private NormalTx constructNormalTransaction(double ethAmount, string from, string to, long gasUsed = 130000, long gasPrice = 28000000000, string hash = "TEST_HASH", long timestamp = 1608628104L)
        {
            NormalTx tx = this.getSampleNormalTransaction();
            tx.Hash = hash;
            tx.TimeStamp = timestamp;
            tx.Value = (long)(ethAmount * Math.Pow(10, 18));
            tx.From = from;
            tx.To = to;
            tx.GasUsed = gasUsed;
            tx.GasPrice = gasPrice;
            return tx;
        }

        private InternalTx constructInternalTransaction(NormalTx normalTx, double ethAmount)
        {
            InternalTx tx = this.getSampleInternalTransaction();
            tx.BlockNumber = normalTx.BlockNumber;
            tx.TimeStamp = normalTx.TimeStamp;
            tx.Hash = normalTx.Hash;
            tx.From = normalTx.From;
            tx.To = normalTx.To;
            tx.Value = (long)(ethAmount * Math.Pow(10, 18));
            return tx;
        }

        private TokenTx getSampleTokenTransferTransaction()
        {
            DString data = "{\"blockNumber\":\"7929864\",\"timeStamp\":\"1560151686\",\"hash\":\"0xca57e2138dc5a2f8ebba77179cce7c662aabab4e2e5d91933fa1032d1d3a60fd\",\"nonce\":\"76391\",\"blockHash\":\"0x1e259e553fcada48fdefa13fadcb5b3a398fb86185c665fd1ec39a21487709a3\",\"from\":\"0x33152b0176dd5b08b523838fe5719e78cfd5b63d\",\"contractAddress\":\"0x1c95b093d6c236d3ef7c796fe33f9cc6b8606714\",\"to\":\"0x7042db4c642f6ac868df6717b65be6d4590ea720\",\"value\":\"3\",\"tokenName\":\"BOMB\",\"tokenSymbol\":\"BOMB\",\"tokenDecimal\":\"0\",\"transactionIndex\":\"78\",\"gas\":\"1000000\",\"gasPrice\":\"11900000000\",\"gasUsed\":\"313111\",\"cumulativeGasUsed\":\"3019408\",\"input\":\"deprecated\",\"confirmations\":\"3556434\"}";
            dynamic jObject = JObject.Parse(data) as dynamic;
            TokenTx tx = new TokenTx(jObject);
            return tx;
        }

        private NormalTx getSampleNormalTransaction()
        {
            DString data = "{\"blockNumber\":\"7929423\",\"timeStamp\":\"1560145963\",\"hash\":\"0xa04315f998a4c60b958c027dbba8faa93f78fd0e1028453a81e248a43654ede0\",\"nonce\":\"3184318\",\"blockHash\":\"0x9969d40a876e369d9f09c821ed6193d0edeca3d983d88064e5d032c02458c9d7\",\"transactionIndex\":\"20\",\"from\":\"0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be\",\"to\":\"0x7042db4c642f6ac868df6717b65be6d4590ea720\",\"value\":\"5400000000000000000\",\"gas\":\"21000\",\"gasPrice\":\"40000000000\",\"isError\":\"0\",\"txreceipt_status\":\"1\",\"input\":\"0x\",\"contractAddress\":\"\",\"cumulativeGasUsed\":\"802024\",\"gasUsed\":\"21000\",\"confirmations\":\"3557525\"}";
            dynamic jObject = JObject.Parse(data) as dynamic;
            NormalTx tx = new NormalTx(jObject);
            return tx;
        }

        private InternalTx getSampleInternalTransaction()
        {
            DString data = "{\"blockNumber\":\"7975868\",\"timeStamp\":\"1560774069\",\"hash\":\"0x7194063f76f4275c7a08482620b7a56cc3aa90ef4894ac869bd418d99255042e\",\"from\":\"0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2\",\"to\":\"0x7042db4c642f6ac868df6717b65be6d4590ea720\",\"value\":\"7683300000000000000\",\"contractAddress\":\"\",\"input\":\"\",\"type\":\"call\",\"gas\":\"2300\",\"gasUsed\":\"0\",\"traceId\":\"0\",\"isError\":\"0\",\"errCode\":\"\"}";
            dynamic jObject = JObject.Parse(data) as dynamic;
            InternalTx tx = new InternalTx(jObject);
            return tx;
        }
    }
}
