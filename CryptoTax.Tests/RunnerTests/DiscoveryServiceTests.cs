﻿using CryptoTax.DTO;
using CryptoTax.Runner.Models;
using CryptoTax.Runner.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CryptoTax.Tests.RunnerTests
{
    [TestClass]
    public class DiscoveryServiceTests
    {
        private string _Executable;
        private DiscoverySettings _Settings;
        private Mock<IProcessService> _MockProcessService;
        private Mock<IResultsService> _MockResultsService;
        private DiscoveryService _DiscoveryService;

        [TestInitialize]
        public void Setup()
        {
            this._Executable = "Program.exe";
            this._Settings = getTestSettings();
            this._MockProcessService = new Mock<IProcessService>();
            this._MockResultsService = new Mock<IResultsService>();
            this._DiscoveryService = new DiscoveryService(this._Executable, this._Settings, this._MockProcessService.Object, this._MockResultsService.Object);
        }

        [TestMethod]
        public void DoDiscovery_RunsProcessAndCalculatesResults()
        {
            int loadStateFromRunNumber = 5;
            Configuration configuration = new Configuration(InventoryType.FILO, CandlestickInterval.Day_1, PriceCalculationType.Close, InventoryManagementType.Single);

            string expectedArgs = Runner.Helpers.ConstructArgs(FinancialYear.Y2016, InventoryType.FILO, CandlestickInterval.Day_1, PriceCalculationType.Close, InventoryManagementType.Single, false, false, false, loadStateFromRunNumber);
            ProcessResult processResult = new ProcessResult(1, expectedArgs, new TimeSpan(50000));
            this._MockProcessService.Setup(p => p.RunProcessToCompletion(
                It.Is<ProcessStartInfo>(startInfo => startInfo.Arguments == expectedArgs && startInfo.FileName == this._Executable)))
                .Returns(processResult);

            CalculationResultsDTO calculationResultsDto = new CalculationResultsDTO(new CapitalGainsDTO(2, 4, -1.2m), 4, 1.5m);
            this._MockResultsService.Setup(r => r.GetResultsForRun(1)).Returns(calculationResultsDto);

            Dictionary<FinancialYear, SingleResult> singleResults = new Dictionary<FinancialYear, SingleResult>()
            {
                { FinancialYear.Y2016, new SingleResult(processResult, calculationResultsDto) }
            };
            AggregateResult aggregateResult = new AggregateResult(configuration, singleResults, new Dictionary<FinancialYear, decimal>(), 1, 1, 1);
            Dictionary<Configuration, AggregateResult> aggregateResults = new Dictionary<Configuration, AggregateResult>()
            {
                {configuration, aggregateResult }
            };
            this._MockResultsService.Setup(r => r.GetAggregateResult(
                It.Is<Configuration>(c => 
                    JsonConvert.SerializeObject(c) == JsonConvert.SerializeObject(configuration)),
                It.Is<Dictionary<FinancialYear, SingleResult>>(sr => 
                    sr.First().Key == FinancialYear.Y2016 &&
                    JsonConvert.SerializeObject(sr.Single().Value) == JsonConvert.SerializeObject(singleResults.Single().Value))))
                .Returns(aggregateResult);

            Dictionary<Configuration, AggregateResult> actualDiscoveryResult = this._DiscoveryService.DoDiscovery(false, false, false, loadStateFromRunNumber);

            this._MockProcessService.VerifyAll();
            this._MockResultsService.VerifyAll();
            Assert.AreEqual(JsonConvert.SerializeObject(aggregateResults), JsonConvert.SerializeObject(actualDiscoveryResult));
        }

        private static DiscoverySettings getTestSettings()
        {
            IEnumerable<FinancialYear> financialYears = new[] { FinancialYear.Y2016 };
            IEnumerable<InventoryType> inventoryTypes = new[] { InventoryType.FILO };
            IEnumerable<CandlestickInterval> candlestickIntervals = new[] { CandlestickInterval.Day_1 };
            IEnumerable<PriceCalculationType> priceCalculationTypes = new[] { PriceCalculationType.Close };
            IEnumerable<InventoryManagementType> inventoryManagementTypes = new[] { InventoryManagementType.Single };

            return new DiscoverySettings(financialYears, inventoryTypes, candlestickIntervals, priceCalculationTypes, inventoryManagementTypes);
        }
    }
}
