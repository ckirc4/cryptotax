﻿using CryptoTax.DTO;
using CryptoTax.Runner.Models;
using CryptoTax.Runner.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.RunnerTests
{
    [TestClass]
    public class ResultsServiceTests
    {
        private Func<DString, DString> _RunOutputFolder;
        private DString _DiscoveryOutputFolder;
        private Mock<IFileService> _MockFileService;
        private ResultsService _ResultsService;

        [TestInitialize]
        public void Setup()
        {
            this._RunOutputFolder = (runId) => $"BaseFolder\\Output\\{runId}\\";
            this._DiscoveryOutputFolder =$"BaseFolder\\DiscoveryOutput\\";
            this._MockFileService = new Mock<IFileService>();
            this._ResultsService = new ResultsService(this._MockFileService.Object, this._RunOutputFolder, this._DiscoveryOutputFolder);
        }

        [TestMethod]
        public void GetResultsForRun_LoadsCorrectFile()
        {
            int runId = 31;
            CalculationResultsDTO calculationResults = new CalculationResultsDTO(new CapitalGainsDTO(2, 4, -1.2m), 4, 1.5m);
            this._MockFileService.Setup(f => f.ReadAndDeserialiseFile<CalculationResultsDTO>(this._RunOutputFolder($"#n{runId}") + "Results.json")).Returns(calculationResults);

            CalculationResultsDTO actual = this._ResultsService.GetResultsForRun(runId);

            this._MockFileService.VerifyAll();
            Assert.AreEqual(calculationResults, actual);
        }

        [TestMethod]
        public void SaveDiscoveryResults_SavesToFile()
        {
            CalculationResultsDTO calculationResults = new CalculationResultsDTO(new CapitalGainsDTO(2, 4, -1.2m), 4, 1.5m);
            ProcessResult processResult = new ProcessResult(31, "testKey=testVale", new TimeSpan(50000));
            Dictionary<FinancialYear, SingleResult> singleResults = new Dictionary<FinancialYear, SingleResult>()
            {
                { FinancialYear.Y2016, new SingleResult(processResult, calculationResults) }
            };
            Dictionary<FinancialYear, decimal> taxPayableOn = new Dictionary<FinancialYear, decimal>() { { FinancialYear.Y2016, 540.14m } };

            Configuration configuration = new Configuration(InventoryType.FILO, CandlestickInterval.Day_1, PriceCalculationType.Close, InventoryManagementType.Single);
            Dictionary<Configuration, AggregateResult> results = new Dictionary<Configuration, AggregateResult>()
            {
                { configuration, new AggregateResult(configuration, singleResults, taxPayableOn, 2, 3, 4) }
            };

            this._ResultsService.SaveDiscoveryResults(results);

            this._MockFileService.Verify(f => f.WriteFile(this._DiscoveryOutputFolder + "DiscoveryResults.json", JsonConvert.SerializeObject(results, Formatting.Indented), false));
        }

        [TestMethod]
        public void GetAggregateResult_CorrectlyCalculatesValues()
        {
            Configuration configuration = new Configuration(InventoryType.FILO, CandlestickInterval.Day_1, PriceCalculationType.Close, InventoryManagementType.Single);
            CalculationResultsDTO calculationResults2016 = new CalculationResultsDTO(new CapitalGainsDTO(2, 2, -4), -1, 0.5m); // total cg: -1. total ai: 0.5
            CalculationResultsDTO calculationResults2017 = new CalculationResultsDTO(new CapitalGainsDTO(1.3m, 0, 0), 1.3m, 0.25m); // total cg: 1.3. total ai: 0.25
            CalculationResultsDTO calculationResults2018 = new CalculationResultsDTO(new CapitalGainsDTO(0, 0, -2), -2, 0.1m); // total cg: -2. total ai: 0.1
            ProcessResult processResult = new ProcessResult(31, "testKey=testVale", new TimeSpan(50000));
            Dictionary<FinancialYear, SingleResult> singleResults = new Dictionary<FinancialYear, SingleResult>()
            {
                { FinancialYear.Y2016, new SingleResult(processResult, calculationResults2016) },
                { FinancialYear.Y2017, new SingleResult(processResult, calculationResults2017) },
                { FinancialYear.Y2018, new SingleResult(processResult, calculationResults2018) }
            };

            AggregateResult aggregateResult = this._ResultsService.GetAggregateResult(configuration, singleResults);

            Assert.AreEqual(configuration, aggregateResult.Configuration);
            Assert.AreEqual(singleResults, aggregateResult.Results);

            Assert.AreEqual(0.5m, aggregateResult.TaxPayableOn[FinancialYear.Y2016]);
            Assert.AreEqual(0.3m + 0.25m, aggregateResult.TaxPayableOn[FinancialYear.Y2017]);
            Assert.AreEqual(0.1m, aggregateResult.TaxPayableOn[FinancialYear.Y2018]);

            Assert.AreEqual((1.3m - 1) + 0, aggregateResult.SumCapitalGains); // 2018 doesn't count towards sum of capital gains, since it is a loss
            Assert.AreEqual(-2, aggregateResult.CapitalLossesToCarryForward); // all losses before 2018 were "used up", so only the 2018 losses count
            Assert.AreEqual(0.5m + 0.25m + 0.1m, aggregateResult.SumAssessableIncome);
        }
    }
}
