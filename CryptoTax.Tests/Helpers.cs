﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UtilityLibrary.Types;

namespace CryptoTax.Tests
{
    internal static class Helpers
    {
        /// <summary>
        /// Quickly creates a dictionary with a single key and a collection of values.
        /// </summary>
        /// <typeparam name="K">Type of the key.</typeparam>
        /// <typeparam name="V">Type of the collection.</typeparam>
        /// <typeparam name="C">Constructor input (possibly a tuple).</typeparam>
        /// <param name="constructor">Given an input of type C (possibly a tuple), outputs an instance of a single value to be added to the key's collection in the dictionary.</param>
        /// <param name="constructorValues">The values to pass into the constructor.</param>
        public static Dictionary<K, IEnumerable<V>> QuickDictCtor<K, V, C>(K key, Func<C, V> constructor, params C[] constructorValues)
        {
            Dictionary<K, IEnumerable<V>> dict = new Dictionary<K, IEnumerable<V>>
            {
                { key, new List<V>() }
            };

            foreach (C val in constructorValues)
            {
                dict[key] = dict[key].Append(constructor(val));
            }

            return dict;
        }

        /// <summary>
        /// Quickly creates a dictionary with multiple keys and collections of values.
        /// </summary>
        /// <typeparam name="K">Type of the key.</typeparam>
        /// <typeparam name="V">Type of the collection.</typeparam>
        /// <param name="key">The first key.</param>
        /// <param name="value">The first value.</param>
        /// <param name="additional">Any additional keys or values.
        /// If value, it will be added to the collection for the key that has last been mentioned in the parameter list.
        /// If key, will create key (if doesn't exist yet) or add subsequent values to the key's collection until next key.</param>
        /// <returns></returns>
        public static Dictionary<K, IEnumerable<V>> QuickDict<K, V>(K key, V value, params object[] additional)
        {
            K currentKey = key;
            Dictionary<K, IEnumerable<V>> dict = new Dictionary<K, IEnumerable<V>>
            {
                { key, new List<V>() { value } }
            };

            foreach (object arg in additional)
            {
                if (arg.GetType().Equals(typeof(K)))
                {
                    // is key
                    currentKey = (K)arg;
                    if (!dict.ContainsKey(currentKey))
                    {
                        dict.Add(currentKey, new List<V>());
                    }
                }
                else if (arg.GetType().Equals(typeof(V)))
                {
                    // is value
                    dict[currentKey] = dict[currentKey].Append((V)arg);
                }
                else
                {
                    throw new ArgumentException($"Argument must be of type {typeof(K)} or {typeof(V)} but was of type {arg.GetType()}.");
                }
            }

            return dict;
        }

        /// <summary>
        /// Compares public instance properties of the two objects. Returns property names that were not equal - if the returned string is null, objects were considered equal.
        /// </summary>
        // annoyingly cannot just directly compare objects... see UTIL-10
        public static NString CompareObjects<T>(T expected, T actual, params string[] excludeProperties)
        {
            if (expected == null && actual == null) return null;
            else if (expected == null && actual != null) return "Object should be null";
            else if (expected != null && actual == null) return "Object should not be null";

            List<DString> results = new List<DString>();

            Type type = typeof(T);
            foreach (PropertyInfo pi in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (excludeProperties.Contains(pi.Name)) continue;

                object expectedValue = type.GetProperty(pi.Name).GetValue(expected, null);
                object actualValue = type.GetProperty(pi.Name).GetValue(actual, null);

                if (expectedValue == null && actualValue == null)
                {
                    continue;
                }
                else if (expectedValue == null && actualValue != null || !expectedValue.Equals(actualValue))
                {
                    results.Add($"{pi.Name} (ex: {expectedValue}, ac: {actualValue})");
                }
            }

            if (results.Any())
            {
                return string.Join(", ", results);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Compares the given record to the expected values. Only does a single assertion to group together all comparisons into a single test.
        /// </summary>
        public static void PerformRecordTest(Record record, TradeDTO expectedTrade, TransactionDTO expectedTransaction)
        {
            // check everything in one go so that we can see every error at a glance.
            TradeDTO actualTrade = record.ToTradeDTO();
            TransactionDTO actualTransaction = record.ToTransactionDTO();
            performTradeAndTransactionTest(expectedTrade, actualTrade, expectedTransaction, actualTransaction);
        }

        public static void PerformRecordGroupTest(RecordGroup group, TradeDTO expectedTrade, TransactionDTO expectedTransaction)
        {
            // check everything in one go so that we can see every error at a glance.
            TradeDTO actualTrade = group.ToTradeDTO();
            TransactionDTO actualTransaction = group.ToTransactionDTO();
            performTradeAndTransactionTest(expectedTrade, actualTrade, expectedTransaction, actualTransaction);
        }

        private static void performTradeAndTransactionTest(TradeDTO expectedTrade, TradeDTO actualTrade, TransactionDTO expectedTransaction, TransactionDTO actualTransaction)
        {
            NString tradeComparison = CompareObjects(expectedTrade, actualTrade, nameof(TradeDTO.Symbol), nameof(TradeDTO.ContainerEvent));
            NString symbolComparison = CompareObjects(expectedTrade?.Symbol, actualTrade?.Symbol);
            NString tradeEventComparison = CompareObjects(expectedTrade?.ContainerEvent, actualTrade?.ContainerEvent);
            NString transactionComparison = CompareObjects(expectedTransaction, actualTransaction, nameof(TransactionDTO.ContainerEvent));
            NString transactionEventComparison = CompareObjects(expectedTransaction?.ContainerEvent, actualTransaction?.ContainerEvent);

            List<string> resultList = new List<string>();
            if (tradeComparison != null) resultList.Add($"Trade had incorrect values for properties: {tradeComparison}");
            if (tradeEventComparison != null) resultList.Add($"Trade event had incorrect values for properties: {tradeEventComparison}");
            if (symbolComparison != null) resultList.Add($"Symbol had incorrect values for properties: {symbolComparison}");
            if (transactionComparison != null) resultList.Add($"Transaction had incorrect values for properties: {transactionComparison}");
            if (transactionEventComparison != null) resultList.Add($"Transaction event had incorrect values for properties: {transactionEventComparison}");

            string result = string.Join(" | ", resultList);
            Assert.IsTrue(string.IsNullOrEmpty(result), result);
        }
    }
}
