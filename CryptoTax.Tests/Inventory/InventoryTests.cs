﻿using CryptoTax.Domain;
using CryptoTax.Domain.Enums;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Models.Reporting;
using CryptoTax.Domain.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace CryptoTax.Tests.Inventory
{
    /// <summary>
    /// Tests for the base <see cref="Inventory"/> class.
    /// </summary>
    [TestClass]
    public class InventoryTests
    {
        private readonly static DateTime Time1 = DateTime.Now;
        private readonly static DateTime Time2 = DateTime.Now.AddHours(1);
        private readonly static DateTime Time3 = DateTime.Now.AddHours(2);
        private readonly static Exchange Exchange = Exchange.BTCMarkets;

        protected IdProviderService _IdProviderService;
        protected MockTrackingReportingService _ReportingService;
        protected MockLoggingService _LoggingService;

        /// <summary>
        /// For legacy reasons, this is of type FILO
        /// </summary>
        private IInventory _Inventory;
        /// <summary>
        /// 30 XRP valued at 60 AUD
        /// </summary>
        private CurrencyAmount _Purchase1;
        /// <summary>
        /// 30 XRP valued at 90 AUD
        /// </summary>
        private CurrencyAmount _Purchase2;

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._ReportingService = new MockTrackingReportingService();
            this._LoggingService = new MockLoggingService();
            this._Inventory = new FILOInventoryFactory().Create(this._IdProviderService, this._ReportingService, this._LoggingService, Exchange, "FILO Test Inventory");

            this._Purchase1 = new CurrencyAmount(this._IdProviderService, "XRP", 30, 60, Time1, Exchange);
            this._Purchase2 = new CurrencyAmount(this._IdProviderService, "XRP", 30, 90, Time2, Exchange);
        }

        [TestMethod]
        public void EnsureFactoryDefinitions()
        {
            foreach (InventoryType type in Enum.GetValues(typeof(InventoryType)))
            {
                if (type == InventoryType.MockImmutable || type == InventoryType.MockTracking) continue;

                IInventoryFactory inventory = InventoryFactoryGetter.Get(type);
                Assert.IsNotNull(inventory);
            }
        }

        [TestMethod]
        public void Purchase_CallsAdapterAdd()
        {
            object collection = new object();
            Mock<IInventoryAdapter<object>> adapter = new Mock<IInventoryAdapter<object>>();
            adapter.Setup(a => a.Instantiate()).Returns(collection);
            Inventory<object> inventory = new Inventory<object>(this._IdProviderService, this._ReportingService, this._LoggingService, InventoryType.MockImmutable, "Test", Exchange, adapter.Object);

            inventory.Purchase(this._Purchase1);

            adapter.Verify(a => a.Add(collection, this._Purchase1, Time1));
        }

        [TestMethod]
        public void TransferIn_CallsAdapterAdd()
        {
            object collection = new object();
            Mock<IInventoryAdapter<object>> adapter = new Mock<IInventoryAdapter<object>>();
            adapter.Setup(a => a.Instantiate()).Returns(collection);
            Inventory<object> inventory = new Inventory<object>(this._IdProviderService, this._ReportingService, this._LoggingService, InventoryType.MockImmutable, "Test", Exchange, adapter.Object);

            inventory.TransferIn(this._Purchase1, this._Purchase1.TimeModified);

            adapter.Verify(a => a.Add(collection, this._Purchase1, Time1));
        }

        [TestMethod]
        public void Disposal_CallsAdapterRemoveMany()
        {
            object collection = new object();
            Mock<IInventoryAdapter<object>> adapter = new Mock<IInventoryAdapter<object>>();
            adapter.Setup(a => a.Instantiate()).Returns(collection);
            Inventory<object> inventory = new Inventory<object>(this._IdProviderService, this._ReportingService, this._LoggingService, InventoryType.MockImmutable, "Test", Exchange, adapter.Object);

            inventory.Disposal(this._Purchase1, 0, out decimal _);

            decimal overdrawnAmount;
            adapter.Verify(a => a.RemoveMany(collection, this._Purchase1.Amount, this._Purchase1.TimeModified, out overdrawnAmount));
        }

        [TestMethod]
        public void TransferOut_CallsAdapterRemoveMany()
        {
            object collection = new object();
            Mock<IInventoryAdapter<object>> adapter = new Mock<IInventoryAdapter<object>>();
            adapter.Setup(a => a.Instantiate()).Returns(collection);
            Inventory<object> inventory = new Inventory<object>(this._IdProviderService, this._ReportingService, this._LoggingService, InventoryType.MockImmutable, "Test", Exchange, adapter.Object);

            inventory.TransferOut(this._Purchase1, TransferOutReason.Transfer, out decimal _);

            decimal overdrawnAmount;
            adapter.Verify(a => a.RemoveMany(collection, this._Purchase1.Amount, this._Purchase1.TimeModified, out overdrawnAmount));
        }

        [TestMethod]
        public void AverageAdapter_Add_AddsCorrectReportingData()
        {
            DateTime time = DateTime.Now;
            Func<decimal, decimal, CurrencyAmount> CA = (amount, value) => new CurrencyAmount(this._IdProviderService, "XRP", amount, value, time);

            AverageInventoryAdapter adapter = new AverageInventoryAdapter(this._IdProviderService, this._ReportingService);
            CurrencyAmount existingAmount = CA(10, 20);
            Ref<CurrencyAmount> collection = new Ref<CurrencyAmount>(existingAmount);
            CurrencyAmount addingAmount = CA(5, 2);

            adapter.Add(collection, addingAmount, time);

            CurrencyAmount expectedAmount = CA(15, 22);
            ReportingMergeData data = this._ReportingService.AddedData.Single() as ReportingMergeData;
            Assert.IsTrue(data.From1 == existingAmount);
            Assert.IsTrue(data.From2 == addingAmount);
            Assert.IsTrue(data.To == expectedAmount && collection.Get() == expectedAmount);
        }

        [TestMethod]
        public void AverageAdapter_RemoveMany_AddsCorrectReportingData()
        {
            DateTime time = DateTime.Now;
            Func<decimal, decimal, CurrencyAmount> CA = (amount, value) => new CurrencyAmount(this._IdProviderService, "XRP", amount, value, time);

            AverageInventoryAdapter adapter = new AverageInventoryAdapter(this._IdProviderService, this._ReportingService);
            CurrencyAmount existingAmount = CA(10, 20);
            Ref<CurrencyAmount> collection = new Ref<CurrencyAmount>(existingAmount);
            decimal removingQty = 2;

            CurrencyAmount removed = adapter.RemoveMany(collection, removingQty, time, out decimal overdrawn).Single();

            CurrencyAmount expectedRemainingAmount = CA(8, 16);
            CurrencyAmount expectedRemovedAmount = CA(2, 4);
            ReportingSplitData data = this._ReportingService.AddedData.Single() as ReportingSplitData;
            Assert.IsTrue(data.From == existingAmount);
            Assert.IsTrue(data.To1 == expectedRemainingAmount && collection.Get() == expectedRemainingAmount);
            Assert.IsTrue(data.To2 == expectedRemovedAmount);
        }

        [TestMethod]
        public void TransferOut_ReturnsCorrectOverdrawnAmount_EmptyInventory()
        {
            // I don't really know why this test is here, but it's a legacy test so it won't hurt to have it.
            decimal expectedAmount = 100;
            CurrencyAmount transfer = new CurrencyAmount(this._IdProviderService, "XRP", expectedAmount, 0, Time1, Exchange);

            CurrencyAmount output = this._Inventory.TransferOut(transfer, TransferOutReason.Transfer, out decimal overdrawn).Single();

            Assert.AreEqual(expectedAmount, overdrawn);
            Assert.AreEqual(expectedAmount, output.Amount);
            Assert.AreEqual(0, output.Value);
            Assert.AreEqual("XRP", output.Currency.Value);
            Assert.AreEqual(Time1, output.TimeModified);
            Assert.AreEqual(null, output.ExchangeAcquired);
        }

        #region reporting tests (legacy)
        [TestMethod]
        public void Purchase_AddsCorrectReportingData()
        {
            // identical to CorrectReportingDataAddedForTransferIn - but don't want to simply call method here because implementation may be decoupled in the future
            CurrencyAmount purchase = new CurrencyAmount(this._IdProviderService, "XRP", 100, 150, Time1, Exchange);

            this._Inventory.Purchase(purchase);

            Assert.AreEqual(1, this._ReportingService.AddedData.Count());
            ReportingAddData data = this._ReportingService.AddedData.First() as ReportingAddData;
            Assert.IsNotNull(data);
            Assert.AreEqual(Time1, data.Time);
            Assert.IsTrue(purchase == data.CurrencyAmount);
            Assert.AreEqual(this._Inventory, data.Inventory);
        }

        [TestMethod]
        public void Disposal_AddsCorrectReportingData_NoOverdraw()
        {
            // process: add purchase data to initialise inventory, clear reporting, then add dispose
            CurrencyAmount disposal = new CurrencyAmount(this._IdProviderService, "XRP", 50, 200, Time3, Exchange);
            decimal additionalBasisCost = 0.1m; // fees

            this._Inventory.Purchase(this._Purchase1);
            this._Inventory.Purchase(this._Purchase2);
            this._ReportingService.ClearData();
            this._Inventory.Disposal(disposal, additionalBasisCost, out decimal overdrawnAmount);

            Assert.AreEqual(0, overdrawnAmount);
            Assert.AreEqual(3, this._ReportingService.AddedData.Count());
            ReportingSplitData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingSplitData;
            ReportingRemoveData data2 = this._ReportingService.AddedData.ElementAt(1) as ReportingRemoveData;
            ReportingRemoveData data3 = this._ReportingService.AddedData.ElementAt(2) as ReportingRemoveData;

            Assert.AreEqual(Time3, data1.Time);
            Assert.IsTrue(this._Purchase1 == data1.From);
            CurrencyAmount caInInventory = this._Inventory.GetCurrentInventory().Values.First().First();
            Assert.IsTrue(data3.RemovedAmount == data1.To1 && caInInventory == data1.To2 || caInInventory == data1.To1 && data3.RemovedAmount == data1.To2);

            Assert.IsFalse(data2.IsPhantomRemoval);
            Assert.AreEqual(Time3, data2.Time);
            Assert.IsFalse(data2.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data2.OriginalAmount);
            Assert.IsTrue(this._Purchase2 == data2.RemovedAmount);

            Assert.IsFalse(data3.IsPhantomRemoval);
            Assert.AreEqual(Time3, data3.Time);
            Assert.IsFalse(data3.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data3.OriginalAmount);
            decimal actualValue = data3.RemovedAmount.Value;
            Assert.IsTrue(Math.Abs((double)(actualValue - 40)) < 1e-12); // yucky rounding error
            Assert.IsTrue(this._Purchase1.WithAmount(20, updateTime: Time3, scaleValue: true).WithValue(actualValue) == data3.RemovedAmount);
        }

        [TestMethod]
        public void Disposal_AddsCorrectReportingData_PartialOverdraw()
        {
            CurrencyAmount disposal = new CurrencyAmount(this._IdProviderService, "XRP", 80, 200, Time3, Exchange);
            decimal additionalBasisCost = 0.1m; // fees

            this._Inventory.Purchase(this._Purchase1);
            this._Inventory.Purchase(this._Purchase2);
            this._ReportingService.ClearData();
            this._Inventory.Disposal(disposal, additionalBasisCost, out decimal overdrawnAmount);

            Assert.AreEqual(20, overdrawnAmount);
            Assert.AreEqual(3, this._ReportingService.AddedData.Count());
            ReportingRemoveData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingRemoveData;
            ReportingRemoveData data2 = this._ReportingService.AddedData.ElementAt(1) as ReportingRemoveData;
            ReportingRemoveData data3 = this._ReportingService.AddedData.ElementAt(2) as ReportingRemoveData;

            Assert.IsFalse(data1.IsPhantomRemoval);
            Assert.AreEqual(Time3, data1.Time);
            Assert.IsFalse(data1.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data1.OriginalAmount);
            Assert.IsTrue(this._Purchase2 == data1.RemovedAmount);

            Assert.IsFalse(data2.IsPhantomRemoval);
            Assert.AreEqual(Time3, data2.Time);
            Assert.IsFalse(data2.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data2.OriginalAmount);
            Assert.IsTrue(this._Purchase1 == data2.RemovedAmount);

            Assert.IsTrue(data3.IsPhantomRemoval);
            Assert.AreEqual(Time3, data3.Time);
            Assert.IsFalse(data3.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data3.OriginalAmount);
            Assert.IsTrue(new CurrencyAmount(this._IdProviderService, "XRP", 20, additionalBasisCost * (20.0m / 80), Time3, Exchange) == data3.RemovedAmount);
        }

        [TestMethod]
        public void Disposal_AddsCorrectReportingData_FullOverdraw()
        {
            CurrencyAmount disposal = new CurrencyAmount(this._IdProviderService, "XRP", 80, 200, Time3, Exchange);
            decimal additionalBasisCost = 0.1m; // fees

            this._Inventory.Disposal(disposal, additionalBasisCost, out decimal overdrawnAmount);

            Assert.AreEqual(disposal.Amount, overdrawnAmount);
            Assert.AreEqual(1, this._ReportingService.AddedData.Count());
            ReportingRemoveData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingRemoveData;

            Assert.IsTrue(data1.IsPhantomRemoval);
            Assert.AreEqual(Time3, data1.Time);
            Assert.IsFalse(data1.CapitalGainsDelta.Value.IsZero());
            Assert.IsTrue(disposal == data1.OriginalAmount);
            Assert.IsTrue(disposal.WithValue(additionalBasisCost) == data1.RemovedAmount);
        }

        [TestMethod]
        public void TransferIn_AddsCorrectReportingData()
        {
            CurrencyAmount purchase = new CurrencyAmount(this._IdProviderService, "XRP", 100, 150, Time1, Exchange);

            this._Inventory.TransferIn(purchase, Time1);
            Assert.AreEqual(1, this._ReportingService.AddedData.Count());
            ReportingAddData data = this._ReportingService.AddedData.First() as ReportingAddData;
            Assert.IsNotNull(data);
            Assert.AreEqual(Time1, data.Time);
            Assert.IsTrue(purchase == data.CurrencyAmount);
            Assert.AreEqual(this._Inventory, data.Inventory);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Burning_NoOverdraw()
        {
            this.correctReportingDataAddedForTransferOutNoOverdraw(TransferOutReason.Burn);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Burning_PartialOverdraw()
        {
            this.correctReportingDataAddedForTransferOutPartialOverdraw(TransferOutReason.Burn);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Burning_FullOverdraw()
        {
            this.correctReportingDataAddedForTransferOutFullOverdraw(TransferOutReason.Burn);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Transferring_NoOverdraw()
        {
            this.correctReportingDataAddedForTransferOutNoOverdraw(TransferOutReason.Transfer);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Transferring_PartialOverdraw()
        {
            this.correctReportingDataAddedForTransferOutPartialOverdraw(TransferOutReason.Transfer);
        }

        [TestMethod]
        public void TransferOut_AddsCorrectReportingData_Transferring_FullOverdraw()
        {
            this.correctReportingDataAddedForTransferOutFullOverdraw(TransferOutReason.Transfer);
        }

        // at the moment, the underlying logic of transferring out funds is independent of the reason, so we can simply use the same tests for both reasons.
        private void correctReportingDataAddedForTransferOutNoOverdraw(TransferOutReason reason)
        {
            bool isTransfer = reason == TransferOutReason.Transfer;
            CurrencyAmount transfer = new CurrencyAmount(this._IdProviderService, "XRP", 50, 200, Time3, Exchange);

            this._Inventory.Purchase(this._Purchase1);
            this._Inventory.Purchase(this._Purchase2);
            this._ReportingService.ClearData();
            this._Inventory.TransferOut(transfer, reason, out decimal overdrawnAmount);

            Assert.AreEqual(0, overdrawnAmount);
            Assert.AreEqual(3, this._ReportingService.AddedData.Count());
            ReportingSplitData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingSplitData;
            ReportingRemoveData data2 = this._ReportingService.AddedData.ElementAt(1) as ReportingRemoveData;
            ReportingRemoveData data3 = this._ReportingService.AddedData.ElementAt(2) as ReportingRemoveData;

            Assert.AreEqual(Time3, data1.Time);
            Assert.IsTrue(this._Purchase1 == data1.From);
            CurrencyAmount caInInventory = this._Inventory.GetCurrentInventory().Values.First().First();
            Assert.IsTrue(data3.RemovedAmount == data1.To1 && caInInventory == data1.To2 || caInInventory == data1.To1 && data3.RemovedAmount == data1.To2);

            Assert.IsFalse(data2.IsPhantomRemoval);
            Assert.AreEqual(Time3, data2.Time);
            Assert.IsNull(data2.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data2.IsTransfer);
            Assert.IsTrue(transfer == data2.OriginalAmount);
            Assert.IsTrue(this._Purchase2 == data2.RemovedAmount);

            Assert.IsFalse(data3.IsPhantomRemoval);
            Assert.AreEqual(Time3, data3.Time);
            Assert.IsNull(data3.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data3.IsTransfer);
            Assert.IsTrue(transfer == data3.OriginalAmount);
            decimal actualValue = data3.RemovedAmount.Value;
            Assert.IsTrue(Math.Abs((double)(actualValue - 40)) < 1e-12); // yucky rounding error
            Assert.IsTrue(this._Purchase1.WithAmount(20, updateTime: Time3, scaleValue: true).WithValue(actualValue) == data3.RemovedAmount);
        }

        private void correctReportingDataAddedForTransferOutPartialOverdraw(TransferOutReason reason)
        {
            bool isTransfer = reason == TransferOutReason.Transfer;
            CurrencyAmount transfer = new CurrencyAmount(this._IdProviderService, "XRP", 80, 200, Time3, Exchange);

            this._Inventory.Purchase(this._Purchase1);
            this._Inventory.Purchase(this._Purchase2);
            this._ReportingService.ClearData();
            this._Inventory.TransferOut(transfer, reason, out decimal overdrawnAmount);

            Assert.AreEqual(20, overdrawnAmount);
            Assert.AreEqual(3, this._ReportingService.AddedData.Count());
            ReportingRemoveData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingRemoveData;
            ReportingRemoveData data2 = this._ReportingService.AddedData.ElementAt(1) as ReportingRemoveData;
            ReportingRemoveData data3 = this._ReportingService.AddedData.ElementAt(2) as ReportingRemoveData;

            Assert.IsFalse(data1.IsPhantomRemoval);
            Assert.AreEqual(Time3, data1.Time);
            Assert.IsNull(data1.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data1.IsTransfer);
            Assert.IsTrue(transfer == data1.OriginalAmount);
            Assert.IsTrue(this._Purchase2 == data1.RemovedAmount);

            Assert.IsFalse(data2.IsPhantomRemoval);
            Assert.AreEqual(Time3, data2.Time);
            Assert.IsNull(data2.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data2.IsTransfer);
            Assert.IsTrue(transfer == data2.OriginalAmount);
            Assert.IsTrue(this._Purchase1 == data2.RemovedAmount);

            Assert.IsTrue(data3.IsPhantomRemoval);
            Assert.AreEqual(Time3, data3.Time);
            Assert.IsNull(data3.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data3.IsTransfer);
            Assert.AreEqual(transfer, data3.OriginalAmount);
            Assert.AreEqual(new CurrencyAmount(this._IdProviderService, "XRP", 20, 0, Time3, null), data3.RemovedAmount);
        }

        private void correctReportingDataAddedForTransferOutFullOverdraw(TransferOutReason reason)
        {
            bool isTransfer = reason == TransferOutReason.Transfer;
            CurrencyAmount transfer = new CurrencyAmount(this._IdProviderService, "XRP", 80, 200, Time3, Exchange);

            this._Inventory.TransferOut(transfer, reason, out decimal overdrawnAmount);

            Assert.AreEqual(transfer.Amount, overdrawnAmount);
            Assert.AreEqual(1, this._ReportingService.AddedData.Count());
            ReportingRemoveData data1 = this._ReportingService.AddedData.ElementAt(0) as ReportingRemoveData;

            Assert.IsTrue(data1.IsPhantomRemoval);
            Assert.AreEqual(Time3, data1.Time);
            Assert.IsNull(data1.CapitalGainsDelta);
            Assert.AreEqual(isTransfer, data1.IsTransfer);
            Assert.AreEqual(transfer, data1.OriginalAmount);
            Assert.AreEqual(transfer.AsPhantom(), data1.RemovedAmount);
        }
        #endregion
    }
}
