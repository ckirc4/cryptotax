﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tag = CryptoTax.Tests.Inventory.TestDataSetTag;
using UtilityLibrary.Types;
using CryptoTax.Domain;
using CryptoTax.Shared.Enums;
using UtilityLibrary.Collections;

namespace CryptoTax.Tests.Inventory
{
    public static class TestData
    {
        public static IEnumerable<object[]> GetTestData()
        {
            foreach (Tag tag in Enum.GetValues(typeof(Tag)))
            {
                yield return new object[] { tag, GetTestDataSet(tag).Select((gen, i) => gen(i + 1)) };
            }
        }

        /// <summary>
        /// Defines the data sets for each tag. These data sets are used solely to test individual IInventory implementations.
        /// Generally, data sets should not be modified as it almost certainly breaks any tests which rely on them.
        /// Instead, create a new tag, define the events here, and let the {type}InventoryTests define the expected result.
        /// <br/>
        /// The events in the returned collection must be instatiated with a (modified) time index in order to be usable.
        /// </summary>
        private static IEnumerable<EventGenerator> GetTestDataSet(Tag tag)
        {
            // using this approach, we can chain data sets together easily.
            // some general guidelines:
            // - currencies are named "A", "B", "C", ...
            // - amounts must be integers so that they can be encoded in the tag name
            // - values must be integers so that they can be encoded in the tag name
            // - the tag name should be structured as <currencyName><amount><b/s (buy/sell)><value>
            // - if multiple inventory events occur, they should be included in order in the tag name, separated by _
            // - at the end, add __<id>

            OrderSide b = OrderSide.Buy;
            OrderSide s = OrderSide.Sell;
            DString A = "A";
            DString B = "B";

            List<EventGenerator> list = new List<EventGenerator>();

            switch (tag)
            {
                case Tag.Empty__0a:
                    return list;

                case Tag.A1b0__1a:
                    return list.Append(Ev(A, 1, b, 0));
                case Tag.A1b1__1b:
                    return list.Append(Ev(A, 1, b, 1));
                case Tag.A1b1_A1b1__1c:
                    return list.AppendMany(
                       Ev(A, 1, b, 1),
                       Ev(A, 1, b, 1));
                case Tag.A1b1_B1b1__1d:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(B, 1, b, 1));

                case Tag.A1b1_A1s1__2a:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 1, s, 1));
                case Tag.A1b1_A1s2__2b:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 1, s, 2));
                case Tag.A1b1_A1s0__2c:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 1, s, 0));
                case Tag.A1b2_A1s1__2d:
                    return list.AppendMany(
                        Ev(A, 1, b, 2),
                        Ev(A, 1, s, 1));
                case Tag.A2b2_A1s1__2e:
                    return list.AppendMany(
                        Ev(A, 2, b, 2),
                        Ev(A, 1, s, 1));
                case Tag.A1b1_A1b2_A1s3__2f:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 1, b, 2),
                        Ev(A, 1, s, 3));
                case Tag.A1b1_A1b2_A1s3_A1s4__2g:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 1, b, 2),
                        Ev(A, 1, s, 3),
                        Ev(A, 1, s, 4));
                case Tag.A2b1_A1s3_B1b2_A2b2_A1s4__2h:
                    return list.AppendMany(
                        Ev(A, 2, b, 1),
                        Ev(A, 1, s, 3),
                        Ev(B, 1, b, 2),
                        Ev(A, 2, b, 2),
                        Ev(A, 1, s, 4));
                case Tag.A2b1_A2b2_A3s3__2i:
                    return list.AppendMany(
                        Ev(A, 2, b, 1),
                        Ev(A, 2, b, 2),
                        Ev(A, 3, s, 3));
                case Tag.A3b1_A2s2__2j:
                    return list.AppendMany(
                        Ev(A, 3, b, 1),
                        Ev(A, 2, s, 2));
                case Tag.A6b3_A3b3_A1s2_A1s2__2k:
                    return list.AppendMany(
                        Ev(A, 6, b, 3),
                        Ev(A, 3, b, 3),
                        Ev(A, 1, s, 2),
                        Ev(A, 1, s, 2));
                case Tag.A2b2_A2b1_A2b3_A2b2_A1s1__2l:
                    return list.AppendMany(
                        Ev(A, 2, b, 2),
                        Ev(A, 2, b, 1),
                        Ev(A, 2, b, 3),
                        Ev(A, 2, b, 2),
                        Ev(A, 1, s, 1));

                case Tag.A1s0__3a:
                    return list.AppendMany(
                        Ev(A, 1, s, 0));
                case Tag.A1s1__3b:
                    return list.AppendMany(
                        Ev(A, 1, s, 1));
                case Tag.A1s1_A1s2__3c:
                    return list.AppendMany(
                        Ev(A, 1, s, 1),
                        Ev(A, 1, s, 2));
                case Tag.A1b2_A2s1__3d:
                    return list.AppendMany(
                        Ev(A, 1, b, 2),
                        Ev(A, 2, s, 1)
                        );
                case Tag.A1b2_A2s0__3e:
                    return list.AppendMany(
                        Ev(A, 1, b, 2),
                        Ev(A, 2, s, 0));
                case Tag.A1b2_A2s3__3f:
                    return list.AppendMany(
                        Ev(A, 1, b, 2),
                        Ev(A, 2, s, 3));
                case Tag.A1b1_A2s1__3g:
                    return list.AppendMany(
                        Ev(A, 1, b, 1),
                        Ev(A, 2, s, 1));

                case Tag.A1b1t2_A1b2t1_A1s3t3__4a:
                    return list.AppendMany(
                        Ev(A, 1, b, 1, 2),
                        Ev(A, 1, b, 2, 1),
                        Ev(A, 1, s, 3, 3));
                case Tag.A1b2t2_A1b3t3_A1b1t1_A1s3t4__4b:
                    return list.AppendMany(
                        Ev(A, 1, b, 2, 2),
                        Ev(A, 1, b, 3, 3),
                        Ev(A, 1, b, 1, 1),
                        Ev(A, 1, s, 3, 4));
                case Tag.A1b2t2_A1b7t3_A1b1t1_A1s4t4_A1b6t6_A1b0t5_A1s10t7__4c:
                    return list.AppendMany(
                        Ev(A, 1, b, 2, 2),
                        Ev(A, 1, b, 7, 3),
                        Ev(A, 1, b, 1, 1),
                        Ev(A, 1, s, 4, 4),
                        Ev(A, 1, b, 6, 6),
                        Ev(A, 1, b, 0, 5),
                        Ev(A, 1, s, 10, 7));

                default:
                    throw new NotImplementedException($"Dataset for tag {tag} does not exist. Unable to carry out tests until data has been provided.");
            }
        }

        /// <summary>
        /// Create an event whose time modified can be set later. Note that the timeModified is equal to the original creation time if the CA is never updated.
        /// </summary>
        private static EventGenerator Ev(DString currency, int amount, OrderSide side, int value)
        {
            return (int timeModified) => EvWithTime(currency, amount, side, value, timeModified);
        }

        /// <summary>
        /// Create an event with a custom time modified.
        /// </summary>
        private static EventGenerator Ev(DString currency, int amount, OrderSide side, int value, int timeModified)
        {
            return (int _) => EvWithTime(currency, amount, side, value, timeModified);
        }

        private static InventoryEvent EvWithTime(DString currency, int amount, OrderSide side, int value, int timeModified)
        {
            // we require that each event CA has a unique id
            return new InventoryEvent(new CurrencyAmount(InventoryDataTests_.IdProvider, currency, amount, value, InventoryDataTests_.getTime(timeModified)), side == OrderSide.Buy);
        }

        private delegate InventoryEvent EventGenerator(int timeModified);
    }
}
