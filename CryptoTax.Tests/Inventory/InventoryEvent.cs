﻿using CryptoTax.Domain;

namespace CryptoTax.Tests.Inventory
{
    public struct InventoryEvent
    {
        public CurrencyAmount PurchaseOrDisposal { get; }
        public bool IsPurchase { get; }

        public InventoryEvent(CurrencyAmount purchaseOrDisposal, bool isPurchase)
        {
            this.PurchaseOrDisposal = purchaseOrDisposal;
            this.IsPurchase = isPurchase;
        }
    }
}
