﻿using CryptoTax.Domain;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using UtilityLibrary.Types;
using Tag = CryptoTax.Tests.Inventory.TestDataSetTag;

namespace CryptoTax.Tests.Inventory
{
    [TestClass]
    public class FIFOInventoryDataTests : InventoryDataTests<Queue<CurrencyAmount>>
    {
        private IInventory _Inventory;
        private FIFOInventoryAdapter _Adapter;

        public override IInventory Inventory { get { return this._Inventory; } }
        public override IInventoryAdapter<Queue<CurrencyAmount>> Adapter { get { return this._Adapter; } }
        public override bool IsOrderedInventory { get { return true; } }

        [TestInitialize]
        public void Setup()
        {
            this._IdProviderService = new IdProviderService(0);
            this._ReportingService = new MockTrackingReportingService();
            this._LoggingService = new MockLoggingService();
            this._Inventory = new FIFOInventoryFactory().Create(this._IdProviderService, this._ReportingService, this._LoggingService, Exchange, "FIFO Test Inventory");
            this._Adapter = new FIFOInventoryAdapter(this._IdProviderService, this._ReportingService);
        }

        public override Dictionary<DString, IEnumerable<CurrencyAmount>> GetExpectedInventory(TestDataSetTag tag)
        {
            switch (tag)
            {
                case Tag.Empty__0a:
                    return Empty;

                case Tag.A1b0__1a:
                    return Dict(A, CA(A, 1, 0, 1));
                case Tag.A1b1__1b:
                    return Dict(A, CA(A, 1, 1, 1));
                case Tag.A1b1_A1b1__1c:
                    return Dict(
                        A, CA(A, 1, 1, 1),
                        CA(A, 1, 1, 2));
                case Tag.A1b1_B1b1__1d:
                    return Dict(
                        A, CA(A, 1, 1, 1),
                        B, CA(B, 1, 1, 2));

                case Tag.A1b1_A1s1__2a:
                case Tag.A1b1_A1s2__2b:
                case Tag.A1b1_A1s0__2c:
                case Tag.A1b2_A1s1__2d:
                    return Empty;
                case Tag.A2b2_A1s1__2e:
                    return Dict(A, CA(A, 1, 1, 2));
                case Tag.A1b1_A1b2_A1s3__2f:
                    return Dict(A, CA(A, 1, 2, 2));
                case Tag.A1b1_A1b2_A1s3_A1s4__2g:
                    return Empty;
                case Tag.A2b1_A1s3_B1b2_A2b2_A1s4__2h:
                    return Dict(B, CA(B, 1, 2, 3),
                        A, CA(A, 2, 2, 4));
                case Tag.A2b1_A2b2_A3s3__2i:
                    return Dict(A, CA(A, 1, 1, 3));
                case Tag.A3b1_A2s2__2j:
                    return Dict(A, CA(A, 1, 1/3.0m, 2));
                case Tag.A6b3_A3b3_A1s2_A1s2__2k:
                    return Dict(A, CA(A, 4, 2, 4), CA(A, 3, 3, 2));
                case Tag.A2b2_A2b1_A2b3_A2b2_A1s1__2l:
                    return Dict(A, CA(A, 1, 1, 5), CA(A, 2, 1, 2), CA(A, 2, 3, 3), CA(A, 2, 2, 4));

                case Tag.A1s0__3a:
                case Tag.A1s1__3b:
                case Tag.A1s1_A1s2__3c:
                case Tag.A1b2_A2s1__3d:
                case Tag.A1b2_A2s0__3e:
                case Tag.A1b2_A2s3__3f:
                case Tag.A1b1_A2s1__3g:
                    return Empty;

                case Tag.A1b1t2_A1b2t1_A1s3t3__4a:
                    return Dict(A, CA(A, 1, 2, 1));
                case Tag.A1b2t2_A1b3t3_A1b1t1_A1s3t4__4b:
                    return Dict(A, CA(A, 1, 3, 3), CA(A, 1, 1, 1));
                case Tag.A1b2t2_A1b7t3_A1b1t1_A1s4t4_A1b6t6_A1b0t5_A1s10t7__4c:
                    return Dict(A, CA(A, 1, 1, 1), CA(A, 1, 6, 6), CA(A, 1, 0, 5));

                default:
                    throw new AssertUnreachable(tag);
            }
        }

        public override decimal? GetExpectedCapitalGains(TestDataSetTag tag)
        {
            switch (tag)
            {
                case Tag.Empty__0a:
                    return 0;

                case Tag.A1b0__1a:
                case Tag.A1b1__1b:
                case Tag.A1b1_A1b1__1c:
                case Tag.A1b1_B1b1__1d:
                    return 0;

                case Tag.A1b1_A1s1__2a:
                    return 0;
                case Tag.A1b1_A1s2__2b:
                    return 1;
                case Tag.A1b1_A1s0__2c:
                    return -1;
                case Tag.A1b2_A1s1__2d:
                    return -1;
                case Tag.A2b2_A1s1__2e:
                    return 0;
                case Tag.A1b1_A1b2_A1s3__2f:
                    return 2;
                case Tag.A1b1_A1b2_A1s3_A1s4__2g:
                    return 2 + 2;
                case Tag.A2b1_A1s3_B1b2_A2b2_A1s4__2h:
                    return 2.5m + 3.5m;
                case Tag.A2b1_A2b2_A3s3__2i:
                    return 0.5m + 0.5m + 0;
                case Tag.A3b1_A2s2__2j:
                    return 1 - 1/3.0m + 1 - 1/3.0m;
                case Tag.A6b3_A3b3_A1s2_A1s2__2k:
                    return 1.5m + 1.5m;
                case Tag.A2b2_A2b1_A2b3_A2b2_A1s1__2l:
                    return 0;

                case Tag.A1s0__3a:
                    return 0;
                case Tag.A1s1__3b:
                    return 1;
                case Tag.A1s1_A1s2__3c:
                    return 1 + 2;
                case Tag.A1b2_A2s1__3d:
                    return -1.5m + 0.5m;
                case Tag.A1b2_A2s0__3e:
                    return -2;
                case Tag.A1b2_A2s3__3f:
                    return -0.5m + 1.5m;
                case Tag.A1b1_A2s1__3g:
                    return -0.5m + 0.5m;

                case Tag.A1b1t2_A1b2t1_A1s3t3__4a:
                    return 2;
                case Tag.A1b2t2_A1b3t3_A1b1t1_A1s3t4__4b:
                    return 1;
                case Tag.A1b2t2_A1b7t3_A1b1t1_A1s4t4_A1b6t6_A1b0t5_A1s10t7__4c:
                    return 2 + 3;

                default:
                    throw new AssertUnreachable(tag);
            }
        }
    }
}
