﻿using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Domain;
using CryptoTax.Domain.Inventory;
using CryptoTax.Domain.Services;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Tests.Mocks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using UtilityLibrary.Collections;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.Inventory
{
    /// <summary>
    /// Implement this class to test an inventory's data management. Test from the provided datasets (may add new) by returning an expected outcome to the GetExpected{property tested} method for a given data tag. If this method returns null for a tag, tests for the tag will be skipped.
    /// </summary>
    [TestClass]
    public abstract class InventoryDataTests<TCollection>
        where TCollection : class
    {
        private const decimal _TOL = 1e-12m;

        protected IdProviderService _IdProviderService;
        protected MockTrackingReportingService _ReportingService;
        protected MockLoggingService _LoggingService;

        public static Func<int, DateTime> getTime = InventoryDataTests_.getTime;
        public static Exchange Exchange = InventoryDataTests_.Exchange;

        /// <summary>
        /// Instantiated inventory
        /// </summary>
        public abstract IInventory Inventory { get; }
        public abstract IInventoryAdapter<TCollection> Adapter { get; }
        /// <summary>
        /// Whether the order of the contents of this inventory matters. This is required for the correct comparison of an expected inventory item collection against an actual inventory item collection.
        /// </summary>
        public abstract bool IsOrderedInventory { get; }
        public abstract Dictionary<DString, IEnumerable<CurrencyAmount>> GetExpectedInventory(TestDataSetTag tag);
        public abstract decimal? GetExpectedCapitalGains(TestDataSetTag tag);

        /// <summary>
        /// Tests whether the inventory correctly tracks purchases and disposals.
        /// </summary>
        [DataTestMethod()]
        [DynamicData(nameof(getTestData), DynamicDataSourceType.Method)]
        public void TestInventoryTracksContentsCorrectly(TestDataSetTag tag, IEnumerable<InventoryEvent> events)
        {
            // check that we have the results to compare eagainst - if not, skip the test
            Dictionary<DString, IEnumerable<CurrencyAmount>> expectedInventory = this.GetExpectedInventory(tag);
            if (expectedInventory == null) return;
            DString id = $"{this.Inventory.Type}{tag}";

            // carry out the sequence of events
            foreach (InventoryEvent e in events)
            {
                if (e.IsPurchase) this.Inventory.Purchase(e.PurchaseOrDisposal);
                else this.Inventory.Disposal(e.PurchaseOrDisposal, 0, out _);
            }
            Dictionary<DString, IEnumerable<CurrencyAmount>> actualInventory = this.Inventory.GetCurrentInventory();

            // check whether the inventory for each currency is correct
            int expectedCurrencyCount = expectedInventory.Where(kvp => kvp.Value.Count() > 0).Count();
            int actualCurrencyCount = actualInventory.Where(kvp => kvp.Value.Count() > 0).Count();
            Assert.AreEqual(expectedCurrencyCount, actualCurrencyCount, $"[{id}] Expected {expectedCurrencyCount} currencies in the inventory, but found {actualCurrencyCount}");

            foreach (KeyValuePair<DString, IEnumerable<CurrencyAmount>> kvp in actualInventory)
            {
                if (kvp.Value.Count() == 0) continue; // currency still exists in dictionary, but is empty

                if (!expectedInventory.ContainsKey(kvp.Key)) Assert.Fail($"[{id}] Unexpected currency found in the current inventory: {kvp.Key}");

                List<CurrencyAmount> expected = new List<CurrencyAmount>(expectedInventory[kvp.Key]);
                List<CurrencyAmount> actual = new List<CurrencyAmount>(kvp.Value);

                Assert.AreEqual(expected.Count, actual.Count, $"[{id}] Expected {expected.Count} inventory entrances for currency {kvp.Key}, but found {actual.Count}.");
                bool areEqual = expected.CompareSequences(actual, (x, y) => CurrencyAmount.EqualWithinTol(x, y, _TOL), this.IsOrderedInventory);
                Assert.AreEqual(true, areEqual, $"[{id}] Inventory for currency {kvp.Key} is unexpected.");
            }
        }

        /// <summary>
        /// Tests whether the inventory correctly tracks purchases and disposals.
        /// </summary>
        [DataTestMethod()]
        [DynamicData(nameof(getTestData), DynamicDataSourceType.Method)]
        public void TestInventoryCalculatesCapitalGainsCorrectly(TestDataSetTag tag, IEnumerable<InventoryEvent> events)
        {
            // check that we have the results to compare eagainst - if not, skip the test
            decimal? expectedCapitalGains = this.GetExpectedCapitalGains(tag);
            if (expectedCapitalGains == null) return;
            DString id = $"{this.Inventory.Type}{tag}";

            // carry out the sequence of events
            foreach (InventoryEvent e in events)
            {
                if (e.IsPurchase) this.Inventory.Purchase(e.PurchaseOrDisposal);
                else this.Inventory.Disposal(e.PurchaseOrDisposal, 0, out _);
            }
            decimal actualCapitalGains = this.Inventory.NetCapitalGains.GetTotalGainsOrLosses();

            // check whether the result is correct
            Assert.IsTrue(Math.Abs((decimal)expectedCapitalGains - actualCapitalGains) <= _TOL, $"[{id}] Expected {expectedCapitalGains} capital gains from the inventory, but got {actualCapitalGains}");
        }

        /// <summary>
        /// Tests whether the inventory correctly tracks purchases and disposals with nothing being created/destroyed.
        /// </summary>
        [DataTestMethod()]
        [DataRow("", 0, 0)]
        [DataRow("1", 1, 0)]
        [DataRow("1,1", 2, 0)]
        [DataRow("1,-1", 0, 0)]
        [DataRow("1,-2", 0, 1)]
        [DataRow("1,1,-2", 0, 0)]
        [DataRow("1,1,-3,1", 1, 1)]
        [DataRow("-2,2,-3", 0, 3)]
        [DataRow("-2,2,-3,1", 1, 3)]
        public void TestInventoryDataIntegrity(string deltasString, double expectedAmountLeft, double expectedOverdrawnAmount)
        {
            decimal totalOverdrawn = 0;
            TCollection collection = this.Adapter.Instantiate();

            IEnumerable<decimal> deltas = string.IsNullOrEmpty(deltasString) ? new List<decimal>() : deltasString.Split(',').Select(s => decimal.Parse(s));
            int i = 0;
            foreach (decimal delta in deltas)
            {
                i++;
                decimal thisOverdrawn = 0;
                if (delta == 0) continue;
                else if (delta > 0)
                {
                    CurrencyAmount toAdd = CA(A, delta, delta, i);
                    this.Adapter.Add(collection, toAdd, toAdd.TimeModified);
                }
                else
                {
                    decimal amountToRemove = -delta;
                    IEnumerable<CurrencyAmount> removed = this.Adapter.RemoveMany(collection, amountToRemove, getTime(i), out thisOverdrawn);
                    decimal actualRemoved = removed.Sum(c => c.Amount);
                    Assert.AreEqual(amountToRemove, actualRemoved + thisOverdrawn);
                }

                totalOverdrawn += thisOverdrawn;
            }

            Assert.AreEqual((decimal)expectedAmountLeft, this.Adapter.TotalAmount(collection));
            Assert.AreEqual((decimal)expectedOverdrawnAmount, totalOverdrawn);
        }

        [TestMethod]
        public void TestAdapterDtoConversions()
        {
            // given some inventory state, converting to DTO and then loading a new inventory from that DTO should result in the exact same state.
            // this was prompted by the realisation that there was a huge bug in FILO inventory where loading a DTO would reverse the order of the Stack!
            TCollection collection = this.Adapter.Instantiate();
            List<CurrencyAmount> cas = new List<CurrencyAmount>() { CA(A, 1, 2, 1), CA(A, 3, 4, 2), CA(A, 5, 6, 3), CA(A, 7, 8, 4), CA(A, 9, 10, 5) };
            foreach (CurrencyAmount ca in cas)
            {
                this.Adapter.Add(collection, ca, ca.TimeModified);
            }

            IEnumerable<InventoryItemDTO> dto = this.Adapter.ToDTO(collection);
            TCollection loadedCollection = this.Adapter.FromDTO(dto);

            IEnumerable<CurrencyAmount> originalContents = this.Adapter.GetContents(collection);
            IEnumerable<CurrencyAmount> loadedContents = this.Adapter.GetContents(loadedCollection);
            bool areEqual = originalContents.CompareSequences(loadedContents, (x, y) => CurrencyAmount.EqualWithinTol(x, y, 0), true);
            Assert.AreEqual(true, areEqual);
        }

        #region Static Quickies
        /// <summary>
        /// Abbreviated currency "A".
        /// </summary>
        protected static DString A = "A";
        /// <summary>
        /// Abbreviated currency "B".
        /// </summary>
        protected static DString B = "B";
        /// <summary>
        /// Empty inventory.
        /// </summary>
        protected static Dictionary<DString, IEnumerable<CurrencyAmount>> Empty = new Dictionary<DString, IEnumerable<CurrencyAmount>>();

        /// <summary>
        /// Quick constructor for creating a CurrencyAmount object with arbitrary Id.
        /// </summary>
        protected static CurrencyAmount CA(DString currency, decimal amount, decimal value, int timeModified) => new CurrencyAmount(InventoryDataTests_.IdProvider, currency, amount, value, getTime(timeModified));

        /// <summary>
        /// Quick constructor for creating an inventory items dict.<br/>
        /// Values are appended to the collection for the key most recently mentioned in the parameter list.<br/>
        /// Example:
        /// <code>Dict(firstKey, firstValue1, secondKey, secondValue1, secondValue2, firstKey, firstValue2)</code>
        /// </summary>
        protected static Dictionary<DString, IEnumerable<CurrencyAmount>> Dict(DString key, CurrencyAmount value, params object[] additional) => Helpers.QuickDict(key, value, additional);
        #endregion

        private static IEnumerable<object[]> getTestData() { return TestData.GetTestData(); }
    }

    // hack so we can use static field members that are separate to the generic implementation of the class.
    public static class InventoryDataTests_
    {
        private static DateTime Now = DateTime.Now;
        public static Func<int, DateTime> getTime = (t) => Now.AddMinutes(t);
        public static Exchange Exchange = Exchange.BTCMarkets;
        public static IdProviderService IdProvider = new IdProviderService(0);
    }
}
