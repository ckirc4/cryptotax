﻿namespace CryptoTax.Tests.Inventory
{
    /// <summary>
    /// &lt;currencyName&gt;&lt;amount&gt;&lt;b/s (buy/sell)&gt;&lt;price&gt;
    /// <br/><br/>
    /// Naming convention<br/>
    /// - Currencies are named A, B, ...<br/>
    /// - Amounts and values are integers<br/>
    /// - Events are ordered, and separated by _<br/>
    /// - Each event occurs one time unit after the previous, starting at time 1
    /// <br/><br/>
    /// Categorisation<br/>
    /// 0: Empty data sets [a]<br/>
    /// 1: Receiving data sets [a-d]<br/>
    /// 2: Normal buy/sell data sets [a-j]<br/>
    /// 3: Overdrawing data sets [a-g]<br/>
    /// 4: Timed data sets (to simulate transfer between inventories) [a-c]
    /// </summary>
    public enum TestDataSetTag
    {
        Empty__0a,

        // receiving events
        A1b0__1a,
        A1b1__1b,
        A1b1_A1b1__1c,
        A1b1_B1b1__1d,

        // normal buy/sell events
        A1b1_A1s1__2a,
        A1b1_A1s2__2b,
        A1b1_A1s0__2c,
        A1b2_A1s1__2d,
        A2b2_A1s1__2e,
        A1b1_A1b2_A1s3__2f,
        A1b1_A1b2_A1s3_A1s4__2g,
        /// <summary>
        /// purchase 2 A @ price of 0.5 <br/>
        /// sell 1 A @ price of 3 <br/>
        /// purchase another 2 A @ price of 1 <br/>
        /// sell 1 A @ price of 4
        /// </summary>
        A2b1_A1s3_B1b2_A2b2_A1s4__2h,
        /// <summary>
        /// Purchase 2 A @ price of 0.5 <br/>
        /// Purchase 2 A @ price of 1 <br/>
        /// Sell 3 A @ Price of 1
        /// </summary>
        A2b1_A2b2_A3s3__2i,
        A3b1_A2s2__2j,
        A6b3_A3b3_A1s2_A1s2__2k,
        A2b2_A2b1_A2b3_A2b2_A1s1__2l,

        // overdrawing events
        A1s0__3a,
        A1s1__3b,
        A1s1_A1s2__3c,
        A1b2_A2s1__3d,
        A1b2_A2s0__3e,
        A1b2_A2s3__3f,
        A1b1_A2s1__3g,

        // timed events
        A1b1t2_A1b2t1_A1s3t3__4a,
        A1b2t2_A1b3t3_A1b1t1_A1s3t4__4b,
        A1b2t2_A1b7t3_A1b1t1_A1s4t4_A1b6t6_A1b0t5_A1s10t7__4c,
    }
}
