﻿The unit testing environment for inventories implementing `IInventory` has been designed 
with maintainability and extending in mind. The actual unit tests ("act") are completed in 
the abstract `InventoryDataTests` and are highly generalised. The specific inventory data test implementations 
are the source of truth used for passing tests ("assert"), while data sets ("arrange") are managed 
separately in a shared space.

A data set is a predefined sequence of certain events, where an event is a purchase or 
disposal (for tax purposes). Data sets are defined statically in `TestData.GetTestData()`. 
Note that each data set is associated with a unique `TestDataSetTag` enum, which succinctly 
describes the event sequence for quick reference. A data set **must** be defined for each 
tag, otherwise an exception will be thrown during the test.

To test an inventory class, a new unit test class must be created under `CryptoTax.Tests.Inventory` 
and it must implement the abstract `InventoryDataTests` class. As part of this, it 
will need to override a few items, notably the `GetExpected*` methods (where the `*` is 
replaced with the property that is being tested). Given a `TestDataSetTag`, the method 
should return the *true* value (hardcoded - this may require some working out) that unit 
tests can compare their results against (floating point error is ignored). The method is allowed to return null 
if it chooses not to test against a certain data set.
