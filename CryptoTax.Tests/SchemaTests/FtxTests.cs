﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.FTX;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class FtxTests
    {
        private static readonly Exchange EXCHANGE = Exchange.FTX;
        private static readonly Container CONTAINER = Container.FromExchange(EXCHANGE);

        [TestMethod]
        public void FuturesTradesAreIgnored()
        {
            FtxRecord record = this.getRecord("123523651,9/07/2020 7:41:59 PM,Trade,USD,0.00104739,BAL-PERP,0.01,USD,0.0000668325,,,Futures,9.5475,Sell");

            Assert.IsNull(record.ToTradeDTO());
            Assert.IsNull(record.ToTransactionDTO());
        }

        [TestMethod]
        public void FundingPaymentsAreIgnored()
        {
            FtxRecord record = this.getRecord("BAL-PERP-29681950,9/07/2020 8:00:00 PM,FundingPayment,,,USD,-0.79984181,,,,,,,");

            Assert.IsNull(record.ToTradeDTO());
            Assert.IsNull(record.ToTransactionDTO());
        }

        [TestMethod]
        public void NoFeesAreSetToNull()
        {
            // otc trades have zero fees, should be converted to null currency and amount.
            FtxRecord record = this.getRecord("123522391,9/07/2020 7:39:32 PM,Trade,USD,0.00243148,ETH,0.57967924,USD,0.0,,,OTC,238.405764,Sell");

            DateTime time = new DateTime(2020, 7, 9, 19, 39, 32, DateTimeKind.Utc);
            this.testTrade(record, time, "123522391", "USD", "ETH", MarketType.Spot, 238.405764m, OrderSide.Sell, "USD", 0.00243148m, "ETH", 0.57967924m);
        }

        [TestMethod]
        public void DepositWorksCorrectly()
        {
            FtxRecord record = this.getRecord("0xa3e190a92864aeb01175e202e9e7f09a922ca2b4427d9ff71d23a70d40d97b9f,9/07/2020 7:37:07 PM,Deposit,ETH,0.57967924,,,,,,,,,");

            DateTime time = new DateTime(2020, 7, 9, 19, 37, 07, DateTimeKind.Utc);
            this.testTransaction(record, time, null, "0xa3e190a92864aeb01175e202e9e7f09a922ca2b4427d9ff71d23a70d40d97b9f", null, TransactionType.CryptoDeposit, "ETH", 0.57967924m);
        }

        [TestMethod]
        public void WithdrawalWorksCorrectly()
        {
            FtxRecord record = this.getRecord("ec8e33be790abda81da31c00cef8d062a58cdca01216508c54c01362bfee157f,11/12/2019 1:04:06 AM,Withdrawal,,,BTC,0.02071784,,,3QiuSs27UhgBtLvuiiDfoM6hzSh5wpY2kv,,,,");

            DateTime time = new DateTime(2019, 12, 11, 1, 4, 6, DateTimeKind.Utc);
            this.testTransaction(record, time, null, "ec8e33be790abda81da31c00cef8d062a58cdca01216508c54c01362bfee157f", "3QiuSs27UhgBtLvuiiDfoM6hzSh5wpY2kv", TransactionType.CryptoWithdrawal, null, null, "BTC", 0.02071784m);
        }

        [TestMethod]
        public void PnlSnapshotWorksCorrectly()
        {
            FtxRecord record = this.getRecord("pnl_09/07/2020,9/07/2020 11:59:59 PM,PnlSnapshot,,,USD,1.2439736350,,,,,,,");

            DateTime time = new DateTime(2020, 7, 9, 23, 59, 59, DateTimeKind.Utc);
            this.testTransaction(record, time, "pnl_09/07/2020", null, null, TransactionType.FuturesPnL, null, null, "USD", 1.2439736350m);
        }

        [TestMethod]
        public void SpotTradeWithFeeWorksCorrectly()
        {
            FtxRecord record = this.getRecord("131168410,18/07/2020 1:10:47 AM,Trade,DOGEBEAR,1.408,USD,204.16,DOGEBEAR,0.0002816,,,Spot,145.0,Buy");

            DateTime time = new DateTime(2020, 7, 18, 1, 10, 47, DateTimeKind.Utc);
            this.testTrade(record, time, "131168410", "USD", "DOGEBEAR", MarketType.Spot, 145, OrderSide.Buy, "DOGEBEAR", 1.408m, "USD", 204.16m, "DOGEBEAR", 0.0002816m);
        }

        [TestMethod]
        public void BankDepositDetectedCorrectly()
        {
            FtxRecord record = this.getRecord("1094109,26/8/2022 8:15,Deposit,AUD,9000,,,,,,,,,");

            DateTime time = new DateTime(2022, 8, 26, 8, 15, 0, DateTimeKind.Utc);
            this.testTransaction(record, time, null, "1094109", null, TransactionType.FiatDeposit, "AUD", 9000);
        }

        private FtxRecord getRecord(DString csvLine)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { "Id,Time,Type,CurrencyReceived,AmountReceived,CurrencySent,AmountSent,FeeCurrency,FeeAmount,WithdrawalAddress,WithdrawalAddressTag,MarketType,Price,Side", csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            FtxSchema schema = new FtxSchema();

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record as FtxRecord;
        }

        private void testTrade(Record record, DateTime expectedTime, DString expectedTradeId, DString expectedQuoteCurrency, DString expectedBaseCurrency, MarketType expectedMarketType, decimal expectedPrice, OrderSide expectedSide, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            SymbolDTO expectedSymbol = new SymbolDTO(expectedQuoteCurrency, expectedBaseCurrency);
            TradeDTO expected = new TradeDTO(expectedTime, EXCHANGE, expectedTradeId, expectedSymbol, expectedMarketType, expectedSide, expectedPrice, expectedContainerEvent);

            Helpers.PerformRecordTest(record, expected, null);
        }

        private void testTransaction(Record record, DateTime expectedTime, NString expectedInternalId, NString expectedPublicId, NString expectedCounterparty, TransactionType expectedType, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            TransactionDTO expected = new TransactionDTO(expectedTime, EXCHANGE, expectedInternalId, expectedPublicId, expectedCounterparty, expectedType, expectedContainerEvent);

            Helpers.PerformRecordTest(record, null, expected);
        }
    }
}
