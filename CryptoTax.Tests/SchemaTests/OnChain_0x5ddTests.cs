﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.OnChain;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class OnChain_0x5ddTests
    {
        [TestMethod]
        public void MigrationIsIgnored()
        {
            OnChain_0xe5ddRecord record = this.getRecord("0x817a0c906d329d8b24e488d68859c0db9fb7aa010d69ddd456822e692cf8c822,1627079933,23/07/2021 22:38,0xe5dd240231608367fb2d61d5348693a7cbf5396e,0xdf82c9014f127243ce1305dfe54151647d74b27a,\"8,748,822,063,560,480,000,000\",0xdf82c9014f127243ce1305dfe54151647d74b27a,Moons,MOON");

            Helpers.PerformRecordTest(record, null, null);
        }

        [TestMethod]
        public void AirdropTest()
        {
            OnChain_0xe5ddRecord record = this.getRecord("0x1ff26a073d9813c938227420e958d0c7004769339f952f70b81527c66736a76b,1589427215,14/05/2020 3:33,0x0000000000000000000000000000000000000000,0xe5dd240231608367fb2d61d5348693a7cbf5396e,\"6,303,296,644,902,570,000,000\",0xdf82c9014f127243ce1305dfe54151647d74b27a,Moons,MOON");

            DateTime expectedTime = new DateTime(2020, 5, 14, 3, 33, 35, DateTimeKind.Utc);
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, Container.FromWallet("0xe5dd240231608367fb2d61d5348693a7cbf5396e"), null, null, "MOON", 6303.29664490257m, null, null);
            TransactionDTO expectedTransactionDTO = new TransactionDTO(expectedTime, Shared.Enums.Exchange.OnChain, null, "0x1ff26a073d9813c938227420e958d0c7004769339f952f70b81527c66736a76b", "0x0000000000000000000000000000000000000000", Shared.Enums.TransactionType.StakingOrAirdrops, expectedContainerEvent);
            Helpers.PerformRecordTest(record, null, expectedTransactionDTO);
        }

        private OnChain_0xe5ddRecord getRecord(DString csvLine)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { "Txhash,UnixTimestamp,DateTime,From,To,Value,ContractAddress,TokenName,TokenSymbol", csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            OnChain_0xe5ddSchema schema = new OnChain_0xe5ddSchema("0xe5dd240231608367fb2d61d5348693a7cbf5396e");

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record as OnChain_0xe5ddRecord;
        }
    }
}
