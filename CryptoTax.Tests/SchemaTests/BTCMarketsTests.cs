﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using CryptoTax.Persistence.Schema.Exchange.BTCMarkets;
using CryptoTax.Persistence.Services.Csv;
using Moq;
using CryptoTax.Shared.Services;
using UtilityLibrary.Types;
using CryptoTax.Persistence.Schema;
using CryptoTax.DTO;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class BTCMarketsTests
    {
        private const string _HEADER_ROW = "transactionId, creationTime, recordType, action, currency, amount, description, balance, referenceId";
        private const Exchange _EXCHANGE = Exchange.BTCMarkets;
        private static readonly Container _Container = Container.FromExchange(_EXCHANGE);

        [DataTestMethod]
        [DataRow("Processed by System: D2461E62A10D0E20D07FD818594DD53C0757E15BFAFE87366A5CA1100B63EFE5", "D2461E62A10D0E20D07FD818594DD53C0757E15BFAFE87366A5CA1100B63EFE5")]
        [DataRow("Processed by System: c3b93c206ff360d2f89ce2fdcbf806569b5ad98b5fb18bb7539e8c491437df17", "c3b93c206ff360d2f89ce2fdcbf806569b5ad98b5fb18bb7539e8c491437df17")]
        [DataRow("Processed by System: 0x4a88fe127ebd4fe99b688671def8195ec250f7e391d2bcfa49532cb67fa977ba", "0x4a88fe127ebd4fe99b688671def8195ec250f7e391d2bcfa49532cb67fa977ba")]
        [DataRow("Bitcoin deposit was successfull.Reference: 6ec39448d527895576764d9ef90459608dc5ff9f4c5dcb441a26f1e62f601206", "6ec39448d527895576764d9ef90459608dc5ff9f4c5dcb441a26f1e62f601206")]
        [DataRow("Ripple DEPOSIT was successfull.Ripple Deposit  X 7127.12880000", null)]
        public void TransactionExtractedFromDescription(string description, string expected)
        {
            string detected = DescriptionParser.TryGetTransactionHash(ActionType.Withdraw, description);
            Assert.AreEqual(expected, detected);
        }

        [DataTestMethod]
        [DataRow("Deposit for Bitcoin Cash Fork", true)]
        [DataRow("Bitcoin deposit was successfull.Reference: 6ec39448d527895576764d9ef90459608dc5ff9f4c5dcb441a26f1e62f601206", false)]
        public void IsForkFromDescriptionTests(string description, bool expected)
        {
            bool isFOrk = DescriptionParser.IsForkDeposit(description);
            Assert.AreEqual(expected, isFOrk);
        }

        [TestMethod]
        public void BuyTradeDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1105129528,2018-01-11T04:57:58Z,Trade,Buy Order,BTC,-0.00254176,Buy 0.01381396BCH @ BTC 0.18399990 Fully matched at 0.18399990,0.00519217,1105129519",
                "1105129529,2018-01-11T04:57:58Z,Trade,Buy Order,BCH,0.01381396,Buy 0.01381396BCH @ BTC 0.18399990 Trade settled,0.07054844,1105129519",
                "1105129530,2018-01-11T04:57:58Z,Trade,Trading Fee,BCH,-0.00003039,Buy 0.01381396BCH @ BTC 0.18399990 Trading fee,0.07051805,1105129519");

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 11, 4, 57, 58, DateTimeKind.Utc);
            decimal expectedPrice = 0.00254176m / 0.01381396m;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "BCH");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", 0.00254176m, "BCH", 0.01381396m, "BCH", 0.00003039m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1105129528", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void SellTradeDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1581511339,2018-04-13T16:38:08Z,Trade,Sell Order,XRP,-456.26971494,Sell 456.26971000XRP @ AUD 0.89000000 Fully matched at 0.89000000,9.65000657,1580049849",
                "1581511340,2018-04-13T16:38:08Z,Trade,Sell Order,AUD,406.08004629,Sell 456.26971000XRP @ AUD 0.89000000 Trade settled,406.0800463,1580049849",
                "1581511341,2018-04-13T16:38:08Z,Trade,Trading Fee,AUD,-1.01520011,Sell 456.26971000XRP @ AUD 0.89000000 Trading fee,405.06484619,1580049849");

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 4, 13, 16, 38, 8, DateTimeKind.Utc);
            decimal expectedPrice = 406.08004629m / 456.26971494m;
            SymbolDTO expectedSymbol = new SymbolDTO("AUD", "XRP");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "XRP", 456.26971494m, "AUD", 406.08004629m, "AUD", 1.01520011m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1581511339", expectedSymbol, MarketType.Spot, OrderSide.Sell, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void TradeWithFeesFollowedByTradeWithoutFeesDetectedCorrectly()
        {
            IEnumerable<BTCMarketsRecordGroup> groups = this.getMultipleGroups(
                "1031185209,2017-12-23T03:16:02Z,Trade,Buy Order,AUD,-218.74999693,Buy 5.00000000ETC @ AUD 43.75000000 Partially matched at 43.75000000,418.41563,1030324186",
                "1031185210,2017-12-23T03:16:02Z,Trade,Buy Order,ETC,4.99999993,Buy 4.99999990ETC @ AUD 43.75000000 Trade settled,12.99999993,1030324186",
                "1031185211,2017-12-23T03:16:02Z,Trade,Trading Fee,AUD,-0.32812499,Buy 4.99999990ETC @ AUD 43.75000000 Trading fee,418.08750501,1030324186",
                "1031185483,2017-12-23T03:16:09Z,Trade,Buy Order,AUD,-0.00000306,Buy 5.00000000ETC @ AUD 43.75000000 Fully matched at 43.75000000,418.08750195,1030324186",
                "1031185484,2017-12-23T03:16:09Z,Trade,Buy Order,ETC,0.00000007,Buy 0.00000007ETC @ AUD 43.75000000 Trade settled,13,1030324186");

            Assert.AreEqual(2, groups.Count());
            BTCMarketsRecordGroup group1 = groups.ElementAt(0);
            BTCMarketsRecordGroup group2 = groups.ElementAt(1);
            Assert.AreEqual(3, group1.Group.Count());
            Assert.AreEqual(2, group2.Group.Count());

            SymbolDTO expectedSymbol = new SymbolDTO("AUD", "ETC");
            TransactionDTO expectedTransaction = null;

            GroupType expectedType1 = GroupType.TradeWithFee;
            DateTime expectedTime1 = new DateTime(2017, 12, 23, 3, 16, 2, DateTimeKind.Utc);
            decimal expectedPrice1 = 218.74999693m / 4.99999993m;
            ContainerEventDTO expectedEvent1 = new ContainerEventDTO(expectedTime1, _Container, "AUD", 218.74999693m, "ETC", 4.99999993m, "AUD", 0.32812499m);
            TradeDTO expectedTrade1 = new TradeDTO(expectedTime1, _EXCHANGE, "1031185209", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice1, expectedEvent1);
            this.performGroupTest(group1, expectedType1, expectedTime1, expectedTrade1, expectedTransaction);

            GroupType expectedType2 = GroupType.TradeWithoutFee;
            DateTime expectedTime2 = new DateTime(2017, 12, 23, 3, 16, 9, DateTimeKind.Utc);
            decimal expectedPrice2 = 0.00000306m / 0.00000007m;
            ContainerEventDTO expectedEvent2 = new ContainerEventDTO(expectedTime2, _Container, "AUD", 0.00000306m, "ETC", 0.00000007m, null, null);
            TradeDTO expectedTrade2 = new TradeDTO(expectedTime2, _EXCHANGE, "1031185483", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice2, expectedEvent2);
            this.performGroupTest(group2, expectedType2, expectedTime2, expectedTrade2, expectedTransaction);
        }

        [TestMethod]
        public void LeadingLonelyRecordAddedToNext()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1100160460,2018-01-10T04:03:17Z,Trade,Buy Order,LTC,0.00000018,Buy 0.00000018LTC @ BTC 0.01699984 Trade settled,0.34832498,1100160131", // this is the lonely record
                "1100197205,2018-01-10T04:14:41Z,Trade,Buy Order,BTC,-0.0023595,Buy 0.13879604LTC @ BTC 0.01699984 Fully matched at 0.01699984,0.00639018,1100160131",
                "1100197206,2018-01-10T04:14:41Z,Trade,Buy Order,LTC,0.13879586,Buy 0.13879586LTC @ BTC 0.01699984 Trade settled,0.3483248,1100160131", // should be added to this
                "1100197207,2018-01-10T04:14:41Z,Trade,Trading Fee,LTC,-0.00030535,Buy 0.13879586LTC @ BTC 0.01699984 Trading fee,0.34801945,1100160131");

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 10, 4, 14, 41, DateTimeKind.Utc);
            decimal totalAmount = 0.00000018m + 0.13879586m;
            decimal expectedPrice = 0.0023595m / totalAmount;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "LTC");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", 0.0023595m, "LTC", totalAmount, "LTC", 0.00030535m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1100197205", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void TrailingLonelyRecordAddedToNext()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1107265023,2018-01-11T15:08:06Z,Trade,Sell Order,BCH,-0.01335814,Sell 0.01335817BCH @ BTC 0.18100000 Partially matched at 0.18100000,0.04323195,1107245347", // should be added to this
                "1107265024,2018-01-11T15:08:06Z,Trade,Sell Order,BTC,0.00241782,Sell 0.01335814BCH @ BTC 0.18100000 Trade settled,0.00936008,1107245347",
                "1107265025,2018-01-11T15:08:06Z,Trade,Trading Fee,BCH,-0.00002938,Sell 0.01335814BCH @ BTC 0.18100000 Trading fee,0.04320257,1107245347",
                "1107300326,2018-01-11T15:18:43Z,Trade,Sell Order,BCH,-0.00000003,Sell 0.01335817BCH @ BTC 0.18100000 Fully matched at 0.18100000,0.04320254,1107245347"); // this is the lonely record

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 11, 15, 8, 6, DateTimeKind.Utc);
            decimal totalAmount = 0.01335814m + 0.00000003m;
            decimal expectedPrice = 0.00241782m / totalAmount;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "BCH");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BCH", totalAmount, "BTC", 0.00241782m, "BCH", 0.00002938m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1107265023", expectedSymbol, MarketType.Spot, OrderSide.Sell, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void TrailingLonelyRecordWithFee()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1102344763,2018-01-10T15:52:17Z,Trade,Sell Order,XRP,-7.39524681,Sell 7.39525420XRP @ BTC 0.00013552 Partially matched at 0.00013552,39.2313995,1102092275", // A
                "1102344764,2018-01-10T15:52:17Z,Trade,Sell Order,BTC,0.0010022,Sell 7.39524680XRP @ BTC 0.00013552 Trade settled,0.01412573,1102092275",
                "1102344765,2018-01-10T15:52:17Z,Trade,Trading Fee,XRP,-0.01626954,Sell 7.39524680XRP @ BTC 0.00013552 Trading fee,39.21512996,1102092275", // B
                "1102344820,2018-01-10T15:52:17Z,Trade,Sell Order,XRP,-0.0000074,Sell 7.39525420XRP @ BTC 0.00013552 Fully matched at 0.00013552,39.21512256,1102092275", // add to A
                "1102344822,2018-01-10T15:52:17Z,Trade,Trading Fee,XRP,-0.00000001,Sell 0.00000740XRP @ BTC 0.00013552 Trading fee,39.21512255,1102092275"); // add to B

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 10, 15, 52, 17, DateTimeKind.Utc);
            decimal totalAmount = 7.39524681m + 0.0000074m;
            decimal feeAmount = 0.01626954m + 0.00000001m;
            decimal expectedPrice = 0.0010022m / totalAmount;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "XRP");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "XRP", totalAmount, "BTC", 0.0010022m, "XRP", feeAmount);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1102344763", expectedSymbol, MarketType.Spot, OrderSide.Sell, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void LonelyBuyRecordLongBeforeMainTradeWithFee()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1100160460,2018-01-10T04:03:17Z,Trade,Buy Order,LTC,0.00000018,Buy 0.00000018LTC @ BTC 0.01699984 Trade settled,0.34832498,1100160131",
                "1100197205,2018-01-10T04:14:41Z,Trade,Buy Order,BTC,-0.0023595,Buy 0.13879604LTC @ BTC 0.01699984 Fully matched at 0.01699984,0.00639018,1100160131",
                "1100197206,2018-01-10T04:14:41Z,Trade,Buy Order,LTC,0.13879586,Buy 0.13879586LTC @ BTC 0.01699984 Trade settled,0.3483248,1100160131",
                "1100197207,2018-01-10T04:14:41Z,Trade,Trading Fee,LTC,-0.00030535,Buy 0.13879586LTC @ BTC 0.01699984 Trading fee,0.34801945,1100160131");

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 10, 4, 14, 41, DateTimeKind.Utc);
            decimal totalAmount = 0.00000018m + 0.13879586m;
            decimal expectedPrice = 0.0023595m / totalAmount;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "LTC");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", 0.0023595m, "LTC", totalAmount, "LTC", 0.00030535m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1100197205", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void LonelyBuyRecordAndLonelyFeeRecordBeforeMainTradeWithFee()
        {
            BTCMarketsRecordGroup group = this.getGroup(
                "1146659438,2018-01-19T18:35:36Z,Trade,Buy Order,XRP,0.00002595,Buy 0.00002595XRP @ BTC 0.00013605 Trade settled,183.70869304,1146659426",
                "1146659439,2018-01-19T18:35:36Z,Trade,Trading Fee,XRP,-0.00000005,Buy 0.00002595XRP @ BTC 0.00013605 Trading fee,183.70869299,1146659426",
                "1146659448,2018-01-19T18:35:36Z,Trade,Buy Order,BTC,-0.00191098,Buy 14.04515600XRP @ BTC 0.00013606 Fully matched at 0.00013606,0.02416878,1146659426",
                "1146659449,2018-01-19T18:35:36Z,Trade,Buy Order,XRP,14.04512969,Buy 14.04513000XRP @ BTC 0.00013606 Trade settled,197.75382268,1146659426",
                "1146659450,2018-01-19T18:35:36Z,Trade,Trading Fee,XRP,-0.03089928,Buy 14.04513000XRP @ BTC 0.00013606 Trading fee,197.7229234,1146659426");

            GroupType expectedType = GroupType.TradeWithFee;
            DateTime expectedTime = new DateTime(2018, 1, 19, 18, 35, 36, DateTimeKind.Utc);
            decimal totalAmount = 14.04512969m + 0.00002595m;
            decimal totalFee = 0.00000005m + 0.03089928m;
            decimal expectedPrice = 0.00191098m / totalAmount;
            SymbolDTO expectedSymbol = new SymbolDTO("BTC", "XRP");
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", 0.00191098m, "XRP", totalAmount, "XRP", totalFee);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _EXCHANGE, "1146659448", expectedSymbol, MarketType.Spot, OrderSide.Buy, expectedPrice, expectedEvent);
            TransactionDTO expectedTransaction = null;
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void ForkDepositDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("6173,2017-08-05T19:24:13Z,Fund Transfer,Deposit,BCH,0.5004707,Deposit for Bitcoin Cash Fork,0.5004707,6173");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2017, 8, 5, 19, 24, 13, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "BCH", 0.5004707m, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "6173", null, null, TransactionType.Fork, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void DepositWithFeesDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("549015567,2017-08-15T10:36:03Z,Fund Transfer,Deposit,AUD,100,Poli fund transfer was successfull.,100.00000001,549000849", "549015568,2017-08-15T10:36:03Z,Fund Transfer,Withdraw,AUD,-3.3,Fund transfer fee: DEPOSIT POLI,96.70000001,549000849");

            GroupType expectedType = GroupType.DepositWithFee;
            DateTime expectedTime = new DateTime(2017, 8, 15, 10, 36, 3, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "AUD", 100, "AUD", 3.3m);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "549015567", null, null, TransactionType.FiatDeposit, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void FiatDepositDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("1733502974,2018-05-11T20:40:01Z,Fund Transfer,Deposit,AUD,50,EFT DEPOSIT was successfull.EFT Deposit  $ 50.00000000,50.00000002,1733502963");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2018, 5, 11, 20, 40, 1, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "AUD", 50, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "1733502974", null, null, TransactionType.FiatDeposit, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void FiatWithdrawalDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("4803139865,2019-12-11T01:10:13Z,Fund Transfer,Withdraw,AUD,-218.55,277211d1-a418-41d6-9c30-1f89565ebc65,0.00133642,4803133350");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2019, 12, 11, 1, 10, 13, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "AUD", 218.55m, null, null, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "4803139865", null, null, TransactionType.FiatWithdrawal, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void CryptoDepositDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("1228787114,2018-02-06T06:18:46Z,Fund Transfer,Deposit,BTC,1.60415422,Bitcoin DEPOSIT was successfull.BITCOIN Deposit  B 1.60415420,1.60415422,1228787111");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2018, 2, 6, 6, 18, 46, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "BTC", 1.60415422m, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "1228787114", null, null, TransactionType.CryptoDeposit, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void CryptoWithdrawalDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("5113285948,2020-01-19T13:04:49Z,Fund Transfer,Withdraw,XRP,-73.63919858,8156999FB4A975FCB0BFAE98D798D9D088549BAF0907ABDEC74AB2E52EC53513,0,5113274777");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2020, 1, 19, 13, 4, 49, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "XRP", 73.63919858m, null, null, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "5113285948", "8156999FB4A975FCB0BFAE98D798D9D088549BAF0907ABDEC74AB2E52EC53513", null, TransactionType.CryptoWithdrawal, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void FlrAirdropDetectedCorrectly()
        {
            BTCMarketsRecordGroup group = this.getGroup("12766310766,2023-01-10T16:38:04Z,Fund Transfer,Deposit,FLR,1829.76045,Airdrop payment. Source: XRP  Target: FLR,1829.76045,6502221690");

            GroupType expectedType = GroupType.FundsTransfer;
            DateTime expectedTime = new DateTime(2023, 1, 10, 16, 38, 04, DateTimeKind.Utc);
            TradeDTO expectedTrade = null;
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "FLR", 1829.76045m, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _EXCHANGE, "12766310766", null, null, TransactionType.StakingOrAirdrops, expectedEvent);
            this.performGroupTest(group, expectedType, expectedTime, expectedTrade, expectedTransaction);
        }

        /// <summary>
        /// Reads the given csv data and gets the single group from that data. Throws if there is not exactly 1 group.
        /// </summary>
        private BTCMarketsRecordGroup getGroup(params DString[] csvContentsNoHeader)
        {
            return this.getMultipleGroups(csvContentsNoHeader).Single();
        }

        /// <summary>
        /// Reads the given csv data and gets any number of groups from that data.
        /// </summary>
        private IEnumerable<BTCMarketsRecordGroup> getMultipleGroups(params DString[] csvContentsNoHeader)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { _HEADER_ROW }.Concat(csvContentsNoHeader);
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            BTCMarketsSchema schema = new BTCMarketsSchema();

            IEnumerable<Record> allRecords = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row));
            IEnumerable<RecordGroup> groups = schema.ProcessAll(allRecords);
            return groups.Cast<BTCMarketsRecordGroup>();
        }

        private void performGroupTest(BTCMarketsRecordGroup group, GroupType expectedType, DateTime expectedTime, TradeDTO expectedTrade, TransactionDTO expectedTransaction)
        {
            Assert.AreEqual(expectedType, group.Type);
            Assert.AreEqual(expectedTime, group.Time);
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }
    }
}
