﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.Binance;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Collections;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class BinanceTests
    {
        private static readonly Exchange EXCHANGE = Exchange.Binance;
        private static readonly Container CONTAINER = Container.FromExchange(EXCHANGE);

        [TestMethod]
        public void BinanceDepositTest()
        {
            Record record = this.getRecord("2018-12-29 02:15:22,XRP,3729.412,0,rEb8TK3gBgk5auZkwc6sHnwrGVJH8DuaLh,002F03F28D0896A7DDEFBF71E6D1D37CD59DF253A963A4411B93B5BD030CC50B,,109285712,Completed", SchemaIdentifier.Binance_Deposits);

            DateTime time = new DateTime(2018, 12, 29, 2, 15, 22, DateTimeKind.Utc);
            this.testTransaction(record, time, null, "002F03F28D0896A7DDEFBF71E6D1D37CD59DF253A963A4411B93B5BD030CC50B", null, TransactionType.CryptoDeposit, "XRP", 3729.412m);
        }

        [TestMethod]
        public void BinanceWithdrawalsTest()
        {
            Record record = this.getRecord("2017-12-29 08:59:18,XRP,7147.128825,0.15,rKaYwDZoKoP6rL6nxtPEucN8V27KvtNDRg,93F23718FC0E4DC7AAD919FC326F0C99E98E835916E7372FE229CA214F0D8F49,,,Completed", SchemaIdentifier.Binance_Withdrawals);

            DateTime time = new DateTime(2017, 12, 29, 8, 59, 18, DateTimeKind.Utc);
            this.testTransaction(record, time, null, "93F23718FC0E4DC7AAD919FC326F0C99E98E835916E7372FE229CA214F0D8F49", "rKaYwDZoKoP6rL6nxtPEucN8V27KvtNDRg", TransactionType.CryptoWithdrawal, null, null, "XRP", 7147.128825m, "XRP", 0.15m);
        }

        [TestMethod]
        public void BinanceCancelledWithdrawalTest()
        {
            Record record = this.getRecord("2019-06-26 08:34:20,BTC,0.045,0.0005,3BMEXiHnHTXW1pNsfeEgc57fMsoTHkfhg6,,,,Cancelled", SchemaIdentifier.Binance_Withdrawals);

            Assert.IsNull(record.ToTradeDTO());
            Assert.IsNull(record.ToTransactionDTO());
        }

        [TestMethod]
        public void BinanceFiatWithdrawalTest()
        {
            Record record = this.getRecord("2020-03-26 20:22:15,AUD,196.2,Successful, ,200,3.80,e64517dc134c47219e400494c7465fe6", SchemaIdentifier.Binance_FiatDeposits);

            DateTime time = new DateTime(2020, 3, 26, 20, 22, 15, DateTimeKind.Utc);
            // important: the fee is subtracted after the total amount has been received
            this.testTransaction(record, time, "e64517dc134c47219e400494c7465fe6", null, null, TransactionType.FiatDeposit, "AUD", 200m, null, null, "AUD", 3.8m);
        }

        [TestMethod]
        public void BinanceDistributionTest()
        {
            Record record = this.getRecord("2018-07-17 22:08:44, BNB, 2.53071347, Fiat and Spot, Distribution, Refund 70% Trading Fee(2018/7/5-2018/7/14)", SchemaIdentifier.Binance_Distributions);

            DateTime time = new DateTime(2018, 7, 17, 12, 8, 44, DateTimeKind.Utc);
            this.testTransaction(record, time, "17/07/2018: Refund 70% Trading Fee(2018/7/5-2018/7/14)", null, null, TransactionType.StakingOrAirdrops, "BNB", 2.53071347m);
        }

        [TestMethod]
        public void BinanceSellTradeWithFeeTest()
        {
            Record record = this.getRecord("2018-10-30 02:28:20,NCASHBTC,SELL,0.0000008800,\"1,405.0000000000NCASH\",0.00123640BTC,0.0006221100BNB", SchemaIdentifier.Binance_SpotTrades);

            DateTime time = new DateTime(2018, 10, 30, 2, 28, 20, DateTimeKind.Utc);
            this.testTrade(record, time, "", "BTC", "NCASH", MarketType.Spot, 0.00000088m, OrderSide.Sell, "BTC", 0.0012364m, "NCASH", 1405, "BNB", 0.00062211m);
        }
        
        [TestMethod]
        public void BinanceBuyTradeWithFeeTest()
        {
            Record record = this.getRecord("2018-10-01 00:12:13,GRSBTC,BUY,0.0000837500,6.0000000000GRS,0.00050250BTC,0.0060000000GRS", SchemaIdentifier.Binance_SpotTrades);

            DateTime time = new DateTime(2018, 10, 1, 0, 12, 13, DateTimeKind.Utc);
            this.testTrade(record, time, "", "BTC", "GRS", MarketType.Spot, 0.00008375m, OrderSide.Buy, "GRS", 6, "BTC", 0.0005025m, "GRS", 0.006m);
        }

        [TestMethod]
        public void BinanceTradeWithNoFeeTest()
        {
            Record record = this.getRecord("2018-12-25 03:46:54,PAXUSDT,SELL,0.9915000000,10.5500000000PAX,10.46032500USDT,0.0000000000BNB", SchemaIdentifier.Binance_SpotTrades);

            DateTime time = new DateTime(2018, 12, 25, 3, 46, 54, DateTimeKind.Utc);
            this.testTrade(record, time, "", "USDT", "PAX", MarketType.Spot, 0.9915m, OrderSide.Sell, "USDT", 10.460325m, "PAX", 10.55m);
        }

        [TestMethod]
        public void BinanceFuturesNegativePnlTest()
        {
            Record record = this.getRecord("2020-06-21 21:59:41,XRPUSDT,SELL,0.18710000,8454.20000000,\"1,581.7808200000 USDT\",-0.6327123200 USDT,-66.7881800000", SchemaIdentifier.Binance_FuturesTrades);

            DateTime time = new DateTime(2020, 6, 21, 21, 59, 41, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.FuturesPnL, null, null, "USDT", 66.78818m + 0.63271232m);
        }

        [TestMethod]
        public void BinanceFuturesNoPnlTest()
        {
            Record record = this.getRecord("2020-06-09 19:24:10,VETUSDT,BUY,0.00773000,132.00000000,1.0203600000 USDT,-0.0002040700 USDT,0.0000000000", SchemaIdentifier.Binance_FuturesTrades);

            DateTime time = new DateTime(2020, 6, 9, 19, 24, 10, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.FuturesPnL, null, null, "USDT", 0.0002040700m);
        }

        [TestMethod]
        public void BinanceFuturesPositivePnlTest()
        {
            Record record = this.getRecord("2020-01-26 22:17:10,LINKUSDT,SELL,2.57600000,0.11000000,0.2833600000 USDT,-0.0000566700 USDT,0.0071674400", SchemaIdentifier.Binance_FuturesTrades);

            DateTime time = new DateTime(2020, 1, 26, 22, 17, 10, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.FuturesPnL, "USDT", -0.00005667m + 0.00716744m);
        }

        [TestMethod]
        public void BinanceOptionsNegativePnlTest()
        {
            RecordGroup group = this.getRecordGroup(SchemaIdentifier.Binance_Options, "2021-01-08 05:13:25,Option Cost,-69.8401USDT,USDT", "2021-01-08 05:13:25,Fee,-0.5081USDT,USDT");

            DateTime time = new DateTime(2021, 1, 7, 19, 13, 25, DateTimeKind.Utc); // account for UTC+10
            this.testTransaction(group, time, "Options-07/01/2021 19:13:25", null, null, TransactionType.OptionsPnL, null, null, "USDT", 69.8401m + 0.5081m);
        }

        [TestMethod]
        public void BinanceWalletTransactionsInsuranceFundCompensationTest()
        {
            Record record = this.getRecord("2020-01-03 17:40:21,USDT-Futures,Insurance fund compensation,USDT,1.48825351,穿仓保证金补偿", SchemaIdentifier.Binance_WalletTransactions);

            DateTime time = new DateTime(2020, 1, 3, 17, 40, 21, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.StakingOrAirdrops, "USDT", 1.48825351m);
        }

        [TestMethod]
        public void BinanceWalletTransactionsSingleFundingPaymentTest()
        {
            RecordGroup group = this.getRecordGroup(SchemaIdentifier.Binance_WalletTransactions, "2020-01-02 16:00:00,USDT-Futures,Funding Fee,USDT,0.00001296,\"\"");

            DateTime time = new DateTime(2020, 1, 2, 16, 0, 0, DateTimeKind.Utc);
            this.testTransaction(group, time, "AggregateFundingFee-02/01/2020 16:00:00", null, null, TransactionType.FuturesFunding, "USDT", 0.00001296m);
        }

        [TestMethod]
        public void BinanceWalletTransactionsMultipleFundingPaymentTest()
        {
            RecordGroup group = this.getRecordGroup(SchemaIdentifier.Binance_WalletTransactions, "2020-01-05 08:00:00,USDT-Futures,Funding Fee,USDT,-0.22386937,\"\"", "2020-01-05 08:00:01,USDT-Futures,Funding Fee,USDT,0.06800466,\"\"");

            DateTime time = new DateTime(2020, 1, 5, 8, 0, 0, DateTimeKind.Utc);
            this.testTransaction(group, time, "AggregateFundingFee-05/01/2020 08:00:00", null, null, TransactionType.FuturesFunding, null, null, "USDT", 0.15586471m);
        }

        [TestMethod]
        public void BinanceWalletTransactionsWithExponentNotationTest()
        {
            BinanceWalletTransactionsRecord record = this.getRecord("2018-04-13 00:41:41,Spot,Fee,BTC,-7.1E-7,\"\"", SchemaIdentifier.Binance_WalletTransactions) as BinanceWalletTransactionsRecord;

            Assert.AreEqual(-0.00000071m, record.Amount);
        }

        [TestMethod]
        public void BinanceWalletTransactionsV2DetectsTrade()
        {
            RecordGroup group = this.getRecordGroup(SchemaIdentifier.Binance_WalletTransactions_V2,
                "38805222,6/01/2022 21:52,Spot,Large OTC trading,AUD,-500,",
                "38805222,6/01/2022 21:52,Spot,Large OTC trading,USDT,357.006,");

            DateTime time = new DateTime(2022, 1, 6, 21, 52, 0, DateTimeKind.Utc);
            // equivalent to buying the USDTAUD pair
            this.testTrade(group, time, "Trade-06/01/2022 21:52:00", "AUD", "USDT", MarketType.Spot, 500 / 357.006m, OrderSide.Buy, "USDT", 357.006m, "AUD", 500);
        }

        [TestMethod]
        public void BinanceBnbDustConversionIncomingTest()
        {
            // as seen in Binance_Master_WalletTransactions_2020_1.csv
            Record record = this.getRecord("2020-03-26 21:02:31,Spot,Small assets exchange BNB,BNB,0.00074474,\"\"", SchemaIdentifier.Binance_WalletTransactions);

            DateTime time = new DateTime(2020, 3, 26, 21, 2, 31, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.DustConversion, "BNB", 0.00074474m);
        }

        [TestMethod]
        public void BinanceBnbDustConversionOutgoingTest()
        {
            // as seen in Binance_Master_WalletTransactions_2020_1.csv
            Record record = this.getRecord("2020-03-26 21:02:31,Spot,Small assets exchange BNB,XRP,-0.03409400,\"\"", SchemaIdentifier.Binance_WalletTransactions);

            DateTime time = new DateTime(2020, 3, 26, 21, 2, 31, DateTimeKind.Utc);
            this.testTransaction(record, time, "", null, null, TransactionType.DustConversion, null, null, "XRP", 0.03409400m);
        }

        [TestMethod]
        public void BinanceBuyCryptoTest()
        {
            // as seen in Binance_Master_BuyCrypto_2020_1.csv
            Record record = this.getRecord("2020-03-26 20:22:15,Credit Card,200.00 AUD,11388.73 BTC/AUD,4.00 AUD,0.01721 BTC,Completed,cc9307e5f5314015bd65ef1003fe6fac", SchemaIdentifier.Binance_BuyCrypto);

            DateTime txTime = new DateTime(2020, 3, 26, 20, 22, 15, DateTimeKind.Utc);
            TransactionDTO tx = new TransactionDTO(txTime, EXCHANGE, "cc9307e5f5314015bd65ef1003fe6fac_tx", null, null, TransactionType.FiatDeposit, new ContainerEventDTO(txTime, CONTAINER, null, null, "AUD", 200, "AUD", 4));

            DateTime tradeTime = txTime.AddSeconds(1);
            TradeDTO trade = new TradeDTO(tradeTime, EXCHANGE, "cc9307e5f5314015bd65ef1003fe6fac_trade", new SymbolDTO("AUD", "BTC"), MarketType.Spot, OrderSide.Buy, 196.0m / 0.01721m, new ContainerEventDTO(tradeTime, CONTAINER, "AUD", 196, "BTC", 0.01721m, null, null));

            Helpers.PerformRecordTest(record, trade, tx);
        }

        [TestMethod]
        public void BinanceConvertTest()
        {
            // as seen in 2022-2023/Binance_Master_Convert_2022-2023.csv
            Record record = this.getRecord("2022-11-12 02:21:31,Spot Wallet,ETHAUD,Market,5000 AUD,2.61667576 ETH,1 AUD = 0.00052334 ETH,1 ETH = 1910.82 AUD,2022-11-12 02:21:32,Successful", SchemaIdentifier.Binance_Convert);
            DateTime time = new DateTime(2022, 11, 12, 2, 21, 31, DateTimeKind.Utc);

            this.testTrade(record, time, "BinanceConvert-12/11/2022 02:21:31", "AUD", "ETH", MarketType.Spot, 5000 / 2.61667576m, OrderSide.Buy, "ETH", 2.61667576m, "AUD", 5000, null, null);
        }

        [TestMethod]
        public void BinanceTransferV2_WithdrawalTest()
        {
            // as seen in 2022-2023/Binance_Master_Withdrawals_2022-2023.csv
            Record record = this.getRecord("2022-11-12 02:26:53,ETH,ETH,0.049232,0.000768,0xa5478Cab569FEE520a63D6B43E5faef45ff3EF99,0xc26bcaab4eb9912c108e13c0d77246fe5399e3f2ba4d4e136ea027a52bf9587d,,,", SchemaIdentifier.Binance_Withdrawals_V2);

            DateTime time = new DateTime(2022, 11, 12, 2, 26, 53, DateTimeKind.Utc);

            this.testTransaction(record, time, null, "0xc26bcaab4eb9912c108e13c0d77246fe5399e3f2ba4d4e136ea027a52bf9587d", "0xa5478Cab569FEE520a63D6B43E5faef45ff3EF99", TransactionType.CryptoWithdrawal, null, null, "ETH", 0.049232m, "ETH", 0.000768m);
        }

        private Record getRecord(DString csvLine, SchemaIdentifier schemaId)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { this.getCsvHeader(schemaId), csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            Schema schema = SchemaFactory.Create(schemaId, "");

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record;
        }

        private RecordGroup getRecordGroup(SchemaIdentifier schemaId, params DString[] csvLines)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { this.getCsvHeader(schemaId) }.AppendMany(csvLines);
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            Schema schema = SchemaFactory.Create(schemaId, "");

            IEnumerable<Record> records = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row));
            RecordGroup group = schema.ProcessAll(records).Single();
            return group;
        }

        private DString getCsvHeader(SchemaIdentifier schemaId)
        {
#pragma warning disable IDE0010 // Add missing cases
            switch (schemaId)
            {
                case SchemaIdentifier.Binance_Withdrawals:
                case SchemaIdentifier.Binance_Deposits:
                    return "Date(UTC),Coin,Amount,TransactionFee,Address,TXID,SourceAddress,PaymentID,Status";
                case SchemaIdentifier.Binance_Withdrawals_V2:
                    return "Date(UTC),Coin,Network,Amount,TransactionFee,Address,TXID,SourceAddress,PaymentID,Status";
                case SchemaIdentifier.Binance_FiatDeposits:
                    return "Date(UTC),Coin,Amount,Status,Payment Method,Indicated Amount,Fee,Order ID";
                case SchemaIdentifier.Binance_Distributions:
                    return "Time(UTC+10), Coin, Amount, Account, Type, Note";
                case SchemaIdentifier.Binance_SpotTrades:
                    return "Date(UTC),Pair,Side,Price,Executed,Amount,Fee";
                case SchemaIdentifier.Binance_BuyCrypto:
                    return "Date(UTC),Method,Amount,Price,Fees,Final Amount,Status,Transaction ID";
                case SchemaIdentifier.Binance_FuturesTrades:
                    return "Date(UTC),Pair,Side,Price,Quantity,Amount,Fee,Realized Profit";
                case SchemaIdentifier.Binance_Options:
                    return "Time(UTC+10),Type,Amount,Asset";
                case SchemaIdentifier.Binance_WalletTransactions:
                    return "UTC_Time,Account,Operation,Coin,Change,Remark";
                case SchemaIdentifier.Binance_WalletTransactions_V2:
                    return "User_ID,UTC_Time,Account,Operation,Coin,Change,Remark";
                case SchemaIdentifier.Binance_Convert:
                    return "Date,Wallet,Pair,Type,Sell,Buy,Price,Inverse Price,Date Updated,Status";

                case SchemaIdentifier.MercatoX:
                case SchemaIdentifier.OnChain_General:
                case SchemaIdentifier.OnChain_0xe5dd:
                case SchemaIdentifier.BtcMarkets:
                case SchemaIdentifier.BitMex:
                case SchemaIdentifier.Ftx:
                default:
                    throw new AssertUnreachable(schemaId);
            }
#pragma warning restore IDE0010 // Add missing cases
        }

        private void testTrade(Record record, DateTime expectedTime, DString expectedTradeId, DString expectedQuoteCurrency, DString expectedBaseCurrency, MarketType expectedMarketType, decimal expectedPrice, OrderSide expectedSide, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            SymbolDTO expectedSymbol = new SymbolDTO(expectedQuoteCurrency, expectedBaseCurrency);
            TradeDTO expected = new TradeDTO(expectedTime, EXCHANGE, expectedTradeId, expectedSymbol, expectedMarketType, expectedSide, expectedPrice, expectedContainerEvent);

            Helpers.PerformRecordTest(record, expected, null);
        }

        private void testTrade(RecordGroup record, DateTime expectedTime, DString expectedTradeId, DString expectedQuoteCurrency, DString expectedBaseCurrency, MarketType expectedMarketType, decimal expectedPrice, OrderSide expectedSide, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            SymbolDTO expectedSymbol = new SymbolDTO(expectedQuoteCurrency, expectedBaseCurrency);
            TradeDTO expected = new TradeDTO(expectedTime, EXCHANGE, expectedTradeId, expectedSymbol, expectedMarketType, expectedSide, expectedPrice, expectedContainerEvent);

            Helpers.PerformRecordGroupTest(record, expected, null);
        }

        private void testTransaction(Record record, DateTime expectedTime, NString expectedInternalId, NString expectedPublicId, NString expectedCounterparty, TransactionType expectedType, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            TransactionDTO expected = new TransactionDTO(expectedTime, EXCHANGE, expectedInternalId, expectedPublicId, expectedCounterparty, expectedType, expectedContainerEvent);

            Helpers.PerformRecordTest(record, null, expected);
        }

        private void testTransaction(RecordGroup group, DateTime expectedTime, NString expectedInternalId, NString expectedPublicId, NString expectedCounterparty, TransactionType expectedType, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            TransactionDTO expected = new TransactionDTO(expectedTime, EXCHANGE, expectedInternalId, expectedPublicId, expectedCounterparty, expectedType, expectedContainerEvent);

            Helpers.PerformRecordGroupTest(group, null, expected);
        }
    }
}
