﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.Kraken;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class KrakenTests
    {
        private static readonly Exchange EXCHANGE = Exchange.Kraken;
        private static readonly Container CONTAINER = Container.FromExchange(EXCHANGE);

        [TestMethod]
        public void TradesAreIgnoredInLedger()
        {
            KrakenLedgerRecord record = this.getLedgerRecord("\"LUGEON-BKOE2-GWNBN7\",\"TOPWK7-4LVL4-OMXKHY\",\"2024-04-03 05:55:11\",\"trade\",\"\",\"currency\",\"MOON\",\"spot / main\",2037.77737,0,547107.51653");

            Assert.IsNull(record.ToTradeDTO());
            Assert.IsNull(record.ToTransactionDTO());
        }

        [TestMethod]
        public void DepositWorksCorrectly()
        {
            KrakenLedgerRecord record = this.getLedgerRecord("\"LSVVDW-K37LA-TXQUIY\",\"FTkR0xQ-BSYnZzbITjppunnyVkhOab\",\"2024-03-17 23:31:35\",\"deposit\",\"\",\"currency\",\"MOON\",\"spot / main\",100.00000,0,308252.78417");

            DateTime time = new DateTime(2024, 3, 17, 23, 31, 35, DateTimeKind.Utc);
            this.testTransaction(record, time, "LSVVDW-K37LA-TXQUIY", null, null, TransactionType.CryptoDeposit, "MOON", 100);
        }

        [TestMethod]
        public void BuyTradeWorksCorrectly()
        {
            KrakenTradeRecord record = this.getTradeRecord("\"TF6C6T-UXZR3-MB2GAI\",\"ORIQHV-SYGAS-CMN6WL\",\"AUD/USD\",\"2024-05-17 06:15:58.175\",\"buy\",\"market\",0.66738,2002.14000,4.00428,3000.00000000,0.00000,\"initiated\",\"LUV4AA-SIM4R-47U4UO,L4RPDA-DMESW-RGXOHZ\"");

            DateTime time = new DateTime(2024, 5, 17, 6, 15, 58, 175, DateTimeKind.Utc);
            this.testTrade(record, time, "TF6C6T-UXZR3-MB2GAI", "USD", "AUD", MarketType.Spot, 0.66738m, OrderSide.Buy, "AUD", 3000, "USD", 2002.14000m, "USD", 4.00428m);
        }

        [TestMethod]
        public void SellTradeWorksCorrectly()
        {
            KrakenTradeRecord record = this.getTradeRecord("\"TE7JYW-F2XZZ-BEIL6Z\",\"OR4XHS-RS4E2-44CP6J\",\"ETH/USD\",\"2023-12-22 10:16:03.097\",\"sell\",\"market\",2308.18000,46394.41800,120.62549,20.10000000,0.00000,\"initiated\",\"LDKNLE-2VOXY-4EWYGZ,LIOSWU-6CG7M-6U362F\"");

            DateTime time = new DateTime(2023, 12, 22, 10, 16, 3, 97, DateTimeKind.Utc);
            this.testTrade(record, time, "TE7JYW-F2XZZ-BEIL6Z", "USD", "ETH", MarketType.Spot, 2308.18000m, OrderSide.Sell, "USD", 46394.41800m, "ETH", 20.1m, "USD", 120.62549m);
        }

        [TestMethod]
        public void BankWithdrawalDetectedCorrectly()
        {
            KrakenLedgerRecord record = this.getLedgerRecord("\"LK2CZ5-X4YZ4-D4OHX4\",\"FTjprpd-tjajZL5zixJ9jmL3g8fFSx\",\"2024-03-26 04:22:13\",\"withdrawal\",\"\",\"currency\",\"AUD\",\"spot / main\",-2000.0000,0,0.0000");

            DateTime time = new DateTime(2024, 3, 26, 4, 22, 13, DateTimeKind.Utc);
            this.testTransaction(record, time, "LK2CZ5-X4YZ4-D4OHX4", null, null, TransactionType.FiatWithdrawal, null, null, "AUD", 2000);
        }

        private KrakenTradeRecord getTradeRecord(DString csvLine)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { "\"txid\",\"ordertxid\",\"pair\",\"time\",\"type\",\"ordertype\",\"price\",\"cost\",\"fee\",\"vol\",\"margin\",\"misc\",\"ledgers\"", csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            KrakenTradeSchema schema = new KrakenTradeSchema();

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record as KrakenTradeRecord;
        }

        private KrakenLedgerRecord getLedgerRecord(DString csvLine)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { "\"txid\",\"refid\",\"time\",\"type\",\"subtype\",\"aclass\",\"asset\",\"wallet\",\"amount\",\"fee\",\"balance\"", csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            KrakenLedgerSchema schema = new KrakenLedgerSchema();

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record as KrakenLedgerRecord;
        }

        private void testTrade(Record record, DateTime expectedTime, DString expectedTradeId, DString expectedQuoteCurrency, DString expectedBaseCurrency, MarketType expectedMarketType, decimal expectedPrice, OrderSide expectedSide, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            SymbolDTO expectedSymbol = new SymbolDTO(expectedQuoteCurrency, expectedBaseCurrency);
            TradeDTO expected = new TradeDTO(expectedTime, EXCHANGE, expectedTradeId, expectedSymbol, expectedMarketType, expectedSide, expectedPrice, expectedContainerEvent);

            Helpers.PerformRecordTest(record, expected, null);
        }

        private void testTransaction(Record record, DateTime expectedTime, NString expectedInternalId, NString expectedPublicId, NString expectedCounterparty, TransactionType expectedType, string expectedCurrencyReceived = null, decimal? expectedAmountReceived = null, string expectedCurrencySent = null, decimal? expectedAmountSent = null, string expectedFeeCurrency = null, decimal? expectedFeeAmount = null)
        {
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, CONTAINER, expectedCurrencySent, expectedAmountSent, expectedCurrencyReceived, expectedAmountReceived, expectedFeeCurrency, expectedFeeAmount);
            TransactionDTO expected = new TransactionDTO(expectedTime, EXCHANGE, expectedInternalId, expectedPublicId, expectedCounterparty, expectedType, expectedContainerEvent);

            Helpers.PerformRecordTest(record, null, expected);
        }
    }
}
