﻿using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.OnChain;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class SchemaFactoryTests
    {
        [TestMethod]
        public void AllIdentifiersCreateSchema()
        {
            List<SchemaIdentifier> fails = new List<SchemaIdentifier>();
            List<Schema> schemas = new List<Schema>();

            foreach (SchemaIdentifier identifier in Enum.GetValues(typeof(SchemaIdentifier)))
            {
                // there is a test below specifically for this identifier
                if (identifier == SchemaIdentifier.OnChain_General) continue;

                try
                {
                    Schema schema = SchemaFactory.Create(identifier, "");
                    if (schemas.Contains(schema)) throw new Exception("Duplicate schema");
                    schemas.Add(schema);
                }
                catch
                {
                    fails.Add(identifier);
                }
            }

            Assert.AreEqual(0, fails.Count, $"The following SchemaIdentifiers currently do not produce schemas: {string.Join(", ", fails.Select(f => f))}");
        }

        [DataTestMethod]
        [DataRow("EthereumMainnet", BlockchainType.Ethereum)]
        [DataRow("EthereumArbitrumNova", BlockchainType.ArbitrumNova)]
        [DataRow("XRPL", BlockchainType.XRPL)]
        [DataRow("Rinkeby", null)]
        public void OnChain_GeneralSchema_CorrectlyParsedFromFileName(string chainTypeString, BlockchainType? expectedBlockchainType)
        {
            string fileName = $@"test\{chainTypeString}\{KnownWallets.ETH_MY_WALLET.Address}.csv";

            if (expectedBlockchainType == null)
            {
                Assert.ThrowsException<Exception>(() => SchemaFactory.Create(SchemaIdentifier.OnChain_General, fileName));
                return;
            }

            FieldInfo walletAddressProperty = typeof(OnChain_GeneralSchema).GetField("_WalletAddress", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo chainTypeProperty = typeof(OnChain_GeneralSchema).GetField("_BlockchainType", BindingFlags.NonPublic | BindingFlags.Instance);

            OnChain_GeneralSchema schema = SchemaFactory.Create(SchemaIdentifier.OnChain_General, fileName) as OnChain_GeneralSchema;

            Assert.AreEqual(KnownWallets.ETH_MY_WALLET.Address, walletAddressProperty.GetValue(schema));
            Assert.AreEqual(expectedBlockchainType, chainTypeProperty.GetValue(schema));
        }

        [TestMethod]
        public void OnChain_GeneralSchema_ThrowsIfGivenUnsupportedWallet()
        {
            Assert.ThrowsException<ArgumentException>(() => SchemaFactory.Create(SchemaIdentifier.OnChain_General, $@"test\EthereumMainnet\address.csv"));
        }
    }
}
