﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.BitMex;
using CryptoTax.Persistence.Services;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

#pragma warning disable CS0162 // Unreachable code detected

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class BitMexTests
    {
        private const string _CSV_HEADER_ROW = "Id,Time,RowType,Amount,Fee,Currency,Text,ExecutionType,Symbol,SettlCurrency,Side,Price,Cost,OrderType,WalletEventType,Address,Tx,Status";
        private static readonly Exchange _Exchange = Exchange.BitMex;
        private static readonly Container _Container = Container.FromExchange(_Exchange);
        private static readonly Func<long, decimal> _BTC = sats => (decimal)sats / 100_000_000;
        private Mock<ICsvDataReaderServiceFactory> _Factory = new Mock<ICsvDataReaderServiceFactory>(MockBehavior.Strict);

        [TestMethod]
        public void DepositDetectedCorrectly()
        {
            BitMexWalletRecord record = this.getWalletRecord("22b55c10-7724-88af-f020-028804f5d64b,24/06/2019 1:23:11 AM,Wallet,1000000,,XBt,,,,,,,,,Deposit,,,Completed");

            DateTime expectedTime = new DateTime(2019, 6, 24, 1, 23, 11, DateTimeKind.Utc);
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "BTC", 0.01m, null, null);
            TradeDTO expectedTrade = null;
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _Exchange, "22b55c10-7724-88af-f020-028804f5d64b", null, null, TransactionType.CryptoDeposit, expectedEvent);
            this.performRecordTest(record, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void PositiveFundingPaymentDetectedCorrectly()
        {
            // receiving funding (positive refers to how it effects US) - don't bother checking the negative case as that's trivial if positive works
            BitMexEmptyRecord record = this.getRecord<BitMexEmptyRecord>("bbff8dc3-141a-a0a1-7de4-efb8399c33a5,24/06/2019 4:00:00 AM,Execution,1000,-14683,USD,Funding,Funding,XBTUSD,XBt,,10638.11,9400000,Limit,,,,");
            Assert.IsNotNull(record);
            return;

            DateTime expectedTime = new DateTime(2019, 6, 24, 4, 0, 0, DateTimeKind.Utc);
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "BTC", _BTC(14683), null, null);
            TradeDTO expectedTrade = null;
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _Exchange, "bbff8dc3-141a-a0a1-7de4-efb8399c33a5", null, null, TransactionType.FuturesFunding, expectedEvent);
            this.performRecordTest(record, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void NegativeFundingPaymentDetectedCorrectly()
        {
            // sending funding (negative refers to how it effects US)
            BitMexEmptyRecord record = this.getRecord<BitMexEmptyRecord>("936d3108-8f7a-db12-c884-72f0295064c9,27/06/2019 4:00:00 AM,Execution,3000,32753,USD,Funding,Funding,XBTUSD,XBt,,12814.53,-23412000,Limit,,,,");
            Assert.IsNotNull(record);
            return;

            DateTime expectedTime = new DateTime(2019, 6, 24, 4, 0, 0, DateTimeKind.Utc);
            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", _BTC(32753), null, null, null, null);
            TradeDTO expectedTrade = null;
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _Exchange, "936d3108-8f7a-db12-c884-72f0295064c9", null, null, TransactionType.FuturesFunding, expectedEvent);
            this.performRecordTest(record, expectedTime, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void BuyIncreasingPosition()
        {
            RecordGroup group = this.getGroupFromNthRecord(1,
                "042d61e9-7b66-f0be-cdb4-16d435aabbc8,26/06/2019 2:20:35 AM,Execution,2000,17851,XBT,Submission from www.bitmex.com,Trade,XRPM19,XBt,Buy,0.00003967,7934000,Market,,,,", // increasing - this is the one we are testing
                "042d61e9-7b66-f0be-cdb4-16d435aabbc9,26/06/2019 2:21:35 AM,Execution,2000,17851,XBT,Submission from www.bitmex.com,Trade,XRPM19,XBt,Sell,0.00003967,7934000,Market,,,,"); // closing trade required

            DateTime expectedTime = new DateTime(2019, 6, 26, 2, 20, 35, DateTimeKind.Utc);
            BitMexSymbol symbol = BitMexSymbol.XRPM19;
            decimal price = 0.00003967m;

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", _BTC(7934000), "XRPM19", 2000, "BTC", _BTC(17851));
            TransactionDTO expectedTransaction = null;
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _Exchange, "042d61e9-7b66-f0be-cdb4-16d435aabbc8", symbol.ToDTO(), MarketType.DerivativesFutures, OrderSide.Buy, price, expectedEvent);
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void BuyDecreasingPosition()
        {
            RecordGroup group = this.getGroupFromNthRecord(4,
                "28ae1409-7491-2948-155c-f0ad83ac2480,24/06/2019 1:35:54 AM,Execution,1000,6396,USD,Submission from www.bitmex.com,Trade,XBTUSD,XBt,Sell,10553,9476000,Market,,,,", // 1
                "bbff8dc3-141a-a0a1-7de4-efb8399c33a5,24/06/2019 4:00:00 AM,Execution,1000,-14683,USD,Funding,Funding,XBTUSD,XBt,,10638.11,9400000,Limit,,,,", // funding
                "0bb7978c-a258-fe12-e630-7df93844fbae,24/06/2019 12:00:00 PM,Wallet,8287,0,XBt,,,,,,,,,RealisedPNL,XBTUSD,496eb450-b377-2962-3a78-5780008f0ecb,Completed", // rpnl
                "2295c962-4704-afab-26e9-80120c4159f7,25/06/2019 10:34:24 PM,Execution,10,58,USD,Submission from www.bitmex.com,Trade,XBTUSD,XBt,Sell,11611.5,86120,Market,,,,", // 2
                "e607a159-2e7e-9b7f-8b04-22b91890ed56,25/06/2019 10:36:38 PM,Execution,500,2916,USD,Submission from www.bitmex.com,Trade,XBTUSD,XBt,Sell,11571.5,4321000,Market,,,,", // 3
                "0e40f8a0-c415-58bd-d876-e5837a39bc53,25/06/2019 10:57:15 PM,Execution,1000,5779,USD,Submission from www.bitmex.com,Trade,XBTUSD,XBt,Buy,11680,-8562000,Market,,,,", // 4 - this record here
                "863e8a85-2c36-37dd-ec06-e3f6a1c0e46f,26/06/2019 12:33:48 AM,Execution,510,2889,USD,Liquidation,Trade,XBTUSD,XBt,Buy,11914.68,-4280430,StopLimit,,,,");

            DateTime expectedTime = new DateTime(2019, 6, 25, 10, 57, 15, DateTimeKind.Utc);
            BitMexSymbol symbol = BitMexSymbol.XBTUSD;
            decimal price = 11680;

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "XBTUSD", 1000, "BTC", _BTC(8562000), "BTC", _BTC(5779));
            TransactionDTO expectedTransaction = null;
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _Exchange, "0e40f8a0-c415-58bd-d876-e5837a39bc53", symbol.ToDTO(), MarketType.DerivativesFuturesPerpetual, OrderSide.Buy, price, expectedEvent);
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SellIncreasingPosition()
        {
            RecordGroup group = this.getGroupFromNthRecord(1,
                "a871ce01-6baa-dc0f-a7a2-4142feac9bc5,26/06/2019 8:38:43 PM,Execution,5000,-8250,XBT,Submission from www.bitmex.com,Trade,XRPU19,XBt,Buy,0.00003667,18335000,Limit,,,,", // interested in this one
                "a871ce01-6baa-dc0f-a7a2-4142feac9bc6,26/06/2019 8:39:43 PM,Execution,5000,-8250,XBT,Submission from www.bitmex.com,Trade,XRPU19,XBt,Sell,0.00003667,18335000,Limit,,,,"); // required to close position

            DateTime expectedTime = new DateTime(2019, 6, 26, 8, 38, 43, DateTimeKind.Utc);
            BitMexSymbol symbol = BitMexSymbol.XRPU19;
            decimal price = 0.00003667m;

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", _BTC(18335000), "XRPU19", 5000, "BTC", _BTC(-8250));
            TransactionDTO expectedTransaction = null;
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _Exchange, "a871ce01-6baa-dc0f-a7a2-4142feac9bc5", symbol.ToDTO(), MarketType.DerivativesFutures, OrderSide.Buy, price, expectedEvent);
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void SellDecreasingPosition()
        {
            RecordGroup group = this.getGroupFromNthRecord(4,
                "ed34bc45-5f87-4967-3956-4794feac62fb,26/06/2019 3:41:16 AM,Execution,2500,22325,XBT,Submission from www.bitmex.com,Trade,XRPM19,XBt,Buy,0.00003969,9922500,Market,,,,",
                "bd4f3ec7-78d8-4ebe-668b-625e54af8c41,26/06/2019 3:51:51 AM,Execution,1000,-1777,XBT,Submission from www.bitmex.com,Trade,XRPM19,XBt,Buy,0.0000395,3950000,Limit,,,,",
                "392ae2ba-44df-01cd-d868-d40ed21099c8,26/06/2019 4:03:39 AM,Execution,1000,-1755,XBT,Submission from www.bitmex.com,Trade,XRPM19,XBt,Buy,0.000039,3900000,Limit,,,,",
                "315ca162-867a-c472-3207-078a470d9ccd,26/06/2019 4:54:03 AM,Execution,4500,37989,XBT,Liquidation,Trade,XRPM19,XBt,Sell,0.00003752,-16884000,StopLimit,,,,");

            DateTime expectedTime = new DateTime(2019, 6, 26, 4, 54, 3, DateTimeKind.Utc);
            BitMexSymbol symbol = BitMexSymbol.XRPM19;
            decimal price = 0.00003752m;

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "XRPM19", 4500, "BTC", _BTC(16884000), "BTC", _BTC(37989));
            TransactionDTO expectedTransaction = null;
            TradeDTO expectedTrade = new TradeDTO(expectedTime, _Exchange, "315ca162-867a-c472-3207-078a470d9ccd", symbol.ToDTO(), MarketType.DerivativesFutures, OrderSide.Sell, price, expectedEvent);
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void BitmexSinglePnlTest()
        {
            RecordGroup group = this.getGroupFromNthRecord(1,
                "0bb7978c-a258-fe12-e630-7df93844fbae,24/06/2019 12:00:00 PM,Wallet,8287,0,XBt,,,,,,,,,RealisedPNL,XBTUSD,496eb450-b377-2962-3a78-5780008f0ecb,Completed");

            DateTime expectedTime = new DateTime(2019, 6, 24, 12, 00, 00, DateTimeKind.Utc);

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, null, null, "BTC", _BTC(8287), null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _Exchange, "0bb7978c-a258-fe12-e630-7df93844fbae", null, null, TransactionType.FuturesPnL, expectedEvent);
            TradeDTO expectedTrade = null;
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestMethod]
        public void BitmexMultiplePnlTest()
        {
            RecordGroup group = this.getGroupFromNthRecord(1,
                "27093d92-f796-4f9e-b3b9-e5b066233ab7,6/08/2019 12:00:00 PM,Wallet,3051,0,XBt,,,,,,,,,RealisedPNL,XRPU19,f366b0c7-b7d2-1e21-76ff-f3e5bf525913,Completed",
                "a36aac1e-e727-e592-7abf-3a21153da793,6/08/2019 12:00:00 PM,Wallet,-193991,0,XBt,,,,,,,,,RealisedPNL,XBTUSD,f366b0c7-b7d2-1e21-76ff-f3e5bf525913,Completed",
                "43e17c67-e8f1-1658-5e46-66bbd1f3866e,6/08/2019 12:00:00 PM,Wallet,64415,0,XBt,,,,,,,,,RealisedPNL,TRXU19,f366b0c7-b7d2-1e21-76ff-f3e5bf525913,Completed",
                "cfbc2f82-9ac5-0251-fb0f-42a3b6d6c443,6/08/2019 12:00:00 PM,Wallet,1092,0,XBt,,,,,,,,,RealisedPNL,LTCU19,f366b0c7-b7d2-1e21-76ff-f3e5bf525913,Completed",
                "5f42a89a-7ee9-8c4b-9a7d-aa13419c58db,6/08/2019 12:00:00 PM,Wallet,1718,0,XBt,,,,,,,,,RealisedPNL,ADAU19,f366b0c7-b7d2-1e21-76ff-f3e5bf525913,Completed");

            DateTime expectedTime = new DateTime(2019, 8, 6, 12, 00, 00, DateTimeKind.Utc);

            ContainerEventDTO expectedEvent = new ContainerEventDTO(expectedTime, _Container, "BTC", _BTC(123715), null, null, null, null);
            TransactionDTO expectedTransaction = new TransactionDTO(expectedTime, _Exchange, "27093d92-f796-4f9e-b3b9-e5b066233ab7", null, null, TransactionType.FuturesPnL, expectedEvent);
            TradeDTO expectedTrade = null;
            Helpers.PerformRecordGroupTest(group, expectedTrade, expectedTransaction);
        }

        [TestCategory(Tests.LONG)]
        [TestMethod]
        public void BitMexPnlBookKeeping()
        {
            // expected: sum(deposits) + sum(pnl) = 27 sats
            decimal targetBalance = _BTC(27);

            DString file = new ExchangeDataLocationService("Folder\\").GetDataSources(_Exchange, FinancialYear.Y2018).Single().Value.Single().Single();
            FileService fileService = new FileService(new CryptoService());

            CsvDataReaderService data1 = new CsvDataReaderService(fileService, this._Factory.Object, new CsvInfo(file), new BitMexSchema(), FinancialYear.Y2018, 0);
            CsvDataReaderService data2 = new CsvDataReaderService(fileService, this._Factory.Object, new CsvInfo(file), new BitMexSchema(), FinancialYear.Y2019, 0);

            IEnumerable<TransactionDTO> transactions = data1.GetTransactions().Concat(data2.GetTransactions()).Where(tx => tx.Type == TransactionType.CryptoDeposit || tx.Type == TransactionType.FuturesPnL);
            IEnumerable<ContainerEventDTO> events = transactions.Select(tx => tx.ContainerEvent);

            decimal total = events.Sum(e => (e.AmountReceived ?? 0) - (e.AmountSent ?? 0));
            Assert.AreEqual(targetBalance, total);
        }

        [TestCategory(Tests.LONG)]
        [TestMethod]
        public void BitMexPositionBookKeeping()
        {
            // There are problems when a trade crosses a position direction, as we don't know what the cost should be
            return;

            // started account with 0 XBt, and finished with 27 XBt - go through every record one-by-one and verify that the difference between total incoming and total outgoing funds is indeed that.

            decimal targetBalance = _BTC(27);
            DString file = new ExchangeDataLocationService("Folder\\").GetDataSources(_Exchange, new FinancialYear("2017")).Single().Value.Single().Single();
            FileService fileService = new FileService(new CryptoService());

            CsvDataReaderService data1 = new CsvDataReaderService(fileService, this._Factory.Object, new CsvInfo(file), new BitMexSchema(), new FinancialYear("2018"), 0);
            CsvDataReaderService data2 = new CsvDataReaderService(fileService, this._Factory.Object, new CsvInfo(file), new BitMexSchema(), new FinancialYear("2019"), 0);

            IEnumerable<TradeDTO> trades = data1.GetTrades().Concat(data2.GetTrades());
            IEnumerable<TransactionDTO> transactions = data1.GetTransactions().Concat(data2.GetTransactions());
            IEnumerable<ContainerEventDTO> events = trades.Select(t => t.ContainerEvent).Concat(transactions.Select(tx => tx.ContainerEvent));

            IEnumerable<decimal> totals = events.Select((e, i) =>
            {
                decimal thisSum = 0;
                if (e.CurrencyReceived == "BTC")
                {
                    if (e.AmountReceived.Value <= 0) throw new Exception("Amounts must be positive");
                    thisSum += e.AmountReceived.Value;
                }
                else if (e.CurrencySent == "BTC")
                {
                    if (e.AmountSent.Value <= 0) throw new Exception("Amounts must be positive");
                    thisSum -= e.AmountSent.Value;
                }
                else throw new Exception($"Discrepency at i={i}");

                if (e.FeeCurrency != null)
                {
                    if (e.FeeCurrency == "BTC")
                    {
                        // adds if fee is negative
                        thisSum -= e.FeeAmount.Value;
                    }
                    else throw new Exception($"Discrepency in fee at i={i}");
                }

                return thisSum;
            });

            decimal sum = totals.Sum();
            Assert.AreEqual(targetBalance, sum);
        }

        private BitMexWalletRecord getWalletRecord(DString csvLine)
        {
            return this.getRecord<BitMexWalletRecord>(csvLine);
        }

        private BitMexExecutionRecord getExecutionRecord(DString csvLine)
        {
            return this.getRecord<BitMexExecutionRecord>(csvLine);
        }

        /// <summary>
        /// N is one-based.
        /// </summary>
        private RecordGroup getGroupFromNthRecord(int N, params DString[] csvLines)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { _CSV_HEADER_ROW }.Concat(csvLines);
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            BitMexSchema schema = new BitMexSchema();

            IEnumerable<Record> records = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row));
            return schema.ProcessAll(records).Skip(N - 1).First();
        }

        private R getRecord<R>(DString csvLine) where R : Record
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { _CSV_HEADER_ROW, csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            BitMexSchema schema = new BitMexSchema();

            object[] row = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Single();
            return (R)schema.ProcessOne(row);
        }

        private void performRecordTest<R>(R record, DateTime expectedTime, TradeDTO expectedTrade, TransactionDTO expectedTransaction) where R : Record
        {
            if (record is BitMexExecutionRecord execution && execution.ExecutionType != ExecutionType.Funding) throw new ArgumentException("Trade execution records must be processed using the group API.");
            Assert.AreEqual(expectedTime, (record as BitMexBaseRecord)?.Time);
            Helpers.PerformRecordTest(record, expectedTrade, expectedTransaction);
        }
    }
}
