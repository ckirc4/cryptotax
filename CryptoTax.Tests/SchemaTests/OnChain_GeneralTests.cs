﻿using CryptoTax.DTO;
using CryptoTax.Persistence.Schema;
using CryptoTax.Persistence.Schema.Exchange.OnChain;
using CryptoTax.Persistence.Services.Csv;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;
using UtilityLibrary.Timing;

namespace CryptoTax.Tests.SchemaTests
{
    [TestClass]
    public class OnChain_GeneralTests
    {
        [TestMethod]
        public void BuyOnUniswap_GeneratesCorrectDTOs()
        {
            OnChain_GeneralRecord record = this.getRecord("0xcd9e690761ce46bc4009ad0b93f87c4be4bb929e32c44964f4762062f4d3fb4b,1669866119000,0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45,TradedWithCounterparty,1876.18632057,AMPL,1.56943594891622927,ETH,0.001704724616810268", KnownWallets.ETH_MY_WALLET.Address, BlockchainType.Ethereum);

            DateTime expectedTime = new DateTime().FromUnix(1669866119000);
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, Container.FromWallet(KnownWallets.ETH_MY_WALLET), "ETH", 1.56943594891622927m, "AMPL", 1876.18632057m, "ETH", 0.001704724616810268m);
            TradeDTO expectedTrade = new TradeDTO(expectedTime, Exchange.OnChain, record.TxHash, new SymbolDTO("ETH", "AMPL"), MarketType.Spot, OrderSide.Buy, 1.56943594891622927m / 1876.18632057m, expectedContainerEvent);
            Helpers.PerformRecordTest(record, expectedTrade, null);
        }

        [TestMethod]
        public void SellOnUniswap_GeneratesCorrectDTOs()
        {
            OnChain_GeneralRecord record = this.getRecord("0xf369762a940d5dba5d2f1c324cc8da17bbbc09aaa8418dbae53a41a238b90e4b,1673079983000,0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45,TradedWithCounterparty,5.113003516039936746,ETH,4452.669743418,AMPL,0.001935133147640514", KnownWallets.ETH_MY_WALLET.Address, BlockchainType.Ethereum);

            DateTime expectedTime = new DateTime().FromUnix(1673079983000);
            ContainerEventDTO expectedContainerEvent = new ContainerEventDTO(expectedTime, Container.FromWallet(KnownWallets.ETH_MY_WALLET), "AMPL", 4452.669743418m, "ETH", 5.113003516039936746m, "ETH", 0.001935133147640514m);
            // note: selling on the AMPL-ETH pair at price P is equivalent to buying on the ETH-AMPL pair at price 1/P
            TradeDTO expectedTrade = new TradeDTO(expectedTime, Exchange.OnChain, record.TxHash, new SymbolDTO("AMPL", "ETH"), MarketType.Spot, OrderSide.Buy, 4452.669743418m / 5.113003516039936746m, expectedContainerEvent);
            Helpers.PerformRecordTest(record, expectedTrade, null);
        }

        /// <summary>
        /// TxHash, Time, Counterparty, TransactionType, QuantityReceived, CurrencyReceived, QuantitySent, CurrencySent, Fee
        /// </summary>
        private OnChain_GeneralRecord getRecord(DString csvLine, DString walletAddress, BlockchainType chainType)
        {
            Mock<IFileService> fileService = new Mock<IFileService>(MockBehavior.Strict);
            IEnumerable<DString> contents = new List<DString>() { "TxHash,Time,Counterparty,TransactionType,QuantityReceived,CurrencyReceived,QuantitySent,CurrencySent,Fee", csvLine };
            fileService.Setup(f => f.ReadLines("path")).Returns(contents);

            CsvInfo csv = new CsvInfo("path");
            OnChain_GeneralSchema schema = new OnChain_GeneralSchema(walletAddress, chainType);

            Record record = CsvHelpers.ParseCsv(fileService.Object, csv, schema.Columns).Select(row => schema.ProcessOne(row)).Single();
            return record as OnChain_GeneralRecord;
        }
    }
}
