﻿
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Tests.Helpers.CompareSequences``1(System.Collections.Generic.IEnumerable{``0},System.Collections.Generic.IEnumerable{``0},System.Func{``0,``0,System.Boolean},System.Boolean)~System.Boolean")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0008:Use explicit type", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Tests.Inventory.InventoryDataTests.Intenvory_SimplePurchases_ContainedInInventory(CryptoTax.Domain.CurrencyAmount[],System.Collections.Generic.Dictionary{CryptoTax.Shared.DString,System.Collections.Generic.IEnumerable{CryptoTax.Domain.CurrencyAmount}})")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Style", "IDE0009:Member access should be qualified.", Justification = "<Pending>", Scope = "member", Target = "~M:CryptoTax.Tests.Inventory.InventoryDataTests.Intenvory_SimplePurchases_ContainedInInventory(CryptoTax.Domain.CurrencyAmount[],System.Collections.Generic.Dictionary{CryptoTax.Shared.DString,System.Collections.Generic.IEnumerable{CryptoTax.Domain.CurrencyAmount}})")]