﻿using CryptoTax.Shared.Enums;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    public struct Currency
    {
        public NString Name { get; }
        public DString Symbol { get; }
        public bool IsFiat { get; }
        public BlockchainType Blockchain { get; }

        public Currency(NString name, DString symbol)
        {
            this.Name = name;
            this.Symbol = symbol;
            this.IsFiat = false;
            this.Blockchain = BlockchainType.Undefined;
        }

        public Currency(NString name, DString symbol, bool isFiat) : this(name, symbol)
        {
            this.IsFiat = isFiat;
        }

        public Currency(NString name, DString symbol, bool isFiat, BlockchainType blockchain) : this(name, symbol, isFiat)
        {
            this.Blockchain = blockchain;
        }

        public static Currency FromSerialised(string name, string symbol, bool isFiat, BlockchainType blockchain)
        {
            return new Currency(name, symbol, isFiat, blockchain);
        }

        public override bool Equals(object obj)
        {
            if (obj is Currency other)
            {
                // we don't care if the name or isFiat are the same, since they do not define this Currency
                return this.Symbol == other.Symbol;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return this.Symbol.GetHashCode();
        }

        public override string ToString()
        {
            return this.Symbol;
        }

        public static bool operator ==(Currency first, Currency second)
        {
            return first.Equals(second);
        }
        public static bool operator !=(Currency first, Currency second)
        {
            return !(first == second);
        }
    }
}

