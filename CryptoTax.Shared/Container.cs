﻿using CryptoTax.Shared.Enums;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    /// <summary>
    /// Represents a place in which funds can be stored or sent to.
    /// </summary>
    public struct Container
    {
        /// <summary>
        /// A non-unique identifier that can be used to group together transactions to infer inventories.
        /// E.g. BinanceSpot
        /// </summary>
        public DString Id { get; private set; }
        public ContainerType Type { get; private set; }
        /// <summary>
        /// Defined if ContainerType is any type of wallet (including ExchangeWallet)
        /// </summary>
        public Wallet? Wallet { get; private set; }
        /// <summary>
        /// Defined if ContainerType is ExchangeAccount (NOT if ContainerType is ExchangeWallet, since the exchange wallet and user account may be separate containers)
        /// </summary>
        public Exchange? Exchange { get; private set; }

        private Container(DString id, ContainerType type, Wallet? wallet, Exchange? exchange)
        {
            this.Id = id;
            this.Type = type;
            this.Wallet = wallet;
            this.Exchange = exchange;
        }

        public static Container FromWallet(DString walletAddress, BlockchainType? chainType = null)
        {
            Wallet wallet = KnownWallets.GetWallet(walletAddress, chainType) ?? new Wallet(walletAddress);
            return FromWallet(wallet);
        }

        public static Container FromWallet(Wallet wallet)
        {
            string id = $"Wallet {wallet.Address}" + (wallet.Name.HasValue ? $" ({wallet.Name})" : "");
            ContainerType type;
            if (wallet.HasControl && wallet.ExchangeOwner == null) type = ContainerType.OwnedWallet;
            else if (wallet.ExchangeOwner != null && wallet.ExchangeOwner != Enums.Exchange.Undefined) type = ContainerType.ExchangeWallet;
            else type = ContainerType.ThirdPartyWallet;

            return new Container(id, type, wallet, null);
        }

        public static Container FromExchange(Exchange exchange)
        {
            return new Container(exchange.ToString(), ContainerType.ExchangeAccount, null, exchange);
        }

        public static Container FromBankAccount()
        {
            return new Container("Bank Account", ContainerType.BankAccount, null, null);
        }

        public static Container FromSerialised(string id, ContainerType type, Wallet? wallet, Exchange? exchange)
        {
            return new Container(id, type, wallet, exchange);
        }

        // .Equals() for structs compares all properties using .Equals(), which is safe if all properties are value-type.
        // https://docs.microsoft.com/en-us/dotnet/api/system.valuetype.equals?view=net-5.0
        public static bool operator ==(Container c1, Container c2)
        {
            return c1.Equals(c2);
        }
        public static bool operator !=(Container c1, Container c2)
        {
            return !c1.Equals(c2);
        }

        public override bool Equals(object obj)
        {
            if (obj is Container other)
            {
                return this.Id == other.Id
                    && this.Type == other.Type
                    && this.Wallet == other.Wallet
                    && this.Exchange == other.Exchange;
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return new
            {
                this.Id,
                this.Type,
                this.Wallet,
                this.Exchange
            }.GetHashCode();
        }
    }
}
