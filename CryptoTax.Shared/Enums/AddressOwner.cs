﻿namespace CryptoTax.Shared.Enums
{
    public enum AddressOwner
    {
        Private,
        Exchange
    }
}
