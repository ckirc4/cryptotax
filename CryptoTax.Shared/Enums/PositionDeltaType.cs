﻿namespace CryptoTax.Shared.Enums
{
    public enum PositionDeltaType
    {
        /// <summary>
        /// Long or short position is increasing.
        /// </summary>
        Increasing,
        /// <summary>
        /// Long or short position is decreasing.
        /// </summary>
        Decreasing
    }
}
