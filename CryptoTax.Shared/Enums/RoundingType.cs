﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Enums
{
    public enum RoundingType
    {
        [ArgEnumValue("none", "no")]
        None,

        [ArgEnumValue("nearest", "default")]
        Nearest,

        [ArgEnumValue("up", "+")]
        Up,

        [ArgEnumValue("down", "-")]
        Down,

        [ArgEnumValue("minimiseGains")]
        MinimiseGains,

        [ArgEnumValue("maximiseGains")]
        MaximiseGains
    }
}
