﻿namespace CryptoTax.Shared.Enums
{
    public enum TransactionType
    {
        FuturesFunding,
        /// <summary>
        /// Note: for FTX, reported PNL are actually PNL + UPNL for the day, so an inventory may be overdrawn... we will have to live with that if we don't want to calculate our own PNL (and we don't want to do that).
        /// </summary>
        FuturesPnL,
        OptionsPnL,
        StakingOrAirdrops,
        Fork,
        LoanInterest,
        DustConversion,
        /// <summary>
        /// AUD deposit into exchange account
        /// </summary>
        FiatDeposit,
        /// <summary>
        /// AUD withdrawal into bank account
        /// </summary>
        FiatWithdrawal,
        CryptoDeposit,
        CryptoWithdrawal,
        OnChainTransfer
    }
}
