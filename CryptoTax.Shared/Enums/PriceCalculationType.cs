﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Enums
{
    public enum PriceCalculationType
    {
        [ArgEnumValue("open", "o")]
        Open,

        [ArgEnumValue("close", "c")]
        Close,

        /// <summary>
        /// Price is (open + close) / 2
        /// </summary>
        [ArgEnumValue("arithmeticMean", "mean", "avg")]
        ArithmeticMean,

        /// <summary>
        /// Price is fractionOfDesiredTimeWithinCandlestickInterval * (close - open) + open
        /// </summary>
        [ArgEnumValue("timeWeightedAverage", "twa")]
        TimeWeightedAverage
    }
}
