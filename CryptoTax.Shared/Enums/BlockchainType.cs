﻿namespace CryptoTax.Shared.Enums
{
    public enum BlockchainType
    {
        Bitcoin,
        /// <summary>
        /// For all ERC tokens
        /// </summary>
        Ethereum,
        EthereumClassic,
        /// <summary>
        /// Ethereum testnet
        /// </summary>
        Rinkeby,
        XRPL,
        USDT,
        BinanceChain,
        Undefined,
        ArbitrumNova,
    }
}
