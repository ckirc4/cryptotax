﻿namespace CryptoTax.Shared.Enums
{
    public enum ContainerType
    {
        OwnedWallet,
        /// <summary>
        /// A wallet for which I do not control the keys (i.e. funds are gone)
        /// </summary>
        ThirdPartyWallet,
        /// <summary>
        /// A wallet associated with an exchange. This may be the incoming or outgoing address, and may not be directly linked to an exchange account.
        /// </summary>
        ExchangeWallet,
        BankAccount,
        ExchangeAccount
    }
}
