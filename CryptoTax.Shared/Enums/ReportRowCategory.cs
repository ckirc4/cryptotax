﻿namespace CryptoTax.Shared.Enums
{
    public enum ReportRowCategory
    {
        /// <summary>
        /// Event initiated as a result (direct or indirect) of a user, not generated internally in this application.
        /// </summary>
        Action,
        /// <summary>
        /// State management updates within our application
        /// </summary>
        State,
        /// <summary>
        /// Something outside of our control
        /// </summary>
        Environment,
        /// <summary>
        /// Administrative messages that may not represent any physical event
        /// </summary>
        Admin
    }
}
