﻿namespace CryptoTax.Shared.Enums
{
    public enum ReportRowType
    {
        #region State-type - handled by inventories
        /// <summary>
        /// CurrencyAmount is split [State]
        /// </summary>
        Split,
        /// <summary>
        /// CurrencyAmounts are merged [State]
        /// </summary>
        Merge,
        /// <summary>
        /// CurrencyAmount added to inventory [State]
        /// </summary>
        Add,
        /// <summary>
        /// CurrencyAmount removed from inventory [State]
        /// </summary>
        Remove,
        #endregion

        #region Action-type - not handled by inventories
        /// <summary>
        /// CurrencyAmount purchased [Action]
        /// </summary>
        Purchase,
        /// <summary>
        /// CurrencyAmount disposed [Action]
        /// </summary>
        Dispose,
        /// <summary>
        /// CurrencyAmount transferred [Action]
        /// </summary>
        Transfer,
        /// <summary>
        /// CurrencyAmount is converted to another currency [Action]
        /// </summary>
        Convert,
        /// <summary>
        /// CurrencyAmount is created out of thin air [Action]
        /// </summary>
        Create,
        /// <summary>
        /// CurrencyAmount is destroyed/deleted [Action]
        /// </summary>
        Burn,
        #endregion

        #region Environment-type
        /// <summary>
        /// One or more prices of an exchange symbol [Environment]
        /// </summary>
        Price,
        #endregion

        #region Admin-type
        /// <summary>
        /// [Admin]
        /// </summary>
        StartRun,
        /// <summary>
        /// [Admin]
        /// </summary>
        EndRun,
        /// <summary>
        /// [Admin]
        /// </summary>
        LoadInventories
        #endregion
    }
}
