﻿namespace CryptoTax.Shared.Enums
{
    public enum OrderSide
    {
        Buy,
        Sell
    }
}
