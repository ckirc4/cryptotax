﻿namespace CryptoTax.Shared.Enums
{
    public enum Exchange
    {
        Binance,
        FTX,
        MercatoX,
        BitMex,
        BTCMarkets,
        OnChain,
        Undefined,
        Kraken,
    }
}
