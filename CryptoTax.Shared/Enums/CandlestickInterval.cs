﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Enums
{
    // any other intervals are not supported by BTCMarkets (e.g. see https://api.btcmarkets.net/v3/markets/BTC-AUD/candles?timeWindow=15m)
    public enum CandlestickInterval
    {
        [ArgEnumValue("minute_1", "minute1", "min_1", "min1", "m_1", "m1", "1_minute", "1minute", "1_min", "1min", "1_m", "1m")]
        Minute_1,
        [ArgEnumValue("hour_1", "hour1", "hr_1", "hr1", "h_1", "h1", "1_hour", "1hour", "1_hr", "1hr", "1_h", "1h")]
        Hour_1,
        [ArgEnumValue("day_1", "day1", "d_1", "d1", "1_day", "1day", "1_d", "1d")]
        Day_1
    }
}
