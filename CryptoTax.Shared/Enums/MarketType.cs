﻿namespace CryptoTax.Shared.Enums
{
    public enum MarketType
    {
        /// <summary>
        /// E.g. BTCUSDT
        /// </summary>
        Spot,
        /// <summary>
        /// E.g. BTC 09-26
        /// </summary>
        Futures,
        /// <summary>
        /// E.g. BTC-PERP
        /// </summary>
        DerivativesFuturesPerpetual,
        /// <summary>
        /// E.g. SHIT-PERP
        /// </summary>
        DerivativesFutures,
        /// <summary>
        /// E.g. XRPBULL
        /// </summary>
        DerivativesSpot
    }
}
