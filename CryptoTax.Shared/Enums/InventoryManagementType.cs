﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Enums
{
    public enum InventoryManagementType
    {
        [ArgEnumValue("single", "one")]
        Single,

        [ArgEnumValue("onePerExchangeType", "exchange")]
        OnePerExchangeType
    }
}
