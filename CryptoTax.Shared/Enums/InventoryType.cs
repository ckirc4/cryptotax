﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Enums
{
    public enum InventoryType
    {
        /// <summary>
        /// First in, last out (aka last in, first out)
        /// </summary>
        [ArgEnumValue("filo", "lifo")]
        FILO,

        /// <summary>
        /// First in, first out
        /// </summary>
        [ArgEnumValue("fifo")]
        FIFO,

        /// <summary>
        /// Chooses the parcel with the lowest unit cost.
        /// </summary>
        [ArgEnumValue("min", "minimum")]
        Min,

        /// <summary>
        /// Chooses the parcel with the highest unit cost.
        /// </summary>
        [ArgEnumValue("max", "maximum")]
        Max,

        /// <summary>
        /// Chooses the parcel with the oldest creation time.
        /// </summary>
        [ArgEnumValue("oldest", "old")]
        Oldest,

        /// <summary>
        /// Chooses the parcel with the most recent creation time.
        /// </summary>
        [ArgEnumValue("newest", "new")]
        Newest,

        /// <summary>
        /// Averages all parcels into a single one.<br/><br/>
        /// Note: Generally, averaging is not an acceptable method of determining the cost base of a parcel (because it gives the taxpayer too much of an advantage, it turns out!).
        /// <see cref="https://archive.is/wip/AceR2"/>
        /// </summary>
        [ArgEnumValue("average", "avg")]
        Average,

        [ArgEnumValue("mockImmutable")]
        MockImmutable,
        [ArgEnumValue("mockTracking")]
        MockTracking
    }
}
