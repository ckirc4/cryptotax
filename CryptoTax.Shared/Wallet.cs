﻿using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    public struct Wallet
    {
        public DString Address { get; }
        public IEnumerable<Currency> CurrenciesAccepted { get; }
        /// <summary>
        /// If null, is privately owned
        /// </summary>
        public Exchange? ExchangeOwner { get; }
        /// <summary>
        /// Whether I have control over the wallet, i.e. whether I own the private key (can never be true if Owner is not null)
        /// </summary>
        public bool HasControl { get; }
        public BlockchainType Blockchain { get; }
        public NString Name { get; }
        public NString Description { get; }

        public Wallet(DString address)
        {
            this.Address = address;
            this.CurrenciesAccepted = new List<Currency>();
            this.ExchangeOwner = null;
            this.HasControl = false;
            this.Blockchain = BlockchainType.Undefined;
            this.Name = null;
            this.Description = null;
        }

        public Wallet(DString address, IEnumerable<Currency> currenciesAccepted, Exchange? exchangeOwner, bool hasControl, BlockchainType blockchain) : this(address)
        {
            if (exchangeOwner != null && hasControl)
            {
                throw new ArgumentException("Cannot have control of an exchange wallet");
            }

            this.CurrenciesAccepted = currenciesAccepted;
            this.ExchangeOwner = exchangeOwner;
            this.HasControl = hasControl;
            this.Blockchain = blockchain;
        }

        public Wallet(DString address, IEnumerable<Currency> currenciesAccepted, Exchange? exchangeOwner, bool hasControl, BlockchainType blockchain, NString name) : this(address, currenciesAccepted, exchangeOwner, hasControl, blockchain)
        {
            this.Name = name;
        }

        public Wallet(DString address, IEnumerable<Currency> currenciesAccepted, Exchange? exchangeOwner, bool hasControl, BlockchainType blockchain, NString name, NString description) : this(address, currenciesAccepted, exchangeOwner, hasControl, blockchain, name)
        {
            this.Description = description;
        }

        public static Wallet FromSerialised(string address, IEnumerable<Currency> currenciesAccepted, Exchange? exchangeOwner, bool hasControl, BlockchainType blockchain, string name, string description)
        {
            return new Wallet(address, currenciesAccepted, exchangeOwner, hasControl, blockchain, name, description);
        }

        public static bool operator ==(Wallet first, Wallet second)
        {
            return first.Address == second.Address
                && first.CurrenciesAccepted.Select((c, i) => c == second.CurrenciesAccepted.ElementAt(i)).All(r => r)
                && first.ExchangeOwner == second.ExchangeOwner
                && first.HasControl == second.HasControl
                && first.Blockchain == second.Blockchain
                && first.Name == second.Name
                && first.Description == second.Description;
        }

        public static bool operator !=(Wallet first, Wallet second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (obj is Wallet wallet) return this == wallet;
            else return false;
        }

        public override int GetHashCode()
        {
            return new {
                this.Address,
                this.CurrenciesAccepted,
                this.ExchangeOwner,
                this.HasControl,
                this.Blockchain,
                this.Name,
                this.Description
            }.GetHashCode();
        }
    }
}
