﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    public struct ExchangeSymbol
    {
        public GenericSymbol Symbol { get; set; }
        /// <summary>
        /// The name (id) used by the API
        /// </summary>
        public DString Name { get; set; }
        public Exchange Exchange { get; set; }

        /// <summary>
        /// Will try to infer symbol name.
        /// </summary>
        public ExchangeSymbol(GenericSymbol symbol, Exchange exchange)
        {
            this.Symbol = symbol;
            this.Exchange = exchange;
            this.Name = TryInferName(symbol, exchange);
        }
        public ExchangeSymbol(GenericSymbol symbol, Exchange exchange, DString name)
        {
            this.Symbol = symbol;
            this.Exchange = exchange;
            this.Name = name;
        }

        public ExchangeSymbol(DString quoteCurrency, DString baseCurrency, Exchange exchange)
        {
            this.Symbol = new GenericSymbol(
                KnownCurrencies.TryGetCurrency(quoteCurrency),
                KnownCurrencies.TryGetCurrency(baseCurrency));
            this.Exchange = exchange;
            this.Name = TryInferName(this.Symbol, exchange);
        }

        public static DString TryInferName(GenericSymbol symbol, Exchange exchange)
        {
            NString name = new NString(null);
            switch (exchange)
            {
                case Exchange.Binance:
                    name = $"{symbol.BaseCurrency.Symbol}{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.FTX:
                    // name of futures is contract name, name of spot is BASE/QUOTE
                    name = symbol.BaseCurrency.Symbol.Value.Contains("-") ? symbol.BaseCurrency.Symbol.Value : $"{symbol.BaseCurrency.Symbol}/{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.MercatoX:
                    name = $"{symbol.BaseCurrency.Symbol}_{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.BitMex:
                    name = symbol.BaseCurrency.Symbol;
                    break;
                case Exchange.BTCMarkets:
                    name = $"{symbol.BaseCurrency.Symbol}-{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.OnChain:
                    name = $"{symbol.BaseCurrency.Symbol}/{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.Kraken:
                    // this is not strictly true, because Kraken prefixes some cryptos with X and some fiats with Z, but it seems that we can use the API (at least the Trade API) using this name
                    name = $"{symbol.BaseCurrency.Symbol}{symbol.QuoteCurrency.Symbol}";
                    break;
                case Exchange.Undefined:
                    name = $"{symbol.BaseCurrency.Symbol}/{symbol.QuoteCurrency.Symbol}";
                    break;
                default:
                    throw new AssertUnreachable(exchange);
            }

            if (name == null) throw new Exception("Cannot infer symbol name.");
            return (DString)name;
        }

        public static bool operator ==(ExchangeSymbol first, ExchangeSymbol second)
        {
            return first.Equals(second);
        }
        public static bool operator !=(ExchangeSymbol first, ExchangeSymbol second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (obj is ExchangeSymbol other)
            {
                return this.Symbol == other.Symbol
                    && this.Exchange == other.Exchange;
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return this.Symbol.GetHashCode() + this.Exchange.GetHashCode();
        }
    }
}
