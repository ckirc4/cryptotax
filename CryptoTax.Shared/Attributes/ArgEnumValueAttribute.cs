﻿using System;
using System.Linq;

namespace CryptoTax.Shared.Attributes
{
    /// <summary>
    /// Defines one or more arg values that an enum value, used for a property in RunInfo, belongs to (not case sensitive).
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class ArgEnumValueAttribute : Attribute
    {
        public string[] Names { get; }

        public ArgEnumValueAttribute(string name, params string[] otherNames)
        {
            // the constructor signature ensures that at least one name is provided
            this.Names = new[] { name }.Concat(otherNames).Select(n => n.ToLower()).ToArray();
        }
    }
}
