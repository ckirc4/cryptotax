﻿using System;
using System.Linq;

namespace CryptoTax.Shared.Attributes
{
    /// <summary>
    /// Defines one or more arg keys that a property in RunInfo belongs to (not case sensitive).
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ArgKeyAttribute : Attribute
    {
        public string[] Names { get; }

        public ArgKeyAttribute(string name, params string[] otherNames)
        {
            // the constructor signature ensures that at least one name is provided
            this.Names = new[] { name }.Concat(otherNames).Select(n => n.ToLower()).ToArray();
        }
    }
}
