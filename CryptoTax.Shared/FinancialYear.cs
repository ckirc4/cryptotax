﻿using System;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    /// <summary>
    /// All times are in the local time zone.
    /// </summary>
    public struct FinancialYear
    {
        private static FinancialYear _DEFAULT = new FinancialYear(2016);

        private DString _Name;
        private DateTime _Start;
        private DateTime _End;

        /// <summary>
        /// E.g. 2020-2021
        /// </summary>
        public DString Name
        {
            get { this.fixDefault(); return this._Name; }
            set { this.fixDefault(); this._Name = value; }
        }

        public DateTime Start
        {
            get { this.fixDefault(); return this._Start; }
            set { this.fixDefault(); this._Start = value; }
        }
        public DateTime End
        {
            get { this.fixDefault(); return this._End; }
            set { this.fixDefault(); this._End = value; }
        }

        /// <summary>
        /// Gets the financial year that the given date is part of.
        /// </summary>
        public FinancialYear(DateTime date) : this(getStartYear(date))
        { }

        /// <summary>
        /// Gets the financial year that begins in the given calendar year.
        /// </summary>
        public FinancialYear(int startYear) : this()
        {
            this.constructor(startYear);
        }

        /// <summary>
        /// Expected in the form "2020-2021" or "2020" (start year)
        /// </summary>
        public FinancialYear(DString str) : this()
        {
            bool isNumber(string s) { return s.All(c => char.IsDigit(c)); }

            string input = str;
            bool error = false;

            if (input.Length == 4 && isNumber(input)) this.constructor(int.Parse(input));
            else if (input.Length == 9 && input[4] == '-' && isNumber(input.Remove(4, 1)))
            {
                int start = int.Parse(input.Substring(0, 4));
                int end = int.Parse(input.Substring(5, 4));

                if (end - start != 1) error = true;
                else this.constructor(start);
            }
            else error = true;

            if (error) throw new ArgumentException($"Cannot construct FinancialYear from input '{str}'");
        }

        #region operators

        public static FinancialYear operator ++(FinancialYear financialYear)
        {
            return financialYear + 1;
        }
        public static FinancialYear operator +(FinancialYear financialYear, int value)
        {
            return new FinancialYear(financialYear._Start.AddYears(value));
        }
        public static FinancialYear operator -(FinancialYear financialYear, int value)
        {
            return new FinancialYear(financialYear._Start.AddYears(-value));
        }

        public static bool operator <(FinancialYear first, FinancialYear second)
        {
            return first._Start < second._Start;
        }

        public static bool operator >(FinancialYear first, FinancialYear second)
        {
            return first._Start > second._Start;
        }

        public static bool operator ==(FinancialYear first, FinancialYear second)
        {
            return first._Start == second._Start;
        }

        public static bool operator !=(FinancialYear first, FinancialYear second)
        {
            return first._Start != second._Start;
        }

        public static bool operator >=(FinancialYear first, FinancialYear second)
        {
            return first > second || first == second;
        }

        public static bool operator <=(FinancialYear first, FinancialYear second)
        {
            return first < second || first == second;
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj is FinancialYear fy) return this == fy;
            else return false;
        }

        public override int GetHashCode()
        {
            return this._Start.GetHashCode();
        }

        /// <summary>
        /// The financial year 2016-2017.
        /// </summary>
        public static FinancialYear Y2016 = new FinancialYear(2016);
        /// <summary>
        /// The financial year 2017-2018.
        /// </summary>
        public static FinancialYear Y2017 = new FinancialYear(2017);
        /// <summary>
        /// The financial year 2018-2019.
        /// </summary>
        public static FinancialYear Y2018 = new FinancialYear(2018);
        /// <summary>
        /// The financial year 2019-2020.
        /// </summary>
        public static FinancialYear Y2019 = new FinancialYear(2019);
        /// <summary>
        /// The financial year 2020-2021.
        /// </summary>
        public static FinancialYear Y2020 = new FinancialYear(2020);
        /// <summary>
        /// The financial year 2021-2022.
        /// </summary>
        public static FinancialYear Y2021 = new FinancialYear(2021);
        /// <summary>
        /// The financial year 2022-2023.
        /// </summary>
        public static FinancialYear Y2022 = new FinancialYear(2022);
        /// <summary>
        /// The financial year 2023-2024.
        /// </summary>
        public static FinancialYear Y2023 = new FinancialYear(2023);

        /// <summary>
        /// Hacky way of reusing the constructor logic. All official constructors must call : this() before they are able to use this method.
        /// </summary>
        private void constructor(int startYear)
        {
            this._Name = $"{startYear}-{startYear + 1}";
            DateTime start = getStartDate(startYear);
            this._Start = start;
            this._End = start.AddYears(1).Subtract(new TimeSpan(1));
        }

        private static DateTime getStartDate(int startYear)
        {
            return new DateTime(startYear, 7, 1);
        }

        private static int getStartYear(DateTime date)
        {
            int year = date.Year;
            return date.Month < 7 ? year - 1 : year;
        }

        /// <summary>
        /// If `this` is the c# `default`, initialise all fields to a sensible default value. This is equivalent to implementing an empty constructor (which is not allowed for structs, and I really don't want to make this a class just for that).
        /// </summary>
        private void fixDefault()
        {
            if (this == default)
            {
                this = _DEFAULT;
            }
        }
    }
}
