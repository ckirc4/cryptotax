﻿namespace CryptoTax.Shared
{
    /// <summary>
    /// Represents an exchange-independent trading pair
    /// </summary>
    public struct GenericSymbol
    {
        public Currency QuoteCurrency { get; set; }
        public Currency BaseCurrency { get; set; }

        public GenericSymbol(Currency quoteCurrency, Currency baseCurrency)
        {
            this.QuoteCurrency = quoteCurrency;
            this.BaseCurrency = baseCurrency;
        }

        public static bool operator ==(GenericSymbol first, GenericSymbol second)
        {
            return first.Equals(second);
        }
        public static bool operator !=(GenericSymbol first, GenericSymbol second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (obj is GenericSymbol other)
            {
                return this.BaseCurrency.Equals(other.BaseCurrency)
                    && this.QuoteCurrency.Equals(other.QuoteCurrency);
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return new { this.QuoteCurrency, this.BaseCurrency }.GetHashCode();
        }

        public override string ToString()
        {
            return $"{this.BaseCurrency}{this.QuoteCurrency}";
        }
    }
}
