﻿using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    // ideally we can one day create a peristing lookup and external editor for these.
    public static class KnownWallets
    {
        public static readonly Wallet ETH_MY_WALLET_OLD = new Wallet("0x7042db4c642f6ac868df6717b65be6d4590ea720", KnownCurrencies.ERC20, null, true, BlockchainType.Ethereum, "DDEX Browser Wallet");
        public static readonly Wallet ETH_MY_WALLET = new Wallet("0xa5478Cab569FEE520a63D6B43E5faef45ff3EF99".ToLower(), KnownCurrencies.ERC20, null, true, BlockchainType.Ethereum, "Metamask Account 3");
        public static readonly Wallet HOP_PROTOCOL_ETH_BRIDGE_MAINNET = new Wallet("0xb8901acb165ed027e32754e0ffe830802919727f", new Currency[] { KnownCurrencies.ETH }, null, false, BlockchainType.Ethereum, "Hop Protocol: Ethereum Bridge", "ETH sent from ArbNova to Mainnet via Hop will be paid out from this address.");
        public static readonly Wallet HOP_PROTOCOL_ETH_BRIDGE_ARB_NOVA = new Wallet("0xd6bfb71b5ad5fd378cac15c72d8652e3b8d542c4", new Currency[] { KnownCurrencies.ETH }, null, false, BlockchainType.ArbitrumNova, "Hop Protocol: Ethereum Bridge", "ETH is sent to this Hop Protocol address from the ArbNova side to convert it mainnet ETH.");
        public static readonly Wallet ETH_MY_WALLET_ARB_NOVA = new Wallet("0xa5478Cab569FEE520a63D6B43E5faef45ff3EF99".ToLower(), new Currency[] { KnownCurrencies.ETH_ARB_NOVA, KnownCurrencies.MOON }, null, true, BlockchainType.ArbitrumNova, "Metamask Account 3");
        public static readonly Wallet ETH_REDDIT_WALLET_ARB_NOVA = new Wallet("0xe5dd240231608367fb2d61d5348693a7cbf5396e".ToLower(), new Currency[] { KnownCurrencies.ETH_ARB_NOVA, KnownCurrencies.MOON }, null, true, BlockchainType.ArbitrumNova, "Reddit Vault");
        public static readonly Wallet ETH_MAIN_ARB_NOVA_BRIDGE_CONTRACT = new Wallet("0xc4448b71118c9071bcb9734a0eac55d18a153949", new Currency[] { KnownCurrencies.ETH }, null, true, BlockchainType.Ethereum, "ETH sent to this contract from the mainnet is converted to ETH on the arbitrum nova network at a rate of 1-to-1. Technically, the actual bridge contract is at 0xC1Ebd02f738644983b6C4B2d440b8e77DdE276Bd, but the current contract (named \"Arbitrum Nova: Delayed Inbox\" on Etherscan) internally transfers any received funds to the bridge.");
        public static readonly Wallet ETH_ARB_NOVA_MAIN_BRIDGE_CONTRACT = new Wallet("0xb6588cab569fee520a63d6b43e5faef45ff400aa".ToLower(), new Currency[] { KnownCurrencies.ETH_ARB_NOVA }, null, true, BlockchainType.ArbitrumNova, "ETH is sent from this (personal?) contract to my arbitrum nova ETH address.");
        public static readonly Wallet ETH_ARB_NOVA_SUSHISWAP_ROUTER = new Wallet("0x1b02da8cb0d097eb8d57a175b88c7d8b47997506", new Currency[] { KnownCurrencies.ETH_ARB_NOVA }, null, true, BlockchainType.ArbitrumNova);
        public static readonly Wallet ETH_ARB_NOVA_SUSHISWAP_MOON_POOL = new Wallet("0xd6c821b282531868721b41badca1f1ce471f43c5", new Currency[] { KnownCurrencies.ETH_ARB_NOVA, KnownCurrencies.MOON }, null, true, BlockchainType.ArbitrumNova);
        public static readonly Wallet ETH_WRAPPED_ETHER_CONTRACT = new Wallet("0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2", new Currency[] { KnownCurrencies.ETH, KnownCurrencies.WETH }, null, false, BlockchainType.Ethereum, "ETH sent to this contract is converted to WETH at a rate of 1-to-1");
        public static readonly Wallet ETH_BURN_ADDRESS = new Wallet("0x0000000000000000000000000000000000000000", new Currency[] { KnownCurrencies.ETH, KnownCurrencies.BOMB }, null, false, BlockchainType.Ethereum, "Burn Address");
        public static readonly Wallet ETH_DDEX_EXCHANGE_WALLET = new Wallet("0x49497a4d914ae91d34ce80030fe620687bf333fd", new Currency[] { KnownCurrencies.WETH }, Exchange.OnChain, false, BlockchainType.Ethereum, "DDEX 1.0 (Hydro Protocol)", "Receives WETH fees after a trade on DDEX");
        public static readonly Wallet ETH_UNISWAP_V2_ETH_IN = new Wallet("0x7a250d5630b4cf539739df2c5dacb4c659f2488d", new Currency[] { KnownCurrencies.WETH }, Exchange.OnChain, false, BlockchainType.Ethereum, "Uniswap V2: Router 2", "Receives ETH when purchasing an asset.");
        public static readonly Wallet ETH_UNISWAP_V2_MBT_OUT = new Wallet("0xba58ab283a9c30f7229f7c2cfb11444b61945466", new Currency[] { KnownCurrencies.MBT }, Exchange.OnChain, false, BlockchainType.Ethereum, "Uniswap V2: MBT 8", "Sends MBT (Microbit) when it is purchased.");
        public static readonly Wallet ETH_UNISWAP_V2_AMPL_POOL = new Wallet("0xc5be99a02c6857f9eac67bbce58df5572498f40c", new Currency[] { KnownCurrencies.AMPL }, Exchange.OnChain, false, BlockchainType.Ethereum, "Uniswap V2: AMPL", "Purchased AMPL is sent from this address, and bought AMPL is sent to this address.");
        public static readonly Wallet ETH_UNISWAP_V2_ETH_ROUTER = new Wallet("0x3fc91a3afd70395cd496c647d5a6cc9d4b2b7fad", new Currency[] { KnownCurrencies.ETH }, Exchange.OnChain, false, BlockchainType.Ethereum, "Uniswap V2 router that receives/sends the ETH when making a swap.");
        public static readonly Wallet ETH_UNISWAP_V3_ETH_ROUTER = new Wallet("0x68b3465833fb72a70ecdf485e0e4c7bd8665fc45", new Currency[] { KnownCurrencies.ETH }, Exchange.OnChain, false, BlockchainType.Ethereum, "Uniswap router that receives/sends the ETH when making a swap.");
        public static readonly Wallet ETH_UNISWAP_V3_ETH_ROUTER_2 = new Wallet("0xef1c6e67703c7bd7107eed8303fbe6ec2554bf6b", new Currency[] { KnownCurrencies.ETH }, Exchange.OnChain, false, BlockchainType.Ethereum, "Alternate uniswap router that receives/sends the ETH when making a swap.");
        public static readonly Wallet ETH_MY_MERCATOX_DEPOSIT = new Wallet("0xf0c63d3b20dea654d66ee4eac8a52e8f9580f28e", new Currency[] { KnownCurrencies.ETH, KnownCurrencies.BOMB }, Exchange.MercatoX, false, BlockchainType.Ethereum, "My MercatoX ERC20 Deposit Address", "Once MercatoX has received funds here, it will transfer them to its official ERC20 wallet. Note that the transaction hash shown in the MercatoX transaction history is for the original transaction to this address, NOT from this address to the official wallet.");
        public static readonly Wallet ETH_MERCATOX_OFFICIAL_DEPOSIT = new Wallet("0xe03c23519e18d64f144d2800e30e81b0065c48b5", new[] { KnownCurrencies.ETH, KnownCurrencies.BOMB }, Exchange.MercatoX, false, BlockchainType.Ethereum, "MercatoX Official ERC20 Wallet");
        public static readonly Wallet ETH_BINANCE_WITHDRAW = new Wallet("0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be", KnownCurrencies.ERC20, Exchange.Binance, false, BlockchainType.Ethereum, "Binance Wallet (Outgoing)");
        public static readonly Wallet ETH_FTX_PRIVATE = new Wallet("0xb1dee096ff2e5379b020bceb374d19b6a21d5670", new[] { KnownCurrencies.ETH }, Exchange.FTX, false, BlockchainType.Ethereum, "FTX private deposit address", "When depositing ETH on FTX, will send to this wallet, and behind the scenes FTX will transfer it to their own wallet.");
        public static readonly Wallet ETH_BTCMARKETS = new Wallet("0xc55eddadeeb47fcde0b3b6f25bd47d745ba7e7fa", new[] { KnownCurrencies.ETH }, Exchange.BTCMarkets, false, BlockchainType.Ethereum);
        public static readonly Wallet ETH_KRAKEN_DEPOSIT_1 = new Wallet("0xdd4c883739584ed4ea9795ff71b6ddacbfc83d5b", new[] { KnownCurrencies.ETH }, Exchange.Kraken, false, BlockchainType.Ethereum);
        public static readonly Wallet ETH_KRAKEN_DEPOSIT_2 = new Wallet("0x33b6bf4c8509b4c8a2b1eb39883aeb923a877d90", new[] { KnownCurrencies.ETH }, Exchange.Kraken, false, BlockchainType.Ethereum);
        public static readonly Wallet ETH_ARB_NOVA_KRAKEN_DEPOSIT = new Wallet("0xdd4c883739584ed4ea9795ff71b6ddacbfc83d5b", new[] { KnownCurrencies.ETH, KnownCurrencies.MOON }, Exchange.Kraken, false, BlockchainType.ArbitrumNova);

        public static readonly Wallet ETC_BTCMARKETS_WALLET = new Wallet("0x1d3c7ee8dd9e3131834565e0dfecdb29f13e9fd2", new[] { KnownCurrencies.ETC }, Exchange.BTCMarkets, false, BlockchainType.EthereumClassic);

        public static readonly Wallet XRP_BTCMARKETS = new Wallet("r3zUhJWabAMMLT5n631r2wDh9RP3dN1bRy", new[] { KnownCurrencies.XRP }, Exchange.BTCMarkets, false, BlockchainType.XRPL);
        public static readonly Wallet XRP_BINANCE = new Wallet("rEb8TK3gBgk5auZkwc6sHnwrGVJH8DuaLh", new[] { KnownCurrencies.XRP }, Exchange.Binance, false, BlockchainType.XRPL);
        public static readonly Wallet XRP_FTX = new Wallet("rPFXvVo2fYXVPdV9gCHQouHsMgMhQ2aUwM", new[] { KnownCurrencies.XRP }, Exchange.FTX, false, BlockchainType.XRPL);
        public static readonly Wallet XRP_MY_WALLET_SAVINGS = new Wallet("rPTHVbcCBiwrqBr4cH8UnUcPgSQLxojWeg", new[] { KnownCurrencies.XRP }, null, true, BlockchainType.XRPL, "XUMM Wallet (Savings)");
        public static readonly Wallet XRP_MY_WALLET_SAVINGS_NEW = new Wallet("rfnDhAFPgAfFnatVGRBiWHXZeFjqK4sVFL", new[] { KnownCurrencies.XRP }, null, true, BlockchainType.XRPL, "XUMM Wallet (Savings New)", "Created because I lost the recovery numbers for the other wallet.");

        public static readonly Wallet BTC_BTCMARKETS = new Wallet("143zY9S2qeh9jAd9H7T8sVRnzNzLq4zR3z", new[] { KnownCurrencies.BTC }, Exchange.BTCMarkets, false, BlockchainType.Bitcoin);
        public static readonly Wallet BTC_BITMEX = new Wallet("3BMEXiHnHTXW1pNsfeEgc57fMsoTHkfhg6", new[] { KnownCurrencies.BTC }, Exchange.BitMex, false, BlockchainType.Bitcoin);

        // the ordering here is imporant - must first assign all the static Wallet fields before being able to use them!
        private static IEnumerable<Wallet> _KnownWallets = new List<Wallet>() {
            ETH_MY_WALLET_OLD, ETH_MY_WALLET, HOP_PROTOCOL_ETH_BRIDGE_MAINNET, HOP_PROTOCOL_ETH_BRIDGE_ARB_NOVA, ETH_MY_WALLET_ARB_NOVA, ETH_REDDIT_WALLET_ARB_NOVA, ETH_MAIN_ARB_NOVA_BRIDGE_CONTRACT, ETH_ARB_NOVA_MAIN_BRIDGE_CONTRACT, ETH_WRAPPED_ETHER_CONTRACT, ETH_BURN_ADDRESS, ETH_DDEX_EXCHANGE_WALLET, ETH_UNISWAP_V2_ETH_IN, ETH_UNISWAP_V2_MBT_OUT, ETH_MY_MERCATOX_DEPOSIT, ETH_MERCATOX_OFFICIAL_DEPOSIT, ETH_BINANCE_WITHDRAW, ETH_FTX_PRIVATE, ETH_BTCMARKETS, ETH_KRAKEN_DEPOSIT_1, ETH_KRAKEN_DEPOSIT_2, ETH_ARB_NOVA_KRAKEN_DEPOSIT, ETH_UNISWAP_V2_AMPL_POOL, ETH_UNISWAP_V2_ETH_ROUTER, ETH_UNISWAP_V3_ETH_ROUTER, ETH_UNISWAP_V3_ETH_ROUTER_2, ETH_ARB_NOVA_SUSHISWAP_ROUTER, ETH_ARB_NOVA_SUSHISWAP_MOON_POOL,
            ETC_BTCMARKETS_WALLET,
            XRP_BTCMARKETS, XRP_BINANCE, XRP_FTX, XRP_MY_WALLET_SAVINGS, XRP_MY_WALLET_SAVINGS_NEW,
            BTC_BTCMARKETS, BTC_BITMEX
        };

        public static IEnumerable<Wallet> GetAllWallets()
        {
            return _KnownWallets;
        }

        /// <summary>
        /// ChainType is only required where an address may not be unique, e.g. on parallel chains such as Ethereum and Arbitrum Nova. If chainType is provided, the returned wallet will be guaranteed to have the same type, if found.
        /// </summary>
        public static Wallet? GetWallet(DString address, BlockchainType? chainType = null)
        {
            IEnumerable<Wallet> matches = _KnownWallets.Where(w => address == w.Address && (chainType == null || w.Blockchain == chainType));
            int N = matches.Count();
            if (N == 0)
            {
                return null;
            }
            else if (N == 1)
            {
                return matches.First();
            }
            else if (chainType == null)
            {
                throw new Exception($"Unable to get wallet for address {address} because there are {N} matches but no chainType was provided.");
            }
            else
            {
                throw new Exception($"Unable to get wallet for address {address} on chain {chainType} because there are {N} matches.");
            }
        }

        public static bool HasWallet(DString address)
        {
            return _KnownWallets.Any(w => w.Address == address);
        }

        public static void SetWallet(Wallet wallet)
        {
            if (HasWallet(wallet.Address)) _KnownWallets = _KnownWallets.Where(w => w.Address != wallet.Address).Append(wallet);
            else _KnownWallets = _KnownWallets.Append(wallet);
        }
    }
}
