﻿using CryptoTax.Shared.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    public class FileService : IFileService
    {
        // todo: add file lock mechanism: since all methods are async, create a dictionary of filename specific locks, and make sure we are not trying to access the same file at the same time. applies only to writing while reading, or reading while writing, or writing while writing, but not reading and reading.
        // could use queue mechanism: when multiple write requests are made to the same file, use only the latest. if multiple write request are made to the same file, read only once and return result to all. can also use dirty flagging for commonly accessed files, and cache the value to prevent constant re-reading

        private readonly ICryptoService _CryptoService;

        public FileService(ICryptoService cryptoService)
        {
            this._CryptoService = cryptoService;
        }

        public IEnumerable<DString> ReadLines(DString fileName)
        {
            List<DString> lines = new List<DString>();

            using (StreamReader reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    lines.Add(reader.ReadLine());
                }
            }

            return lines;
        }

        public void WriteLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.writeLines(fileName, lines, newFileOnly, false);
        }

        public void AppendLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.writeLines(fileName, lines, newFileOnly, true);
        }

        public DString ReadFile(DString fileName)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                return reader.ReadToEnd();
            }
        }

        public T ReadAndDeserialiseFile<T>(DString fileName)
        {
            using (StreamReader file = File.OpenText(fileName))
            {
                JsonSerializer serializer = new JsonSerializer();
                T result = (T)serializer.Deserialize(file, typeof(T));
                return result;
            }
        }

        public void WriteFile(DString fileName, DString contents, bool newFileOnly)
        {
            DString folder = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

            if (File.Exists(fileName))
            {
                if (newFileOnly) throw new FileAlreadyExistsException(fileName);
                else File.Delete(fileName);
            }

            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.Write(contents);
            }
        }

        public IEnumerable<DString> GetFilesInFolder(DString folder)
        {
            if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

            return Directory.GetFiles(folder)
                // strip the safe extension (this fixes an issue where the safe file exists, but the original file does not, and hence the original file name is never returned even though its data still exists in limbo)
                .Select(f => f.EndsWith(Constants.TEMP_SAFE_FILE_EXTENSION) ? f.Replace(Constants.TEMP_SAFE_FILE_EXTENSION, "") : f)
                .Distinct()
                .Select(f => new DString(f));
        }

        public DString GetFingerprint(DString fileName)
        {
            using (FileStream stream = File.OpenRead(fileName))
            {
                return this._CryptoService.CalculateHash(stream);
            }
        }

        private void writeLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly, bool append)
        {
            DString folder = Path.GetDirectoryName(fileName);
            if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);

            if (File.Exists(fileName))
            {
                if (newFileOnly) throw new FileAlreadyExistsException(fileName);
                else if (!append) File.Delete(fileName);
            }

            using (StreamWriter writer = new StreamWriter(fileName, append, Encoding.Default, 1024 * 1024))
            {
                foreach (NString line in lines)
                {
                    writer.WriteLine(line.Value ?? "");
                }

                writer.Flush();
            }
        }
    }
}
