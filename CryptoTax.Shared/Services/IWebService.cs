﻿using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    public interface IWebService
    {
        Task<DString> DownloadStringAsync(DString address);
    }
}
