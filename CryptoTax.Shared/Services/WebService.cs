﻿using System.Net;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    public class WebService : IWebService
    {
        public async Task<DString> DownloadStringAsync(DString address)
        {
            using (WebClient client = new WebClient())
            {
                return await client.DownloadStringTaskAsync(address);
            }
        }
    }
}
