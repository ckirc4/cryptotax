﻿using CryptoTax.Shared.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    /// <summary>
    /// Use this file service if no other service will be accessing the same files while the application is running in order to speed up writing and re-reading. Call FlushAll() when FileServiceCached is no longer required to persist all cached changes.
    /// </summary>
    public class FileServiceCached : IFileService
    {
        private const char _NEW_LINE = '\n';

        private readonly ICryptoService _CryptoService;

        /// <summary>
        /// The contents of a file may be broken up into several strings arbitrarily while held in memory. These are joined together as-is (with no new line, or anything else).
        /// </summary>
        private readonly Dictionary<DString, IEnumerable<DString>> _Contents;
        private readonly Dictionary<DString, IEnumerable<DString>> _FilesInFolders;
        /// <summary>
        /// If a file path is contained here, we will not need to do any further I/O until flushing.
        /// </summary>
        private readonly HashSet<DString> _FilesIndexed;
        /// <summary>
        /// Only modified files will be updated when flushing - any other files present in _Contents were read-only.
        /// </summary>
        private readonly HashSet<DString> _FilesModified;

        public FileServiceCached(ICryptoService cryptoService)
        {
            this._CryptoService = cryptoService;
            this._Contents = new Dictionary<DString, IEnumerable<DString>>();
            this._FilesInFolders = new Dictionary<DString, IEnumerable<DString>>();
            this._FilesIndexed = new HashSet<DString>();
            this._FilesModified = new HashSet<DString>();
        }

        public void FlushAll()
        {
            foreach (DString fileName in this._FilesModified)
            {
                if (this._Contents.ContainsKey(fileName))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(fileName));
                    DString contents = this.getFullContents(fileName);
                    using (StreamWriter sw = new StreamWriter(fileName))
                    {
                        sw.Write(contents);
                    }
                }
                else
                {
                    File.Delete(fileName);
                }
            }

            this._Contents.Clear();
            this._FilesInFolders.Clear();
            this._FilesIndexed.Clear();
            this._FilesModified.Clear();
        }

        public void AppendLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.indexFileIfRequired(fileName);
            this.markAsModified(fileName);

            DString baseContent = "";
            if (this._Contents.TryGetValue(fileName, out IEnumerable<DString> existing))
            {
                if (existing.Last().Value.Last() != _NEW_LINE) baseContent = _NEW_LINE.ToString();
            }

            DString newContent = baseContent + string.Join(_NEW_LINE.ToString(), lines) + _NEW_LINE;
            if (this._Contents.ContainsKey(fileName) && newFileOnly)
            {
                throw new FileAlreadyExistsException(fileName);
            }
            this.setContents(fileName, newContent, true);
        }

        public IEnumerable<DString> GetFilesInFolder(DString folderPath)
        {
            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);

            IEnumerable<DString> result;
            if (!this._FilesInFolders.TryGetValue(folderPath, out result))
            {
                result = Directory.GetFiles(folderPath).Select(f => new DString(f));
                this._FilesInFolders[folderPath] = result;
            }

            // also add all files for which we have content cached
            return result.Concat(this._Contents.Keys.Where(fileName =>
            {
                string folder = string.Join(@"\", fileName.Value.Split('\\').Reverse().Skip(1).Reverse());
                return folder == folderPath || folder + @"\" == folderPath;
            })).Distinct();
        }

        public DString ReadFile(DString fileName)
        {
            this.indexFileIfRequired(fileName);

            if (!this._Contents.ContainsKey(fileName)) throw new FileNotFoundException($"File {fileName} does not exist.");
            else return this.getFullContents(fileName);
        }

        public T ReadAndDeserialiseFile<T>(DString fileName)
        {
            return JsonConvert.DeserializeObject<T>(this.ReadFile(fileName));
        }

        public IEnumerable<DString> ReadLines(DString fileName)
        {
            string[] lines = this.ReadFile(fileName).Value.Split(_NEW_LINE);
            bool endsWithNewLine = string.IsNullOrEmpty(lines.Last());
            return lines.Select(f => new DString(f)).Take(endsWithNewLine ? lines.Length - 1 : lines.Length);
        }

        public void WriteFile(DString fileName, DString contents, bool newFileOnly)
        {
            this.indexFileIfRequired(fileName);
            this.markAsModified(fileName);

            if (this._Contents.ContainsKey(fileName) && newFileOnly) throw new FileAlreadyExistsException(fileName);
            else this.setContents(fileName, contents, false);
        }

        public void WriteLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this.WriteFile(fileName, string.Join(_NEW_LINE.ToString(), lines), newFileOnly);
        }

        /// <summary>
        /// This may be slow.
        /// </summary>
        public DString GetFingerprint(DString fileName)
        {
            DString contents = this.ReadFile(fileName);
            return this._CryptoService.CalculateHash(contents);
        }

        private void indexFileIfRequired(DString fileName)
        {
            if (!this._FilesIndexed.Contains(fileName))
            {
                this._FilesIndexed.Add(fileName);

                if (File.Exists(fileName))
                {
                    using (StreamReader reader = new StreamReader(fileName))
                    {
                        DString existingContents = reader.ReadToEnd();
                        this.setContents(fileName, existingContents, false);
                    }
                }
            }
        }

        private void markAsModified(DString fileName)
        {
            this._FilesModified.Add(fileName);
        }

        private DString getFullContents(DString fileName)
        {
            return string.Join("", this._Contents[fileName]);
        }

        private void setContents(DString fileName, DString contents, bool append)
        {
            IEnumerable<DString> newContents;
            if (append && this._Contents.ContainsKey(fileName))
            {
                newContents = this._Contents[fileName].Append(contents);
            }
            else
            {
                newContents = new List<DString>() { contents };
            }

            if (this._Contents.ContainsKey(fileName))
            {
                this._Contents[fileName] = newContents;
            }
            else
            {
                this._Contents.Add(fileName, newContents);
            }
        }
    }
}
