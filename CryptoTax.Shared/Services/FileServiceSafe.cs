﻿using System.Collections.Generic;
using System.IO;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    /// <summary>
    /// Wrapper around <see cref="FileService"/>. Uses temporary files to safely read and write files without the possibility of losing previous data if the current data write fails.
    /// <br/>
    /// Recommended over <see cref="FileService"/> when files are updated frequently, or when the file body is very large (and writing takes a long time).
    /// </summary>
    public class FileServiceSafe : IFileService
    {
        private readonly IFileService _FileService;

        public FileServiceSafe(ICryptoService cryptoService)
        {
            this._FileService = new FileService(cryptoService);
        }

        // it is assumed that writing lines is quick enough to not pose a problem, so simply use vanilla implementation.
        public IEnumerable<DString> ReadLines(DString fileName)
        {
            return this._FileService.ReadLines(fileName);
        }

        public void WriteLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this._FileService.WriteLines(fileName, lines, newFileOnly);
        }

        public void AppendLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly)
        {
            this._FileService.AppendLines(fileName, lines, newFileOnly);
        }

        /// <summary>
        /// Creates or overwrites the specified file, ensuring that the existing file is not deleted until the new file has been successfully written to prevent loss of data.
        /// </summary>
        public void WriteFile(DString fileName, DString contents, bool newFileOnly)
        {
            // if the file does not exist, the behaviour should be equivalent to newFileOnly being true
            if (newFileOnly || !File.Exists(fileName))
            {
                this._FileService.WriteFile(fileName, contents, newFileOnly);
            }
            else
            {
                // 1. copy existing file to safe file
                // 2. write file normally
                // 3. delete safe file, since we successfully were able to update the file
                string safeFileName = fileName + Constants.TEMP_SAFE_FILE_EXTENSION;
                File.Move(fileName, safeFileName);
                this._FileService.WriteFile(fileName, contents, false);
                File.Delete(safeFileName);
            }
        }

        /// <summary>
        /// Reads the specified file, loading the previous version if the current version is found to be corrupt.
        /// <exception cref="System.IO.FileNotFoundException"/>
        public DString ReadFile(DString fileName)
        {
            string safeFileName = fileName + Constants.TEMP_SAFE_FILE_EXTENSION;

            if (File.Exists(safeFileName))
            {
                // this implies that the current file is corrupt - instead load the safe file
                File.Delete(fileName);
                File.Move(safeFileName, fileName);
            }

            return this._FileService.ReadFile(fileName);
        }

        /// <summary>
        /// Uses the previous version if the current version is found to be corrupt.
        /// <exception cref="System.IO.FileNotFoundException"/>
        public T ReadAndDeserialiseFile<T>(DString fileName)
        {
            string safeFileName = fileName + Constants.TEMP_SAFE_FILE_EXTENSION;

            if (File.Exists(safeFileName))
            {
                // this implies that the current file is corrupt - instead load the safe file
                File.Delete(fileName);
                File.Move(safeFileName, fileName);
            }

            return this._FileService.ReadAndDeserialiseFile<T>(fileName);
        }

        public IEnumerable<DString> GetFilesInFolder(DString folderPath)
        {
            return this._FileService.GetFilesInFolder(folderPath);
        }

        public DString GetFingerprint(DString fileName)
        {
            return this._FileService.GetFingerprint(fileName);
        }
    }
}
