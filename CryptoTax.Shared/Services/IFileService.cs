﻿using System;
using System.IO;
using System.Collections.Generic;
using UtilityLibrary.Types;
using Newtonsoft.Json;
using CryptoTax.Shared.Exceptions;

namespace CryptoTax.Shared.Services
{
    public interface IFileService
    {
        IEnumerable<DString> ReadLines(DString fileName);

        /// <summary>
        /// Writes the specified lines to the file.
        /// </summary>
        /// <exception cref="FileAlreadyExistsException">Thrown if the file already exists.</exception>
        void WriteLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly);

        /// <exception cref="FileAlreadyExistsException">Thrown if the file already exists.</exception>
        void AppendLines(DString fileName, IEnumerable<NString> lines, bool newFileOnly);

        /// <exception cref="FileAlreadyExistsException">Thrown if the file already exists.</exception>
        void WriteFile(DString fileName, DString contents, bool newFileOnly);

        /// <exception cref="FileNotFoundException">Thrown if the file does not exist.</exception>
        DString ReadFile(DString fileName);

        /// <summary>
        /// Reads the file synchronously and deserialises the contained object to type <typeparamref name="T"/>.<br/>
        /// May return null if serialisation succeeded but resulting `object` could not be casted to <typeparamref name="T"/>.
        /// </summary>
        /// <exception cref="FileNotFoundException">Thrown if the file does not exist.</exception>
        /// <exception cref="JsonSerializationException">Thrown if the object could not be serialised, possibly because the give type <typeparamref name="T"/> is incorrect.</exception>
        T ReadAndDeserialiseFile<T>(DString fileName);

        /// <summary>
        /// Returns the full path of all files within the specified folder.
        /// <br/>
        /// Ignores any temporary safe-files.
        /// </summary>
        IEnumerable<DString> GetFilesInFolder(DString folderPath);

        /// <summary>
        /// Calculates a unique fingerprint for the contents of the file.
        /// </summary>
        DString GetFingerprint(DString fileName);
    }
}
