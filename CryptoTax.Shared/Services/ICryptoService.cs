﻿using System.IO;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    public interface ICryptoService
    {
        DString CalculateHash(DString value);
        DString CalculateHash(FileStream stream);
    }
}
