﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services
{
    public class FlushablePersistorService<T>
    {
        private readonly IFileService _FileService;
        private readonly Func<T, NString> _LineGenerator;
        private readonly IEnumerable<NString> _InitialFileContents;
        private readonly Func<int, DString> _FileName;
        private readonly int _MaxRowsPerFile;
        private readonly int _MaxPendingRows;

        private NString _Folder;
        private int _RowsInCurrentFile;
        private int _CurrentFileIndex;
        private int _PendingRowsCount;
        private readonly List<T> _PendingRows;
        private readonly object _Lock;

        /// <summary>
        /// Note: since the folder may not be available at the time of instantiation, you may also inject it using the <see cref="SetFolder"/> method.
        /// </summary>
        public FlushablePersistorService(
            IFileService fileService,
            Func<T, NString> lineGenerator,
            IEnumerable<NString> initialFileContents,
            Func<int, DString> fileNameWithoutPath,
            int maxRowsPerFile,
            int maxPendingRows,
            NString folder)
        {
            this._FileService = fileService;
            this._LineGenerator = lineGenerator;
            this._InitialFileContents = initialFileContents.ToList();
            this._FileName = fileNameWithoutPath;
            this._MaxRowsPerFile = maxRowsPerFile;
            this._MaxPendingRows = maxPendingRows;

            this._RowsInCurrentFile = 0;
            this._CurrentFileIndex = 1;
            this._PendingRowsCount = 0;
            this._PendingRows = new List<T>();
            this._Lock = new object();

            if (folder != null) this.SetFolder(folder.Value);
        }

        /// <summary>
        /// All rows are pending until the folder has been set.
        /// </summary>
        public void SetFolder(DString folder)
        {
            if (this._Folder != null) throw new Exception("Folder has already been set");
            this._Folder = folder.Value.EndsWith(@"\") ? folder : folder + @"\";
        }

        /// <summary>
        /// Adds the lines to the memory, but may not flush
        /// </summary>
        /// <param name="unformattedLines"></param>
        public void AddObject(T obj)
        {
            lock (this._Lock)
            {
                this._PendingRows.Add(obj);
                this._PendingRowsCount++;
            }

            if (this._PendingRowsCount >= _MaxPendingRows) this.Flush();
        }

        /// <summary>
        /// Writes all pending rows to the file.
        /// </summary>
        public void Flush()
        {
            if (this._Folder == null)
            {
                // not a big deal - hang on to pending rows, and they will be added once the folder is set
                return;
            }
            else if (this._PendingRowsCount == 0)
            {
                return;
            }

            DString filePath = this._Folder + this._FileName(this._CurrentFileIndex);

            lock (this._Lock)
            {
                // initialise file (e.g. csv headers)
                if (this._InitialFileContents.Any() && !this._FileService.GetFilesInFolder(this._Folder.Value).Contains(filePath))
                {
                    this._FileService.WriteLines(filePath, this._InitialFileContents, true);
                }

                // add content and reset state
                this._FileService.AppendLines(filePath, this._PendingRows.Select(obj => this._LineGenerator(obj)).ToList(), false);
                this._RowsInCurrentFile += this._PendingRowsCount;
                this._PendingRows.Clear();
                this._PendingRowsCount = 0;

                if (this._RowsInCurrentFile >= _MaxRowsPerFile)
                {
                    this._RowsInCurrentFile = 0;
                    this._CurrentFileIndex++;
                }
            }
        }
    }
}
