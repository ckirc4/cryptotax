﻿using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    public interface ILoggingPersistorService
    {
        /// <summary>
        /// The persistor will collect data until the folder has been set.
        /// </summary>
        void SetFolder(DString folder);
        void WriteLines(IEnumerable<DString> unformattedLines);
        void Flush();
    }
}
