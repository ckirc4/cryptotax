﻿using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    public partial class LogMessage
    {
        private static DString undefined(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Undefined, thingToFormat?.ToString() ?? "");
        }

        private static DString black(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Black, thingToFormat?.ToString() ?? "");
        }

        private static DString darkBlue(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkBlue, thingToFormat?.ToString() ?? "");
        }

        private static DString darkGreen(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkGreen, thingToFormat?.ToString() ?? "");
        }

        private static DString darkCyan(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkCyan, thingToFormat?.ToString() ?? "");
        }

        private static DString darkRed(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkRed, thingToFormat?.ToString() ?? "");
        }

        private static DString darkMagenta(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkMagenta, thingToFormat?.ToString() ?? "");
        }

        private static DString darkYellow(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkYellow, thingToFormat?.ToString() ?? "");
        }

        private static DString gray(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Gray, thingToFormat?.ToString() ?? "");
        }

        private static DString darkGray(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.DarkGray, thingToFormat?.ToString() ?? "");
        }

        private static DString blue(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Blue, thingToFormat?.ToString() ?? "");
        }

        private static DString green(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Green, thingToFormat?.ToString() ?? "");
        }

        private static DString cyan(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Cyan, thingToFormat?.ToString() ?? "");
        }

        private static DString red(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Red, thingToFormat?.ToString() ?? "");
        }

        private static DString magenta(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Magenta, thingToFormat?.ToString() ?? "");
        }

        private static DString yellow(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.Yellow, thingToFormat?.ToString() ?? "");
        }

        private static DString white(object thingToFormat)
        {
            return ColourHelpers.ColouriseText(Colour.White, thingToFormat?.ToString() ?? "");
        }

        private static DString greenOrRed(decimal value, bool roundTwoDecimals = true)
        {
            DString str = roundTwoDecimals ? value.ToString("0.00") : value.ToString();
            return value < 0 ? red(str) : value > 0 ? green(str) : gray(str);
        }
    }
}
