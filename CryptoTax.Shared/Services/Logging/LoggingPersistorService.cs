﻿using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    public class LoggingPersistorService : FlushablePersistorService<DString>, ILoggingPersistorService
    {
        private const int _MAX_ROWS_PER_LOG = 200_000;
        private const int _MAX_PENDING_ROWS = 100;

        private readonly IRegisteredLogger _Log;

        private bool _FolderSet;
        private bool _NullFolderWarningLogged;

        /// <summary>
        /// Note: since the folder may not be available at the time of instantiation, inject it using the <see cref="SetFolder"/> method.
        /// </summary>
        public LoggingPersistorService(IFileService fileService, ILoggingService loggingService)
            : base(fileService,
                  str => str,
                  new NString[0],
                  (index) => $"Log_{index}.log",
                  _MAX_ROWS_PER_LOG,
                  _MAX_PENDING_ROWS,
                  null)
        {
            this._Log = loggingService.Register(this, nameof(LoggingPersistorService));

            this._FolderSet = false;
            this._NullFolderWarningLogged = false;
        }

        public void WriteLines(IEnumerable<DString> unformattedLines)
        {
            foreach (DString line in unformattedLines)
            {
                this.AddObject(line);
            }
        }

        new public void SetFolder(DString folder)
        {
            if (this._FolderSet) throw new Exception("Folder has already been set");

            this._FolderSet = true;
            base.SetFolder(folder);
        }

        new public void Flush()
        {
            if (!this._FolderSet && !this._NullFolderWarningLogged)
            {
                this._NullFolderWarningLogged = true;
                this._Log.Log(LogMessage.MSG_LOGGINGPERSISTOR_WARNING_FOLDERNULL);
            }

            base.Flush();
        }
    }
}
