﻿namespace CryptoTax.Shared.Services.Logging
{
    public class LoggingSettings : ILoggingSettings
    {
        private readonly LoggingTemplate? _ConsoleTemplate;
        private readonly LoggingTemplate? _PersistorTemplate;

        public LoggingSettings(LoggingTemplate? consoleTemplate, LoggingTemplate? persistorTemplate)
        {
            this._ConsoleTemplate = consoleTemplate;
            this._PersistorTemplate = persistorTemplate;
        }

        public LoggingTemplate? GetConsoleTemplate() => this._ConsoleTemplate;
        public LoggingTemplate? GetPersistorTemplate() => this._PersistorTemplate;
    }
}
