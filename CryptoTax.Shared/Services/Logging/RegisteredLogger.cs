﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    /// <summary>
    /// This class must be used to interact with the LoggingService.
    /// </summary>
    public class RegisteredLogger : IRegisteredLogger
    {
        public DString Id { get; }
        public DString Type { get; }
        public DString Name { get; }

        private object _UnderlyingObject;
        private Func<LogMessage, DString> _OnLog;
        private Action _OnDispose;

        public RegisteredLogger(object obj, DString name, Func<RegisteredLogger, LogMessage, DString> onLog, Action<RegisteredLogger> onDispose)
        {
            this.Id = obj.GetHashCode().ToString();
            this.Type = obj.GetType().Name;
            this.Name = name;

            this._UnderlyingObject = obj;
            this._OnLog = (msg) => onLog(this, msg);
            this._OnDispose = () => onDispose(this);
        }

        public bool IsRegisteredTo(object obj)
        {
            return this._UnderlyingObject == obj;
        }

        public void Dispose()
        {
            this._OnDispose();
            this._UnderlyingObject = null;
            this._OnLog = null;
            this._OnDispose = null;
        }

        /// <summary>
        /// Logs a message and returns the message string that was provided, minus administrative text (e.g. log id, timestamps).
        /// </summary>
        public DString Log(LogMessage msg)
        {
            return this.onLog(msg);
        }

        public DString Log<T1>(LogMessage<T1> msg, T1 arg1)
        {
            return this.onLog(msg.Collapsor(arg1));
        }

        public DString Log<T1, T2>(LogMessage<T1, T2> msg, T1 arg1, T2 arg2)
        {
            return this.onLog(msg.Collapsor(arg1, arg2));
        }

        public DString Log<T1, T2, T3>(LogMessage<T1, T2, T3> msg, T1 arg1, T2 arg2, T3 arg3)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3));
        }

        public DString Log<T1, T2, T3, T4>(LogMessage<T1, T2, T3, T4> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4));
        }

        public DString Log<T1, T2, T3, T4, T5>(LogMessage<T1, T2, T3, T4, T5> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5));
        }

        public DString Log<T1, T2, T3, T4, T5, T6>(LogMessage<T1, T2, T3, T4, T5, T6> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7>(LogMessage<T1, T2, T3, T4, T5, T6, T7> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15));
        }

        public DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15, T16 arg16)
        {
            return this.onLog(msg.Collapsor(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16));
        }

        private DString onLog(LogMessage msg)
        {
            if (this._UnderlyingObject == null || this._OnLog == null || this._OnDispose == null) throw new ObjectDisposedException(nameof(RegisteredLogger), "Cannot log using a disposed registered logger");
            return this._OnLog(msg);
        }
    }
}
