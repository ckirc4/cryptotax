﻿namespace CryptoTax.Shared.Services.Logging
{
    public interface ILoggingSettings
    {
        LoggingTemplate? GetConsoleTemplate();
        LoggingTemplate? GetPersistorTemplate();
    }
}
