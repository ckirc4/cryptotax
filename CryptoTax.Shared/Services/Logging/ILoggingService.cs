﻿using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    /// <summary>
    /// Central service that manages logging. To engage in the logging experience, register your object, then use <see cref="LogMessage"/>s in the <see cref="IRegisteredLogger"/>'s Log() methods to start logging.
    /// </summary>
    public interface ILoggingService
    {
        /// <summary>
        /// This must be the last method called before the application exits.
        /// </summary>
        void Flush();

        /// <summary>
        /// Register your object as a logger.
        /// </summary>
        IRegisteredLogger Register(object obj, DString name);

        /// <summary>
        /// Please call this when disposing of your methods. Once unregistered, cannot log any further.<br/>
        /// Note: Alternatively, can use <see cref="RegisteredLogger.Dispose()"/>.
        /// </summary>
        void Unregister(object obj);

        void UpdateSettings(ILoggingSettings settings);
    }
}
