﻿using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    /// <summary>
    /// Used to display formatted text to the user.
    /// </summary>
    public interface IFrontEndService
    {
        void WriteLines(IEnumerable<DString> formattedLines);
    }
}
