﻿using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    public static class ColourHelpers
    {
        private const string _COLOUR_SYMBOL = "&";
        private const string _COLOUR_START = "{{";
        private const string _COLOUR_END = "}}";

        public static DString ColouriseText(Colour colour, DString text)
        {
            return $"{getCodeFromColour(colour)}{_COLOUR_START}{text}{_COLOUR_END}";
        }

        // e.g. &1{{Hello &2{{World}} &3{{how are &4{{you}}}} going today}}
        // should become (&1, "Hello "), (&2, "World"), (&1, " "), (&3, "how are "), (&4, "you"), (&1, " going today")
        public static IEnumerable<(Colour, DString)> ExtractFormatting(DString text)
        {
            List<(Colour, DString)> result = new List<(Colour, DString)>();
            Stack<Colour> currentColour = new Stack<Colour>();
            currentColour.Push(Colour.Undefined);

            string formattedText = text.Value;
            string currentText = "";
            int lastStart = -1000;
            int lastEnd = -1000;
            for (int i = 0; i < formattedText.Length; i++)
            {
                // format symbols in progress
                if (i - lastStart < _COLOUR_SYMBOL.Length + 1 + _COLOUR_START.Length) continue;
                else if (i - lastEnd < _COLOUR_END.Length) continue;

                if (i <= formattedText.Length - _COLOUR_SYMBOL.Length - 1 - _COLOUR_START.Length - _COLOUR_END.Length // check there is even enough room for more formatting
                    && formattedText.Substring(i, _COLOUR_SYMBOL.Length) == _COLOUR_SYMBOL
                    && formattedText.Substring(i + _COLOUR_SYMBOL.Length + 1, _COLOUR_START.Length) == _COLOUR_START)
                {
                    Colour? nextColour = getColourFromCode(formattedText.Substring(i, _COLOUR_SYMBOL.Length + 1));
                    if (nextColour != null)
                    {
                        // add text up to now for current colour
                        if (!string.IsNullOrWhiteSpace(currentText))
                        {
                            result.Add((currentColour.Peek(), currentText));
                            currentText = "";
                        }

                        lastStart = i;
                        currentColour.Push(nextColour.Value);
                    }

                }
                else if (i <= formattedText.Length - _COLOUR_END.Length
                    && formattedText.Substring(i, _COLOUR_END.Length) == _COLOUR_END)
                {
                    // add text up to now for current colour - note the pop
                    Colour colour = currentColour.Pop();
                    if (!string.IsNullOrWhiteSpace(currentText))
                    {
                        result.Add((colour, currentText));
                        currentText = "";
                    }

                    lastEnd = i;
                }
                else
                {
                    currentText += formattedText[i];
                }
            }

            if (currentText != "")
            {
                if (string.IsNullOrWhiteSpace(currentText))
                {
                    // don't want to create separate entry for just whitespace - instead, add to previous entry
                    if (result.Any()) {
                        (Colour, DString) last = result.Last();
                        result.Remove(last);
                        result.Add((last.Item1, last.Item2 + currentText));
                    }
                }
                else
                {
                    result.Add((currentColour.Pop(), currentText));
                }
            }

            return result;
        }

        public static IEnumerable<DString> RemoveAllFormatting(IEnumerable<DString> textLines)
        {
            return textLines.Select(line => RemoveAllFormatting(line));
        }

        public static DString RemoveAllFormatting(DString text)
        {
            return string.Join("", ExtractFormatting(text).Select(ct => ct.Item2.Value));
        }

        // e.g. "Hello &c{{my}} world" returns "Hello "
        // may return the whole string, or an empty string.
        private static string getLeadingUnformattedText(string text)
        {
            for (int i = 0; i < text.Length - _COLOUR_SYMBOL.Length - 1 - _COLOUR_START.Length; i++)
            {
                if (text.Substring(i, _COLOUR_SYMBOL.Length) == _COLOUR_SYMBOL
                    && text.Substring(i + _COLOUR_SYMBOL.Length + 1, _COLOUR_START.Length) == _COLOUR_START)
                {
                    return text.Substring(0, i);
                }
            }

            return text;
        }

        // e.g. "Hello &c{{my}} world" returns " world"
        private static string getTrailingUnformattedText(string text)
        {
            int startIndex = text.LastIndexOf(_COLOUR_END);

            if (startIndex == -1) return text;
            else return text.Substring(startIndex + _COLOUR_END.Length);
        }

        /// <summary>
        /// Returns true if text is wrapped using the correct format, but does not check whether the colour is valid.
        /// 
        /// This does not necessarily mean that the beginning colour is only closed at the end of the string.
        /// </summary>
        private static bool isColourWrapped(DString text)
        {
            string str = text.Value;
            if (str.Length < _COLOUR_SYMBOL.Length + 1 + _COLOUR_START.Length + _COLOUR_END.Length) return false;
            if (str.Substring(0, _COLOUR_SYMBOL.Length) != _COLOUR_SYMBOL) return false;
            if (str.Substring(_COLOUR_SYMBOL.Length + 1, _COLOUR_START.Length) != _COLOUR_START) return false;
            if (str.Substring(str.Length - _COLOUR_END.Length) != _COLOUR_END) return false;

            return true;
        }

        /// <summary>
        /// Returns null if the code was misformed or referred to a non-existing colour.
        /// </summary>
        private static Colour? getColourFromCode(DString code)
        {
            string str = code.Value;
            if (str.Length != _COLOUR_SYMBOL.Length + 1 || str.Substring(0, _COLOUR_SYMBOL.Length) != _COLOUR_SYMBOL) return null;
            char enumCode = str[1];
            Colour? colour = (Colour?)enumCode;

            return colour;
        }

        private static DString getCodeFromColour(Colour colour)
        {
            return $"{_COLOUR_SYMBOL}{(char)colour}";
        }
    }
}
