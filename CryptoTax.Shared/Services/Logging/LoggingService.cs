﻿using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    /// <summary>
    /// Central service that manages logging. To engage in the logging experience, register your object, then use <see cref="LogMessage"/>s in the <see cref="RegisteredLogger"/>'s Log() methods to start logging.
    /// </summary>
    public class LoggingService : ILoggingService
    {
        /// <summary>
        /// Generates formatted lines, may return empty collection.
        /// </summary>
        private delegate IEnumerable<DString> TemplateInvocation(DString logId, DateTime time, RegisteredLogger logger, LogMessage message);

        private ILoggingSettings _LoggingSettings;
        private readonly IFrontEndService _FrontEndService;
        private readonly ILoggingPersistorService _PersistorService;

        private long _Count;
        private readonly HashSet<RegisteredLogger> _RegisteredLoggers;
        private readonly Dictionary<LoggingTemplate, TemplateInvocation> _TemplateInvocations;

        /// <summary>
        /// Use this constructor if the persistor is not quite ready yet. Will immediately call the generator function and inject itself.
        /// </summary>
        public LoggingService(ILoggingSettings settings, IFrontEndService frontEndService, Func<ILoggingService, ILoggingPersistorService> persistorServiceGenerator)
            : this(settings, frontEndService, null as ILoggingPersistorService) // `as` required to choose the correct constructor signature
        {
            // overwrite the null persistor to use the generated persistor
            this._PersistorService = persistorServiceGenerator(this);
        }

        public LoggingService(ILoggingSettings settings, IFrontEndService frontEndService, ILoggingPersistorService persistorService)
        {
            this._LoggingSettings = settings;
            this._FrontEndService = frontEndService;
            this._PersistorService = persistorService;

            this._Count = 0;
            this._RegisteredLoggers = new HashSet<RegisteredLogger>();

            this._TemplateInvocations = new Dictionary<LoggingTemplate, TemplateInvocation>();
            foreach (LoggingTemplate template in Enum.GetValues(typeof(LoggingTemplate)))
            {
                switch (template)
                {
                    case LoggingTemplate.Console_Verbose:
                        this._TemplateInvocations.Add(template, templateInvocation_Console_Verbose);
                        break;
                    case LoggingTemplate.Console_Production:
                        this._TemplateInvocations.Add(template, templateInvocation_Console_Production);
                        break;
                    case LoggingTemplate.Persistor_Verbose:
                        this._TemplateInvocations.Add(template, templateInvocation_Persistor_Verbose);
                        break;
                    default:
                        throw new AssertUnreachable(template);
                }
            }
        }

        /// <summary>
        /// Register your object as a logger.
        /// </summary>
        public IRegisteredLogger Register(object obj, DString name)
        {
            if (obj == null) throw new ArgumentNullException("Cannot register a null object");

            RegisteredLogger existing = this._RegisteredLoggers.FirstOrDefault(r => r.IsRegisteredTo(obj));

            if (existing == null)
            {
                RegisteredLogger registered = new RegisteredLogger(obj, name, this.onRegisteredLoggerLog, this.onRegisteredLoggerDispose);
                this._RegisteredLoggers.Add(registered);
                return registered;
            }
            else
            {
                return existing;
            }
        }

        /// <summary>
        /// Please call this when disposing of your methods. Once unregistered, cannot log any further.<br/>
        /// Note: Alternatively, can use <see cref="RegisteredLogger.Dispose()"/>.
        /// </summary>
        public void Unregister(object obj)
        {
            if (obj == null) return;

            RegisteredLogger registeredLogger = this._RegisteredLoggers.FirstOrDefault(r => r.IsRegisteredTo(obj));

            if (registeredLogger != null)
            {
                this._RegisteredLoggers.Remove(registeredLogger);
                registeredLogger.Dispose();
            }
        }

        /// <summary>
        /// This must be the last method called before the application exits.
        /// </summary>
        public void Flush()
        {
            this._PersistorService.Flush();
        }

        public void UpdateSettings(ILoggingSettings settings)
        {
            this._LoggingSettings = settings;
        }

        /// <summary>
        /// Returns the message that string was provided, minus administrative text (e.g. log id, timestamps).
        /// </summary>
        private DString onRegisteredLoggerLog(RegisteredLogger logger, LogMessage msg)
        {
            this._Count++;

            LoggingTemplate? consoleTemplate = this._LoggingSettings.GetConsoleTemplate();
            if (consoleTemplate != null)
            {
                TemplateInvocation template = this._TemplateInvocations[consoleTemplate.Value];
                IEnumerable<DString> formattedMsg = template(this._Count.ToString(), DateTime.Now, logger, msg);
                this._FrontEndService.WriteLines(formattedMsg);
            }

            LoggingTemplate? persistorTemplate = this._LoggingSettings.GetPersistorTemplate();
            if (persistorTemplate != null)
            {
                TemplateInvocation template = this._TemplateInvocations[persistorTemplate.Value];
                IEnumerable<DString> formattedMsg = template(this._Count.ToString(), DateTime.Now, logger, msg);
                this._PersistorService.WriteLines(formattedMsg);
            }

            // the point of returning the logging text is to give the original caller the chance to log an exception before throwing it (both using the same message), since exceptions at the application level are not caught/logged.
            return string.Join("\n", ColourHelpers.RemoveAllFormatting(msg.Generator()));
        }

        private void onRegisteredLoggerDispose(RegisteredLogger logger)
        {
            this._RegisteredLoggers.Remove(logger);
        }

        #region template invocations
        private static IEnumerable<DString> templateInvocation_Console_Verbose(DString logId, DateTime time, RegisteredLogger logger, LogMessage message)
        {
            IEnumerable<DString> lines = invokeGenerator(message);
            if (!lines.Any()) return lines;

            Colour severityColour = getSeverityColour(message.Severity);
            bool printPrefix = message.Severity != LoggingSeverity.UI;
            string prefix = printPrefix ? $"<{ColourHelpers.ColouriseText(severityColour, $"{logger.Name}: {message.Severity}")}> " : "";
            IEnumerable<DString> result = addPrefix(lines, prefix, true);
            return result;
        }

        private static IEnumerable<DString> templateInvocation_Console_Production(DString logId, DateTime time, RegisteredLogger logger, LogMessage message)
        {
            if (message.Severity == (LoggingSeverity.Debug | LoggingSeverity.Info | LoggingSeverity.UI)) return new List<DString>();

            IEnumerable<DString> lines = invokeGenerator(message);
            if (!lines.Any()) return lines;

            string prefix = "<" + ColourHelpers.ColouriseText(getSeverityColour(message.Severity), logger.Name) + "> ";
            IEnumerable<DString> result = addPrefix(lines, prefix, true);
            return result;
        }

        private static IEnumerable<DString> templateInvocation_Persistor_Verbose(DString logId, DateTime time, RegisteredLogger logger, LogMessage message)
        {
            if (message.Severity == LoggingSeverity.UI) return new List<DString>();

            IEnumerable<DString> lines = ColourHelpers.RemoveAllFormatting(invokeGenerator(message));
            if (!lines.Any()) return lines;

            string prefix = $"{logId} {time.ToString("HH:mm:ss")} <{logger.Name} | {message.Severity}> ";
            IEnumerable<DString> result = addPrefix(lines, prefix, true);
            return result;
        }
        #endregion

        private static Colour getSeverityColour(LoggingSeverity severity)
        {
            Colour colour;
            switch (severity)
            {
                case LoggingSeverity.Debug:
                    colour = Colour.DarkGray;
                    break;
                case LoggingSeverity.Info:
                    colour = Colour.Cyan;
                    break;
                case LoggingSeverity.Warning:
                    colour = Colour.DarkYellow;
                    break;
                case LoggingSeverity.Error:
                    colour = Colour.Red;
                    break;
                case LoggingSeverity.Fatal:
                    colour = Colour.DarkRed;
                    break;
                case LoggingSeverity.Success:
                    colour = Colour.Green;
                    break;
                case LoggingSeverity.UI:
                    colour = Colour.DarkGray;
                    break;
                default:
                    throw new AssertUnreachable(severity);
            }

            return colour;
        }

        private static IEnumerable<DString> addPrefix(IEnumerable<DString> lines, DString prefix, bool indentSubsequentLines)
        {
            if (!lines.Any()) return lines;

            int padding = indentSubsequentLines ? ColourHelpers.RemoveAllFormatting(prefix).Value.Length : 0;

            return lines.Select((line, index) =>
            {
                DString modifiedLine;
                if (index == 0)
                {
                    modifiedLine = $"{prefix}{line}";
                }
                else
                {
                    modifiedLine = $"{"".PadLeft(padding)}{line}";
                }
                return modifiedLine;
            });
        }

        private static IEnumerable<DString> invokeGenerator(LogMessage message)
        {
            return message.Generator()
                .SelectMany(row => row.Value.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None))
                .Select(str => new DString(str));
        }
    }
}
