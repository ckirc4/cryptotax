﻿namespace CryptoTax.Shared.Services.Logging
{
    public enum LoggingSeverity
    {
        Debug = 1,
        Info = 2,
        Warning = 4,
        Error = 8,
        Fatal = 16,
        Success = 32,
        /// <summary>
        /// E.g. empty line, progress bar
        /// </summary>
        UI = 64
    }
}
