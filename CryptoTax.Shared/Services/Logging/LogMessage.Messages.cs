﻿using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    // define messages in this file. Naming should be ordered by severity and of type MSG_<LOGGER (optional)>_<SEVERITY>_<DESCRIPTION>
    // to make it easy to find specific messages in the class member dropdown list.
    public partial class LogMessage
    {
        public static readonly LogMessage<decimal, decimal> MSG_RESULTS =
            new LogMessage<decimal, decimal>(LoggingSeverity.Success, "PRICE_DATA_INCONSISTENT",
            (netCapitalGains, assessableIncome) => new DString[] {
                $"Total capital gains:     ${greenOrRed(netCapitalGains)}",
                $"Total assessable income: ${greenOrRed(assessableIncome)}"
            });

        public static readonly LogMessage<Exception> MSG_FATAL_UNEXPECTED_EXCEPTION = new LogMessage<Exception>(LoggingSeverity.Fatal, "UNEXPECTED_EXCEPTION",
            (e) => iterativelyPrintException(e));

        /// <summary>
        /// Total candlestick count | Symbol count | Group count
        /// </summary>
        public static readonly LogMessage<int, int, int> MSG_BULKCANDLESTICK_INFO_REQUIRING_CANDLESTICKS = new LogMessage<int, int, int>(LoggingSeverity.Info, "BULK_CANDLESTICK_REQUIREMENT",
            (totalCandlestickCount, symbolCount, groupCount) => new DString[]
            {
                $"Requiring a total number of {cyan(totalCandlestickCount)} candlesticks over {symbolCount} symbols. Grouped into a total of {groupCount} chunks."
            });

        /// <summary>
        /// Error count | Bulk candlestick key | Inner aggregate error messages
        /// </summary>
        public static readonly LogMessage<int, DString, IEnumerable<IEnumerable<DString>>> MSG_BULKCANDLESTICK_WARNING_ERRORS = new LogMessage<int, DString, IEnumerable<IEnumerable<DString>>>(LoggingSeverity.Info, "BULK_CANDLESTICK_ERROR",
            (errorCount, bulkCandlestickKey, errorMessages) => new DString[]
            {
                $"{darkYellow($"Encountered {errorCount} error{(errorCount > 1 ? "s" : "")}")} while attempting to download bulk candlesticks with key {bulkCandlestickKey}:"
            }.Concat(errorMessages.Select(msg => new DString($"  - {string.Join("; ", msg)}"))));

        /// <summary>
        /// Candlestick count | Starting time | Interval | Symbol | Exchange
        /// </summary>
        public static readonly LogMessage<int, DateTime, CandlestickInterval, DString, Exchange> MSG_API_INFO_GET_MISSING_CANDLESTICKS = new LogMessage<int, DateTime, CandlestickInterval, DString, Exchange>(LoggingSeverity.Info, "GET_MISSING_CANDLESTICKS",
            (count, time, interval, symbol, exchange) => new DString[]
            {
                $"{exchange}:{symbol} Getting {count} candlesticks at {time.ToString("dd/MM/yy HH:mm")}"
            });

        /// <summary>
        /// Candlestick count | Recursive depth | From | To | Symbol | Exchange
        /// </summary>
        public static readonly LogMessage<int, int, DateTime, DateTime, DString, Exchange> MSG_API_DEBUG_FILL_IN_LEADING_CANDLESTICKS = new LogMessage<int, int, DateTime, DateTime, DString, Exchange>(LoggingSeverity.Debug, "FILL_IN_LEADING_CANDLESTICKS",
            (count, recursiveDepth, from, to, symbol, exchange) => new DString[]
            {
                $"{exchange}:{symbol} Filling in {count} leading candlesticks [depth {recursiveDepth}] from {from.ToString("dd/MM/yy HH:mm")} to {to.ToString("dd/MM/yy HH:mm")}"
            });

        /// <summary>
        /// Inventory id | overdrawn amount | overdrawn currency | type of removal (e.g. disposal, fee burn, transfer out)
        /// </summary>
        public static readonly LogMessage<DString, decimal, DString, DString> MSG_INV_WARNING_OVERDRAWN = new LogMessage<DString, decimal, DString, DString>(LoggingSeverity.Warning, "INVENTORY_OVERDRAWN",
            (inventoryId, amountOverdrawn, currencyOverdrawn, removalType) => new DString[] {
                $"Overdrew funds during {removalType} removal in inventory {inventoryId} (overdrawn by {amountOverdrawn} {currencyOverdrawn})"
            });

        public static readonly LogMessage MSG_WARNING_OVERDRAWN_CHECK_FEES = new LogMessage(LoggingSeverity.Warning, "OVERDRAWN_CHECK_FEES",
            () => new DString[]
            {
                $"Overdrew inventory while transferring out fees. Did you include the fee amount in the ContainerEventDTO?"
            });

        /// <summary>
        /// Currency | Exception message
        /// </summary>
        public static readonly LogMessage<DString, DString> MSG_WARNING_AIRDROP_PRICE_NOT_FOUND = new LogMessage<DString, DString>(LoggingSeverity.Warning, "OVERDRAWN_CHECK_FEES",
            (currency, message) => new DString[]
            {
                $"Could not find price data for {currency} airdrop: '{message}'"
            });

        public static readonly LogMessage MSG_LOGGINGPERSISTOR_WARNING_FOLDERNULL = new LogMessage(LoggingSeverity.Warning, "FOLDER_NULL", () => new DString[] { "LoggingPersistorService cannot save because the folder was not set. If the folder is set later, pending logging messages will be saved then. This message will not show again." });

        /// <summary>
        /// Row ID
        /// </summary>
        public static readonly LogMessage<DString> MSG_DEBUG_REPORT_ROW = new LogMessage<DString>(LoggingSeverity.Debug, "REPORT_ROW",
            (rowId) => new DString[]
            {
                $"Generated report row {rowId}"
            });

        /// <summary>
        /// Purge count | Time | Is financial year complete? | Ids of soft blocked transactions | Messages for hard blocked transactions
        /// </summary>
        public static readonly LogMessage<int, DateTime, bool, IEnumerable<DString>, IEnumerable<DString>> MSG_DEBUG_TRANSFER_PURGE = new LogMessage<int, DateTime, bool, IEnumerable<DString>, IEnumerable<DString>>(LoggingSeverity.Debug, "TRANSFER_PURGE",
            (purgeCount, time, financialYearComplete, softBlockedTx, hardBlockedMsg) => {
                List<DString> result = new List<DString> { $"Will purge {purgeCount} pending transfers (time: {time.ToString("dd/MM/yyyy HH:mm:ss")} | end of financial year: {financialYearComplete})" };

                if (softBlockedTx.Any() || hardBlockedMsg.Any())
                {
                    result.Add("    The following transactions are not whitelisted:");
                    if (softBlockedTx.Any())
                    {
                        result.Add($"    {darkYellow("Soft blocked transactions:")}");
                        result.AddRange(softBlockedTx.Select(tx => new DString($"    - {tx}")));
                    }
                    if (hardBlockedMsg.Any())
                    {
                        result.Add($"    {red("Hard blocked messages:")}");
                        result.AddRange(hardBlockedMsg.Select(msg => new DString($"    - {msg}")));
                    }
                }

                return result;
            });

        /// <summary>
        /// Current count | % done | # inventories | # items (CAs) | # assets | Total capital gains
        /// </summary>
        public static readonly LogMessage<int, decimal, int, int, int, decimal> MSG_DEBUG_PROGRESS_UPDATE = new LogMessage<int, decimal, int, int, int, decimal>(LoggingSeverity.Debug, "PROGRESS_UPDATE",
            (currentCount, percentDone, numInventories, numItems, numAssets, totalCapitalGains) => new DString[]
            {
                $"{darkCyan($"{currentCount} ({percentDone.ToString("0.00")}%)")}: {numInventories} inventories hold {numItems} total packets over {numAssets} assets with a capital gain of ${greenOrRed(totalCapitalGains)}"
            });

        public static readonly LogMessage MSG_UI_COLOUR_TEST = new LogMessage(LoggingSeverity.UI, "COLOUR_TEST", () => 
        {
            List<DString> lines = new List<DString>();
            foreach (Colour colour in Enum.GetValues(typeof(Colour)))
            {
                lines.Add(ColourHelpers.ColouriseText(colour, colour.ToString()));
            }
            return lines;
        });

        /// <summary>
        /// Standard CG | Discounted CG | Capital losses | Total gains or losses
        /// </summary>
        public static readonly LogMessage<decimal, decimal, decimal, decimal> MSG_INFO_CAPITAL_GAINS = new LogMessage<decimal, decimal, decimal, decimal>(LoggingSeverity.Info, "CAPITAL_GAINS",
            (standardCG, discountableCG, losses, totalGainsOrLosses) => new DString[]
            {
                $"Standard capital gains are ${greenOrRed(standardCG)}, discountable gains are ${greenOrRed(discountableCG)} and losses are ${greenOrRed(losses)}, leading to final capital gains of ${greenOrRed(totalGainsOrLosses)}"
            });

        /// <summary>
        /// Tx summary | Whitelist info message
        /// </summary>
        public static readonly LogMessage<DString, NString> MSG_WHITELISTED_INFO = new LogMessage<DString, NString>(LoggingSeverity.Info, "WHITELISTED_INFO",
            (txSummary, whitelistMessage) => new DString[]
            {
                $"Transaction {txSummary} whitelisted with the following message: {darkGreen(whitelistMessage)}"
            });

        /// <summary>
        /// Tx summary | Whitelist warning message
        /// </summary>
        public static readonly LogMessage<DString, NString> MSG_WHITELISTED_WARNING = new LogMessage<DString, NString>(LoggingSeverity.Warning, "WHITELISTED_WARNING",
            (txSummary, whitelistMessage) => new DString[]
            {
                $"Transaction {txSummary} whitelisted with the following message: {darkYellow(whitelistMessage)}"
            });

        private static IEnumerable<DString> iterativelyPrintException(Exception e)
        {
            List<DString> result = new List<DString>()
            {
                $"An unexpected exception occurred: '{(string.IsNullOrEmpty(e.Message) ? "<no message provided>" : e.Message)}'",
                e.StackTrace
            };

            int exceptionDepth = 0;
            string prefix = "";
            Exception currentException = e;
            while (currentException.InnerException != null)
            {
                // add extra indent and vertical line per depth
                exceptionDepth++;
                prefix += "    | ";
                currentException = currentException.InnerException;

                string msg = string.IsNullOrEmpty(currentException.Message) ? "<no message provided>" : currentException.Message;
                result.Add($"{prefix}Inner exception {exceptionDepth}: '{msg}'");

                if (!string.IsNullOrEmpty(currentException.StackTrace))
                {
                    // to keep indentation, we need to manually split the lines
                    foreach (DString line in currentException.StackTrace.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None))
                    {
                        result.Add($"{prefix}{line}");
                    }
                }
            }

            return result;
        }
    }
}
