﻿using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    /// <summary>
    /// This interface must be used to interact with the LoggingService.
    /// </summary>
    public interface IRegisteredLogger
    {
        DString Id { get; }
        DString Name { get; }
        DString Type { get; }

        void Dispose();
        bool IsRegisteredTo(object obj);
        DString Log(LogMessage msg);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15, T16 arg16);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8, T9>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
        DString Log<T1, T2, T3, T4, T5, T6, T7, T8>(LogMessage<T1, T2, T3, T4, T5, T6, T7, T8> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);
        DString Log<T1, T2, T3, T4, T5, T6, T7>(LogMessage<T1, T2, T3, T4, T5, T6, T7> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
        DString Log<T1, T2, T3, T4, T5, T6>(LogMessage<T1, T2, T3, T4, T5, T6> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
        DString Log<T1, T2, T3, T4, T5>(LogMessage<T1, T2, T3, T4, T5> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
        DString Log<T1, T2, T3, T4>(LogMessage<T1, T2, T3, T4> msg, T1 arg1, T2 arg2, T3 arg3, T4 arg4);
        DString Log<T1, T2, T3>(LogMessage<T1, T2, T3> msg, T1 arg1, T2 arg2, T3 arg3);
        DString Log<T1, T2>(LogMessage<T1, T2> msg, T1 arg1, T2 arg2);
        DString Log<T1>(LogMessage<T1> msg, T1 arg1);
    }
}