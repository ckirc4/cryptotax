﻿using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Services.Logging
{
    public delegate IEnumerable<DString> MessageGenerator();
    public delegate IEnumerable<DString> MessageGenerator<T1>(T1 arg1);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2>(T1 arg1, T2 arg2);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15);
    public delegate IEnumerable<DString> MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15, T16 arg16);

    public delegate LogMessage Collapsor<T1>(T1 arg1);
    public delegate LogMessage Collapsor<T1, T2>(T1 arg1, T2 arg2);
    public delegate LogMessage Collapsor<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);
    public delegate LogMessage Collapsor<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15);
    public delegate LogMessage Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>(T1 arg1, T2 arg2, T3 arg3, T4 arg4, T5 arg5, T6 arg6, T7 arg7, T8 arg8, T9 arg9, T10 arg10, T11 arg11, T12 arg12, T13 arg13, T14 arg14, T15 arg15, T16 arg16);

    public partial class LogMessage
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        /// <summary>
        /// Generates a message, possibly with formatting.
        /// </summary>
        public readonly MessageGenerator Generator;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Generator = generator;
        }
    }

    public class LogMessage<T1>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1) => new LogMessage(severity, name, () => generator(arg1));
        }
    }

    public class LogMessage<T1, T2>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2) => new LogMessage(severity, name, () => generator(arg1, arg2));
        }
    }

    public class LogMessage<T1, T2, T3>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3));
        }
    }

    public class LogMessage<T1, T2, T3, T4>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15));
        }
    }

    public class LogMessage<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16>
    {
        public readonly LoggingSeverity Severity;
        public readonly DString Name;
        public readonly Collapsor<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> Collapsor;

        public LogMessage(LoggingSeverity severity, DString name, MessageGenerator<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12, T13, T14, T15, T16> generator)
        {
            this.Severity = severity;
            this.Name = name;
            this.Collapsor = (arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16) => new LogMessage(severity, name, () => generator(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11, arg12, arg13, arg14, arg15, arg16));
        }
    }
}
