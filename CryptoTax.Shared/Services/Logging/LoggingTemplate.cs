﻿using CryptoTax.Shared.Attributes;

namespace CryptoTax.Shared.Services.Logging
{
    public enum LoggingTemplate
    {
        [ArgEnumValue("consoleVerbose", "console_verbose")]
        Console_Verbose,

        [ArgEnumValue("consoleProduction", "console_production")]
        Console_Production,

        [ArgEnumValue("persistorVerbose", "persistor_verbose")]
        Persistor_Verbose
    }
}
