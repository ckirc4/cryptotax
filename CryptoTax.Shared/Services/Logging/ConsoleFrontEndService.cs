﻿using System;
using ConsoleColour = System.ConsoleColor; // yes
using System.Collections.Generic;
using UtilityLibrary.Types;
using System.Linq;

namespace CryptoTax.Shared.Services.Logging
{
    public class ConsoleFrontEndService : IFrontEndService
    {
        private readonly Dictionary<Colour, ConsoleColour> _ColourMap = new Dictionary<Colour, ConsoleColour>()
        {
            { Colour.Undefined, Console.ForegroundColor },
            { Colour.Black, ConsoleColour.Black },
            { Colour.DarkBlue, ConsoleColour.DarkBlue },
            { Colour.DarkGreen, ConsoleColour.DarkGreen },
            { Colour.DarkCyan, ConsoleColour.DarkCyan },
            { Colour.DarkRed, ConsoleColour.DarkRed },
            { Colour.DarkMagenta, ConsoleColour.DarkMagenta },
            { Colour.DarkYellow, ConsoleColour.DarkYellow },
            { Colour.Gray, ConsoleColour.Gray },
            { Colour.DarkGray, ConsoleColour.DarkGray },
            { Colour.Blue, ConsoleColour.Blue },
            { Colour.Green, ConsoleColour.Green },
            { Colour.Cyan, ConsoleColour.Cyan },
            { Colour.Red, ConsoleColour.Red },
            { Colour.Magenta, ConsoleColour.Magenta },
            { Colour.Yellow, ConsoleColour.Yellow },
            { Colour.White, ConsoleColour.White },
        };

        private readonly object _ConsoleLock;

        public ConsoleFrontEndService()
        {
            this._ConsoleLock = new object();
        }

        public void WriteLines(IEnumerable<DString> formattedLines)
        {
            if (!formattedLines.Any()) return;
            this.writeToConsole(string.Join("\n", formattedLines) + "\n");
        }

        private void writeToConsole(DString formattedText)
        {
            IEnumerable<(Colour, DString)> formatted = ColourHelpers.ExtractFormatting(formattedText);

            lock (this._ConsoleLock)
            {
                foreach ((Colour colour, DString text) in formatted)
                {
                    Console.ForegroundColor = this._ColourMap[colour];
                    Console.Write(text);
                }

                // always reset colour
                Console.ForegroundColor = this._ColourMap[Colour.Undefined];
            }
        }
    }
}
