﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared
{
    public static class Constants
    {
        /// <summary>
        /// API key for the REACTSCAN account
        /// </summary>
        public const string API_KEY_ETHERSCAN = "TK39QCQK4ZE7THKY6WEFSCS5UGDJ378FXI";
        public const string API_KEY_ARBISCAN = "MDQGSPVSMH9S2X3MKRYWSC19PHTAI28C47";
        public const string API_KEY_BITMETX = "stp0BPV4yKIuUj5B08IpNxvG";
        public const string API_SECRET_BITMETX = "bxxxM_6uSp9BzKypUdfj4g5T8Th-89GKzC1IplUTpUZCueOV";
        public const string API_KEY_FTX = "-5JrhPPE8XkGgfUjQagagTrP4dNsDuksG4Z_q3WU";
        public const string API_SECRET_FTX = "Gp4JNMryxz76OUCcaefuPd-XrfkcA4x1lYIvR14u";

        // todo: provide different paths for debug mode, or even better, use injected constants
        private const string _BASE_DATA_PATH = @"C:\Users\chris\Google Drive\Other\Trading\Tax\Data\";
        public const string TRADE_LOGS_PATH = @"C:\Users\chris\Google Drive\Other\Trading\Tax\TradeLogs\";
        public const string CANDLESTICK_DATA_PATH = _BASE_DATA_PATH + @"Candlesticks\";
        public const string ID_DATA_PATH = _BASE_DATA_PATH;
        public const string EXCHANGE_DATA_CACHE_PATH = _BASE_DATA_PATH + @"ExchangeDtoCache\";
        public static readonly Func<DString, DString> OUTPUT_DATA_PATH = (runId) => _BASE_DATA_PATH + $@"Output\{runId}\";
        public static readonly DString RUNNER_OUTPUT_PATH = _BASE_DATA_PATH + $@"Runner\";
        public static readonly Func<DString, DString> INVENTORIES_DATA_PATH = (runId) => _BASE_DATA_PATH + OUTPUT_DATA_PATH(runId) + @"Inventories\";

        public const string TEMP_SAFE_FILE_EXTENSION = "._SAFE";

        public const Boolean DRY_RUN = false;
    }
}
