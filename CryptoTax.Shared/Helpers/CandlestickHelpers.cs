﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System;
using System.Collections.Generic;
using UtilityLibrary.Timing;

namespace CryptoTax.Shared.Helpers
{
    public static class CandlestickHelpers
    {
        public static DateTime GetStartTime(CandlestickInterval interval, DateTime timeUTC)
        {
            return timeUTC.Round(new TimeSpan(IntervalToTicks(interval)), -1);
        }

        public static DateTime GetNextTime(CandlestickInterval interval, DateTime timeUTC)
        {
            return GetTimeAfterIntervals(interval, timeUTC, 1);
        }

        public static DateTime GetPreviousTime(CandlestickInterval interval, DateTime timeUTC)
        {
            return GetTimeAfterIntervals(interval, timeUTC, -1);
        }

        /// <summary>
        /// Assumes the time lies on the start time of a candlestick.
        /// </summary>
        public static DateTime GetTimeAfterIntervals(CandlestickInterval interval, DateTime timeUTC, int N)
        {
            return timeUTC.AddTicks(IntervalToTicks(interval) * N);
        }

        /// <summary>
        /// Gets the number of intervals between the two times. Assumes the times lie on the start time of a candlestick.
        /// When from and to are the same, returns 0.
        /// </summary>
        public static int GetIntervalsBetween(DateTime from, DateTime to, CandlestickInterval interval)
        {
            return (int)((to - from).Ticks / IntervalToTicks(interval));
        }

        private static readonly Dictionary<CandlestickInterval, long> _Ticks = new Dictionary<CandlestickInterval, long>()
        {
            { CandlestickInterval.Minute_1, (long)60 * 10000 * 1000 },
            { CandlestickInterval.Hour_1, (long)3600 * 10000 * 1000 },
            { CandlestickInterval.Day_1, (long)24 * 3600 * 10000 * 1000 }
        };
        public static long IntervalToTicks(CandlestickInterval interval)
        {
            return _Ticks[interval];
        }
    }
}
