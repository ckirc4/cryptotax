﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;

namespace CryptoTax.Shared.Helpers
{
    public static class BlockchainHelpers
    {
        public static Currency GetNativeCurrency(BlockchainType blockchainType)
        {
            switch (blockchainType)
            {
                case BlockchainType.Bitcoin:
                    return KnownCurrencies.BTC;
                case BlockchainType.Ethereum:
                    return KnownCurrencies.ETH;
                case BlockchainType.ArbitrumNova:
                    return KnownCurrencies.ETH_ARB_NOVA;
                case BlockchainType.XRPL:
                    return KnownCurrencies.XRP;
                case BlockchainType.EthereumClassic:
                case BlockchainType.Rinkeby:
                case BlockchainType.USDT:
                case BlockchainType.BinanceChain:
                case BlockchainType.Undefined:
                default:
                    throw new AssertUnreachable(blockchainType, "Can't get native currency for the given blockchain");
            }
        }
    }
}
