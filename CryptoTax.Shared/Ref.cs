﻿using System;

namespace CryptoTax.Shared
{
    /// <summary>
    /// Reference wrapper around a value type.
    /// </summary>
    public class Ref<T> where T : struct
    {
        private T? _Underlying;

        /// <summary>
        /// Initialise an empty ref.
        /// </summary>
        public Ref()
        {
            this._Underlying = null;
        }

        /// <summary>
        /// Initialise using a value.
        /// </summary>
        public Ref(T initialValue)
        {
            this._Underlying = initialValue;
        }

        public bool Has()
        {
            return this._Underlying.HasValue;
        }

        /// <summary>
        /// Gets a copy of the underlying value. Assumes a value has been set.
        /// </summary>
        public T Get()
        {
            return this._Underlying.Value;
        }

        /// <summary>
        /// Sets the underlying type.
        /// </summary>
        public void Set(T newValue)
        {
            this._Underlying = newValue;
        }

        /// <summary>
        /// Update the value, used to get and set in a single transaction. Assumes a value has been set.
        /// </summary>
        /// <param name="updater">The function whose input is the current value, and whose output should be the new value.</param>
        public void Update(Func<T, T> updater)
        {
            // overkill but fun!
            this._Underlying = updater(this._Underlying.Value);
        }
    }
}
