﻿using System.Collections.Generic;
using UtilityLibrary.Types;
using System.Linq;
using CryptoTax.Shared.Enums;

namespace CryptoTax.Shared
{
    public static class KnownCurrencies
    {
        public static readonly Currency AUD = new Currency("Australian Dollar", "AUD", true);
        public static readonly Currency USDT = new Currency("Tether", "USDT", true, BlockchainType.USDT);
        public static readonly Currency BUSD = new Currency("Binancechain US Dollar", "BUSD", true, BlockchainType.BinanceChain);
        public static readonly Currency TUSD = new Currency("True USD", "TUSD", true);
        public static readonly Currency USDC = new Currency("USDC", "USDC", true);
        public static readonly Currency USD = new Currency("United States Dollar/Stablecoin", "USD", true);
        public static readonly Currency BTC = new Currency("Bitcoin", "BTC", false, BlockchainType.Bitcoin);
        public static readonly Currency ETH = new Currency("Ethereum", "ETH", false, BlockchainType.Ethereum);
        public static readonly Currency ETH_ARB_NOVA = new Currency("Ethereum (Arbitrum Nova)", "ETH", false, BlockchainType.ArbitrumNova);
        public static readonly Currency MOON = new Currency("r/CryptoCurrency Moons", "MOON", false, BlockchainType.ArbitrumNova); // contract: 0x0057ac2d777797d31cd3f8f13bf5e927571d6ad0
        public static readonly Currency XRP = new Currency("XRP", "XRP", false, BlockchainType.XRPL);
        public static readonly Currency LTC = new Currency("Litecoin", "LTC", false);
        public static readonly Currency FLR = new Currency("Flare", "FLR", false);
        public static readonly Currency BNB = new Currency("BinanceCoin", "BNB", false, BlockchainType.Ethereum);
        public static readonly Currency BOMB = new Currency("BOMB", "BOMB", false, BlockchainType.Ethereum);
        public static readonly Currency MBT = new Currency("Microbit", "MBT", false, BlockchainType.Ethereum);
        public static readonly Currency AMPL = new Currency("Ampleforth", "AMPL", false, BlockchainType.Ethereum);
        public static readonly Currency WETH = new Currency("Wrapped Ether", "WETH", false, BlockchainType.Ethereum);
        public static readonly Currency ETC = new Currency("Ethereum Classic", "ETC", false, BlockchainType.EthereumClassic);

        public static readonly IEnumerable<Currency> ERC20 = new List<Currency>() { ETH, BOMB, WETH, MOON };

        /// <summary>
        /// These are treated 1:1 as "USD" on FTX: https://help.ftx.com/hc/en-us/articles/360034865571-Blockchain-Deposits-and-Withdrawals
        /// </summary>
        public static readonly IEnumerable<Currency> FTX_STABLECOINS = new List<Currency>() { USD, USDC, TUSD, BUSD };

        private static readonly IEnumerable<Currency> _KnownCurrencies = new List<Currency>() { AUD, USDT, BUSD, TUSD, USDC, USD, BTC, ETH, ETH_ARB_NOVA, MOON, XRP, LTC, FLR, BNB, BOMB, MBT, WETH, ETC, AMPL };

        public static IEnumerable<Currency> GetAllCurrencies()
        {
            return _KnownCurrencies;
        }

        public static Currency? GetCurrency(DString symbol)
        {
            return _KnownCurrencies.Where(c => symbol == c.Symbol).Cast<Currency?>().FirstOrDefault();
        }

        /// <summary>
        /// If possible, gets the known currency. Otherwise, creates a new currency with default entries.
        /// </summary>
        public static Currency TryGetCurrency(DString symbol, bool assumeIsFiat = false)
        {
            Currency? knownCurrency = GetCurrency(symbol);
            if (knownCurrency == null) return new Currency(null, symbol, assumeIsFiat);
            else return (Currency)knownCurrency;
        }

        public static bool IsKnownCurrency(DString symbol)
        {
            return GetCurrency(symbol) != null;
        }
        public static bool IsFtxStablecoin(NString symbol, bool includeUSD)
        {
            if (!includeUSD && symbol == "USD") return false;
            return FTX_STABLECOINS.Any(c => c.Symbol == symbol);
        }
    }
}
