﻿using System;

namespace CryptoTax.Shared.Exceptions
{
    /// <summary>
    /// Use this only in a `default` clause of a `switch` statement.
    /// </summary>
    public class AssertUnreachable : Exception
    {
        public AssertUnreachable(Enum value) : base(generateMessage(value)) { }
        public AssertUnreachable(string value) : base(generateMessage(value)) { }

        public AssertUnreachable(Enum value, string message) : base(generateMessage(value, message)) { }
        public AssertUnreachable(string value, string message) : base(generateMessage(value, message)) { }

        private static string generateMessage(Enum value, string message = null)
        {
            return $"Illegal value '{value}' for enum type '{value.GetType().Name}'{(string.IsNullOrEmpty(message) ? "" : $": {message}")}";
        }

        private static string generateMessage(string value, string message = null)
        {
            return $"Illegal value '{value}'{(string.IsNullOrEmpty(message) ? "" : $": {message}")}";
        }
    }
}
