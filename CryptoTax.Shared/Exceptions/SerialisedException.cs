﻿using Newtonsoft.Json;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Exceptions
{
    public class SerialisedException
    {
        public DString SerialisedContent { get; set; }
        public DString ExceptionType { get; set; }

        public SerialisedException()
        { }

        public SerialisedException(DString serialisedContent, DString exceptionType)
        {
            this.SerialisedContent = serialisedContent;
            this.ExceptionType = exceptionType;
        }

        /// <exception cref="InvalidTypeException">Thrown if type <typeparamref name="T"/> is different than the serialised exception's type.</exception>
        public T ToException<T>() where T : Exception
        {
            DString expectedType = typeof(T).Name;
            if (expectedType != this.ExceptionType) throw new InvalidTypeException("Cannot convert the serialised exception to the specified exception type.", expectedType, this.ExceptionType);

            return JsonConvert.DeserializeObject<T>(this.SerialisedContent);
        }

        public static SerialisedException FromException<T>(T exception) where T : Exception
        {
            DString type = typeof(T).Name;
            DString content = JsonConvert.SerializeObject(exception);
            return new SerialisedException(content, type);
        }
    }
}
