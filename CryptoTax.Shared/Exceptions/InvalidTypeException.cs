﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Exceptions
{
    public class InvalidTypeException : Exception
    {
        public DString ExpectedType { get; }
        public DString ActualType { get; }

        public InvalidTypeException(string msg, DString expectedType, DString actualType) : base(msg)
        {
            this.ExpectedType = expectedType;
            this.ActualType = actualType;
        }

        public InvalidTypeException(string msg, Type expectedType, Type actualType) : base(msg)
        {
            this.ExpectedType = expectedType.Name;
            this.ActualType = actualType.Name;
        }
    }
}
