﻿using System;
using UtilityLibrary.Types;

namespace CryptoTax.Shared.Exceptions
{
    public class FileAlreadyExistsException : Exception
    {
        public DString FileName { get; set; }

        public FileAlreadyExistsException(DString fileName, NString msg = default)
            : base($"File {fileName} already exists: '{msg}'")
        {
            this.FileName = fileName;
        }
    }
}
