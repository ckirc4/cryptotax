﻿using System;

namespace CryptoTax.Shared.Exceptions
{
    /// <summary>
    /// Use this somewhere in an if/else clause if you know that, given the current implementation, the code block can never be reached. Do not use this for `switch` statements - we have <see cref="AssertUnreachable"/> for that.
    /// </summary>
    public class UnreachableCodeException : Exception
    {
        public UnreachableCodeException(string msg)
            : base(msg)
        { }
    }
}
