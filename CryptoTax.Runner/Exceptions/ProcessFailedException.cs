﻿using CryptoTax.Runner.Models;
using System;

namespace CryptoTax.Runner.Exceptions
{
    public class ProcessFailedException : Exception
    {
        public ProcessResult ProcessResult { get; }

        public ProcessFailedException(ProcessResult result) : base($"Exit code was {result.ExitCode} for args '{result.Args}'")
        {
            this.ProcessResult = result;
        }
    }
}
