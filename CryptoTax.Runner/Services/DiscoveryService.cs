﻿using CryptoTax.DTO;
using CryptoTax.Runner.Exceptions;
using CryptoTax.Runner.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Runner.Services
{
    public class DiscoveryService
    {
        private readonly DiscoverySettings _Settings;
        private readonly DString _Executable;
        private readonly IProcessService _ProcessService;
        private readonly IResultsService _ResultsService;

        public DiscoveryService(DString executable, DiscoverySettings settings, IProcessService processService, IResultsService resultsService)
        {
            this._Settings = settings;
            this._Executable = executable;
            this._ProcessService = processService;
            this._ResultsService = resultsService;
        }

        public Dictionary<Configuration, AggregateResult> DoDiscovery(bool enableReporting, bool showConsoleWindow, bool enableLogging, int? loadStateFromRunNumber)
        {
            FinancialYear fromYear = this._Settings.FinancialYears.OrderBy(fy => fy.Start).First();
            FinancialYear toYear = this._Settings.FinancialYears.OrderBy(fy => fy.Start).Last();

            Dictionary<Configuration, AggregateResult> results = new Dictionary<Configuration, AggregateResult>();
            foreach (InventoryType inventoryType in this._Settings.InventoryTypes)
            {
                foreach (CandlestickInterval candlestickInterval in this._Settings.CandlestickIntervals)
                {
                    foreach (PriceCalculationType priceCalculationType in this._Settings.PriceCalculationTypes)
                    {
                        foreach (InventoryManagementType inventoryManagementType in this._Settings.InventoryManagementTypes)
                        {
                            Configuration configuration = new Configuration(inventoryType, candlestickInterval, priceCalculationType, inventoryManagementType);
                            AggregateResult result = this.runWithConfiguration(fromYear, toYear, configuration, enableReporting, showConsoleWindow, enableLogging, loadStateFromRunNumber);
                            results.Add(configuration, result);
                        }
                    }
                }
            }

            return results;
        }

        private AggregateResult runWithConfiguration(
            FinancialYear fromYear,
            FinancialYear toYear,
            Configuration configuration,
            bool enableReporting,
            bool showConsoleWindow,
            bool enableLogging,
            int? loadStateFromRunNumber)
        {
            Dictionary<FinancialYear, ProcessResult> runs = new Dictionary<FinancialYear, ProcessResult>();

            string args = Helpers.ConstructArgs(fromYear, configuration.InventoryType, configuration.CandlestickInterval, configuration.PriceCalculationType, configuration.InventoryManagementType, enableReporting, showConsoleWindow, enableLogging, loadStateFromRunNumber);
            runs.Add(fromYear, this.runProcess(args, showConsoleWindow));

            for (FinancialYear year = fromYear + 1; year <= toYear; year++)
            {
                int prevRun = runs[year - 1].ExitCode;
                runs.Add(year, this.runProcess(Helpers.ConstructArgs(prevRun), showConsoleWindow));
            }

            Dictionary<FinancialYear, SingleResult> results = new Dictionary<FinancialYear, SingleResult>();
            foreach (KeyValuePair<FinancialYear, ProcessResult> kvp in runs)
            {
                int runId = kvp.Value.ExitCode;
                CalculationResultsDTO calculationResults = this._ResultsService.GetResultsForRun(runId);
                results.Add(kvp.Key, new SingleResult(kvp.Value, calculationResults));
            }

            return this._ResultsService.GetAggregateResult(configuration, results);
        }

        /// <summary>
        /// Returns the positive run number after the process completes.
        /// </summary>
        /// <exception cref="ProcessFailedException"/>
        private ProcessResult runProcess(string args, bool showConsoleWindow)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo()
            {
                FileName = this._Executable,
                CreateNoWindow = !showConsoleWindow,
                WindowStyle = showConsoleWindow ? ProcessWindowStyle.Maximized : ProcessWindowStyle.Hidden,
                Arguments = args
            };

            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Now running process with args {args}");
            Console.Title = args;
            ProcessResult result = this._ProcessService.RunProcessToCompletion(startInfo);
            if (result.ExitCode <= 0) throw new ProcessFailedException(result);
            return result;
        }
    }
}
