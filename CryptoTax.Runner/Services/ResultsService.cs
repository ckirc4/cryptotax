﻿using CryptoTax.DTO;
using CryptoTax.Runner.Models;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.Runner.Services
{
    public class ResultsService : IResultsService
    {
        private readonly IFileService _FileService;
        private readonly Func<DString, DString> _RunOutputFolder;
        private readonly DString _DiscoveryOutputFolder;

        public ResultsService(IFileService fileService, Func<DString, DString> runOutputFolder, DString discoveryOutputFolder)
        {
            this._FileService = fileService;
            this._RunOutputFolder = runOutputFolder;
            this._DiscoveryOutputFolder = discoveryOutputFolder.Value.EndsWith(@"\") ? discoveryOutputFolder : discoveryOutputFolder + @"\";
        }

        public CalculationResultsDTO GetResultsForRun(int runId)
        {
            DString file = this._RunOutputFolder($"#n{runId}") + "Results.json";
            return this._FileService.ReadAndDeserialiseFile<CalculationResultsDTO>(file);
        }

        public void SaveDiscoveryResults(Dictionary<Configuration, AggregateResult> results)
        {
            // write raw results
            string contents = JsonConvert.SerializeObject(results, Formatting.Indented);
            this._FileService.WriteFile(this._DiscoveryOutputFolder + "DiscoveryResults.json", contents, false);

            // write human-friendly summary file with table
            // this is for UX purposes only - all information is sourced from the raw results.
            string years = string.Join(", ", results.First().Value.Results.Select(r => r.Key.Name));
            IEnumerable<NString> lines = new NString[] { $"The following table summarises the results for the years {years}, sorted by the sum of 'TaxPayableOn' - Losses Carried Forward:", "" };

            foreach (KeyValuePair<Configuration, AggregateResult> result in results.OrderBy(r => r.Value.TaxPayableOn.Sum(t => t.Value) + r.Value.CapitalLossesToCarryForward))
            {
                string configIndices = getIndicesAsString(result.Key);
                string yearlyResults = string.Join(" | ", result.Value.TaxPayableOn.Select(t => t.Value.ToString("0.00").PadLeft(9)));
                string carryForward = result.Value.CapitalLossesToCarryForward.ToString("0.00");
                lines = lines.Append($"{configIndices}: {yearlyResults} => {carryForward}");
            }

            lines = lines.Append("").Append("").Append("Keys:");
            lines = lines.Append($"[{nameof(InventoryType)}, {nameof(CandlestickInterval)}, {nameof(PriceCalculationType)}, {nameof(InventoryManagementType)}]");
            lines = lines.Append("");
            lines = lines.Concat(printEnumIndices<InventoryType>());
            lines = lines.Concat(printEnumIndices<CandlestickInterval>());
            lines = lines.Concat(printEnumIndices<PriceCalculationType>());
            lines = lines.Concat(printEnumIndices<InventoryManagementType>());

            this._FileService.WriteLines(this._DiscoveryOutputFolder + "SummaryResults.txt", lines, false);
            Console.Write(string.Join("\n", lines.Select(line => line.Value)));
        }

        public AggregateResult GetAggregateResult(Configuration configuration, Dictionary<FinancialYear, SingleResult> results)
        {
            Dictionary<FinancialYear, decimal> taxPayableOn = new Dictionary<FinancialYear, decimal>();

            decimal cg = 0;
            decimal cl = 0;
            decimal ai = 0;
            foreach (KeyValuePair<FinancialYear, SingleResult> kvp in results.OrderBy(r => r.Key.Start))
            {
                FinancialYear year = kvp.Key;
                SingleResult result = kvp.Value;

                // carry forward losses from previous year(s)
                decimal thisNet = cl + result.NetCapitalGains;
                decimal taxableCapitalGains = thisNet < 0 ? 0 : thisNet;
                taxPayableOn.Add(year, taxableCapitalGains + result.AssessableIncome);

                // losses may reset, but gains and income are only additive
                cl = 0;
                if (thisNet < 0) cl = thisNet;
                else cg += thisNet;

                ai += result.AssessableIncome;
            }

            return new AggregateResult(configuration, results, taxPayableOn, cg, cl, ai);
        }

        private static IEnumerable<NString> printEnumIndices<T>() where T: Enum
        {
            IEnumerable<NString> result = new NString[] { $"{typeof(T).Name}:" };
            int i = 0;
            foreach (T x in getOrderedEnumValues<T>())
            {
                result = result.Append($"  - {i}: {x}");
                i++;
            }
            result = result.Append("");

            return result;
        }

        private static DString getIndicesAsString(Configuration config)
        {
            int[] indices = new int[]
            {
                getEnumIndex(config.InventoryType),
                getEnumIndex(config.CandlestickInterval),
                getEnumIndex(config.PriceCalculationType),
                getEnumIndex(config.InventoryManagementType)
            };

            return $"[{string.Join(",", indices.Select(i => i.ToString()))}]";
        }

        private static int getEnumIndex<T>(T value) where T : Enum
        {
            int i = 0;
            foreach (T x in getOrderedEnumValues<T>())
            {
                if (value.Equals(x)) return i;
                else i++;
            }

            throw new Exception($"Cannot find enum value '{value}' of type '{typeof(T)}'");
        }

        private static IEnumerable<T> getOrderedEnumValues<T>() where T : Enum
        {
            return Enum.GetValues(typeof(T)).Cast<T>().OrderBy(v => v.ToString());
        }
    }
}
