﻿using CryptoTax.Runner.Models;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using ThreadState = System.Diagnostics.ThreadState;

namespace CryptoTax.Runner.Services
{
    public class ProcessService : IProcessService
    {
        // todo UTIL-18: refactor into re-useable code
        // https://docs.microsoft.com/en-us/windows/console/setconsolectrlhandler
        [DllImport("Kernel32")]
        private static extern bool SetConsoleCtrlHandler(SetConsoleCtrlEventHandler handler, bool add);

        // https://docs.microsoft.com/en-us/windows/console/handlerroutine
        private delegate bool SetConsoleCtrlEventHandler(CtrlType sig);
        private enum CtrlType
        {
            CTRL_C_EVENT = 0,
            CTRL_BREAK_EVENT = 1,
            CTRL_CLOSE_EVENT = 2,
            CTRL_LOGOFF_EVENT = 5,
            CTRL_SHUTDOWN_EVENT = 6
        }

        private Process _Process;

        public ProcessService()
        {
            // add a callback that will be invoked when the console application exits
            SetConsoleCtrlHandler(this.onApplicationExit, true);
        }

        public ProcessResult RunProcessToCompletion(ProcessStartInfo startInfo)
        {
            int exitCode = 0;
            DateTime startTime = DateTime.Now;

            using (this._Process = Process.Start(startInfo))
            {
                try
                {
                    this._Process.WaitForExit();
                    exitCode = this._Process.ExitCode;
                }
                catch (Exception e)
                {
                    // for some reason neither `using` nor `process.Dispose()` properly closes the CryptoTax application
                    Console.WriteLine("ERROR - KILLING PROCESS: " + e.Message);
                    this._Process.Kill();
                }
            }

            TimeSpan duration = DateTime.Now - startTime;
            return new ProcessResult(exitCode, startInfo.Arguments, duration);
        }

        /// <summary>
        /// Make sure any running processes are automatically killed when we exit the application.
        /// </summary>
        private bool onApplicationExit(CtrlType type)
        {
            try
            {
                // without this, the process may never exit
                this._Process.Kill();
            }
            catch { /* don't care */ }

            return false;
        }

        private static bool anyThreadWaitingForInput(ProcessThreadCollection threads)
        {
            foreach (ProcessThread thread in threads)
            {
                if (thread.ThreadState == ThreadState.Wait && thread.WaitReason == ThreadWaitReason.UserRequest) return true;
            }

            return false;
        }
    }
}
