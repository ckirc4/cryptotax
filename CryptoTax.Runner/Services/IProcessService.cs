﻿using CryptoTax.Runner.Models;
using System.Diagnostics;

namespace CryptoTax.Runner.Services
{
    public interface IProcessService
    {
        ProcessResult RunProcessToCompletion(ProcessStartInfo startInfo);
    }
}