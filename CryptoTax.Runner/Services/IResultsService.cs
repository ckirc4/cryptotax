﻿using CryptoTax.DTO;
using CryptoTax.Runner.Models;
using CryptoTax.Shared;
using System.Collections.Generic;

namespace CryptoTax.Runner.Services
{
    public interface IResultsService
    {
        AggregateResult GetAggregateResult(Configuration configuration, Dictionary<FinancialYear, SingleResult> results);
        CalculationResultsDTO GetResultsForRun(int runId);
        void SaveDiscoveryResults(Dictionary<Configuration, AggregateResult> results);
    }
}