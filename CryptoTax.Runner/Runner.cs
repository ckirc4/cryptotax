﻿using CryptoTax.Runner.Models;
using CryptoTax.Runner.Services;
using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CryptoTax.Runner
{
    class Runner
    {
        private const bool _ENABLE_REPORTING = true;
        private const bool _ENABLE_PERSIST_LOG = true;
        private const bool _DONT_SHOW_CONSOLE_WINDOW = false;
        private static readonly int? _LOAD_STATE_FROM_RUN_NUMBER = 7;

        static void Main(string[] args)
        {
            string mode = "release";
            #if DEBUG
            mode = "debug";
            #endif

            DirectoryInfo folder = new DirectoryInfo(Directory.GetCurrentDirectory());
            FileInfo executable = folder.Parent.Parent.Parent.EnumerateFiles($@"CryptoTax\bin\{mode}\CryptoTax.exe").Single();

            ICryptoService cryptoService = new CryptoService();
            IFileService fileService = new FileService(cryptoService);
            ResultsService resultsService = new ResultsService(fileService, Constants.OUTPUT_DATA_PATH, Constants.RUNNER_OUTPUT_PATH);
            ProcessService processService = new ProcessService();

            DiscoveryService discoveryService = new DiscoveryService(
                executable.FullName,
                getFullSettings(),
                processService,
                resultsService);

            bool showConsoleWindow = !_DONT_SHOW_CONSOLE_WINDOW && mode == "debug";
            Dictionary<Configuration, AggregateResult> results = discoveryService.DoDiscovery(_ENABLE_REPORTING, showConsoleWindow, _ENABLE_PERSIST_LOG, _LOAD_STATE_FROM_RUN_NUMBER);
            resultsService.SaveDiscoveryResults(results);

            Console.WriteLine("\nDone...");
            Console.ReadLine();
        }

        private static DiscoverySettings getTestSettings()
        {
            IEnumerable<FinancialYear> financialYears = new[] { FinancialYear.Y2023 };
            IEnumerable<InventoryType> inventoryTypes = new[] { InventoryType.FIFO };
            IEnumerable<CandlestickInterval> candlestickIntervals = new[] { CandlestickInterval.Day_1 };
            IEnumerable<PriceCalculationType> priceCalculationTypes = new[] { PriceCalculationType.Close };
            IEnumerable<InventoryManagementType> inventoryManagementTypes = new[] { InventoryManagementType.Single };

            return new DiscoverySettings(financialYears, inventoryTypes, candlestickIntervals, priceCalculationTypes, inventoryManagementTypes);
        }

        private static DiscoverySettings getFullSettings()
        {
            IEnumerable<FinancialYear> financialYears = new[] { FinancialYear.Y2023 };
            IEnumerable<InventoryType> inventoryTypes = new[] { InventoryType.FILO, InventoryType.FIFO, InventoryType.Min, InventoryType.Max, InventoryType.Oldest, InventoryType.Newest };
            IEnumerable<CandlestickInterval> candlestickIntervals = new[] { CandlestickInterval.Minute_1, CandlestickInterval.Hour_1, CandlestickInterval.Day_1 };
            IEnumerable<PriceCalculationType> priceCalculationTypes = new[] { PriceCalculationType.Open, PriceCalculationType.Close, PriceCalculationType.ArithmeticMean, PriceCalculationType.TimeWeightedAverage };
            IEnumerable<InventoryManagementType> inventoryManagementTypes = new[] { InventoryManagementType.Single };

            return new DiscoverySettings(financialYears, inventoryTypes, candlestickIntervals, priceCalculationTypes, inventoryManagementTypes);
        }

        private static DiscoverySettings getOptimalSettings()
        {
            // calculated on 26-27/09/2021 by running all 168 possible combinations. a handful of InventoryType.Average runs were more profitable, but it turns out that this is generally prohibited, and now we know why!
            IEnumerable<FinancialYear> financialYears = new[] { FinancialYear.Y2016, FinancialYear.Y2017, FinancialYear.Y2018, FinancialYear.Y2019, FinancialYear.Y2020 };
            IEnumerable<InventoryType> inventoryTypes = new[] { InventoryType.Max };
            IEnumerable<CandlestickInterval> candlestickIntervals = new[] { CandlestickInterval.Day_1 };
            IEnumerable<PriceCalculationType> priceCalculationTypes = new[] { PriceCalculationType.Close };
            IEnumerable<InventoryManagementType> inventoryManagementTypes = new[] { InventoryManagementType.Single };

            return new DiscoverySettings(financialYears, inventoryTypes, candlestickIntervals, priceCalculationTypes, inventoryManagementTypes);
        }
    }
}
