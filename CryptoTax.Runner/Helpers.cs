﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System.Linq;

namespace CryptoTax.Runner
{
    public static class Helpers
    {
        /// <summary>
        /// Construct a new configuration from scratch.
        /// </summary>
        public static string ConstructArgs(
            FinancialYear year,
            InventoryType inventoryType,
            CandlestickInterval candlestickInterval,
            PriceCalculationType priceCalculationType,
            InventoryManagementType inventoryManagementType,
            bool enableReporting,
            bool consoleShown,
            bool enableLogging,
            int? state)
        {
            string[] args = new string[]
            {
                $"financialYear={year.Name}",
                $"inventoryType={inventoryType}",
                $"candlestickInterval={candlestickInterval}",
                $"priceCalculationType={priceCalculationType}",
                $"inventoryManagementType={inventoryManagementType}",
                $"enableReporting={enableReporting}",
                $"persistorLoggingTemplate={(enableLogging ? "persistorVerbose" : "null")}"
            };

            if (state.HasValue)
            {
                args = args.Append($"state=#n{state.Value}").ToArray();
            }
            if (!consoleShown)
            {
                args = args.Append($"consoleLoggingTemplate=null").ToArray();
            }

            return string.Join(" ", args);
        }

        /// <summary>
        /// Continue on from the given run ID.
        /// </summary>
        public static string ConstructArgs(int cont)
        {
            return $"cont=#n{cont}";
        }
    }
}
