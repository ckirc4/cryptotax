﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System.Collections.Generic;

namespace CryptoTax.Runner.Models
{
    public class DiscoverySettings
    {
        public IEnumerable<FinancialYear> FinancialYears { get; private set; }
        public IEnumerable<InventoryType> InventoryTypes { get; private set; }
        public IEnumerable<CandlestickInterval> CandlestickIntervals { get; private set; }
        public IEnumerable<PriceCalculationType> PriceCalculationTypes { get; private set; }
        public IEnumerable<InventoryManagementType> InventoryManagementTypes { get; private set; }

        public DiscoverySettings(
            IEnumerable<FinancialYear> financialYears,
            IEnumerable<InventoryType> inventoryTypes,
            IEnumerable<CandlestickInterval> candlestickIntervals,
            IEnumerable<PriceCalculationType> priceCalculationTypes,
            IEnumerable<InventoryManagementType> inventoryManagementTypes)
        {
            this.FinancialYears = financialYears;
            this.InventoryTypes = inventoryTypes;
            this.CandlestickIntervals = candlestickIntervals;
            this.PriceCalculationTypes = priceCalculationTypes;
            this.InventoryManagementTypes = inventoryManagementTypes;
        }
    }
}
