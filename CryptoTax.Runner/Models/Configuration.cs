﻿using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTax.Runner.Models
{
    public class Configuration
    {
        public InventoryType InventoryType { get; set; }
        public CandlestickInterval CandlestickInterval { get; set; }
        public PriceCalculationType PriceCalculationType { get; set; }
        public InventoryManagementType InventoryManagementType { get; set; }

        public Configuration(InventoryType inventoryType, CandlestickInterval candlestickInterval, PriceCalculationType priceCalculationType, InventoryManagementType inventoryManagementType)
        {
            this.InventoryType = inventoryType;
            this.CandlestickInterval = candlestickInterval;
            this.PriceCalculationType = priceCalculationType;
            this.InventoryManagementType = inventoryManagementType;
        }
    }
}
