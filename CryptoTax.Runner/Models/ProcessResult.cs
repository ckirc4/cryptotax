﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTax.Runner.Models
{
    public class ProcessResult
    {
        public int ExitCode { get; set; }
        public string Args { get; set; }
        public TimeSpan Duration { get; set; }

        public ProcessResult(int exitCode, string args, TimeSpan duration)
        {
            this.ExitCode = exitCode;
            this.Args = args;
            this.Duration = duration;
        }
    }
}
