﻿using CryptoTax.Shared;
using System.Collections.Generic;

namespace CryptoTax.Runner.Models
{
    public class AggregateResult
    {
        public Configuration Configuration { get; }
        public Dictionary<FinancialYear, SingleResult> Results { get; }
        public Dictionary<FinancialYear, decimal> TaxPayableOn { get; }

        /// <summary>
        /// Sum of each year's net capital gains (always after carrying forward the previous year's losses). Never negative.
        /// </summary>
        public decimal SumCapitalGains { get; }

        /// <summary>
        /// At the end of the aggregate period, the losses that are to be carried forward to the next year (if any). Never positive.
        /// </summary>
        public decimal CapitalLossesToCarryForward { get; }

        /// <summary>
        /// The sum of each year's assessable income. Never negative.
        /// </summary>
        public decimal SumAssessableIncome { get; }

        public AggregateResult(Configuration configuration, Dictionary<FinancialYear, SingleResult> results, Dictionary<FinancialYear, decimal> taxPayableOn, decimal sumCapitalGains, decimal capitalLossesToCarryForward, decimal sumAssessableIncome)
        {
            this.Configuration = configuration;
            this.Results = results;
            this.TaxPayableOn = taxPayableOn;
            this.SumCapitalGains = sumCapitalGains;
            this.CapitalLossesToCarryForward = capitalLossesToCarryForward;
            this.SumAssessableIncome = sumAssessableIncome;
        }
    }
}
