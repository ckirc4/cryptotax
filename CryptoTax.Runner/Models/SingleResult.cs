﻿using CryptoTax.DTO;

namespace CryptoTax.Runner.Models
{
    public class SingleResult
    {
        public ProcessResult ProcessResult { get; }
        public decimal StandardCG { get; }
        public decimal DiscountableCG { get; }
        public decimal CapitalLosses { get; }
        public decimal NetCapitalGains { get; }
        public decimal AssessableIncome { get; }

        public SingleResult(ProcessResult processResult, CalculationResultsDTO result)
        {
            this.ProcessResult = processResult;
            this.StandardCG = result.CapitalGains.StandardCapitalGains;
            this.DiscountableCG = result.CapitalGains.DiscountableCapitalGains;
            this.CapitalLosses = result.CapitalGains.CapitalLosses;
            this.NetCapitalGains = result.NetCapitalGains;
            this.AssessableIncome = result.AssessableIncome;
        }
    }
}
