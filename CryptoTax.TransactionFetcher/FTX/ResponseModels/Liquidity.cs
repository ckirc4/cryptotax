﻿namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public enum Liquidity
    {
        Maker,
        Taker
    }
}