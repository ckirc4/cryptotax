﻿namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxBaseResponse<T>
    {
        public bool Success { get; set; }
        public T Result { get; set; }
    }
}
