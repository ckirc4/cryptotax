﻿namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public enum FillType
    {
        Order,
        OTC,
        Liquidation,
        /// <summary>
        /// Only possible for futures
        /// </summary>
        Expiration
    }
}