﻿namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public enum WithdrawalStatus
    {
        Requested,
        Processing,
        Complete,
        Cancelled
    }
}