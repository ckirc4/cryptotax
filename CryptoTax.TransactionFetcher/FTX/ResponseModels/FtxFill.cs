﻿using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxFill
    {
        public decimal Fee { get; set; }
        public string FeeCurrency { get; set; }
        public decimal FeeRate { get; set; }
        /// <summary>
        /// Only for futures - e.g. BTC-PERP
        /// </summary>
        public string Future { get; set; }
        public long Id { get; set; }
        public Liquidity Liquidity { get; set; }
        /// <summary>
        /// Only for spot - e.g. XRP/USD
        /// </summary>
        public string Market { get; set; }

        /// <summary>
        /// Spot only
        /// </summary>
        public string BaseCurrency { get; set; }
        /// <summary>
        /// Spot only
        /// </summary>
        public string QuoteCurrency { get; set; }

        /// <summary>
        /// Null for liquidations.
        /// </summary>
        public long? OrderId { get; set; }
        /// <summary>
        /// Only for later orders.
        /// </summary>
        public long? TradeId { get; set; }
        public decimal Price { get; set; }
        public OrderSide Side { get; set; }
        public decimal Size { get; set; }
        public DateTime Time { get; set; }
        public FillType Type { get; set; }
    }
}
