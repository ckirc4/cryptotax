﻿using System.Collections.Generic;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxPnlHistory
    {
        // each day (timestamp, in decimal seconds formatted as string) has a list of positions with profit/loss (in USD)
        public Dictionary<string, Dictionary<string, decimal>> PnlByDay { get; set; }
        public decimal EarliestDay { get; set; }
        public decimal Today { get; set; }
        public Dictionary<string, decimal> TotalPnl { get; set; }
    }
}
