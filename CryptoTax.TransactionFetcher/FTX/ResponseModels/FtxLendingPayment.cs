﻿using System;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxLendingPayment
    {
        public string Coin { get; set; }
        /// <summary>
        /// Amount of Coin received - positive.
        /// </summary>
        public decimal Proceeds { get; set; }
        public decimal Rate { get; set; }
        /// <summary>
        /// Amount lent.
        /// </summary>
        public decimal Size { get; set; }
        public DateTime Time { get; set; }
    }
}
