﻿namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public enum DepositStatus
    {
        Confirmed,
        Unconfirmed,
        Cancelled
    }
}