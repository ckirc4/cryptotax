﻿using System;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxFundingPayment
    {
        public string Future { get; set; }
        public long Id { get; set; }
        public decimal Payment { get; set; }
        public DateTime Time { get; set; }
    }
}
