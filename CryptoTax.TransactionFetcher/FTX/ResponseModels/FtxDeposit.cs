﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxDeposit
    {
        public string Coin { get; set; }
        public int Confirmations { get; set; }
        public DateTime ConfirmedTime { get; set; }
        /// <summary>
        /// Not included in size.
        /// </summary>
        public decimal Fee { get; set; }
        /// <summary>
        /// Deposit id.
        /// </summary>
        public long Id { get; set; }
        public DateTime SentTime { get; set; }
        /// <summary>
        /// Does not include fee.
        /// </summary>
        public decimal Size { get; set; }
        public DepositStatus Status { get; set; }
        public DateTime Time { get; set; }
        /// <summary>
        /// Only defined if status is confirmed?
        /// </summary>
        public string TxId { get; set; }
        public string Notes { get; set; }
    }
}
