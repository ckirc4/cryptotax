﻿using System;

namespace CryptoTax.TransactionFetcher.FTX.ResponseModels
{
    public class FtxWithdrawal
    {
        public string Coin { get; set; }
        public string Address { get; set; }
        public string Tag { get; set; }
        /// <summary>
        /// Fee is not included in size.
        /// </summary>
        public decimal Fee { get; set; }
        /// <summary>
        /// Withdrawal id.
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Size does not include fee.
        /// </summary>
        public decimal Size { get; set; }
        public WithdrawalStatus Status { get; set; }
        public DateTime Time { get; set; }
        public string TxId { get; set; }
        public string Notes { get; set; }
    }
}
