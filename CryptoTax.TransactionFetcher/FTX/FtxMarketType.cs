﻿namespace CryptoTax.TransactionFetcher.FTX
{
    public enum FtxMarketType
    {
        Spot,
        OTC,
        Futures
    }
}