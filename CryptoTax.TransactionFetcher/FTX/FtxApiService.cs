﻿using CryptoTax.Shared;
using CryptoTax.TransactionFetcher.FTX.ResponseModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace CryptoTax.TransactionFetcher.FTX
{
    // see https://docs.ftx.com/
    public class FtxApiService
    {
        private const string _BASE_URL = "https://ftx.com/api";

        private readonly string _ApiKey;
        private readonly string _ApiSecret;

        public FtxApiService()
        {
            this._ApiKey = Constants.API_KEY_FTX;
            this._ApiSecret = Constants.API_SECRET_FTX;
        }

        public IEnumerable<FtxWithdrawal> GetWithdrawalHistory(DateTime fromUTC, DateTime toUTC)
        {
            IEnumerable<FtxWithdrawal> results = this.getCollection<FtxWithdrawal>(() => this.query("GET", "/wallet/withdrawals", this.getParams(fromUTC, toUTC)));
            return results;
        }

        public IEnumerable<FtxDeposit> GetDepositHistory(DateTime fromUTC, DateTime toUTC)
        {
            IEnumerable<FtxDeposit> results = this.getCollection<FtxDeposit>(() => this.query("GET", "/wallet/deposits", this.getParams(fromUTC, toUTC)));
            return results;
        }

        public IEnumerable<FtxFill> GetFills(DateTime fromUTC, DateTime toUTC)
        {
            IEnumerable<FtxFill> results = this.getCollection<FtxFill>(() => this.query("GET", "/fills", this.getParams(fromUTC, toUTC)));
            return results;
        }

        public IEnumerable<FtxFundingPayment> GetFundingPayments(DateTime fromUTC, DateTime toUTC)
        {
            IEnumerable<FtxFundingPayment> results = this.getCollection<FtxFundingPayment>(() => this.query("GET", "/funding_payments", this.getParams(fromUTC, toUTC)));
            return results;
        }

        public IEnumerable<FtxLendingPayment> GetLendingPayments(DateTime fromUTC, DateTime toUTC)
        {
            // there are ~10000 payments per year, but the API only returns a maximum of 5000 items
            List<FtxLendingPayment> results = new List<FtxLendingPayment>();
            DateTime from = fromUTC;
            DateTime to = toUTC;
            do
            {
                to = from.AddDays(100);
                if (to > toUTC) to = toUTC;

                results.AddRange(this.getCollection<FtxLendingPayment>(() => this.query("GET", "/spot_margin/lending_history", this.getParams(from, to))));

                from = to.AddSeconds(1); // ensure no overlap
            } while (to < toUTC);
            return results;
        }

        public FtxPnlHistory GetPnlHistory(DateTime fromUTC, DateTime toUTC)
        {
            FtxPnlHistory results = this.getSingle<FtxPnlHistory>(() => this.query("GET", "/pnl/historical_changes", this.getParams(fromUTC, toUTC)));
            return results;
        }

        private IEnumerable<T> getCollection<T>(Func<string> getFn)
        {
            IEnumerable<T> result = this.getSingle<IEnumerable<T>>(getFn);

            if (result.Count() >= 5000) throw new Exception("Pagination is required for this endpoint.");
            return result;
        }

        private T getSingle<T>(Func<string> getFn)
        {
            // make sure we ratelimit - about 1.5 seconds per request, to be on the safe side
            DateTime start = DateTime.Now;
            string raw = getFn();
            DateTime end = DateTime.Now;
            double durationMs = end.Subtract(start).TotalMilliseconds;
            if (durationMs < 1500)
            {
                Thread.Sleep(1500 - (int)durationMs);
            }

            FtxBaseResponse<T> thisResults = JsonConvert.DeserializeObject<FtxBaseResponse<T>>(raw);

            if (thisResults.Success == false) throw new Exception("Response was unsuccessful");
            return thisResults.Result;
        }

        private Dictionary<string, string> getParams(DateTime startTimeUTC, DateTime endTimeUTC)
        {
            return new Dictionary<string, string>()
            {
                { "start_time", new DateTimeOffset(startTimeUTC).ToUnixTimeSeconds().ToString() },
                { "end_time", new DateTimeOffset(endTimeUTC).ToUnixTimeSeconds().ToString() }
            };
        }

        private string buildQueryData(Dictionary<string, string> param)
        {
            if (param == null)
                return "";

            StringBuilder b = new StringBuilder();
            foreach (KeyValuePair<string, string> item in param)
                b.Append(string.Format("&{0}={1}", item.Key, WebUtility.UrlEncode(item.Value)));

            try { return b.ToString().Substring(1); }
            catch (Exception) { return ""; }
        }

        private string buildJSON(Dictionary<string, string> param)
        {
            if (param == null)
                return "";

            List<string> entries = new List<string>();
            foreach (KeyValuePair<string, string> item in param)
                entries.Add(string.Format("\"{0}\":\"{1}\"", item.Key, item.Value));

            return "{" + string.Join(",", entries) + "}";
        }

        private static string byteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private string query(string method, string function, Dictionary<string, string> param = null, bool auth = true, bool json = false)
        {
            string paramData = json ? this.buildJSON(param) : this.buildQueryData(param);
            string url = function + ((method == "GET" && paramData != "") ? "?" + paramData : "");
            string postData = (method != "GET") ? paramData : "";
            Console.WriteLine($"Making {method} request to {url}");

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_BASE_URL + url);
            webRequest.Method = method;

            if (auth)
            {
                string timestamp = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds().ToString();
                string message = timestamp + method + "/api" + url + postData;
                HMACSHA256 hashMaker = new HMACSHA256(Encoding.UTF8.GetBytes(_ApiSecret));
                byte[] hash = hashMaker.ComputeHash(Encoding.UTF8.GetBytes(message));
                string signatureString = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();

                webRequest.Headers.Add("FTX-TS", timestamp);
                webRequest.Headers.Add("FTX-KEY", _ApiKey);
                webRequest.Headers.Add("FTX-SIGN", signatureString);
            }

            try
            {
                if (postData != "")
                {
                    webRequest.ContentType = json ? "application/json" : "application/x-www-form-urlencoded";
                    byte[] data = Encoding.UTF8.GetBytes(postData);
                    using (Stream stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                using (Stream str = webResponse.GetResponseStream())
                using (StreamReader sr = new StreamReader(str))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                using (HttpWebResponse response = (HttpWebResponse)wex.Response)
                {
                    if (response == null)
                        throw;

                    using (Stream str = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(str))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }
        }

        private static byte[] hmacsha256(byte[] keyByte, byte[] messageBytes)
        {
            using (HMACSHA256 hash = new HMACSHA256(keyByte))
            {
                return hash.ComputeHash(messageBytes);
            }
        }
    }
}
