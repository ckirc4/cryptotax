﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using CryptoTax.TransactionFetcher.FTX.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.FTX
{
    public class FtxService
    {
        private readonly IFileService _FileService;
        private readonly FtxApiService _ApiService;

        public FtxService(IFileService fileService, FtxApiService apiService)
        {
            this._FileService = fileService;
            this._ApiService = apiService;
        }

        public void GetAndWriteAllTransactions(DString fileName, FinancialYear year)
        {
            DateTime fromUTC = year.Start.ToUniversalTime();
            DateTime toUTC = year.End.ToUniversalTime();

            IEnumerable<FtxDeposit> depositHistory = this._ApiService.GetDepositHistory(fromUTC, toUTC);
            IEnumerable<FtxWithdrawal> withdrawalHistory = this._ApiService.GetWithdrawalHistory(fromUTC, toUTC);
            IEnumerable<FtxFill> fills = this._ApiService.GetFills(fromUTC, toUTC);
            IEnumerable<FtxFundingPayment> fundingPayments = this._ApiService.GetFundingPayments(fromUTC, toUTC);
            IEnumerable<FtxLendingPayment> lendingPayments = this._ApiService.GetLendingPayments(fromUTC, toUTC);
            FtxPnlHistory pnlHistory = this._ApiService.GetPnlHistory(fromUTC, toUTC);

            IOrderedEnumerable<FtxRow> rows = depositHistory.Select(x => FtxRow.Create(x))
                .Concat(withdrawalHistory.Select(x => FtxRow.Create(x)))
                .Concat(fills.Select(x => FtxRow.Create(x)))
                .Concat(fundingPayments.Select(x => FtxRow.Create(x)))
                .Concat(lendingPayments.Select(x => FtxRow.Create(x)))
                .Concat(FtxRow.Create(pnlHistory))
                .OrderBy(r => r.Time);
            IEnumerable<NString> csvRows = rows.Select(r => new NString(r.ToCsv())).ToList();
            this._FileService.WriteLines(fileName, new NString[] { FtxRow.CsvHeader() }.Concat(csvRows), false);
        }
    }
}
