﻿namespace CryptoTax.TransactionFetcher.FTX
{
    public enum RowType
    {
        Deposit,
        Withdrawal,
        /// <summary>
        /// Includes Spot, OTC, and Futures
        /// </summary>
        Trade,
        FundingPayment,
        LendingPayment,
        /// <summary>
        /// Conducted one second before midnight, agreggates profit/loss of futures positions and trades for the day, including fees and funding payments.
        /// </summary>
        PnlSnapshot
    }
}
