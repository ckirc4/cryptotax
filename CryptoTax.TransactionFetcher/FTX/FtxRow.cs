﻿using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using CryptoTax.TransactionFetcher.FTX.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Mathematics;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.FTX
{
    public class FtxRow
    {
        /// <summary>
        /// Internal id or on-chain transaction
        /// </summary>
        public DString Id { get; set; }
        /// <summary>
        /// UTC
        /// </summary>
        public DateTime Time { get; set; }
        public RowType Type { get; set; }
        public NString CurrencyReceived { get; set; }
        public decimal? AmountReceived { get; set; }
        public NString CurrencySent { get; set; }
        public decimal? AmountSent { get; set; }
        public NString FeeCurrency { get; set; }
        public decimal? FeeAmount { get; set; }

        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        public NString WithdrawalAddress { get; set; }
        /// <summary>
        /// Defined only for withdrawals
        /// </summary>
        public NString WithdrawalAddressTag { get; set; }

        /// <summary>
        /// Defined only for trades
        /// </summary>
        public FtxMarketType? MarketType { get; set; }
        /// <summary>
        /// Defined only for trades
        /// </summary>
        public decimal? Price { get; set; }
        /// <summary>
        /// Defined only for trades
        /// </summary>
        public OrderSide? Side { get; set; }

        public static FtxRow Create(FtxDeposit x)
        {
            if (x.Status != DepositStatus.Confirmed) return null;
            if (x.Fee != 0) throw new ArgumentException();
            return CreateIn(x.TxId, x.Time, RowType.Deposit, x.Coin, x.Size);
        }

        public static FtxRow Create(FtxWithdrawal x)
        {
            if (x.Status != WithdrawalStatus.Complete) return null;
            if (x.Fee != 0) throw new ArgumentException();
            FtxRow row = CreateOut(x.TxId, x.Time, RowType.Withdrawal, x.Coin, x.Size);
            row.WithdrawalAddress = x.Address;
            row.WithdrawalAddressTag = x.Tag;

            return row;
        }

        public static FtxRow Create(FtxFill x)
        {
            DString quoteCurrency, baseCurrency;
            FtxMarketType marketType;

            if (!string.IsNullOrEmpty(x.Future))
            {
                marketType = FtxMarketType.Futures;
                quoteCurrency = "USD";
                baseCurrency = x.Future;
            }
            else if (!string.IsNullOrEmpty(x.Market)
                || x.Type == FillType.OTC && !string.IsNullOrEmpty(x.QuoteCurrency) && !string.IsNullOrEmpty(x.BaseCurrency))
            {
                switch (x.Type)
                {
                    case FillType.Order:
                    case FillType.Liquidation:
                        marketType = FtxMarketType.Spot;
                        break;
                    case FillType.OTC:
                        marketType = FtxMarketType.OTC;
                        break;
                    case FillType.Expiration:
                    default:
                        // note: Expiration should never occur here because it can only happen for futures - previous if-block
                        throw new AssertUnreachable(x.Type);
                }
                quoteCurrency = x.QuoteCurrency;
                baseCurrency = x.BaseCurrency;
            }
            else throw new Exception("Unable to determine market type for fill.");

            DString currencyReceived, currencySent;
            decimal amountReceived, amountSent;
            if (x.Side == OrderSide.Buy)
            {
                currencyReceived = baseCurrency;
                amountReceived = x.Size;
                currencySent = quoteCurrency;
                amountSent = Numerics.Round(x.Size * x.Price, 8);
            }
            else
            {
                currencyReceived = quoteCurrency;
                amountReceived = Numerics.Round(x.Size * x.Price, 8);
                currencySent = baseCurrency;
                amountSent = x.Size;
            }

            FtxRow row = new FtxRow(x.Id.ToString(), x.Time, RowType.Trade, currencyReceived, amountReceived, currencySent, amountSent, x.FeeCurrency, x.Fee, marketType, x.Price, x.Side);

            return row;
        }

        public static FtxRow Create(FtxFundingPayment x)
        {
            FtxRow row = new FtxRow($"{x.Future}-{x.Id}", x.Time, RowType.FundingPayment);
            if (x.Payment < 0)
            {
                row.AmountSent = x.Payment;
                row.CurrencySent = "USD";
            }
            else
            {
                row.AmountReceived = x.Payment;
                row.CurrencyReceived = "USD";
            }

            return row;
        }

        public static FtxRow Create(FtxLendingPayment x)
        {
            FtxRow row = new FtxRow(Guid.NewGuid().ToString().Substring(0, 8), x.Time, RowType.LendingPayment);
            if (x.Proceeds < 0)
            {
                throw new NotImplementedException();
            }
            else
            {
                row.AmountReceived = x.Proceeds;
                row.CurrencyReceived = x.Coin;
            }

            return row;
        }

        public static IEnumerable<FtxRow> Create(FtxPnlHistory pnlHistory)
        {
            List<FtxRow> rows = new List<FtxRow>();

            foreach (KeyValuePair<string, Dictionary<string, decimal>> kvp in pnlHistory.PnlByDay)
            {
                // the timestamp marks the start of the day (midnight UTC) for which the pnl was tracked, such that the data for pnlHistory.PnlByDay[pnlHistory.Today] would be incomplete. However, we want to timestamp the pnl transaction event at 1 second before midnight of the next day, UTC.
                long timestamp = (int)(decimal.Parse(kvp.Key));
                DateTime dayStart = DateTimeOffset.FromUnixTimeSeconds(timestamp).UtcDateTime;
                DateTime eventTime = dayStart.AddDays(1).AddSeconds(-1);
                decimal pnl = kvp.Value.Sum(entry => entry.Value);

                DString id = $"pnl_{dayStart.ToString("dd/MM/yyyy")}";
                FtxRow row = new FtxRow(id, eventTime, RowType.PnlSnapshot);
                if (pnl == 0)
                {
                    // no need for an "empty" row, as these carry forward indefinitely
                    continue;
                }
                else if (pnl < 0)
                {
                    // loss
                    row.AmountSent = -pnl;
                    row.CurrencySent = "USD";
                }
                else
                {
                    // profit
                    row.AmountReceived = pnl;
                    row.CurrencyReceived = "USD";
                }

                rows.Add(row);
            }

            return rows;
        }

        private FtxRow(DString id, DateTime time, RowType type)
        {
            this.Id = id;
            this.Time = time;
            this.Type = type;
            this.CurrencyReceived = null;
            this.AmountReceived = null;
            this.CurrencySent = null;
            this.AmountSent = null;
            this.FeeCurrency = null;
            this.FeeAmount = null;

            this.WithdrawalAddress = null;
            this.WithdrawalAddressTag = null;

            this.MarketType = null;
            this.Price = null;
        }

        private FtxRow(DString id, DateTime time, RowType type, DString currencyReceived, decimal amountReceived, DString currencySent, decimal amountSent, DString feeCurrency, decimal feeAmount, FtxMarketType marketType, decimal price, OrderSide side) : this(id, time, type)
        {
            this.CurrencyReceived = currencyReceived;
            this.AmountReceived = amountReceived;
            this.CurrencySent = currencySent;
            this.AmountSent = amountSent;
            this.FeeCurrency = feeCurrency;
            this.FeeAmount = feeAmount;

            this.MarketType = marketType;
            this.Price = price;
            this.Side = side;
        }

        private static FtxRow CreateIn(DString id, DateTime time, RowType type, DString currency, decimal amount, string feeCurrency = null, decimal? feeAmount = null)
        {
            FtxRow row = new FtxRow(id, time, type);
            row.CurrencyReceived = currency;
            row.AmountReceived = amount;
            row.FeeCurrency = feeCurrency;
            row.FeeAmount = feeAmount;

            return row;
        }

        private static FtxRow CreateOut(DString id, DateTime time, RowType type, DString currency, decimal amount, string feeCurrency = null, decimal? feeAmount = null)
        {
            FtxRow row = new FtxRow(id, time, type);
            row.CurrencySent = currency;
            row.AmountSent = amount;
            row.FeeCurrency = feeCurrency;
            row.FeeAmount = feeAmount;

            return row;
        }

        public DString ToCsv()
        {
            return string.Join(",", new string[]
            {
                this.Id,
                this.Time.ToUniversalTime().ToString(),
                this.Type.ToString(),
                this.CurrencyReceived,
                this.AmountReceived?.ToString(),
                this.CurrencySent,
                this.AmountSent?.ToString(),
                this.FeeCurrency,
                this.FeeAmount?.ToString(),

                this.WithdrawalAddress,
                this.WithdrawalAddressTag,

                this.MarketType.ToString(),
                this.Price?.ToString(),
                this.Side?.ToString()
            });
        }

        public static DString CsvHeader()
        {
            return string.Join(",", new string[]
            {
                nameof(Id),
                nameof(Time),
                nameof(Type),
                nameof(CurrencyReceived),
                nameof(AmountReceived),
                nameof(CurrencySent),
                nameof(AmountSent),
                nameof(FeeCurrency),
                nameof(FeeAmount),

                nameof(WithdrawalAddress),
                nameof(WithdrawalAddressTag),

                nameof(MarketType),
                nameof(Price),
                nameof(Side)
            });
        }
    }
}
