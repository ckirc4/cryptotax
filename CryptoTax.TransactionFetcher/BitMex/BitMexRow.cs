﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.BitMex
{
    public class BitMexRow
    {
        #region Shared properties - use base constructor
        /// <summary>
        /// The transaction id or execution id.
        /// </summary>
        public DString Id { get; set; }
        /// <summary>
        /// The time at which the information in the row was received (transact time, NOT internal system timestamp).
        /// </summary>
        public DateTime Time { get; set; }
        public RowType RowType { get; set; }
        public decimal Amount { get; set; }
        /// <summary>
        /// In XBt
        /// </summary>
        public decimal? Fee { get; set; }
        public DString Currency { get; set; }
        public NString Text { get; set; }
        #endregion

        #region Execution properties
        public string ExecutionType { get; set; } = null;
        public string Symbol { get; set; } = null;
        public string SettlCurrency { get; set; } = null;
        public string Side { get; set; } = null;
        public decimal? Price { get; set; } = null;
        /// <summary>
        /// The cost in XBt
        /// </summary>
        public decimal? Cost { get; set; } = null;
        public string OrderType { get; set; } = null;
        #endregion

        #region Wallet properties
        public string WalletEventType { get; set; } = null;
        public string Address { get; set; } = null;
        public string Tx { get; set; } = null;
        public string Status { get; set; } = null;
        #endregion

        public BitMexRow(BitMexExecutionEvent trade) : this(trade.execId, trade.transactTime, RowType.Execution, trade.lastQty.Value, trade.execComm, trade.currency, trade.text)
        {
            this.ExecutionType = fullOrNull(trade.execType, false);
            this.Symbol = fullOrNull(trade.symbol, false);
            this.SettlCurrency = fullOrNull(trade.settlCurrency, false);
            this.Side = fullOrNull(trade.side, true); // can be null for funding events
            this.Price = trade.price.Value;
            this.Cost = trade.execCost.Value;
            this.OrderType = fullOrNull(trade.ordType, false);
        }

        public BitMexRow(BitMexWalletEvent transaction) : this(transaction.transactID, transaction.transactTime, RowType.Wallet, transaction.amount.Value, transaction.fee, transaction.currency, transaction.text)
        {
            this.WalletEventType = fullOrNull(transaction.transactType, false);
            this.Address = fullOrNull(transaction.address, true); // can be null for deposits/rpnl
            this.Tx = fullOrNull(transaction.tx, true); // can be null for deposits/rpnl
            this.Status = fullOrNull(transaction.transactStatus, false);
        }

        private BitMexRow(DString id, DateTime time, RowType rowType, decimal amount, decimal? fee, DString currency, NString text)
        {
            this.Id = fullOrNull(id, false);
            this.Time = time;
            this.RowType = rowType;
            this.Amount = amount;
            this.Fee = fee;
            this.Currency = fullOrNull(currency, false);
            this.Text = fullOrNull(text, true);
        }

        /// <summary>
        /// Converts an input of "" to null.
        /// </summary>
        private static string fullOrNull(string str, bool allowEmptyOrNull)
        {
            str = str == "" ? null : str;
            if (!allowEmptyOrNull && str == null) throw new Exception("String must not be null or empty, but was null or empty");
            return str;
        }

        public DString ToCsv()
        {
            return string.Join(",", new string[]
            {
                this.Id,
                this.Time.ToUniversalTime().ToString(),
                this.RowType.ToString(),
                this.Amount.ToString(),
                this.Fee?.ToString(),
                this.Currency,
                this.Text.Value?.Replace(",", "").Replace('\n', '|'),
                this.ExecutionType,
                this.Symbol,
                this.SettlCurrency,
                this.Side,
                this.Price?.ToString(),
                this.Cost?.ToString(),
                this.OrderType,
                this.WalletEventType,
                this.Address,
                this.Tx,
                this.Status
            });
        }

        public static DString CsvHeader()
        {
            return string.Join(",", new string[]
            {
                nameof(Id),
                nameof(Time),
                nameof(RowType),
                nameof(Amount),
                nameof(Fee),
                nameof(Currency),
                nameof(Text),
                nameof(ExecutionType),
                nameof(Symbol),
                nameof(SettlCurrency),
                nameof(Side),
                nameof(Price),
                nameof(Cost),
                nameof(OrderType),
                nameof(WalletEventType),
                nameof(Address),
                nameof(Tx),
                nameof(Status)
            });
        }
    }
}
