﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

#pragma warning disable CS0162 // Unreachable code detected

namespace CryptoTax.TransactionFetcher.BitMex
{
    // mostly stolen from https://github.com/BitMEX/api-connectors/blob/master/official-http/csharp/BitMEXAPI.cs
    public class BitMexApiService
    {
        private const string _BASE_URL = "https://www.bitmex.com";
        private const int _MAX_ITEM_COUNT = 500;

        private readonly string _ApiKey;
        private readonly string _ApiSecret;

        public BitMexApiService()
        {
            this._ApiKey = Constants.API_KEY_BITMETX;
            this._ApiSecret = Constants.API_SECRET_BITMETX;
        }

        public IEnumerable<BitMexWalletEvent> GetWalletEvents(DateTime? fromUTC = null, DateTime? toUTC = null)
        {
            if (fromUTC != null || toUTC != null) throw new NotImplementedException("Time ranges are not implemented yet, as they are only required for future tax reports.");

            IEnumerable<BitMexWalletEvent> results = this.getAndParseAll<BitMexWalletEvent>((startAt) => this.getWalletTransactions(startAt));
            return results;
        }

        public IEnumerable<BitMexExecutionEvent> GetTradeEvents(DateTime? fromUTC = null, DateTime? toUTC = null)
        {
            if (fromUTC != null || toUTC != null) throw new NotImplementedException("Time ranges are not implemented yet, as they are only required for future tax reports.");

            IEnumerable<BitMexExecutionEvent> results = this.getAndParseAll<BitMexExecutionEvent>((startAt) => this.getOrderExecutions(startAt));
            return results;
        }

        private IEnumerable<T> getAndParseAll<T>(Func<int, string> getFn)
        {
            IEnumerable<T> results = new List<T>();
            int lastCount;
            int startAt = 0;

            do
            {
                // make sure we ratelimit - about 1.5 seconds per request, to be on the safe side
                DateTime start = DateTime.Now;
                string raw = getFn(startAt);
                DateTime end = DateTime.Now;
                double durationMs = end.Subtract(start).TotalMilliseconds;
                if (durationMs < 1500) {
                    Thread.Sleep(1500 - (int)durationMs);
                }

                IEnumerable<T> thisResults = JsonConvert.DeserializeObject<IEnumerable<T>>(raw);
                results = results.Concat(thisResults);

                // check if we need to get more - if we got less than the max number of items, then we are done
                lastCount = thisResults.Count();
                startAt += lastCount;
            } while (lastCount == _MAX_ITEM_COUNT);

            return results;
        }

        /// <summary>
        /// Gets 500 executions beginning at the starting point and optionally between the given times.
        /// </summary>
        private string getOrderExecutions(int startingPoint = 0, DateTime? fromUTC = null, DateTime? toUTC = null)
        {
            // since bitmex has disabled my API access completely, used swagger "try it out" functionality to aggregate all data manually. e.g. https://www.bitmex.com/api/explorer/#!/User/User_getWalletHistory
            return new FileService(null).ReadFile(@"C:\Users\chris\Google Drive\Other\Trading\Tax\TradeLogs\BitMex\GetExecutionHistoryResponse.json");

            Dictionary<string, string> param = new Dictionary<string, string>
            {
                ["count"] = _MAX_ITEM_COUNT.ToString(),
                ["start"] = startingPoint.ToString()
            };
            return this.query("GET", "/api/v1/execution/tradeHistory", param, true);
        }

        /// <summary>
        /// Gets 500 wallet transactions beginning at the starting point.
        /// </summary>
        private string getWalletTransactions(int startingPoint = 0)
        {
            return new FileService(null).ReadFile(@"C:\Users\chris\Google Drive\Other\Trading\Tax\TradeLogs\BitMex\GetUserWalletHistoryResponse.json");

            Dictionary<string, string> param = new Dictionary<string, string>
            {
                ["count"] = _MAX_ITEM_COUNT.ToString(),
                ["start"] = startingPoint.ToString()
            };
            return this.query("GET", "/api/v1/user/walletHistory", param, true);
        }


        private string buildQueryData(Dictionary<string, string> param)
        {
            if (param == null)
                return "";

            StringBuilder b = new StringBuilder();
            foreach (KeyValuePair<string, string> item in param)
                b.Append(string.Format("&{0}={1}", item.Key, WebUtility.UrlEncode(item.Value)));

            try { return b.ToString().Substring(1); }
            catch (Exception) { return ""; }
        }

        private string buildJSON(Dictionary<string, string> param)
        {
            if (param == null)
                return "";

            List<string> entries = new List<string>();
            foreach (KeyValuePair<string, string> item in param)
                entries.Add(string.Format("\"{0}\":\"{1}\"", item.Key, item.Value));

            return "{" + string.Join(",", entries) + "}";
        }

        private static string byteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        private long getExpires()
        {
            return DateTimeOffset.UtcNow.ToUnixTimeSeconds() + 3600; // set expires one hour in the future
        }

        private string query(string method, string function, Dictionary<string, string> param = null, bool auth = false, bool json = false)
        {
            throw new Exception("BitMex has disabled the account's API access.");
            string paramData = json ? this.buildJSON(param) : this.buildQueryData(param);
            string url = function + ((method == "GET" && paramData != "") ? "?" + paramData : "");
            string postData = (method != "GET") ? paramData : "";

            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_BASE_URL + url);
            webRequest.Method = method;

            if (auth)
            {
                string expires = this.getExpires().ToString();
                string message = method + url + expires + postData;
                byte[] signatureBytes = hmacsha256(Encoding.UTF8.GetBytes(_ApiSecret), Encoding.UTF8.GetBytes(message));
                string signatureString = byteArrayToString(signatureBytes);

                webRequest.Headers.Add("api-expires", expires);
                webRequest.Headers.Add("api-key", _ApiKey);
                webRequest.Headers.Add("api-signature", signatureString);
            }

            try
            {
                if (postData != "")
                {
                    webRequest.ContentType = json ? "application/json" : "application/x-www-form-urlencoded";
                    byte[] data = Encoding.UTF8.GetBytes(postData);
                    using (Stream stream = webRequest.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }
                }

                using (WebResponse webResponse = webRequest.GetResponse())
                using (Stream str = webResponse.GetResponseStream())
                using (StreamReader sr = new StreamReader(str))
                {
                    return sr.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                using (HttpWebResponse response = (HttpWebResponse)wex.Response)
                {
                    if (response == null)
                        throw;

                    using (Stream str = response.GetResponseStream())
                    {
                        using (StreamReader sr = new StreamReader(str))
                        {
                            return sr.ReadToEnd();
                        }
                    }
                }
            }
        }

        private static byte[] hmacsha256(byte[] keyByte, byte[] messageBytes)
        {
            using (HMACSHA256 hash = new HMACSHA256(keyByte))
            {
                return hash.ComputeHash(messageBytes);
            }
        }
    }
}
