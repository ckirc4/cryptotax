﻿using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.BitMex
{
    public class BitMexService
    {
        private readonly IFileService _FileService;
        private readonly BitMexApiService _ApiService;

        public BitMexService(IFileService fileService, BitMexApiService apiService)
        {
            this._FileService = fileService;
            this._ApiService = apiService;
        }

        public void GetAndWriteAllTransactions(DString fileName, DateTime? fromUTC = null, DateTime? toUTC = null)
        {
            if (fromUTC != null || toUTC != null) throw new NotImplementedException("Time ranges are not implemented yet, as they are only required for future tax reports.");

            IEnumerable<BitMexWalletEvent> walletEvents = this._ApiService.GetWalletEvents(fromUTC, toUTC);
            IEnumerable<BitMexExecutionEvent> tradeEvents = this._ApiService.GetTradeEvents(fromUTC, toUTC);

            IOrderedEnumerable<BitMexRow> rows = walletEvents.Select(w => new BitMexRow(w)).Concat(tradeEvents.Select(t => new BitMexRow(t))).OrderBy(r => r.Time);
            IEnumerable<NString> csvRows = rows.Select(r => new NString(r.ToCsv())).ToList();
            this._FileService.WriteLines(fileName, new NString[] { BitMexRow.CsvHeader() }.Concat(csvRows), false);
        }
    }
}
