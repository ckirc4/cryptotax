﻿using System;

namespace CryptoTax.TransactionFetcher.BitMex
{
    public class BitMexWalletEvent
    {
        public string transactID { get; set; } // base
        public string currency { get; set; } // base
        public string transactType { get; set; } // ext
        public decimal? amount { get; set; } // base
        public decimal? fee { get; set; } // base
        public string transactStatus { get; set; } // ext
        public string address { get; set; } // ext
        public string tx { get; set; } // ext
        public string text { get; set; } // base
        public DateTime transactTime { get; set; } // base
        public decimal? walletBalance { get; set; }
        public decimal? marginBalance { get; set; }
        public DateTime timestamp { get; set; }
    }
}
