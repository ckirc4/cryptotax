﻿using System;

namespace CryptoTax.TransactionFetcher.BitMex
{
    public class BitMexExecutionEvent
    {
        public string execId { get; set; } // base
        public string orderId { get; set; }
        public string symbol { get; set; } // ext
        public string side { get; set; } // ext
        public decimal? lastQty { get; set; } // base
        public decimal? lastPx { get; set; }
        public decimal? underlyingLastPx { get; set; }
        public decimal? simpleOrderQty { get; set; }
        public decimal? orderQty { get; set; }
        public decimal? price { get; set; } // ext
        public string currency { get; set; } // base
        public string settlCurrency { get; set; } // ext
        public string execType { get; set; } // ext
        public string ordType { get; set; } // ext
        public string ordStatus { get; set; }
        public decimal? simpleLeavesQty { get; set; }
        public decimal? leavesQty { get; set; }
        public decimal? simpleCumQty { get; set; }
        public decimal? cumQty { get; set; }
        public decimal? avgPx { get; set; }
        public decimal? commission { get; set; }
        public string text { get; set; } // base
        public decimal? execCost { get; set; } // ext
        public decimal? execComm { get; set; } // base
        public DateTime transactTime { get; set; } // base
        public DateTime timestamp { get; set; }
    }
}
