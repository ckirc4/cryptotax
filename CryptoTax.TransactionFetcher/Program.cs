﻿using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using CryptoTax.TransactionFetcher.BitMex;
using CryptoTax.TransactionFetcher.Ethereum;
using CryptoTax.TransactionFetcher.FTX;
using CryptoTax.TransactionFetcher.XRPL;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher
{
    internal class Program
    {
        private static DString _Folder = $@"C:\Users\chris\Google Drive\Other\Trading\Tax\TradeLogs\";

        static void Main(string[] args)
        {
            //fetchOnChain(FinancialYear.Y2022, KnownWallets.ETH_MY_WALLET);
            //fetchOnChain(FinancialYear.Y2022, KnownWallets.ETH_MY_WALLET_ARB_NOVA);
            //fetchOnChain(FinancialYear.Y2022, KnownWallets.XRP_MY_WALLET_SAVINGS);

            //fetchOnChain(FinancialYear.Y2023, KnownWallets.ETH_MY_WALLET);
            //fetchOnChain(FinancialYear.Y2023, KnownWallets.ETH_MY_WALLET_ARB_NOVA);
            //fetchOnChain(FinancialYear.Y2023, KnownWallets.XRP_MY_WALLET_SAVINGS);
            //fetchOnChain(FinancialYear.Y2023, KnownWallets.XRP_MY_WALLET_SAVINGS_NEW);

            //fetchBitMex();

            //fetchFtx(new FinancialYear(2019));
            //fetchFtx(new FinancialYear(2020));
            //fetchFtx(new FinancialYear(2021));
            //fetchFtx(new FinancialYear(2022));

            Console.WriteLine("Done... press any key to exit");
            Console.ReadKey();
        }

        private static void fetchFtx(FinancialYear year)
        {
            DString file = _Folder + $@"FTX\FTX_{year.Name}.csv";

            FtxApiService apiService = new FtxApiService();
            IFileService fileService = new FileService(new CryptoService());
            FtxService service = new FtxService(fileService, apiService);

            service.GetAndWriteAllTransactions(file, year);
        }

        private static void fetchBitMex()
        {
            DString file = _Folder += @"BitMex\BitMex.csv";

            BitMexApiService apiService = new BitMexApiService();
            IFileService fileService = new FileService(new CryptoService());
            BitMexService service = new BitMexService(fileService, apiService);

            service.GetAndWriteAllTransactions(file);
        }

        private static void fetchOnChain(FinancialYear financialYear, Wallet wallet)
        {
            string chainTypeString;
            EthereumChainType? ethereumChainType;
            switch (wallet.Blockchain)
            {
                case Shared.Enums.BlockchainType.Ethereum:
                    chainTypeString = "EthereumMainnet";
                    ethereumChainType = EthereumChainType.Mainnet;
                    break;
                case Shared.Enums.BlockchainType.ArbitrumNova:
                    chainTypeString = "EthereumArbitrumNova";
                    ethereumChainType = EthereumChainType.ArbitrumNova;
                    break;
                case Shared.Enums.BlockchainType.XRPL:
                    chainTypeString = "XRPL";
                    ethereumChainType = null;
                    break;
                case Shared.Enums.BlockchainType.Bitcoin:
                case Shared.Enums.BlockchainType.EthereumClassic:
                case Shared.Enums.BlockchainType.Rinkeby:
                case Shared.Enums.BlockchainType.USDT:
                case Shared.Enums.BlockchainType.BinanceChain:
                case Shared.Enums.BlockchainType.Undefined:
                default:
                    throw new NotImplementedException($"TransactionFetcher not implemented for chain {wallet.Blockchain}");
            }

            DString filePath = _Folder + $@"OnChain\{financialYear.Start.Year}\{chainTypeString}\{wallet.Address}.csv";

            // this can be abstracted into a common interface, but whatever
            if (ethereumChainType.HasValue)
            {
                fetchEthereum(financialYear, wallet, ethereumChainType.Value, filePath);
            }
            else
            {
                fetchXRPL(financialYear, wallet, filePath);
            }
        }

        private static void fetchEthereum(FinancialYear financialYear, Wallet wallet, EthereumChainType ethereumChainType, DString filePath)
        {
            IEthereumApiService apiService = new EthereumApiService(new WebService(), ethereumChainType);
            IFileService fileService = new FileService(new CryptoService());
            EthereumService service = new EthereumService(apiService, fileService, ethereumChainType);

            service.GetAndWriteAllTransactions(wallet, filePath, financialYear).Wait();
        }

        private static void fetchXRPL(FinancialYear financialYear, Wallet wallet, DString filePath)
        {
            IXRPLApiService apiService = new XRPLApiService(new WebService());
            IFileService fileService = new FileService(new CryptoService());
            XRPLService service = new XRPLService(apiService, fileService);

            service.GetAndWriteAllTransactions(wallet, filePath, financialYear).Wait();
        }

        private static void fetchReddit()
        {
            // note: reddit wallet address is unequivocally 0xe5dd240231608367fb2d61d5348693a7cbf5396e

            // the original wallet link is https://rinkeby.etherscan.io/address/0xe5dd240231608367fb2d61d5348693a7cbf5396e#tokentxns
            // - the first MOON transaction was at 14/05/2020
            // - the MOON migration was at 23/7/2021
            //      - from old MOON token on https://rinkeby.etherscan.io/token/0xdf82c9014f127243ce1305dfe54151647d74b27a?a=0xe5dd240231608367fb2d61d5348693a7cbf5396e
            //      - to new MOON token on https://testnet.redditspace.com/tokens/0x138fAFa28a05A38f4d2658b12b0971221A7d5728/token-transfers
            // - 8748.822 MOON were migrated (though the balance of the original address was 8748.82206356048)
            // the new wallet link is https://testnet.redditspace.com/address/0xe5dD240231608367fb2D61d5348693A7CBF5396e/token-transfers
        }
    }
}
