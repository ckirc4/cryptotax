﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using CryptoTax.TransactionFetcher.XRPL.ResponseModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.XRPL
{
    public class XRPLService
    {
        private readonly IXRPLApiService _Api;
        private readonly IFileService _FileService;

        public XRPLService(IXRPLApiService apiService, IFileService fileService)
        {
            this._Api = apiService;
            this._FileService = fileService;
        }

        public async Task GetAndWriteAllTransactions(Wallet wallet, DString fileName, FinancialYear financialYear)
        {
            List<Transaction> transactions = await this._Api.GetTransactions(wallet, financialYear);
            List<XRPLCsvRow> rows = this.generateRows(wallet.Address, transactions);
            this.writeRows(rows, fileName);
        }

        private List<XRPLCsvRow> generateRows(DString address, List<Transaction> transactions)
        {
            List<XRPLCsvRow> rows = new List<XRPLCsvRow>();

            foreach (Transaction tx in transactions)
            {
                if (tx.TransactionType != "Payment")
                {
                    throw new NotImplementedException("We do not support non-payment XRPL transactions.");
                }
                else if (tx.Amount.Currency != "XRP")
                {
                    throw new NotImplementedException("We do not support non-XRP denominated transactions.");
                }

                decimal amount = this.dropsToXrp(tx.Amount.Value);
                decimal fee = this.dropsToXrp(long.Parse(tx.Fee));

                if (tx.Account == address)
                {
                    // we are sending XRP
                    rows.Add(new XRPLCsvRow(tx.hash, tx.date, tx.Destination, XRPLTransactionType.SentToCounterparty, null, amount, fee));
                }
                else if (tx.Destination == address)
                {
                    // we are receiving XRP
                    rows.Add(new XRPLCsvRow(tx.hash, tx.date, tx.Account, XRPLTransactionType.ReceivedFromCounterparty, amount, null, fee));
                }
                else
                {
                    throw new Exception("Invalid transaction - it does not contain our address as either the sender or receiver.");
                }
            }

            return rows;
        }

        private void writeRows(List<XRPLCsvRow> rows, DString fileName)
        {
            if (!fileName.Value.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
            {
                fileName += ".csv";
            }

            List<NString> lines = new List<NString>() { XRPLCsvRow.CsvHeader() };
            lines.AddRange(rows.Select(row => (NString)row.ToCsv()));
            this._FileService.WriteLines(fileName, lines, false);
        }

        private decimal dropsToXrp(long drops)
        {
            return drops / (decimal)Math.Pow(10, 6);
        }
    }
}
