﻿using CryptoTax.Shared;
using CryptoTax.TransactionFetcher.XRPL.ResponseModels;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoTax.TransactionFetcher.XRPL
{
    public interface IXRPLApiService
    {
        Task<List<Transaction>> GetTransactions(Wallet wallet, FinancialYear financialYear);
    }
}
