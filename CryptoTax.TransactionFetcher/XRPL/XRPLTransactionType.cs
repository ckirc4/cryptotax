﻿namespace CryptoTax.TransactionFetcher.XRPL
{
    public enum XRPLTransactionType
    {
        ReceivedFromCounterparty,
        SentToCounterparty
    }
}
