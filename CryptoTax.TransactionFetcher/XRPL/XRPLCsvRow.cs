﻿using CryptoTax.Shared;
using System;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.XRPL
{
    public struct XRPLCsvRow
    {
        private static readonly Currency CURRENCY = KnownCurrencies.XRP;

        /// <summary>
        /// The hash of the underlying on-chain transaction.
        /// </summary>
        public DString TxHash { get; }
        public DateTime Time { get; }
        /// <summary>
        /// The other wallet involved in the transaction
        /// </summary>
        public DString Counterparty { get; }
        public XRPLTransactionType TransactionType { get; }
        /// <summary>
        /// Only defined for incoming transactions.
        /// </summary>
        public decimal? QuantityReceived { get; }
        /// <summary>
        /// Only defined for outgoing transactions.
        /// </summary>
        public decimal? QuantitySent { get; }
        /// <summary>
        /// The fee, in XRP, paid for the transaction.
        /// </summary>
        public decimal Fee { get; }

        /// <summary>
        /// For inbound/outbound transactions.
        /// </summary>
        public XRPLCsvRow(DString txHash, DateTime time, DString counterparty, XRPLTransactionType type, decimal? qtyReceived, decimal? qtySent, decimal fee)
        {
            this.TxHash = txHash;
            this.Time = time;
            this.Counterparty = counterparty;
            this.TransactionType = type;
            this.QuantitySent = qtySent;
            this.QuantityReceived = qtyReceived;
            this.Fee = fee;
        }

        public DString ToCsv()
        {
            return $"{this.TxHash},{this.Time.ToUnix()},{this.Counterparty},{this.TransactionType},{this.QuantityReceived},{(this.QuantityReceived.HasValue ? CURRENCY.Symbol.Value : null)},{this.QuantitySent},{(this.QuantitySent.HasValue ? CURRENCY.Symbol.Value : null)},{this.Fee}";
        }

        public static DString CsvHeader()
        {
            return "TxHash,Time,Counterparty,TransactionType,QuantityReceived,CurrencyReceived,QuantitySent,CurrencySent,Fee";
        }
    }
}
