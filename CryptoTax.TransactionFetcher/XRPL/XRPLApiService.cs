﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using CryptoTax.TransactionFetcher.XRPL.ResponseModels;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.XRPL
{
    public class XRPLApiService : IXRPLApiService
    {
        private readonly IWebService _WebService;

        public XRPLApiService(IWebService webService)
        {
            this._WebService = webService;
        }

        public async Task<List<Transaction>> GetTransactions(Wallet wallet, FinancialYear financialYear)
        {
            // docs: https://docs.xrpscan.com/api-documentation/account/transactions
            DString url = $"https://api.xrpscan.com/api/v1/account/{wallet.Address}/transactions";
            DString result = await this._WebService.DownloadStringAsync(url);

            GetTransactionsResponse response = JsonConvert.DeserializeObject<GetTransactionsResponse>(result);
            return response.transactions
                .Where(r => r.date >= financialYear.Start && r.date <= financialYear.End)
                .OrderBy(r => r.date)
                .ToList();
        }
    }
}
