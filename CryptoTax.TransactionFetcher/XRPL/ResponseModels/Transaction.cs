﻿using System;
namespace CryptoTax.TransactionFetcher.XRPL.ResponseModels
{
    public class Transaction
    {
        /// <summary>
        /// The source wallet.
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// The destination wallet.
        /// </summary>
        public string Destination { get; set; }
        /// <summary>
        /// Optional.
        /// </summary>
        public int? DestinationTag { get; set; }
        /// <summary>
        /// In units of drops.
        /// </summary>
        public string Fee { get; set; }
        /// <summary>
        /// Should be "Payment".
        /// </summary>
        public string TransactionType { get; set; }
        public string hash { get; set; }
        public DateTime date { get; set; }
        public Amount Amount { get; set; }
        /// <summary>
        /// Optional. Only set for official source wallets (e.g. exchange wallets).
        /// </summary>
        public AccountName AccountName { get; set; }
        /// <summary>
        /// Optional. Only set for official destination wallets (e.g. exchange wallets).
        /// </summary>
        public AccountName DestinationName { get; set; }
    }
}
