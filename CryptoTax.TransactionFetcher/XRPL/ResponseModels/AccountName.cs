﻿namespace CryptoTax.TransactionFetcher.XRPL.ResponseModels
{
    public class AccountName
    {
        public string name { get; set; }
        public string desc { get; set; }
        public string domain { get; set; }
    }
}
