﻿using System.Collections.Generic;

namespace CryptoTax.TransactionFetcher.XRPL.ResponseModels
{
    public class GetTransactionsResponse
    {
        public string account { get; set; }
        public long ledger_index_min { get; set; }
        public long ledger_index_max { get; set; }
        public List<Transaction> transactions { get; set; }
    }
}
