﻿namespace CryptoTax.TransactionFetcher.XRPL.ResponseModels
{
    public class Amount
    {
        /// <summary>
        /// In units of drops.
        /// </summary>
        public long Value { get; set; }
        /// <summary>
        /// Always "XRP".
        /// </summary>
        public string Currency { get; set; }
    }
}
