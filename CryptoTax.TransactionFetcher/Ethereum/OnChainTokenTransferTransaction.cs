﻿using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public class OnChainTokenTransferTransaction : OnChainTransactionBlockBase
    {
        public DString TokenName { get; set; }
        public DString TokenSymbol { get; set; }
        public int TokenDecimal { get; set; }

        public OnChainTokenTransferTransaction(dynamic obj) : base((object)obj)
        {
            /*
            Example object:
            {
                "blockNumber":"7929864",
                "timeStamp":"1560151686",
                "hash":"0xca57e2138dc5a2f8ebba77179cce7c662aabab4e2e5d91933fa1032d1d3a60fd",
                "nonce":"76391",
                "blockHash":"0x1e259e553fcada48fdefa13fadcb5b3a398fb86185c665fd1ec39a21487709a3",
                "from":"0x33152b0176dd5b08b523838fe5719e78cfd5b63d",
                "contractAddress":"0x1c95b093d6c236d3ef7c796fe33f9cc6b8606714",
                "to":"0x7042db4c642f6ac868df6717b65be6d4590ea720",
                "value":"3",
                "tokenName":"BOMB",
                "tokenSymbol":"BOMB",
                "tokenDecimal":"0",
                "transactionIndex":"78",
                "gas":"1000000",
                "gasPrice":"11900000000",
                "gasUsed":"313111",
                "cumulativeGasUsed":"3019408",
                "input":"deprecated",
                "confirmations":"3556434"
            }
            */

            this.TokenName = (string)obj.tokenName;
            this.TokenSymbol = (string)obj.tokenSymbol;
            this.TokenDecimal = (int)obj.tokenDecimal;
        }
    }
}