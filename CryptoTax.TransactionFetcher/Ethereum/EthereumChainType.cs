﻿namespace CryptoTax.TransactionFetcher.Ethereum
{
    public enum EthereumChainType
    {
        Mainnet,
        ArbitrumNova
    }
}
