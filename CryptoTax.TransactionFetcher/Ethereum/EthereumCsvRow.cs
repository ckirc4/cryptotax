﻿
using CryptoTax.Shared;
using System;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public struct EthereumCsvRow
    {
        /// <summary>
        /// The hash of the underlying on-chain transaction.
        /// </summary>
        public DString TxHash { get; }
        public DateTime Time { get; }
        /// <summary>
        /// The other wallet involved in the transaction
        /// </summary>
        public Wallet Counterparty { get; }
        public EthereumTransactionType TransactionType { get; }
        /// <summary>
        /// Only defined for trades or incoming transactions.
        /// </summary>
        public decimal? QuantityReceived { get; }
        /// <summary>
        /// Only defined for trades or incoming transactions.
        /// </summary>
        public Currency? CurrencyReceived { get; }
        /// <summary>
        /// Only defined for trades or outgoing transactions.
        /// </summary>
        public decimal? QuantitySent { get; }
        /// <summary>
        /// Only defined for trades or outgoing transactions.
        /// </summary>
        public Currency? CurrencySent { get; }
        /// <summary>
        /// The fee, in ETH, paid for the transaction (aka GAS).
        /// </summary>
        public decimal Fee { get; }

        /// <summary>
        /// New inbound transaction.
        /// </summary>
        public EthereumCsvRow(DString txHash, DateTime time, Wallet counterparty, decimal qtyReceived, Currency currencyReceived, decimal fee) : this(txHash, time, counterparty, fee)
        {
            this.QuantityReceived = qtyReceived;
            this.CurrencyReceived = currencyReceived;
            this.TransactionType = EthereumTransactionType.ReceivedFromCounterparty;
        }

        /// <summary>
        /// New outbound transaction.
        /// </summary>
        public EthereumCsvRow(DString txHash, DateTime time, Wallet counterparty, decimal qtySent, Currency currencySent, decimal fee, object _ = null) : this(txHash, time, counterparty, fee)
        {
            this.QuantitySent = qtySent;
            this.CurrencySent = currencySent;
            this.TransactionType = EthereumTransactionType.SentToCounterparty;
        }

        /// <summary>
        /// New trade.
        /// </summary>
        public EthereumCsvRow(DString txHash, DateTime time, Wallet counterparty, decimal qtyReceived, Currency currencyReceived, decimal qtySent, Currency currencySent, decimal fee) : this(txHash, time, counterparty, fee)
        {
            this.QuantityReceived = qtyReceived;
            this.CurrencyReceived = currencyReceived;
            this.QuantitySent = qtySent;
            this.CurrencySent = currencySent;
            this.TransactionType = EthereumTransactionType.TradedWithCounterparty;
        }

        private EthereumCsvRow(DString txHash, DateTime time, Wallet counterparty, decimal fee)
        {
            this.TxHash = txHash;
            this.Time = time;
            this.Counterparty = counterparty;
            this.TransactionType = EthereumTransactionType.Unknown;
            this.QuantityReceived = null;
            this.CurrencyReceived = null;
            this.QuantitySent = null;
            this.CurrencySent = null;
            this.Fee = fee;
        }

        public DString ToCsv()
        {
            return $"{this.TxHash},{this.Time.ToUnix()},{this.Counterparty.Address},{this.TransactionType},{this.QuantityReceived},{this.CurrencyReceived?.Symbol},{this.QuantitySent},{this.CurrencySent?.Symbol},{this.Fee}";
        }

        public static DString CsvHeader()
        {
            return "TxHash,Time,Counterparty,TransactionType,QuantityReceived,CurrencyReceived,QuantitySent,CurrencySent,Fee";
        }
    }
}
