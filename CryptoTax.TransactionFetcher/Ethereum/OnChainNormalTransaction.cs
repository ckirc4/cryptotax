﻿namespace CryptoTax.TransactionFetcher.Ethereum
{
    public class OnChainNormalTransaction : OnChainTransactionBlockBase
    {
        /// <summary>
        /// 0 if the transaction completed successfully, and 1 if there was an error. Note: gas is still used even if the transaction did not completely successfully.
        /// </summary>
        public int IsError { get; set; }
        public int TxReceiptStatus { get; set; }

        public OnChainNormalTransaction(dynamic obj) : base((object)obj) // nasty cast is necessary but I don't understand why
        {
            /*
            Example object:
            {
                "blockNumber":"7929423",
                "timeStamp":"1560145963",
                "hash":"0xa04315f998a4c60b958c027dbba8faa93f78fd0e1028453a81e248a43654ede0",
                "nonce":"3184318",
                "blockHash":"0x9969d40a876e369d9f09c821ed6193d0edeca3d983d88064e5d032c02458c9d7",
                "transactionIndex":"20",
                "from":"0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be",
                "to":"0x7042db4c642f6ac868df6717b65be6d4590ea720",
                "value":"5400000000000000000",
                "gas":"21000",
                "gasPrice":"40000000000",
                "isError":"0",
                "txreceipt_status":"1",
                "input":"0x",
                "contractAddress":"",
                "cumulativeGasUsed":"802024",
                "gasUsed":"21000",
                "confirmations":"3557525"
            }
            */

            this.IsError = (int)obj.isError;
            this.TxReceiptStatus = (int)obj.txreceipt_status;
        }
    }
}
