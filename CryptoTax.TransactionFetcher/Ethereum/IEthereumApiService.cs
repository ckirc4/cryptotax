﻿using CryptoTax.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public interface IEthereumApiService
    {
        Task<IEnumerable<OnChainNormalTransaction>> GetNormalTransactions(Wallet wallet, FinancialYear financialYear);
        Task<IEnumerable<OnChainTokenTransferTransaction>> GetTokenTransferTransactions(Wallet wallet, FinancialYear financialYear);
        Task<IEnumerable<OnChainInternalTransaction>> GetInternalTransactions(Wallet wallet, FinancialYear financialYear);
    }
}
