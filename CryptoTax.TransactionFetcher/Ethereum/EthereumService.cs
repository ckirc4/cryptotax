﻿using CryptoTax.Shared;
using CryptoTax.Shared.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;
using NormalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainNormalTransaction;
using TokenTx = CryptoTax.TransactionFetcher.Ethereum.OnChainTokenTransferTransaction;
using InternalTx = CryptoTax.TransactionFetcher.Ethereum.OnChainInternalTransaction;
using BlockBaseTx = CryptoTax.TransactionFetcher.Ethereum.OnChainTransactionBlockBase;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Exceptions;
using System.Security.Policy;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public class EthereumService
    {
        // urls:
        // wallet: transactions: https://etherscan.io/txs?a=0x7042db4c642f6ac868df6717b65be6d4590ea720, token transactions: https://etherscan.io/tokentxns?a=0x7042db4c642f6ac868df6717b65be6d4590ea720
        // list of etherscan apis: https://api.etherscan.io/apis
        // normal tx: https://api.etherscan.io/api?module=account&action=txlist&address=0x7042db4c642f6ac868df6717b65be6d4590ea720&apikey=TK39QCQK4ZE7THKY6WEFSCS5UGDJ378FXI
        // token transfer tx: https://api.etherscan.io/api?module=account&action=tokentx&address=0x7042db4c642f6ac868df6717b65be6d4590ea720&apikey=TK39QCQK4ZE7THKY6WEFSCS5UGDJ378FXI
        private readonly IEthereumApiService _Api;
        private readonly IFileService _FileService;
        private readonly BlockchainType chainType;

        public EthereumService(IEthereumApiService apiService, IFileService fileService, EthereumChainType chainType)
        {
            this._Api = apiService;
            this._FileService = fileService;

            switch (chainType)
            {
                case EthereumChainType.Mainnet:
                    this.chainType = BlockchainType.Ethereum;
                    break;
                case EthereumChainType.ArbitrumNova:
                    this.chainType = BlockchainType.ArbitrumNova;
                    break;
                default:
                    throw new AssertUnreachable(chainType);
            }
        }

        public async Task GetAndWriteAllTransactions(Wallet wallet, DString fileName, FinancialYear financialYear)
        {
            IEnumerable<NormalTx> normalTx = await this._Api.GetNormalTransactions(wallet, financialYear);
            IEnumerable<TokenTx> tokenTx = await this._Api.GetTokenTransferTransactions(wallet, financialYear);
            IEnumerable<InternalTx> internalTx = await this._Api.GetInternalTransactions(wallet, financialYear);

            List<EthereumCsvRow> rows = this.generateRows(wallet.Address, normalTx, tokenTx, internalTx);
            this.writeRows(rows, fileName);
        }

        /// <summary>
        /// Generates the rows that should be written to the CSV file in the correct order.
        /// </summary>
        private List<EthereumCsvRow> generateRows(DString address, IEnumerable<NormalTx> normalTx, IEnumerable<TokenTx> tokenTx, IEnumerable<InternalTx> internalTx)
        {
            List<EthereumCsvRow> rows = new List<EthereumCsvRow>();

            // Token Transfer Transactions *can* complement Normal Transactions.
            // For every hash, there may be one normal transaction and/or any number of Token Transfer Transactions (e.g. a trade on DDEX will have multiple ones), even zero (e.g. wrapping ETH into WETH). These can be matched by grouping by transaction hash.
            IEnumerable<IGrouping<DString, BlockBaseTx>> allTx = new List<BlockBaseTx>()
                .Concat(normalTx)
                .Concat(tokenTx)
                .OrderBy(tx => tx.TimeStamp)
                .GroupBy(tx => tx.Hash);

            foreach (IEnumerable<BlockBaseTx> txGroup in allTx)
            {
                NormalTx ntx = txGroup.Where(tx => tx is NormalTx).Select(tx => tx as NormalTx).FirstOrDefault();
                IEnumerable<TokenTx> ttx = txGroup.Where(tx => tx is TokenTx).Select(tx => tx as TokenTx);
                int numTtx = ttx?.Count() ?? 0;

                // the following block of variables is shared between all transactions of this group
                BlockBaseTx first = txGroup.First(); // never null
                DString hash = first.Hash;
                DateTime time = new DateTime().FromUnixSeconds(first.TimeStamp);
                decimal value = this.weiToEth(first.Value);
                decimal fee = this.getFee(first);
                bool isFeeApplied = false; // set to true when added to a row. we don't want to duplicate the fee for multiple rows that belong to the same underlying transaction

                if (numTtx == 0) // here, we assume that a normal tx never sends value when token transfer tx exist
                {
                    // it must be true that ntx is defined at this point
                    // note that we are not necessarily sending value - for example, in the case of wrapping to WETH (considered as sending), we only pay fees. still, this should be added as a row, since it modifies the inventory.
                    DString counterpartyAddress = ntx.From == address ? ntx.To : ntx.From;
                    Wallet counterparty = KnownWallets.HasWallet(counterpartyAddress) ? (Wallet)KnownWallets.GetWallet(counterpartyAddress) : new Wallet(counterpartyAddress, KnownCurrencies.ERC20, null, false, this.chainType);

                    if (ntx.From == address)
                    {
                        bool didWrap = false;
                        if (ntx.To == KnownWallets.ETH_WRAPPED_ETHER_CONTRACT.Address)
                        {
                            if (value == 0)
                            {
                                // unwrapping WETH back into ETH - must check matching internal transaction to get quantity
                                InternalTx intTx = internalTx.Where(tx => tx.Hash == hash).SingleOrDefault();
                                if (intTx != null)
                                {
                                    decimal qtySent = this.weiToEth(intTx.Value);
                                    Currency currencySent = KnownCurrencies.WETH;
                                    decimal qtyReceived = qtySent;
                                    Currency currencyReceived = KnownCurrencies.ETH;
                                    rows.Add(new EthereumCsvRow(hash, time, counterparty, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                                    didWrap = true;
                                }
                            }
                            else
                            {
                                // wrapping ETH into WETH - represent as trade
                                decimal qtySent = value;
                                Currency currencySent = KnownCurrencies.ETH;
                                decimal qtyReceived = value;
                                Currency currencyReceived = KnownCurrencies.WETH;
                                rows.Add(new EthereumCsvRow(hash, time, counterparty, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                                didWrap = true;
                            }
                        }

                        if (!didWrap)
                        {
                            // vanilla ETH sending
                            decimal qtySent = ntx.IsError == 1 ? 0 : value;
                            Currency currencySent = KnownCurrencies.ETH;
                            object _ = null;
                            rows.Add(new EthereumCsvRow(hash, time, counterparty, qtySent, currencySent, fee, _));
                        }
                        isFeeApplied = true;
                    }
                    else if (ntx.To == address)
                    {
                        // we are receiving
                        decimal qtyReceived = value;
                        Currency currencyReceived = KnownCurrencies.ETH;
                        rows.Add(new EthereumCsvRow(hash, time, counterparty, qtyReceived, currencyReceived, fee));
                        isFeeApplied = true;
                    }
                    else throw new Exception($"Received a normal transaction from the {this.chainType} network, but neither the 'from' nor the 'to' address matches our address. Tx hash: {ntx.Hash}");
                }
                else
                {
                    /*
                    We are probably trading (since Wrapping/Unwrapping doesn't show up as a Token Transfer Transaction - see e.g. 0x7194063f76f4275c7a08482620b7a56cc3aa90ef4894ac869bd418d99255042e)
                    Let W be our wallet address, B be the burn address, D be DDEX address, and C be the counterparty address.
                    Let's say we are buying a BOMB for b WETH. We expect the following transactions to exist (for BOMB):
                    C -> W: a BOMB
                    C -> B: 1 BOMB (BOMB specific)
                    W -> C: b WETH
                    W -> D: x WETH
                    And we can infer that the value of (a) BOMB is (ETH/AUD price)*(b) AUD, so the price per BOMB is (ETH/AUD price)*(b)/(a).
                    If there is a scenario where we make multiple trades with the same person at the same time, we probably have to consider only the net transfer, since it is impossible to match up the individual BOMB and WETH quantities reliably.
                    */

                    // we care only about the transactions in which we were involved
                    IEnumerable<TokenTx> primaryTtx = ttx.Where(tx => tx.From == address || tx.To == address);
                    IEnumerable<TokenTx> received = primaryTtx.Where(tx => tx.To == address);
                    IEnumerable<TokenTx> receivedSecondary = ttx.Except(primaryTtx).Where(tx => received.Select(rec => rec.From).Contains(tx.To)); // somebody else is sending to somebody from which we have received, keeping the token constant
                    IEnumerable<TokenTx> sent = primaryTtx.Where(tx => tx.From == address);
                    IEnumerable<TokenTx> sentSecondary = ttx.Except(primaryTtx).Where(tx => sent.Select(sen => sen.To).Contains(tx.From)); // somebody else receives from somebody to which we have sent, keeping the token constant

                    // find the counterparty - this will be the only address that interacts with our address both ways
                    IEnumerable<DString> counterpartyIn = received.Select(tx => tx.From);
                    // secondary transactions must have a matching primary transaction in the same direction and the same token
                    IEnumerable<DString> counterpartyInSecondary = receivedSecondary.Where(tx => received.FirstOrDefault(rec => rec.TokenSymbol == tx.TokenSymbol && rec.From == tx.To) != null).Select(tx => tx.From);
                    IEnumerable<DString> counterpartyOut = sent.Select(tx => tx.To);
                    IEnumerable<DString> counterpartyOutSecondary = sentSecondary.Where(tx => sent.FirstOrDefault(sen => sen.TokenSymbol == tx.TokenSymbol && sen.To == tx.From) != null).Select(tx => tx.To);
                    IEnumerable<DString> possibleCounterparties = counterpartyIn.Intersect(counterpartyOut)
                        .Concat(counterpartyIn.Intersect(counterpartyOutSecondary))
                        .Concat(counterpartyOut.Intersect(counterpartyInSecondary));
                    IEnumerable<TokenTx> oneWayTx = primaryTtx;

                    // if A -> C and C -> B -> A, and we are A, then the trade counterparty is C.
                    // if A -> B -> C, and C -> B -> A, then the trade counterparty is B.
                    foreach (DString tradeCounterparty in possibleCounterparties)
                    {
                        this.generateTradeRow(tradeCounterparty, received, receivedSecondary, sent, sentSecondary, ref oneWayTx, rows, hash, time, fee, ref isFeeApplied);
                    }

                    // chained transactions won't be recognised automatically because only transactions in which our address is involved show up.
                    // if we have more than 1 tokens transferred within the one-way transactions, we can treat them as a trade.
                    // however, this could also include a burn transaction.
                    // here, we deal with a very specific scenario, seen in https://etherscan.io/tx/0x51feff2df291cc5159cc17eb5b14f242fe7db2040522c346335fc9c626bfc070
                    // assume that we pay the seller directly, but receive the asset indirectly
                    IEnumerable<TokenTx> hackedOneWayTx = oneWayTx.Where(tx => !(tx.To == KnownWallets.ETH_DDEX_EXCHANGE_WALLET.Address && tx.TokenSymbol == KnownCurrencies.WETH.Symbol));
                    if (hackedOneWayTx.GroupBy(tx => tx.TokenSymbol).Count() == 2 && hackedOneWayTx.Count() <= 3)
                    {
                        // hack! yuck!!
                        DString hackedCounterparty = hackedOneWayTx.First(tx => tx.From == address && tx.To != KnownWallets.ETH_BURN_ADDRESS.Address).To;
                        hackedOneWayTx.Where(tx => tx.To == address).First().From = hackedCounterparty;
                        this.generateTradeRow(hackedCounterparty, received, receivedSecondary, sent, sentSecondary, ref oneWayTx, rows, hash, time, fee, ref isFeeApplied);
                    }

                    // another hack: in 0x5e24ee70604ef5999a562469a86ef902f9e1ec758782c73e316378fa980b8ae6, we currently have two one-way txs:
                    // one where we receive BOMB from a third party wallet
                    // the other where we send WETH to the DDEX. Under the hood, the DDEX passes on those funds to the third party wallet, but that tx is not shown because our address is not involved.
                    if (oneWayTx.Count() == 2
                        && counterpartyIn.Count() == 1
                        && oneWayTx.Where(tx => tx.From == counterpartyIn.Single() && tx.To == address).Count() == 1
                        && oneWayTx.Where(tx => tx.From == address && tx.To == KnownWallets.ETH_DDEX_EXCHANGE_WALLET.Address).Count() == 1)
                    {
                        DString hackedCounterparty = counterpartyIn.First();
                        oneWayTx.Where(tx => tx.From == address).Single().To = hackedCounterparty;
                        // it is not clear who actually paid the trading fee... so just assume it was us! if the government disagrees they better be able to show their working
                        this.generateTradeRow(hackedCounterparty, received, receivedSecondary, sent, sentSecondary, ref oneWayTx, rows, hash, time, fee, ref isFeeApplied);
                    }

                    // another special case - this doesn't fire for anything DDEX related it seems (no idea why and don't care)
                    // but does for Uniswap V2 e.g. https://etherscan.io/tx/0x2b0b249ac1c552d7ce876cded939ac7e1dd406a1f08345f1b81d1e05897130f9
                    // the normal transaction and single token transaction constitute a swap
                    if (numTtx == 1 && ntx != null && ntx.Value != 0)
                    {
                        // -> uniswap buy
                        TokenTx tx = ttx.First();
                        decimal qtySent = this.weiToEth(ntx.Value);
                        Currency currencySent = KnownCurrencies.ETH;
                        decimal qtyReceived = this.getQuantity(tx);

                        if (ntx.From == address && ntx.To == KnownWallets.ETH_UNISWAP_V2_ETH_IN.Address && tx.From == KnownWallets.ETH_UNISWAP_V2_MBT_OUT.Address && tx.To == address)
                        {
                            // legacy handler for buying MBT
                            Currency currencyReceived = KnownCurrencies.MBT;
                            rows.Add(new EthereumCsvRow(hash, time, KnownWallets.ETH_UNISWAP_V2_ETH_IN, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                        }
                        else if (ntx.From == address && (ntx.To == KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER.Address || ntx.To == KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER_2.Address || ntx.To == KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER.Address) && tx.From == KnownWallets.ETH_UNISWAP_V2_AMPL_POOL.Address && tx.To == address)
                        {
                            // buying AMPL on Uniswap V2 or V3
                            Wallet counterparty = ntx.To == KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER.Address ? KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER : KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER;
                            Currency currencyReceived = KnownCurrencies.AMPL;
                            rows.Add(new EthereumCsvRow(hash, time, counterparty, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                        }
                        else if (ntx.From == address && ntx.To == KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address && tx.From == KnownWallets.ETH_ARB_NOVA_SUSHISWAP_MOON_POOL.Address && tx.To == address)
                        {
                            // buying MOON on Sushiswap (arb nova)
                            Currency currencyReceived = KnownCurrencies.MOON;
                            rows.Add(new EthereumCsvRow(hash, time, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                        }
                        else
                        {
                            throw new NotImplementedException("Hack does not support this Uniswap buy scenario");
                        }

                        continue; // we know this is the end of the transaction, so continue and don't worry about oneWayTx.
                    }
                    else if (numTtx == 1 && ntx != null && ntx.Value == 0 && internalTx.Any(x => x.Hash == hash))
                    {
                        // -> uniswap sell
                        // this only works if we also have an internal transaction (to tell us how much ETH we received back. the normal and token transactions only mention the base token)
                        TokenTx tx = ttx.First();
                        InternalTx itx = internalTx.Single(x => x.Hash == hash);
                        Currency currencySent = KnownCurrencies.GetCurrency(tx.TokenSymbol).Value;
                        decimal qtySent = this.getQuantity(tx);
                        Currency currencyReceived = KnownCurrencies.ETH;
                        decimal qtyReceived = this.getQuantity(itx);

                        if (tx.From == address && tx.To == KnownWallets.ETH_UNISWAP_V2_AMPL_POOL.Address && (itx.From == KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER.Address || itx.From == KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER.Address) && itx.To == address)
                        {
                            // selling AMPL on Uniswap V2 or V3
                            Wallet counterparty = itx.From == KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER.Address ? KnownWallets.ETH_UNISWAP_V3_ETH_ROUTER : KnownWallets.ETH_UNISWAP_V2_ETH_ROUTER;
                            rows.Add(new EthereumCsvRow(hash, time, counterparty, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                        }
                        else if (tx.From == address && tx.To == KnownWallets.ETH_ARB_NOVA_SUSHISWAP_MOON_POOL.Address && itx.From == KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER.Address && itx.To == address)
                        {
                            // selling MOON on Sushiswap (arb nova)
                            rows.Add(new EthereumCsvRow(hash, time, KnownWallets.ETH_ARB_NOVA_SUSHISWAP_ROUTER, qtyReceived, currencyReceived, qtySent, currencySent, fee));
                        }
                        else
                        {
                            throw new NotImplementedException("Hack does not support this Uniswap sell scenario");
                        }

                        continue;
                    }

                    // we now handle any leftover token transactions, one row per tx (since they are now one-way, rather than a trade)
                    foreach (TokenTx tx in oneWayTx)
                    {
                        DString otherAddress = tx.To == address ? tx.From : tx.To;
                        Wallet otherWallet = KnownWallets.HasWallet(otherAddress) ? (Wallet)KnownWallets.GetWallet(otherAddress) : new Wallet(otherAddress, KnownCurrencies.ERC20, null, false, BlockchainType.Ethereum);
                        decimal quantity = this.getQuantity(tx);
                        Currency currency = KnownCurrencies.TryGetCurrency(tx.TokenSymbol);
                        decimal txFee = isFeeApplied ? 0 : fee;
                        object _ = null;
                        isFeeApplied = true;

                        if (tx.To == address)
                        {
                            // inbound
                            rows.Add(new EthereumCsvRow(hash, time, otherWallet, quantity, currency, txFee));
                        }
                        else
                        {
                            // outbound
                            rows.Add(new EthereumCsvRow(hash, time, otherWallet, quantity, currency, txFee, _));
                        }
                    }
                }
            }

            // there is one special case where internal transactions are found without being attached
            // to a normal/token transaction: in the case of withdrawing from the Hop Protocol.
            // note that this gets added to the end of the trade log, so the temporal order may be inconsistent.
            // this is fine since we are sorting everything by time at the time of importing.
            HashSet<DString> hashes = new List<BlockBaseTx>()
                .Concat(normalTx)
                .Concat(tokenTx)
                .Select(tx => tx.Hash)
                .ToHashSet();
            IEnumerable<InternalTx> detachedInternalTxs = internalTx.Where(tx => !hashes.Contains(tx.Hash));
            foreach (InternalTx tx in detachedInternalTxs)
            {
                if (tx.From != KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_MAINNET.Address)
                {
                    throw new Exception("Detached internal transactions are expected to originate from the Hop Protocol address");
                }

                // note that this is only requird on the mainnet side when receiving funds - the sending on the
                // ArbNova side works fine without any special handling.
                DateTime time = new DateTime().FromUnixSeconds(tx.TimeStamp);
                decimal quantity = this.getQuantity(tx);
                Currency currency = KnownCurrencies.ETH;
                rows.Add(new EthereumCsvRow(tx.Hash, time, KnownWallets.HOP_PROTOCOL_ETH_BRIDGE_MAINNET, quantity, currency, 0));
            }

            return rows;
        }

        private void generateTradeRow(DString tradeCounterparty, IEnumerable<TokenTx> received, IEnumerable<TokenTx> receivedSecondary, IEnumerable<TokenTx> sent, IEnumerable<TokenTx> sentSecondary, ref IEnumerable<TokenTx> oneWayTx, List<EthereumCsvRow> rows, DString hash, DateTime time, decimal fee, ref bool isFeeApplied)
        {
            // the transactions in which we have received a currency from the counterparty (directly or via a third man, such as an exchange)
            IEnumerable<TokenTx> tradeReceived = received.Where(tx =>
                    tx.From == tradeCounterparty
                    || receivedSecondary.Where(sec =>
                        sec.From == tradeCounterparty && sec.To == tx.From).Any());
            // the transactions in which we have sent a currency to the counterparty
            IEnumerable<TokenTx> tradeSent = sent.Where(tx =>
                tx.To == tradeCounterparty
                || sentSecondary.Where(sec =>
                        sec.From == tx.To && sec.To == tradeCounterparty).Any());
            // any other transactions
            oneWayTx = oneWayTx.Except(tradeReceived).Except(tradeSent);

            // validate data
            if (!tradeReceived.Any() || !tradeSent.Any()) throw new Exception($"Tried to generate Ethereum CSV row from trade on {this.chainType}, but found that not both sides had transactions.");
            else if (tradeReceived.GroupBy(tx => tx.TokenSymbol).Count() != 1) throw new Exception($"Tried to generate Ethereum CSV row from trade on {this.chainType}, but found that the number of currencies received in the trade is not 1.");
            else if (tradeSent.GroupBy(tx => tx.TokenSymbol).Count() != 1) throw new Exception($"Tried to generate Etherscan CSV row from trade on {this.chainType}, but found that the number of currencies sent in the trade is not 1. Hash.");

            // generate row
            decimal qtyReceived = tradeReceived.Sum(tx => this.getQuantity(tx));
            Currency currencyReceived = KnownCurrencies.TryGetCurrency(tradeReceived.First().TokenSymbol);
            decimal qtySent = tradeSent.Sum(tx => this.getQuantity(tx));
            Currency currencySent = KnownCurrencies.TryGetCurrency(tradeSent.First().TokenSymbol);

            rows.Add(new EthereumCsvRow(
                hash,
                time,
                new Wallet(tradeCounterparty, KnownCurrencies.ERC20, null, false, this.chainType),
                qtyReceived,
                currencyReceived,
                qtySent,
                currencySent,
                isFeeApplied ? 0 : fee));
            isFeeApplied = true;
        }

        /// <summary>
        /// Returns the fee (in ETH) of the transaction
        /// </summary>
        private decimal getFee(BlockBaseTx tx)
        {
            decimal gasPriceEth = this.weiToEth(tx.GasPrice);
            return tx.GasUsed * gasPriceEth;
        }

        /// <summary>
        /// Converts the given amount of Wei to Ether
        /// </summary>
        private decimal weiToEth(BigInteger wei)
        {
            // there are 10^9 (1 Billion) Wei in a Gwei and 10^9 Gwei in an ETH
            return (decimal)wei / 1000000000 / 1000000000;
        }

        private decimal getQuantity(TokenTx tx)
        {
            return (decimal)tx.Value / (decimal)Math.Pow(10, tx.TokenDecimal);
        }

        private decimal getQuantity(InternalTx tx)
        {
            // ETH has 18 token decimals
            return (decimal)tx.Value / (decimal)Math.Pow(10, 18);
        }

        private void writeRows(List<EthereumCsvRow> rows, DString fileName)
        {
            if (!fileName.Value.EndsWith(".csv", StringComparison.OrdinalIgnoreCase))
            {
                fileName += ".csv";
            }

            List<NString> lines = new List<NString>() { EthereumCsvRow.CsvHeader() };
            lines.AddRange(rows.Select(row => (NString)row.ToCsv()));
            this._FileService.WriteLines(fileName, lines, false);
        }
    }
}
