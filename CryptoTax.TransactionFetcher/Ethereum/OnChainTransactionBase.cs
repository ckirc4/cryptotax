﻿using System.Numerics;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public abstract class OnChainTransactionBase
    {
        public long BlockNumber { get; set; }
        /// <summary>
        /// Unix seconds
        /// </summary>
        public long TimeStamp { get; set; }
        public DString Hash { get; set; }
        public DString From { get; set; }
        public DString To { get; set; }
        public BigInteger Value { get; set; }
        public long Gas { get; set; }
        public NString Input { get; set; }
        public NString ContractAddress { get; set; }
        public long GasUsed { get; set; }

        public OnChainTransactionBase(dynamic obj)
        {
            this.BlockNumber = (long)obj.blockNumber;
            this.TimeStamp = (long)obj.timeStamp;
            this.Hash = (string)obj.hash;
            this.From = (string)obj.from;
            string contractAddress = (string)obj.contractAddress;
            this.ContractAddress = string.IsNullOrEmpty(contractAddress) ? null : contractAddress;
            this.To = (string)obj.to;
            this.Value = (BigInteger)obj.value;
            this.Gas = (long)obj.gas;
            this.GasUsed = (long)obj.gasUsed;
            string input = (string)obj.input;
            this.Input = string.IsNullOrEmpty(input) ? null : input;
        }
    }
}