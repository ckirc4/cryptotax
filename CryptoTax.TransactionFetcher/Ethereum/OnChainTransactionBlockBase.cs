﻿using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public abstract class OnChainTransactionBlockBase : OnChainTransactionBase
    {
        public long Nonce { get; set; }
        public DString BlockHash { get; set; }
        public int TransactionIndex { get; set; }
        public long GasPrice { get; set; }
        public long CumulativeGasUsed { get; set; }
        public long Confirmations { get; set; }

        public OnChainTransactionBlockBase(dynamic obj) : base((object)obj)
        {
            this.Nonce = (long)obj.nonce;
            this.BlockHash = (string)obj.blockHash;
            this.TransactionIndex = (int)obj.transactionIndex;
            this.GasPrice = (long)obj.gasPrice;
            this.CumulativeGasUsed = (long)obj.cumulativeGasUsed;
            this.Confirmations = (long)obj.confirmations;
        }
    }
}