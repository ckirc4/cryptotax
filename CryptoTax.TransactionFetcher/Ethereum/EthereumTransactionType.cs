﻿namespace CryptoTax.TransactionFetcher.Ethereum
{
    public enum EthereumTransactionType
    {
        ReceivedFromCounterparty,
        SentToCounterparty,
        TradedWithCounterparty,
        Unknown
    }
}