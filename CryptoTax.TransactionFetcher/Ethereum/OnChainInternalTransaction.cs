﻿namespace CryptoTax.TransactionFetcher.Ethereum
{
    public class OnChainInternalTransaction : OnChainTransactionBase
    {
        public string Type { get; private set; }
        public string TraceId { get; private set; }
        public bool IsError { get; }
        public string ErrCode { get; private set; }

        public OnChainInternalTransaction(dynamic obj) : base((object)obj)
        {
            /*
            Example object:
            {
	            "blockNumber":"7975868",
	            "timeStamp":"1560774069",
	            "hash":"0x7194063f76f4275c7a08482620b7a56cc3aa90ef4894ac869bd418d99255042e",
	            "from":"0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2",
	            "to":"0x7042db4c642f6ac868df6717b65be6d4590ea720",
	            "value":"7683300000000000000",
	            "contractAddress":"",
	            "input":"",
	            "type":"call",
	            "gas":"2300",
	            "gasUsed":"0",
	            "traceId":"0_1",
	            "isError":"0",
	            "errCode":""
            }
            */

            this.Type = (string)obj.type;
            this.TraceId = (string)obj.traceId;
            this.IsError = (string)obj.isError == "0" ? false : true;
            this.ErrCode = (string)obj.errCode;
        }
    }
}