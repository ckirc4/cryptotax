﻿using CryptoTax.Shared;
using CryptoTax.Shared.Exceptions;
using CryptoTax.Shared.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UtilityLibrary.Timing;
using UtilityLibrary.Types;

namespace CryptoTax.TransactionFetcher.Ethereum
{
    public class EthereumApiService : IEthereumApiService
    {
        private readonly IWebService _WebService;
        private readonly EthereumChainType chainType;

        public EthereumApiService(IWebService webService, EthereumChainType chainType)
        {
            this._WebService = webService;
            this.chainType = chainType;
        }

        public async Task<IEnumerable<OnChainNormalTransaction>> GetNormalTransactions(Wallet wallet, FinancialYear financialYear)
        {
            IEnumerable<dynamic> dynamicTransactions = await this.getAccountData("txlist", wallet.Address, financialYear);
            return dynamicTransactions.Select(tx => new OnChainNormalTransaction(tx));
        }

        public async Task<IEnumerable<OnChainTokenTransferTransaction>> GetTokenTransferTransactions(Wallet wallet, FinancialYear financialYear)
        {
            IEnumerable<dynamic> dynamicTransactions = await this.getAccountData("tokentx", wallet.Address, financialYear);
            return dynamicTransactions.Select(tx => new OnChainTokenTransferTransaction(tx));
        }

        public async Task<IEnumerable<OnChainInternalTransaction>> GetInternalTransactions(Wallet wallet, FinancialYear financialYear)
        {
            IEnumerable<dynamic> dynamicTransactions = await this.getAccountData("txlistinternal", wallet.Address, financialYear);
            return dynamicTransactions.Select(tx => new OnChainInternalTransaction(tx));
        }

        private async Task<IEnumerable<dynamic>> getAccountData(DString action, DString address, FinancialYear financialYear)
        {
            DString url = this.getUrl(action, address);
            DString result = await this._WebService.DownloadStringAsync(url);

            // response format: {"status":"1","message":"OK","result":[{"timeStamp":<unix_seconds>,...]}
            dynamic parsed = JObject.Parse(result) as dynamic;
            IEnumerable<dynamic> fullResults = parsed.result as IEnumerable<dynamic>;

            if (fullResults.Count() >= 10_000)
            {
                throw new Exception("Request returned the maximum number of records (10,000), please implement pagination.");
            }

            long start = financialYear.Start.ToUnixSeconds();
            long end = financialYear.End.ToUnixSeconds();
            return fullResults
                .Where(r => r.timeStamp >= start && r.timeStamp <= end)
                .OrderBy(r => r.timeStamp)
                .ToList();
        }

        private DString getUrl(DString action, DString address)
        {
            string baseUrl;
            string apiKey;

            switch (this.chainType)
            {
                case EthereumChainType.Mainnet:
                    baseUrl = "https://api.etherscan.io/api";
                    apiKey = Constants.API_KEY_ETHERSCAN;
                    break;
                case EthereumChainType.ArbitrumNova:
                    baseUrl = "https://api-nova.arbiscan.io/api";
                    apiKey = Constants.API_KEY_ARBISCAN;
                    break;
                default:
                    throw new AssertUnreachable(this.chainType);
            }

            return $"{baseUrl}?module=account&action={action}&address={address}&apikey={apiKey}";
        }
    }
}
