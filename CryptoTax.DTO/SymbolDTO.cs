﻿using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class SymbolDTO
    {
        public DString QuoteCurrency { get; set; }
        public DString BaseCurrency { get; set; }

        public SymbolDTO(DString quoteCurrency, DString baseCurrency)
        {
            this.QuoteCurrency = quoteCurrency;
            this.BaseCurrency = baseCurrency;
        }

        public override bool Equals(object obj)
        {
            if (obj is SymbolDTO other)
            {
                return this.QuoteCurrency == other.QuoteCurrency
                    && this.BaseCurrency == other.BaseCurrency;
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return new
            {
                this.QuoteCurrency,
                this.BaseCurrency
            }.GetHashCode();
        }
    }
}
