﻿using Newtonsoft.Json;

namespace CryptoTax.DTO
{
    public class CapitalGainsDTO
    {
        [JsonProperty]
        public decimal StandardCapitalGains { get; private set; }
        [JsonProperty]
        public decimal DiscountableCapitalGains { get; private set; }
        [JsonProperty]
        public decimal CapitalLosses { get; private set; }

        public CapitalGainsDTO(decimal standardCapitalGains, decimal discountedCapitalGains, decimal capitalLosses)
        {
            this.StandardCapitalGains = standardCapitalGains;
            this.DiscountableCapitalGains = discountedCapitalGains;
            this.CapitalLosses = capitalLosses;
        }

        // bad practice! use only for testing!
        public void Reset_Do_Not_Use()
        {
            this.StandardCapitalGains = 0;
            this.DiscountableCapitalGains = 0;
            this.CapitalLosses = 0;
        }
    }
}
