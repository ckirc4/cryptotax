﻿using CryptoTax.Shared.Enums;
using Newtonsoft.Json;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class InventoryItemDTO
    {
        [JsonProperty("i")]
        public DString Id { get; }
        [JsonProperty("o")]
        public NString OriginalId { get; }
        [JsonProperty("g")]
        public DateTime OriginalTime { get; }
        [JsonProperty("c")]
        public DString Currency { get; }
        [JsonProperty("a")]
        public decimal Amount { get; }
        [JsonProperty("v")]
        public decimal Value { get; }
        [JsonProperty("t")]
        public DateTime TimeModified { get; }
        [JsonProperty("e")]
        public Exchange? ExchangeAcquired { get; }

        public InventoryItemDTO(DString id, NString originalId, DateTime originalTime, DString currency, decimal amount, decimal value, DateTime timeModified, Exchange? exchangeAcquired)
        {
            this.Id = id;
            this.OriginalId = originalId;
            this.OriginalTime = originalTime;
            this.Currency = currency;
            this.Amount = amount;
            this.Value = value;
            this.TimeModified = timeModified;
            this.ExchangeAcquired = exchangeAcquired;
        }
    }
}
