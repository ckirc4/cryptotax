﻿using CryptoTax.Shared;
using CryptoTax.Shared.Attributes;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Services.Logging;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    /// <summary>
    /// Properties relating to a run's configuration.
    /// </summary>
    public class RunInfoDTO
    {
        public DString Id { get; set; }

        /// <summary>
        /// The args that were originally used to generate this RunInfo.
        /// </summary>
        public string[] OriginalArgs { get; set; }

        [ArgKey("name")]
        public NString Name { get; set; }

        [ArgKey("financialYear", "year", "fy", "t")]
        public FinancialYear FinancialYear { get; set; }

        /// <summary>
        /// **Unused - implied value: FILO**
        /// </summary>
        [ArgKey("inventoryType", "inventory")]
        public InventoryType InventoryType { get; set; }

        [ArgKey("candlestickInterval", "interval")]
        public CandlestickInterval CandlestickInterval { get; set; }

        [ArgKey("priceCalculationType", "priceCalculation", "priceType", "price")]
        public PriceCalculationType PriceCalculationType { get; set; }

        [ArgKey("inventoryManagementType", "inventoryManagement", "managementType", "management")]
        public InventoryManagementType InventoryManagementType { get; set; }

        /// <summary>
        /// The number of decimal places to which to round (AUD values ONLY). If null, does not do rounding.
        /// <br/>
        /// **Unused - implied value: null**
        /// </summary>
        [ArgKey("decimalPlaces", "decimals")]
        public int? DecimalPlaces { get; set; }

        /// <summary>
        /// **Unused - implied value: None**
        /// </summary>
        [ArgKey("roundingType", "rounding")]
        public RoundingType RoundingType { get; set; }

        /// <summary>
        /// Disabling reporting immensely speeds up application, so could be used to "scout" for configuration.
        /// </summary>
        [ArgKey("enableReporting", "reporting", "generateReport")]
        public bool EnableReporting { get; set; }

        [ArgKey("consoleLoggingTemplate", "consoleLogging", "consoleLogTemplate", "consoleLog")]
        public LoggingTemplate? ConsoleLoggingTemplate { get; set; }

        [ArgKey("persistorLoggingTemplate", "persistorLogging", "persistorLogTemplate", "persistorLog")]
        public LoggingTemplate? PersistorLoggingTemplate { get; set; }

        /// <summary>
        /// The state used at the beginning of the run. May be null.
        /// </summary>
        public RunStateDTO BeginningState { get; set; }

        /// <summary>
        /// DO NOT USE THIS EXPLICITLY. Exists only for deserialisation.
        /// </summary>
        public RunInfoDTO()
        { }

        /// <summary>
        /// Generates a new RunInfo with default settings and empty state.
        /// <br/>
        /// Careful when changing defaults, as existing scripts may be affected.
        /// </summary>
        public RunInfoDTO(
            DString id, // required
            string[] originalArgs, // required
            NString name = default,
            FinancialYear financialYear = default,
            InventoryType inventoryType = InventoryType.FILO,
            CandlestickInterval candlestickInterval = CandlestickInterval.Day_1,
            PriceCalculationType priceCalculationType = PriceCalculationType.Open,
            InventoryManagementType inventoryManagementType = InventoryManagementType.Single,
            int? decimalPlaces = null,
            RoundingType roundingType = RoundingType.None,
            bool enableReporting = true,
            LoggingTemplate? consoleLoggingTemplate = LoggingTemplate.Console_Verbose,
            LoggingTemplate? persistorLoggingTemplate = null,
            RunStateDTO beginningState = null)
        {
            /*
                Things to note when adding new properties:
                    - make sure property has a PUBLIC setter - otherwise deserialisation fails
                    - add getter to IRunService
                    - update runInfoFromService in RunServiceTests
                    - must have ArgKeyAttribute
                    - ArgKeyAttribute names should never be removed, only added to
                    - default value must be non-breaking for old runs
                    - properties cannot be removed, they can only be deprecated
                    - update the docs (Readme.md)
            */

            this.Id = id;
            this.OriginalArgs = originalArgs;
            this.Name = name;
            this.FinancialYear = financialYear;
            this.InventoryType = inventoryType;
            this.CandlestickInterval = candlestickInterval;
            this.PriceCalculationType = priceCalculationType;
            this.InventoryManagementType = inventoryManagementType;
            this.DecimalPlaces = decimalPlaces;
            this.RoundingType = roundingType;
            this.EnableReporting = enableReporting;
            this.ConsoleLoggingTemplate = consoleLoggingTemplate;
            this.PersistorLoggingTemplate = persistorLoggingTemplate;
            this.BeginningState = beginningState;
        }
    }
}
