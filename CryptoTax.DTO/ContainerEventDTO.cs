﻿using CryptoTax.DTO.Serialised;
using CryptoTax.Shared;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    /// <summary>
    /// Represents an event in which a <seealso cref="Container"/> has changed in some way, e.g. a trade, fee, withdrawal, or futures funding.
    /// </summary>
    public class ContainerEventDTO
    {
        /// <summary>
        /// UTC
        /// </summary>
        public DateTime EventTime { get; set; }
        public Container Container { get; set; }

        public NString CurrencySent { get; set; }
        /// <summary>
        /// Positive. Once the amount sent has been subtracted from the inventory, the fee amount will also be subtracted.
        /// </summary>
        public decimal? AmountSent { get; set; }

        public NString CurrencyReceived { get; set; }
        /// <summary>
        /// Positive. For deposits with a fee, the fee will be subtracted after the amount has been received.
        /// </summary>
        public decimal? AmountReceived { get; set; }

        /// <summary>
        /// Always destroyed, never transferred.
        /// </summary>
        public NString FeeCurrency { get; set; }
        /// <summary>
        /// Positive. The fee amount will be subtracted from the inventory after amount sent.
        /// </summary>
        // todo: currently does not support negative fees - will throw in many places. should negative fees be considered a transaction of type FeeRebate?
        public decimal? FeeAmount { get; set; }

        public ContainerEventDTO(DateTime eventTime, Container container, NString currencySent, decimal? amountSent, NString currencyReceived, decimal? amountReceived, NString feeCurrency, decimal? feeAmount)
        {
            this.EventTime = eventTime;
            this.Container = container;
            this.CurrencySent = currencySent;
            this.AmountSent = amountSent;
            this.CurrencyReceived = currencyReceived;
            this.AmountReceived = amountReceived;
            this.FeeCurrency = feeCurrency;
            this.FeeAmount = feeAmount;
        }

        public override bool Equals(object obj)
        {
            if (obj is ContainerEventDTO other)
            {
                return this.EventTime == other.EventTime
                    && this.Container == other.Container
                    && this.CurrencySent == other.CurrencySent
                    && this.AmountSent == other.AmountSent
                    && this.CurrencyReceived == other.CurrencyReceived
                    && this.AmountReceived == other.AmountReceived
                    && this.FeeCurrency == other.FeeCurrency
                    && this.FeeAmount == other.FeeAmount;
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return new
            {
                this.EventTime,
                this.Container,
                this.CurrencySent,
                this.AmountSent,
                this.CurrencyReceived,
                this.AmountReceived,
                this.FeeCurrency,
                this.FeeAmount
            }.GetHashCode();
        }

        public SerialisedContainerEventDTO Serialise()
        {
            return new SerialisedContainerEventDTO(this);
        }

        public static ContainerEventDTO Deserialise(SerialisedContainerEventDTO serialised)
        {
            return serialised.DeserialiseContainerEvent();
        }
    }
}
