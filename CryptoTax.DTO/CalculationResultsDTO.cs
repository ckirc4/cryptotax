﻿namespace CryptoTax.DTO
{
    public class CalculationResultsDTO
    {
        public CapitalGainsDTO CapitalGains { get; }
        public decimal NetCapitalGains { get; }
        public decimal AssessableIncome { get; }

        public CalculationResultsDTO(CapitalGainsDTO capitalGains, decimal netCapitalGains, decimal assessableIncome)
        {
            this.CapitalGains = capitalGains;
            this.NetCapitalGains = netCapitalGains;
            this.AssessableIncome = assessableIncome;
        }
    }
}
