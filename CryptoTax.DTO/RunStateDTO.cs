﻿using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    /// <summary>
    /// Holds properties that represent a run's state, for example the input or output state.
    /// </summary>
    public class RunStateDTO
    {
        /// <summary>
        /// The run id from which this state originated. E.g. if #n1, this state was the output from run 1.
        /// </summary>
        public DString RunId { get; }
        public IEnumerable<InventoryDTO> Inventories { get; }


        public RunStateDTO(DString runId, IEnumerable<InventoryDTO> inventories)
        {
            this.RunId = runId;
            this.Inventories = inventories;
        }
    }
}
