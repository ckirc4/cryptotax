﻿using CryptoTax.Shared.Enums;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class WalletAddressDTO
    {
        public DString Address { get; set; }
        public NString AddressLabel { get; set; }
        public BlockchainType ChainType { get; set; }
        public AddressOwner Owner { get; set; }

        public WalletAddressDTO(DString address, NString addressLabel, BlockchainType chainType, AddressOwner owner)
        {
            this.Address = address;
            this.AddressLabel = addressLabel;
            this.ChainType = chainType;
            this.Owner = owner;
        }
    }
}
