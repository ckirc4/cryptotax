﻿using CryptoTax.DTO.Serialised;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class TradeDTO
    {
        /// <summary>
        /// UTC Time
        /// </summary>
        public DateTime Time { get; set; }
        public Exchange Exchange { get; set; }
        public DString TradeId { get; set; }
        public SymbolDTO Symbol { get; set; }
        public MarketType MarketType { get; set; }
        public OrderSide Side { get; set; }
        /// <summary>
        /// Marked price, as seen in the orderbooks. (this should be the same as the calculated price given received/sent amounts if there were no fees - otherwise, will cause an error when processed).
        /// <br/>
        /// Reminder that received and sent amounts are those that directly constituted the trade with the other user, and fees are separate.
        /// </summary>
        public decimal Price { get; set; }
        public ContainerEventDTO ContainerEvent { get; set; }

        public TradeDTO(DateTime time, Exchange exchange, DString tradeId, SymbolDTO symbol, MarketType marketType, OrderSide side, decimal price, ContainerEventDTO disposable)
        {
            this.Time = time;
            this.Exchange = exchange;
            this.TradeId = tradeId;
            this.Symbol = symbol;
            this.MarketType = marketType;
            this.Side = side;
            this.Price = price;
            this.ContainerEvent = disposable;
        }

        public override bool Equals(object obj)
        {
            if (obj is TradeDTO other)
            {
                return this.Time == other.Time
                    && this.Exchange == other.Exchange
                    && this.TradeId == other.TradeId
                    && this.Symbol.Equals(other.Symbol)
                    && this.MarketType == other.MarketType
                    && this.Side == other.Side
                    && this.Price == other.Price
                    && this.ContainerEvent.Equals(other.ContainerEvent);
            }
            else return false;
        }

        public override int GetHashCode()
        {
            return new
            {
                this.Time,
                this.Exchange,
                this.TradeId,
                this.Symbol,
                this.MarketType,
                this.Side,
                this.Price,
                this.ContainerEvent
            }.GetHashCode();
        }

        public SerialisedTradeDTO Serialise()
        {
            return new SerialisedTradeDTO(this);
        }

        public static TradeDTO Deserialise(SerialisedTradeDTO serialised)
        {
            return serialised.DeserialiseTrade();
        }
    }
}
