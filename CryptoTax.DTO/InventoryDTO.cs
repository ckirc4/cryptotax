﻿using CryptoTax.Shared.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class InventoryDTO
    {
        [JsonProperty("n")]
        public DString Name { get; }
        [JsonProperty("t")]
        public InventoryType Type { get; }
        [JsonProperty("i")]
        public DString Id { get; }
        [JsonProperty("e")]
        public Exchange Exchange { get; }
        [JsonProperty("m")]
        public Dictionary<string, IEnumerable<InventoryItemDTO>> Items { get; }
        [JsonProperty("g")]
        public CapitalGainsDTO CapitalGains { get; }

        public InventoryDTO(DString name, InventoryType type, DString id, Exchange exchange, Dictionary<string, IEnumerable<InventoryItemDTO>> items, CapitalGainsDTO capitalGains)
        {
            this.Name = name;
            this.Type = type;
            this.Id = id;
            this.Exchange = exchange;
            this.Items = items;
            this.CapitalGains = capitalGains;
        }
    }

}
