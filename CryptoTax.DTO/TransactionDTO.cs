﻿using CryptoTax.DTO.Serialised;
using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    /// <summary>
    /// When withdrawing or depositing, the container event must indicate the amount removed/added to the particular exchange/inventory.
    /// </summary>
    public class TransactionDTO
    {
        /// <summary>
        /// UTC Date
        /// </summary>
        public DateTime Time { get; set; }
        public Exchange Exchange { get; set; }
        /// <summary>
        /// e.g. Centralised exchange transaction id.
        /// </summary>
        public NString InternalTransactionId { get; set; }
        /// <summary>
        /// e.g. Blockchain transaction hash.
        /// </summary>
        public NString PublicTransactionId { get; set; }
        /// <summary>
        /// The other party (e.g. address received from/sent to), if known and if applicable
        /// </summary>
        public NString Counterparty { get; set; }
        public TransactionType Type { get; set; }
        public ContainerEventDTO ContainerEvent { get; set; }

        public TransactionDTO(DateTime date, Exchange exchange, NString internalTransactionId, NString publicTransactionId, NString counterparty, TransactionType type, ContainerEventDTO containerEvent)
        {
            if (internalTransactionId == null && publicTransactionId == null) throw new ArgumentException("At least one transaction id type must be defined.");

            this.Time = date;
            this.Exchange = exchange;
            this.InternalTransactionId = internalTransactionId;
            this.PublicTransactionId = publicTransactionId;
            this.Counterparty = counterparty;
            this.Type = type;
            this.ContainerEvent = containerEvent;
        }

        public override bool Equals(object obj)
        {
            if (obj is TransactionDTO other)
            {
                return this.Time == other.Time
                    && this.Exchange == other.Exchange
                    && this.InternalTransactionId == other.InternalTransactionId
                    && this.PublicTransactionId == other.PublicTransactionId
                    && this.Counterparty == other.Counterparty
                    && this.Type == other.Type
                    && this.ContainerEvent.Equals(other.ContainerEvent);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return new
            {
                this.Time,
                this.Exchange,
                this.InternalTransactionId,
                this.PublicTransactionId,
                this.Counterparty,
                this.Type,
                this.ContainerEvent
            }.GetHashCode();
        }

        public SerialisedTransactionDTO Serialise()
        {
            return new SerialisedTransactionDTO(this);
        }

        public static TransactionDTO Deserialise(SerialisedTransactionDTO serialised)
        {
            return serialised.DeserialiseTx();
        }
    }
}
