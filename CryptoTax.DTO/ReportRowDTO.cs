﻿using CryptoTax.Shared.Enums;
using System;
using UtilityLibrary.Types;

namespace CryptoTax.DTO
{
    public class ReportRowDTO
    {
        /// <summary>
        /// The id of the row in the report
        /// </summary>
        public DString Id { get; private set; }
        public DateTime Time { get; private set; }
        public ReportRowCategory Category { get; private set; }
        /// <summary>
        /// The data types used to generate this row
        /// </summary>
        public ReportRowType[] Types { get; private set; }
        public DString Message { get; private set; }
        public decimal? StandardCapitalGains { get; private set; }
        public decimal? DiscountableCapitalGains { get; private set; }
        public decimal? CapitalLosses { get; private set; }
        public decimal? AssessableIncome { get; private set; }

        public ReportRowDTO(DString id, DateTime time, ReportRowCategory category, ReportRowType[] types, DString message) : this(id, time, category, types, message, null, null, null, null)
        { }

        public ReportRowDTO(DString id, DateTime time, ReportRowCategory category, ReportRowType[] types, DString message, decimal? standardCapitalGains, decimal? discountableCapitalGains, decimal? capitalLosses, decimal? assessableIncome)
        {
            this.Id = id;
            this.Time = time;
            this.Types = types;
            this.Category = category;
            this.Message = message;
            this.StandardCapitalGains = standardCapitalGains;
            this.DiscountableCapitalGains = discountableCapitalGains;
            this.CapitalLosses = capitalLosses;
            this.AssessableIncome = assessableIncome;
        }
    }
}
