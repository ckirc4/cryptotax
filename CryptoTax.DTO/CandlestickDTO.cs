﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using System;

namespace CryptoTax.DTO
{
    public class CandlestickDTO
    {
        /// <summary>
        /// Open time
        /// </summary>
        public DateTime Time { get; set; }
        public CandlestickInterval Interval { get; set; }
        public ExchangeSymbol Symbol { get; set; }
        public decimal Open { get; set; }
        public decimal Close { get; set; }

        public CandlestickDTO(DateTime time, CandlestickInterval interval, ExchangeSymbol symbol, decimal open, decimal close)
        {
            this.Time = time;
            this.Interval = interval;
            this.Symbol = symbol;
            this.Open = open;
            this.Close = close;
        }
    }
}
