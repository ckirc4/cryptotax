﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using CryptoTax.Shared.Helpers;
using Newtonsoft.Json;
using System;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedCandlestickDTO
    {
        [JsonProperty("a")]
        public int Offset { get; set; }
        [JsonProperty("b")]
        public decimal Open { get; set; }
        [JsonProperty("c")]
        public decimal Close { get; set; }

        public SerialisedCandlestickDTO() { }

        public SerialisedCandlestickDTO(CandlestickDTO c, DateTime baseTime)
        {
            this.Offset = CandlestickHelpers.GetIntervalsBetween(baseTime, c.Time, c.Interval);
            this.Open = c.Open;
            this.Close = c.Close;
        }

        public CandlestickDTO Deserialise(DateTime baseTime, CandlestickInterval interval, ExchangeSymbol symbol)
        {
            DateTime time = CandlestickHelpers.GetTimeAfterIntervals(interval, baseTime, this.Offset);
            return new CandlestickDTO(time, interval, symbol, this.Open, this.Close);
        }
    }
}
