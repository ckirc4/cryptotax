﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedWallet
    {
        [JsonProperty("a")]
        public string Address;
        [JsonProperty("b")]
        public IEnumerable<SerialisedCurrency> CurrenciesAccepted;
        [JsonProperty("c")]
        public Exchange? ExchangeOwner;
        [JsonProperty("d")]
        public bool HasControl;
        [JsonProperty("e")]
        public BlockchainType Blockchain;
        [JsonProperty("f")]
        public string Name;
        [JsonProperty("g")]
        public string Description;

        public SerialisedWallet() { }

        public SerialisedWallet(Wallet wallet)
        {
            this.Address = wallet.Address;
            this.CurrenciesAccepted = wallet.CurrenciesAccepted.Select(c => new SerialisedCurrency(c));
            this.ExchangeOwner = wallet.ExchangeOwner;
            this.HasControl = wallet.HasControl;
            this.Blockchain = wallet.Blockchain;
            this.Name = wallet.Name;
            this.Description = wallet.Description;
        }

        public Wallet Deserialise()
        {
            return Wallet.FromSerialised(this.Address, this.CurrenciesAccepted.Select(c => c.Deserialise()), this.ExchangeOwner, this.HasControl, this.Blockchain, this.Name, this.Description);
        }
    }
}
