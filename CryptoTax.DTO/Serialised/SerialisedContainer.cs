﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Newtonsoft.Json;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedContainer
    {
        [JsonProperty("a")]
        public string Id;
        [JsonProperty("b")]
        public ContainerType Type;
        [JsonProperty("c")]
        public SerialisedWallet Wallet;
        [JsonProperty("d")]
        public Exchange? Exchange;

        public SerialisedContainer() { }

        public SerialisedContainer(Container container)
        {
            this.Id = container.Id;
            this.Type = container.Type;
            this.Wallet = container.Wallet.HasValue ? new SerialisedWallet(container.Wallet.Value) : null;
            this.Exchange = container.Exchange;
        }

        public Container DeserialiseContainer()
        {
            return Container.FromSerialised(this.Id, this.Type, this.Wallet?.Deserialise(), this.Exchange);
        }
    }
}
