﻿using CryptoTax.Shared.Enums;
using Newtonsoft.Json;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedTradeDTO : SerialisedContainerEventDTO
    {
        [JsonProperty("l")]
        public Exchange TradeExchange;
        [JsonProperty("m")]
        public string TradeId;
        [JsonProperty("n")]
        public string Quote;
        [JsonProperty("o")]
        public string Base;
        [JsonProperty("p")]
        public MarketType MarketType;
        [JsonProperty("q")]
        public OrderSide Side;
        [JsonProperty("r")]
        public decimal Price;

        public SerialisedTradeDTO() { }

        public SerialisedTradeDTO(TradeDTO trade) : base(trade.ContainerEvent)
        {
            this.TradeExchange = trade.Exchange;
            this.TradeId = trade.TradeId;
            this.Quote = trade.Symbol.QuoteCurrency;
            this.Base = trade.Symbol.BaseCurrency;
            this.MarketType = trade.MarketType;
            this.Side = trade.Side;
            this.Price = trade.Price;
        }

        public TradeDTO DeserialiseTrade()
        {
            return new TradeDTO(this.Time, this.TradeExchange, this.TradeId, new SymbolDTO(this.Quote, this.Base), this.MarketType, this.Side, this.Price, this.DeserialiseContainerEvent());
        }
    }
}
