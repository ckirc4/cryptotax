﻿using Newtonsoft.Json;
using System;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedContainerEventDTO : SerialisedContainer
    {
        [JsonProperty("e")]
        public DateTime Time;
        [JsonProperty("f")]
        public string CurrencySent;
        [JsonProperty("g")]
        public decimal? AmountSent;
        [JsonProperty("h")]
        public string CurrencyReceived;
        [JsonProperty("i")]
        public decimal? AmountReceived;
        [JsonProperty("j")]
        public string FeeCurrency;
        [JsonProperty("k")]
        public decimal? FeeAmount;

        public SerialisedContainerEventDTO() { }

        public SerialisedContainerEventDTO(ContainerEventDTO e) : base(e.Container)
        {
            this.Time = e.EventTime;
            this.CurrencySent = e.CurrencySent;
            this.AmountSent = e.AmountSent;
            this.CurrencyReceived = e.CurrencyReceived;
            this.AmountReceived = e.AmountReceived;
            this.FeeCurrency = e.FeeCurrency;
            this.FeeAmount = e.FeeAmount;
        }

        public ContainerEventDTO DeserialiseContainerEvent()
        {
            return new ContainerEventDTO(this.Time, this.DeserialiseContainer(), this.CurrencySent, this.AmountSent, this.CurrencyReceived, this.AmountReceived, this.FeeCurrency, this.FeeAmount);
        }
    }
}
