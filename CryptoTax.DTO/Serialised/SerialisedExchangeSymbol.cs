﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedExchangeSymbol
    {
        public SerialisedCurrency QuoteCurrency { get; set; }
        public SerialisedCurrency BaseCurrency { get; set; }
        public string Name { get; set; }
        public Exchange Exchange { get; set; }

        public SerialisedExchangeSymbol(ExchangeSymbol symbol)
        {
            this.QuoteCurrency = new SerialisedCurrency(symbol.Symbol.QuoteCurrency);
            this.BaseCurrency = new SerialisedCurrency(symbol.Symbol.BaseCurrency);
            this.Name = symbol.Name;
            this.Exchange = symbol.Exchange;
        }

        public ExchangeSymbol Deserialise()
        {
            return new ExchangeSymbol(new GenericSymbol(this.QuoteCurrency.Deserialise(), this.BaseCurrency.Deserialise()), this.Exchange, this.Name);
        }
    }
}
