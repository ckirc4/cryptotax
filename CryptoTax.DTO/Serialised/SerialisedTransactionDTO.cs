﻿using CryptoTax.Shared.Enums;
using Newtonsoft.Json;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedTransactionDTO : SerialisedContainerEventDTO
    {
        [JsonProperty("l")]
        public Exchange TxExchange;
        [JsonProperty("m")]
        public string InternalId;
        [JsonProperty("n")]
        public string PublicId;
        [JsonProperty("o")]
        public string Counterparty;
        [JsonProperty("p")]
        public TransactionType TxType;

        public SerialisedTransactionDTO() { }

        public SerialisedTransactionDTO(TransactionDTO tx) : base(tx.ContainerEvent)
        {
            this.TxExchange = tx.Exchange;
            this.InternalId = tx.InternalTransactionId;
            this.PublicId = tx.PublicTransactionId;
            this.Counterparty = tx.Counterparty;
            this.TxType = tx.Type;
        }

        public TransactionDTO DeserialiseTx()
        {
            return new TransactionDTO(this.Time, this.TxExchange, this.InternalId, this.PublicId, this.Counterparty, this.TxType, this.DeserialiseContainerEvent());
        }
    }
}
