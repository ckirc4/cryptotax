﻿using CryptoTax.Shared;
using CryptoTax.Shared.Enums;
using Newtonsoft.Json;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedCurrency
    {
        [JsonProperty("a")]
        public string Name;
        [JsonProperty("b")]
        public string Symbol;
        [JsonProperty("c")]
        public bool IsFiat;
        [JsonProperty("d")]
        public BlockchainType Blockchain;

        public SerialisedCurrency() { }

        public SerialisedCurrency(Currency currency)
        {
            this.Name = currency.Name;
            this.Symbol = currency.Symbol;
            this.IsFiat = currency.IsFiat;
            this.Blockchain = currency.Blockchain;
        }

        public Currency Deserialise()
        {
            return Currency.FromSerialised(this.Name, this.Symbol, this.IsFiat, this.Blockchain);
        }
    }
}
