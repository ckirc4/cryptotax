﻿using CryptoTax.Shared.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CryptoTax.DTO.Serialised
{
    public class SerialisedCandlestickDtoCollection
    {
        public DateTime BaseTime { get; set; }
        public CandlestickInterval Interval { get; set; }
        public SerialisedExchangeSymbol Symbol { get; set; }
        public IEnumerable<SerialisedCandlestickDTO> Candlesticks { get; set; }

        public SerialisedCandlestickDtoCollection() { }

        public SerialisedCandlestickDtoCollection(IEnumerable<CandlestickDTO> candlesticks)
        {
            this.BaseTime = candlesticks.First().Time;
            this.Interval = candlesticks.First().Interval;
            this.Symbol = new SerialisedExchangeSymbol(candlesticks.First().Symbol);
            this.Candlesticks = candlesticks.Select(c => new SerialisedCandlestickDTO(c, this.BaseTime)).ToList();
        }

        public IEnumerable<CandlestickDTO> Deserialise()
        {
            return this.Candlesticks.Select(c => c.Deserialise(this.BaseTime, this.Interval, this.Symbol.Deserialise()));
        }
    }
}
