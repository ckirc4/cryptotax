﻿namespace CryptoTax.DTO
{
    public class IdProviderDTO
    {
        public static IdProviderDTO Empty = new IdProviderDTO(null, null, null, null, null);

        public long? InventoryId { get; private set; }
        public long? CurrencyAmountId { get; private set; }
        public long? ReportDataRowId { get; private set; }
        public long? ReportRowId { get; private set; }
        public long? RunId { get; private set; }

        public IdProviderDTO(long? inventoryId, long? currencyAmountId, long? reportDataRowId, long? reportRowId, long? runId)
        {
            this.InventoryId = inventoryId;
            this.CurrencyAmountId = currencyAmountId;
            this.ReportDataRowId = reportDataRowId;
            this.ReportRowId = reportRowId;
            this.RunId = runId;
        }
    }
}
